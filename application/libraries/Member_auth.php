<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**

 * Author: Rabin Shrestha
 * 		  rabin.shrestha@ebpearls.com
 *         @rabinshrestha
 *
 *
 * Created:  20.03.2015
 *
 */
class Member_auth {

    function __construct() {

        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;

        $this->load->model('member_auth_model');
    }

    public function __get($var) {
        return get_instance()->$var;
    }

    public function __call($method, $arguments) {
        if (!method_exists($this->member_auth_model, $method)) {
            throw new Exception('Undefined method Member_auth::' . $method . '() called');
        }

        return call_user_func_array(array($this->member_auth_model, $method), $arguments);
    }

    /**
     * For user logout (session is destroyed)
     * 
     * @return boolean
     */
    public function logout() {
        //Destroy the session
        $this->session->sess_destroy();

        //Recreate the session
        if (substr(CI_VERSION, 0, 1) == '2') {
            $this->session->sess_create();
        }

        $this->set_message('Logout Successful');
        return TRUE;
    }

}
