<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'third_party/paypal-php-library/autoload.php');

class paypal 
{
    
    var $quint_paypal_email = 'melinawilkins@optusnet.com.au';
    var $printer_paypal_email = 'paypal@mostly.com.au';
    
    function __construct($config = array())
	{
        $this->PayPalConfig = array(
                            'Sandbox'               => $config['sandbox'],
                            'DeveloperAccountEmail' => $config['developer_account_email'],
                            'ApplicationID'         => $config['application_id'],
                            'DeviceID'              => $config['device_id'],
                            'IPAddress'             => $_SERVER['REMOTE_ADDR'],
                            'APIUsername'           => $config['api_username'],
                            'APIPassword'           => $config['api_password'],
                            'APISignature'          => $config['api_signature'], 
                            'APISubject'            => $config['api_subject'], 
                            'PrintHeaders'          => $config['print_headers'],
                            'LogResults'            => $config['log_results'],
                            'LogPath'               => $config['log_path'],	
                
                
                );
        

        
	}
    
    function DoDirectPayment($data)
    {
        $PayPal = new angelleye\PayPal\PayPal($this->PayPalConfig);

        $DPFields = array(
                            'paymentaction' => 'Sale', 						// How you want to obtain payment.  Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                            'ipaddress' => $_SERVER['REMOTE_ADDR'], 							// Required.  IP address of the payer's browser.
                            'returnfmfdetails' => '1' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                        );

        $CCDetails = array(
                            'creditcardtype' => $data['creditcardtype'],//'MasterCard', 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                            'acct' => $data['card_number'], 								// Required.  Credit card number.  No spaces or punctuation.  
                            'expdate' => $data['expdate'], 							// Required.  Credit card expiration date.  Format is MMYYYY
                            'cvv2' => $data['cvv2'], 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
                            'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
                            'issuenumber' => ''							// Issue number of Maestro or Solo card.  Two numeric digits max.
                        );

        $PayerInfo = array(
                            'email' => $data['email'], 								// Email address of payer.
                            'payerid' => '', 							// Unique PayPal customer ID for payer.
                            'payerstatus' => '', 						// Status of payer.  Values are verified or unverified
                            'business' => '' 							// Payer's business name.
                        );

        $PayerName = array(
                            'salutation' => '', 						// Payer's salutation.  20 char max.
                            'firstname' => $data['firstname'], 							// Payer's first name.  25 char max.
                            'middlename' => '', 						// Payer's middle name.  25 char max.
                            'lastname' => $data['lastname'], 							// Payer's last name.  25 char max.
                            'suffix' => ''								// Payer's suffix.  12 char max.
                        );

        $BillingAddress = array(
                                'street' => '', 						// Required.  First street address.
                                'street2' => '', 						// Second street address.
                                'city' => '', 							// Required.  Name of City.
                                'state' => '', 							// Required. Name of State or Province.
                                'countrycode' => '', 					// Required.  Country code.
                                'zip' => '', 							// Required.  Postal code of payer.
                                'phonenum' => '' 						// Phone Number of payer.  20 char max.
                            );

        $ShippingAddress = array(
                                'shiptoname' => '', 					// Required if shipping is included.  Person's name associated with this address.  32 char max.
                                'shiptostreet' => '', 					// Required if shipping is included.  First street address.  100 char max.
                                'shiptostreet2' => '', 					// Second street address.  100 char max.
                                'shiptocity' => '', 					// Required if shipping is included.  Name of city.  40 char max.
                                'shiptostate' => '', 					// Required if shipping is included.  Name of state or province.  40 char max.
                                'shiptozip' => '', 						// Required if shipping is included.  Postal code of shipping address.  20 char max.
                                'shiptocountrycode' => '', 				// Required if shipping is included.  Country code of shipping address.  2 char max.
                                'shiptophonenum' => ''					// Phone number for shipping address.  20 char max.
                                );

        $PaymentDetails = array(
                                'amt' => $data['amount'], 							// Required.  Total amount of order, including shipping, handling, and tax.  
                                'currencycode' => $data['currency'], 					// Required.  Three-letter currency code.  Default is USD.
                                'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                                'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                                'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                                'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
                                'desc' => $data['desc'], 							// Description of the order the customer is purchasing.  127 char max.
                                'custom' => $data['other_data'], 						// Free-form field for your own use.  256 char max.
                                'invnum' => '', 						// Your own invoice or tracking number
                                'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
                                'notifyurl' => ''						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                            );

        $OrderItems = array();
        foreach($data['items'] as $val) {
            $Item	 = array(
                                    'l_name' => $val['name'], 						// Item Name.  127 char max.
                                    'l_desc' => $val['desc'], 						// Item description.  127 char max.
                                    'l_amt' => $val['cost'], 							// Cost of individual item.
                                    'l_number' => '', 						// Item Number.  127 char max.
                                    'l_qty' => $val['qty'], 							// Item quantity.  Must be any positive integer.  
                                    'l_taxamt' => $val['tax'], 						// Item's sales tax amount.
                                    'l_ebayitemnumber' => '', 				// eBay auction number of item.
                                    'l_ebayitemauctiontxnid' => '', 		// eBay transaction ID of purchased item.
                                    'l_ebayitemorderid' => '' 				// eBay order ID for the item.
                            );
            array_push($OrderItems, $Item);
            
        }

        $PayPalRequestData = array(
                                   'DPFields' => $DPFields, 
                                   'CCDetails' => $CCDetails, 
                                   'PayerInfo' => $PayerInfo,
                                   'PayerName' => $PayerName, 
                                   'BillingAddress' => $BillingAddress, 
                                   'PaymentDetails' => $PaymentDetails, 
                                   'OrderItems' => $OrderItems
                                   );

        $PayPalResult = $PayPal -> DoDirectPayment($PayPalRequestData);
        
        $this->paypal0bj = $PayPal;
        
        return $PayPalResult;

//        $_SESSION['transaction_id'] = isset($PayPalResult['TRANSACTIONID']) ? $PayPalResult['TRANSACTIONID'] : '';
//
//        echo '<pre />';
//        print_r($PayPalResult);
        
    }
    
    function splitPayment($data)
    {
        $PayPal = new angelleye\PayPal\Adaptive($this->PayPalConfig);

        // Prepare request arrays
        $PayRequestFields = array(
                                'ActionType' => 'PAY', 								// Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
                                'CancelURL' => site_url('webservice/payal_cancel'), 									// Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
                                'CurrencyCode' => $data['currency'], 								// Required.  3 character currency code.
                                'FeesPayer' => '', 									// The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
                                'IPNNotificationURL' => site_url('webservice/payal_ipn'), 						// The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
//                                'IPNNotificationURL' => 'http://ci.draftserver.com/quint/webservice/ipn', 						// The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
                                'Memo' => '', 										// A note associated with the payment (text, not HTML).  1000 char max
                                'Pin' => '', 										// The sener's personal id number, which was specified when the sender signed up for the preapproval
                                'PreapprovalKey' => '', 							// The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.  
                                'ReturnURL' => site_url('webservice/payal_success'),								// Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
                                'ReverseAllParallelPaymentsOnError' => '', 			// Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
                                'SenderEmail' => '', 								// Sender's email address.  127 char max.
                                'TrackingID' => ''									// Unique ID that you specify to track the payment.  127 char max.
                                );

        $ClientDetailsFields = array(
                                'CustomerID' => $data['user_id'], 								// Your ID for the sender  127 char max.
                                'CustomerType' => '', 								// Your ID of the type of customer.  127 char max.
                                'GeoLocation' => '', 								// Sender's geographic location
                                'Model' => '', 										// A sub-identification of the application.  127 char max.
                                'PartnerName' => ''									// Your organization's name or ID
                                );

        $FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');

        $Receivers = array();
        // for printer
        $Receiver = array(
                        'Amount' => $data['printer_amount'], 											// Required.  Amount to be paid to the receiver.
                        'Email' => $this->printer_paypal_email, 												// Receiver's email address. 127 char max.
                        'InvoiceID' => '', 											// The invoice number for the payment.  127 char max.
                        'PaymentType' => '', 										// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
                        'PaymentSubType' => '', 									// The transaction subtype for the payment.
                        'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
                        'Primary' => ''												// Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
                        );
        array_push($Receivers,$Receiver);

        // for quint
        $Receiver = array(
                        'Amount' => $data['quint_amount'],									// Required.  Amount to be paid to the receiver.
                        'Email' => $this->quint_paypal_email, 												// Receiver's email address. 127 char max.
                        'InvoiceID' => '', 											// The invoice number for the payment.  127 char max.
                        'PaymentType' => '', 										// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
                        'PaymentSubType' => '', 									// The transaction subtype for the payment.
                        'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
                        'Primary' => ''												// Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
                        );
        array_push($Receivers,$Receiver);

        $SenderIdentifierFields = array(
                                        'UseCredentials' => ''						// If TRUE, use credentials to identify the sender.  Default is false.
                                        );

        $AccountIdentifierFields = array(
                                        'Email' => '', 								// Sender's email address.  127 char max.
                                        'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')								// Sender's phone number.  Numbers only.
                                        );

        $PayPalRequestData = array(
                            'PayRequestFields' => $PayRequestFields, 
                            'ClientDetailsFields' => $ClientDetailsFields, 
                            //'FundingTypes' => $FundingTypes, 
                            'Receivers' => $Receivers, 
                            'SenderIdentifierFields' => $SenderIdentifierFields, 
                            'AccountIdentifierFields' => $AccountIdentifierFields
                            );

        // Pass data into class for processing with PayPal and load the response array into $PayPalResult
        $PayPalResult = $PayPal->Pay($PayPalRequestData);
        
        $this->paypal0bj = $PayPal;
        
        if (!$PayPal->APICallSuccessful($PayPalResult['Ack'])) {
            $errors = array('Errors' => $PayPalResult['Errors']);
            
            return FALSE;
        } else { 
            return $PayPalResult;
        }
        
    }
    function payment($data){
         $PayPal = new angelleye\PayPal\Adaptive($this->PayPalConfig);

        // Prepare request arrays
        $PayRequestFields = array(
                                'ActionType' => 'PAY', 								// Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
                                'CancelURL' => site_url('orders/paypal_cancel'), 									// Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
                                'CurrencyCode' => 'AUD', 								// Required.  3 character currency code.
                                'FeesPayer' => '', 									// The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
                                'IPNNotificationURL' => site_url('orders/paypal_ipn'), 						// The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
//                                'IPNNotificationURL' => 'http://ci.draftserver.com/quint/webservice/ipn', 						// The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
                                'Memo' => '', 										// A note associated with the payment (text, not HTML).  1000 char max
                                'Pin' => '', 										// The sener's personal id number, which was specified when the sender signed up for the preapproval
                                'PreapprovalKey' => '', 							// The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.  
                                'ReturnURL' => site_url('orders/payal_success'),								// Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
                                'ReverseAllParallelPaymentsOnError' => '', 			// Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
                                'SenderEmail' => '', 								// Sender's email address.  127 char max.
                                'TrackingID' => ''									// Unique ID that you specify to track the payment.  127 char max.
                                );

        $ClientDetailsFields = array(
                                'CustomerID' => $data['member_id'], 								// Your ID for the sender  127 char max.
                                'CustomerType' => '', 								// Your ID of the type of customer.  127 char max.
                                'GeoLocation' => '', 								// Sender's geographic location
                                'Model' => '', 										// A sub-identification of the application.  127 char max.
                                'PartnerName' => ''									// Your organization's name or ID
                                );

        $FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');

        $Receivers = array();
        
        $Receiver = array(
                        'Amount' => $data['amount'], 											// Required.  Amount to be paid to the receiver.
                        'Email' => 'ebpearls.ta@gmail.com', 												// Receiver's email address. 127 char max.
                        'InvoiceID' => '', 											// The invoice number for the payment.  127 char max.
                        'PaymentType' => '', 										// Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
                        'PaymentSubType' => '', 									// The transaction subtype for the payment.
                        'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
                        'Primary' => ''												// Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
                        );
        array_push($Receivers,$Receiver);
          $SenderIdentifierFields = array(
                                        'UseCredentials' => ''						// If TRUE, use credentials to identify the sender.  Default is false.
                                        );

        $AccountIdentifierFields = array(
                                        'Email' => '', 								// Sender's email address.  127 char max.
                                        'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')								// Sender's phone number.  Numbers only.
                                        );

        $PayPalRequestData = array(
                            'PayRequestFields' => $PayRequestFields, 
                            'ClientDetailsFields' => $ClientDetailsFields, 
                            //'FundingTypes' => $FundingTypes, 
                            'Receivers' => $Receivers, 
                            'SenderIdentifierFields' => $SenderIdentifierFields, 
                            'AccountIdentifierFields' => $AccountIdentifierFields
                            );

      //  echo 'hi';die;
        // Pass data into class for processing with PayPal and load the response array into $PayPalResult
        $PayPalResult = $PayPal->Pay($PayPalRequestData);
        
        $this->paypal0bj = $PayPal;
        
        if (!$PayPal->APICallSuccessful($PayPalResult['Ack'])) {
            $errors = array('Errors' => $PayPalResult['Errors']);
//            debug($errors);die;
            return FALSE;
        } else { 
            return $PayPalResult;
        }
    }
    
    function paymentDetail($pay_key)
    {
        $PayPal = new angelleye\PayPal\Adaptive($this->PayPalConfig);

        // Prepare request arrays
        $PaymentDetailsFields = array(
                                    'PayKey' => $pay_key, 							// The pay key that identifies the payment for which you want to retrieve details.  
                                    'TransactionID' => '', 						// The PayPal transaction ID associated with the payment.  
                                    'TrackingID' => ''							// The tracking ID that was specified for this payment in the PayRequest message.  127 char max.
                                    );

        $PayPalRequestData = array('PaymentDetailsFields' => $PaymentDetailsFields);


        // Pass data into class for processing with PayPal and load the response array into $PayPalResult
        $PayPalResult = $PayPal->PaymentDetails($PayPalRequestData);

        $DOM = new DOMDocument();
		$DOM -> loadXML($PayPalResult['XMLResponse']);

        $total_payments = $DOM->getElementsByTagName('paymentInfo')->length;
        $payments = array();
        
        for($i = 0;$i< $total_payments; $i++) {
            $payment = $DOM->getElementsByTagName('paymentInfo')->item($i);
            
            $TransactionID = $payment -> getElementsByTagName('transactionId') -> length > 0 ? $payment -> getElementsByTagName('transactionId') -> item(0) -> nodeValue : '';
            $TransactionStatus = $payment -> getElementsByTagName('transactionStatus') -> length > 0 ? $payment -> getElementsByTagName('transactionStatus') -> item(0) -> nodeValue : '';
            $Amount = $payment -> getElementsByTagName('amount') -> length > 0 ? $payment -> getElementsByTagName('amount') -> item(0) -> nodeValue : '';
            $receiver = $payment -> getElementsByTagName('email') -> length > 0 ? $payment -> getElementsByTagName('email') -> item(0) -> nodeValue : '';
            
            $paymentinfo = array(
                    'receiver' => $receiver,
                    'Amount' => $Amount, 
                    'TransactionID' => $TransactionID, 
                    'TransactionStatus' => $TransactionStatus
            );
            array_push($payments, $paymentinfo);
        }
        $PayPalResult['payments'] = $payments;
        
        $this->paypal0bj = $PayPal;
        return $PayPalResult;
    }

}