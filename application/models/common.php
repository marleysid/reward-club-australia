<?php if(!defined('BASEPATH'))	exit('No direct script access allowed');

class common extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function code($code)
	{
		$codes = array(
			'0000' => 'Error occured',
			'0001' => 'Success',
			'0002' => 'Email or password is incorrect',
			'0003' => 'User registered',
			'0004' => 'Email already exists',
			'0005' => 'Image upload error',
			'0006' => 'You are not logged in from this device',
			'0007' => 'User is inactive.',
                
                
		);
		return $codes[$code];
	}
	
	function insert_data($table_name,$data)
	{
		if(is_array($data) && !empty($data)){
			$insrtdb = $this->load->database('default', TRUE);
			$result = $insrtdb->insert($table_name, $data);
			if($result)
			{
				return $insrtdb->insert_id();
			}
		}
		return FALSE;
	}
	
	function insert_batch($table_name,$data) 
	{
		if(is_array($data) && !empty($data)){
			$insrtdb = $this->load->database('default', TRUE);
			$result = $insrtdb->insert_batch($table_name, $data);
		}
		return $result ? TRUE : FALSE;
	}
	
}