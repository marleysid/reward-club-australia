<?php

/**
 * Description of Member_auth_model
 *
 * @author rabin
 */
class Member_auth_model extends CI_Model {

    protected $errors;
    protected $messages;
    protected $table;
    protected $user_id;
    protected $_ion_hooks;

    function __construct() {
        parent::__construct();
        $this->errors = array();
        $this->messages = array();
        $this->table = 'users';
        $this->load->config('member',TRUE);



        //initialize db tables data
        $this->tables = $this->config->item('tables', 'member');
        $this->store_salt = $this->config->item('store_salt', 'member');
        $this->salt_length = $this->config->item('salt_length', 'member');
        $this->hash_method = $this->config->item('hash_method', 'member');
        $this->message = $this->config->item('success_user_create', 'member');
        //echo "here".$this->hash_method; exit;
        if ($this->logged_in()) {
            $this->user_id = $this->session->userdata('id');
        }
    }

    /**
     * 
     * @param string $identity
     * @param string $password
     * @return boolean
     */
    public function login($identity, $password) {
        $query = $this->db->select('id, first_name,last_name,email, password, suburb')
                ->where(
                        array('email' => $identity,
                            'active' => '1',
                ))
                ->limit(1)
                ->get('users');
        if ($query->num_rows() == 1) {
            $user = $query->row();
            $this->hash_password_db($user->id, $password);

            if ($user->password == $this->hash_password_db($user->id, $password)){
                 $this->set_session($user);
                $this->set_message('login_successful');
                return TRUE;
            }
           
        }
        $this->set_error('Invalid email/password');
        return FALSE;
    }

    private function set_session($user) {

        $session_data = array(
            'id' => $user->id,
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'suburb' => $user->suburb
        );
        if($user->suburb!=""){
            $current_loc = get_lat_long_generic($user->suburb);
            $_SESSION['latitude']= $current_loc['lat'];
            $_SESSION['longitude'] = $current_loc['long'];
            $_SESSION['distance'] =  15; 
        }
        $this->session->set_userdata($session_data);
        $this->session->set_userdata('customLocation', $user->suburb);
        return TRUE;
    }

    /**
     * Get user details, if no parameter is supplied then detail of current logged in user is returned
     * @param type $id
     * @return boolean
     */
    public function user($id = NULL) {
        if ($this->logged_in()) {
            $id || $id = $this->user_id;
            $query = $this->db
                    ->select('id , first_name , last_name, email,contact_no,address,postcode,suburb,membership_type_id, club_id')
                    ->where(
                            array('id' => $id,
                                )
                    )
                    ->limit(1)
                    ->get('users');
            if ($query->num_rows() == 1) {
                $user = $query->row();
                return $user;
            }
        }
        $this->set_error('Not logged in');
        return FALSE;
    }

    public function set_error($error) {
        $this->errors[] = $error;
        return $error;
    }

    public function set_message($message) {
        $this->messages[] = $message;
        return $message;
    }

    public function get_errors() {
        $_output = '';
        foreach ($this->errors as $value) {
            $_output .= $value;
        }
        return $_output;
    }

    public function get_messages() {
        $_output = '';
        foreach ($this->messages as $value) {
            $_output .= $value;
        }
        return $_output;
        return $this->messages;
    }

    /**
     * register
     *
     * @return bool
     * @author Rabin
     * */
    public function register($email, $password, $additional_data = array()) {
        $query = $this->db
                ->select('id')
                ->where('email', $email)
                ->limit(1)
                ->get($this->table);
        if ($query->num_rows() == '1') {
            // user with email already registered
            $this->set_error($this->config->item('email_exist', 'member_config'));
            return FALSE;
        }
        $salt = $this->store_salt ? $this->salt() : FALSE;

        $password = $this->hash_password($password, $salt);
        // Users table.

        $data = array(
            'email' => $email,
            'password' => $password,
        );

        if ($this->store_salt) {
            $data['salt'] = $salt;
        }
        $insertData = array_merge($data, $additional_data);
        $this->db->insert($this->table, $insertData);
        $insertId = $this->db->insert_id();
        if ($insertId)
            $this->set_message($this->config->item('success_user_create', 'member_config'));
        else
            $this->set_error($this->config->item('error_user_create', 'member_config'));
        return (isset($insertId)) ? $insertId : FALSE;
    }

    public function edit_profile($id, $email, $first_name, $last_name, $member_contact_no, $member_postcode, $member_address, $suburb) {

        $this->db->update('users', array(
            'email' => $email,
            'first_name' => $first_name,
            'last_name' => $last_name,
            'address' => $member_address,
            'contact_no' => $member_contact_no,
            'postcode' => $member_postcode,
            'suburb' => $suburb),
          array('id' => $id));

        $current_loc = get_lat_long_generic($suburb);
        $_SESSION['latitude']= $current_loc['lat'];
        $_SESSION['longitude'] = $current_loc['long'];
        $_SESSION['distance'] =  15; 

        return TRUE;
    }

    public function change_password($old_password, $id) {
        //echo $password . $old_password . $id . "<br/>";
        /*$salt = $this->store_salt ? $this->salt() : FALSE;

        $password123= array(
            $old_password = $this->hash_password($old_password, $salt),
            $password = $this->hash_password($password, $salt));


        return $password123;*/
        $password_matches = $this->hash_password_db($id, $old_password);
         if ($password_matches){
                return TRUE;
         }
    }

    public function new_pasword_hash($password, $id) {
        $salt = $this->store_salt ? $this->salt() : FALSE;
        $password = $this->hash_password($password, $salt);
        $this->db->update('users', array(
            'password' => $password),
            array('id' => $id));

        return ;
    }
    /**
     * Check if user is logged in
     * 
     * @return boolean
     */
    public function logged_in() {
        if ($this->session->userdata('id')) {
            return TRUE;
        }
        return FALSE;
    }

    function hashCode($length = 32) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }

    public function encryptPassword($password) {
        if (empty($password)) {
            return FALSE;
        }
        return sha1($password);
    }

    public function salt() {
        return substr(md5(uniqid(rand(), true)), 0, $this->salt_length);
    }


    public function hash_password($password, $salt = false, $use_sha1_override = FALSE) {

        if (empty($password)) {
            return FALSE;
        }
        //dd($this->hash_method);
        //bcrypt
        if ($use_sha1_override === FALSE && $this->hash_method == 'bcrypt') {
            return $this->bcrypt->hash($password);
        }


        if ($this->store_salt && $salt) {
            return sha1($password . $salt);
        } else {
            $salt = $this->salt();
            return $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
        }
    }


    public function hash_code($password) {
        return $this->hash_password($password, FALSE, TRUE);
    }
    public function hash_password_db($id, $password, $use_sha1_override = FALSE) {

        if (empty($id) || empty($password)) {
            return FALSE;
        }

        $this->trigger_events('extra_where');

        $query = $this->db->select('password, salt')
            ->where('id', $id)
            ->limit(1)
            ->get($this->tables['users']);

        $hash_password_db = $query->row();

            
           
        if ($query->num_rows() !== 1) {
            return FALSE;
        }

        // bcrypt
        if ($use_sha1_override === FALSE && $this->hash_method == 'bcrypt') {
            if ($this->bcrypt->verify($password, trim($hash_password_db->password))) {
                return TRUE;
            }
            return FALSE;
        }

        // sha1
        if ($this->store_salt) {
            $db_password = sha1($password . $hash_password_db->salt);
        } else {
            $salt = substr($hash_password_db->password, 0, $this->salt_length);

            $db_password = $salt . substr(sha1($salt . $password), 0, -$this->salt_length);
        }

        if ($db_password == $hash_password_db->password) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public function trigger_events($events) {
        if (is_array($events) && !empty($events)) {
            foreach ($events as $event) {
                $this->trigger_events($event);
            }
        } else {
            if (isset($this->_ion_hooks->$events) && !empty($this->_ion_hooks->$events)) {
                foreach ($this->_ion_hooks->$events as $name => $hook) {
                    $this->_call_hook($events, $name);
                }
            }
        }
    }
    protected function _call_hook($event, $name) {
        if (isset($this->_ion_hooks->{$event}[$name]) && method_exists($this->_ion_hooks->{$event}[$name]->class, $this->_ion_hooks->{$event}[$name]->method)) {
            $hook = $this->_ion_hooks->{$event}[$name];

            return call_user_func_array(array($hook->class, $hook->method), $hook->arguments);
        }

        return FALSE;
    }
}
