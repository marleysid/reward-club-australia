<!-- banner -->

<div class="banner">
  <div class="slider"><img src="<?php echo get_asset('assets/frontend/images/banner_of_club_member.jpg'); ?>"  class="img-responsive" alt="slide"></div>
  <div class="banner-slider">
    
  </div>
</div>
<!-- banner--> 

<!-- search-featured -->
<div class="search-featured" id="search-featured">
<div class="container">
<div class="col-md-6 member-detail club_for_member_wrp">
  <h3><span>Get in Touch ddd</span></h3>
  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto</p>
  <div class="advanced-search">
    <form role="form">
      <div class="col-md-12 pdn_mrgn_nn">
        <div class="col-md-6 pdn_mrgn_nn_right">
          <div class="wrap">
            <input type="Phone" class="form-control nt_wth_incld" placeholder="Name" />
          </div>
          <div class="wrap">
            <input type="Phone" class="form-control nt_wth_incld"  placeholder="Contact No." />
          </div>
        </div>
        <div class="col-md-6 pdn_mrgn_nn">
          <div class="wrap">
            <input type="Phone"  class="form-control nt_wth_incld" placeholder="Address" />
          </div>
          <div class="wrap">
            <input type="Phone" class="form-control nt_wth_incld" placeholder="Email Address" />
          </div>
          
        </div>
      </div>
      <div class="form-group">
        <h4 class="left_align"><em>Choose a sample promotional &amp; Support Material you want to enquire about.</em></h4>
      </div>
      <div class="form-group">
        <label class="wth_prcnt">Type of Enquiry</label>
        <select class="form-control wth_prcnt_01" id="select-sub-category">
          <option Type of Enquiry></option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </select>
      </div>
      <div class="form-group">
        <input type="Phone"  class="form-control nt_wth_incld" placeholder="subject" />
      </div>
      <div class="text-right">
        <button class="btn btn-danger">search</button>
      </div>
    </form>
  </div>
</div>
<div class="col-md-6 vdo-md-cls">
<!-- featureCarousel -->
<ul class="adrs_wrp_vip">
  <li><img src="<?php echo get_asset('assets/frontend/images/your_vip_card.jpg'); ?>"></li>
  <li> <a href="#"> <span class="glyphicon glyphicon-map-marker"></span> </a> Suite 103, Lvl 1, 270 Pacific Highway, Crows Nest NSW</li>
  <li><a href="#"> <span class="glyphicon glyphicon-credit-card"></span> </a> PO Box 255, Neutral Bay NSW 2089 Australia</li>
  <li></li>
  <li> <a href="#"> <span class="glyphicon glyphicon-earphone"></span> </a> 1300 305 690</li>
  <li> <a href="#"> <span class="glyphicon glyphicon-print"></span> </a>1300 884 208</li>
  <li><a href="#"> <span class="glyphicon glyphicon-envelope"></span> </a> info@rewardsclub.com.au</li>
  <p>Rewards Club VIP benefits are included on selected club memberships. 
    For information about your local club <a><u>click here</u></a></p>
</ul>
<h4>Sample Promotional &amp; Support Material <span>(see Order Supplies above)</span></h4>
<ul class="adrs_wrp_vip_01">
<li> <img src="<?php echo get_asset('assets/frontend/images/cnt_00.jpg'); ?>" class="img-responsive" > </li>
<li> <img src="<?php echo get_asset('assets/frontend/images/cnt_01.jpg'); ?>" class="img-responsive"> </li>
<li> <img src="<?php echo get_asset('assets/frontend/images/cnt_02.jpg'); ?>" class="img-responsive"> </li>
<li> <img src="<?php echo get_asset('assets/frontend/images/cnt_03.jpg'); ?>" class="img-responsive"> </li>
</ul>
<!-- featureCarousel-->
</div>
</div>
</div>

<div class="feature-service">
  <div class="container margn_wrp_fx_bx">
    <h3 class="mrgn_top_02"><span>Rewards Clubs Partners</span></h3>
    <div id="owl-service" class="owl-carousel brd_nn">
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/beaumont_tiles.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/Harvey_norman_logo.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/hungry_jacks_logo.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/bakers_Delight.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/subway_logo.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/tackle_world.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/bakers_Delight.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/beaumont_tiles.jpg'); ?>" class="img-responsive"></div>
    </div>
  </div>
</div>

<!-- media --> 