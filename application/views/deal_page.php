<div class="banner">
  <div class="slider"><img src="<?php echo get_asset('assets/frontend/images/banner_of_club_member.jpg'); ?>"  class="img-responsive" alt="slide"></div>
  <div class="banner-slider">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
      <ul>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="col-md-12">
<div class="container"> 
  <!-- breadcrumbs -->
  <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Library</a></li>
    <li class="active">Data</li>
  </ol>
  <div class="row">
    <div class="col-sm-3">
      <div class="local-search club">
        <h4>Search a different region</h4>
        <form role="form">
          <div class="form-group">
            <label class="lbl_cls">Region</label>
            <select class="form-control input_txt">
              <option>Illawarra</option>
            </select>
          </div>
        </form>
      </div>
      <br />
      <div class="local-search club refine_search">
        <h4>Refine Search</h4>
        <ul id="nav">
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Cafe and Takeaway</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-5</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-6</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-7</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-8</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Grocery</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Home and Garden</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Leisure and Entertainment </a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Online Shopping</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Restaurants</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Shopping and Fashion</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Utilites</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                
              
            </ul>
      </div>
     <img src="<?php echo get_asset('assets/frontend/images/bulk_01.jpg'); ?>" class="img-responsive">
     
    </div>
    <div class="col-sm-9"> 
      <!--repeated start here -->
      <div class="col-md-12 bg_nn_pgmrgn">
        <div class="col-sm-3 pdn_mrgn_nn">
          <h5>Showing 1- 10 of 34 total results</h5>
        </div>
        <div class="col-sm-5 pdn_mrgn_nn">
          <ul class="deal_page_map">
            <li><strong>View as</strong></li>
            <li>
              <button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-th-large"></span> </button>
              Grid</li>
            <li>
              <button type="button" class="active btn btn-default btn-sm"> <span class="glyphicon glyphicon-list"></span> </button>
              List</li>
            <li>Map
              <button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-map-marker"></span> </button>
            </li>
          </ul>
        </div>
        <div class="col-sm-4 ">
          <div class="local-search club bg_nn_pgmrgn">
            <form role="form">
              <div class="form-group">
                <label class="lbl_cls">Sort By</label>
                <select class="form-control input_txt clr">
                  <option>Illawarra</option>
                </select>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div class="col-md-12 tp_btn_mrgn_wrp bg_nn_pgmrgn">
        <div class="col-sm-4">
          <button class="hvr active">
           <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm">Best price</span>
        <img src="<?php echo get_asset('assets/frontend/images/img_best_pricebg.png'); ?>" class="img-responsive opct">
        <div class="with_resuld_board">
          <div class="whit_box hdgn_dfn_wrp">
            <h4><strong>Example of Main title for this deal.</strong></h4>
          </div>
          
        </div>
        <div class="light_grw light_grw_lght_wth none_dspl">
            <div class="whit_box hdgn_dfn_wrp red_bg"><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a> <a href="#"><span class="glyphicon glyphicon-heart"></span> </a><a href="#">View Product</a></div>
          </div>
        </button>
      </div>
      <div class="col-sm-4">
          <button class="hvr">
           <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm">Best price</span>
        <img src="<?php echo get_asset('assets/frontend/images/img_best_pricebg.png'); ?>" class="img-responsive opct">
        <div class="with_resuld_board">
          <div class="whit_box hdgn_dfn_wrp">
            <h4><strong>Example of Main title for this deal.</strong></h4>
          </div>
          
        </div>
        <div class="light_grw light_grw_lght_wth none_dspl">
            <div class="whit_box hdgn_dfn_wrp red_bg"><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a> <a href="#"><span class="glyphicon glyphicon-heart"></span> </a><a href="#">View Product</a></div>
          </div>
        </button>
      </div>
      <div class="col-sm-4">
          <button class="hvr">
           <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm">Best price</span>
        <img src="<?php echo get_asset('assets/frontend/images/img_best_pricebg.png'); ?>" class="img-responsive opct">
        <div class="with_resuld_board">
          <div class="whit_box hdgn_dfn_wrp">
            <h4><strong>Example of Main title for this deal.</strong></h4>
          </div>
          
        </div>
        <div class="light_grw light_grw_lght_wth none_dspl">
            <div class="whit_box hdgn_dfn_wrp red_bg"><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a> <a href="#"><span class="glyphicon glyphicon-heart"></span> </a><a href="#">View Product</a></div>
          </div>
        </button>
      </div>
      
    </div>
    <div class="col-md-12 tp_btn_mrgn_wrp bg_nn_pgmrgn">
    <div class="col-sm-4">
          <button class="hvr">
           <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm">Best price</span>
        <img src="<?php echo get_asset('assets/frontend/images/img_best_pricebg.png'); ?>" class="img-responsive opct">
        <div class="with_resuld_board">
          <div class="whit_box hdgn_dfn_wrp">
            <h4><strong>Example of Main title for this deal.</strong></h4>
          </div>
          
        </div>
        <div class="light_grw light_grw_lght_wth none_dspl">
            <div class="whit_box hdgn_dfn_wrp red_bg"><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a> <a href="#"><span class="glyphicon glyphicon-heart"></span> </a><a href="#">View Product</a></div>
          </div>
        </button>
      </div>
      <div class="col-sm-4">
          <button class="hvr">
           <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm">Best price</span>
        <img src="<?php echo get_asset('assets/frontend/images/img_best_pricebg.png'); ?>" class="img-responsive opct">
        <div class="with_resuld_board">
          <div class="whit_box hdgn_dfn_wrp">
            <h4><strong>Example of Main title for this deal.</strong></h4>
          </div>
          
        </div>
        <div class="light_grw light_grw_lght_wth none_dspl">
            <div class="whit_box hdgn_dfn_wrp red_bg"><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a> <a href="#"><span class="glyphicon glyphicon-heart"></span> </a><a href="#">View Product</a></div>
          </div>
        </button>
      </div>
      <div class="col-sm-4 mrgn_tpbtn_only_sml_dvc">
          <button class="hvr">
           <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm">Best price</span>
        <img src="<?php echo get_asset('assets/frontend/images/img_best_pricebg.png'); ?>" class="img-responsive opct">
        <div class="with_resuld_board">
          <div class="whit_box hdgn_dfn_wrp">
            <h4><strong>Example of Main title for this deal.</strong></h4>
          </div>
          
        </div>
        <div class="light_grw light_grw_lght_wth none_dspl">
            <div class="whit_box hdgn_dfn_wrp red_bg"><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a> <a href="#"><span class="glyphicon glyphicon-heart"></span> </a><a href="#">View Product</a></div>
          </div>
        </button>
      </div>
     </div>
     <div class="col-md-12 tp_btn_mrgn_wrp bg_nn_pgmrgn">
    <div class="col-sm-4 mrgn_tpbtn_only_sml_dvc">
          <button class="hvr">
           <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm">Best price</span>
        <img src="<?php echo get_asset('assets/frontend/images/img_best_pricebg.png'); ?>" class="img-responsive opct">
        <div class="with_resuld_board">
          <div class="whit_box hdgn_dfn_wrp">
            <h4><strong>Example of Main title for this deal.</strong></h4>
          </div>
          
        </div>
        <div class="light_grw light_grw_lght_wth none_dspl">
            <div class="whit_box hdgn_dfn_wrp red_bg"><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a> <a href="#"><span class="glyphicon glyphicon-heart"></span> </a><a href="#">View Product</a></div>
          </div>
        </button>
      </div>
      <div class="col-sm-4 mrgn_tpbtn_only_sml_dvc">
          <button class="hvr">
           <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm">Best price</span>
        <img src="<?php echo get_asset('assets/frontend/images/img_best_pricebg.png'); ?>" class="img-responsive opct">
        <div class="with_resuld_board">
          <div class="whit_box hdgn_dfn_wrp">
            <h4><strong>Example of Main title for this deal.</strong></h4>
          </div>
          
        </div>
        <div class="light_grw light_grw_lght_wth none_dspl">
            <div class="whit_box hdgn_dfn_wrp red_bg"><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a> <a href="#"><span class="glyphicon glyphicon-heart"></span> </a><a href="#">View Product</a></div>
          </div>
        </button>
      </div>
      <div class="col-sm-4 mrgn_tpbtn_only_sml_dvc">
          <button class="hvr">
           <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm">Best price</span>
        <img src="<?php echo get_asset('assets/frontend/images/img_best_pricebg.png'); ?>" class="img-responsive opct">
        <div class="with_resuld_board">
          <div class="whit_box hdgn_dfn_wrp">
            <h4><strong>Example of Main title for this deal.</strong></h4>
          </div>
          
        </div>
        <div class="light_grw light_grw_lght_wth none_dspl">
            <div class="whit_box hdgn_dfn_wrp red_bg"><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span></a> <a href="#"><span class="glyphicon glyphicon-heart"></span> </a><a href="#">View Product</a></div>
          </div>
        </button>
      </div>
     </div>
    
    <!--repeated end here --> 
    <!--pagination start here -->
    <div class="col-md-12">
      <div class="col-md-3"> </div>
      <div class="col-md-9 bg_nn_pgmrgn">
        <div class="col-md-7 pgn_wrp_clr">
          <ul class="pagination">
            <li>
              <button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-arrow-left"></span> </button>
            </li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
              <button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-arrow-right"></span> </button>
            </li>
          </ul>
        </div>
        <div class="col-md-4">
          <div class="btn-group" role="group" aria-label="...">
            <div class="btn-group" role="group">
              <h4 class="margin_right_wrp_bxt_itm">Items per page:</h4>
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">1 <span class="caret"></span> </button>
              <ul class="dropdown-menu insrt_with" role="menu">
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--pagination end here --> 
  </div>
</div>
</div>
<div class="feature-service">
  <div class="container">
    <div id="owl-service_01" class="owl-carousel margn_wrp_fx_bx">
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
    </div>
  </div>
</div>
</div>