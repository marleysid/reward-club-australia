<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $site_title; ?></title>
        <meta name="description" content="<?php echo $site_description; ?>" />
        <meta name="keywords" content="<?php echo $site_keywords; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $meta_tag; ?>
        <?php echo $styles; ?>
        <!-- JS -->
        <?php echo $scripts_header; ?>
    </head>
    <body>
        <div id="content">
            <?php echo $content; ?>
        </div>
        <?php echo $scripts_footer; ?>
    </body>
</html>