<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container">

			<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>

			<!-- Be sure to leave the brand out there if you want it shown -->
			<a class="brand" href="<?php echo base_url(); ?>"><?php echo Settings::get('site_title') ?></a>

			<!-- Everything you want hidden at 940px or less, place within here -->

			<div class="nav-collapse collapse navbar-responsive-collapse">
<!--				<ul class="nav">
				</ul>-->

				<ul class="nav pull-right">
					<li><a href="<?php echo base_url('login'); ?>">Login</a></li>
					<li class="divider-vertical"></li>
					<li><a href="<?php echo base_url('register'); ?>">Register</a></li>
				</ul>
			</div>

		</div>
	</div>
</div>
<div style="margin-top: 50px"></div>