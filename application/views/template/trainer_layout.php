<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!-- Consider adding an manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--><html class="no-js" lang="<?php echo $lang; ?>"> <!--<![endif]--><head>
		<meta charset="<?php echo $meta_charset; ?>">
		<title><?php echo $site_title; ?></title>
		<meta name="description" content="<?php echo $site_description; ?>" />
		<meta name="keywords" content="<?php echo $site_keywords; ?>" />
		<?php echo $meta_tag; ?>
		<?php echo $styles; ?>

		<!-- JS -->
		<script type='text/javascript'> var base_url = '<?php echo base_url(); ?>'; //base_url </script>

		<script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/plugins/jquery/jquery-1.10.2.min.js"></script>  


		<link href='http://fonts.googleapis.com/css?family=Lato:300,400,900' rel='stylesheet' type='text/css'>

		<script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/plugins/validation/jquery.validate.js"></script>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/scripts/plugins/validation/additional-methods.js"></script>
		<script src="<?php echo base_url(); ?>assets/scripts/js_loader/script.js"></script>
		<script src="<?php echo base_url(); ?>assets/scripts/utilities.js"></script>
		<script type="text/javascript">
			$script(base_url + "assets/scripts/fitness_calendar/fitness_calendar_model.js", function() {
				$script(base_url + "assets/scripts/fitness_calendar/fitness_calendar.js", function() {
				})
			});
		</script>

		<?php echo $scripts_header; ?>
	</head>
	<body>
		<?php $this->load->view('template/blocks/header'); ?>
		<section>
        	<?php $this->load->view('template/blocks/slider'); ?>
        	<?php $this->load->view('search/blocks/search_block'); ?>
			<div class="container"><?php echo $content; ?></div>
		</section>

		<?php $this->load->view('template/blocks/footer'); ?>
        
		<!--for info popup-->
		<a class="hide" id="info-modal-popup" href="#info-modal" role="button" data-toggle="modal"></a>
		<div id="info-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-header">
		<button type="button" class="close" onClick="$.fancybox.close();" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3></h3>
		</div>
		<div class="modal-body"></div>
		</div>
		<!--end info popup-->
        
		<div id="loading_ajax" style="display: none;">
			<div class="loading-overlay">
				<div id="loading-page">
					<img src="<?php echo base_url('assets/img/ajax-loader_blue.gif'); ?>" alt="loading">
				</div>
			</div>
		</div>

		<?php echo $scripts_footer; ?>
	</body>
</html>

