<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>Rewards Club</title>

    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-TileColor" content="#5bc0de">
    <meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/img/metis-tile.png">
<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/favicon.png">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/bootstrap/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/Font-Awesome/css/font-awesome.min.css">

    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/theme.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/switch/switchv3/build/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/datatables/css/demo_page.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/gritter/css/jquery.gritter.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/datatables/css/DT_bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/lib/fancyBox/source/jquery.fancybox.css">
	<?php echo $styles; ?>
	<script> var base_url = "<?php echo base_url();?>" </script>
    
    <script src="<?php echo base_url(); ?>assets/lib/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
      <script src="<?php echo base_url(); ?>assets/lib/html5shiv/html5shiv.js"></script>
	      <script src="<?php echo base_url(); ?>assets/lib/respond/respond.min.js"></script>
	    <![endif]-->

    <!--Modernizr 3.0-->
    <script src="<?php echo base_url(); ?>assets/lib/modernizr-build.min.js"></script>
	<?php echo $scripts_header; ?>
  </head>
  <body class="padTop53">
    <div id="wrap">
		<?php $this->load->view('template/blocks/admin_header'); ?>
		<?php $this->load->view('template/blocks/admin_sidebar'); ?>
      <div id="content">
        <div class="outer">
		  <?php $this->load->view('bootstrap_messages'); ?>
          <div class="inner">
            <div class="row">
				<?php echo $content; ?>
            </div>
          </div>
          <!-- end .inner -->
        </div>
        <!-- end .outer -->
      </div>
      <!-- end #content -->
    </div><!-- /#wrap -->
	<?php $this->load->view('template/blocks/admin_footer'); ?>
    <script src="<?php echo base_url(); ?>assets/js/utilities.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib/bootstrap/js/bootstrap.min.js"></script>
    <!--<script type="text/javascript" src="<?php // echo base_url(); ?>assets/js/style-switcher.js"></script>-->
	
    <script src="<?php echo base_url(); ?>assets/lib/screenfull/screenfull.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib/switch/switchv3/build/js/bootstrap-switch.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib/datatables/DT_bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib/gritter/js/jquery.gritter.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/lib/fancyBox/source/jquery.fancybox.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/main.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>	
    <?php echo $scripts_footer; ?>

  </body>
</html>
