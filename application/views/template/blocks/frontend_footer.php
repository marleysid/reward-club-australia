<div class="twitter">
   <div class="container">
      <div class="twit" id="latest-twits-rewards">&nbsp;
         <span><?php echo $tweets ?: '&nbsp;'; ?> </span>
      </div>
   </div>
</div>
<footer>
   <div class="container footer">
      <div class="footer-logo-wrap">
         <a class="logo" href="<?php echo site_url(); ?>"><img src="<?php echo get_asset('assets/frontend/images/logo.png'); ?>"  alt="holiday crown"></a>
      </div>
      <div class="footer-nav-wrap">
         <div class=" sitemap">
            <ul class="list-unstyled">
               <li><a href="<?php echo site_url(); ?>">Home</a></li>
               <li><a href="<?php echo site_url('information-for-members'); ?>">Member Information</a></li>
               <li><a href="<?php echo site_url('information-for-clubs'); ?>">Club Information</a></li>
               <li><a href="<?php echo site_url('information-for-suppliers'); ?>">Suppliers Information</a></li>
               <li><a href="<?php echo site_url('contacts'); ?>">Contact Us</a></li>
               <!-- <li><a href="<?php echo site_url('faq'); ?>">faqs</a></li> -->
            </ul>
         </div>
      </div>
      <div class="subscribe-wrap">
         <div class="joinus">
            <h3><span>Subscribe</span> </h3>
            <form method="post" id="joinus_form">
               <span>Subscribe now for hot offers and updates.</span>
               <div class="input-group">
                  <input type="text"  class="form-control" placeholder="Enter your email" name="member_email" id="member_email">
                  <!-- <input type="submit" name="submit" class="email"> -->
                  <input type="submit" value="Subscribe" class="subscribe-button">
               </div>
            </form>
            <div class="joinus-holder">
              <span class="social">
                   <a class="fb-icon-btn" href="<?php echo $facebook_url->value; ?>" target="_blank">
                      <!-- <img src="<?php //echo get_asset('assets/frontend/images/white-small-fb-icon.png'); ?>"> -->
                   </a>
                   <a class="youtube-icon-btn" href="<?php echo $youtube_url->value; ?>" target="_blank">
                      <!-- <img src="<?php//echo get_asset('assets/frontend/images/yt.png'); ?>"> -->
                   </a>
                </span>
               <span class="copyright">© Copyright <a href="#">Rewards Club</a> <?php echo date('Y');?>. <a href="#">Privacy Policy</a>.</span>
               <!--  <h4>Like and follow us</h4> -->

            </div>
         </div>
      </div>
   </div>
   <!--/.row-->
   </div>
   <!--/.container-->

   <!--/.footer-bottom-->
</footer>
<div class="modal " id="feed_back_Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <form method="post" id="feedbackform">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="closelogin"><span aria-hidden="true">&times;</span></button>
               <h3><span>Submit Feedback</span> </h3>
            </div>
            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto</p>
            <div class="feed_background_wrap">
               <div class="modal-body">
                  <div id="feedback_error_msg" class="error-display"> </div>
                  <!--                            <div class="form-group">
                     <label>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</label>
                     </div>-->
                  <div class="form-group">
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6" >
                        <input type="text" name="first_name" class="form-control pp_frm_inpt_01" id="first_name" placeholder="Name*" style="margin-bottom:10px;" >
                     </div>
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                        <input type="text" name="email" class="form-control pp_frm_inpt_01" id="email" placeholder="Email Address*" >
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mrgn_wrp_tp">
                        <textarea class="form-control" rows="3" name="feedback" placeholder="Message*"></textarea>
                     </div>
                  </div>
                  <div class="form-group">
                     <input type="submit" class="btn btn-danger pull-right" id="feedback_button" value="Submit">
                  </div>
               </div>
            </div>
         </div>
      </form>
   </div>
</div>
<div class="modal " id="feedbacksuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="close-feedbacksuccessmsg"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Thank You for your feedback!</h4>
         </div>
         <div class="modal-body">
         </div>
      </div>
   </div>
</div>
<!-- wow script -->
<script src="<?php echo get_asset('assets/frontend/assets/wow/wow.min.js'); ?>"></script>
<!-- owlcarousel -->
<script src="<?php echo get_asset('assets/frontend/assets/owl-carousel/owl.carousel.min.js'); ?>"></script>
<!-- bootstrap -->
<script src="<?php echo get_asset('assets/frontend/assets/bootstrap/js/bootstrap.js'); ?>" type="text/javascript" ></script>
<!-- jquery mobile -->
<script src="<?php echo get_asset('assets/frontend/assets/mobile/touchSwipe.min.js'); ?>"></script>
<script src="<?php echo get_asset('assets/frontend/assets/respond/respond.js'); ?>"></script>
<!-- gallery -->
<script src="<?php echo get_asset('assets/frontend/assets/gallery/jquery.blueimp-gallery.min.js'); ?>"></script>

<script src="<?php echo get_asset('assets/frontend/assets/svgcheckbx.js'); ?>"></script>
<!-- custom script -->
<script src="<?php echo get_asset('assets/frontend/assets/script.js'); ?>"></script>

<!-- selectbox -->
<script type="text/javascript" src="<?php echo get_asset('assets/frontend/assets/selectbox/jScrollPane.js'); ?>"></script>
<script type="text/javascript" src="<?php echo get_asset('assets/frontend/assets/selectbox/jquery.mousewheel.js'); ?>"></script>
<script type="text/javascript" src="<?php echo get_asset('assets/frontend/assets/selectbox/SelectBox.js'); ?>"></script>
<script src="<?php echo get_asset('assets/frontend/assets/jquery.tinyscrollbar.js'); ?>"></script>
<script src="<?php echo get_asset('assets/js/jquery.print.js'); ?>"></script>
<script src="<?php echo get_asset('assets/frontend/js/jquery.range.js'); ?>"></script>

<?php echo $scripts_footer; ?>
 <script type="text/javascript">
   $(function(){

      $("#search").autocomplete({
         source: '<?php echo base_url();?>' + "offers/search_auto" // path to the get_list method
      });

   });

</script>


<script type="text/javascript">
  $(function(){

      $("#clubname").autocomplete({
         source: "cms/clubs" // path to the get_list method
      });

   });

</script>

<script type="text/javascript">
  $(function(){

      $("#suburbpostcode, #location").autocomplete({
         source: '<?php echo base_url();?>' +"cms/postcodes" // path to the get_list method
      });

   });

</script>





<script type="text/javascript">
    $(function(){

        $("#word").autocomplete({
            source: "offers/search_auto_refine" // path to the get_list method
        });

    });

</script>
<script>
  $(function() {
  // select.custom (custom named classed used with selectbox (dropdown)
    $("select.custom").each(function() {
         var sb = new SelectBox({
         selectbox: $(this),
         height: 150,
         width:208,
        });
    });
   
  });
</script>
<script type="text/javascript">
   /*$(function(){
        $("#suburbs-search").autocomplete({
            source: "offers/search_auto_suburbs",
            minLength: 1
        });
    });*/

    $(function(){
      $("#suburbs-search").autocomplete({
          source: function(request, response){
            $.ajax({
            type : 'Get',
               data: {
                term: request.term,
                limit: 6
            },
            url: '<?php echo site_url();?>offers/search_auto_suburbs',
            success: function(data) {
                response( $.map( data, function(item) {
                       
                       return {
                        label: item.label,
                        value: item.value
                    }
                }));
            },
            error: function(data) {
               console.log('failed');
         }
            });

          },
           minLength: 1,
      });
    });
</script>





  <script type="text/javascript">
   $(document).ready(function() {
        $( "#handler" ).slider({
        range: "min",
        value: 1,
        min: 1,
        max: 25,
        slide: function( event, ui ) {
          var leftValue = $('#handler .ui-state-focus')[0].style.left;
          var finalValue = parseFloat(leftValue);
          var handler =   $('.handler-value-wrap')[0].style.left;

          console.log("leftValue :" + leftValue);
          console.log("handler :" + handler);
          console.log("final :" + finalValue);

          if (handler > leftValue){
            var last = finalValue-4;
          } else {
            var last = finalValue+4.16;
          }
          var currentVal = $( "#amount" ).val();
          $('.handler-value-wrap').css('left', last+'%');
          $( "#amount" ).val(ui.value );

        },
        create: function(event, ui){
          $("#amount").val(1);
        }
      });
      //$( "#amount" ).val($( "#slider-range-min" ).slider( "value" ) + "km" );
     
  });
  </script>

<script type="text/javascript">
  /* $( window ).load(function() {
        $( "#handler" ).slider({
        range: "min",
        value: 1,
        min: 1,
        max: 25,
        slide: function( event, ui ) {
          var leftValue = $('.ui-state-default')[0].style.left;
          var currentVal = $( "#amount" ).val();
          $('.handler-value-wrap').css('left', leftValue);
          $( "#amount" ).val(ui.value + "km");

        }
      });
      $( "#amount" ).val($( "#slider-range-min" ).slider( "value" ) + "km" );
  });*/
</script>


<script type="text/javascript">
  $('.single-slider').jRange({
    from: 1.0,
    to: 25.0,
    step: 1.0,
    //scale: [1.0,],
    format: '%s Km',
    width: 260,
    showLabels: true,
    theme: 'theme-blue',
    snap: true
});
</script>

<script>
        $(function () {
            new google.maps.places.Autocomplete(
                    document.getElementById('mysuburb'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>

    <script>
        $(function () {
            new google.maps.places.Autocomplete(
                    document.getElementById('suburbsall'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>


 <script>
        $(function () {
            new google.maps.places.Autocomplete(
                    document.getElementById('editsuburb'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>


    <script>
        $(function () {
            new google.maps.places.Autocomplete(
                    document.getElementById('searchclub'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>

     <script>
        $(function () {
            new google.maps.places.Autocomplete(
                    document.getElementById('sidesearch'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>


    <script>
        $(function () {
            new google.maps.places.Autocomplete(
                    document.getElementById('mynewsuburb'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });

    </script>








</body>
</html>
