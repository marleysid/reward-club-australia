<footer>
	<div class="container">
		<p class="pull-left">&copy;&nbsp;Fitness Calendar <?php echo date('Y'); ?>. All rights reserved.</p>
		<ul class="pull-right">
			<?php 
			$loc_arrays = array('1','0','4','2','3');
			foreach($loc_arrays as $loc_array){
				if(!empty($home_links[$loc_array])){
					echo '<li><a href="'.site_url().$home_links[$loc_array]->article_slug.'.html">'.$home_links[$loc_array]->article_title.'</a></li>';
				}
			}
			?>
        </ul>
	</div>
</footer>
<?php 
$route_module = $this->router->fetch_class();
if(in_array(strtolower($route_module), array( 'search',  'users', 'classes'))):
$this->load->view('enquiry/enquiry_form');
endif;?>
<div id="grab-offer-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
<h3 id="offer-title"></h3>
</div>
<div class="modal-body">
	<p id="offer-business-name"></p>
	<p id="offer-desc"></p>
<form name="offer_form" id="offer_form" onsubmit="return false;" action="offers/grab_offer" autocomplete="off" class="form-horizontal">
	<p>Please provide your details and we'll be in touch as soon as possible to arrange your offer redemption.</p>
      <div class="control-group"><label class="control-label">Name *</label><div class="span3"><input type="text" class="text required" name="offer_contact_name" id="offer_contact_name" /></div></div>
      <div class="control-group"><label class="control-label">Email Address *</label><div class="span3"><input type="text" class="text required" name="offer_contact_email" id="offer_contact_email" /></div></div>
      <div class="control-group"><label class="control-label">Phone Number*</label><div class="span3"><input type="text" class="text required" name="offer_contact_no" id="offer_contact_no" /></div></div>
      <input type="hidden" name="offer" id="offer" value="" />
      <input type="hidden" name="offer_for" id="offer_for" value="" />
      <input type="hidden" name="offer_by" id="offer_by" value="" />
</div>
<div class="modal-footer">
    <input type="submit" name="submit" class="btn btn-small" id="send_offer" value="Send" />
    <input type="button" name="cancel" class="btn btn-small" data-dismiss="modal" aria-hidden="true" value="Cancel" />
</div>
</form>
</div>