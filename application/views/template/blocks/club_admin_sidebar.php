<div id="left">
    <div class="media user-media">		
        <div class="user-media-toggleHover">
            <span class="fa fa-user"></span> 
        </div>
        <div class="user-wrapper">
            <a class="user-link" href="">
                <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?php echo base_url(); ?>assets/img/user.gif">
                <!--<span class="label label-danger user-label">16</span>--> 
            </a> 
            <div class="media-body">
                <h5 class="media-heading"><?php echo $current_user->first_name; ?></h5>
                <ul class="list-unstyled user-info">
                    <li> <a href="">Administrator</a>  </li>
                    <li>Last Access :
                        <br>
                        <small>
                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('j M H:i', $current_user->last_login); ?></small> 
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #menu -->
    <ul id="menu" class="" style="border-right: 1px solid rgba(0, 0, 0, 0.3);">
        <li class="nav-header">Menu</li>
        <li class="nav-divider"></li>
        <li class="<?php echo is_active_module('dashboard'); ?>"><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> <span class="link-title">Dashboard</span></a></li>
        <li class="<?php echo is_active_module('offers'); ?>">
            <a href="<?php echo base_url('club_admin/offers'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Offers</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('club_admin/offers'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Offers
                    </a> 
                </li>
                <li class="">
                    <a href="<?php echo base_url('club_admin/offers/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add Offers
                    </a> 
                </li>
            </ul>
        </li>




        <li class="<?php echo is_active_module('entertainment'); ?>">
            <a href="<?php echo base_url('club_admin/entertainment'); ?>"><i class="fa fa-group "></i> <span class="link-title">Entertainment</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('club_admin/entertainment'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Entertainment
                    </a> 
                </li>
                <li class="">
                    <a href="<?php echo base_url('club_admin/entertainment/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add Entertainment
                    </a> 
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('facilities'); ?>">
            <a href="<?php echo base_url('club_admin/facilities'); ?>"><i class="fa fa-group "></i> <span class="link-title">Facilities</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('club_admin/facilities'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Facilities
                    </a> 
                </li>
                <li class="">
                    <a href="<?php echo base_url('club_admin/facilities/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add Facilities
                    </a> 
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('cms'); ?>">
            <a href="<?php echo base_url('club_admin/cms'); ?>"><i class="fa fa-list-ul"></i> <span class="link-title">CMS</span><span class="fa arrow"></span></a>
        <li class="<?php echo is_active_module('contacts'); ?>">
            <a href="<?php echo base_url('club_admin/contacts'); ?>"><i class="fa fa-list-ul"></i> <span class="link-title">Contacts</span><span class="fa arrow"></span></a>

        </li>

        <li class="nav-divider"></li>
    </ul><!-- /#menu -->
</div><!-- /#left -->