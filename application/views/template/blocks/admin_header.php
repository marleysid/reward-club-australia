<div id="top">
    <!-- .navbar -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <!-- Brand and toggle get grouped for better mobile display -->
        <header class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
                <span class="icon-bar"></span> 
            </button>
            <a href="<?php echo base_url('admin'); ?>" class="navbar-brand">
                                <!--<img src="<?php // echo base_url();  ?>assets/img/logo.png" alt="">-->
            </a> 
        </header>
        <div class="topnav">
            <div class="btn-toolbar">
                <div class="btn-group">
                    <a data-placement="bottom" data-original-title="Fullscreen" data-toggle="tooltip" class="btn btn-default btn-sm" id="toggleFullScreen">
                        <i class="glyphicon glyphicon-fullscreen"></i>
                    </a> 
                </div>
                <div class="btn-group">
                    <a data-placement="bottom" data-original-title="Show / Hide Sidebar" data-toggle="tooltip" class="btn btn-success btn-sm" id="changeSidebarPos">
                        <i class="fa fa-expand"></i>
                    </a> 
                </div>
                <div class="btn-group">
                    <a class="btn btn-default btn-sm" data-toggle="tooltip" href="<?php echo base_url('admin/auth/change_password'); ?>" data-original-title="Change Password" data-placement="bottom">
                        <i class="fa fa-key"></i>
                    </a> 
                    <!--                <a href="#helpModal" class="btn btn-default btn-sm" data-placement="bottom" data-original-title="Help" data-toggle="modal">
                                      <i class="fa fa-question"></i>
                                    </a> -->
                </div>
                <div class="btn-group">
                    <a href="<?php echo base_url('admin/logout'); ?>" data-toggle="tooltip" data-original-title="Logout" data-placement="bottom" class="btn btn-metis-1 btn-sm">
                        <i class="fa fa-power-off"></i>
                    </a> 
                </div>
            </div>
        </div><!-- /.topnav -->
        <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
            <!-- .nav -->
            <ul class="nav navbar-nav">
<!--				<li> <a href="<?php echo base_url('admin/pages/about'); ?>">About Us </a>  </li>-->
            </ul><!-- /.nav -->
        </div>
    </nav><!-- /.navbar -->
    <!-- header.head -->
    <header class="head">
        <!--		<div class="search-bar">
                    <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                                        <i class="fa fa-expand"></i>
                    </a> 
                    <form class="main-search">
                                        <div class="input-group">
                                                <input type="text" class="input-small form-control" placeholder="Live Search ...">
                                                <span class="input-group-btn">
                                                        <button class="btn btn-primary btn-sm text-muted" type="button"><i class="fa fa-search"></i></button>
                                                </span> 
                                        </div>
                    </form>
                        </div>-->
        <!-- ."main-bar -->
        <div class="main-bar">
            <h3>
                <i class="fa fa-home"></i>Rewards Club</h3>
        </div><!-- /.main-bar -->
    </header>
    <!-- end header.head -->
</div><!-- /#top -->