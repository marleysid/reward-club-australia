<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="description" content="<?php echo $site_description; ?>" />
        <meta name="keywords" content="<?php echo $site_keywords; ?>" />
        <title><?php echo $site_title; ?></title>
        <?php echo $meta_tag; ?>
        <?php echo $styles; ?>
        <!-- JS -->

        <!-- Google fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800|Old+Standard+TT' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800' rel='stylesheet' type='text/css'>

        <!-- font awesome -->
        <!--<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">-->


        <!-- bootstrap -->
        <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/bootstrap/css/bootstrap.min.css'); ?>" />
        <!-- animate.css -->
        <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/wow/animate.css'); ?>" />
        <!-- gallery -->
        <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/gallery/blueimp-gallery.min.css'); ?>">
        <!-- selectbox -->
        <link href="<?php echo get_asset('assets/frontend/assets/selectbox/customSelectBox.css'); ?>" type="text/css" rel="stylesheet" />
        <link href="<?php echo get_asset('assets/frontend/assets/selectbox/jquery.jscrollpane.css'); ?>" type="text/css" rel="stylesheet" />

        <!-- Owl Carousel Assets -->
        <link href="<?php echo get_asset('assets/frontend/assets/owl-carousel/owl.carousel.css'); ?>" rel="stylesheet">
        <!-- <link href="<?php echo get_asset('assets/frontend/assets/owl-carousel/owl.theme.css'); ?>" rel="stylesheet">-->
        <!-- favicon -->
        <link rel="shortcut icon" href="images/favicon.png?1" type="image/x-icon">
        <link rel="icon" href="images/favicon.png?1" type="image/x-icon">
        <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/style.css'); ?>">        
        <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/menu.css'); ?>">
        <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/component.css'); ?>">
        <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/tinyscrollbar.css'); ?>">
        <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
        <script type="text/javascript">
            var base_url = "<?php echo site_url(); ?>";
        </script>
        <script src="<?php echo get_asset('assets/frontend/assets/jquery.js'); ?>"></script>
          
      
        <?php echo $scripts_header; ?>
            <!-- wow script -->
            <script src="<?php echo get_asset('assets/frontend/assets/wow/wow.min.js'); ?>"></script>
            <!-- owlcarousel -->
            <script src="<?php echo get_asset('assets/frontend/assets/owl-carousel/owl.carousel.min.js'); ?>"></script>
            <!-- bootstrap -->
            <script src="<?php echo get_asset('assets/frontend/assets/bootstrap/js/bootstrap.js'); ?>" type="text/javascript" ></script>
            <!-- jquery mobile -->
            <script src="<?php echo get_asset('assets/frontend/assets/mobile/touchSwipe.min.js'); ?>"></script>
            <script src="<?php echo get_asset('assets/frontend/assets/respond/respond.js'); ?>"></script>
            <!-- gallery -->
            <script src="<?php echo get_asset('assets/frontend/assets/gallery/jquery.blueimp-gallery.min.js'); ?>"></script>
            <!-- selectbox -->
            <script type="text/javascript" src="<?php echo get_asset('assets/frontend/assets/selectbox/SelectBox.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo get_asset('assets/frontend/assets/selectbox/jScrollPane.js'); ?>"></script>
            <script type="text/javascript" src="<?php echo get_asset('assets/frontend/assets/selectbox/jquery.mousewheel.js'); ?>"></script>
            <!-- custom script -->
            <script src="<?php echo get_asset('assets/frontend/assets/script.js'); ?>"></script>
            <script src="<?php echo get_asset('assets/frontend/assets/bootstrap-slider.js'); ?>"></script>
            <script src="<?php echo get_asset('assets/js/jquery.print.js');?>"></script>
            <script>    
                $(function() {
                    
                    $("select.custom").each(function() {                    
                        var sb = new SelectBox({
                            selectbox: $(this),
                            height: 150,
                            width: 200
                        });
                    });
                    
                });
            </script>
    </head>

    <body id="home">
        <!-- header -->
        <div class="header clearfix">
            <div class="container">
                <a class="logo pull-left" href="<?php echo site_url(); ?>"><img src="<?php echo get_asset('assets/frontend/images/logo.png'); ?>"  alt="holiday crown"></a>
                <div class="pull-right text-right">
                    <div class="links">
                        <span class="social"><a href="<?php echo $facebook_url->value; ?>" target="_blank"><img src="<?php echo get_asset('assets/frontend/images/facebook.png'); ?>"></a> <a href="<?php echo $twitter_url->value; ?>" target="_blank"><img src="<?php echo get_asset('assets/frontend/images/twitter.png'); ?>"></a> <a href="<?php echo $youtube_url->value; ?>" target="_blank"><img src="<?php echo get_asset('assets/frontend/images/mail.png'); ?>"></a></span>
                          <?php if (isset($userLoggedIn) and $userLoggedIn): ?>
                          <a class="favourites" href="<?php echo site_url('deals/favourites'); ?>">My Favourites</a> 
                            <a class="cart" data-toggle="modal" href="<?php echo site_url('deals/cart_detail'); ?>">My Cart</a> 
                            
                            
                        <?php else: ?>
                            <a class="cart" href="" data-toggle="modal" data-target="#loginModal" data-url="<?php echo site_url('deals/cart_detail'); ?>" data-whatever="@getbootstrap">My Cart</a> 
                            <a class="favourites" data-toggle="modal" data-target="#loginModal" data-url="<?php echo site_url('deals/favourites'); ?>" data-whatever="@getbootstrap">My Favourites</a>
                        <?php endif; ?>
                        <?php
                        if (isset($userLoggedIn) and $userLoggedIn):
                            echo "Hello, {$userDetail->member_first_name}";
                            ?>

                            <a href="<?php echo site_url('user/logout'); ?>">Logout</a>
                        <?php else:
                            ?>
                            <span class="login-register">
                                <a class="login" href="<?php //echo site_url('user/login');        ?>" data-toggle="modal" data-target="#loginModal" data-whatever="@getbootstrap"  >LOGIN</a> / <a class="register" href="<?php //echo site_url('user/register');        ?>" data-toggle="modal" data-target="#registerModal" data-whatever="@getbootstrap" >REGISTER</a>
                            </span>
                        <?php endif; ?>
                    </div>
                    <div class="search">
                        <form method="GET" action="<?php echo site_url('offers'); ?>">
                            <div class="region">
                                <!-- <select class="form-control" name="region">
                                    <option value="">Select a Region</option>
                                    <?php
                                    if ($regions):
                                        foreach ($regions as $value):
                                            echo '<option value="' . $value->region_id . '" '.(($this->input->get('region') and $this->input->get('region') == $value->region_id) ? ' selected="selected" ' : ' ' ).' >' . $value->region_name . '</option>';
                                        endforeach;
                                    endif;
                                    ?>
                                </select> -->
                            </div>
                            <div class="category">
                                <!-- <select class="form-control" name="category[]">
                                    <option value="">Select a Category</option>
                                    <?php
                                    if ($categories):
                                        foreach ($categories as $value):
                                            echo '<option value="' . $value->category_id . '" '.(($this->input->get('category') and in_array($value->category_id,(array)$this->input->get('category')) ) ? ' selected="selected" ' : ' ' ).'>' . $value->category_name . '</option>';
                                        endforeach;
                                    endif;
                                    ?>
                                </select> -->
                            </div>          
                            <div class="button-search">
<!--                                <button type="button" class="btn btn-danger">Search Now</button>-->
                                 <input type="submit"  class="btn btn-danger" value="search now" />       
                                 <button class="btn btn-default" id="search-reset" type="button" onclick="window.location.href='<?php echo site_url(); ?>'">Advanced Search</button>
                                 <!--<button type="button" class="btn btn-default" onclick="window.location.href='<?php //echo site_url(); ?>'"></button>-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- header -->


        <!-- nav -->
        <nav class="navbar  navbar-inverse" role="navigation">
            <div class="container">
            	<div class="row">
            	
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">

                    <ul class="nav navbar-nav">        
                        <li class="<?php if ($this->uri->segment(1) == '') echo 'active'; ?>"><a href="<?php echo site_url(); ?>">Home </a></li>
                        <li class="<?php if ($this->uri->segment(1) == 'information-for-members') echo 'active'; ?>"><a href="<?php echo site_url('information-for-members'); ?>">Information for Members</a></li>        
                        <li class="<?php if ($this->uri->segment(1) == 'information-for-clubs') echo 'active'; ?>"><a href="<?php echo site_url('information-for-clubs'); ?>">Information for Clubs</a></li>
                        <li class="<?php if ($this->uri->segment(1) == 'information-for-suppliers') echo 'active'; ?>"><a href="<?php echo site_url('information-for-suppliers'); ?>">Information for Suppliers</a></li>
                        <li class="<?php if ($this->uri->segment(1) == 'contacts') echo 'active'; ?>"><a href="<?php echo site_url('contacts'); ?>">Contact Rewards Club</a></li>
                    </ul>
                </div><!-- Wnavbar-collapse -->
                </div>
            </div><!-- container-fluid -->
        </nav>
        <!-- nav -->

        <div class="modal fade" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="post" id="registerform" class="register" autocomplete="off">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeform"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Register</h4>
                    </div>
                    <div class="modal-body">
                        <div id="error_msg">                            
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label pp_frm_label">First Name*</label>
                            <div class="form-input-holder">
                            	<input type="text" name="first_name" class="form-control pp_frm_inpt" id="first_name" value="<?php echo set_value('first_name'); ?>">
                            </div>                                
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label pp_frm_label">Surname*</label>
                            <div class="form-input-holder">
                                <input type="text" name="last_name" class="form-control pp_frm_inpt" id="last_name" value="<?php echo set_value('last_name'); ?>">
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label pp_frm_label">Email*</label>
                            <div class="form-input-holder">
                                <input type="text" name="email" class="form-control pp_frm_inpt" id="email" value="<?php echo set_value('email'); ?>">
                        </div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label pp_frm_label">Password*</label>
                            <div class="form-input-holder">
                                <input type="password" name="password" class="form-control pp_frm_inpt" id="password" value="<?php echo set_value('password'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label pp_frm_label">Confirm Password*</label>
                            <div class="form-input-holder">
                                <input type="password" name="confirm_password" class="form-control pp_frm_inpt" id="confirm_password"  value="<?php echo set_value('password'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label pp_frm_label">Postcode*</label>
                            <div class="form-input-holder">
                                <input type="text" name="postcode" class="form-control pp_frm_inpt" id="postcode" value="<?php echo set_value('postcode'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label pp_frm_label">Address</label>
                            <div class="form-input-holder">
                                <input type="text" name="address" class="form-control pp_frm_inpt" id="address" value="<?php echo set_value('address'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label pp_frm_label">Contact Number</label>
                            <div class="form-input-holder">
                                <input type="text" name="mobile" class="form-control pp_frm_inpt" id="mobile" value="<?php echo set_value('mobile'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label or="sel1" class="control-label pp_frm_label"> <img src="<?php echo get_asset('assets/frontend/images/agree_icon.png'); ?>"> Membership Type*</label>
                            <div class="form-input-holder">
                            <select class="form-control pp_frm_inpt" id="sel1" name="membership_type" id="membership_type">
                                <option value="" >---Membership Type---</option>
                                <?php
                                if (is_array($membership_types) and count($membership_types) > 0):
                                    foreach ($membership_types as $value):
                                        ?>
                                        <option value="<?php echo $value->membership_type_id; ?>" <?php echo set_select('membership_type', $value->membership_type_id); ?>>
                                            <?php echo $value->membership_title; ?>
                                        </option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
						</div>
                        </div>
                        <div class="form-group ">
                            <label for="message-text" class="control-label check_bx_pp_lbl"> <a href="<?php echo site_url('terms-and-conditions'); ?>" target="_blank"></a><img src="<?php echo get_asset('assets/frontend/images/agree_icon.png'); ?>"> Agree to Terms</label>
                            <input class="check_bx_pp" type="checkbox" name="terms_conditions" id="terms_conditions"/>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-default lft_pp_btn" id="registerreset" value="reset" >
                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="register_button" value="Register">
                    </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                     <form method="post" id="loginform">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="closelogin"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Login</h4>
                    </div>
                    <div class="modal-body">
                            <div id="login_error_msg">
                            
                            </div>
                           <div class="form-group">
                                <label for="recipient-name" class="control-label pp_frm_label">Email*</label>
                                <div class="form-input-holder">
                                <input type="text" name="email" class="form-control pp_frm_inpt" id="email" value="<?php echo set_value('email'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="recipient-name" class="control-label pp_frm_label">Password*</label>
                                <div class="form-input-holder"><input type="password" name="password" class="form-control pp_frm_inpt" id="password" value="<?php echo set_value('password'); ?>"></div>
                            </div>
                        <input type="hidden" value="" id="redirecturl">
                           
                       
                    </div>
                    <div class="modal-footer">
                        <input type="reset" class="btn btn-default lft_pp_btn" id="loginreset" value="reset" >
                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="login_button" value="login">
                    </div>
                          </form>
                </div>
            </div>
        </div>

        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld_01">
      <ul>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
      </ul>
    </div>

        <div class="modal fade" id="successModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="closesuccessmsg"><span aria-hidden="true">&times;</span></button>
                       
                         <h4 class="modal-title" id="exampleModalLabel">Success! An activation link has been sent to your email</h4>
                    </div>
                    <div class="modal-body" id="showmsg">
                        
                    </div>
                </div> 
            </div>
        </div>

        
       