<?php
$profile_url = FALSE;
$calendar_links = FALSE;
$account_setting_links = FALSE;

if ($this->ion_auth->is_admin()) {
    $profile_url = base_url() . 'admin';
    $account_setting_links = base_url() . 'admin';
} else if ($this->ion_auth->is_general_user()) {
    $profile_url = $dashboard_links = base_url() . 'trainee';
    $account_setting_links = base_url() . 'trainee/account_settings';
    $calendar_links = base_url() . 'trainee/classes';
} else if ($this->ion_auth->is_personal_trainer()) {
    $profile_url = base_url() . 'personal_trainer';
    $account_setting_links = base_url() . 'personal_trainer/account_settings';
} else if ($this->ion_auth->is_fitness_operator()) {
    $profile_url = base_url() . 'fitness_operator';
    $account_setting_links = base_url() . 'fitness_operator/account_settings';
} else if ($this->ion_auth->is_instructor()) {
    $profile_url = $dashboard_links = base_url() . 'instructor/instructor';
    $calendar_links = base_url() . 'trainee/account_settings';
    $account_setting_links = base_url() . 'instructor/account_settings';
}
?>
<header><div class="container">
        <div class="navbar">		

            <!-- Be sure to leave the brand out there if you want it shown -->
            <a class="brand" href="<?php echo base_url(); ?>"><span><?php echo Settings::get('site_title') ?></span><img src="<?php echo site_url() . 'assets/img/logo.png'; ?>" /></a>
            <ul class="nav pull-right">
                <?php
// if(!$this->ion_auth->logged_in()):
//						echo '<li><a href="'.site_url('login').'">Trainer Site</a></li>';
//               		  endif;  
                ?>
                <!-- 
<li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0)">Logged in as <?php //echo $current_user->user_firstname;  ?>
                <li class="dropdown"><a href="<?php echo base_url() . 'trainee'; ?>">Logged in as </a></li>
                -->					
                <li class="dropdown">
                    <?php if ($this->ion_auth->logged_in()): ?>
                        <span>
                            <a href="<?php echo $profile_url; ?>"><img id="header-profile-pic" src="<?php echo imager(config_item('profile_image_path') . ((!empty($current_user->user_img) && file_exists(config_item('profile_image_path') . $current_user->user_img)) ? $current_user->user_img : 'default_profile.png'), 32, 32) ?>" class="img-circle" />
                                <?php echo ($current_user->user_firstname) ? $current_user->user_firstname : ''; ?></a>
                        </span>
                        <a data-toggle="dropdown" class="dropdown-toggle">							
                            <img src="<?php echo site_url('assets/img/linelink.png'); ?>"/>
                        </a>                    
                        <ul class="dropdown-menu"> 
                            <?php if ($profile_url): ?>                   
                                <li><a href="<?php echo $profile_url; ?>"><img src="<?php echo site_url('assets/img/profile.png'); ?>" />profile</a></li>
                            <?php endif; ?>
                            <?php if ($calendar_links): ?>
                                <li><a href="<?php echo $calendar_links; ?>"><img src="<?php echo site_url('assets/img/calender.png'); ?>" />Calendar</a></li>
                            <?php endif; ?>
                            <?php if ($account_setting_links): ?>
                                <li><a href="<?php echo $account_setting_links; ?>"><img src="<?php echo site_url('assets/img/settings.png'); ?>" />Settings</a></li>
                            <?php endif; ?>
                            <li><a href="<?php echo base_url('logout') ?>"><img src="<?php echo site_url('assets/img/signout.png'); ?>" />Sign Out</a></li>
                        </ul>
                    </li>
                <?php else: ?>
                    <li><a href="<?php echo base_url('login'); ?>">Login</a></li>
                    <li><a href="<?php echo base_url('register'); ?>">Register</a></li>
                    <!-- <li class="dropdown"><a data-toggle="dropdown" class="dropdown-toggle" href="javascript:void(0)">Register 
              <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                            <li><a href="<?php // echo base_url('register');   ?>">Register as trainee</a></li>
                                            <li class="divider"></li>
                                            <li><a href="<?php // echo base_url('trainer_register');   ?>">Register as trainer</a></li>
                                    </ul></li>-->
                <?php endif; ?>
            </ul>
        </div>
    </div>
</header>