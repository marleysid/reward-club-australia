<div id="left">
    <div class="media user-media">
        <div class="user-media-toggleHover">
            <span class="fa fa-user"></span>
        </div>
        <div class="user-wrapper">
            <a class="user-link" href="">
                <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?php echo base_url(); ?>assets/img/user.gif">
                <!--<span class="label label-danger user-label">16</span>-->
            </a>
            <div class="media-body">
                <h5 class="media-heading"><?php echo $current_user->first_name; ?></h5>
                <ul class="list-unstyled user-info">
                    <li> <a href="">Administrator</a>  </li>
                    <li>Last Access :
                        <br>
                        <small>
                            <i class="fa fa-calendar"></i>&nbsp;<?php echo date('j M H:i', $current_user->last_login); ?></small>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #menu -->
    <ul id="menu" class="" style="border-right: 1px solid rgba(0, 0, 0, 0.3);">
        <li class="nav-header">Menu</li>
        <li class="nav-divider"></li>
        <li class="<?php echo is_active_module('dashboard'); ?>"><a href="<?php echo base_url('admin'); ?>"><i class="fa fa-dashboard"></i> <span class="link-title">Dashboard</span></a></li>
        <!--<li class="<?php // echo is_active_module('region');    ?>"><a href="<?php // echo base_url('admin/region');     ?>"><i class="fa fa-globe"></i> <span class="link-title">Manage Regions</span></a></li>-->
        <li class="<?php echo is_active_module('categories'); ?>"><a href="<?php echo base_url('admin/categories'); ?>"><i class="fa fa-tags "></i> <span class="link-title">Manage Categories</span></a></li>
        <li class="<?php echo is_active_module('offers'); ?>">
            <a href="<?php echo base_url('admin/offers'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Offers</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/offers'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Offers
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/offers/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add Offers
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/offers/import_csv_data'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Import csv data
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/offers/edit_csv'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Edit csv data
                    </a>
                </li>
                 <li class="">
                    <a href="<?php echo base_url('admin/reviews/offers'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;View Reviews
                    </a>
                </li>
            </ul>
        </li>
        <!-- <li class="<?php echo is_active_module('deals'); ?>">
            <a href="<?php echo base_url('admin/deals'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Deals</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/deals'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Deals
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/deals/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add Deals
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/deals/import_csv_data'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Import csv data
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/reviews/deals'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;View Reviews
                    </a>
                </li>
            </ul>
        </li> -->
        <li class="<?php echo is_active_module('news'); ?>">
            <a href="<?php echo base_url('admin/news'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">News</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/news'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage News
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/news/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add News
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('promotional_material'); ?>">
            <a href="<?php echo base_url('admin/promotional_material'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Materials</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/promotional_material'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Materials
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/promotional_material/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add Material
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('auth'); ?>">
            <a href="<?php echo base_url('admin/auth/club'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Users</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/auth/club'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Club Users
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/auth/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add User
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('faq'); ?>">
            <a href="<?php echo base_url('admin/faq'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">FAQ's</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/faq'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage FAQ's
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/faq/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add FAQ's
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('suburb'); ?>">
            <a href="<?php echo base_url('admin/suburb'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Suburb</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/suburb'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage suburb
                    </a>
                </li>
                 <li class="">
                    <a href="<?php echo base_url('admin/suburb/import_csv_data'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Import csv data
                    </a>
                </li>


                <li class="">
                    <a href="<?php echo base_url('admin/suburb/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add suburb
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('community'); ?>">
            <a href="<?php echo base_url('admin/community'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">community</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/community'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Community
                    </a>
                </li>

            </ul>
        </li>
        <li class="<?php echo is_active_module('admin/suppliers_icons'); ?>">
            <a href="<?php echo base_url('admin/suppliers_icons'); ?>"><i class="fa fa-group "></i> <span class="link-title">Suppliers Icon</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/suppliers_icons'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage icons
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/suppliers_icons/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add icons
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('members'); ?>">
            <a href="<?php echo base_url('admin/members'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Members</span><span class="fa arrow"></span></a>

        </li>
        <li class="<?php echo is_active_module('materials_enquired'); ?>">
            <a href="<?php echo base_url('admin/materials_enquired'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Materials Enquired</span><span class="fa arrow"></span></a>

        </li>

        <li class="<?php echo is_active_module('cms'); ?>">
            <a href="<?php echo base_url('admin/cms'); ?>"><i class="fa fa-list-ul"></i> <span class="link-title">CMS</span><span class="fa arrow"></span></a>
            <ul>
<!--                <li class="">-->
<!--                    <a href="--><?php //echo base_url('admin/cms/information_for_members'); ?><!--">-->
<!--                        <i class="fa fa-angle-right"></i>&nbsp;Information for members-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li class="">-->
<!--                    <a href="--><?php //echo base_url('admin/cms/information_for_suppliers'); ?><!--">-->
<!--                        <i class="fa fa-angle-right"></i>&nbsp;Information for Suppliers-->
<!--                    </a>-->
<!--                </li>-->
                <li class="">
                    <a href="<?php echo base_url('admin/cms'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage CMS
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/cms/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add CMS
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('subscription'); ?>">
            <a href="<?php echo base_url('admin/subscription'); ?>"><i class="fa fa-list-ul"></i> <span class="link-title">Subscription</span><span class="fa arrow"></span></a>

        </li>
        <li class="<?php echo is_active_module('contacts'); ?>">
            <a href="<?php echo base_url('admin/contacts'); ?>"><i class="fa fa-list-ul"></i> <span class="link-title">Contacts</span><span class="fa arrow"></span></a>

        <li class="<?php echo is_active_module('orders'); ?>">
            <a href="<?php echo base_url('admin/orders'); ?>"><i class="fa fa-list-ul"></i> <span class="link-title">Orders</span><span class="fa arrow"></span></a>


        <li class="<?php echo is_active_module('reviews'); ?>">
            <a href="<?php echo base_url('admin/reviews'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Reviews</span></a>

        </li>
        <li class="<?php echo is_active_module('region'); ?>">
            <a href="<?php echo base_url('admin/region'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Regions</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/region'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Region
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/region/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add Region
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('partners'); ?>">
            <a href="<?php echo base_url('admin/partners'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Partners</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/partners'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Partners
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo base_url('admin/partners/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add Partners
                    </a>
                </li>
            </ul>
        </li>
<!--        <li class="<?php // echo is_active_module('rewards');    ?>">
                <a href="<?php // echo base_url('admin/rewards');     ?>"><i class="fa fa-gift"></i> <span class="link-title">Rewards</span><span class="fa arrow"></span></a>
                <ul>
      <li class="">
        <a href="<?php // echo base_url('admin/rewards');     ?>">
          <i class="fa fa-angle-right"></i>&nbsp;Manage Rewards
        </a>
      </li>
      <li class="">
        <a href="<?php // echo base_url('admin/rewards/add');     ?>">
          <i class="fa fa-angle-right"></i>&nbsp;Add Reward
        </a>
      </li>
    </ul>
        </li>-->
        <li class="<?php echo is_active_module('logo'); ?>">
            <a href="<?php echo base_url('admin/logo'); ?>"><i class="fa fa-sitemap"></i> <span class="link-title">Logo</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/logo'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Logo
                    </a>
                </li>

            </ul>
        </li>
<!--        <li class="--><?php //echo is_active_module('notification'); ?><!--">-->
<!--            <a href="--><?php //echo base_url('admin/notification'); ?><!--"><i class="fa fa-sitemap"></i> <span class="link-title">Notification</span><span class="fa arrow"></span></a>-->
<!--            <ul>-->
<!--                <li class="">-->
<!--                    <a href="--><?php //echo base_url('admin/notification'); ?><!--">-->
<!--                        <i class="fa fa-angle-right"></i>&nbsp;Notification-->
<!--                    </a>-->
<!--                </li>-->
<!---->
<!--            </ul>-->
<!--        </li>-->
        <li class="<?php echo is_active_module('clubs'); ?>">
            <a href="<?php echo base_url('admin/clubs'); ?>"><i class="fa fa-group "></i> <span class="link-title">Clubs</span><span class="fa arrow"></span></a>
            <ul>
                <li class="">
                    <a href="<?php echo base_url('admin/clubs'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Manage Clubs
                    </a>
                </li>

                 <li class="">
                    <a href="<?php echo base_url('admin/clubs/import_clubcsv_data'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Import csv data
                    </a>
                </li>


                <li class="">
                    <a href="<?php echo base_url('admin/clubs/add'); ?>">
                        <i class="fa fa-angle-right"></i>&nbsp;Add Club
                    </a>
                </li>
            </ul>
        </li>
        <li class="<?php echo is_active_module('feedback'); ?>">
            <a href="<?php echo base_url('admin/feedback'); ?>"><i class="fa fa-group "></i> <span class="link-title">Feedbacks</span><span class="fa arrow"></span></a>
        </li>
        <li class="<?php echo is_active_module('settings'); ?>">
            <a href="<?php echo base_url('admin/settings'); ?>"><i class="fa fa-group "></i> <span class="link-title">Settings</span><span class="fa arrow"></span></a>
        </li>

        <li class="<?php echo is_active_module('notification_offer'); ?>">
            <a href="<?php echo base_url('admin/notification_offer'); ?>"><i class="fa fa-group "></i> <span class="link-title">Notification</span><span class="fa arrow"></span></a>
        </li>

<!--        <li class="<?php // echo is_active_module('products');    ?>">
                <a href="<?php // echo base_url('admin/products');     ?>"><i class="fa fa-shopping-cart "></i> <span class="link-title">Products</span><span class="fa arrow"></span></a>
                <ul>
      <li class="">
        <a href="<?php // echo base_url('admin/products');     ?>">
          <i class="fa fa-angle-right"></i>&nbsp;Manage Products
        </a>
      </li>
      <li class="">
        <a href="<?php // echo base_url('admin/products/add');     ?>">
          <i class="fa fa-angle-right"></i>&nbsp;Add Product
        </a>
      </li>
      <li class="">
        <a href="<?php // echo base_url('admin/products/orders');     ?>">
          <i class="fa fa-angle-right"></i>&nbsp;Online Orders
        </a>
      </li>
    </ul>
        </li>-->
        <li class="<?php echo is_active_module('advertisement'); ?>"><a href="<?php echo base_url('admin/advertisement'); ?>"><i class="fa fa-ticket "></i><span class="link-title"> Manage Advertisement</span></a></li>
        <li class="nav-divider"></li>
    </ul><!-- /#menu -->
</div><!-- /#left -->