 <!DOCTYPE html>
 <html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
   <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
   <meta name="description" content="<?php echo $site_description; ?>" />
   <meta name="keywords" content="<?php echo $site_keywords; ?>" />
   <title><?php echo $site_title; ?></title>
   <?php echo $meta_tag; ?>
   <?php echo $styles; ?>
   <!-- JS -->
   <link rel="icon" href="<?php echo get_asset('assets/frontend/images/favicon.ico'); ?>" type="image/x-icon" />
   <!-- Google fonts -->
   <link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800|Old+Standard+TT' rel='stylesheet' type='text/css'>
   <link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800' rel='stylesheet' type='text/css'>
   <!-- font awesome -->
   <!--<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">-->
   <!-- bootstrap -->
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/bootstrap/css/bootstrap.min.css'); ?>" />
   <!-- animate.css -->
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/wow/animate.css'); ?>" />
   <!-- gallery -->
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/gallery/blueimp-gallery.min.css'); ?>">
   <!-- selectbox -->
   <link href="<?php echo get_asset('assets/frontend/assets/selectbox/jquery.jscrollpane.css'); ?>" type="text/css" rel="stylesheet" />
   <link href="<?php echo get_asset('assets/frontend/assets/selectbox/customSelectBox.css'); ?>" type="text/css" rel="stylesheet" />
   <!-- Owl Carousel Assets -->
   <link href="<?php echo get_asset('assets/frontend/assets/owl-carousel/owl.carousel.css'); ?>" rel="stylesheet">
   <!-- <link href="<?php echo get_asset('assets/frontend/assets/owl-carousel/owl.theme.css'); ?>" rel="stylesheet">
         -->
   <link href="<?php echo get_asset('assets/css/jquery-ui.css'); ?>" rel="stylesheet">
   <!-- favicon -->
   <link rel="shortcut icon" href="images/favicon.png?1" type="image/x-icon">
   <link rel="icon" href="images/favicon.png?1" type="image/x-icon">
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/style.css'); ?>">
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/main.css'); ?>">
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/menu.css'); ?>">
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/component.css'); ?>">
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/all.css'); ?>">
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/bootstrap-slider.css'); ?>">
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/tinyscrollbar.css'); ?>">
   <link rel="stylesheet" href="<?php echo get_asset('assets/frontend/assets/jquery.range.css'); ?>">
   <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
   <script type="text/javascript">
      var base_url = "<?php echo site_url(); ?>";
   </script>
   <script src="<?php echo get_asset('assets/frontend/assets/jquery.js'); ?>"></script>
   <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyAdjyK2gpKXvKjmPPDt4iDwDenIh3wAZWk&signed_in=true&libraries=places"></script>
     <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

   <?php echo $scripts_header; ?>
   
</head>
<body id="home">
<script>
   var bodyTag = document.getElementsByTagName("body")[0];
   bodyTag.className = bodyTag.className.replace("noJS", "hasJS");
</script>
<!-- header  -->
<div class="header clearfix">
   <div class="container">
      <div class="row">
         <div class=" col-md-10 col-lg-10 pull-left h-left">
            <div class="rewards-club-logos-wrap">
               <div class="main-rewards-club-logo-here">
                  <a class="logo" href="<?php echo site_url(); ?>"><img src="<?php echo base_url().$logo->logo; ?>"  alt="logo"></a>
               </div>
               <div class="associate-logo display-none">
                  <a class="logo" href="#"><img src="<?php echo get_asset('assets/frontend/images/associate-logo.png'); ?>"  alt="logo"></a>
               </div>
            </div>
            <div class="search-wrapper-header">
               <form method="GET" action="<?php echo site_url('offers'); ?>">
                  <div class="search-wrap">
                     <!-- <input type="search" name="keyword" value="<?php if (!empty($_GET['keyword'])){echo $_GET['keyword'];};?>" placeholder="Search offers" class="main-search-box-holder" id="search" > -->
                     <input type="search" name="keyword" value="<?php if (!empty($_GET['keyword'])){echo $_GET['keyword'];};?>" placeholder="Search Discounts" class="main-search-box-holder" id="search1" >
                     <?php 
                    /* if (isset($_GET['category'])){
                        if ($cat->category_id == $_GET['category']){
                           echo "selected";
                        } else {
                           echo "";
                        }
                     } else {
                        echo "";
                     }*/


                     ?>

                     <select name="category" id="" class="custom select-category">                   

                     <!-- <select name="category" id="" class="select-category"> -->
                   

                        <option value="">Select Category</option>
                        <?php foreach($categories as $cat) { ?>
                           <option value="<?php echo $cat->category_id;?>" <?php if (!empty($_GET['category']) && ($cat->category_id == $_GET['category'])){ echo "selected";} else {echo "";}?>>
                              <?php echo $cat->category_name;?>
                           </option>
                        <?php } ?>
                     </select>
                     <span class="search-icon">search</span>
                  </div>
                  <input type="submit" class="search-icon-button" value="Search"> 

               </form>


            


               <div class="location-wrap">
                  <span>Current location is 
                  <address>
                  <?php
                  
                   $cust = $this->session->userdata('customLocation');
                   if (!empty($cust)){
                     echo $cust . ',' . $this->session->userdata('pcode');

                   } else {
                     $myvar = getCurrentAddress();
                       if (strpos($myvar, 'ā') && strpos($myvar, 'ī')){
                           $raw = array('ā', 'ī');
                           $rep = array('a', 'i');
                           echo str_replace($raw, $rep, $myvar);
                        } else {
                           echo $myvar;
                        }
                   }
                  ?>
                 
                  </address>
                  </span>
                  <span class="change-location"><a href="" data-toggle="modal" data-target="#changelocation" data-whatever="@getbootstrap" >Change my location</a></span>
               </div>
            </div>
         </div>
         <div class=" col-md-2 col-lg-2 pull-right">
            <div class="head-right-acount-detail">
               <?php
               if (isset($userLoggedIn) and $userLoggedIn):
                  ?>
                  <div class="account-detail userloggedin">
                     <strong>Hi  <a class="register" href="" data-toggle="modal" data-target="<?php echo (isset($userDetail) ? '#editModal' : '#registerModal');?>" data-whatever="@getbootstrap" ><?php echo "{$userDetail->first_name}"; ?>  </strong><a href="<?php echo site_url('user/logout'); ?>"><span>Sign out</span></a>
                  </div>
               <?php else: ?>
                  <div class="account-detail">
                     <a class="login" href="" data-toggle="modal" data-target="#loginModal" data-whatever="@getbootstrap"  >
                        <span>Login</span></a>
                     <a class="register" href="" data-toggle="modal" data-target="#registerModal" data-whatever="@getbootstrap" >
                        <span>Register</span></a>
                  </div>
               <?php endif; ?>
               <?php if (isset($userLoggedIn) and $userLoggedIn): ?>
                  <ul class="account-detail-holder">
                     <li><a href="" class="edit_profile" data-toggle="modal" data-target ="#editModal" data-whatever="@getbootstrap">My Account</a></li>
                     <li> <a href="<?php echo site_url('deals/favourites'); ?>">Saved Items</a></li>
                     <li>  <a data-toggle="modal" href="<?php echo site_url('deals/cart_detail'); ?>">My Cart $<?php echo $cart_amount->total ? $cart_amount->total:'0.00'; ?></a></li>
                  </ul>
               <?php else: ?>
                  
               <?php endif;?>
               <div class="mobile-search-icon"><span>search-mob</span></div>
            </div>
         </div>
      </div>
   </div>
   <!-- nav -->
   <div class="container">
      <nav class="navbar  navbar-inverse" role="navigation">
         <!-- Brand and toggle get grouped for better mobile display -->
         <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed">
               <span class="sr-only">Toggle navigation</span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
               <span class="icon-bar"></span>
            </button>
         </div>
         <!-- Collect the nav links, forms, and other content for toggling -->
         <div class="navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav">
               <li class="<?php if ($this->uri->segment(1) == '') echo 'active'; ?>"><a href="<?php echo site_url(); ?>">Home </a></li>
               <li class="<?php if ($this->uri->segment(1) == 'information-for-members') echo 'active'; ?>"><a href="<?php echo site_url('information-for-members'); ?>">Members Information</a></li>
               <li class="<?php if ($this->uri->segment(1) == 'information-for-clubs') echo 'active'; ?>"><a href="<?php echo site_url('information-for-clubs'); ?>">Clubs Information</a></li>
               <li class="<?php if ($this->uri->segment(1) == 'information-for-suppliers') echo 'active'; ?>"><a href="<?php echo site_url('information-for-suppliers'); ?>">Suppliers Information</a></li>
               <li class="<?php if ($this->uri->segment(1) == 'contacts') echo 'active'; ?>"><a href="<?php echo site_url('contacts'); ?>">Contact Us</a></li>
            </ul>
         </div>
         <!-- Wnavbar-collapse -->
      </nav>
   </div>
   <!-- container-fluid -->
   <!-- nav -->
</div>
<!-- header -->

<div class="modal " id="changelocation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form method="post" id="changeloc" action="<?php echo site_url();?>welcome">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeform"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="exampleModalLabel">What’s your location?</h4>
            </div>
            <div class="modal-body">
               <div id="error_msg1" class="error-display">
               </div>

               
               <!-- <form method="GET" action="<?php echo site_url(); ?>"> -->
                  <div class="search-wrap add">
                      <input type="search" name="searchsuburb" placeholder="Enter Suburb or Postcode" class="main-search-box-holder" id="mysuburb" value="<?php if (!empty($this->session->userdata('customLocation'))){echo $this->session->userdata('customLocation');}?>">

                  </div>
                     <!-- <a href="<?php echo base_url()?>welcome/resetLocation">Change to Default</a> -->
                  <div class="distance-wrap">
                     <label for="distance">Include Discounts</label>
                     <select name="km" class="custom distance-select" id="distance">
                        <option value="5" <?php echo (!empty($this->session->userdata('distance')) && $this->session->userdata('distance') == "5") ? 'selected': '' ;?>>Within 5km</option>
                        <option value="10" <?php echo (!empty($this->session->userdata('distance')) && $this->session->userdata('distance') == "10") ? 'selected': '' ;?>>Within 10km</option>
                        <option value="15" <?php echo (empty($this->session->userdata('distance')) || $this->session->userdata('distance') == "15") ? 'selected' : '';?>>Within 15km</option>
                        <option value="20" <?php echo (!empty($this->session->userdata('distance')) && $this->session->userdata('distance') == "20") ? 'selected': '' ;?>>Within 20km</option>
                     </select>
                     <input type="submit" class="search-icon-button" value="Select">
                  </div>
                  <span class="discount-note">Only the discounts available in this location will displayed. Do not worry you can update the location anytime.</span>
                  <div class="login-wrap">

                     <?php if (isset($userLoggedIn) and $userLoggedIn): ?>

                     <span></span>
                        <?php else: ?>
<!--                        <span class="login-btn"><a class="login" href="" data-toggle="modal" data-target="#loginModal" data-whatever="@getbootstrap"  >Or Login</a></span>-->
                        <span class="login-btn"><a class="login" id="openlogin"  >Or Login</a></span>
                     <?php endif; ?>
                  </div>
              

            </div>
          </form>
      </div>
   </div>
</div>





<div class="modal " id="registerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form method="post" id="registerform" class="register" autocomplete="off">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeform"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="exampleModalLabel"><?php echo (isset($userDetail)) ? 'Update Profile' : 'Register';?></h4>
            </div>
            <div class="modal-body">
               <div id="error_msg" class="error-display">
               </div>
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">First Name*</label>
                  <div class="form-input-holder">
                     <input type="text" name="first_name" class="form-control pp_frm_inpt" id="first_name" value="<?php echo ( isset($userDetail) && $userDetail->first_name != "") ? $userDetail->first_name : set_value('first_name');?>">
                  </div>
               </div>



               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Surname*</label>
                  <div class="form-input-holder">
                     <input type="text" name="last_name" class="form-control pp_frm_inpt" id="last_name" value="<?php echo ( isset($userDetail) && $userDetail->last_name != "") ? $userDetail->last_name : set_value('last_name');?>">
                  </div>
               </div>
                  

           
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Email*</label>
                  <div class="form-input-holder">
                     <input type="text" name="email" class="form-control pp_frm_inpt" id="email" value="<?php echo ( isset($userDetail) && $userDetail->email != "") ? $userDetail->email : set_value('email');?>" <?php echo (isset($userDetail)) ? "readonly" : '';?>>
                  </div>
               </div>
           

               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Password*</label>
                  <div class="form-input-holder">
                     <input type="password" name="password" class="form-control pp_frm_inpt" id="password" value="<?php echo set_value('password'); ?>">
                  </div>
               </div>
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Confirm Password*</label>
                  <div class="form-input-holder">
                     <input type="password" name="confirm_password" class="form-control pp_frm_inpt" id="confirm_password"  value="<?php echo set_value('password'); ?>">
                  </div>
               </div>


               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Suburb</label>
                  <div class="form-input-holder">
                     <input type="text" name="suburb" class="form-control pp_frm_inpt" id="mynewsuburb" value="<?php echo ( isset($userDetail) && $userDetail->suburb != "") ? $userDetail->suburb : set_value('suburb');?>">
                  </div>
               </div>


               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Postcode*</label>
                  <div class="form-input-holder">
                     <input type="text" name="postcode" class="form-control pp_frm_inpt" id="postcode" value="<?php echo ( isset($userDetail) && $userDetail->postcode != "") ? $userDetail->postcode : set_value('postcode');?>">
                  </div>
               </div>


               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Address</label>
                  <div class="form-input-holder">
                     <input type="text" name="address" class="form-control pp_frm_inpt" id="address" value="<?php echo ( isset($userDetail) && $userDetail->address != "") ? $userDetail->address : set_value('address');?>">
                  </div>
               </div>
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Contact Number</label>
                  <div class="form-input-holder">
                     <input type="text" name="mobile" class="form-control pp_frm_inpt" id="mobile" value="<?php echo ( isset($userDetail) && $userDetail->contact_no != "") ? $userDetail->contact_no : set_value('contact_no');?>">
                  </div>
               </div>
               <!-- <div class="form-group">
                  <label style="line-height:15px;" or="sel1" class="control-label pp_frm_label"> <img src="<?php echo get_asset('assets/frontend/images/agree_icon.png'); ?>"> Membership Type*</label>
                  <div class="form-input-holder">
                     <select class="form-control pp_frm_inpt" id="sel1" name="membership_type" id="membership_type">
                        <option value="" >---Membership Type---</option>
                        <?php
                        if (is_array($membership_types) and count($membership_types) > 0):
                           foreach ($membership_types as $value):
                              ?>
                              <option value="<?php echo $value->membership_type_id; ?>" <?php echo set_select('membership_type', $value->membership_type_id); ?>>
                                 <?php echo $value->membership_title; ?>
                              </option>
                              <?php
                           endforeach;
                        endif;
                        ?>
                     </select>
                  </div>
               </div> -->
               <div class="form-group ">
                  <input type="checkbox" id="terms_conditions" name="terms_conditions" >
                  <label class="control-label check_bx_pp_lbl" for="terms_conditions"><a href="<?php echo site_url('terms-and-conditions'); ?>" target="_blank" title="Terms and conditions"><img src="<?php echo get_asset('assets/frontend/images/agree_icon.png'); ?>"></a> Agree to Terms &nbsp;&nbsp;<span></span></label>
                  <!--<label for="message-text" class="control-label check_bx_pp_lbl"> <a href="<?php echo site_url('terms-and-conditions'); ?>" target="_blank"></a> </label>
                           <input class="check_bx_pp" type="checkbox" name="terms_conditions" id="terms_conditions"/>-->
               </div>
            </div>
            <div class="modal-footer">
               <input type="reset" class="btn btn-default lft_pp_btn" id="registerreset" value="reset" >
               <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="register_button" value="Register">
            </div>
         </form>
      </div>
   </div>
</div>
<?php if (isset($userLoggedIn) and $userLoggedIn): ?>
   <div class="modal " id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <form method="post" id="editform" class="editprofile" autocomplete="off">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeform"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="exampleModalLabel">Edit Profileeeee</h4>
               </div>
               <div class="modal-body">
                  <div id="edit_error_msg" class="show-error">
                  </div>
                  <div class="form-group">
                     <label for="recipient-name" class="control-label pp_frm_label">First Name*</label>
                     <div class="form-input-holder">
                        <input type="text" name="first_name" class="form-control pp_frm_inpt" id="first_name" value="<?php echo set_value('first_name', (isset($userLoggedIn) and $userLoggedIn and $edit) ? $userDetail->first_name : ''); ?>">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="recipient-name" class="control-label pp_frm_label">Surname*</label>
                     <div class="form-input-holder">
                        <input type="text" name="last_name" class="form-control pp_frm_inpt" id="last_name" value="<?php echo set_value('last_name', (isset($userLoggedIn) and $userLoggedIn and $edit) ? $userDetail->last_name : ''); ?>">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="recipient-name" class="control-label pp_frm_label">Email*</label>
                     <div class="form-input-holder">
                        <input type="text" name="email" class="form-control pp_frm_inpt" id="email" value="<?php echo set_value('email', (isset($userLoggedIn) and $userLoggedIn and $edit) ? $userDetail->email : ''); ?>">
                     </div>
                  </div>

                   <div class="form-group">
                     <label for="recipient-name" class="control-label pp_frm_label">Suburb*</label>
                     <div class="form-input-holder">
                        <input type="text" name="suburb" class="form-control pp_frm_inpt" id="editsuburb" value="<?php echo set_value('suburb', (isset($userLoggedIn) and $userLoggedIn and $edit) ? $userDetail->suburb : ''); ?>">
                     </div>
                  </div>

                  <div class="form-group">
                     <label for="recipient-name" class="control-label pp_frm_label">Postcode*</label>
                     <div class="form-input-holder">
                        <input type="text" name="postcode" class="form-control pp_frm_inpt" id="postcode" value="<?php echo set_value('postcode', (isset($userLoggedIn) and $userLoggedIn and $edit) ? $userDetail->postcode : ''); ?>">
                     </div>
                  </div>


                  <div class="form-group">
                     <label for="recipient-name" class="control-label pp_frm_label">Address</label>
                     <div class="form-input-holder">
                        <input type="text" name="address" class="form-control pp_frm_inpt" id="address" value="<?php echo set_value('address', (isset($userLoggedIn) and $userLoggedIn and $edit) ? $userDetail->address : ''); ?>">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="recipient-name" class="control-label pp_frm_label">Contact Number</label>
                     <div class="form-input-holder">
                        <input type="text" name="mobile" class="form-control pp_frm_inpt" id="mobile" value="<?php echo set_value('mobile', (isset($userLoggedIn) and $userLoggedIn and $edit) ? $userDetail->contact_no : ''); ?>">
                     </div>
                  </div>
                <!--   <div class="form-group">
                     <label or="sel1" class="control-label pp_frm_label"> <img src="<?php echo get_asset('assets/frontend/images/agree_icon.png'); ?>"> Membership Type*</label>
                     <div class="form-input-holder">
                        <select class="form-control pp_frm_inpt" id="sel1" name="membership_type" id="membership_type">
                           <option value="" >---Membership Type---</option>
                           <?php
                           if (is_array($membership_types) and count($membership_types) > 0):
                              foreach ($membership_types as $value):
                                 ?>
                                 <option value="<?php echo $value->membership_type_id; ?>" <?php echo set_select('membership_type', $value->membership_type_id); ?><?php
                                 if ($userDetail->membership_type_id == $value->membership_type_id) {
                                    echo "selected=\"selected\" ";
                                 };
                                 ?>>
                                    <?php echo $value->membership_title; ?>
                                 </option>
                                 <?php
                              endforeach;
                           endif;
                           ?>
                        </select>
                     </div>
                  </div> -->
                  <a href="" id="changepassword" data-toggle="modal" data-target="#emailModal"  data-whatever="@getbootstrap">Change Password?</a>
               </div>
               <div class="modal-footer">
                  <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="savebutton" value="Save">
               </div>
            </form>
         </div>
      </div>
   </div>
<?php endif; ?>
<div class="modal " id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form method="post" id="loginform">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="closelogin"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="exampleModalLabel">Login</h4>
            </div>
            <div class="modal-body">
               <div id="login_error_msg" class="show-error">
               </div>
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Email*</label>
                  <div class="form-input-holder">
                     <input type="text" name="email" class="form-control pp_frm_inpt" id="email" value="<?php echo set_value('email'); ?>">
                  </div>
               </div>
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Password*</label>
                  <div class="form-input-holder"><input type="password" name="password" class="form-control pp_frm_inpt" id="password" value="<?php echo set_value('password'); ?>"></div>
               </div>
               <input type="hidden" value="" id="redirecturl">
            </div>
            <div class="modal-footer">
               <input type="reset" class="btn btn-default lft_pp_btn" id="loginreset" value="reset" >
               <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="login_button" value="login">
            </div>
            <div class="login-footer-register-forgot-password-holder">
               <a href="javascript:void(0);" id="register_modal_link">Register</a>
               &nbsp;&nbsp; &nbsp;&nbsp;
               <a href="javascript:void(0);" id="forget_password">Forgot password?</a>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="social_menu_sld_01">
   <ul>
      <li class="left-rounded box-whte"><a href="<?php echo $facebook_url->value; ?>" target="_blank"><svg version="1.1" id="Vector_Smart_Object_xA0_Image_1_"
    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32px" height="32px"
    viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
<image overflow="visible" width="32" height="32" id="Vector_Smart_Object_xA0_Image" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsSAAALEgHS3X78AAAA
GXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAgpJREFUeNrMVz1LA0EQzS2XLiqi
CH6AHmijGIKNYiGmENuAWOQXaCHYBcwPMEQE6/wBu0Cws1JB1EowaqdcI6QSxEREosQ3OgvrQbi9
Ncll4LHD3e2+d7O7s7NWRNNc1x1AkwKSQBxwgBi/rtEnQBk4AUqO4zzrjGtpEM+iyQJrQFRTbx0o
ArsQcmskAMQ9aPLApo7QJtYACkAGQqraAkA+Q2EEJiOtsQeaPoi49xUA8gU0x0BvpLX2CqxCxFVT
AfznF20gV0UsqpGwPHN+bRB2mudH4I0X6RAw6DMdc3JNCOVF3oD8CBjHYFNAAqAIrvv0IY69PxHg
rXYTcLVfAksg/fRM4zLnAr+okeCyzQ+yBlttXyUHMSWmPs0oEtcOkLY4w1UCJBlpIxBQYfI0msOA
/SlZDQtOr1GDFf2u+PMG/YkzJTi3h2VJmw8WXaP8/sT+h/L8TPGngRXN8eK0BqrKqearGPN+6nN4
ZXhL61hNBCDXtYkA38bsgIMn8IfSP5fbEM9o643Jb4IMaHMxoRuFA8XvB17Y3wK2DaL1MwVuiLvA
FVxGhWVloZG322kngiufegjkxFkSXL0WQxBQJG5ZD+T4iOyUNZjztyChc5mr105ZgTn/VZLdAV/s
j/qUYU1Lsu4pSruiLO+Ki4lnTVD1utHxq5lHSJwLSJPLaU6uduPbcbuv598CDABfkLKodhNelgAA
AABJRU5ErkJggg==">
</image>
</svg></a></li>
      <li class="left-rounded box-whte"><a href="<?php echo $youtube_url->value; ?>" target="_blank"><svg version="1.1" id="Vector_Smart_Object_xA0_Image_1_"
    xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="32px" height="32px"
    viewBox="0 0 32 32" enable-background="new 0 0 32 32" xml:space="preserve">
<image overflow="visible" width="32" height="32" id="Vector_Smart_Object_xA0_Image" xlink:href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAACXBIWXMAAAsSAAALEgHS3X78AAAA
GXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAj5JREFUeNrMVz1LA0EQzS0JQYwK
ioVY6IGFGhJiKrVLZWlAC1OKjWBnYTA/QIm9EEEQq9gE0mphQEGs/EiINkK0slL8iIiIxLcwgRh2
7/bu8uHAY4/bZd7b3dnZWc2laKVSqQ9NFIgAQUAHfNRd5kOAPJADsrquP6n41RSIA2gSwBzgUdT7
DWSADQgp2BIA4i40SWBZRajEKkAKiEPIu7IAkPv5MgIjrsbYHd8+iCiaCgD5JJpDoNvVWHsDZiDi
XCqAZn7WBPJaEdO1K6HV7flFA5fdaDvC1ZhgNR3JFpC7iGPrzwrQUbt2EO12TkcIq5B304+EhPwe
2AYe6Gxz+wI+JY47AC9985wxBKwAw4LgXwdiGmW4R0mSGYfKWydThf8xNDeSZDXAKL2KyE+dknMj
HyeCLs4ZZZTbRXZsMrMFCzpykv8RRheLkMPEaRoissCgyk5I/gcZ3Woie1VwPAsUIWIJMDpBMl86
q7lS6+1FcXl7gF3gCCJ0i758zMCxx2K88aj+serLTcWEaBU6FYmfgVVE+77BGJmvMjMIkH4Fcl50
+E3IjXyV3FRGBQSdoyZO50GcUVwlma88MzijUyYJJmMhPmS+cmapeAJEVw5TcQjNpSwVV2/DNBpR
ZuPC9ihOeLB90GVkZF4Kul7KMYucSDDuAJOLtf06ZrSfBapeW2U7nLy+IopTudRs4xxr/68o/Rdl
eTseJkySZPjAMAVmxWG0p6gML9p9nAapgLTzON2sRrvt13Gzn+e/AgwAGdTCmR4Z+MoAAAAASUVO
RK5CYII=">
</image>
</svg></a></li>
   </ul>
</div>
<div class="modal " id="successModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="closesuccessmsg"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Success! An activation link has been sent to your email</h4>
         </div>
         <div class="modal-body" id="showmsg">
         </div>
      </div>
   </div>
</div>
<div class="modal " id="editsuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="close-editsuccessmsg"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Successfully updated your profile</h4>
         </div>
         <div class="modal-body" id="showmsg">
         </div>
      </div>
   </div>
</div>
<div class="modal " id="passwordsuccessModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="close-passwordsuccessmsg"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Successfully updated your password</h4>
         </div>
         <div class="modal-body" id="showmsg">
         </div>
      </div>
   </div>
</div>
<div class="modal " id="emailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form method="post" id="passwordform">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="closeform"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="exampleModalLabel">Enter Your Password</h4>
            </div>
            <div class="modal-body">
               <div class="password_error_msg show-error">
               </div>
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Old password*</label>
                  <div class="form-input-holder">
                     <input type="password" name="old_password" class="form-control pp_frm_inpt" id="old_password" value="<?php echo set_value('old_password'); ?>">
                  </div>
               </div>
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Password*</label>
                  <div class="form-input-holder">
                     <input type="password" name="password" class="form-control pp_frm_inpt" id="password" value="<?php echo set_value('password'); ?>">
                  </div>
               </div>
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Confirm Password*</label>
                  <div class="form-input-holder">
                     <input type="password" name="confirm_password" class="form-control pp_frm_inpt" id="confirm_password"  value="<?php echo set_value('password'); ?>">
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="email_sent" value="submit">
            </div>
         </form>
      </div>
   </div>
</div>

<!-- FORGET PASSWORD MODAL FORM-->
<div class="modal " id="forget_passwordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <form method="post" id="forget_passwordform">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="closelogin"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="exampleModalLabel">Forgot Password</h4>
            </div>
            <div class="modal-body">
               <div id="login_error_msg" class="show-error">
               </div>
               <div class="form-group">
                  <label for="recipient-name" class="control-label pp_frm_label">Email*</label>
                  <div class="form-input-holder">
                     <input type="text" name="user-email" class="form-control pp_frm_inpt" id="user-email" value="<?php echo set_value('email'); ?>">
                  </div>
               </div>
               <input type="hidden" value="" id="redirecturl">
            </div>
            <div class="modal-footer">
               <a class="btn btn-default lft_pp_btn" id="go_to_login" href="">Return to Login</a>
               <!--                            <input type="reset" class="btn btn-default lft_pp_btn" id="loginreset" value="reset" >-->
               <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="forget_password_btn" value="Request password reset">
            </div>
         </form>
      </div>
   </div>
</div>
<div class="modal " id="successForgetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"  id="closesuccessmsg"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="exampleModalLabel">Success! An reset password link has been sent to your email</h4>
         </div>
         <div class="modal-body" id="showmsg">
         </div>
      </div>
   </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/frontend/js/login_register.js'); ?>"></script>






