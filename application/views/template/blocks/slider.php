<?php if(isset($sliders) && is_array($sliders) && !empty($sliders)): ?>
<div id="myCarousel" class="carousel slide">
	<div class="carousel-inner">
		<?php 
        $cnt = 0;
            foreach($sliders as $slider)
            {
                ?>
                <div class="item <?php echo ($cnt == 0)?'active':'';?>">
                <img src="<?php echo imager(config_item('slider_image_root').$slider->slider_image, 1920, 460); ?>" alt="Slider Image" />
                <div class="container"><div class="caption"><p><?php echo $slider->slider_title;?></p><i><?php echo $slider->slider_title_second;?></i></div></div>
                </div>
                <?php
            $cnt++;
            }
        ?>
	</div>
<!--	<a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
	<a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>-->
</div>
<?php endif; ?>