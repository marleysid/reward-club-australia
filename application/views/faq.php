<!-- banner -->

<div class="banner">
  <div class="slider"><img src="<?php echo get_asset('assets/frontend/images/banner_of_club_member.jpg'); ?>"  class="img-responsive" alt="slide"></div>
  <div class="banner-slider">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
      <ul>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
      </ul>
    </div>
  </div>
</div>
<!-- banner--> 

<!-- search-featured -->
<div class="col-md-12">
<div class="container">
<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default">
    <div class="panel-heading pdn_mrgn_nn" role="tab" id="headingOne">
      <h4 class="panel-title faq_wrp">
        <a data-toggle="collapse" class="faq_head_wrp active" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
          Q. But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system?
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
      <p>
      At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p><p>

Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. </p><p>

Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading pdn_mrgn_nn" role="tab" id="headingTwo">
      <h4 class="panel-title faq_wrp">
        <a class="collapsed faq_head_wrp" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
          Q. But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system?
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body">
         <p>
      At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p><p>

Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. </p><p>

Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading pdn_mrgn_nn" role="tab" id="headingThree">
      <h4 class="panel-title faq_wrp">
        <a class="collapsed faq_head_wrp" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
         Q. But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system?
        </a>
      </h4>
    </div>
    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
      <div class="panel-body">
        <p>
      At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. </p><p>

Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. </p><p>

Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat."</p>
      </div>
    </div>
  </div>
</div>

</div>
</div>




<!-- media --> 