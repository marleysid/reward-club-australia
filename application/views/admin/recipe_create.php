<div class="box dark">
	<header>
		<div class="icons">
			<i class="fa fa-edit"></i>
		</div>
		<h5><?php echo $title; ?></h5>

		<!-- .toolbar -->
		<div class="toolbar">
			<ul class="nav">
				<li>
					<a class="minimize-box" data-toggle="collapse" href="#div-1">
						<i class="fa fa-chevron-up"></i>
					</a> 
				</li>
			</ul>
		</div><!-- /.toolbar -->
	</header>
	<div id="div-1" class="accordion-body collapse in body">
		<form class="form-horizontal" method="POST" id="recipe_form" enctype="multipart/form-data">
			<div class="form-group">
				<label for="text1" class="control-label col-lg-3">Name</label>
				<div class="col-lg-8">
					<input type="text" id="recipe_name" name="recipe_name" class="form-control" value="<?php echo set_value('recipe_name', isset($recipe) ? $recipe->recipe_name : ''); ?>">
					<?php echo form_error('recipe_name'); ?>
				</div>
			</div><!-- /.form-group -->
			<div class="form-group">
				<label for="limiter" class="control-label col-lg-3">Description</label>
				<div class="col-lg-8">
					<textarea id="recipe_description" name="recipe_description" class="form-control"><?php echo set_value('recipe_description', isset($recipe) ? $recipe->recipe_description : ''); ?></textarea>
					<?php echo form_error('recipe_description'); ?>
				</div>
			</div><!-- /.form-group -->
			<div class="form-group">
				<label for="limiter" class="control-label col-lg-3">Ingredient </label>
				<div class="col-lg-8">
					<textarea id="recipe_ingredient" name="recipe_ingredient" class="form-control"><?php echo set_value('recipe_ingredient', isset($recipe) ? $recipe->recipe_ingredient : ''); ?></textarea>
					<?php echo form_error('recipe_ingredient'); ?>
				</div>
			</div><!-- /.form-group -->
			<div class="form-group">
				<label class="control-label col-lg-3">Image</label>
				<div class="col-lg-8">
					<input type="file" name="image">
					<div class="clear"></div>
					<div style="margin-top:10px">
					<?php if(isset($recipe) && !empty($recipe->recipe_image) && file_exists(config_item('recipe_image_root').$recipe->recipe_image)) : 
						echo img(array('src'=>imager(config_item('recipe_image_path').$recipe->recipe_image,200,200),'class'=>'img-thumbnail','alt'=>'recipe-image'));
					?>
						<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="<?php echo $recipe->recipe_id; ?>" data-toggle="tooltip" title="Delete image">×</button>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="form-actions no-margin-bottom">
				<input type="submit" value="Submit" name="submit" class="btn btn-primary">
			</div>
		</form>
	</div>
</div>

<script>
	$(function(){
		$('.close').tooltip();
		$('#recipe_form .close').on('click', function(){
			
			var _this = $(this),
			_post_data = {id: _this.attr('data-id')},
			_url = base_url+"admin/recipe/delete_image"
			
			_this.parent('div').css({opacity:0.2});
			
			$.ajax({
				url: _url,
				data: _post_data,
				type: 'POST',
				success: function(data) {console.log(data)
//					if(data.stat == "ok") {
						_this.parent().fadeOut('normal', function(){
							$(this).remove();
						});
//					}
				}
			});			
		});
		
		
	})
</script>