
<div class="box">
	<header>
		<div class="icons">
			<i class="fa fa-table"></i>
		</div>
		<h5>Recipes</h5>
	</header>
	<div id="collapse4" class="body">
		<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>Name</th>
					<th>Description</th>
					<th>Ingredient</th>
					<th>Image</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if($recipes):
				foreach($recipes as $key=>$recipe): ?>
				<tr>
					<td><?php echo ++$key; ?></td>
					<td><?php echo $recipe->recipe_name; ?></td>
					<td><?php echo $recipe->recipe_description; ?></td>
					<td><?php echo $recipe->recipe_ingredient; ?></td>
					<td><?php echo (!empty($recipe->recipe_image) && file_exists(config_item('recipe_image_root').$recipe->recipe_image))?  img(imager(config_item('recipe_image_path').$recipe->recipe_image,100,100)):''; ?></td>
					<td><a href="<?php echo base_url('admin/recipe/edit/'.$recipe->recipe_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
						<a href="<?php echo base_url('admin/recipe/delete/'.$recipe->recipe_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
					</td>
				</tr>
				<?php endforeach;
				endif; ?>
			</tbody>
		</table>
	</div>
</div>
