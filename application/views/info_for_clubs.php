<!-- banner -->

<div class="banner">
  <div class="slider"><img src="<?php echo get_asset('assets/frontend/images/banner_of_club_member.jpg'); ?>"  class="img-responsive" alt="slide"></div>
  <div class="banner-slider">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
      <ul>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
      </ul>
    </div>
  </div>
</div>
<!-- banner--> 

<!-- search-featured -->
<div class="col-md-12">
<div class="search-featured" id="search-featured">
  <div class="container">
    <div class="row">
      <div class="col-md-12 menu-detail">
        <ul>
          <li><a href="#" class="active">Home</a></li>
          <li><a href="#">Information For Members</a></li>
        </ul>
      </div>
      <div class="col-md-7 member-detail club_for_member_wrp"> <img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive img-thumbnail for_club" alt="card detail">
        <p>By adding our logo to club membership cards, we help clubs build, activate and retain valuable members through a unique alliance with local businesses. </p>
        <p> Our VIP offers at almost 1000 businesses, save club members a total of more than $120 million annually. With the best ongoing offers in the market from the most established and respected businesses, it doesn't take long to see how our average annual member saving is $300 and can go well into the thousands. </p>
      </div>
      <div class="col-md-5 vdo-md-cls"> 
        <!-- featureCarousel -->
        <p> Rewards Club membership is included on your membership card when you join selected local clubs. If you are involved in a Club in Sydney or NSW regional areas, ask us how we can customise Rewards Club and develop a fully co-branded benefits program for your Club members.</p>
        <p>If there is not a participating club in your area with Rewards Club included with membership, you can join the Rewards Club direct from just $50 for a 12 month membership.</p>
        
        <!-- featureCarousel--> 
      </div>
      <div class="col-md-6 clb_exprsn_blt_hm">
        <p>By combining the best clubs with the best retailers and delivering great results for both, you end up with the best member benefits program in the market. We welcome feedback from members about your experiences at any time.</p>
        <ul>
          <li>To search Rewards offer</li>
          <li>For membership information</li>
          <li>To become a VIP Suppliers</li>
          <li>To email us an enquiry</li>
          <li>To view one of our club membership videos</li>
          <li>Club Brochure</li>
          <li>To view our corporate video</li>
        </ul>
      </div>
      <div class="col-md-6">
        <div class="embed-responsive embed-responsive-16by9 wowload fadeInLeft">
          <iframe  class="embed-responsive-item" src="//player.vimeo.com/video/10623511?title=0"  width="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>
      </div>
      <div class="col-md-12 enquiry_box_wrp"> <a href="#"><img src="<?php echo get_asset('assets/frontend/images/mid_lrg_box.png'); ?>" class="img-responsive"></a> </div>
    </div>
  </div>
</div>

</div>
<div class="col-md-12">
<div class="col-md-12">
  <div class="container">
    <h3><span>Enquiry to get promotional material from Rewards Club.</span></h3>
    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et do</p>
    <div class="advanced-search">
      <form role="form">
        <div class="col-md-4">
          <input type="Phone" class="form-control nt_wth_incld" placeholder="first name *" />
          <input type="Phone" class="form-control nt_wth_incld"  placeholder="E-mail address" />
        </div>
        <div class="col-md-4">
          <input type="Phone"  class="form-control nt_wth_incld" placeholder="sur name *" />
          <input type="Phone" class="form-control nt_wth_incld" placeholder="contact number" />
        </div>
        <div class="col-md-4">
          <input type="Phone" class="form-control nt_wth_incld" placeholder="address *" />
          <input type="Phone" class="form-control nt_wth_incld"  placeholder="address *" />
        </div>
        <div class="col-md-12">
          <h4 class="left_align">Choose a sample promotional &amp; Support Material you want to enquire about.</h4>
          <h4 class="right_align"><u>See order supplies above</u></h4>
        </div>
        <div class="col-md-12 mrgn_tp_btn_wrp">
          <div class="col-md-3 window_register_decal">
            <div class="window_register_decal_box">
              <label>
                <input type="button" class="active" />
                Window / Register decal</label>
            </div>
          </div>
          <div class="col-md-3 counter_stands">
            <div class="counter_stands_box">
              <label>
                <input type="button" class="active" />
                Window / Register decal</label>
            </div>
          </div>
          <div class="col-md-3 deal_card">
            <div class="deal_card_box">
              <label>
                <input type="button" class="active" />
                Window / Register decal</label>
            </div>
          </div>
          <div class="col-md-3 shop_front_welcome_banner">
            <div class="shop_front_welcome_banner_box">
              <label>
                <input type="button" class="active" />
                Window / Register decal</label>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="form-group">
            <textarea class="form-control nt_wth_incld" rows="5" id="comment" placeholder="Enquiry"></textarea>
          </div>
        </div>
        <div class="text-right">
          <button class="btn btn-danger">search</button>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<div class="col-md-12">
<div class="feature-service">
  <div class="container">
    <div id="owl-service" class="owl-carousel margn_wrp_fx_bx">
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/beaumont_tiles.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/Harvey_norman_logo.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/hungry_jacks_logo.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/bakers_Delight.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/subway_logo.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/tackle_world.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/bakers_Delight.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/beaumont_tiles.jpg'); ?>" class="img-responsive"></div>
    </div>
  </div>
</div>
</div>
<div class="col-md-12">
<div class="media-block">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 club-news">
      
        <div class="feature-service">
          <div class="container"><h3><span>Rewards Club News</span></h3>
            <div id="owl-service_01" class="owl-carousel margn_wrp_fx_bx brd_nn_01">
              
              <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/img1_01.jpg'); ?>" class="img-responsive">
                <div class="info"> <span>5 november</span>
                  <h4>Get your Club into Rewards Club</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                  <a href="#" class="btn btn-danger">Read More</a> </div>
              </div>
              <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/img1_01.jpg'); ?>" class="img-responsive">
                <div class="info"> <span>5 november</span>
                  <h4>Get your Club into Rewards Club</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                  <a href="#" class="btn btn-danger">Read More</a> </div>
              </div>
              <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/img1_01.jpg'); ?>" class="img-responsive">
                <div class="info"> <span>5 november</span>
                  <h4>Get your Club into Rewards Club</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                  <a href="#" class="btn btn-danger">Read More</a> </div>
              </div>
              <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/img1_01.jpg'); ?>" class="img-responsive">
                <div class="info"> <span>5 november</span>
                  <h4>Get your Club into Rewards Club</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                  <a href="#" class="btn btn-danger">Read More</a> </div>
              </div>
              <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/img1_01.jpg'); ?>" class="img-responsive">
                <div class="info"> <span>5 november</span>
                  <h4>Get your Club into Rewards Club</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                  <a href="#" class="btn btn-danger">Read More</a> </div>
              </div>
              <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/img1_01.jpg'); ?>" class="img-responsive">
                <div class="info"> <span>5 november</span>
                  <h4>Get your Club into Rewards Club</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                  <a href="#" class="btn btn-danger">Read More</a> </div>
              </div>
              <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/img1_01.jpg'); ?>" class="img-responsive">
                <div class="info"> <span>5 november</span>
                  <h4>Get your Club into Rewards Club</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                  <a href="#" class="btn btn-danger">Read More</a> </div>
              </div>
              <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/img1_01.jpg'); ?>" class="img-responsive">
                <div class="info"> <span>5 november</span>
                  <h4>Get your Club into Rewards Club</h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                  <a href="#" class="btn btn-danger">Read More</a> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- media --> 