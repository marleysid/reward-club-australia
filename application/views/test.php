<div class="container">
	<!-- breadcrumbs -->
	<ol class="breadcrumb">
  	<li><a href="#">Home</a></li>
  	<li><a href="#">Library</a></li>
  	<li class="active">Data</li>
	</ol>


	<div class="row">
		<div class="col-sm-3">
        	<div class="local-search club">
            	<h4>Search a different region</h4>
            	 <form role="form">
                 	<div class="form-group">
                <label class="lbl_cls">Region</label>
                <select class="form-control input_txt">
                  <option>Illawarra</option>
                </select>
              </div>
                 </form>
            </div>
            
            <br />
            
            <div class="local-search club refine_search">
            	<h4>Refine Search</h4>
                
                <ul id="nav">
                <li><a href="#"><input type="checkbox" value="<?php echo $cat->category_id;?>" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Cafe and Takeaway</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-5</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-6</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-7</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-8</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Grocery</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Home and Garden</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Leisure and Entertainment </a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Online Shopping</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Restaurants</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Shopping and Fashion</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Utilites</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                
              
            </ul>
            </div>
        </div>
        
		<div class="col-sm-9">
			<div class="info-block pull-left">
			<img src="<?php echo get_asset('assets/frontend/images/opsm.jpg'); ?>">
			<div class="info-links">
            <div class="pull-left"><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a>
          <br /><br />123 Reviews</div>
				<div class="pull-right"><a href="#" class="btn btn-danger">View Website &nbsp;&nbsp;<span class="glyphicon glyphicon-paperclip"></span></a><a href="#" class="btn btn-default">Favourite this&nbsp;&nbsp;<span class="glyphicon glyphicon-heart"></span></a></div>
			</div>
			</div>
			<h1 class="page-title">OPSM (Optical Prescription Spectacle Makers)</h1>
			<h5 class="sub-title">Bowral, 4/350 Bong Bong Street (Also see Dapto, Nowra, Wollongong, Shellharbour & Campbelltown.)</h5>
			<p>*Simply show your Club Member card to save 20% on frames, lenses, contacts, accessorises, prescription and non-prescription sunglasses from OPSM's $400 or below range or up to $80 off OPSM's above $400 range. </p>
			<p>Offer excludes eye tests, gift cards, Chanel, Tiffany &nbsp; Co., Bvlgari, Oliver Peoples, Paul Smith, Tag Heuer products and Gold & Wood products. Cannot be used in conjunction with special offer packages sale items, health fund associated discounts or any other discount or benefit from any source other than a rebate from a health fund.</p>
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  esta gone Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
			<blockquote>“Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa 		    quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit 		    aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. ”</blockquote>
			<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut </p>
          <div class="col-md-12 pdn_mrgn_nn"> <div class="col-md-4"> <div class="billing_available"> <img src="<?php echo get_asset('assets/frontend/images/bulk_01.jpg'); ?>" class="img-responsive">
          
           
          </div></div>
          
          <div class="col-md-4">
          <div class="billing_available"> <img src="<?php echo get_asset('assets/frontend/images/online_01.jpg'); ?>" class="img-responsive">
          
           
          </div>
          </div><div class="col-md-4">
          <div class="billing_available"> <img src="<?php echo get_asset('assets/frontend/images/sunglasses_01.jpg'); ?>" class="img-responsive">
          
           
          </div>
          </div>
		</div>		
	</div>
    </div>

</div>