<div class="banner">
  <div class="slider"><img src="<?php echo get_asset('assets/frontend/images/banner_of_club_member.jpg'); ?>"  class="img-responsive" alt="slide"></div>
  <div class="banner-slider">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
      <ul>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="col-md-12">
<div class="container"> 
  <!-- breadcrumbs -->
  <ol class="breadcrumb">
    <li><a href="#">Home</a></li>
    <li><a href="#">Library</a></li>
    <li class="active">Data</li>
  </ol>
  <div class="row">
    <div class="col-sm-3">
      <div class="local-search club">
        <h4>Search a different region</h4>
        <form role="form">
          <div class="form-group">
            <label class="lbl_cls">Region</label>
            <select class="form-control input_txt">
              <option>Illawarra</option>
            </select>
          </div>
        </form>
      </div>
      <br />
      <div class="local-search club refine_search">
        <h4>Refine Search</h4>
        <ul id="nav">
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Cafe and Takeaway</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-5</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-6</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-7</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-8</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Grocery</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Home and Garden</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Leisure and Entertainment </a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Online Shopping</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Restaurants</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Shopping and Fashion</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Utilites</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                
              
            </ul>
      </div>
  
     <img src="<?php echo get_asset('assets/frontend/images/bulk_01.jpg'); ?>" class="img-responsive">
    </div>
    <div class="col-sm-9">
   
      <div class="col-md-12 bg_nn_pgmrgn">
        <div class="col-sm-3 pdn_mrgn_nn">
          <h5>Showing 1- 10 of 34 total results</h5>
        </div>
        <div class="col-sm-5 pdn_mrgn_nn">
          <ul class="deal_page_map">
            <li><strong>View as</strong></li>
            <li>
              <button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-th-large"></span> </button>
              Grid</li>
            <li>
              <button type="button" class="active btn btn-default btn-sm"> <span class="glyphicon glyphicon-list"></span> </button>
              List</li>
            <li>Map
              <button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-map-marker"></span> </button>
            </li>
          </ul>
        </div>
        <div class="col-sm-4 ">
          <div class="local-search club bg_nn_pgmrgn">
            <form role="form">
              <div class="form-group">
                <label class="lbl_cls">Sort By</label>
                <select class="form-control input_txt clr">
                  <option>Illawarra</option>
                  <option>Illawarra1</option>
                  <option>Illawarra2</option>
                  <option>Illawarra3</option>
                  <option>Illawarra4</option>
                  <option>Illawarra</option>
                  <option>Illawarra</option>
                </select>
              </div>
            </form>
          </div>
        </div>
      </div>
       <!--repeated start here -->
       <div class="col-md-12 bg_nn_pgmrgn bt_mrgn_wrp">
        <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price" ><span>Best Price</span>
          <a href="#" class="best_img_sprt_clr_cgng"><img src="<?php echo get_asset('assets/frontend/images/best_price.png'); ?>" class="img-responsive"></a> </div>
        <div class="col-md-9 with_resuld_board bg_grw_01">
          <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">
         
            <h4>Example of Main title for this deal. </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labe et dolore magna aliqua. <br />Ut enim ad, quisnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
          </div>
          <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
            <h4><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a> <br />
              123 Reviews </h4>
               <button class="view_website btn fx_font_siz heart_bg"> Favourite this <span class="glyphicon glyphicon-heart"></span> </button>
          
            <button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-arrow-right"></span> </button>
          </div>
        </div>
      </div>
      <div class="col-md-12 bg_nn_pgmrgn bt_mrgn_wrp">
        <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span>buy now</span>
          <a href="#" class="best_img_sprt_clr_cgng"><img src="<?php echo get_asset('assets/frontend/images/best_price.png'); ?>" class="img-responsive"></a> </div>
        <div class="col-md-9 with_resuld_board bg_grw_01">
          <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">
         
            <h4>Example of Main title for this deal. </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labe et dolore magna aliqua. <br />Ut enim ad, quisnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
          </div>
          <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
            <h4><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a> <br />
              123 Reviews </h4>
               <button class="view_website btn fx_font_siz heart_bg"> Favourite this <span class="glyphicon glyphicon-heart"></span> </button>
          
            <button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-arrow-right"></span> </button>
          </div>
        </div>
      </div>
      <div class="col-md-12 bg_nn_pgmrgn bt_mrgn_wrp">
        <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span>Best price</span>
          <a href="#" class="best_img_sprt_clr_cgng"><img src="<?php echo get_asset('assets/frontend/images/best_price.png'); ?>" class="img-responsive"></a> </div>
        <div class="col-md-9 with_resuld_board bg_grw_01">
          <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">
         
            <h4>Example of Main title for this deal. </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labe et dolore magna aliqua. <br />Ut enim ad, quisnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
          </div>
          <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
            <h4><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a> <br />
              123 Reviews </h4>
               <button class="view_website btn fx_font_siz heart_bg"> Favourite this <span class="glyphicon glyphicon-heart"></span> </button>
          
            <button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-arrow-right"></span> </button>
          </div>
        </div>
      </div>
      <div class="col-md-12 bg_nn_pgmrgn bt_mrgn_wrp">
        <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span>buy now</span>
          <a href="#" class="best_img_sprt_clr_cgng"><img src="<?php echo get_asset('assets/frontend/images/best_price.png'); ?>" class="img-responsive"></a> </div>
        <div class="col-md-9 with_resuld_board bg_grw_01">
          <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">
         
            <h4>Example of Main title for this deal. </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labe et dolore magna aliqua. <br />Ut enim ad, quisnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
          </div>
          <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
            <h4><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a> <br />
              123 Reviews </h4>
               <button class="view_website btn fx_font_siz heart_bg"> Favourite this <span class="glyphicon glyphicon-heart"></span> </button>
          
            <button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-arrow-right"></span> </button>
          </div>
        </div>
      </div>
      <div class="col-md-12 bg_nn_pgmrgn bt_mrgn_wrp">
        <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span>best price</span>
          <a href="#" class="best_img_sprt_clr_cgng"><img src="<?php echo get_asset('assets/frontend/images/best_price.png'); ?>" class="img-responsive"></a> </div>
        <div class="col-md-9 with_resuld_board bg_grw_01">
          <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">
         
            <h4>Example of Main title for this deal. </h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labe et dolore magna aliqua. <br />Ut enim ad, quisnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
          </div>
          <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
            <h4><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a> <br />
              123 Reviews </h4>
               <button class="view_website btn fx_font_siz heart_bg"> Favourite this <span class="glyphicon glyphicon-heart"></span> </button>
          
            <button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-arrow-right"></span> </button>
          </div>
        </div>
      </div>
    
      <!--repeated end here -->
      <!--pagination start here -->
      
      <div class="col-md-12">
                        <div class="col-md-3">
                            
                        </div>
                        <div class="col-md-9 bg_nn_pgmrgn">
                        <div class="col-md-7 pgn_wrp_clr">
                        <ul class="pagination">
                                <li><button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-arrow-left"></span>
        </button></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-arrow-right"></span>
        </button></li>
                            </ul>
                         </div>
                            <div class="col-md-4 bg_nn_pgmrgn pull-righ">
                            <div class="btn-group" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                	<h4 class="margin_right_wrp_bxt_itm">Items per page:</h4>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">1 <span class="caret"></span> </button>
                                    <ul class="dropdown-menu insrt_with" role="menu">
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                    </ul>
                                </div>
                                         
                                
                            </div>
                            </div>

                        </div>
                    </div>
      <!--pagination end here -->
    </div>
  </div>
</div>
<div class="feature-service">
  <div class="container">
    <div id="owl-service_01" class="owl-carousel margn_wrp_fx_bx">
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
    </div>
  </div>
</div>
</div>