

<!-- banner -->
<!-- <div class="main-banner">
   <?php $this->load->view('deals/frontend/top_slider');  ?>
</div> -->

<!-- banner-->
<!-- search-featured -->
<div class="search-clubs-wrap">
    <div class="search-wrapper">
        <h3>Search Offers</h3>



        <div class="advanced-search add">
        <?php $suburbimg = $this->session->userdata('image');?>
        <?php if ($suburbimg != "" && file_exists($suburbimg)){
            ?>
             <img src="<?php echo get_asset($suburbimg); ?>"  alt="image01">
        <?php 
        } else{      
       ?>
        <img src="<?php echo get_asset('assets/frontend/images/img01.jpg'); ?>"  alt="image01">

        <?php 
    } 
    ?>
        <div class="advanced-search-content">
            <form role="form" method="GET" action="<?php echo site_url('offers'); ?>" id="advancesearch">
                
                <div class="form-wrap">
                    <input type="text" class="form-control"  placeholder="Search discounts in my local area" id="suburbsall" name="location">
                </div>
                <div class="form-wrap">
                    <div class="select-wrap">
                        <select name="category" id="" class="custom search-category">
                            <option value="">Category</option>
                            <?php foreach ($categories as $cat){?>
                                <option value="<?php echo $cat->category_id;?>"><?php echo $cat->category_name;?></option>
                            <?php } ?>
                        </select>
                    </div>                
                    <input type="submit"  class="search-button" value="Search" /> 
                </div>
            </form>
        </div>
        </div>
    </div>
    <div class="local-clubs-wrap">
        <h3>Local Clubs</h3>
        
        <div class="local-clubs-holder">
            <div id="localClubs" class="">
                <?php if($all_favourites): ?>
                    <?php foreach($all_favourites as $key=>$val): ?>
                <div class="item">

                    <div class="image-wrap" style="background:url(<?php echo base_url().$val->club_home_image_path ?>) no-repeat; height:171px; background-position:50% 50%;">
                    </div>
                    <div class="info-wrap">
                        <div class="text-holder">
                            <strong class="title"><a href="<?php echo site_url('cms/club_detail/'  . safe_b64encode($val->club_id)).'/'.strtolower(url_title($val->club_name,'-','TRUE')) ?>"><?php echo $val->club_name?></a></strong>
                            <span class="location"><?php echo $val->club_suburb?></span>
                        </div>
                        <span class="like-wrap">like</span>
                    </div>
                </div>
                <?php endforeach; ?>
                <?php endif; ?>

            </div>            
        </div>
    </div> 
</div>

<div class="discount-featured-wrap">
    <div class="discount-wrap">
        
        <h3>Other Local Discounts</h3>

        <div class="discount-holder">
            <div id="owl-discount" class="owl-carousel">
                <?php if (isset($get_bottom_slider) )://and ! empty($get_bottom_slider)):  ?>

                    <?php foreach ($get_bottom_slider as $bottom_slider): ?>
                    
                        <div class="item"><a href="<?php echo site_url('offers/detail/'.$bottom_slider->type.'/' . safe_b64encode($bottom_slider->slider_type_id).'/'.strtolower(url_title($bottom_slider->offer_title,'-','TRUE'))); ?>">
                                <div class="image-wrap">
                                    <img src="<?php
                                    if ($bottom_slider->slider_type == 'offer')
                                        echo imager($bottom_slider->offer_image ? 'assets/uploads/offer_image/' . $bottom_slider->offer_image : '', 199, 199,1);
                                    ?>" class="img-responsive">
                                </div>
                            </a> 
                            <div class="info-wrap"><strong class="title"><a href="<?php echo site_url('offers/detail/'.$bottom_slider->type.'/' . safe_b64encode($bottom_slider->slider_type_id).'/'.strtolower(url_title($bottom_slider->offer_title,'-','TRUE'))); ?>"><?php echo $bottom_slider->offer_title; ?></a></strong>
                                <span class="location"><?php echo $bottom_slider->oa_suburb; ?></span></div></div>
                    <?php endforeach; ?>
                <?php endif; ?>



            </div> 
        </div>
    </div>
    <?php $this->load->view('deals/frontend/featured_slider'); ?>
</div>
<!-- search-featured -->
<!-- media -->
<div class="media-block add">
    <div class="featured-club-news-holder add">
    <h3><span>Rewards Club News</span></h3>
    <div id="owl-news" class="owl-carousel club-news">
        <?php if (isset($news_detail)): ?>
        <?php foreach ($news_detail as $news): ?>
    <div>
    <a href="<?php echo site_url('news').'/'.safe_b64encode($news->news_id) .'/'.strtolower(url_title($news->news_title,'-','TRUE')) ?>"> <img src="<?php echo imager($news->news_image ? 'assets/uploads/news/'.$news->news_image:'',218,106,1); ?>" class="img-responsive"></a>
    <div class="info">
        <span><?php echo date("d F", strtotime($news->news_create_date)); ?></span>
        <h4><a href="<?php echo site_url('news').'/'.safe_b64encode($news->news_id).'/'.strtolower(url_title($news->news_title,'-','TRUE')) ?>"><?php echo character_limiter(ucwords($news->news_title), 20); ?></a></h4>
        <p><?php echo character_limiter($news->news_excerpt, 75) ?> </p>
        <!--                            <button class="btn btn-danger">Read More</button>-->
        <?php echo anchor("news/".safe_b64encode($news->news_id).'/'.strtolower(url_title($news->news_title,'-','TRUE')), 'Read More', 'class="btn btn-danger"'); ?>
    </div>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>
    </div>
    </div>
</div>
<script type="text/javascript">
   $('#search-reset').click(function() {
       //alert('test');
        $("#advancesearch")[0].reset();
   
   
   });
</script>
<script src="<?php echo base_url('assets/lib/js_loader/script.min.js')?>" ></script>
<script src="<?php echo base_url('assets/js/autocomplete.js')?>" ></script>

