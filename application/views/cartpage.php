<div class="banner">
  <div class="slider"><img src="<?php echo get_asset('assets/frontend/images/banner_of_club_member.jpg'); ?>"  class="img-responsive" alt="slide"></div>
  <div class="banner-slider">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
      <ul>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="col-md-12">
  <div class="container"> 
    <!-- breadcrumbs -->
    <ol class="breadcrumb">
      <li class="active"><a href="#">My Cart: </a></li>
      <li>2 Items</li>
    </ol>
    <div class="row">
      <div class="col-md-12">
        <div class="col-md-12 mrgn_pdgn_bx_none_wrp_fx">
          <div class="col-md-10 bg_nn_pgmrgn bt_mrgn_wrp mrgn_rght_bx_wrp widht_cntr_mm"> 
            <!--repeated start here -->
            <div class="col-md-12">
              <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> <a href="#" class="best_img_sprt_clr_cgng"><img src="<?php echo get_asset('assets/frontend/images/best_price.png'); ?>" class="img-responsive"></a> </div>
              <div class="col-md-9 with_resuld_board bg_grw_01 ">
                <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">
                  <h4>Example of Main title for this deal. </h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labe et dolore magna aliqua. <br />
                    Ut enim ad, quisnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                </div>
                <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
                  <h4><a href="#" >AU $299</a> <br />
                    QTY: 1 </h4>
                  <button class="view_website btn fx_font_siz heart_bg"> Edit <span class="glyphicon glyphicon-edit"></span></button>
                  <button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-heart"></span> </button>
                </div>
              </div>
            </div>
            <!--repeated end here --> 
            <!--repeated start here -->
            <div class="col-md-12">
              <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> <a href="#" class="best_img_sprt_clr_cgng"><img src="<?php echo get_asset('assets/frontend/images/best_price.png'); ?>" class="img-responsive"></a> </div>
              <div class="col-md-9 with_resuld_board bg_grw_01 ">
                <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">
                  <h4>Example of Main title for this deal. </h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labe et dolore magna aliqua. <br />
                    Ut enim ad, quisnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                </div>
                <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
                  <h4><a href="#" >AU $299</a> <br />
                    QTY: 1 </h4>
                  <button class="view_website btn fx_font_siz heart_bg"> Edit <span class="glyphicon glyphicon-edit"></span></button>
                  <button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-heart"></span> </button>
                </div>
              </div>
            </div>
            <!--repeated end here --> <!--repeated start here -->
            <div class="col-md-12">
              <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> <a href="#" class="best_img_sprt_clr_cgng"><img src="<?php echo get_asset('assets/frontend/images/best_price.png'); ?>" class="img-responsive"></a> </div>
              <div class="col-md-9 with_resuld_board bg_grw_01 ">
                <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">
                  <h4>Example of Main title for this deal. </h4>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labe et dolore magna aliqua. <br />
                    Ut enim ad, quisnostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                </div>
                <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
                  <h4><a href="#" >AU $299</a> <br />
                    QTY: 1 </h4>
                  <button class="view_website btn fx_font_siz heart_bg"> Edit <span class="glyphicon glyphicon-edit"></span> </button>
                  <button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-heart"></span> </button>
                </div>
              </div>
            </div>
            <!--repeated end here --> 
            
          </div>
          <div class="col-md-2 mrgn_none_div_cls">
            <div class="totwal_wrp_bx bg_grw_01 mrgn_rght_bx_wrp mrgn_rght_bx_wrp">
              <div class="whit_box fix_with_right flt_right_wrp tp_btn_mrgn_wrp widht_cntr_mm">
                <h4>TOTAL: (excludes delivery) <br />
<a href="#" >$548.00</a> <br /></h4>
              </div>
            </div>
            <button class="view_website btn fx_font_siz heart_bg"> Continue Shopping </button>
            <button class="view_website btn fx_font_siz">Pay Securely Now </button>
          </div>
        </div>
        
        <!--pagination start here --> 
        
        <!--pagination end here --> 
      </div>
    </div>
  </div>
</div>
