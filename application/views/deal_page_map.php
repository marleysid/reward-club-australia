<div class="container">
	<!-- breadcrumbs -->
	<ol class="breadcrumb">
  	<li><a href="#">Home</a></li>
  	<li><a href="#">Library</a></li>
  	<li class="active">Data</li>
	</ol>


	<div class="row">
		<div class="col-sm-3">
        	<div class="local-search club">
            	<h4>Search a different region</h4>
            	 <form role="form">
                 	<div class="form-group">
                <label class="lbl_cls">Region</label>
                <select class="form-control input_txt">
                  <option>Illawarra</option>
                </select>
              </div>
                 </form>
            </div>
            
   
            
            <div class="local-search club refine_search">
            	<h4>Refine Search</h4>
                <ul id="nav">
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/gft.png'); ?>">Special deals</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/auto.png'); ?>">Automotive</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Cafe and Takeaway</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-5</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-6</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-7</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/cafe.png'); ?>">Submenu 2-8</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Grocery</a>
                    <ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/grocery.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Home and Garden</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/home_garden.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Leisure and Entertainment </a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/les.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Online Shopping</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/online_shop.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Restaurants</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/resturant.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Shopping and Fashion</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/shop.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Utilites</a>
                	<ul class="subs">
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-1</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-2</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-3</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-4</a></li>
                        <li><a href="#"><input type="checkbox" class="srch_bt_bx"><img src="<?php echo get_asset('assets/frontend/images/utilies.png'); ?>">Submenu 3-5</a></li>
                    </ul>
                </li>
                
              
            </ul>
                
                
            
           
      
        </div>
         <img src="<?php echo get_asset('assets/frontend/images/bulk_01.jpg'); ?>" class="img-responsive">
        </div>
       
		<div class="col-sm-9">
        <div class="col-sm-3 pdn_mrgn_nn"> <h5>Showing 1- 10 of 34 total results</h5></div>
        <div class="col-sm-5 pdn_mrgn_nn">
        <ul class="deal_page_map">
        	<li><strong>View as</strong></li>
            <li><button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-th-large"></span>
        </button>Grid</li>
            <li> <button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-list"></span>
        </button>List</li>
            <li>Map <button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-map-marker"></span>
        </button></li>
        </ul>
        </div>
        <div class="col-sm-4 ">    
		<div class="local-search club bg_nn_pgmrgn"><form role="form">
                 	<div class="form-group">
                <label class="lbl_cls">Sort By</label>
                <select class="form-control input_txt clr">
                  <option>Illawarra</option>
                </select>
              </div>
                 </form></div>	</div>
        </ul>
        <br />
        <br />
			<img src="<?php echo get_asset('assets/frontend/images/map.jpg'); ?>" class="img-responsive">
		
			
			
          		
	</div>
    </div>

</div>
<div class="feature-service">
  <div class="container">
    <div id="owl-service_01" class="owl-carousel margn_wrp_fx_bx">
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
      <div class="item logo_img"><img src="<?php echo get_asset('assets/frontend/images/information_for_club.jpg'); ?>" class="img-responsive"></div>
    </div>
  </div>
</div>