<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
/*
| -------------------------------------------------------------------------
| Site Messaging
| -------------------------------------------------------------------------
| This file lets you display messages of all types.
| 1. Success
| 2. Error
| 3. Warning
| 4. Information
*/
?>
<?php
/*
| -------------------------------------------------------------------------
| Site Messaging :: Success
| -------------------------------------------------------------------------
*/
?>
<?php if($this->session->flashdata('success_message')): ?>		
    <div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo $this->session->flashdata('success_message'); ?>
    </div>
<?php endif; ?>
<?php
/*
| -------------------------------------------------------------------------
| Site Messaging :: Error
| -------------------------------------------------------------------------
*/
?>
<?php if($this->session->flashdata('error_message')): ?>		
    <div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo $this->session->flashdata('error_message'); ?>
    </div>
<?php endif; ?>
<?php
/*
| -------------------------------------------------------------------------
| Site Messaging :: Warning
| -------------------------------------------------------------------------
*/
?>
<?php if($this->session->flashdata('warning_message')): ?>		
    <div class="alert">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo $this->session->flashdata('warning_message'); ?>
    </div>
<?php endif; ?>
<?php
/*
| -------------------------------------------------------------------------
| Site Messaging :: Information
| -------------------------------------------------------------------------
*/
?>
<?php if($this->session->flashdata('info_message')): ?>		
    <div class="alert alert-info">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <?php echo $this->session->flashdata('info_message'); ?>
    </div>
<?php endif; ?>