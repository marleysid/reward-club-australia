<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Web_Service_Controller extends MX_Controller {

    protected $data = array();

    public function __construct() {
        parent::__construct();
        $this->load->database('webservice');
        $this->load->model('webservice/webservice_model');
        $this->load->model('webservice/common');
    }

    public function __response($responsetext = array()) {
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($responsetext));
    }

    public function response($code, $response = array()) {
        $returnArr = array('code' => $code, 'msg' => $this->common->code($code));
        $returnArr = array_merge($returnArr, $response);
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($returnArr));
    }

    public function is_logged_in() {
        $member_id = $this->input->post('member_id');
        $hash_code = $this->input->post('hash_code');
        $device_id = $this->input->post('device_id');
        $device_type = $this->input->post('device_type');

        if ($user = $this->webservice_model->checkLogin($member_id, $hash_code, $device_id, $device_type)) {
            return $user;
        }

        return FALSE;
    }

}
