<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Base_Controller extends MX_Controller {

    /**
     * Stores the previously viewed page's complete URL.
     *
     * @var string
     */
    protected $previous_page;

    /**
     * Stores the page requested. This will sometimes be
     * different than the previous page if a redirect happened
     * in the controller.
     *
     * @var string
     */
    protected $requested_page;

    /**
     * Stores the current user's details, if they've logged in.
     *
     * @var object
     */
    protected $current_user = NULL;
    protected $current_member = NULL;
    protected $data = array();
    public $autoload = array(
        'helper' => array('url', 'form'),
        'libraries' => array('auth/ion_auth','libraries/member_auth'),
    );

    //--------------------------------------------------------------------

    /**
     * Class constructor
     *
     */
    public function __construct() {
        parent::__construct();
        $this->load->database('default');
      //  $this->spdb = $this->load->database('webservice', true);

        // Load Activity Model Since it's used everywhere.
//		$this->load->library(array('auth/ion_auth'));
        // Load our current logged in user so we can access it anywhere.
        if ($this->ion_auth->logged_in()) {

            $this->current_user = $this->ion_auth->user()->row();
            $this->current_user->id = (int) $this->current_user->user_id;
//			$this->current_user->user_img = gravatar_link($this->current_user->email, 22, $this->current_user->email, "{$this->current_user->email} Profile");
        }
        Template::set('current_user', $this->current_user);

        /*
          Performance optimizations for production environments.
         */
//		if (ENVIRONMENT == 'production')
//		{
//		    $this->db->save_queries = FALSE;
//
//		    $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
//		}
//
//		// Testing niceties...
//		else if (ENVIRONMENT == 'testing')
//		{
//			$this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));
//		}
        // Development niceties...
//		else if (ENVIRONMENT == 'development')
        if (ENVIRONMENT == 'development') {
            if (!$this->input->is_cli_request() AND ! $this->input->is_ajax_request()) {
//				$this->load->library('Console');
//				$this->output->enable_profiler(TRUE);
            } else {
//				$this->output->enable_profiler(FALSE);
            }
        }

        // Make sure no assets in up as a requested page or a 404 page.
//		if ( ! preg_match('/\.(gif|jpg|jpeg|png|css|js|ico|shtml)$/i', $this->uri->uri_string()))
//		{
//			$this->previous_page = $this->session->userdata('previous_page');
//			$this->requested_page = $this->session->userdata('requested_page');
//		}
    }

//end __construct()
    //--------------------------------------------------------------------
}

//end Base_Controller
//--------------------------------------------------------------------

/**
 * Front Controller
 *
 * This class provides a common place to handle any tasks that need to
 * be done for all public-facing controllers.
 *
 */
class Front_Controller extends Base_Controller {
    //--------------------------------------------------------------------

    /**
     * Class constructor
     *
     */
    public function __construct() {
        parent::__construct();

        $this->load->helper(array('html'));
        $this->load->model(array(
            'region/region_model',
            'categories/category_model',
            'feedback/feedback_model',
            'settings/settings_model',
            'orders/order_model',
                'logo/logo_model',
                'clubs/club_model')
        );

        // initialize required data
        $regions = $this->region_model
                ->select('region_id,region_name')
                ->where('region_status', '1')
                ->order_by('region_name')
                ->get_all();

        $categories = $this->category_model
                ->select('category_id,category_name')
                ->where('category_status', '1')
                 ->order_by('category_name')
                ->where('category_parent_id','0')
                ->get_all();

        $feedbacks = $this->feedback_model
                ->select('feedback_name as name, feedback_content', FALSE)
                ->where('feedback_status', '1')
                ->order_by('created_ts','desc')
                ->limit(10)
                ->get_all();

        $logo = $this->logo_model->logo_detail();
        $membership_types = $this->db->select('*')->get('membership_types')->result();
       $tweets = '';//$this->get_tweets();
        $_settings = $this->settings_model
                ->select('key, value')
                ->get_all();
        $settings = FALSE;
        if ($_settings) {
            $settings = array();
            foreach ($_settings as $key => $value) {
                $settings[$value->key] = $value->value;
            }
        }
        $facebook_url = $this->settings_model->get_facebook_url();
        $twitter_url = $this->settings_model->get_twitter_url();
        $youtube_url = $this->settings_model->get_youtube_url();

        $userLoggedIn = FALSE;
        $userDetail = NULL;
        $cart_amount = NULL;
        $cart = NULL;
        $clubeventdetail= NULL;
        $clubfacilitydetail= NULL;
        $clubfeaturedetail= NULL;
        $clubcontactdetail= NULL;
        $clubcontactcount = NULL;
        $club_list = NULL;
        $favourite =NULL;
        $user_id=NULL;
        $get_all_clubs_desc=NULL;
        $userclub_id= NULL;
        if ($this->member_auth->logged_in()) {
            
            $this->data['edit'] = TRUE;
            $userLoggedIn = TRUE;
            $userDetail = $this->member_auth->user();
            $this->current_member = $this->member_auth->user();
            $this->current_member->id = (int) $userDetail->id;;
            $cart_amount = $this->order_model->total_amount($userDetail->id);

            $userclub_id = $userDetail->club_id;

            $user_id = $userDetail->id;
//            $get_clubs=$this->club_model->get_all_clubs($user_id);
            $get_all_clubs_desc=$this->club_model->get_all_clubs_desc($user_id);
            $clubeventdetail =$this->club_model->get_club_events($userclub_id);
            $clubfacilitydetail =$this->club_model->get_club_facilities($userclub_id);
            $clubfeaturedetail =$this->club_model->get_club_features($userclub_id);
            $clubcontactdetail =$this->club_model->get_club_contacts($userclub_id);
            $clubcontactcount = $this->club_model->count_club_contact($userclub_id);
            $favourite  = $this->club_model->get_club_favourite($user_id);
            $club_list = $this->club_model->get_club_list($user_id,$userclub_id);

        }

        Template::add_js(base_url('assets/frontend/js/jquery.validate.js'));
        Template::add_js(base_url('assets/frontend/js/additional-methods.js'));
        Template::add_js(base_url('assets/frontend/js/feedback_subscription.js'));
        Template::set('regions', $regions);
        Template::set('categories', $categories);
        Template::set('userLoggedIn', $userLoggedIn);
        Template::set('userDetail', $userDetail);
        Template::set('cart_amount', $cart_amount);
        Template::set('feedbacks', $feedbacks);
        Template::set('settings', $settings);
        Template::set('club_list', $club_list);
        Template::set('clubeventdetail', $clubeventdetail);
        Template::set('clubfacilitydetail', $clubfacilitydetail);
        Template::set('clubcontactdetail', $clubcontactdetail);
        Template::set('clubfeaturedetail', $clubfeaturedetail);
        Template::set('clubcontactcount', $clubcontactcount);
        Template::set('get_all_clubs_desc', $get_all_clubs_desc);
        Template::set('favourite', $favourite);
        Template::set('user_id', $user_id);
        Template::set('membership_types', $membership_types);
        Template::set('current_member', $this->current_member);
        Template::set('userclub_id', $userclub_id);
        Template::set('facebook_url', $facebook_url);
        Template::set('twitter_url', $twitter_url);
        Template::set('youtube_url', $youtube_url);
        Template::set('tweets', $tweets);
        Template::set('logo', $logo);
       
        Template::set_layout('template/frontend_template');
    }

//end __construct()
    //--------------------------------------------------------------------
    
   
}


//end Front_Controller
//--------------------------------------------------------------------

/**
 * Admin Controller
 * 
 *
 */
class Admin_Controller extends Base_Controller {
    //--------------------------------------------------------------------

    /**
     * Class constructor - setup paging and keyboard shortcuts as well as
     * load various libraries
     *
     */
    public function __construct() {
        parent::__construct();

        if ($this->ion_auth->is_admin() === FALSE) {
            redirect('admin/login');
        }
        Template::set_base_title('Admin Panel');

        $this->load->helper(array('html'));

        Template::set_layout('template/admin');

        // Pagination config
        $this->pager = array();
        $this->pager['full_tag_open'] = '<div class="table_pagination left">';
        $this->pager['full_tag_close'] = '</div>';
        $this->pager['next_link'] = '&raquo;';
        $this->pager['prev_link'] = '&laquo;';

        $this->pager['first_link'] = '&laquo;&laquo;';
        $this->pager['prev_link'] = '&laquo;';
        $this->pager['cur_tag_open'] = '<a href="javascript:void(0)" class="active">';
        $this->pager['cur_tag_close'] = '</a>';
//		$this->pager['next_tag_open']	= '<li>';
//		$this->pager['next_tag_close']	= '</li>';
//		$this->pager['prev_tag_open']	= '<li>';
//		$this->pager['prev_tag_close']	= '</li>';
//		$this->pager['first_tag_open']	= '<li>';
//		$this->pager['first_tag_close']	= '</li>';
//		$this->pager['last_tag_open']	= '<li>';
//		$this->pager['last_tag_close']	= '</li>';
//		$this->pager['num_tag_open']	= '<li>';
//		$this->pager['num_tag_close']	= '</li>';
    }

//end construct()
    //--------------------------------------------------------------------
}

//end Admin_Controller

class Club_Admin_Controller extends Base_Controller {
    //--------------------------------------------------------------------

    /**
     * Class constructor - setup paging and keyboard shortcuts as well as
     * load various libraries
     *
     */
    public function __construct() {
        parent::__construct();

        if ($this->ion_auth->is_club() === FALSE) {
            redirect('login');
        }
        Template::set_base_title('Club Admin Panel');

        $this->load->helper(array('html'));

        Template::set_layout('template/club_admin');

        // Pagination config
        $this->pager = array();
        $this->pager['full_tag_open'] = '<div class="table_pagination left">';
        $this->pager['full_tag_close'] = '</div>';
        $this->pager['next_link'] = '&raquo;';
        $this->pager['prev_link'] = '&laquo;';

        $this->pager['first_link'] = '&laquo;&laquo;';
        $this->pager['prev_link'] = '&laquo;';
        $this->pager['cur_tag_open'] = '<a href="javascript:void(0)" class="active">';
        $this->pager['cur_tag_close'] = '</a>';
//		$this->pager['next_tag_open']	= '<li>';
//		$this->pager['next_tag_close']	= '</li>';
//		$this->pager['prev_tag_open']	= '<li>';
//		$this->pager['prev_tag_close']	= '</li>';
//		$this->pager['first_tag_open']	= '<li>';
//		$this->pager['first_tag_close']	= '</li>';
//		$this->pager['last_tag_open']	= '<li>';
//		$this->pager['last_tag_close']	= '</li>';
//		$this->pager['num_tag_open']	= '<li>';
//		$this->pager['num_tag_close']	= '</li>';
    }

//end construct()
    //--------------------------------------------------------------------
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
