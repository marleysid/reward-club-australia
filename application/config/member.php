<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['email_exist'] = 'Email address already registered.';
$config['success_user_create'] = 'User created successfully.';
$config['error_user_create'] = 'User created successfully.';

$config['tables']['users'] = 'users';
$config['salt_length'] = 10;
$config['store_salt'] = FALSE;
$config['hash_method'] = 'bcrypt';



