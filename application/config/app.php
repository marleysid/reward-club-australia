<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//for image paths
$config['site_title'] = "Health Synergy";
$config['assets_path'] = 'assets/';
$config['users_image_path'] = $config['assets_path'] . 'uploads/users/';
$config['vehicle_image_path'] = $config['assets_path'] . 'uploads/vehicles/';
$config['category_image_path'] = $config['assets_path'] . 'uploads/category/';
$config['recipe_image_path'] = $config['assets_path'] . 'uploads/recipes/';
$config['cms_image_path'] = $config['assets_path'] . 'uploads/cms/';



//for image root
$config['assets_root'] = FCPATH . 'assets/';
$config['users_image_root'] = FCPATH . $config['users_image_path'];
$config['vehicle_image_root'] = FCPATH . $config['vehicle_image_path'];
$config['category_image_root'] = FCPATH . $config['category_image_path'];
$config['recipe_image_root'] = FCPATH . $config['recipe_image_path'];
$config['cms_image_root'] = FCPATH . $config['cms_image_path'];




/**
 * other config
 */




/* End of file app.php */
/* Location: ./application/config/app.php */
