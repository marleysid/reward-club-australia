<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "welcome";
$route['404_override'] = '';
$route['login'] = 'login/admin';


$route['user/login'] = 'login/login';
$route['user/register'] = 'login/register';

$route['user/logout'] = 'login/logout';
//$route['user/editprofile'] = 'login/editprofile';


$route['club_admin/logout'] = 'login/club_admin/logout';
$route['admin/logout'] = 'login/admin/logout';
$route['admin/([a-zA-Z_-]+)/(:any)'] = '$1/admin/$2';
$route['club_admin/([a-zA-Z_-]+)'] = '$1/club_admin';
$route['club_admin/([a-zA-Z_-]+)/(:any)'] = '$1/club_admin/$2';
$route['admin/([a-zA-Z_-]+)'] = '$1/admin';
$route['admin'] = 'dashboard/admin';
$route['club_admin'] = 'dashboard/club_admin';
$route['webservice/categories'] = 'categories/webservice';
$route['webservice/categories/(:any)'] = 'categories/webservice/$1';
$route['webservice/offers'] = 'categories/offers';
$route['webservice/deals'] = 'categories/deals';
$route['webservice/offers/(:any)'] = 'offers/webservice/$1';
$route['webservice/deals/(:any)'] = 'deals/webservice/$1';


$route['webservice/reviews/(:any)'] = 'reviews/webservice/$1';
$route['reset_password/(:num)/(:any)'] = 'webservice/reset_password/$1/$2';
$route['member/reset_password/(:num)/(:any)'] = 'login/reset_password/$1/$2';


//$route['frontend'] = "frontend";
//$route['frontend/([a-zA-Z_-]+)/([0-9]+)'] = "$1/frontend/index/$2";
//$route['frontend/([a-zA-Z_-]+)/(:any)'] = "$1/frontend/$2";
//$route['frontend/([a-zA-Z_-]+)'] = "$1/frontend/index";
//$route['(:any)/frontend'] = "frontend/$1";

$route['information-for-members'] = "cms/information-for-members";
$route['club_information'] = "cms/club_information";
$route['club_information/(:num)'] = "cms/club_information/$1";
$route['information-for-members/(:any)'] = "cms/information-for-members/$1";
$route['information-for-suppliers'] = "cms/info_for_suppliers/information-for-suppliers";
$route['information-for-clubs'] = "cms/info_for_clubs/information-for-clubs";
$route['information-for-clubs/(:any)'] = "cms/club_detail_view/$1";
$route['terms-and-conditions'] = "cms/terms_conditions/terms-and-conditions";
$route['privacy-policy'] = "cms/privacy_policy/privacy-policy";
$route['contacts'] = "cms/contact/contacts";
//$route['details/(:any)/(:any)'] = "offers/detail/$1/$1";
$route['faq'] = "cms/faq";
$route['offers/grid'] = "offers";
$route['activate/(:num)/(:any)'] = 'login/activate/$1/$2';


//for test controller only (design integration purpose only need to change later)
$route['deal-page-map'] = "test/deal_page_map";
$route['deal-page-list'] = "test/deal_page_list";
$route['deal-page'] = "test/deal_page";
$route['cartpage'] = "test/cartpage";
$route['favourite'] = "test/favourite";
/* End of file routes.php */
/* Location: ./application/config/routes.php */