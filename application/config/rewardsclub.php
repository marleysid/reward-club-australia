<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//for image paths
//$config['site_title'] = "Rewards Club";
//$config['assets_path'] = 'assets/';
//$config['company_logo_path'] = $config['assets_path'] . 'uploads/company_logo/';
//$config['product_image_path'] = $config['assets_path'] . 'uploads/product/';
//$config['ad_image_path'] = $config['assets_path'] . 'uploads/ad/';
//$config['club_image_path'] = $config['assets_path'] . 'uploads/club/';

$config['site_title'] = "Rewards Club";
$config['assets_path'] = 'assets/';
$config['offer_image_path'] = $config['assets_path'] . 'uploads/offer_image/';
$config['deal_image_path'] = $config['assets_path'] . 'uploads/deal_image/';
$config['ad_image_path'] = $config['assets_path'] . 'uploads/ad/';
$config['club_image_path'] = $config['assets_path'] . 'uploads/club/';
$config['category_image_path'] = $config['assets_path'] . 'uploads/category/';
$config['entertainment_image_path'] = $config['assets_path'] . 'uploads/entertainment/';
$config['facilities_image_path'] = $config['assets_path'] . 'uploads/facilities/';
$config['news_image_path'] = $config['assets_path'] . 'uploads/news/';
$config['cms_image_path'] = $config['assets_path'] . 'uploads/cms/';
$config['partners_image_path'] = $config['assets_path'] . 'uploads/partners/';
$config['material_image_path'] = $config['assets_path'] . 'uploads/promotional_materials/';

$config['offer_image_csv_path'] = $config['assets_path'] . 'uploads/temp_images/';


//for image root
//$config['assets_root'] = FCPATH . 'assets/';
//$config['company_logo_root'] = $config['assets_root'] . 'uploads/company_logo/';
//$config['product_image_root'] = $config['assets_root'] . 'uploads/product/';
//$config['ad_image_root'] = $config['assets_root'] . 'uploads/ad/';
//$config['club_image_root'] = $config['assets_root'] . 'uploads/club/';

$config['assets_root'] = FCPATH . 'assets/';
$config['offer_image_root'] = $config['assets_root'] . 'uploads/offer_image/';
$config['deal_image_root'] = $config['assets_root'] . 'uploads/deal_image/';
$config['ad_image_root'] = $config['assets_root'] . 'uploads/ad/';
$config['club_image_root'] = $config['assets_root'] . 'uploads/club/';
$config['category_image_root'] = $config['assets_root'] . 'uploads/category/';
$config['entertainment_image_root'] = $config['assets_root'] . 'uploads/entertainment/';
$config['facilities_image_root'] = $config['assets_root'] . 'uploads/facilities/';
$config['news_image_root'] = $config['assets_root'] . 'uploads/news/';
$config['cms_image_root'] = $config['assets_root'] . 'uploads/cms/';
$config['partners_image_root'] = $config['assets_root'] . 'uploads/partners/';
$config['material_image_root'] = $config['assets_root'] . 'uploads/promotional_materials/';

/**
 * other config
 */

//ad location
$config['ad_location'] = array(
							'1' => 'mobile',
							'2' => 'left_side',
                                                        '3' => 'right_side',
                                                        '4' => 'bottom'
						);

/* End of file rewardsclub.php */
/* Location: ./application/config/rewardsclub.php */
