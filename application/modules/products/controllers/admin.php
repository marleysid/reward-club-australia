<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * @property product_model $product_model 
 */
class Admin extends Admin_Controller 
{	
	function __construct() {
		parent::__construct();	
		$this->load->model('product_model');
		$this->load->helper(array('text','image'));
		$this->data['logo_error'] = '';
		
	}
	
	public function _remap($method, $params )
	{
		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		} else 
			return $this->index($method);
		
	}

	public function index() 
	{		
		$this->data['products'] = $this->product_model->get_all();
		
        Template::render('admin/index', $this->data);
	
	}
	
	function add() 
	{		
		if($this->input->post()) {
			$this->_add_edit_product(0);
		}
		
		$this->data['edit'] = FALSE;
		
		$this->load->model(array('category/category_model'));
		$this->data['categories'] = render_select($this->category_model->get_all(), 'category_id', set_value('category_id',$this->data['edit'] ? $this->data['company_detail']->category_id : FALSE), 'class="form-control" ', 'category_id', 'category_name');
					
		Template::add_css( base_url()."assets/lib/jasny/css/jasny-bootstrap.min.css");
		Template::add_js( base_url()."assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
		
		Template::render('admin/form', $this->data);
		
	}
	
	function edit($id = 0) 
	{
		$id = (int) $id;
		
		$id || redirect('admin/products');
		
		$this->data['product_detail'] = $this->product_model->get($id);
		$this->data['product_detail'] || redirect('admin/product/');
		$this->data['edit'] = TRUE;
		
		if($this->input->post()) {
			$this->_add_edit_product($id);
		}
		
		$this->load->model(array('category/category_model'));
		$this->data['categories'] = render_select($this->category_model->get_all(), 'category_id', set_value('category_id',$this->data['edit'] ? $this->data['product_detail']->category_id : FALSE), 'class="form-control" ', 'category_id', 'category_name');
				
		Template::add_css( base_url()."assets/lib/jasny/css/jasny-bootstrap.min.css");
		Template::add_js( base_url()."assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
		
		Template::render('admin/form', $this->data);
		
	}
	
	function _add_edit_product($id) 
	{
        $this->load->library('form_validation');
//		$this->form_validation->CI =& $this;
		$data = $this->input->post();
//		debug($data);die;
        $this->form_validation->set_rules(
                    array(
                        array('field'=>'product_name', 'label'=>'Name', 'rules'=>'required|trim|min_length[2]|xss_clean'),
                        array('field'=>'product_price', 'label'=>'Product Price ', 'rules'=>'required|numeric|trim|xss_clean'),
                        array('field'=>'category_id', 'label'=>'Category', 'rules'=>'trim|xss_clean'),
                        array('field'=>'product_details', 'label'=>'Details', 'rules'=>'trim|xss_clean'),
                        array('field'=>'product_status', 'label'=>'Status', 'rules'=>'trim|xss_clean'),
                    )
                );
		
        $this->form_validation->set_error_delimiters('<span class="help-block">','</span>');
        
		if ($this->form_validation->run($this) === FALSE)
		{
			return FALSE;
		}
		
		$product_image_path = NULL;
		if($this->input->post('product_image_path') and $data['product_image_path']['name']) {
			if($result = upload_image('product_image_path', config_item('product_image_root'),FALSE)) {				
				$image_path = config_item('product_image_root').$result;
//				list($width, $height, $imgtype, $attr) = getimagesize($image_path);
////				resize of file uploaded
//				if($width > 800 || $height > 600) {
//					create_thumb($image_path,$image_path, array('w'=>'800','h'=>'600'), TRUE);
//				}
				$product_image_path = $result;
			} else {
				$this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">','</span>');
				return FALSE;
			}
		}
		
		unset($data['product_image_path']);
		
		!$product_image_path || ($data['product_image_path'] = $product_image_path);
		
		$data['product_status'] = $this->input->post('product_status') ? '1' : '0';
		
		if ($id == 0)
		{
			$this->product_model->insert($data);
			$this->session->set_flashdata('success_message','Data inserted successfully.');
			
		}
		else
		{
			$old_image = NULL;
			if($product_image_path) {
				$old_image = $this->product_model->get($id)->product_image_path;
			}
			$this->product_model->update($id, $data);
			if($old_image and file_exists(config_item('product_image_root').$old_image))	unlink_file(config_item('product_image_root').$old_image);
			
			$this->session->set_flashdata('success_message','Data updated successfully.');
						
		}
		
		redirect('admin/products/','refresh');
		
	}
	
	function orders()
	{
		$this->load->model('order_model');
		$this->data['orders'] = $this->order_model->order_by('order_date','desc')->get_all();
		
		Template::render('admin/orders', $this->data);
		
	}
	
	function order($order_id = NULL)
	{
		$this->load->model('order_model');
		
		$order_id || redirect('admin/products/orders');
		
		($this->data['order_detail'] = $this->order_model->get($order_id)) || redirect('admin/products/orders');
		if($this->input->post('order_status_submit')) {
			$order_status = ($this->input->post('order_status') == 'Complete') ? 'Complete' : 'Process' ;
			$this->order_model->update($order_id,array('order_status'=>$order_status));
			
			$this->session->set_flashdata('success_message','Order Status updated.');			
			redirect(current_url());
			
			
		}
		$this->data['products'] = $this->order_model->get_ordered_products($order_id);
		
        Template::render('admin/order_detail', $this->data);
		
	}
	
	function toggle_status() 
	{
		if($this->input->is_ajax_request()) {
			$id =  $this->input->post('id');
			$status =  $this->input->post('status') == 'true' ? '1' : '0' ;
			$this->product_model->update($id, array('product_status'=>$status));
			
			$this->output->set_content_type('application/json')->set_output(json_encode(array('status'=>'ok')));
		}
	}
	
	function delete($id) 
	{
		$old_image = $this->product_model->get($id)->product_image_path;
		
		if($this->product_model->delete($id)) {
			if($old_image and file_exists(config_item('product_image_root').$old_image))	unlink_file(config_item('product_image_root').$old_image);
			$this->session->set_flashdata('success_message','Data deleted successfully.');			
		} else {
			
			$this->session->set_flashdata('error_message','Data deletion failed.');			
		}
		redirect('admin/products/','refresh');
	}
}
