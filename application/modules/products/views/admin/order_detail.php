<div class="col-lg-12">
	<br>
	<a href="<?php echo base_url('admin/products/orders'); ?>" class="btn btn-default pull-right"><i class=' fa fa-arrow-left'></i> Back</a>
<div class="row">
<div class="col-lg-12">
	<div class="box">
	<header>
		<h5>Order Detail</h5>
	</header>
	<div class="body">
<div class="row">
	<div class="col-lg-6">
		  <fieldset>
			  <legend>Details</legend>
	<dl class="dl-horizontal">
      <dt>Order Id</dt>
      <dd><?php echo $order_detail->order_id; ?></dd>
      <dt>Order Date </dt>
      <dd><?php echo $order_detail->order_date; ?></dd>
      <dt>Order Status</dt>
      <dd><?php echo $order_detail->order_status; ?></dd>
	  <dt>Payment Status</dt>
      <dd><?php echo $order_detail->txnState; ?></dd>
      <dt>Transaction Id </dt>
      <dd><?php echo $order_detail->txnId; ?></dd>
	  <br>
      <dt>Customer's Name </dt>
      <dd><?php echo $order_detail->name; ?></dd>
      <dt>Email </dt>
      <dd><?php echo $order_detail->email_address; ?></dd>
      <dt>Contact Number </dt>
      <dd><?php echo $order_detail->contact_number; ?></dd>
      <dt>Delivery Address  </dt>
      <dd><?php echo nl2br($order_detail->delivery_address); ?></dd>
    </dl></fieldset>
	</div>
	<div class="col-lg-3">		
		<form role="form" action="" method="POST">
		  <fieldset>
			  <legend>Change Order Status</legend>
			<div class="form-group">
			  <label for="order_status">Order Status</label>
			  <select name="order_status" id="order_status" class="form-control ">
				  <option  <?php echo $order_detail->order_status == 'Process' ?'selected="selected"':''; ?> >Process </option>
				<option  <?php echo $order_detail->order_status == 'Complete' ?'selected="selected"':''; ?>>Complete </option>
			  </select>			  
			</div>
			<button type="submit" name='order_status_submit' value='true' class="btn btn-primary">Save Order Status</button>
		  </fieldset>
		</form>
	</div>
	</div>
	</div>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12">
	<div class="box">
	<header>
		<h5>Products Ordered</h5>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="">
		  <thead>
			<tr>
				<th style="width:30px;" >#</th>
				<th>Product Name</th>
				<th>Price</th>
				<th style="width:85px;" >Quantity</th>
				<!--<th style="width:90px;" >Settings</th>-->
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all">
			<?php if($products): ?>
				<?php foreach($products as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->product_name; ?></td>
				<td><?php echo $val->product_price; ?></td>
				<td><?php echo $val->quantity; ?></td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>		
	</div>
</div>
</div>
</div>
</div>