<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage Entertainment</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Entertainment Title</th>
				<th>Description Title</th>
				<th> Image</th>
				<th>Description </th>
				<th style="width:85px;" >Status</th>
				<th style="width:90px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all" >
			<?php if($entertainment): ?>
				<?php foreach($entertainment as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->entertainment_title; ?></td>
				<td><?php echo $val->entertainment_hero_title; ?></td>
				<td><?php if(file_exists(config_item('entertainment_image_path').$val->entertainment_image) and $val->entertainment_image!=null): ?><a title="<?php echo $val->entertainment_title; ?>" href="<?php echo base_url(config_item('entertainment_image_path').$val->entertainment_image); ?>" class="img-popup"><img class="img-thumbnail" src="<?php echo imager(config_item('entertainment_image_path').$val->entertainment_image,80, 80); ?>" /></a><?php endif; ?></td>
				<td><?php echo $val->entertainment_desc; ?></td>
				<td class="center"><input type="checkbox"  <?php echo $val->entertainment_status ? "checked": ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('club_admin/entertainment/toggle_status/'); ?>" data-id="<?php echo $val->entertainment_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
                                <td class="center">
					<a href="<?php echo base_url('club_admin/entertainment/edit/'.$val->entertainment_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
					<a href="<?php echo base_url('club_admin/entertainment/delete/'.$val->entertainment_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
					
                                </td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>
