<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Facilities_model extends MY_Model {

    public $_search_total = 0;
    public $_order_by = 0;

    public function __construct() {
        parent::__construct();
        $this->table = 'club_facilities';
        $this->field_prefix = 'facilities_';
        $this->log_user = FALSE;
    }

    function facilities_detail($id) {
        $this->db->select('*');
        $this->db->from('club_facilities');
        $this->db->where("club_id = '$id'");
        $this->db->where("is_deleted = '0'");
        $query = $this->db->get();
        return $query->result();
    }

    function delete_facilities($id) {
        return $query = $this->db->update('club_facilities', array('is_deleted' => '1'), array('facilities_id' => $id));
    }

    function contact_detail($id) {
        $this->db->select('contact_info_website,contact_info_phone_number');
        $this->db->from('contact_info');
        $this->db->where("contact_info_type_id = '$id'");
        $query = $this->db->get();
        return $query->row();
    }

}
