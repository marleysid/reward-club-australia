<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * @property Region_model $region_model 
 * @property General_model $general_model
 */
class Admin extends Admin_Controller 
{	
	function __construct() {
		parent::__construct();	
		$this->load->model('region_model');
		
	}
	
	public function _remap($method, $params )
	{
		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		} else 
			return $this->index($method);
		
	}

	public function index($id = 0) 
	{
		$id = (int) $id;
		$this->load->model('general_model');
		$this->data['edit'] = FALSE;
		if($id) {
			$this->data['region_detail'] = $this->region_model->get($id);
			$this->data['region_detail'] || redirect('admin/region/');
			$this->data['edit'] = TRUE;
		}
		if($this->input->post()) {
			$this->_add_edit_region($id);
		}
		$this->_manage_regions();
		
        Template::render('admin/index', $this->data);
	
	}
	
	function _manage_regions()
	{
		$this->data['regions'] = $this->region_model->get_all();
		
	}
	
	function _add_edit_region($id) 
	{
        $this->load->library('form_validation');
		
		$data = $this->input->post();
		
        $this->form_validation->set_rules(
                    array(
                        array('field'=>'region_name', 'label'=>'Region Name', 'rules'=>'required|trim|min_length[2]|xss_clean'),
                                          )
                );
		
        $this->form_validation->set_error_delimiters('<span class="help-block">','</span>');
        
		if ($this->form_validation->run($this) === FALSE)
		{
			return FALSE;
		}
		
		$data['region_status'] = $this->input->post('region_status') ? '1' : '0';
		;
//		$state = @$this->general_model->getById('states', 'id', $data['state_id'] , 'state')->state;
//		
//		$location_info_data = json_decode(file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?address=".urlencode("{$data['region_name']}, {$state}, australia")."&sensor=false"));
//
//		$data['lat'] = @($location_info_data->results[0]->geometry->location->lat)?:'0';
//		$data['lon'] = @($location_info_data->results[0]->geometry->location->lng)?:'0';
		
		if ($id == 0)
		{
			$this->region_model->insert($data);
			$this->session->set_flashdata('success_message','Data inserted successfully.');
			
		}
		else
		{
			$this->region_model->update($id, $data);
			$this->session->set_flashdata('success_message','Data updated successfully.');
						
		}
		
		redirect('admin/region/','refresh');
		
	}
	
	function toggle_status() 
	{
		if($this->input->is_ajax_request()) {
			$id =  $this->input->post('id');
			$status =  $this->input->post('status') == 'true' ? '1' : '0' ;
			$this->region_model->update($id, array('region_status'=>$status));
			
			$this->output->set_content_type('application/json')->set_output(json_encode(array('status'=>'ok')));
		}
	}
	
	function delete($id) 
	{
		if($this->region_model->delete($id)) {
			$this->session->set_flashdata('success_message','Data deleted successfully.');			
		} else {
			$this->session->set_flashdata('error_message','Data deletion failed.');			
		}
		redirect('admin/region/','refresh');
	}
	
}
