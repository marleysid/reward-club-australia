<?php

class Admin extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        show_error('No Service');
    }

    function elfinder_init()
    {
        $this->load->helper('path');
        $opts = array(
                'roots' => array(
                        array(
                                'driver' => 'LocalFileSystem',
                                'path' => FCPATH.'assets/uploads/cms', // path to files (REQUIRED)
                                'URL' => base_url().'assets/uploads/cms', // URL to files (REQUIRED),
                                'alias' => 'Images',
                        // more elFinder options here
                        ),
                )
        );
        $this->load->library('media/elfinder_lib', $opts);
    }

    function elfinder_csv_temp_image_init()
    {
        $this->load->helper('path');
        $opts = array(
                'debug' => true,
                'roots' => array(
                        array(
                                'driver' => 'LocalFileSystem',
                                'path' => FCPATH.'assets/uploads/temp_images', // path to files (REQUIRED)
                                'URL' => base_url().'assets/uploads/temp_images/', // URL to files (REQUIRED),
                                'alias' => 'Temp offer image',
                                'uploadAllow' => array('image'),
                                'uploadOrder' => array('allow', 'deny'),
                                'accessControl' => 'access',
                                'mimeDetect' => 'internal',
                                'imgLib' => 'gd',
                        // more elFinder options here
                        ),
                )
        );
        $this->load->library('media/elfinder_lib', $opts);
//        $this->load->helper('media/elfinder_lib');
    }

    function elfinder_csv_temp_deal_image_init()
    {
        $this->load->helper('path');
        $opts = array(
                // 'debug' => true, 
                'roots' => array(
                        array(
                                'driver' => 'LocalFileSystem',
                                'path' => FCPATH.'assets/uploads/temp_deal_images', // path to files (REQUIRED)
                                'URL' => base_url().'assets/uploads/temp_deal_images/', // URL to files (REQUIRED),
                                'alias' => 'Temp deal image',
                                'uploadAllow' => array('image'),
                                'uploadOrder' => array('allow', 'deny'),
                                'accessControl' => 'access',
                                'mimeDetect' => 'internal',
                                'imgLib' => 'gd',
                        // more elFinder options here
                        ),
                )
        );
        $this->load->library('media/elfinder_lib', $opts);
    }

    function manage_media($type = 'offer')
    {

        //echo FCPATH;die();
        $this->load->helper('media/elfinder');
        $this->data['type'] = $type;
        Template::set('title', 'Import csv');
        Template::set_base_title('Import csv');
        Template::render('admin/index', $this->data);
    }

    function editor_browser()
    {
//        $this->load->helper('media/elfinder');
        $this->load->view('elfinder_editor_browser');
    }

}
