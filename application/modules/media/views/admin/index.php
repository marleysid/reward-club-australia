<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css">
		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/lib/jquery-ui.js');?>"></script>

<!-- elFinder CSS (REQUIRED) -->
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/lib/elfinder/css/elfinder.min.css');?>">
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo base_url('assets/lib/elfinder/css/theme.css');?>">

<!-- elFinder JS (REQUIRED) -->
<script type="text/javascript" src="<?php echo base_url('assets/lib/elfinder/js/elfinder.min.js');?>"></script>

<!-- elFinder translation (OPTIONAL) -->
<script type="text/javascript" src="<?php echo base_url('assets/lib/elfinder/js/i18n/elfinder.ru.js');?>"></script>
<style>
    .elfinder div{ -webkit-box-sizing: initial !important ;box-sizing: initial !important;}
</style>
<script type="text/javascript" charset="utf-8">
         $().ready(function() {
 
            $('#finder').elfinder({
 
                 url : '<?php echo base_url('admin/media/'.($type == 'offer'? 'elfinder_csv_temp_image_init' : 'elfinder_csv_temp_deal_image_init'))?>',
 
             })

//             $('#close,#open,#dock,#undock').click(function() {
//                 $('#finder').elfinder($(this).attr('id'));
//             })
 
         })
</script>
<div class="col-lg-12">
    <div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5>Manage Temp Files</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>

    </header>
        
    <div class="body">
	<div id="finder" >finder</div>
    </div>
    </div>
</div>
     