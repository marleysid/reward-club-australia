<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('faq_model');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['faq'] = $this->faq_model->faq_detail();

        Template::render('admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_faq(0);
        }

        $this->data['edit'] = FALSE;

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);

        Template::render('admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        //  $id || redirect('admin/faq');

        $this->data['faq_detail'] = $this->faq_model->get($id);

        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_faq($id);
        }



        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);

        Template::render('admin/form', $this->data);
    }

    function _add_edit_faq($id) {

        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;

        $data = $this->input->post();
        $this->form_validation->set_rules(
                array(
                    array('field' => 'faq_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'faq_desc', 'label' => 'Short Description', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                  
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }

    
     if ($id == 0) {
            $this->faq_model->insert($data);
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
           
            $this->faq_model->update($id, $data);
           
            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/faq/', 'refresh');
    }

   
    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->offer_model->update($id, array('offer_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    
    function delete($id) {
       if ($this->faq_model->delete_faq($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/faq/', 'refresh');
    }

   
    
}
