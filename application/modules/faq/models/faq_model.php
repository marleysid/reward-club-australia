<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Faq_model extends MY_Model {

   
    public function __construct() {
        parent::__construct();
        $this->table = 'faq';
        $this->field_prefix = 'faq_';
        $this->log_user = FALSE;
    }
    
    function faq_detail() {
         $this->db->select('*');
        $this->db->from('faq');
        $this->db->where("is_deleted = '0'");
        $query = $this->db->get();
        return $query->result();
    }
     
    function delete_faq($id){
         return $query = $this->db->update('faq', array('is_deleted' => '1'), array('faq_id' => $id));

    }

}
