<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification_offer_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'notification_offer';
        $this->log_user = FALSE;
    }

    function get_regions() {
        $query = $this->db->select("offers.*,notification_offer.*")
            ->from("offers")
            ->join("notification_offer", "offers.offer_id = notification_offer.offer_id")
            ->get();

        return $query->result();
            }

    function get_all_offer()
    {
        $query = $this->db->select("offers.*,notification_offer.*")
            ->from("offers")
            ->join("notification_offer", "offers.offer_id = notification_offer.offer_id")
            ->get();

        return $query->result();


    }
    function get_offer_by_id($id)
    {

        $query = $this->db->select("offers.*,notification_offer.*")
            ->from("offers")
            ->join("notification_offer", "offers.offer_id = notification_offer.offer_id")
            ->where("notification_offer.id = $id")
            ->get();

        return $query->row();


    }
    function get_offers(){
        $query = $this->db->select('*')
            ->from('offers')
            ->get();
        return $query->result();
    }





}
