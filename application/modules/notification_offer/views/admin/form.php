<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>
		<h5><?php echo $edit ? 'Edit' : 'Add'; ?> Notification</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<form class="form-horizontal" method="POST">

			<div class="form-group <?php echo form_error('offer_id')?'has-error':''?>">
				<label for="key" class="control-label col-lg-4">Offers</label>
				<div class="col-lg-8">
					<select class="input-sm" name="offer_id"  <?php echo $edit ? 'disabled' : '' ?>>
						<?php if(($notification_details) && (!isset($noti_detail))) : ?>
							<?php foreach($notification_details as $key1): ?>

								<option value="<?php echo $key1->offer_id?>"><?php echo $key1->offer_title ?></option>
							<?php endforeach ?>
						<?php else: ?>
							<option value="<?php echo $noti_detail->offer_id?>"><?php echo $noti_detail->offer_title ?></option>
						<?php endif;?>
						<?php echo form_error('offer_id'); ?>
					</select>
				</div>

			</div>

			<div class="form-group <?php echo form_error('notification') ? 'has-error' : '' ?>">
				<label for="faq_title" class="control-label col-lg-4">notification</label>
				<div class="col-lg-8">
					<textarea type="text" id="notification" placeholder="notification" name="notification" class="form-control"  ><?php echo set_value("offer_address", $edit ? $noti_detail->notification : ''); ?></textarea>

					<?php echo form_error('notification'); ?>
				</div>
			</div>
		<!-- /.form-group -->
		  <div class="form-group">
			  <div class="col-sm-offset-4 col-sm-9"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
				  <?php echo $edit ? anchor('admin/notification_offer', 'Cancel', 'class="btn btn-warning"'):''; ?>
			  </div>
		  </div>
		</form>		
	</div>
</div>