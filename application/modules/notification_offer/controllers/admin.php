<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * @property Region_model $region_model 
 * @property General_model $general_model
 */
class Admin extends Admin_Controller 
{	
	function __construct() {
		parent::__construct();	
		$this->load->model('notification_offer_model');
		
	}
	
	public function _remap($method, $params )
	{
		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		} else 
			return $this->index($method);
		
	}

	public function index($id = 0) 
	{

		$id = (int) $id;

		$this->data['edit'] = FALSE;
		if($id) {
			$this->data['noti_detail'] = $this->notification_offer_model->get_offer_by_id($id);
			$this->data['noti_detail'] || redirect('admin/notification_offer/');
			$this->data['edit'] = TRUE;
		}
		if($this->input->post()) {
			$this->_add_edit_region($id);
		}
		$this->_manage_notification();
		$this->data['notification_details'] = $this->notification_offer_model->get_offers();
		
        Template::render('admin/index', $this->data);
	
	}
	
	function _manage_notification()
	{
		$this->data['notification'] = $this->notification_offer_model->get_all_offer();

		
	}
	
	function _add_edit_region($id) 
	{
        $this->load->library('form_validation');


				$data = $this->input->post();

				$this->form_validation->set_rules(
							array(
								array('field'=>'notification', 'label'=>'Notification', 'rules'=>'required|trim|min_length[2]|xss_clean')


												  )
						);

				$this->form_validation->set_error_delimiters('<span class="help-block">','</span>');

				if ($this->form_validation->run($this) === FALSE)
				{
					return FALSE;
				}

				if ($id == 0)
				{
					$this->notification_offer_model->insert($data);
					$this->session->set_flashdata('success_message','Data inserted successfully.');

				}
				else
				{
					$this->notification_offer_model->update($id, $data);
					$this->session->set_flashdata('success_message','Data updated successfully.');

				}

		
		redirect('admin/notification_offer/','refresh');
//		Template::render('admin/notification_offer', $this->data);
		
	}
	
	function toggle_status() 
	{
		if($this->input->is_ajax_request()) {
			$id =  $this->input->post('id');
			$status =  $this->input->post('status') == 'true' ? '1' : '0' ;
			$this->notification_offer_model->update($id, array('region_status'=>$status));
			
			$this->output->set_content_type('application/json')->set_output(json_encode(array('status'=>'ok')));
		}
	}
	
	function delete($id) 
	{
		if($this->notification_offer_model->delete($id)) {
			$this->session->set_flashdata('success_message','Data deleted successfully.');			
		} else {
			$this->session->set_flashdata('error_message','Data deletion failed.');			
		}
		redirect('admin/notification_offer/','refresh');
	}
	
}
