<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Club_model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->table        = 'clubs';
        $this->field_prefix = 'club_';
        $this->log_user     = false;
    }

    public function get_all_clubs($id)
    {
        $query = $this->db->select('*')
            ->from('clubs')
            ->where("generaluser_id_join = '$id'")
            ->get();

        return $query->result();
    }

    public function get_all_clubs_all()
    {
        $query = $this->db->select('clubs.*,users.first_name,users.last_name')
            ->from('clubs')
            ->join('users','users.club_id = clubs.club_id')
            ->get();
        //echo $this->db->last_query(); exit;
        return $query->result();
    }

    public function search_clubs($limit, $offset, $state, $postcode, $suburb, $clubname, $filterBy)
    {
        $postcode = substr($postcode, 0, 5);
        if($offset)
            $offset = ",".$offset;
        else
            $offset = "";
        $query_search = '
                    SELECT SQL_CALC_FOUND_ROWS *,clubs.*,users.`first_name`,users.`last_name`
                    FROM (`clubs`)
                    JOIN `users` ON `users`.`club_id` = `clubs`.`club_id`
                    WHERE clubs.`club_suburb` LIKE "%'.$postcode.'%"
                    OR clubs.`club_address` LIKE "%'.$postcode.'%"
                    AND clubs.club_name LIKE "%'.$clubname.'%"
                    AND clubs.club_status != "0"
                    ORDER BY clubs.`club_name`
                    LIMIT '.$limit.$offset;

            $query = $this->db->query($query_search);
//echo $this->db->last_query(); exit;
        if ($query->num_rows() > 0) {

            return $query->result();
        } else {
            return false;
        }


    }

    public function count_clubs()
    {
        return $this->db->count_all("clubs");
    }

    public function club_url($id)
    {
        $query = $this->db->select("club_website")
            ->from("clubs")
            ->where("club_id = '$id'")
            ->get();
        return $query->row();
    }
    public function get_club_detail($id)
    {
        $query = $this->db->select('*')
            ->from('clubs')
            ->where("club_id = '$id'")
            ->get();

        return $query->row();
    }

    public function get_club_events($id)
    {
        $query = $this->db->select('*')
            ->from('event')
            ->where("club_user_id = '$id'")
            ->get();

        return $query->result();
    }
    public function get_club_facilities($id)
    {
        $query = $this->db->select('*')
            ->from('facility')
            ->where("club_user_id = '$id'")
            ->get();

        return $query->result();
    }
    public function get_club_features($id)
    {
        $query = $this->db->select('*')
            ->from('feature')
            ->where("club_user_id = '$id'")
            ->get();

        return $query->result();
    }
    public function get_club_contacts($id)
    {
        $query = $this->db->select('*')
            ->from('club_contacts')
            ->where("club_user_id = '$id'")
            ->get();

        return $query->row();
    }

    public function count_club_contact($id)
    {
        $this->db->select('*');
        $this->db->where("club_user_id = '$id'");
        $query = $this->db->get('club_contacts');
        $num   = $query->num_rows();
        return $num;
    }

    public function get_club_favourite($id)
    {
        $query = $this->db->select('*')
            ->from('club_favourite')
            ->where("user_id = '$id'")
            ->get();
        return $query->row();
    }
    public function get_favourites()
    {
        $this->db->select('*');
        $this->db->from('clubs');
        $this->db->join('club_favourite', 'clubs.club_id = club_favourite.club_id', 'inner');
        $query = $this->db->get();
        //echo $this->db->last_query(); exit;
        return $query->result();
    }
    public function get_local_clubs()
    {
        // $this->db->select('*');
        // $this->db->from('clubs');
        // $this->db->join('club_favourite', 'clubs.club_id = club_favourite.club_id', 'inner');
        // $query = $this->db->get();
        $lat = $_SESSION['latitude'];
      $lon = $_SESSION['longitude'];
      $distance = $_SESSION['distance'];
        $query_main = "SELECT clubs.*, 3956*2 * ASIN ( SQRT (POWER(SIN(($lat - `clubs`.`lat`)*pi()/180 / 2),2) + COS($lat * pi()/180) * COS(`clubs`.`lat` *pi()/180) * POWER(SIN(($lon - `clubs`.`lon`) *pi()/180 / 2), 2) ) ) as distance FROM `clubs` having distance <= $distance ";
        $query = $this->db->query($query_main);
        //echo $this->db->last_query(); exit;
        return $query->result();
    }
    public function insert_favourite($data)
    {
        $this->db->insert('club_favourite', $data);

    }
    public function get_club_list($user_id,$userclub_id)
    {
        $query = $this->db->select('*')
            ->from('clubs')
            ->where("club_id = '$userclub_id'")
            ->get();
        return $query->result();
    }
    public function importData($data)
    {
        $this->db->insert('clubs', $data);
    }

    public function get_offer_csv()
    {

        $this->db->select('*');
        $this->db->from('clubs');
        $query = $this->db->get();

        return $query->result();

    }
    public function get_all_clubs_desc_bck($user_id)
    {

            $notification = $this->db->select('notification_id')
                ->from('user_notification')
                ->where('general_user_id', $user_id)
                ->get();

            $result = $notification->result();

                foreach ($result as $res) {
                    $not[] = $res->notification_id;
                }
                    $this->db->select('notification.*,clubs.*');
                    $this->db->from('clubs');
                    $this->db->join('notification', 'clubs.club_id = notification.club_id', 'inner');
                    $this->db->order_by("notification.id", "desc");
                    if (!empty($not)) {
                        $this->db->where_not_in('notification.id', $not);
                    }
                    
                    $query = $this->db->get();

                return $query->result();



    }
    public function get_all_clubs_desc($user_id)
    {
        $notification = $this->db->select('notification_id')
            ->from('user_notification')
            ->where('general_user_id', $user_id)
            ->get();

        $result = $notification->result();


        foreach ($result as $res) {
            $not[] = $res->notification_id;
        }

        $this->db->select('notification_offer.*,offers.*,notification_unread.unread');
        $this->db->from('offers');
        $this->db->join('notification_offer', 'offers.offer_id = notification_offer.offer_id', 'inner');
        $this->db->join('notification_unread', 'notification_offer.id = notification_unread.notification_id','left');
        $this->db->order_by("notification_offer.id", "desc");
        if (!empty($not)) {
            $this->db->where_not_in('notification_offer.id', $not);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); exit;
        return $query->result();

    }
    public function get_all_clubs_desc_brk($user_id)
    {
        $notification = $this->db->select('notification_id')
            ->from('user_notification')
            ->where('general_user_id', $user_id)
            ->get();

        $result = $notification->result();


        foreach ($result as $res) {
            $not[] = $res->notification_id;
        }



        $this->db->select('notification_unread.unread,notification_id');
        $this->db->from('notification_unread');
        $this->db->join('notification_offer', 'notification_offer.offer_id = notification_unread.notification_id','inner');
        $this->db->where("notification_unread.general_user_id", "$user_id");

        $query = $this->db->get();
        // echo $this->db->last_query(); exit;
        $list = $query->result();


        $this->db->select('notification_offer.*,offers.*,notification_unread.unread');
        $this->db->from('offers');
        $this->db->join('notification_offer', 'offers.offer_id = notification_offer.offer_id', 'inner');
        $this->db->join('notification_unread', 'notification_offer.offer_id = notification_unread.notification_id','left');
        $this->db->order_by("notification_offer.id", "desc");
        if (!empty($not)) {
            $this->db->where_not_in('notification_offer.offer_id', $not);
        }
        $query = $this->db->get();
        // echo $this->db->last_query(); exit;
        return $query->result();

    }
    public function get_all_clubs_unread($user_id)
    {
//        $unread = $this->db->select('notification_unread.*')
//            ->from('notification_unread')
//            ->where('general_user_id', $user_id)
//            ->get();
//
//        $unread = $unread->result();
//
//        foreach ($unread as $ur) {
//            $unre[] = $ur->notification_id;
//        }

        $this->db->select('notification_unread.unread,notification_id');
        $this->db->from('notification_unread');
        $this->db->join('notification_offer', 'notification_offer.offer_id = notification_unread.notification_id','inner');
        $this->db->where("notification_unread.general_user_id", "$user_id");

        $query = $this->db->get();
        // echo $this->db->last_query(); exit;
        return $query->result();

//        echo $this->db->last_query(); exit;


    }
    public function get_clubs_for_members($user_id){
        $sql = 'SELECT * FROM clubs JOIN club_members_connection On `club_members_connection`.`club_id` = `clubs`.`club_id` where `club_members_connection`.`member_id` = ?';
        return $this->db->query($sql, array($user_id))->result();
    }

    public function get_club_logo()
    {
        $this->db->select('club_logo');
        $this->db->from('clubs');
        $query = $this->db->get();
        return $query->result();
    }



}
