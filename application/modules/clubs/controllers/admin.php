<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @property club_model $club_model
 */
class Admin extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('club_model', 'offers/offer_model'));
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params)
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else {
            return $this->index($method);
        }

    }

    public function index()
    {

        $this->data['clubs'] = $this->club_model->get_all();

        Template::render('admin/index', $this->data);
    }

    public function import_clubcsv_data()
    {

        if ($this->xls_upload()) {
            $this->load->library('my_phpexcel');
            $this->uploaded_xls_file = $this->xls_file_data['full_path'];
//          $inputFileType = 'Excel2007';
            $inputFileType = 'CSV';
//          $sheetname = 'Gyms';
            $inputFileName = $this->uploaded_xls_file;
            $objReader     = PHPExcel_IOFactory::createReader($inputFileType);

//              $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
            $objPHPExcel = $objReader->load($inputFileName);

            $worksheet = $objPHPExcel->getActiveSheet();

            $highestRow         = $worksheet->getHighestRow();
            $highestColumn      = $worksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            //get header name and informations

            $req_header = array(
                'club_code'
                , 'club_name'
                , 'club_address'
                , 'club_suburb'
                , 'club_postcode'
                , 'club_state'
                , 'club_website'
                , 'club_phone'
                , 'club_description'
                , 'club_home_image_path'
                , 'club_home_image_path2'
                , 'club_logo'
                , 'club_privacy_policy'
                , 'club_status'
                , 'fax',

            );

            $csv_header_title     = array();
            $csv_header_title_col = array();

            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, 1);
                $val  = $cell->getValue();
                if (!empty($val)) {
                    $csv_header_title[]                     = strtolower($val);
                    $csv_header_title_col[strtolower($val)] = $col;
                }
            }

            // dd(array_diff($req_header, $csv_header_title));
            if (!empty(array_diff($req_header, $csv_header_title))) {
                $this->session->set_flashdata('error_message', 'Error csv file.');
                redirect(current_url());
                // echo 'error csv file';
                // die();
            }

            $log_file = FCPATH . 'application/logs/' . uniqid() . '.txt';
            file_put_contents($log_file, PHP_EOL . 'Import Errors: ' . PHP_EOL);

            for ($row = 2; $row <= $highestRow; ++$row) {

                $insArr = array();

                //for id
                $insArr['club_id'] = isset($csv_header_title_col['club_id']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_id'], $row)->getValue()) : '';

                //for title
                $insArr['club_code'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_code'], $row)->getValue());

                $insArr['club_name']     = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_name'], $row)->getValue());
                $insArr['club_address']  = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_address'], $row)->getValue());
                $insArr['club_suburb']   = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_suburb'], $row)->getValue());
                $insArr['club_postcode'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_postcode'], $row)->getValue());
                $insArr['club_state']    = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_state'], $row)->getValue());
                $insArr['club_website']  = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_website'], $row)->getValue());
                //for category
                $insArr['club_phone']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_phone'], $row)->getValue());
                $insArr['club_description'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_description'], $row)->getValue());

                //for image
                $insArr['club_home_image_path'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_home_image_path'], $row)->getValue());

                //for short_description
                $insArr['club_home_image_path2'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_home_image_path2'], $row)->getValue());

                //for description
                $insArr['club_logo'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_logo'], $row)->getValue());

                //for promocodes
                $insArr['club_privacy_policy'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_privacy_policy'], $row)->getValue());
                $insArr['club_status']         = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['club_status'], $row)->getValue());
                 $insArr['fax']         = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['fax'], $row)->getValue());

                $this->error_csv = array();
                $this->_update_data($insArr);

                if (!empty($this->error_csv)) {

                    $data = PHP_EOL . ' Row ' . $row . ': ' . implode(PHP_EOL . "\t", $this->error_csv);
                    file_put_contents($log_file, $data, FILE_APPEND);
                    $this->session->set_flashdata('log_data', $log_file);
                }

                $insArr          = null;
                $this->error_csv = null;
                $data            = null;
            }

            unlink($this->uploaded_xls_file);
            $this->session->set_flashdata('success_message', 'Import Successfully Done.');
            redirect(current_url());
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);

        Template::set('title', 'Import csv');
        Template::set_base_title('Import csv');
        Template::render('admin/import_csv_form', $this->data);
    }

    public function _update_data($data)
    {
        
        if (!empty($data)) {
            //for image 1
            if (!empty($data['club_home_image_path']) and file_exists(FCPATH . 'assets/uploads/temp_images/' . $data['club_home_image_path'])) {

                $data['club_home_image_path'] = move_file(FCPATH . 'assets/uploads/temp_images/', config_item('club_image_root'),$data['club_home_image_path']);
            } else {
                $data['club_home_image_path'] = '';
            }
            //Image 2
            //echo FCPATH . 'assets/uploads/temp_images/' . $data['club_home_image_path2']; exit;
            if (!empty($data['club_home_image_path2']) and file_exists(FCPATH . 'assets/uploads/temp_images/' . $data['club_home_image_path2'])) {
                
                $data['club_home_image_path2'] = move_file(FCPATH . 'assets/uploads/temp_images/', config_item('club_image_root'),$data['club_home_image_path2']);
            } else {
                $data['club_home_image_path2'] = '';
            }
            //Image 3
            if (!empty($data['club_logo']) and file_exists(FCPATH . 'assets/uploads/temp_images/' . $data['club_logo'])) {
                
                $data['club_logo'] = move_file(FCPATH . 'assets/uploads/temp_images/', config_item('club_image_root'),$data['club_logo']);
            } else {
                $data['club_logo'] = '';
            }
            
            $this->club_model->importData($data);
        }
    }

    public function xls_upload()
    {
        $config['upload_path']   = './assets/uploads/csv';
        $config['allowed_types'] = 'csv';
        $config['max_size']      = '1024';

        $this->load->library('upload', $config);
        //print_r($_FILES);
        if (!$this->upload->do_upload()) {
            //echo $this->upload->display_errors(); exit;
            return false;
        } else {
            $this->xls_file_data = $this->upload->data();
            //debug($this->xls_file_data);
            return true;
        }
    }

    public function add()
    {

        if ($this->input->post()) {

            $this->_add_edit_club(0);
        }
        $this->data['suburb'] = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());


        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());

        $this->data['state'] = array_map(function ($v) {
            return $v->state;
        }, $this->offer_model->get_state());
        // debug($this->offer_model->get_suburb());die;
        $this->data['edit'] = false;

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", true);
        Template::add_js(base_url() . "assets/js/autocomplete.js", true);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", true);

        Template::render('admin/form', $this->data);
    }

    public function edit($id = 0)
    {
        $id = (int) $id;

        $id || redirect('admin/clubs');

        $this->data['club_detail'] = $this->club_model->get($id);
        $this->data['suburb']      = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());
        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());

        $this->data['state'] = array_map(function ($v) {
            return $v->state;
        }, $this->offer_model->get_state());

        $this->data['club_detail'] || redirect('admin/club/');
        $this->data['edit'] = true;

        if ($this->input->post()) {
            $this->_add_edit_club($id);
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", true);
        Template::add_js(base_url() . "assets/js/autocomplete.js", true);
        Template::add_js(base_url() . "assets/js/chosen.jquery.min.js", true);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", true);

        Template::render('admin/form', $this->data);
    }

    public function _add_edit_club($id)
    {
        $this->load->library('form_validation');
        $data = $this->input->post();

        $latng = get_lat_long_generic($this->input->post('club_suburb'));

        $data['lat'] = $latng["lat"];
        $data['lon'] = $latng["long"];

        $this->form_validation->set_rules(
            array(
                array('field' => 'club_name', 'label' => 'Name', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                array('field' => 'club_address', 'label' => 'Street Address', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                array('field' => 'club_suburb', 'label' => 'Suburb', 'rules' => 'required'),
                array('field' => 'club_postcode', 'label' => 'PostCode', 'rules' => 'required'),
                array('field' => 'club_state', 'label' => 'State', 'rules' => 'required'),
                array('field' => 'club_privacy_policy', 'label' => 'Privacy Policy', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                array('field' => 'club_description', 'label' => 'club description', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                array('field' => 'club_website', 'label' => 'Website', 'rules' => 'required|prep_url'),
                array('field' => 'club_phone', 'label' => 'Phone', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                array('field' => 'fax', 'label' => 'Fax', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                array('field' => 'club_status', 'label' => 'Status', 'rules' => 'trim|xss_clean'),
            )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === false) {
            return false;
        }
        $club_image_path = null;
        if (isset($_FILES['club_home_image_path']['name']) and $_FILES['club_home_image_path']['name'] != '') {
            if ($result = upload_image('club_home_image_path', config_item('club_image_root'), false)) {
                $image_path = config_item('club_image_path') . $result;

                $club_image_path = config_item('club_image_path') . $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return false;
            }
        } elseif ($id == 0) {

            $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
            return false;
        }

        unset($data['club_home_image_path']);
        !$club_image_path || ($data['club_home_image_path'] = $club_image_path);

        $club_image_path2 = null;
        if (isset($_FILES['club_home_image_path2']['name']) and $_FILES['club_home_image_path2']['name'] != '') {
            if ($result = upload_image('club_home_image_path2', config_item('club_image_root'), false)) {
                $image_path = config_item('club_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
                //                resize of file uploaded
                //                if ($width > 800 || $height > 600) {
                //                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'));
                //                }
                $club_image_path2 = config_item('club_image_path') . $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return false;
            }
        } elseif ($id == 0) {

            $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
            return false;
        }

        unset($data['club_home_image_path2']);
        !$club_image_path2 || ($data['club_home_image_path2'] = $club_image_path2);

        $club_logo = null;
        if (isset($_FILES['club_logo']['name']) and $_FILES['club_logo']['name'] != '') {
            if ($result = upload_image('club_logo', config_item('club_image_root'), false)) {
                $image_path = config_item('club_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
                ////                resize of file uploaded
                //                if ($width > 800 || $height > 600) {
                //                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'));
                //                }
                $club_logo = config_item('club_image_path') . $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return false;
            }
        } elseif ($id == 0) {

            $this->data['logo_error'] = '<span class="help-block">Logo is required</span>';
            return false;
        }

        unset($data['club_logo']);
        !$club_logo || ($data['club_logo'] = $club_logo);

        $data['club_status'] = $this->input->post('club_status') ? '1' : '0';

        if ($id == 0) {
            $data['club_code'] = $this->generate_clubcode();
            $latlon = get_lat_long_generic($data['club_suburb']);
            $data['lat'] = $latlon['lat'];
            $data['lon'] = $latlon['long'];
            
            $this->club_model->insert($data);
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_image  = null;
            $old_logo   = null;
            $old_image2 = null;
            if ($club_image_path) {
                $old_image = $this->club_model->get($id)->club_home_image_path;
            }
            if ($club_logo) {
                $old_logo = $this->club_model->get($id)->club_logo;
            }
            if ($old_image2) {
                $old_logo = $this->club_model->get($id)->club_home_image_path2;
            }
            // debug($this->club_model->get($id)->club_home_image_path);die;

            $latlon = get_lat_long_generic($data['club_suburb']);
            $data['lat'] = $latlon['lat'];
            $data['lon'] = $latlon['long'];
            
            $this->club_model->update($id, $data);
            if ($old_image and file_exists($old_image)) {
                unlink_file($old_image);
            }

            if ($old_logo and file_exists($old_logo)) {
                unlink_file($old_logo);
            }

            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/clubs/', 'refresh');
    }

    public function generate_clubcode()
    {
        $random_no = '';
        for ($i = 0; $i < 5; $i++) {
            $random_no .= rand(8, 25);
        }
        $query = $this->db->query("select * from clubs where club_code = '$random_no'");
        if ($query->num_rows() > 0) {
            $query = $query->row();

            $random_no = $this->generate_clubcode();
        }
        return $random_no;
    }

    public function toggle_status()
    {
        if ($this->input->is_ajax_request()) {
            $id     = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->club_model->update($id, array('club_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    public function delete($id)
    {
        if ($this->club_model->delete($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/clubs/', 'refresh');
    }

    public function _check_suburb($str)
    {
        if (!empty($_POST['club_suburb']) && !empty($str) && $str && $str != 0) {

            return true;
        } else {
            $this->form_validation->set_message('_check_suburb', 'Please select %s from the suggestion.');
            return false;
        }
    }

    public function offer_csv()
    {
        $this->load->library('my_phpexcel');

        $sheet = $this->my_phpexcel->getActiveSheet();
        $this->my_phpexcel->setActiveSheetIndex(0);
        $this->my_phpexcel->getProperties()
            ->setCreator("Report")
            ->setLastModifiedBy("ADMIN")
            ->setTitle('Report')
            ->setSubject('Report')
            ->setDescription('Report')
            ->setKeywords("")
            ->setCategory("");

//        echo "<pre>";
        //        print_r($offer_arr);

        $this->my_phpexcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'club_id')
            ->setCellValue('B1', 'club_code')
            ->setCellValue('C1', 'club_name')
            ->setCellValue('D1', 'club_address')
            ->setCellValue('E1', 'club_suburb')
            ->setCellValue('F1', 'club_postcode')
            ->setCellValue('G1', 'club_state')
            ->setCellValue('H1', 'club_website')
            ->setCellValue('I1', 'club_phone')
            ->setCellValue('J1', 'club_description')
            ->setCellValue('K1', 'club_home_image_path')
            ->setCellValue('L1', 'club_home_image_path2')
            ->setCellValue('M1', 'club_logo')
            ->setCellValue('N1', 'club_privacy_policy')
            ->setCellValue('O1', 'club_status')
            ->setCellValue('P1', 'fax');
        $offer_details = $this->club_model->get_offer_csv();

        if (!empty($offer_details)) {
            $offer_arr = array();
            foreach ($offer_details as $offer_detail) {
                $offer_arr[] = array(
                    'club_id'               => $offer_detail->club_id,
                    'club_code'             => $offer_detail->club_code,
                    'club_name'             => $offer_detail->club_name,
                    'club_address'          => $offer_detail->club_address,
                    'club_suburb'           => $offer_detail->club_suburb,
                    'club_postcode'         => $offer_detail->club_postcode,
                    'club_state'            => strip_tags($offer_detail->club_state),
                    'club_website'          => strip_tags($offer_detail->club_website),
                    'club_phone'            => $offer_detail->club_phone,
                    'club_description'      => $offer_detail->club_description,
                    'club_home_image_path'  => $offer_detail->club_home_image_path,
                    'club_home_image_path2' => $offer_detail->club_home_image_path2,
                    'club_logo'             => $offer_detail->club_logo,
                    'club_privacy_policy'   => $offer_detail->club_privacy_policy,
                    'club_status'           => $offer_detail->club_status,
                    'fax'                   => $offer_detail->fax,

                );
            }

            $i = 2;
           // echo "<pre>"; print_r($offer_arr); exit;
            foreach ($offer_arr as $offer) {
                $this->my_phpexcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $offer['club_id'])
                    ->setCellValue('B' . $i, $offer['club_code'])
                    ->setCellValue('C' . $i, $offer['club_name'])
                    ->setCellValue('D' . $i, $offer['club_address'])
                    ->setCellValue('E' . $i, $offer['club_suburb'])
                    ->setCellValue('F' . $i, $offer['club_postcode'])
                    ->setCellValue('G' . $i, $offer['club_state'])
                    ->setCellValue('H' . $i, $offer['club_website'])
                    ->setCellValue('I' . $i, $offer['club_phone'])
                    ->setCellValue('J' . $i, $offer['club_description'])
                    ->setCellValue('K' . $i, $offer['club_home_image_path'])
                    ->setCellValue('L' . $i, $offer['club_home_image_path2'])
                    ->setCellValue('M' . $i, $offer['club_logo'])
                    ->setCellValue('N' . $i, $offer['club_privacy_policy'])
                    ->setCellValue('O' . $i, $offer['club_status'])
                    ->setCellValue('P' . $i, $offer['fax']);
                    $i++;
            }
        }

        // Redirect output to a client’s web browser (Excel5)
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $objWriter = PHPExcel_IOFactory::createWriter($this->my_phpexcel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }
}
