 <?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Clubs extends Front_Controller {

    // public $data;

    function __construct() {
        parent::__construct();
        $this->load->model(array('club_model', 'offers/offer_model','deals/deal_model','cms/cms_model'));
        $this->load->library('pagination');
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $club_id = $this->uri->segment(2);
        $this->data['club_url'] = $this->club_model->club_url($club_id);
        //echo $this->db->last_query();die;
           //header('Location: '.$this->data['club_url']->club_website);     
        $this->output->set_header("Location: ".  $this->data['club_url']->club_website);
    }


    function club_lists() {
        $state = $this->input->post('state');
        $postcode = $this->input->post('postcode');
        $suburb = $this->input->post('suburb');
        $clubname = $this->input->post('club_name');
        $config['total_rows'] = $this->club_model->count_clubs();
        $config['per_page'] = 10;
        $config['base_url'] = site_url() . 'clubs/search_clubs';
        $config['num_links'] = 5;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='5'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;

        $this->pagination->initialize($config);
        $result = $this->club_model->search_clubs($config['per_page'], $page, $state, $postcode, $suburb, $clubname);

        if ($result != false) {
            $this->data['club_lists'] = $result;
        } else {
            $this->data['club_lists'] = array();
        }
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        $this->data['get_clubs'] = $this->club_model->get_all_clubs();
        $this->data['get_suburb'] = $this->offer_model->get_suburb();
        $this->data['get_state'] = $this->offer_model->get_state();
        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());
        $slug = 'information-for-members';
        $this->data['info'] = $this->cms_model->info_slug($slug);
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);

        Template::render('cms/frontend/index', $this->data);
    }

}
