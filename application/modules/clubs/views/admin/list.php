<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage Clubs</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a>
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a>
		  </nav>
		</div>
	</header>
	<div class="body">
	   <div style="padding:10px;float:right">
    <a class="btn btn-info btn-xs"href="<?php echo site_url('admin/clubs/offer_csv') ?>">Download CSV</a>
    </div><br><br>

		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="club_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Club Name</th>
				<th>Logo</th>
				<th>Address </th>
				<th>Website </th>
				<th>Phone Number </th>
				<th style="width:85px;" >Status</th>
				<th style="width:90px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all">
			<?php if ($clubs): ?>
				<?php foreach ($clubs as $key => $val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->club_name; ?></td>
                                <td><?php if (file_exists($val->club_logo) and $val->club_logo != null): ?><a title="<?php echo $val->club_name; ?>" href="<?php echo base_url($val->club_logo); ?>" class="img-popup"><img class="img-thumbnail" src="<?php echo imager($val->club_logo, 80, 80); ?>" /></a><?php endif;?></td>
                                <td><?php echo $val->club_address; ?></td>
				<td><?php echo $val->club_website; ?></td>
				<td><?php echo $val->club_phone; ?></td>
				<td class="center"><input type="checkbox"  <?php echo $val->club_status ? "checked" : ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('admin/clubs/toggle_status/'); ?>" data-id="<?php echo $val->club_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
				<td class="center">
					<a href="<?php echo base_url('admin/clubs/edit/' . $val->club_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
					<a href="<?php echo base_url('admin/clubs/delete/' . $val->club_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
				</td>
			</tr>
				<?php endforeach;?>
			<?php endif;?>
		</tbody>
		</table>

	</div>
</div>