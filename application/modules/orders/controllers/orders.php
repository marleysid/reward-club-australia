<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Orders extends Front_Controller {

    // public $data;

    function __construct() {
        parent::__construct();
        $this->load->model(array('order_model', 'members/member_model'));
        $this->load->library('pagination');
    }

    public function _remap($method, $params) {
        
        $public_access = array('paypal_ipn');
        
        if(!in_array($method, $public_access)) {
            if( !$this->member_auth->logged_in()) {
                redirect(base_url());
            }
        }
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    function index() {
        
        $this->data['items'] = $this->order_model->cart_items($this->current_member->member_id);
        $this->data['total'] = $this->order_model->total_amount($this->current_member->member_id);
        // echo $this->db->last_query();die;
        $this->data['address'] = $this->order_model->currrent_member_address($this->current_member->member_id);
        $this->data['member'] = $this->member_model->current_member_detail($this->current_member->member_id);
        $this->data['shipping_charge'] = $this->order_model->shipping_charge();
        //echo $this->db->last_query();die;
        // debug($this->data['member']->member_first_name);die;
        if ($this->input->post()) {


            $this->load->library('form_validation');

            $this->form_validation->CI = & $this;
            $this->form_validation->set_rules(
                    array(
                        array('field' => 'shipping', 'label' => 'Shipping or in-store any one ', 'rules' => 'trim|required'),
                    )
            );
            $this->form_validation->set_error_delimiters('<label class="error">', '</label>');
            if ($this->form_validation->run($this) === TRUE) {
                if ($_POST['shipping'] == 1) {
                    $total_amount = $this->data['total']->total + $this->data['shipping_charge']->value;
                    $delivery_ad = $this->input->post('delivery_address') ?: ucwords($this->data['address']->member_address).' '. $this->data['address']->member_postcode;
                } else {
                    $total_amount = $this->data['total']->total;
                    $delivery_ad = '';
                }
                $in_store_pickup = ($delivery_ad == '') ? '1' : '0';
                // echo $total_amount;die;

                $this->load->library('paypal');
                $data = array('member_id' => $this->current_member->member_id,
                    'name' => $this->data['member']->member_first_name,
                    'delivery_address' => $delivery_ad,
                    'in_store_pickup' => $in_store_pickup,
                    'email_address' => $this->data['member']->member_email,
                    'contact_number' => $this->data['member']->member_contact_no,
                    'amount' => $total_amount
                );

                $response = $this->paypal->payment($data);

                if ($response == FALSE) {
                    redirect('orders/payment_fail');
                } else {

                    $name = $data['name'];
                    $delivery_address = $data['delivery_address'];
                    $email_address = $data['email_address'];
                    $contact_number = $data['contact_number'];

                    $amount = $data['amount'];

                    $paykey = $response['PayKey'];
                    $this->db->insert('temp_order', array('member_id' => $this->current_member->member_id,
                        'name' => $name,
                        'amount' => $amount,
                        'payment_id' => $paykey,
                        'delivery_address' => $delivery_address,
                        'in_store_pickup' => $in_store_pickup,
                        'email_address' => $email_address,
                        'contact_number' => $contact_number
                    ));

                    // $query = $this->db->query("CALL sp_setOnlineOrder ({this->current_member->member_id},'{$name}','{$this->db->escape_like_str($delivery_address)}','{$email_address}','{$contact_number}', '{$paykey}', '{$total_amount}','{$quint_amount}','{$printer_amount}')");

                    redirect($response['RedirectURL']);
                }
            }
//            } else {
//                $this->data['errors'] = array();
//
//                Template::render('frontend/securepay',  $this->data);
//                
//            }
        }
//        echo validation_errors();
        Template::render('frontend/securepay', $this->data);
    }

    function paypal_cancel() {
//        echo '<p id="payment-msg">Payment has been cancelled.</p>';
        echo '<div style="width:200px;height:50px;border:1px solid black;border-radius:10px;margin:auto;">
            <p id="payment-msg" style="color:black;text-align:center;">Payment Cancelled</p>
            </div>';
    }

    function paypal_success() {

//        echo "<p id='payment-msg' >Payment Successful</p>"; 
        echo '<div style="width:200px;height:50px;border:1px solid black;border-radius:10px;margin:auto;">
            <p id="payment-msg" style="color:black;text-align:center;">Payment Successful</p>
            </div>';
    }

    function payment_fail() {
//        echo '<p id="payment-msg">Payment Fail</p>';
        echo '<div style="width:200px;height:50px;border:1px solid black;border-radius:10px;margin:auto;">
            <p id="payment-msg" style="color:black;text-align:center;">Payment Fail</p>
            </div>';
    }

    function paypal_ipn() {
        $path = APPPATH . 'logs/online_orders/paypal' . date('Y-m-d') . '.txt';
        @file_put_contents($path, print_r($_POST, TRUE), FILE_APPEND | LOCK_EX);

        $this->load->library('paypal');
        $response = $this->paypal->paymentDetail($this->input->post('pay_key'));
        // $response = $this->paypal->paymentDetail('AP-1LR44752JF560704K');

        if ($this->paypal->paypal0bj->APICallSuccessful($response['Ack'])) {
            
            $payKey = $response['PayKey'];
            $this->data['temp_order_data'] = $this->order_model->temp_order_data($payKey);
            
           // echo $this->db->last_query();

            $txnId = NULL;
            $txnState = NULL;


             foreach($response['payments'] as $k=>$v) {
                    $txnId = $v['TransactionID'];
                    $txnState = $v['TransactionStatus'];
                    
                
            }

            $insrtdb = $this->load->database('webservice', TRUE);
                
            
            $query = $insrtdb->query("CALL sp_setOnlineOrder ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
                    array(
                            $this->data['temp_order_data']->member_id
                    , $this->data['temp_order_data']->name
                    , $this->data['temp_order_data']->delivery_address
                    , $this->data['temp_order_data']->in_store_pickup
                    , $this->data['temp_order_data']->amount
                    , $this->data['temp_order_data']->email_address
                    , $this->data['temp_order_data']->contact_number
                    , $this->data['temp_order_data']->payment_id
                    , json_encode($response)
                    , $txnId
                    , $txnState ));
            $insrtdb->reconnect();
            
            if($query->num_rows() > 0) {
                $results = $query->row();
                if($results->status == '0') {
                    $code = '0000';
                    
                } else {
                    
                    $this->load->library('email');
                    $message = "<p>Thank you for your payment.</p>"
                            ."<p>This email is to confirm your payment submitted on ".date('F jS, Y')."</p>"
                            ."<p>Details:<br>"
                            ."<strong>Transaction Id</strong>: {$txnId}<br>"
                            ."<strong>Transaction Amount</strong>: $ {$txnAmount} {$txnCur}<br>"
                            ."</p>"
                            ."<p>Rewards Club</p>";

//					$this->email->to($user_detail->email)
//						->from("info@quint.com")
//						->subject("Thank you for your payment")
//						->message($message);
//
//					$this->email->send();

                    
                }
            } else {
                
            }
            
            exit();
            $this->db->insert('orders', array('member_id' => $this->data['temp_order_data']->member_id,
                'name' => $this->data['temp_order_data']->name,
                'amount' => $this->data['temp_order_data']->amount,
                'payment_id' => $this->data['temp_order_data']->payment_id,
                'delivery_address' => $this->data['temp_order_data']->delivery_address,
                'in_store_pickup' => $this->data['temp_order_data']->in_store_pickup,
                'email_address' => $this->data['temp_order_data']->email_address,
                'contact_number' => $this->data['temp_order_data']->contact_number,
                'txnId' => $txnId,
                'txnState' => $txnState
            ));
            $id = $this->db->insert_id();

            $this->db->insert('ordered_items', array('order_id' => $id,
                'deal_id' => $this->data['order_deals_detail']->deal_id,
                'quantity' => $this->data['order_deals_detail']->quantity,
                'amount_per_quantity' => $this->data['order_deals_detail']->deal_price));

            $this->db->delete('cart_session', array('member_id' => $this->current_member->member_id));

            // if(!$txnId or !$txnState or !$txnIdPrinter or !$txnStatePrinter ) {
            //     return FALSE;
            // }
            // echo 'here';
        }
    }

}
