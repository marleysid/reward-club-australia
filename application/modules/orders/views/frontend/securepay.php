<div class="container">
<!-- breadcrumbs -->
<ol class="breadcrumb">
    <li>My Order summary</li>
</ol>
<form method="post" action="<?php echo site_url('orders'); ?>">
    <div class="my-order-holder">
    	
       
        
            <div class="my-order-summary-box-holder">
                <?php if(isset($items)): ?>
                    <?php foreach($items as $item): ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 brd_btm_dfn">
                            <div class="col-xs-12 col-sm-12 col-md-9 fnt_n_clr_fx">  <div class="row"><p><?php echo $item->deal_title; ?></p>
                                <span>QTY: <?php echo $item->quan; ?></span></div></div>
                            <div class="col-xs-12 col-sm-12 col-md-3 fnt_n_clr_fx_rgt"> <div class="row"><p>$<?php echo $item->deal_price; ?></p></div></div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

                <div class="col-xs-12 col-sm-12 col-md-12 brd_btm_dfn">
                  <div class="col-xs-12 col-sm-12 col-md-9"> <div class="row"> <div class="fnt_n_clr_fx"> <p>Sub total</p></div></div></div>
                   <div class="col-xs-12 col-sm-12 col-md-3">  <div class="row"><div class="fnt_n_clr_fx_rgt"><p>$<?php echo $total->total; ?></p></div></div></div>
</div>
 <div class="col-xs-12 col-sm-12 col-md-12 brd_btm_dfn">

                   <div class="col-xs-12 col-sm-12 col-md-9"> <div class="row"> <div class="fnt_n_clr_fx"> <p>Standard Shipping $<?php echo $shipping_charge->value; ?></p>
                        <span> Est. delivery between Wed 26 Nov. - Tues 2 Dec </span>
                    </div></div></div>
                   
                  <div class="col-xs-12 col-sm-12 col-md-3">   <div class="row"><div class="fnt_n_clr_fx_rgt">
                  
                        <p>
                            <input type="checkbox" class="check_bx_pp" name="shipping" value="1" id="shipping"  />
                            <label class="control-label check_bx_pp_lbl" for="shipping"><span></span></label>
                        </p>
                    </div></div>
                    </div>
                     </div>
                     <div class="col-xs-12 col-sm-12 col-md-12 brd_btm_dfn">
                    <div class="col-xs-12 col-sm-12 col-md-9">  <div class="row"><div class="fnt_n_clr_fx"> <p>In-store pick up - FREE</p> </div> </div></div>
                    <div class="col-xs-12 col-sm-12 col-md-3 fnt_n_clr_fx_rgt"> <div class="row">
                        <p><input type="checkbox" class="check_bx_pp" name="shipping" value="0" id="in-store" checked/>          
                            <label class="control-label check_bx_pp_lbl" for="in-store"><span></span></label>
                        </p>
                        </div>
                    </div>
                    </div>
                    <div> <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 order-sumary-total-amount-holder">
                    <div class="fnt_n_clr_fx"><h3> TOTAL:</h3></div>
                     <?php echo form_error('shipping'); ?>


                
                <div class="fnt_n_clr_fx_rgt total_amount"> <h3>$<?php echo $total->total; ?></h3></div>
                </div>
                </div> 
                </div>
              
                </div>
               
           

            <div class="col-xs-12 col-sm-12 col-md-12 brd_btm_tp_dfn">
                <h4>DELIVERY OPTIONS</h4>
            </div>

          
            <div class="my-order-summary-box-delivery-holder">
          <!--    <div class="col-md-4 fnt_n_clr_fx"><p>In-store pick up - FREE</p></div>
              <div class="col-md-2 fnt_n_clr_fx_rgt">
                <p><input class="check_bx_pp" type="checkbox" /></p>
              </div>-->
                <div class="col-xs-12 col-sm-12 col-md-9 fnt_n_clr_fx"><p>Delivery</p> </div>
                <div class="col-xs-12 col-sm-12 col-md-3 fnt_n_clr_fx_rgt"><p class="edit-add-holder"><span>Edit/Add</span>
                        <input class="check_bx_pp" type="checkbox" id="delivery"/>       
                        <label class="control-label " for="delivery"><span></span></label></p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 dlivery_btn_box_wrp "><textarea class="address" name="delivery_address"  id="delivery_addresstxt" disabled="disabled"><?php echo ucwords($address->member_address); ?>,&nbsp;<?php echo $address->member_postcode; ?>,&nbsp;<?php echo ucwords($address->member_suburb); ?> </textarea></div>
                </div>
        

            <!--<div class="col-xs-12 col-sm-12 col-md-12 brd_btm_tp_dfn">
                <h4>Checkout</h4>
            </div>-->

           
          <!--    <div class="col-md-6 fnt_n_clr_fx"><p>Select your method of payment:</p></div>-->
                <!--    <div class="col-md-6 fnt_n_clr_fx"><h5>PAY BY CREDIT CARD
                      <input type="checkbox" class="check_bx_pp" /></h5>
                    </div>-->
                <!--    <div class="col-md-6 crd_wrp">
                      <ul>
                        <li><a href="#"><img src="<?php // echo get_asset('assets/frontend/images/master_card.png');   ?>" class="img-responsive"></a></li>
                        <li><a href="#"><img src="<?php // echo get_asset('assets/frontend/images/master_card.png');   ?>" class="img-responsive"></a></li>
                        <li><a href="#"><img src="<?php // echo get_asset('assets/frontend/images/master_card.png');   ?>" class="img-responsive"></a></li>
                        <li><a href="#"><img src="<?php // echo get_asset('assets/frontend/images/master_card.png');   ?>" class="img-responsive"></a></li>
                        <li><a href="#"><img src="<?php // echo get_asset('assets/frontend/images/master_card.png');   ?>" class="img-responsive"></a></li>
                      </ul>
                    </div>-->


                
                    <div class="col-xs-12 col-sm-12 col-md-12"> <div class="row"> <input class="btn check_out" type="submit" value="Checkout" name="submit" /> </div></div>
  
       

</div>
</form>
</div>
<script type="text/javascript">
    $(function () {
        $('#shipping').on('click', function () {
            $('#in-store').attr('checked', false);
            var shipping = $(this).is(':checked') ? 1 : 0;
            if (shipping == 1) {
                var total = "$<?php echo number_format(((float)$total->total + (float)$shipping_charge->value),2, '.',''); ?>";
                $('.total_amount h3').html(total);
            }
            else {
                var total = "$<?php echo number_format((float)$total->total,2, '.',''); ?>";
                //alert(total);
                $('.total_amount h3').html(total);
            }


        });
        $('#in-store').on('click', function () {
            $('#shipping').attr('checked', false);
            var total = "$<?php echo number_format((float)$total->total,2, '.',''); ?>";
            $('.total_amount h3').html(total);

        });
        $('#delivery').on('click', function () {
            var delivery = $(this).is(':checked') ? 1 : 0;
            if (delivery == 1) {
                $('#delivery_addresstxt').removeAttr('disabled');

            }
            else {
                $('#delivery_addresstxt').attr('disabled', true);
            }


        });
    });

</script>