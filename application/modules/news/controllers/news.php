<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends Front_Controller {

   // public $data;
    
    function __construct() {
        parent::__construct();
        $this->load->model(array('news_model','advertisement/advertisement_model','cms/cms_model'));
        $this->load->library(array('pagination', 'email'));
    }
     public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }
 
    public function index() {
        $id = safe_b64decode($this->uri->segment(2));
        $this->data['news_detail'] = $this->news_model->news_detail_page($id);
        $this->data['get_ads'] = $this->advertisement_model->ads_for_detail_page();
        Template::render('frontend/index', $this->data);
    }
    function subscribe(){
        $status = '';
        $name = 'Rewards email subscription';
        $email = $this->input->post('member_email');
        $check_subscribe = $this->news_model->check_subscription($email);
        if($check_subscribe == FALSE){
            $this->db->insert('newsletter_subscription',array('name' => $name,
                                             'email' => $email));
            $status = 1;
            echo json_encode(array('status' => $status));
            $info = " Add business";
            $value = $this->cms_model->rewards_club_email();

//            $this->load->library('email');
//        $from = "info@rewards.com.au";
            $from = $email;
            $message = $this->load->view('frontend/email_template',$email, $name, $value, $info);
//        $message = "Hi {$this->input->post('contact_name')},
//                                hello hello ".
//
//            "";

//        $this->email->to($_POST['email'])
            $this->email->to($value->value)
                ->from($from, "Rewards")
                ->subject('welcome')
                ->message($message);

            $this->email->send();



        } else{
            $status = 0;
            echo json_encode(array('status' => $status));
        }

        
    }
}