<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> News</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>" enctype="multipart/form-data" id="append">
                <div class="form-group <?php echo form_error('news_title') ? 'has-error' : '' ?>">
                    <label for="news_title" class="control-label col-lg-3">Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="news_title" placeholder="Title" name="news_title" class="form-control" value="<?php echo set_value("news_title", $edit ? $news_detail->news_title : ''); ?>" >
                        <?php echo form_error('news_title'); ?>
                    </div>
                </div>
                

                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="news_image" class="control-label col-lg-3">Image *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span> 
                                <span class="fileinput-exists">Change</span> 
                                <input type="file" name="news_image">
                            </span>
                            (maximum image size 218 *106 or same proportional)
                            <span class="fileinput-filename"></span> 
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists(config_item('news_image_path') . $news_detail->news_image)): ?>
                            <div>
                                <img src="<?php echo imager(config_item('news_image_path') . $news_detail->news_image, 128, 128); ?>" alt="news image" class="img-thumbnail">
                                </div>
                        <?php endif; ?>
                    </div>
                </div><!-- /.form-group -->		  
                <div class="form-group <?php echo form_error('news_excerpt') ? 'has-error' : '' ?>">
                    <label for="news_excerpt" class="control-label col-lg-3">Excerpt *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="news_excerpt" placeholder="Excerpt" name="news_excerpt" class="form-control"><?php echo set_value("news_excerpt", $edit ? $news_detail->news_excerpt : ''); ?></textarea>
                        <?php echo form_error('news_excerpt'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('news_desc') ? 'has-error' : '' ?>">
                    <label for="news_desc" class="control-label col-lg-3"> Description *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="offer_desc" placeholder=" Description" name="news_desc" class="form-control"><?php echo set_value("news_desc", $edit ? $news_detail->news_desc : ''); ?></textarea>
                        <?php echo display_ckeditor('news_desc'); ?>
                        <?php echo form_error('news_desc'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="news_status" class="control-label col-lg-3">Status *</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="news_status" class="switch switch-small"  value="1" <?php echo set_checkbox('news_status', '1', ($edit) ? ($news_detail->news_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                        <?php echo anchor('admin/news', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>

