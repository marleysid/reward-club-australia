<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage News</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>News Title</th>
				<th> Image</th>
				<th> Excerpt </th>
				<th style="width:85px;" >Status</th>
				<th style="width:90px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all" >
			<?php if($news): ?>
				<?php foreach($news as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->news_title; ?></td>
				<td><?php if(file_exists(config_item('news_image_path').$val->news_image) and $val->news_image!=null): ?><a title="<?php echo $val->news_title; ?>" href="<?php echo base_url(config_item('news_image_path').$val->news_image); ?>" class="img-popup"><img class="img-thumbnail" src="<?php echo imager(config_item('news_image_path').$val->news_image,80, 80); ?>" /></a><?php endif; ?></td>
				<td><?php echo $val->news_excerpt; ?></td>
				<td class="center"><input type="checkbox"  <?php echo $val->news_status ? "checked": ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('admin/news/toggle_status/'); ?>" data-id="<?php echo $val->news_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
                                <td class="center">
					<a href="<?php echo base_url('admin/news/edit/'.$val->news_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
					<a href="<?php echo base_url('admin/news/delete/'.$val->news_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
					
                                </td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>
