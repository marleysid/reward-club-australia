<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Settings_model $settings_model
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('settings_model');

        $title = "Manage Settings";
        Template::set('title', $title);
        Template::add_title_segment($title);
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index($id = 0) {
        $id = (int) $id;
        $this->data['edit'] = FALSE;
        if ($id) {
            $this->data['settings_details'] = $this->settings_model->get($id);
            $this->data['settings_details'] || redirect('admin/settings/');
            $this->data['edit'] = TRUE;
        }
        if ($this->input->post()) {
            $this->_add_edit_setting($id);
        }
        $this->_manage_setting();

        Template::render('admin/index', $this->data);
    }

    function _manage_setting() {
        $this->data['settings'] = $this->settings_model->get_all();
    }

    function _add_edit_setting($id) {
        $this->load->library('form_validation');

        $data = $this->input->post();

        $rules = array(
            array('field' => 'value', 'label' => 'Value', 'rules' => 'required|trim|xss_clean'),
        );

        if ($id) {
            
        } else {
            $rules[] = array('field' => 'key', 'label' => 'Unique Key', 'rules' => 'required|trim|unique[settings.key,settings.id]|xss_clean');
        }

        $this->form_validation->set_rules($rules);

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }

        if ($id == 0) {
            $this->settings_model->insert($data);
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $this->settings_model->update($id, $data);
            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/settings/', 'refresh');
    }

    function apk_parser() {
        if ($this->input->post('submit')) {
            if (!upload_file('userfile', config_item('apk_root'), '*', NULL, '0', array('file_name' => config_item('apk_file_name')), $up_data, $err)) {
                $this->session->set_flashdata('error_message', $err);
                redirect(uri_string());
            }

            $this->uploaded_file = $up_data['full_path'];
            $app_version = @$this->settings_model->find_by('key', 'app_version')->value;
            $app_version_code = @$this->settings_model->find_by('key', 'app_version_code')->value;
            $old_file_name = config_item('apk_file_name') . '.' . $app_version . '.apk';

            $this->load->helper('php_apk_parser');

            $apk_file_to_parse = copy($up_data['full_path'], config_item('apk_root') . 'parsefile.apk');
            $manifest = get_apk_manifest(config_item('apk_root') . 'parsefile.apk');

            $data = array(
                array(
                    'key' => 'app_version',
                    'value' => $manifest->getVersionName(),
                ),
                array(
                    'key' => 'app_version_code',
                    'value' => $manifest->getVersionCode(),
                )
            );
            $this->settings_model->update_batch($data, 'key');
            rename(config_item('apk_root') . config_item('apk_file_name'), config_item('apk_root') . $old_file_name);

            rename($up_data['full_path'], config_item('apk_root') . config_item('apk_file_name'));

            $this->session->set_flashdata('success_message', 'Apk uploaded successfully.');
            redirect(current_url());
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);

        Template::set('title', 'Apk');
        Template::set_base_title('Apk');
        Template::render('admin/apk_parser_form', $this->data);


//        $this->load->helper('php_apk_parser');
//
//        $manifest = get_apk_manifest(FCPATH.'assets/apkfile/Survey.apk');
//        
//        echo $manifest->getVersionName() . " (" . $manifest->getVersionCode() . ")" ;
    }

}
