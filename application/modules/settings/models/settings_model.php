<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Settings_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'settings';
        $this->field_prefix = '';
        $this->log_user = FALSE;
    }

    function get_facebook_url() {
        $query = $this->db->select("value")
                ->from("settings")
                ->where("key = 'rewards_club_facebook'")
                ->get();

        return $query->row();
    }

    function get_twitter_url() {
        $query = $this->db->select("value")
                ->from("settings")
                ->where("key = 'rewards_club_twitter'")
                ->get();

        return $query->row();
    }

    function get_youtube_url() {
        $query = $this->db->select("value")
                ->from("settings")
                ->where("key = 'rewards_club_youtube'")
                ->get();

        return $query->row();
    }
    function get_latlon($val){
         $query = $this->db->select("value")
                ->from("settings")
                ->where("key = '$val'")
                ->get();

        return $query->row();
    }
    
    function get_address(){
        $query = $this->db->select('key,value')
                          ->from('settings')
                          ->get();
        return $query->result();
    }
  

}
