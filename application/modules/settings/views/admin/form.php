<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>
		<h5><?php echo $edit ? 'Edit' : 'Add'; ?> settings</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<form class="form-horizontal" method="POST">
		  <div class="form-group <?php echo form_error('key')?'has-error':''?>">
			<label for="key" class="control-label col-lg-4">Unique Key *</label>
			<div class="col-lg-8">
				<input type="text" id="key" placeholder="write_unique_key" name="key" class="form-control" value="<?php echo set_value("key", $edit ? $settings_details->key : ''); ?>" <?php echo $edit ? 'disabled' : '' ?>>
				<?php echo form_error('key'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('value')?'has-error':''?>">
			<label for="value" class="control-label col-lg-4">Value *</label>
			<div class="col-lg-8">
				<textarea id="value" placeholder="Key value" name="value" class="form-control"><?php echo set_value("value", $edit ? $settings_details->value : ''); ?></textarea>
				<?php echo form_error('value'); ?>
			</div>
		  </div>
		  <div class="form-group">
			  <div class="col-sm-offset-4 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
				  <?php echo $edit ? anchor('admin/settings', 'Cancel', 'class="btn btn-warning"'):''; ?>
			  </div>
		  </div>
		</form>		
	</div>
</div>