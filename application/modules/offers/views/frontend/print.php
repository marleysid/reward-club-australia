<div class="right-column-wrapper">


    <div class="result-text"><span>Rewards Club local discounts are subject to change, are available at listed stores only and may exclude specials and other offers.</span></div>


    <div class="result-holder-001">
        <div class="result-list-view">
            <div class="result-list-inner-view">
                <div class="image-wrap">
                    <img class="price" src="<?php echo imager($detail->offer_image ? 'assets/uploads/offer_image/' . $detail->offer_image : '', 280, 280, 1);?>" alt="">
                </div>

                <div class="result-info-block">
                    <strong class="title"><a href="#"><?php echo $detail->offer_title;?></a></strong>
                    <span class="discount-text"><?php echo substr($detail->offer_short_desc, 0, 20);?></span>
                    <span class="subtitle"><?php echo $detail->offer_tag;?></span>
                    <span class="price">  <!-- <?php if (!empty($detail->price)){ echo "$".$detail->price; } else { echo 'N/A';}?> -->$31.95</span>
                </div>

            </div>

            <div class="result-list-button-holder">
                <span class="view-website"><a href="" target="_blank">View Website</a></span>
                <div class="condition-wrap"><span>Conditions: This offer is valid for purchase more than $30.00</span></div>
            </div>
        </div>


        <div class="participant-wrap">
            <h3>Participating stores</h3>
            <div class="participant-list-wrap">
                <table class="participant-table">
                    <thead>
                    <tr>
                        <th class="first">#</th>
                        <th class="second">Suburb</th>
                        <th class="third">Address</th>
                        <th class="fourth">Phone Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($address as $add) { ?>
                    <tr>
                        <td class="first">1</td>
                        <td class="second"><?php echo $add->oa_suburb;?></td>
                        <td class="third"><?php echo $add->oa_address;?></td>
                        <td class="fourth"><?php echo $add->oa_phone;?></td>
                    </tr>
                    </tbody>
                    <?php } ?>
                </table>
            </div>
        </div>   <!--end of participating store wrap-->



        <div class="print-offer-wrap">
            <div class="social-sharing-wrap">
                <?php $var = site_url().uri_string(); ?>



                <span class="twitter"><a href="http://twitter.com/share?url=<?php echo $var;?>" target="_blank">twitter</a></span>
                <span class="facebook"><a href="http://www.facebook.com/sharer.php?u=<?php echo $var;?>"  target="_blank">facebook</a></span>
                <span class="googleplus"><a href="https://plus.google.com/share?url=<?php echo $var?>">googleplus</a></span>
            </div>
            <span class="print-button"><a href="<?php echo base_url("offers/caljid"); ?>"><span class="icon">icon</span>Print Offer</a></span>

        </div>


    </div>

    <!--repeated end here -->
</div>
<script>
    $(function() {
        $('.rates').raty({path: base_url + 'assets/lib/jquery-raty/images',
            readOnly: true,
            score: function() {
                return $(this).attr('data-score');
            }});

        $('#sortby').on('change', function() {
            var option = $(this).val();
            //alert (option);
            $('#sort_option').val(option);
            $('.rgt_pp_btn').trigger('click');

        });
    });

</script>