<?php
/* echo "<pre>";
print_r($data['selected_cat']);
echo "<pre>";
die();*/
//print_r($regi);
?>

<div class="container">
  <!-- breadcrumbs -->
    <div class="breadcrumb-wrap add01">
                <ul class="breadcrumb-list">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <?php if ($this->input->get('region')): ?>
                    <li><?php echo $region_name->region_name; ?></li>
                    <?php endif;?>
                    <li>Discounts</li>
                </ul>
    </div>


   <div class="two-columns-wrapper">
             <div class="left-column-wrapper">
               <?php $this->load->view('refine_search');?>
            </div>

            <div class="right-column-wrapper">
                <div class="result-heading">
                 <div class="total-result">
                            <span>Showing all total results</span>
                        </div>

                    <div class="sorting-options-wrap">
                        <ul class="sorting-list">

                            <li class="grid">

                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/grid?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm btn-grid  <?php echo ($this->uri->segment(2) != 'list') ? 'active' : '' ?>"> </button>
                               Grid
                            </li>

                            <li class="list">

                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/list?' . $_SERVER['QUERY_STRING']; ?>'" class=" btn btn-default btn-sm btn-list   <?php echo ($this->uri->segment(2) == 'list') ? 'active' : '' ?>">  </button>
                                List
                                </li>

                           <li class="map">

                                <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/map?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm btn-map <?php echo ($this->uri->segment(2) != 'list' || $this->uri->segment(2) != 'map') ? 'active' : '' ?>">  </button>
                                Map
                            </li>
                        </ul>
                    </div>

                    <div class="sort-by-wrap">
                        <div class="local-search club bg_nn_pgmrgn">
                            <form role="form">
                                <div class="form-group">
                                    <label class="lbl_cls">Sort By</label>
                                    <select class="custom form-control input_txt clr" id="sortby">
                                        <option value="new"<?php echo ($this->input->get('sort_option') == 'new') ? "selected=\"selected\" " : '' ?>>New</option>
                                        <option value="alpha" <?php echo ($this->input->get('sort_option') == 'alpha' || $this->input->get('sort_option') == '') ? "selected=\"selected\" " : '' ?> >Alphabetical</option>
                                        <option value="nearest" <?php echo ($this->input->get('sort_option') == 'nearest') ? "selected=\"selected\" " : '' ?> >Nearest</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>





            <div class="result-holder">
                <div id="map" style="height: 580px; width: 100%;padding: 0px"><br /></div>
            </div>
             </div>
    </div>
</div>



  <div class="inner-slider-wrapper add01">
        <div id="inner-slider" class="owl-carousel">
            <?php $this->load->view('deals/frontend/bottom_slider');?>
        </div>
    </div>


<script>
    var locations =  <?php echo $location; ?>;
    var lat = <?php echo (!empty($user_location_lat)) ? $user_location_lat : '-37.682979'; ?>;
    var lon =  <?php echo (!empty($user_location_lon)) ? $user_location_lon : '144.580466'; ?>;

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: new google.maps.LatLng(lat, lon),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;


    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map,
        zIndex: locations[i][3],
        url: locations[i][4],
        title: locations[i][0]
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
            window.location.href = this.url;
        }
      })(marker, i));
    }

</script>