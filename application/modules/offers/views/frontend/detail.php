<style type="text/css">
    @media print {
    html, body {
        height: 99%;    
    }
}

</style>


<div class="container"> 
         <div class="breadcrumb-wrap add01">
                <ul class="breadcrumb-list">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <?php if($this->input->get('region')): ?>
                    <li><?php echo  $region_name->region_name; ?></li>
                    <?php endif; ?>
                    <li>Discounts</li>
                </ul>
            </div>



    <div class="two-columns-wrapper">
             <div class="left-column-wrapper">
               <?php $this->load->view('refine_search'); ?>
            </div>

<div>
       


  <div class="right-column-wrapper">
         <div class="result-heading">
                <div class="total-result">
                    <span><a href="<?php echo base_url('offers');?>">Back To Result</a></span>
                </div>
                <div class="sorting-options-wrap">
                    <ul class="sorting-list">
                       <li class="grid">
                            
                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/grid?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm btn-grid  <?php echo ($this->uri->segment(2) != 'list') ? 'active':'' ?>"> </button>
                               Grid 
                            </li>

                            <li class="list">
                            
                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/list?' . $_SERVER['QUERY_STRING']; ?>'" class=" btn btn-default btn-sm btn-list   <?php echo ($this->uri->segment(2) == 'list') ? 'active':'' ?>">  </button>
                                List
                                </li>
                           
                           <li class="map">
                            
                                <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/map?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm btn-map ">  </button>
                                Map
                            </li>
                    </ul>
                </div>
                <div class="sort-by-wrap">
                        <div class="local-search club bg_nn_pgmrgn">
                            <form role="form">
                                <div class="form-group filters">
                                    <label class="lbl_cls">Sort By</label>
                                    <select class="custom form-control input_txt clr selectBox" id="sortby" style="font-style: initial; font-size:13px; width:140px;">
                                        <option value="new"<?php  echo ($this->input->get('sort_option')=='new') ?   "selected=\"selected\" ":'' ?>>New</option>
                                        <option value="alpha" <?php echo ($this->input->get('sort_option')=='alpha' || $this->input->get('sort_option') == '' ) ?   "selected=\"selected\" ":'' ?> >Alphabetical</option>
                                        <option value="nearest" <?php echo($this->input->get('sort_option') == 'nearest')  ?   "selected=\"selected\" ":'' ?> >Nearest</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
            </div>


            <div class="result-text"><span><?php echo $detail->offer_desc;?></span></div>


                <div class="result-holder-001">
                   <!--   <div class="result-list-view">
                           <div class="result-list-inner-view">
                             <div class="image-wrap">
                             <img class="price" src="<?php echo imager($detail->offer_image ? 'assets/uploads/offer_image/' . $detail->offer_image : '', 280, 280, 1);?>" alt="">
                             </div>

                            <div class="result-info-block">
                                <strong class="title"><?php echo $detail->offer_title;?></strong>
                              <span class="discount-text">(<?php echo $detail->offer_tag; ?>**) <br />on <?php echo $detail->offer_short_desc;?></span>
                              <span class="subtitle"><?php echo $detail->offer_name;?></span>

                                <span class="price">  <?php if (!empty($detail->offer_price)){ echo '$'.number_format($detail->offer_price,2,'.','').'*'; } else { echo '';}?> </span>
                                
                            </div>

                           </div>

                        <div class="result-list-button-holder">
                            <span class="view-website"><a href="http://<?php if(!empty($detail->offer_website)) { echo $detail->offer_website;} else {echo '#';} ?>" target="_blank">View Website</a></span>

                            <div class="condition-wrap"><span> <?php if (!empty($detail->offer_condition)){ echo 'Conditions: '.$detail->offer_condition;} else { echo '';} ?></span></div>

                        </div>
                    </div> -->

                    <div class="result-list-view">
                           <div class="result-list-inner-view">
                             <div class="image-wrap">
                             <img class="price" src="<?php echo imager($detail->offer_image ? 'assets/uploads/offer_image/' . $detail->offer_image : '', 280, 280, 1);?>" alt="">
                             </div>

                            <div class="result-info-block">
                            <strong class="discount-text"><?php echo $detail->offer_name;?></strong>
                              <span class="subtitle"><?php echo $detail->trading_address;?></span>
                              <span class="subtitle"><?php echo $detail->suburb . ' ,' . $detail->trading_phone;?></span>
                                <!-- <span class="price">  <?php if (!empty($detail->offer_price)){ echo '$'.number_format($detail->offer_price,2,'.','').'*'; } else { echo '';}?> </span> -->
                                
                                <?php
                                if (isset($userLoggedIn) and $userLoggedIn):
                                    if(count($postcode)>0): 
                                    foreach($postcode as $discount) { 
                                    //echo "<pre>"; print_r($postcode); echo "</pre>";
                            ?>

                               <span class="price" style="font-size:18px;"><?php echo $discount->op_title;?></span>
                               <span class="title"><?php echo $discount->op_promocode;?></span>
                               <div class="condition-wrap" style="font-size: 12px; padding-top: 2px;"><?php echo $discount->op_description;?></div>

                                <?php 
                                        } 
                                        endif;
                                endif; ?>
                            </div>

                           </div>

                        <div class="result-list-button-holder">
                            <div class="button-wrap">
                                <?php if ($detail->offer_website): ?>
                                <span class="view-website"><a href="http://<?php if(!empty($detail->offer_website)) { echo $detail->offer_website;} else {echo '#';} ?>" target="_blank">View Website</a></span>
                                <?php endif;?>
                                <?php if ($detail->buynowlink): ?>
                                <span class="buy-button"><a href="<?php echo "http://" . $detail->buynowlink;?>" target="_blank">Buy Now</a></span>
                                <?php endif;?>
                            </div>
                            <div class="condition-wrap"><span> <?php if (!empty($detail->offer_condition)){ echo 'Conditions: '.$detail->offer_condition;} else { echo '';} ?></span></div>

                        </div>
                    </div>


                <div class="participant-wrap">
                    <h3>Participating stores</h3>
                    <div class="participant-list-wrap">
                        <table class="participant-table">
                            <thead>
                                <tr>
                                    <th class="first">#</th>
                                    <th class="second">Suburb</th>
                                    <th class="third">Address</th>
                                    <th class="fourth">Phone Number</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; foreach($address as $add) { ?>
                            <tr>
                                <td class="first"><?php echo $i; ?></td>
                                <td class="second"><?php echo $add->oa_suburb;?></td>
                                <td class="third"><?php echo $add->oa_address;?></td>
                                <td class="fourth"><?php echo $add->oa_phone;?></td>
                            </tr>
                            </tbody>
                            <?php $i++; } ?>
                        </table>
                    </div>
                </div>   <!--end of participating store wrap-->
            </div>


                  <div class="print-offer-wrap">
                    <div class="social-sharing-wrap">
                        <?php $var = site_url().uri_string(); ?>



                        <span class="twitter"><a href="http://twitter.com/share?url=<?php echo $var;?>" target="_blank">twitter</a></span>
                        <span class="facebook"><a href="http://www.facebook.com/sharer.php?u=<?php echo $var;?>"  target="_blank">facebook</a></span>
                        <span class="googleplus"><a href="https://plus.google.com/share?url=<?php echo $var?>">googleplus</a></span>
                    </div>
                    <span class="print-button"><a href="#" id="print"><span class="icon">icon</span>Print Offer</a></span>

                </div>
                </div>
                <!--repeated end here -->
            </div> <!--end of right column wrapper-->
        </div> <!--end of two column wrap-->
    </div> <!--end of container-->



        <!--start of printable area-->
        
        <!--end of printable area-->

    <div class="inner-slider-wrapper add01">
        <div id="inner-slider" class="owl-carousel">
            <?php $this->load->view('deals/frontend/bottom_slider'); ?>
        </div>
    </div>


<script>
    $(function() {
        $('.rates').raty({path: base_url + 'assets/lib/jquery-raty/images',
            readOnly: true,
            score: function() {
                return $(this).attr('data-score');
            }});

        $('#sortby').on('change', function() {
            var option = $(this).val();
            //alert (option);
            $('#sort_option').val(option);
            $('.rgt_pp_btn').trigger('click');

        });
    });

</script>





<script type="text/javascript">
    $('#print').on('click', function(){
        $('.result-holder-001').print({
            noPrintSelector: ".view-website",
            globalStyles: true,
            mediaPrint: false,
            stylesheet: null,
            iframe: true,
             append: null,
            prepend: null,
            manuallyCopyFormValues: true,
            deferred: $.Deferred(),
            timeout: 250,
                title: null,
                doctype: '<!doctype html>'
        });
    })
</script>





