<div class="container myoffers"> 
            <div class="breadcrumb-wrap add01">
                <ul class="breadcrumb-list">
                    <li><a href="<?php echo base_url(); ?>">Home</a></li>
                    <?php if($this->input->get('region')): ?>
                   <!--  <li><?php echo  $region_name->region_name; ?></li> -->
                    <?php endif; ?>
                    <li>Discounts</li>
                </ul>
            </div>


    <div class="two-columns-wrapper">
             <div class="left-column-wrapper">
               <?php $this->load->view('refine_search'); ?>
            </div>

            <div class="right-column-wrapper"> 
                 <div class="result-heading">
                        <div class="total-result">
                            <span>Showing <?php echo $total == 0 ? 0 : (($offest) ? ($offest):'1') ?> - <?php echo (($offest + $per_page)< $total) ? ($offest+$per_page):$total ?> of <?php
                                                $alltotal = $this->session->set_userdata('alltotal', $total); 
                                                echo $total ?> total results</span>
                        </div>
                    

                    <div class="sorting-options-wrap">
                        <ul class="sorting-list">
                           
                            <li class="grid">
                            
                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/grid?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm btn-grid  <?php echo ($this->uri->segment(2) != 'list') ? 'active':'' ?>"> </button>
                               Grid 
                            </li>

                            <li class="list">
                            
                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/list?' . $_SERVER['QUERY_STRING']; ?>'" class=" btn btn-default btn-sm btn-list   <?php echo ($this->uri->segment(2) == 'list') ? 'active':'' ?>">  </button>
                                List
                                </li>
                           
                           <li class="map">
                            
                                <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/map?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm btn-map ">  </button>
                                Map
                            </li>
                        </ul>
                    </div>

                    <div class="sort-by-wrap">
                        <div class="local-search club bg_nn_pgmrgn">
                            <form role="form" id="mytest" method="get">
                                <div class="form-group filters">
                                    <label class="lbl_cls">Sort By</label>
                                    <select class="custom form-control input_txt clr" id="sortby" style="font-style: initial; font-size:13px; width:135px;">
                                        <option value="new"<?php  echo ($this->input->get('sort_option')=='new') ?   "selected=\"selected\" ":'' ?>>New</option>
                                        <option value="alpha" <?php echo ($this->input->get('sort_option')=='alpha' || $this->input->get('sort_option') == '' ) ?   "selected=\"selected\" ":'' ?> >Alphabetical</option>
                                        <option value="nearest" <?php echo($this->input->get('sort_option') == 'nearest')  ?   "selected=\"selected\" ":'' ?> >Nearest</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            

            <div class="result-text"><span>Rewards Club local discounts are subject to change, are available at listed stores only and may exclude specials and other offers.</span></div>

               <div class="result-holder">
                 <ul class="result-item-list">
                    <?php if (!empty($get_offers)): ?>
                        <?php foreach ($get_offers as $offers): ?>
                         
                                <li class="offer-list">
                                 <?php 
                                    //$imgpath = site_url() . 'assets/uploads/offer_image/'.$offers->image;
                                    //echo $imgpath;
                                // http://localhost/rewards/assets/frontend/images/price-img.png
                                ?>
                
                                    <div class="img-wrap">
                                        <a href="<?php echo site_url('offers/detail/' . $offers->type . '/' . safe_b64encode($offers->id)).'/'.strtolower(url_title($offers->title,'-','TRUE')) ?>">

                                    <img class="img-responsive" src="<?php echo (isset($offers->image) && $offers->image != "") ? imager('assets/uploads/offer_image/'.$offers->image,237,146,1) : site_url('assets/img/no-image.png');?>" >
                                        </a>
                                        <img class="price" src="<?php echo site_url('assets/frontend/images/price-img.png');?>">
                                        <span class="prc-text"><?php echo $offers->tag;?></span>
                                    </div>
                                   
                                    <div class="info-wrap">
                                        <span><?php echo substr($offers->title, 0, 18);?></span>
                                    </div>
                                </li>
						

                         <?php endforeach; ?>
                         </ul>
                   <?php else: ?>
                   <h4>No Results Found!</h4>
                    <?php endif; ?>
                
               </div>
                <!--repeated end here --> 
             
                    
                        <div class="pagination-wrap">
                             <nav>
                               <?php echo $this->pagination->create_links(); ?>
                            </nav>
                        
                                <div class="pagination-items-option-wrap">
                                    <h4 class="margin_right_wrp_bxt_itm">Items per page:</h4>
                                   
                                    <button type="button"  class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span><?php echo $this->uri->segment(3) ? $this->uri->segment(3):'9' ?></span> <span class="icon-arrow-down-small-grey"></span> </button>
                                    <ul class="dropdown-menu insrt_with" role="menu" >
                                        <li><a href="<?php echo site_url() . 'offers/grid/9?' .  $_SERVER['QUERY_STRING']; ?>" >9</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/grid/15?' .  $_SERVER['QUERY_STRING']; ?>" >15</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/grid/21?' .  $_SERVER['QUERY_STRING']; ?>" >21</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/grid/30?' .  $_SERVER['QUERY_STRING']; ?>" >30</a></li>

                                    </ul>
                                </div>
                        
                    </div>  <!--end of pagination-->

                 
                </div>
               
            </div>  <!--end of right column wrapper-->

        </div> <!--end of two column wrapper-->
    </div> <!--end of container-->





    <div class="inner-slider-wrapper add01">
        <div id="inner-slider" class="owl-carousel">
            <?php $this->load->view('deals/frontend/bottom_slider'); ?>
        </div>
    </div>

        <script src="<?php echo base_url('assets/lib/jQuery.dotdotdot/src/js/jquery.dotdotdot.min.js')?>" ></script>
<script>
  $(function() {
         $('#sortby').on('change', function() {
            var option = $(this).val();
            //alert (option);
            $('#sort_option').val(option);
            $('.rgt_pp_btn').trigger('click');

        });
        $(".gride_view_caption").dotdotdot({
		height		: 60
	});
    });

</script>


<script type="text/javascript">
    // $('.pagination li a').off('click').on('click', function(){
    //     $.ajax({
    //        type: "POST",
    //        url: $(this).attr("href"),
    //        data:"q=<?php echo $_SERVER['QUERY_STRING'] ?>",
    //        success: function(res){
    //           //$(".rewards-main").html(res);
    //             var result = $('<div />').append(res).find('.result-holder').html();
    //             $('.result-item-list').html(result);
    //        }
    //        });
    //        return false;
    // });
</script>





  <script type="text/javascript">
        $(function() {
          // select.custom (custom named classed used with selectbox (dropdown)
            $("select.mycustom").each(function() {
                 var sb = new SelectBox({
                 selectbox: $(this),
                 changeCallback: function() {
                        $("#mytest").submit();
                  }
                });
            });

          });
          // $('select.custom').change(function(){
             //      alert("hello");
             //    })
        </script>
    
