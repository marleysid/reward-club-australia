

    <div class="container"> 
        <!-- breadcrumbs -->
         <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <?php if($this->input->get('region')): ?>
            <li><?php echo  $region_name->region_name; ?></li>
            <?php endif; ?>
            <li class="active">VIP Deals</li>
        </ol>
        <div class="row">
        <div class="col-xs-12 col-md-9 col-sm-12 deal-lists-holder">
				<div class="row">
                	
                    <div class="col-xs-12 col-sm-12 col-md-4">
                    <div class="view_as_grid">
                        <h5>Showing <?php echo $total == 0 ? 0 : (($offest) ? ($offest):'1') ?> - <?php echo (($offest + $per_page)< $total) ? ($offest+$per_page):$total ?> of <?php echo $total ?> total results</h5>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4 padding-left">
                        <ul class="deal_page_map">
                            <li><strong>View as</strong></li>
                            <li>
                            
                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/grid?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm btn-grid  <?php echo ($this->uri->segment(2) != 'list') ? 'active':'' ?>"> </button>
                               Grid 
                            </li>
                            <li>
                            
                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/list?' . $_SERVER['QUERY_STRING']; ?>'" class=" btn btn-default btn-sm btn-list   <?php echo ($this->uri->segment(2) == 'list') ? 'active':'' ?>">  </button>
                                List
                                </li>
                            <li>
                            
                                <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/map?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm btn-map ">  </button>
                                Map
                            </li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-4">
                        <div class="local-search club bg_nn_pgmrgn">
                            <form role="form">
                                <div class="form-group">
                                    <label class="lbl_cls">Sort By</label>
                                    <select class="form-control input_txt clr" id="sortby">
                                        <option value="new"<?php  echo ($this->input->get('sort_option')=='new') ?   "selected=\"selected\" ":'' ?>>New</option>
                                        <option value="alpha" <?php echo ($this->input->get('sort_option')=='alpha' || $this->input->get('sort_option') == '' ) ?   "selected=\"selected\" ":'' ?> >Alphabetical</option>
                                        <option value="nearest" <?php echo($this->input->get('sort_option') == 'nearest')  ?   "selected=\"selected\" ":'' ?> >Nearest</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                
                </div>
                <!--repeated start here -->
                <div class="row clearfix">
                <?php if (!empty($get_offers)): ?>
                    <?php foreach ($get_offers as $offers): ?>

                        <div class="col-xs-12 col-sm-12 col-md-12 btn-margin-35">
                            <div class="col-xs-12 col-sm-12 col-md-4 bg_nn_pgmrgn col-width-adjust-decrease-advance-search-list"> 
                            <div class="tag">
                                    <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span><?php echo space2br($offers->tag); ?></span>
                                    </div>
                                <a style="display:block;" class="thumb-holder" href="<?php echo site_url('offers/detail/' . $offers->type . '/' . safe_b64encode($offers->id)).'/'.strtolower(url_title($offers->title,'-','TRUE')) ?>" class="best_img_sprt_clr_cgng"><img src="<?php if ($offers->type == 'offer') {
                    echo imager($offers->image ? 'assets/uploads/offer_image/' . $offers->image : '', 279, 197,1);
                } else {
                    echo imager($offers->image ? 'assets/uploads/deal_image/' . $offers->image : '', 279, 197,1);
                } ?>" class="img-responsive" /></a>
                			</div>
                            <div class="col-xs-12 col-sm-12 col-md-8 with_resuld_board bg_grw_01 padding-top-bottom-8 col-width-adjust-increase-advance-search-list">
                            	
                                <div class="col-xs-12 col-sm-8 col-md-8 padding-right-adjustment padding-left-7 col-width-adjust-increase-advance-search-list-white">
									<div class="whit_box">
                                    <h4><?php echo $offers->title; ?> </h4>
                                    <p><?php echo character_limiter($offers->short_desc, 150); ?> </p>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-4 col-md-4 padding-left-right-8 col-width-adjust-decrease-advance-search-list-white">
                                <div class="whit_box fix_with_right flt_right_wrp">
                                
                                    <span  data-score="<?php echo $offers->review; ?>" class="rates"></span>
                                    <h4 class="rating">
                                    <?php echo $offers->total_review; ?> Reviews </h4>
        <?php if (isset($userLoggedIn) and $userLoggedIn): ?>
                                        <!--<a href="#" class="btn btn-danger">View Website &nbsp;&nbsp;<span class="glyphicon glyphicon-paperclip"></span></a>-->
                                        <button class="view_website btn fx_font_siz heart_bg favourite" data-type="<?php echo $offers->type; ?>" data-id="<?php echo $offers->id; ?>"> Favourite this <img src="<?php echo get_asset('assets/frontend/images/heart_icon.png'); ?>"  > </button>
        <?php else: ?>
                                        <button class="view_website btn fx_font_siz heart_bg " data-toggle="modal" data-target="#/loginModal"  data-whatever="@getbootstrap">Favourite this<img src="<?php echo get_asset('assets/frontend/images/heart_icon.png'); ?>"  ></button>

                        <?php endif; ?>
             <a href="<?php echo site_url('offers/detail/' . $offers->type . '/' . safe_b64encode($offers->id)); ?>"><button class="view_website btn fx_font_siz"> View Details <img src="<?php echo get_asset('assets/frontend/images/view_detail_arrow.png'); ?>"  > </button></a>
             </div>
                                </div>
                            </div>
                        </div>
    <?php endforeach; ?>
                    <?php else: ?>
                    <div class="col-xs-12 col-sm-12 col-md-12"><h4> No Results Found!</h4></div>
<?php endif; ?>
</div>

                <!--repeated end here -->
              <!--pagination start here -->
                <div class="row clearfix" style="margin-bottom:25px;">
                   
               
                     <div class="col-xs-12 col-sm-3 col-md-3">

                    </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 pgn_wrp_clr">
                        
                            <?php echo $this->pagination->create_links(); ?>
<!--                                <li><button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-arrow-left"></span>
        </button></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-arrow-right"></span>
        </button></li>-->
                          
                         </div>

                        <div class="col-xs-12 col-sm-4 col-md-4 pull-right">
                            <div class="btn-group items-per-page" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <h4 class="margin_right_wrp_bxt_itm">Items per page:</h4>
                                    <button type="button"  class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span><?php echo $this->uri->segment(3) ? $this->uri->segment(3):'9' ?></span> <span class="icon-arrow-down-small-grey"></span> </button>
                                    <ul class="dropdown-menu insrt_with" role="menu">
                                        <li><a href="<?php echo site_url() . 'offers/list/9?' . $_SERVER['QUERY_STRING']; ?>" >9</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/list/15?' . $_SERVER['QUERY_STRING']; ?>" >15</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/list/21?' . $_SERVER['QUERY_STRING']; ?>" >21</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/list/30?' . $_SERVER['QUERY_STRING']; ?>" >30</a></li>

                                    </ul>
                                </div>


                            </div>
                        </div>

                 
                </div>
                <!--pagination end here -->
            </div>
            <div class="col-xs-12 col-sm-12 col-md-3">
                <?php $this->load->view('refine_search'); ?>

            </div>
            
            
        </div>
    </div>
    <div class="feature-service" style="margin-top:60px;">
        <div class="container">
        <h3><span></span></h3>
<?php $this->load->view('deals/frontend/bottom_slider'); ?>
        </div>
    </div>

<script>
    $(function() {
        $('.rates').raty({path: base_url + 'assets/lib/jquery-raty/images',
            readOnly: true,
            score: function() {
                return $(this).attr('data-score');
            }});

        $('#sortby').on('change', function() {
            var option = $(this).val();
            //alert (option);
            $('#sort_option').val(option);
            $('.rgt_pp_btn').trigger('click');

        });
    });

</script>
