<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> Offers</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>" enctype="multipart/form-data" id="append">

                <div role="tabpanel" >
                    <ul  class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#offerdetails" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Detail</a></li>
                        <li role="presentation" class=""><a href="#offerpromocodes" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">Offer Promocodes</a></li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="offerdetails">
                            <br>

                            <div class="form-group <?php echo form_error('offer_title') ? 'has-error' : '' ?>">
                                <label for="company_name" class="control-label col-lg-3">Title *</label>
                                <div class="col-lg-7">
                                    <input type="text" id="offer_title" placeholder="Title" name="offer_title" class="form-control" value="<?php echo set_value("offer_title", $edit ? $offer_detail->offer_title : ''); ?>" >
                                    <?php echo form_error('offer_title'); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo form_error('category_id') ? 'has-error' : '' ?>">
                                <label for="category_id" class="control-label col-lg-3"> Category *</label>
                                <div class="col-lg-7">
                                    <?php echo $categories; ?>
                                    <?php echo form_error('category_id'); ?>

                                </div>
                            </div>
                            <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                                <label for="offer_image" class="control-label col-lg-3">Image *</label>
                                <div class="col-lg-7">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new">Select Image</span> 
                                            <span class="fileinput-exists">Change</span> 
                                            <input type="file" name="offer_image" id="offer-image">

                                        </span>
                                        (maximum image size 280*280 or same proportional)

                                        <span class="fileinput-filename"></span>
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                                        <?php echo @$logo_error; ?>
                                    </div>
                                    <?php if($edit && $offer_detail->offer_image != ""): ?>
                                        <div>
                                            <img src="<?php echo imager(config_item('offer_image_path').$offer_detail->offer_image, 128, 128); ?>" alt="offer image" class="img-thumbnail">
                                            <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                                        </div>
                                    <?php else: ?>
                                        <br/>
                                   <span style="color:red"> Image not available </span>
                                <?php endif; ?>
                                </div>
                            </div><!-- /.form-group -->

                            <div class="form-group <?php echo form_error('trading_address') ? 'has-error' : '' ?>">
                                <label for="offer_tag" class="control-label col-lg-3">Trading Address</label>
                                <div class="col-lg-7">
                                    <input type="text" id="trading_address" placeholder="Trading Address" name="trading_address" class="form-control" value="<?php echo set_value("trading_address", $edit ? $offer_detail->trading_address : ''); ?>" >

                                    <?php echo form_error('trading_address'); ?>
                                </div>
                            </div>


                            <div class="form-group <?php echo form_error('suburb') ? 'has-error' : '' ?>">
                                <label for="offer_tag" class="control-label col-lg-3"> Trading Suburb *</label>
                                <div class="col-lg-7">
                                    <input type="text" id="suburb" placeholder=" trading suburb" name="suburb" class="form-control mysuburb"  value="<?php echo set_value("suburb", $edit ? $offer_detail->suburb : ''); ?>" >

                                    <?php echo form_error('suburb'); ?>
                                </div>
                            </div>



                            <div class="form-group <?php echo form_error('trading_phone') ? 'has-error' : '' ?>">
                                <label for="offer_tag" class="control-label col-lg-3"> Trading Phone *</label>
                                <div class="col-lg-7">
                                    <input type="text" id="trading_phone" placeholder="Price" name="trading_phone" class="form-control" value="<?php echo set_value("trading_phone", $edit ? $offer_detail->trading_phone : ''); ?>" >

                                    <?php echo form_error('trading_phone'); ?>
                                </div>
                            </div>


                            <div class="form-group <?php echo form_error('buynowlink') ? 'has-error' : '' ?>">
                                <label for="offer_tag" class="control-label col-lg-3"> Buy Now Link </label>
                                <div class="col-lg-7">
                                    <input type="text" id="buynowlink" placeholder="buynowlink" name="buynowlink" class="form-control" value="<?php echo set_value("buynowlink", $edit ? $offer_detail->buynowlink : ''); ?>" >

                                    <?php echo form_error('buynowlink'); ?>
                                </div>
                            </div>






                            <div class="form-group <?php echo form_error('offer_short_desc') ? 'has-error' : '' ?>">
                                <label for="offer_short_desc" class="control-label col-lg-3">General Description *</label>
                                <div class="col-lg-7">
                                    <textarea type="text" id="short_desc" placeholder="Short Description" name="offer_short_desc" class="form-control"><?php echo set_value("offer_short_desc", $edit ? $offer_detail->offer_short_desc : ''); ?></textarea>
                                    <?php echo form_error('offer_short_desc'); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo form_error('offer_desc') ? 'has-error' : '' ?>">
                                <label for="offer_desc" class="control-label col-lg-3">Special Description *</label>
                                <div class="col-lg-7">
                                    <textarea type="text" id="offer_desc" placeholder=" Description" name="offer_desc" class="form-control"><?php echo set_value("offer_desc", $edit ? $offer_detail->offer_desc : ''); ?></textarea>
                                    <?php echo display_ckeditor('offer_desc'); ?>
                                    <?php echo form_error('offer_desc'); ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo form_error('offer_tag') ? 'has-error' : '' ?>">
                                <label for="offer_tag" class="control-label col-lg-3"> Discount percentage or offer tag(%) *</label>
                                <div class="col-lg-7">
                                    <input type="text" id="offer_tag" placeholder="Discount percentage" name="offer_tag" class="form-control" value="<?php echo set_value("offer_tag", $edit ? $offer_detail->offer_tag : ''); ?>" >
                                    * Maximum 10 character text only (Generally used 25% off and best price).
                                    <?php echo form_error('offer_tag'); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo form_error('offer_name') ? 'has-error' : '' ?>">
                                <label for="offer_name" class="control-label col-lg-3"> Business Name</label>
                                <div class="col-lg-7">
                                    <input id="offer_name" placeholder="Offer Name" name="offer_name" class="form-control" value="<?php echo set_value("offer_name", $edit ? $offer_detail->offer_name : ''); ?>" >
                                    (What exact offer is? if any).
                                    <?php echo form_error('offer_name'); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo form_error('offer_price') ? 'has-error' : '' ?>">
                                <label for="offer_tag" class="control-label col-lg-3"> Price *</label>
                                <div class="col-lg-7">
                                    <input type="text" id="offer_price" placeholder="Price" name="offer_price" class="form-control" value="<?php echo set_value("offer_price", $edit ? $offer_detail->offer_price : ''); ?>" >

                                    <?php echo form_error('offer_price'); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo form_error('offer_condition') ? 'has-error' : '' ?>">
                                <label for="offer_condition" class="control-label col-lg-3">Condition *</label>
                                <div class="col-lg-7">
                                    <textarea type="text" id="offer_condition" placeholder="Conditions" name="offer_condition" class="form-control"><?php echo set_value("offer_condition", $edit ? $offer_detail->offer_condition : ''); ?></textarea>
                                    <?php echo form_error('offer_condition'); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo form_error('offer_website') ? 'has-error' : '' ?>">
                                <label for="offer_website" class="control-label col-lg-3">Website *</label>
                                <div class="col-lg-7">
                                    <textarea type="text" id="offer_website" placeholder="Website" name="offer_website" class="form-control"><?php echo set_value("offer_website", $edit ? $offer_detail->offer_website : ''); ?></textarea>
                                    <?php echo form_error('offer_condition'); ?>
                                </div>
                            </div>

                            <!--                  <div class="form-group">
                                            <label for="text1" class="control-label col-lg-3">Region</label>
                                            <div class="col-lg-7">
                                                <select name="region_id" class="form-control">
                                                    <option value="0">Select a Region</option>
                                                  
                            
                                                            <option value='<?php // echo $row->region_id;  ?>' <?php // echo set_select('region_id', $row->region_id, ($edit and $regions->region_id == $row->region_id) ? TRUE: FALSE);  ?>
                                                                       
                            <?php // echo $row->region_name; ?></option>
                            
                                                       
                            
                                            </div>
                                        </div>-->
                            <div class="form-group <?php echo form_error('region_id') ? 'has-error' : '' ?>">
                                <label for="region_id" class="control-label col-lg-3"> Region *</label>
                                <div class="col-lg-7">
                                    <select name="region_id" class="form-control">
                                        <option value="">Select a Region</option>
                                        <?php if(isset($regions)): ?>

                                            <?php foreach($regions as $row): ?>
                                                <option value="<?php echo $row->region_id; ?>"<?php echo set_select('region_id', $row->region_id, ($edit and $offer_detail->region_id == $row->region_id) ? TRUE : FALSE); ?>><?php echo $row->region_name; ?></option>
                                            <?php endforeach; ?>

                                        <?php endif; ?>
                                    </select>
                                    <?php echo form_error('region_id'); ?>
                                </div>

                            </div>

                            <div class="form-group <?php echo form_error('keywords') ? 'has-error' : '' ?>">
                                <label for="keywords" class="control-label col-lg-3">Keywords </label>
                                <div class="col-lg-7">
                                    <textarea type="text" id="keywords" placeholder="Keywords" name="keywords" class="form-control"><?php echo set_value("keywords", $edit ? $get_keywords : ''); ?></textarea>
                                    * Please separate the keywords by comma (,)
                                    <?php echo form_error('keywords'); ?>
                                </div>
                            </div>

                            <?php
                            if($edit) {
//                $date = str_replace('-','/',$offer_detail->offer_valid_date);
                                //debug($offer_detail->offer_valid_date);die;
                            }
                            ?>
                            <div class="form-group <?php echo form_error('offer_valid_date') ? 'has-error' : '' ?>">
                                <label for="offer_valid_date" class="control-label col-lg-3"> Date *</label>
                                <div class="col-lg-7">
                                    <input type="text" id="offer_valid_date" placeholder="Date" name="offer_valid_date" class="form-control" value="<?php echo set_value("offer_valid_date", $edit ? date('m/d/Y', strtotime($offer_detail->offer_valid_date)) : ''); ?>" >
<?php echo form_error('offer_valid_date'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="slider_top" class="control-label col-lg-3">Top Slider</label>
                                <div class="col-lg-7">
                                    <input type="checkbox" name="slider_top" class="switch switch-small"  value="1" <?php echo set_checkbox('slider_top', '1', ($edit) ? ((isset($slider_detail->slider_top) and $slider_detail->slider_top) ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="slider_bottom" class="control-label col-lg-3">Bottom Slider</label>
                                <div class="col-lg-7">
                                    <input type="checkbox" name="slider_bottom" class="switch switch-small"  value="1" <?php echo set_checkbox('slider_bottom', '1', ($edit) ? ((isset($slider_detail->slider_bottom) and $slider_detail->slider_bottom) ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="slider_bottom" class="control-label col-lg-3">Featured Image</label>
                                <div class="col-lg-7">
                                    <input type="checkbox" name="slider_featured" class="switch switch-small"  value="1" <?php echo set_checkbox('slider_featured', '1', ($edit) ? ((isset($slider_detail->slider_featured) and $slider_detail->slider_featured) ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="offer_status" class="control-label col-lg-3">Status *</label>
                                <div class="col-lg-7">
                                    <input type="checkbox" name="offer_status" class="switch switch-small"  value="1" <?php echo set_checkbox('offer_status', '1', ($edit) ? ($offer_detail->offer_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <button class="btn btn-info3 " type="button"  class="btn btn-default" style="padding: 5px 10px; font-size: 12px;
                                            line-height: 1.5; border-radius: 3px;" >Add Address</button></div></div>

                            <div id="offer_test">


                                <?php if(isset($address)): ?>
    <?php foreach($address as $add): ?>
                                        <fieldset id="address_fieldset">
                                            <div class="form-group ">
                                                <label for="offer_address" class="control-label col-lg-3">Street Address</label>
                                                <div class="col-lg-7">
                                                                                                      <textarea type="text" id="company_address" placeholder="Street Address" name="offer_address[]" class="form-control"  ><?php echo set_value("offer_address", $edit ? $add->oa_address : ''); ?></textarea>

                                                </div>
                                            </div>
                                        

                                            <div class="form-group">
                                                <label for="offer_phone" class="control-label col-lg-3">Phone</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="offer_phone" placeholder="Phone" name="offer_phone[]" class="form-control" value="<?php echo set_value("offer_phone", $edit ? $add->oa_phone : ''); ?>" >

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="offer_post_code" class="control-label col-lg-3">Post Code</label>
                                                <div class="col-lg-7">
                                                    <input type="text" id="post_code" placeholder="Post Code" name="offer_post_code[]" class="form-control post_codes" value="<?php echo set_value("offer_post_code", $edit ? $add->oa_post_code : ''); ?>" >

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="offer_suburb" class="control-label col-lg-3">Suburb</label>
                                                <div class="col-lg-7">
                                                    <input type="text"  placeholder="Suburb" name="offer_suburb[]" class="form-control mysuburb" value="<?php echo set_value("offer_suburb", $edit ? $add->oa_suburb : ''); ?>" >

                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-8">
                                                    <a href="#" class="remove">Remove</a><br><br>
                                                </div>
                                            </div>
                                        </fieldset>
                                    <?php endforeach; ?>
<?php endif; ?>
                            </div>



                        </div>
                        <div role="tabpanel" class="tab-pane" id="offerpromocodes">
                            <br>
                            <fieldset id="offerpromo1">
                                <legend>Offer 1</legend>
                                <div class="form-group ">
                                    <label for="op_title1" class="control-label col-lg-3">Title</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_title1" placeholder="Title" name="op_title[]" class="form-control" value="<?php echo set_value("op_title"); echo ($edit and !empty($promo[0])) ? $promo[0]->op_title : ''; ?>" >

                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="op_promocode1" class="control-label col-lg-3">Promo Code</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_promocode1" placeholder="Promo Code" name="op_promocode[]" class="form-control" value="<?php echo set_value("op_promocode"); echo ($edit and !empty($promo[0])) ? $promo[0]->op_promocode : ''; ?>" >

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="op_desc1" class="control-label col-lg-3">Description</label>
                                    <div class="col-lg-7">
                                        <textarea id="op_desc1" placeholder="Description" name="op_desc[]" class="form-control"  ><?php echo set_value("op_description"); echo ($edit and !empty($promo[0])) ? $promo[0]->op_description:''; ?></textarea>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="offerpromo2">
                                <legend>Offer 2</legend>
                                <div class="form-group ">
                                    <label for="op_title2" class="control-label col-lg-3">Title </label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_title2" placeholder="Title" name="op_title[]" class="form-control" value="<?php echo set_value("op_title"); echo ($edit and !empty($promo[1]))? $promo[1]->op_title: ''; ?>" >

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="op_promocode2" class="control-label col-lg-3">Promo Code</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_promocode2" placeholder="Promo Code" name="op_promocode[]" class="form-control" value="<?php echo set_value("op_promocode"); echo ($edit and !empty($promo[1])) ? $promo[1]->op_promocode : ''; ?>" >

                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="op_desc2" class="control-label col-lg-3">Description</label>
                                    <div class="col-lg-7">
                                        <textarea  id="op_desc2" placeholder="Description" name="op_desc[]" class="form-control"  ><?php echo set_value("op_description"); echo ($edit and !empty($promo[1])) ? $promo[1]->op_description:''  ?></textarea>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="offerpromo3">
                                <legend>Offer 3</legend>
                                <div class="form-group ">
                                    <label for="op_title3" class="control-label col-lg-3">Title </label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_title3" placeholder="Title" name="op_title[]" class="form-control" value="<?php echo set_value("op_title"); echo ($edit and !empty($promo[2])) ? $promo[2]->op_title : ''; ?>" >

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="op_promocode3" class="control-label col-lg-3">Promo Code</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_promocode3" placeholder="Promo Code" name="op_promocode[]" class="form-control" value="<?php echo set_value("op_promocode"); echo ($edit and !empty($promo[2])) ? $promo[2]->op_promocode : ''; ?>" >

                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="op_desc3" class="control-label col-lg-3">Description</label>
                                    <div class="col-lg-7">
                                        <textarea  id="op_desc3" placeholder="Description" name="op_desc[]" class="form-control"  ><?php echo set_value("op_description"); echo ($edit and !empty($promo[2])) ? $promo[2]->op_description:''  ?></textarea>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="offerpromo4">
                                <legend>Offer 4</legend>
                                <div class="form-group ">
                                    <label for="op_title4" class="control-label col-lg-3">Title </label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_title4" placeholder="Title" name="op_title[]" class="form-control" value="<?php echo set_value("op_title"); echo ($edit and !empty($promo[3])) ? $promo[3]->op_title : ''; ?>" >

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="op_promocode4" class="control-label col-lg-3">Promo Code</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_promocode4" placeholder="Promo Code" name="op_promocode[]" class="form-control" value="<?php echo set_value("op_promocode"); echo ($edit and !empty($promo[3])) ? $promo[3]->op_promocode: ''; ?>" >

                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="op_desc4" class="control-label col-lg-3">Description</label>
                                    <div class="col-lg-7">
                                        <textarea  id="op_desc4" placeholder="Description" name="op_desc[]" class="form-control"  ><?php echo set_value("op_description"); echo ($edit and !empty($promo[3])) ? $promo[3]->op_description:''  ?></textarea>

                                    </div>
                                </div>
                            </fieldset>
                            <fieldset id="offerpromo5">
                                <legend>Offer 5</legend>
                                <div class="form-group ">
                                    <label for="op_title5" class="control-label col-lg-3">Title </label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_title5" placeholder="Title" name="op_title[]" class="form-control" value="<?php echo set_value("op_title"); echo ($edit and !empty($promo[4])) ? $promo[4]->op_title : ''; ?>" >

                                    </div>
                                </div>
                                <div class="form-group ">
                                    <label for="op_promocode5" class="control-label col-lg-3">Promo Code</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="op_promocode5" placeholder="Promo Code" name="op_promocode[]" class="form-control" value="<?php echo set_value("op_promocode"); echo ($edit and !empty($promo[4]))? $promo[4]->op_promocode : ''; ?>" >

                                    </div>
                                </div>

                                <div class="form-group ">
                                    <label for="op_desc5" class="control-label col-lg-3">Description</label>
                                    <div class="col-lg-7">
                                        <textarea id="op_desc5" placeholder="Description" name="op_desc[]" class="form-control"  ><?php echo set_value("op_description"); echo ($edit and !empty($promo[4])) ? $promo[4]->op_description:''  ?></textarea>

                                    </div>
                                </div>
                            </fieldset>

                        </div>

                    </div>
                </div>                 

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
<?php echo anchor('admin/offers', 'Cancel', 'class="btn btn-warning"'); ?>
                                </div>
                            </div>
            </form>		
        </div>

    </div>
</div>
<div style="display:none" id="source" >
    <div class="source_tags">
        <fieldset>
            <div class="form-group ">
                <label for="offer_address" class="control-label col-lg-3">Street Address</label>
                <div class="col-lg-7">
                    <textarea type="text" id="company_address" placeholder="Street Address" name="offer_address[]" class="form-control"  ></textarea>

                </div>
            </div>
        
            <div class="form-group ">
                <label for="offer_phone" class="control-label col-lg-3">Phone</label>
                <div class="col-lg-7">
                    <input type="text" id="offer_phone" placeholder="Phone" name="offer_phone[]" class="form-control" value="" >

                </div>
            </div>
            <div class="form-group">
                <label for="offer_post_code" class="control-label col-lg-3">Post Code</label>
                <div class="col-lg-7">
                    <input type="text" id="post_code" placeholder="Post Code" name="offer_post_code[]" class="form-control post_codes" value="" >
                </div>
            </div>

            <div class="form-group">
                <label for="offer_suburb" class="control-label col-lg-3">Suburb</label>
                <div class="col-lg-7">
                    <input type="text" id="mysuburb" placeholder="Suburb" name="offer_suburb[]" class="form-control mysuburb" value="" >

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-8">
                    <a href="javascript:void(0)" id="remove_fields">Remove</a><br><br>
                </div>
            </div>
        </fieldset>

    </div>
</div>

<script type="text/javascript">
//    $(function() {
//        $('#get_lat_lon').on('click', function() {
//            var _suburb = $('#suburb').val();
//            var _st_add = $('#offer_address').val();
//
//            if (_suburb != '' || _st_add != '') {
//                var _address = _st_add + ',' + _suburb + ',' + 'australia';
//                var _url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' + _address + '&sensor=false';
//
//                $.getJSON(_url, function(data) {
//                    if (data.status == "OK") {
//                        var _res = data.results[0].geometry.location; 
//                        var _lat = _res.lat;
//                        var _lon = _res.lng;
//                        var _add = data.results[0].formatted_address;
//                        $('#offer_lat').val(_lat);
//                        $('#offer_lon').val(_lon);
//                        $('#geo-msg').html('Latitude, Longitude values are for address: ' + _add);
//                    } else {
//                        $('#lat').val('');
//                        $('#lon').val('');
//                        $('#geo-msg').html('');
//                    }
//                });
//            } else {
//                $('#lat').val('');
//                $('#lon').val('');
//                $('#geo-msg').html('');
//            }
//        })
//    })
</script>
<script src="<?php echo base_url('assets').'/js/jquery.tagsinput.js' ?>"></script>

<script>
    $(document).ready(function () {
        var input = document.getElementsByClassName('mysuburb');
          //console.log(input);
           var options = {
                 'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
             };
             for (i = 0; i < input.length; i++) {
                 autocomplete = new google.maps.places.Autocomplete(input[i], options);
             }
        $('.btn-info3').click(function (e) {

            e.preventDefault();
            var app_html = $('#source').html();
            tot = $('#offer_test').append(app_html);
            var suburb = <?php echo json_encode($suburb); ?>;
           
          var input = document.getElementsByClassName('mysuburb');
          //console.log(input);
           var options = {
                 'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
             };

             for (i = 0; i < input.length; i++) {
                 autocomplete = new google.maps.places.Autocomplete(input[i], options);
             }


            var postcode = <?php echo json_encode($postcode); ?>;
            $(".post_codes").autocomplete({source: function (request, response) {
                    var code = $.ui.autocomplete.filter(postcode, request.term);

                    response(code.slice(0, 10));
                },
                change: function (event, ui) {
                    if (!ui.item) {
                        this.value = '';
                    }
                }
            });
        });
        $(document).on('click', '#remove_fields', function (e) {
            e.preventDefault();
            $(this).parents('.source_tags').remove();
        });

        $(document).on('click', '.remove', function (e) {
            e.preventDefault();
            $(this).parents('#address_fieldset').remove();
        });

        $('#parentcat').change(function () {
            // alert('hi');
            var cat_id = $(this).val();
//            var obj = {};
//            obj.subcat = $('#ChildCat').val();
            if (cat_id)
            {

                $.ajax({
                    type: 'post',
                    url: base_url + 'admin/offers/sub_cat_info/' + cat_id,
                    data: {sub_cat_id:<?php echo (int) $this->input->post('sub_category_id'); ?>},
                    dataType: "html",
                    success: function (data) {

                        $('#subcat').html(data);

                    },
                    error: function () {

                    }

                });
            }
            else
            {

            }

        });
<?php if(!$edit): ?>
            $('#parentcat').trigger('change');
<?php endif; ?>



    });
</script>

<script>
    $(function () {
//        $('#offer_valid_date').datepicker("option", "dateFormat", 'dd/mm/yy' );
        $('#offer_valid_date').datepicker( );
    });
</script>
<script>
    $(function () {
        var suburb = <?php echo json_encode($suburb); ?>;
        $(".offer_suburbs").autocomplete({source: function (request, response) {
                var results = $.ui.autocomplete.filter(suburb, request.term);

                response(results.slice(0, 10));
            },
            change: function (event, ui) {
                if (!ui.item) {
                    this.value = '';
                }
            }
        });

        var postcode = <?php echo json_encode($postcode); ?>;
        $(".post_codes").autocomplete({source: function (request, response) {
                var code = $.ui.autocomplete.filter(postcode, request.term);

                response(code.slice(0, 10));
            },
            change: function (event, ui) {
                if (!ui.item) {
                    this.value = '';
                }
            }
        });
    });
</script>


 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>



<script>
        $(function () {
            //$(":input").inputmask();
            //$('.demo2').colorpicker({});

            // add autocomplete for suburb field
       
        });
    </script>



