<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> Offers</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>" enctype="multipart/form-data" id="append">
                <div class="form-group <?php echo form_error('offer_title') ? 'has-error' : '' ?>">
                    <label for="company_name" class="control-label col-lg-3">Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="offer_title" placeholder="Title" name="offer_title" class="form-control" value="<?php echo set_value("offer_title", $edit ? $offer_detail->offer_title : ''); ?>" >
                        <?php echo form_error('offer_title'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('category_id') ? 'has-error' : '' ?>">
                    <label for="category_id" class="control-label col-lg-3"> Category *</label>
                    <div class="col-lg-7">
                        <?php echo $categories; ?>
                        <?php echo form_error('category_id'); ?>
                    </div>
                </div><!-- /.form-group -->
                <div class="form-group">
                    <label for="sub_category" class="control-label col-lg-3">Sub Category </label>
                    <div class="col-lg-7" id="subcat">
                        <?php
                        if (isset($sub_categories)) {
                            echo $sub_categories;
                        }
                        ?>
                    </div>
                </div><!-- /.form-group -->
                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="offer_image" class="control-label col-lg-3">Image *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span> 
                                <span class="fileinput-exists">Change</span> 
                                <input type="file" name="offer_image">
                            </span> 
                            <span class="fileinput-filename"></span> 
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                        <?php echo @$logo_error; ?>
                        </div>
<?php if ($edit and file_exists(config_item('offer_image_path') . $offer_detail->offer_image)): ?>
                            <div>
                                <img src="<?php echo imager(config_item('offer_image_path') . $offer_detail->offer_image, 128, 128); ?>" alt="offer image" class="img-thumbnail">
                                <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                            </div>
<?php endif; ?>
                    </div>
                </div><!-- /.form-group -->		  
                <div class="form-group <?php echo form_error('offer_short_desc') ? 'has-error' : '' ?>">
                    <label for="offer_short_desc" class="control-label col-lg-3">Short Description *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="short_desc" placeholder="Short Description" name="offer_short_desc" class="form-control"><?php echo set_value("offer_short_desc", $edit ? $offer_detail->offer_short_desc : ''); ?></textarea>
<?php echo form_error('offer_short_desc'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('offer_desc') ? 'has-error' : '' ?>">
                    <label for="offer_desc" class="control-label col-lg-3"> Description *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="offer_desc" placeholder=" Description" name="offer_desc" class="form-control"><?php echo  set_value("offer_desc", $edit ? $offer_detail->offer_desc : ''); ?></textarea>
                        <?php echo display_ckeditor('offer_desc'); ?>
<?php echo form_error('offer_desc'); ?>
                    </div>
                </div>

                <div class="form-group <?php echo form_error('offer_tag') ? 'has-error' : '' ?>">
                    <label for="offer_tag" class="control-label col-lg-3"> Tag *</label>
                    <div class="col-lg-7">
                        <input type="text" id="offer_tag" placeholder="Tag" name="offer_tag" class="form-control" value="<?php echo set_value("offer_tag", $edit ? $offer_detail->offer_tag : ''); ?>" >
<?php echo form_error('offer_tag'); ?>
                    </div>
                </div>
                 <?php
                if($edit){
                $date = str_replace('-','/',$offer_detail->offer_valid_date);
                //debug($offer_detail->offer_valid_date);die;
                }?>
                <div class="form-group <?php echo form_error('offer_valid_date') ? 'has-error' : '' ?>">
                    <label for="offer_valid_date" class="control-label col-lg-3"> Date *</label>
                    <div class="col-lg-7">
                        <input type="text" id="offer_valid_date" placeholder="Date" name="offer_valid_date" class="form-control" value="<?php echo set_value("offer_valid_date", $edit ?date('d/m/Y',  strtotime($date)) : ''); ?>" >
<?php echo form_error('offer_valid_date'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="offer_status" class="control-label col-lg-3">Status *</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="offer_status" class="switch switch-small"  value="1" <?php echo set_checkbox('offer_status', '1', ($edit) ? ($offer_detail->offer_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8">
                        <button class="btn btn-info3 " type="button"  class="btn btn-default" style="padding: 5px 10px; font-size: 12px;
                                line-height: 1.5; border-radius: 3px;" >Add Address</button></div></div>

                <div id="offer_test">


<?php if (isset($address)): ?>
    <?php foreach ($address as $add): ?>
                            <fieldset>
                                <div class="form-group ">
                                    <label for="offer_address" class="control-label col-lg-3">Street Address</label>
                                    <div class="col-lg-7">
                                        <textarea type="text" id="company_address" placeholder="Street Address" name="offer_address[]" class="form-control"  ><?php echo set_value("offer_address", $edit ? $add->oa_address : ''); ?></textarea>
       
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="offer_lat" class="control-label col-lg-3">Latitude</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="company_lat" placeholder="Latitude" name="offer_lat[]" class="form-control" value="<?php echo set_value("offer_lat", $edit ? $add->oa_latitude : ''); ?>" >
        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="offer_lon" class="control-label col-lg-3">Longitude</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="company_lon" placeholder="Longitude" name="offer_lon[]" class="form-control" value="<?php echo set_value("offer_lon", $edit ? $add->oa_longitude : ''); ?>" >
       
                                    </div>
                                </div>
                                <!--                        <div class="form-group">
                                                            <div class="col-sm-offset-3 col-sm-8">
                                                                <a class="btn btn-info" id="get_lat_lon" href="javascript:void(0)" >Get Latitude Longitude </a>
                                                                <span id="geo-msg" class="help-block"></span>
                                                            </div>
                                                        </div>-->

                                <div class="form-group">
                                    <label for="offer_phone" class="control-label col-lg-3">Phone</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="offer_phone" placeholder="Phone" name="offer_phone[]" class="form-control" value="<?php echo set_value("offer_phone", $edit ? $add->oa_phone : ''); ?>" >
      
                                    </div>
                                </div>
                                
                                 <div class="form-group">
                                    <label for="offer_post_code" class="control-label col-lg-3">Post Code</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="post_code" placeholder="Post Code" name="offer_post_code[]" class="form-control post_codes" value="<?php echo set_value("offer_post_code", $edit ? $add->oa_post_code : ''); ?>" >
      
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="offer_suburb" class="control-label col-lg-3">Suburb</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="offer_suburb" placeholder="Suburb" name="offer_suburb[]" class="form-control offer_suburbs" value="<?php echo set_value("offer_suburb", $edit ? $add->oa_suburb : ''); ?>" >
        
                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-8">
                                        <a href="#" class="remove_fields">Remove</a><br><br>
                                    </div>
                                </div>
                            </fieldset>
    <?php endforeach; ?>
<?php endif; ?>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
<?php echo anchor('club_admin/offers', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>
<div style="display:none" id="source" >
    <div class="source_tags">
        <fieldset>
            <div class="form-group ">
                <label for="offer_address" class="control-label col-lg-3">Street Address</label>
                <div class="col-lg-7">
                    <textarea type="text" id="company_address" placeholder="Street Address" name="offer_address[]" class="form-control"  ></textarea>

                </div>
            </div>
            <div class="form-group">
                <label for="offer_lat" class="control-label col-lg-3">Latitude</label>
                <div class="col-lg-7">
                    <input type="text" id="company_lat" placeholder="Latitude" name="offer_lat[]" class="form-control" value="" >

                </div>
            </div>
            <div class="form-group ">
                <label for="offer_lon" class="control-label col-lg-3">Longitude</label>
                <div class="col-lg-7">
                    <input type="text" id="company_lon" placeholder="Longitude" name="offer_lon[]" class="form-control" value="" >

                </div>
            </div>
            <!--                        <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-8">
                                            <a class="btn btn-info" id="get_lat_lon" href="javascript:void(0)" >Get Latitude Longitude </a>
                                            <span id="geo-msg" class="help-block"></span>
                                        </div>
                                    </div>-->

            <div class="form-group ">
                <label for="offer_phone" class="control-label col-lg-3">Phone</label>
                <div class="col-lg-7">
                    <input type="text" id="offer_phone" placeholder="Phone" name="offer_phone[]" class="form-control" value="" >

                </div>
            </div>
            <div class="form-group">
                                    <label for="offer_post_code" class="control-label col-lg-3">Post Code</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="post_code" placeholder="Post Code" name="offer_post_code[]" class="form-control post_codes" value="" >
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="offer_suburb" class="control-label col-lg-3">Suburb</label>
                                    <div class="col-lg-7">
                                        <input type="text" id="offer_suburb" placeholder="Suburb" name="offer_suburb[]" class="form-control offer_suburbs" value="" >
       
                                    </div>
                                </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-8">
                    <a href="#" class="remove_fields">Remove</a><br><br>
                </div>
            </div>
        </fieldset>

    </div>
</div>


<script src="<?php echo base_url('assets') . '/js/jquery.tagsinput.js' ?>"></script>

<script>
    $(document).ready(function() {
        $('.btn-info3').click(function(e) {

            e.preventDefault();
            var app_html = $('#source').html();
            tot = $('#offer_test').append(app_html);
               var suburb = <?php echo json_encode($suburb); ?>;
        $(".offer_suburbs").autocomplete({source:  function(request, response) {
        var results = $.ui.autocomplete.filter(suburb, request.term);
        
        response(results.slice(0, 10));
    },
    change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
            }
        }
        });
        
        var postcode = <?php echo json_encode($postcode); ?>;
        $(".post_codes").autocomplete({source:  function(request, response) {
        var code = $.ui.autocomplete.filter(postcode, request.term);
        
        response(code.slice(0, 10));
    },
    change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
            }
        }
        });
        });
        $(document).on('click', '.remove_fields', function(e) {
            e.preventDefault();
            $(this).parents('.source_tags').remove();
        });


        $('#parentcat').change(function() {
            // alert('hi');
            var cat_id = $(this).val();
            //alert(cat_id);
            if (cat_id)
            {
//alert( base_url + 'club_admin/offers/sub_cat_info/' + cat_id);
                $.ajax({
                    url: base_url + 'club_admin/offers/sub_cat_info/' + cat_id,
                    dataType: "html",
                    success: function(data) {

                        $('#subcat').html(data);

                    },
                    error: function() {

                    }

                });
            }
            else
            {

            }
        });
<?php if (!$edit): ?>
            $('#parentcat').trigger('change');
<?php endif; ?>

    });
</script>
<script>
$(function () {
	$('#offer_valid_date').datepicker(
			);
});
</script>
<script>
    $(function() {
        var suburb = <?php echo json_encode($suburb); ?>;
        $(".offer_suburbs").autocomplete({source:  function(request, response) {
        var results = $.ui.autocomplete.filter(suburb, request.term);
        
        response(results.slice(0, 10));
    },
    change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
            }
        }
        });
        
        var postcode = <?php echo json_encode($postcode); ?>;
        $(".post_codes").autocomplete({source:  function(request, response) {
        var code = $.ui.autocomplete.filter(postcode, request.term);
        
        response(code.slice(0, 10));
    },
    change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
            }
        }
        });
    });
</script>