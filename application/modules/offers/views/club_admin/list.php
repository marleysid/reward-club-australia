<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage Offers</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
    <div style="padding:10px;float:right">
    <a class="btn btn-info btn-xs"href="<?php echo site_url('club_admin/offers/offer_csv') ?>">Download CSV</a>
    </div><br><br>
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Offer Title</th>
				<th>Rewards Category</th>
				<th>Offer Image</th>
				<th>Offer Short Description </th>
				<th style="width:85px;" >Status</th>
				<th style="width:90px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all" >
			<?php if($offers): ?>
				<?php foreach($offers as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->offer_title; ?></td>
				<td><?php echo $val->category_name; ?></td>
				<td><?php if(file_exists(config_item('offer_image_path').$val->offer_image) and $val->offer_image!=null): ?><a title="<?php echo $val->offer_title; ?>" href="<?php echo base_url(config_item('offer_image_path').$val->offer_image); ?>" class="img-popup"><img class="img-thumbnail" src="<?php echo imager(config_item('offer_image_path').$val->offer_image,80, 80); ?>" /></a><?php endif; ?></td>
				<td><?php echo $val->offer_short_desc; ?></td>
				<td class="center"><input type="checkbox"  <?php echo $val->offer_status ? "checked": ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('club_admin/offers/toggle_status/'); ?>" data-id="<?php echo $val->offer_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
                                <td class="center">
					<a href="<?php echo base_url('club_admin/offers/edit/'.$val->offer_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
					<a href="<?php echo base_url('club_admin/offers/delete/'.$val->offer_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
					 
                                </td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>
