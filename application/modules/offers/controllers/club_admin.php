<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Club_admin extends Club_Admin_Controller {

    
    public $club_id = null;
    
    function __construct() {
        parent::__construct();
        $this->load->model('offer_model');
        $this->load->model('categories/category_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
        
        
        $this->club_id = $this->ion_auth->user()->row()->club_id;
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $user = $this->ion_auth->user()->row();
        $this->data['offers'] = $this->offer_model->offer_detail($user->club_id);

        Template::render('club_admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_offer(0);
        }

        $this->data['edit'] = FALSE;

        $this->load->model(array('category/category_model'));
        $this->data['categories'] = render_select($this->category_model->get_parent_category(), 'category_id', set_value('category_id', $this->data['edit'] ? $this->data['offer_detail']->category_id : FALSE), 'class="form-control" id="parentcat" ', 'category_id', 'category_name',array('set_top_blank' => true));
//		$this->data['regions'] = render_select($this->region_model->get_all(), 'region_id', set_value('region_id',$this->data['edit'] ? $this->data['company_detail']->region_id : FALSE), 'class="form-control" ', 'region_id', 'region_name');
//       
        $this->data['suburb'] = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());

        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/datepicker.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);



        Template::render('club_admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('club_admin/offers');

        $this->data['offer_detail'] = $this->offer_model->get($id);
        $this->data['address'] = $this->offer_model->get_address($id);
        $this->data['category_info'] = $this->category_model->get($this->data['offer_detail']->category_id);
        $this->data['suburb'] = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());

        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());

        $this->data['offer_detail'] || redirect('club_admin/offers/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_offer($id);
        }

        //debug($this->category_model->get_category_detail($id));die;
        if ($this->data['category_info']->category_parent_id == 0) {
            $this->data['categories'] = render_select($this->category_model->get_category_detail(), 'category_id', $this->data['edit'] ? $this->data['offer_detail']->category_id : FALSE, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank'=> true));
            $this->data['sub_categories'] = render_select($this->category_model->get_category_detail($this->data['category_info']->category_id), 'category_id',  FALSE, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank'=> true));
           
            
        } else {
            $this->data['categories'] = render_select($this->category_model->get_category_detail(), 'category_id', $this->data['edit'] ? $this->data['category_info']->category_parent_id : FALSE, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank'=> true));
            $this->data['sub_categories'] = render_select($this->category_model->get_category_detail($this->data['category_info']->category_parent_id), 'category_id', $this->data['edit'] ? $this->data['offer_detail']->category_id : FALSE, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank'=> true));
        
             
        }


        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/datepicker.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);


        Template::render('club_admin/form', $this->data);
    }

    function _add_edit_offer($id) {
        //debug($this->input->post('offer_image_path'));
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        // debug($this->input->post());die;
        $user = $this->ion_auth->user()->row();
        // debug($user->club_id);die;
        $data['club_id'] = $user->club_id;
        $data['offer_title'] = $this->input->post('offer_title');
        // debug($this->offer_model->user_detail($user->id));die;
        if ($this->input->post('sub_category_id') == null) {
            $data['category_id'] = $this->input->post('category_id');
        } else {
            $data['category_id'] = $this->input->post('sub_category_id');
        }
        $data['offer_short_desc'] = $this->input->post('offer_short_desc');
        $data['offer_desc'] = $this->input->post('offer_desc');
        $data['offer_tag'] = $this->input->post('offer_tag');
        $data['offer_valid_date'] =date("Y-m-d", strtotime($this->input->post('offer_valid_date')) );


//        $data = $this->input->post();
        $this->form_validation->set_rules(
                array(
                    array('field' => 'offer_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'category_id', 'label' => 'Category', 'rules' => 'required'),
                    array('field' => 'offer_short_desc', 'label' => 'Short Description', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'offer_desc', 'label' => ' Description', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'offer_tag', 'label' => ' Tag', 'rules' => 'required|trim|min_length[5]|xss_clean'),
                    array('field' => 'offer_valid_date', 'label' => ' Date', 'rules' => 'required'),
                    array('field' => 'offer_status', 'label' => 'Status', 'rules' => 'trim'),
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }

        $offer_image_path = NULL;
        if (isset($_FILES['offer_image']['name']) and $_FILES['offer_image']['name'] != '') {
            if ($result = upload_image('offer_image', config_item('offer_image_root'), FALSE)) {
                $image_path = config_item('offer_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
////				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'));
//                }
                $offer_image_path = $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
            
        }elseif($id == 0) {
            
                $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
                return FALSE;
            
        }

        unset($data['offer_image']);

        !$offer_image_path || ($data['offer_image'] = $offer_image_path);

        $data['offer_status'] = $this->input->post('offer_status') ? '1' : '0';

        if ($id == 0) {
            // echo 'test';die;
            $offer_id = $this->offer_model->insert($data);
            // echo $this->db->last_query();die;
            $offer_address = $this->input->post('offer_address');
            $offer_latitude = $this->input->post('offer_lat');
            $offer_longitude = $this->input->post('offer_lon');
            $offer_phone = $this->input->post('offer_phone');
            $offer_post_code = $this->input->post('offer_post_code');
            $offer_suburb = $this->input->post('offer_suburb');

            $this->offer_address($offer_id, $offer_address, $offer_latitude, $offer_longitude, $offer_phone, $offer_post_code, $offer_suburb);

            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_logo = NULL;
            if ($offer_image_path) {
                $old_logo = $this->offer_model->get($id)->offer_image;
            }
            $this->offer_model->update($id, $data);
            if ($old_logo and file_exists(config_item('offer_image_root') . $old_logo))
                unlink_file(config_item('offer_image_root') . $old_logo);

            $this->offer_model->delete_address($id);
            $offer_address = $this->input->post('offer_address');
            $offer_latitude = $this->input->post('offer_lat');
            $offer_longitude = $this->input->post('offer_lon');
            $offer_phone = $this->input->post('offer_phone');
            $offer_post_code = $this->input->post('offer_post_code');
            $offer_suburb = $this->input->post('offer_suburb');


            $this->offer_address($id, $offer_address, $offer_latitude, $offer_longitude, $offer_phone, $offer_post_code, $offer_suburb);

            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('club_admin/offers/', 'refresh');
    }

    function offer_address($id, $offer_address, $offer_latitude, $offer_longitude, $offer_phone, $offer_post_code, $offer_suburb) {

        $batchArray = array();

        if (is_array($offer_address)) {
            foreach ($offer_address as $k => $add) {

                $batchArray[$k]['oa_address'] = $add;
                $batchArray[$k]['offer_id'] = $id;
            }
        }
        if (is_array($offer_latitude)) {
            foreach ($offer_latitude as $k => $lat) {
                $batchArray[$k]['oa_latitude'] = $lat;
            }
        }
        if (is_array($offer_longitude)) {
            foreach ($offer_longitude as $k => $lon) {
                $batchArray[$k]['oa_longitude'] = $lon;
            }
        }
        if (is_array($offer_phone)) {
            foreach ($offer_phone as $k => $phone) {
                $batchArray[$k]['oa_phone'] = $phone;
            }
        }
        if (is_array($offer_post_code)) {
            foreach ($offer_post_code as $k => $offer_post_code) {
                $batchArray[$k]['oa_post_code'] = $offer_post_code;
            }
        }
        if (is_array($offer_suburb)) {
            foreach ($offer_suburb as $k => $offer_suburb) {
                $batchArray[$k]['oa_suburb'] = $offer_suburb;
            }
        }
        if (!empty($batchArray))
            $this->db->insert_batch('offer_address', $batchArray);
    }

    function sub_cat_info() {
        $id = $this->uri->segment(4);
        $data = $this->category_model->get_subcategory($id);
        $result = '';
        $result = '<select id="ChildCat" class="form-control" name="sub_category_id">';
         $result .= '<option value="">Select Sub Category</option>';
           
        foreach ($data as $c) {
            $result .= '<option value="' . $c->category_id . '">' . $c->category_name . '</option>';
        }
        $result.='</select>';
        echo $result;
    }

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->offer_model->update($id, array('offer_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function toggle_review_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->load->model(array('review_model'));
            $this->review_model->update($id, array('status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function delete($id) {
        $old_logo = $this->offer_model->get($id)->offer_image;

        if ($this->offer_model->delete($id)) {
            if ($old_logo and file_exists(config_item('offer_image_root') . $old_logo))
                unlink_file(config_item('offer_image_root') . $old_logo);
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('club_admin/offers/', 'refresh');
    }

    function reviews($company_id = NULL) {
        $company_id || redirect('admin/clubs');

        $this->load->model(array('review_model'));

        $this->data['reviews'] = $this->review_model->find_all_by('company_id', $company_id);
        $this->data['companies'] = render_select($this->company_model->get_all(), 'company_id', $company_id, 'style="width:225px; display:inline;" class="form-control" onchange=\' window.location.href = "' . base_url('admin/company/reviews') . '/"+this.value; \' ', 'company_id', 'company_name');

        Template::add_js(base_url() . "assets/lib/jquery.showmore.min.js", TRUE);
        Template::render('admin/company_reviews', $this->data);
    }
    
    

    function import_csv_data() {
        if ($this->xls_upload()) {
            $this->load->library('my_phpexcel');
            $this->uploaded_xls_file = $this->xls_file_data['full_path'];
//			$inputFileType = 'Excel2007';
            $inputFileType = 'CSV';
//			$sheetname = 'Gyms';
            $inputFileName = $this->uploaded_xls_file;
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);

//				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
            $objPHPExcel = $objReader->load($inputFileName);

            $worksheet = $objPHPExcel->getActiveSheet();
            
            


            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            //get header name and informations

            $req_header = array(
                'offer_id'
                , 'title'
                , 'category'
                , 'subcategory'
                , 'image'
                , 'short_description'
                , 'description'
                , 'tag'
                , 'start_date'
                , 'end_date'
                , 'street_address'
                , 'latitude'
                , 'longitude'
                , 'phone'
                , 'post_code'
                , 'suburb'
                , 'status'
            );

            $csv_header_title = array();
            $csv_header_title_col = array();

            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, 1);
                $val = $cell->getValue();
                if (!empty($val)) {
                    $csv_header_title[] = strtolower($val);
                    $csv_header_title_col[strtolower($val)] = $col;
                }
            }
//            dd(array_diff($req_header, $csv_header_title));
//            if (!empty(array_diff($req_header, $csv_header_title))){
//                echo 'error csv file';
//                die();
//            }

            $log_file = FCPATH.'application/logs/'.uniqid().'.txt';
            file_put_contents($log_file, PHP_EOL.'Import Errors: '.PHP_EOL);
            
            for ($row = 2; $row <= $highestRow; ++$row) {

                $insArr = array();

                //for id
                $insArr['offer_id'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_id'], $row)->getValue());

                //for title
                $insArr['title'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['title'], $row)->getValue());

                //for category
                $insArr['category'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['category'], $row)->getValue());
                $insArr['sub_category'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['subcategory'], $row)->getValue());

                //for image
                $insArr['image'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['image'], $row)->getValue());

                //for short_description
                $insArr['short_description'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['short_description'], $row)->getValue());

                //for description
                $insArr['description'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['description'], $row)->getValue());

                //for tag
                $insArr['tag'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['tag'], $row)->getValue());

                //for start_date
                $insArr['start_date'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['start_date'], $row)->getValue());

                //for end date
                $insArr['end_date'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['end_date'], $row)->getValue());

                //for street_address
                $insArr['street_address'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['street_address'], $row)->getValue());

                //for latitude
                $insArr['latitude'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['latitude'], $row)->getValue());

                //for longitude
                $insArr['longitude'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['longitude'], $row)->getValue());

                //for phone
                $insArr['phone'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['phone'], $row)->getValue());

                //for postcode
                $insArr['post_code'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['post_code'], $row)->getValue());

                //for suburb
                $insArr['suburb'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['suburb'], $row)->getValue());

                //for status
                $insArr['status'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['status'], $row)->getValue());

                $this->error_csv = array();
                $this->_update_data($insArr);
                
                if(!empty($this->error_csv)) {
                    
                    $data = PHP_EOL. ' Row '.$row.': '. implode(PHP_EOL. "\t", $this->error_csv);
                    file_put_contents( $log_file, $data, FILE_APPEND);
                    $this->session->set_flashdata('log_data', $log_file);
                    
                }
                
                $insArr = NULL;
                $this->error_csv = NULL;
                $data = NULL;
            }

            unlink($this->uploaded_xls_file);
            $this->session->set_flashdata('success_message', 'Import Successfully Done.');
            redirect(current_url());
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);

        Template::set('title', 'Import csv');
        Template::set_base_title('Import csv');
        Template::render('admin/import_csv_form', $this->data);
    }

    function _update_data($data) {
        
        $offer_id = (int) $data['offer_id'];
        //check offer id
        if (!$this->offer_model->get($offer_id)) {
            $offer_id = 0;
        }        

        //check category
        $category_id = NULL;
        $sub_category_id = NULL;
        
        if (!empty($data['category'])) {
            $category = $this->category_model->find_by('category_name', $data['category']);

            if ($category) {
                $category_id = $category->category_id;
            } else {
                // insert new category
                $category_id = $this->category_model->insert(array(
                    'category_name' => $data['category'],
                    'category_parent_id' => 0,
                    'category_status' => 1
                ));
            }

            //for sub category
            if (!empty($data['sub_category'])) {
                $sub_category = $this->category_model->where('category_parent_id', $category_id)->find_by('category_name', $data['sub_category']);

                if ($sub_category) {
                    $sub_category_id = $sub_category->category_id;
                } else {
                    // insert new category
                    $sub_category_id = $this->category_model->insert(array(
                        'category_name' => $data['sub_category'],
                        'category_parent_id' => $category_id,
                        'category_status' => 1
                    ));
                }
            }
        }

        $category_id = $sub_category_id ?: $category_id;
        
        //address
        $offer_address = array();
        if( !empty($data['street_address']) or 
                !empty($data['latitude'] ) or 
                !empty($data['longitude'] ) or 
                !empty($data['phone'] ) or 
                !empty($data['post_code'] ) or 
                !empty($data['suburb'] ) ) {
            
            $offer_address = array(
                    'oa_address' => $data['street_address'],
                    'oa_latitude' => $data['latitude'],
                    'oa_longitude' => $data['longitude'],
                    'oa_phone' => $data['phone'],
                    'oa_post_code' => $data['post_code'],
                    'oa_suburb' => $data['suburb']
            );

            if((empty($offer_address['oa_latitude']) || empty($offer_address['oa_longitude'])) and (
                    !empty($data['street_address']) or 
                !empty($data['post_code'] ) or 
                !empty($data['suburb'] )
                    ) ) {
                // get latitude longitude from address
                if($geocode = geocode("{$offer_address['oa_address']} {$offer_address['oa_suburb']} {$offer_address['oa_post_code']} australia")) {
                    $offer_address['oa_latitude'] = $geocode[0];
                    $offer_address['oa_longitude'] = $geocode[1];
                }

            }
            
        }
        
        // check if image exists      
        if (!empty($data['image']) and file_exists(FCPATH.'assets/uploads/temp_images/' . $data['image'])) {
            
            $data['image'] = move_file(FCPATH.'assets/uploads/temp_images/' . $data['image'], config_item('offer_image_root'));
            
        } else {
            $data['image'] = '';
        }
        
        //offer data
        $offerArr = array(
                'offer_title' => $data['title']
                , 'category_id' => $category_id
                , 'club_id' => $this->club_id
                , 'offer_image' => $data['image']
                , 'offer_short_desc' => $data['short_description']
                , 'offer_desc' => $data['description']
                , 'offer_tag' => $data['tag']
//                , 'start_date' => $data['start_date']
                , 'offer_valid_date' => $data['end_date']
                , 'offer_status' => $data['status']
        );
        
        $new_offer = FALSE;
        if(!empty($offerArr['offer_title'])) {
            $new_offer = TRUE;
            $this->last_offer_id = NULL;
            //validation
            
            if( !$category_id ) {
                $this->error_csv[] = 'Data Not Inserted. Error in category/subcategory. Category is required field.';
                return FALSE;
                
            }
            //check valid date
            if(!empty($offerArr['offer_valid_date'])) {
                $end_date = !empty($data['offer_valid_date']) && _date_is_valid($data['end_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $data['offer_valid_date']))) : '';
        
            }
            
            if($offer_id == 0) {
                $offer_id = $this->offer_model->insert( $offerArr);
                $this->last_offer_id = $offer_id;

            } else {
                $this->offer_model->update($offer_id, $offerArr);
                $this->last_offer_id = $offer_id;

                $this->offer_model->delete_address($offer_id);

            }
            
        }
        
        if($this->last_offer_id and !empty($offer_address)) {
            
            //update address
            $offer_address['offer_id'] = $this->last_offer_id;
            $this->offer_model->insert_offer_address($offer_address);
        } elseif($this->last_offer_id and !$new_offer) {
            $this->error_csv[] = 'Data Not Inserted.';
        }
        
    }

    function xls_upload() {
        $config['upload_path'] = './assets/uploads/csv';
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = '1024';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            return false;
        } else {
            $this->xls_file_data = $this->upload->data();
            //debug($this->xls_file_data);
            return true;
        }
    }

    function offer_csv() {
        $this->load->library('my_phpexcel');

        $sheet = $this->my_phpexcel->getActiveSheet();

        $this->my_phpexcel->getProperties()
                ->setCreator("Report")
                ->setLastModifiedBy("ADMIN")
                ->setTitle('Report')
                ->setSubject('Report')
                ->setDescription('Report')
                ->setKeywords("")
                ->setCategory("");

//        echo "<pre>";
//        print_r($offer_arr);




        $this->my_phpexcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'offer_id')
                ->setCellValue('B1', 'title')
                ->setCellValue('C1', 'category')
                ->setCellValue('D1', 'subcategory')
                ->setCellValue('E1', 'image')
                ->setCellValue('F1', 'short_description')
                ->setCellValue('G1', 'description')
                ->setCellValue('H1', 'tag')
                ->setCellValue('I1', 'start_date')
                ->setCellValue('J1', 'end_date')
                ->setCellValue('K1', 'status')
                ->setCellValue('L1', 'street_address')
                ->setCellValue('M1', 'latitude')
                ->setCellValue('N1', 'longitude')
                ->setCellValue('O1', 'phone')
                ->setCellValue('P1', 'post_code')
                ->setCellValue('Q1', 'suburb');
        $offer_details = $this->offer_model->get_offer_csv($this->club_id);
//         echo $this->db->last_query();die();
        //debug($offer_details);

        if (!empty($offer_details)) {
            $offer_arr = array();
            foreach ($offer_details as $offer_detail) {
                if (!isset($offer_arr[$offer_detail->offer_id])) {
                    $offer_arr[$offer_detail->offer_id] = array(
                        'offer_id' => $offer_detail->offer_id,
                        'offer_title' => $offer_detail->offer_title,
                        'offer_short_desc' => strip_tags($offer_detail->offer_short_desc),
                        'offer_desc' => strip_tags($offer_detail->offer_desc),
                        'offer_valid_date' => $offer_detail->offer_valid_date,
                        'category_name' => $offer_detail->category_name,
                        'sub_category_name' => $offer_detail->sub_category_name,
                        'offer_tag' => $offer_detail->offer_tag,
                        'offer_created_date' => $offer_detail->offer_created_date,
                        'offer_valid_date' => $offer_detail->offer_valid_date,
                        'offer_status' => $offer_detail->offer_status,
                        'oa_suburb' => $offer_detail->oa_suburb
                    );
                }
                $offer_arr[$offer_detail->offer_id]['address'][] = array(
                    'oa_address' => $offer_detail->oa_address,
                    'oa_latitude' => $offer_detail->oa_latitude,
                    'oa_longitude' => $offer_detail->oa_longitude,
                    'oa_phone' => $offer_detail->oa_phone,
                    'oa_post_code' => $offer_detail->oa_post_code,
                    'oa_suburb' => $offer_detail->oa_suburb
                );
            }

            // debug($offer_arr);
            $i = 2;
            foreach ($offer_arr as $offer) {

                $this->my_phpexcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $offer['offer_id'])
                        ->setCellValue('B' . $i, $offer['offer_title'])
                        ->setCellValue('C' . $i, $offer['category_name'])
                        ->setCellValue('D' . $i, $offer['sub_category_name'])
                        ->setCellValue('E' . $i, '')
                        ->setCellValue('F' . $i, $offer['offer_short_desc'])
                        ->setCellValue('G' . $i, $offer['offer_desc'])
                        ->setCellValue('H' . $i, $offer['offer_tag'])
                        ->setCellValue('I' . $i, date('d/m/Y', strtotime($offer['offer_created_date'])))
                        ->setCellValue('J' . $i, date('d/m/Y', strtotime($offer['offer_valid_date'])))
                        ->setCellValue('K' . $i, $offer['offer_status']);

                if (!empty($offer['address'])) {
                    foreach ($offer['address'] as $ind => $off) {
                        $j = $i + $ind;
                        $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('L' . $j, $off['oa_address'])
                                ->setCellValue('M' . $j, $off['oa_latitude'])
                                ->setCellValue('N' . $j, $off['oa_longitude'])
                                ->setCellValue('O' . $j, $off['oa_phone'])
                                ->setCellValue('P' . $j, $off['oa_post_code'])
                                ->setCellValue('Q' . $j, $off['oa_suburb']);


                        $j++;
                    }
                    $i = $j;
                }
            }
        }


        $this->my_phpexcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel5)
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        //header('Content-Type: application/vnd.ms-excel');
        // header('Content-Disposition: attachment;filename="Transaction_' . date('Y_m_d') . '.xls"');
        // header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        // header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        // header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        // header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        //  header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        //   header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($this->my_phpexcel, 'csv');
        $objWriter->save('php://output');
        exit;
    }

}
