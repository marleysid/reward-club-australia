<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @property Company_model $company_model
 * @property Category_model $category_model
 * @property Review_model $review_model
 * @property Offer_model $offer_model
 * @property region_model $region_model
 */
class Admin extends Admin_Controller
{

    public $last_offer_id = null;
    public $error_csv     = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model('offer_model');
        $this->load->model('categories/category_model');
        $this->load->model('region/region_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params)
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else {
            return $this->index($method);
        }

    }

    public function index()
    {
        $this->data['offers'] = $this->offer_model->get_all();
        Template::render('admin/index', $this->data);
    }

    public function add()
    {
        if ($this->input->post()) {
            $this->_add_edit_offer(0);
        }

        $this->data['edit'] = false;

        $this->load->model(array('category/category_model'));
        $this->data['categories'] = render_select($this->category_model->get_parent_category(), 'category_id', set_value('category_id', $this->data['edit'] ? $this->data['offer_detail']->category_id : false), 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank' => true));

//        $this->data['regions'] = render_select($this->region_model->get_all(), 'region_id', set_value('region_id',$this->data['edit'] ? $this->data['company_detail']->region_id : FALSE), 'class="form-control" ', 'region_id', 'region_name');
        $this->data['suburb'] = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());

        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());

        $this->data['regions'] = $this->region_model->get_regions();
//       debug($this->data['regions']);
        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/datepicker.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", true);
        Template::add_js(base_url() . "assets/js/bootstrap-datepicker.js", true);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", true);

        Template::render('admin/form', $this->data);
    }

    public function edit($id = 0)
    {

        $id = (int) $id;

        $id || redirect('admin/offers');

        $this->data['offer_detail']  = $this->offer_model->get($id);
        $this->data['slider_detail'] = $this->offer_model->slider_detail($id);
        $this->data['promo']         = $this->offer_model->get_promocodes($id);
//        debug($this->data['promo'][0]);die;
        $this->data['address'] = $this->offer_model->get_address($id);

        $this->data['get_keywords'] = implode(',', array_map(function ($arg) {
            return $arg->title;
        }, $this->offer_model->get_keywords($id)));

        $keywords = $this->offer_model->get_keywords($id);
//        debug($this->data['get_keywords']);die;
        //        echo $this->db->last_query();die;
        //        if(is_array( $keywords)){
        //            foreach($keywords as $key){
        //                $test = array_implode(',',$key->title);
        //            }
        //        }

        $this->data['category_info'] = $this->category_model->get($this->data['offer_detail']->category_id);
//        $this->data['suburb'] = $this->offer_model->get_suburb();

        $this->data['suburb'] = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());

        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());

        $this->data['regions'] = $this->region_model->get_regions();

        //debug( implode($this->data['suburb']));die;
        //debug( json_encode($this->data['suburb']));die;
        $this->data['offer_detail'] || redirect('admin/offers/');
        $this->data['edit'] = true;

        if ($this->input->post()) {
            $this->_add_edit_offer($id);
        }

        //debug($this->category_model->get_category_detail($id));die;
        if ($this->data['category_info']->category_parent_id == 0) {
            $this->data['categories']     = render_select($this->category_model->get_category_detail(), 'category_id', $this->data['edit'] ? $this->data['offer_detail']->category_id : false, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank' => true));
            $this->data['sub_categories'] = render_select($this->category_model->get_category_detail($this->data['category_info']->category_id), 'sub_category_id', false, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank' => true));
        } else {
            $this->data['categories']     = render_select($this->category_model->get_category_detail(), 'category_id', $this->data['edit'] ? $this->data['category_info']->category_parent_id : false, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank' => true));
            $this->data['sub_categories'] = render_select($this->category_model->get_category_detail($this->data['category_info']->category_parent_id), 'sub_category_id', $this->data['edit'] ? $this->data['offer_detail']->category_id : false, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank' => true));
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/datepicker.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", true);
        Template::add_js(base_url() . "assets/js/bootstrap-datepicker.js", true);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", true);

        Template::render('admin/form', $this->data);
    }

    public function _add_edit_offer($id)
    {
        //debug($this->input->post('offer_image_path'));
        //        debug(array_filter($this->input->post('op_title')));die;
        $this->load->library('form_validation');
        $this->form_validation->CI = &$this;
//        debug($this->input->post());

        $data['offer_title'] = $this->input->post('offer_title');
        if ($this->input->post('sub_category_id') == null) {
            $data['category_id'] = $this->input->post('category_id');
        } else {
            $data['category_id'] = $this->input->post('sub_category_id');
        }

        $data['trading_address'] = $this->input->post('trading_address');
        $data['suburb']          = $this->input->post('suburb');
        $data['trading_phone']   = $this->input->post('trading_phone');
        $data['buynowlink']      = $this->input->post('buynowlink');

        $data['offer_short_desc'] = $this->input->post('offer_short_desc');
        $data['offer_desc']       = $this->input->post('offer_desc');
        $data['offer_tag']        = $this->input->post('offer_tag');
        $data['offer_price']      = $this->input->post('offer_price');
        $data['offer_condition']  = $this->input->post('offer_condition');
        $data['offer_website']    = $this->input->post('offer_website');
        if ($vDate = DateTime::createFromFormat('m/d/Y', $this->input->post('offer_valid_date'))) {
            $data['offer_valid_date'] = date('Y-m-d', $vDate->getTimestamp());
        } else {
            $data['offer_valid_date'] = date('Y-m-d');
        }

//        $data['offer_valid_date'] = date("Y-m-d", strtotime($this->input->post('offer_valid_date')));
        $data['region_id'] = $this->input->post('region_id');
        //debug( $data['offer_valid_date']); die;
        //        $data = $this->input->post();
        $this->form_validation->set_rules(
            array(
                array('field' => 'offer_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                array('field' => 'category_id', 'label' => 'Category', 'rules' => 'required'),
                array('field' => 'trading_address', 'label' => 'Trading address', 'rules' => 'required'),
                array('field' => 'trading_phone', 'label' => 'Trading phone', 'rules' => 'required'),
                array('field' => 'buynowlink', 'label' => 'buynowlink ', 'rules' => 'xss_clean'),
                array('field' => 'offer_short_desc', 'label' => 'Short Description', 'rules' => 'trim|min_length[2]|xss_clean'),
                array('field' => 'offer_desc', 'label' => ' Description', 'rules' => 'trim|min_length[2]|xss_clean'),
                array('field' => 'offer_tag', 'label' => ' Tag', 'rules' => 'required|trim|max_length[12]|xss_clean'),
                array('field' => 'offer_price', 'label' => ' price', 'rules' => 'trim|xss_clean'),
                array('field' => 'offer_condition', 'label' => ' condition', 'rules' => 'required|trim|xss_clean'),
                array('field' => 'offer_valid_date', 'label' => ' Date', 'rules' => 'required'),
                array('field' => 'slider_top', 'label' => 'Top', 'rules' => 'trim'),
                array('field' => 'slider_bottom', 'label' => 'Bottom', 'rules' => 'trim'),
                array('field' => 'offer_status', 'label' => 'Status', 'rules' => 'trim'),
                array('field' => 'region_id', 'label' => 'Region', 'rules' => 'required'),

            )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === false) {
            return false;
        }

        $offer_image_path = null;
        if (isset($_FILES['offer_image']['name']) and $_FILES['offer_image']['name'] != '') {

            if ($result = upload_image('offer_image', config_item('offer_image_root'), false)) {
                $image_path = config_item('offer_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
                ////                resize of file uploaded
                //                if ($width > 800 || $height > 600) {
                //                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'));
                //                }
                $offer_image_path = $result;
            } else {
//                 echo 'test';die;
                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return false;
            }
        } elseif ($id == 0) {

            $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
            return false;
        }

        unset($data['offer_image']);

        !$offer_image_path || ($data['offer_image'] = $offer_image_path);

        $data['offer_status'] = $this->input->post('offer_status') ? '1' : '0';
        $data['offer_name']   = $this->input->post('offer_name');

        if ($id == 0) {
            $offer_id = $this->offer_model->insert($data);

            $offer_address   = $this->input->post('offer_address');
            $offer_latitude  = $this->input->post('offer_lat');
            $offer_longitude = $this->input->post('offer_lon');
            $offer_phone     = $this->input->post('offer_phone');
            $offer_post_code = $this->input->post('offer_post_code');
            $offer_suburb    = $this->input->post('offer_suburb');

            $this->offer_address($offer_id, $offer_address, $offer_latitude, $offer_longitude, $offer_phone, $offer_post_code, $offer_suburb);

            $top      = $this->input->post('slider_top') ? '1' : '0';
            $bottom   = $this->input->post('slider_bottom') ? '1' : '0';
            $featured = $this->input->post('slider_featured') ? '1' : '0';
            $this->db->insert('slider', array('slider_type_id' => $offer_id,
                'slider_type'                                      => 'offer',
                'slider_top'                                       => $top,
                'slider_bottom'                                    => $featured));

            $keywords = array_filter(array_unique(explode(',', $this->input->post('keywords'))));
//        debug($keywords);die;
            $instkey = array();
            if (!empty($keywords)) {
                foreach ($keywords as $val) {
                    $instkey[] = array('type' => 1, 'type_id' => $offer_id, 'title' => trim($val));
                }

                $this->offer_model->insert_keywords($instkey);
            }

            $op_title = array_filter($this->input->post('op_title'));

            $op_promocode = array_filter($this->input->post('op_promocode'));
            $op_desc      = array_filter($this->input->post('op_desc'));

            $this->offer_promocodes($offer_id, $op_title, $op_promocode, $op_desc);

            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_logo = null;
            if ($offer_image_path) {
                $old_logo = $this->offer_model->get($id)->offer_image;
            }
            $this->offer_model->update($id, $data);
            if ($old_logo and file_exists(config_item('offer_image_root') . $old_logo)) {
                unlink_file(config_item('offer_image_root') . $old_logo);
            }

            $this->offer_model->delete_address($id);
            $offer_address   = $this->input->post('offer_address');
            $offer_latitude  = $this->input->post('offer_lat');
            $offer_longitude = $this->input->post('offer_lon');
            $offer_phone     = $this->input->post('offer_phone');
            $offer_post_code = $this->input->post('offer_post_code');
            $offer_suburb    = $this->input->post('offer_suburb');

            $this->offer_address($id, $offer_address, $offer_latitude, $offer_longitude, $offer_phone, $offer_post_code, $offer_suburb);

            $this->db->delete('slider', array('slider_type_id' => $id));
            $top      = $this->input->post('slider_top') ? '1' : '0';
            $bottom   = $this->input->post('slider_bottom') ? '1' : '0';
            $featured = $this->input->post('slider_featured') ? '1' : '0';

            $this->db->insert('slider', array('slider_type_id' => $id,
                'slider_type'                                      => 'offer',
                'slider_top'                                       => $top,
                'slider_featured'                                  => $featured,
                'slider_bottom'                                    => $bottom,
            ));

            $this->offer_model->delete_keywords($id);
            $keywords = array_filter(array_unique(explode(',', $this->input->post('keywords'))));
//        debug($keywords);die;
            $instkey = array();
            if (!empty($keywords)) {
                foreach ($keywords as $val) {
                    $instkey[] = array('type' => 1, 'type_id' => $id, 'title' => trim($val));
                }
            }

            if (!empty($instkey)) {
                $this->offer_model->insert_keywords($instkey);
            }

            $op_title     = array_filter($this->input->post('op_title'));
            $op_promocode = array_filter($this->input->post('op_promocode'));
            $op_desc      = array_filter($this->input->post('op_desc'));

            $this->offer_model->delete_promo_codes($id);
            $this->offer_promocodes($id, $op_title, $op_promocode, $op_desc);

            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/offers/');
    }

    public function offer_address($id, $offer_address, $offer_latitude, $offer_longitude, $offer_phone, $offer_post_code, $offer_suburb)
    {

        $batchArray = array();

        if (is_array($offer_address)) {
            foreach ($offer_address as $k => $add) {

                $batchArray[$k]['oa_address'] = $add;
                $batchArray[$k]['offer_id']   = $id;
            }
        }
        // if (is_array($offer_latitude)) {
        //     foreach ($offer_latitude as $k => $lat) {
        //         $batchArray[$k]['oa_latitude'] = $lat;
        //     }
        // }
        // if (is_array($offer_longitude)) {
        //     foreach ($offer_longitude as $k => $lon) {
        //         $batchArray[$k]['oa_longitude'] = $lon;
        //     }
        // }
        if (is_array($offer_phone)) {
            foreach ($offer_phone as $k => $phone) {
                $batchArray[$k]['oa_phone'] = $phone;
            }
        }
        if (is_array($offer_post_code)) {
            foreach ($offer_post_code as $k => $offer_post_code) {
                $batchArray[$k]['oa_post_code'] = $offer_post_code;
            }
        }
        if (is_array($offer_suburb)) {
            foreach ($offer_suburb as $k => $offer_suburb) {
                $batchArray[$k]['oa_suburb']    = $offer_suburb;
                $address_data                   = get_lat_long_generic($offer_suburb);
                $lat                            = $address_data['lat'];
                $long                           = $address_data['long'];
                $batchArray[$k]['oa_latitude']  = $lat;
                $batchArray[$k]['oa_longitude'] = $long;
            }
        }
        if (!empty($batchArray)) {
            $this->db->insert_batch('offer_address', $batchArray);
        }

    }

    public function offer_promocodes($id, $op_title, $op_promocode, $op_desc)
    {

        $batchArray = array();

        if (is_array($op_title)) {
            foreach ($op_title as $k => $title) {

                $batchArray[$k]['op_title'] = $title;
                $batchArray[$k]['offer_id'] = $id;
            }
        }
        if (is_array($op_promocode)) {
            foreach ($op_promocode as $k => $lat) {
                $batchArray[$k]['op_promocode'] = $lat;
            }
        }
        if (is_array($op_desc)) {
            foreach ($op_desc as $k => $lon) {
                $batchArray[$k]['op_description'] = $lon;
            }
        }

        if (!empty($batchArray)) {
            $this->db->insert_batch('offer_promocodes', $batchArray);
        }

    }

    public function sub_cat_info()
    {
        $id      = $this->uri->segment(4);
        $sub_cat = $this->input->post('sub_cat_id');
        //  echo ($sub_cat_id);die;
        $data   = $this->category_model->get_subcategory($id);
        $result = '';
        $result = '<select id="ChildCat" class="form-control" name="sub_category_id" >';
        $result .= '<option value="">Select Sub Category</option>';
        foreach ($data as $c) {
            $result .= '<option value=" ' . $c->category_id . '"' . (($sub_cat == $c->category_id) ? "selected=\"selected\" " : "") . ' >' . $c->category_name . '</option>';
        }
        $result .= '</select>';
        echo $result;
    }

    public function toggle_status()
    {
        if ($this->input->is_ajax_request()) {
            $id     = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->offer_model->update($id, array('offer_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    public function toggle_review_status()
    {
        if ($this->input->is_ajax_request()) {
            $id     = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->load->model(array('review_model'));
            $this->review_model->update($id, array('status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    public function delete($id)
    {
        $old_logo = $this->offer_model->get($id)->offer_image;
        if ($this->offer_model->delete($id)) {
            if ($old_logo and file_exists(config_item('offer_image_root') . $old_logo)) {
                unlink_file(config_item('offer_image_root') . $old_logo);
            }
            $this->offer_model->deleteall($id);
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/offers/');
    }

    public function delete_review($id)
    {
        $this->load->model(array('review_model'));
        if ($this->review_model->delete($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect($_SERVER['HTTP_REFERER'] ?: 'admin/company/');
    }

    public function _check_suburb($str)
    {
        if (!empty($_POST['company_suburb']) && !empty($str) && $str && $str != 0) {

            return true;
        } else {
            $this->form_validation->set_message('_check_suburb', 'Please select %s from the suggestion.');
            return false;
        }
    }

    public function reviews($company_id = null)
    {
        $company_id || redirect('admin/clubs');

        $this->load->model(array('review_model'));

        $this->data['reviews']   = $this->review_model->find_all_by('company_id', $company_id);
        $this->data['companies'] = render_select($this->company_model->get_all(), 'company_id', $company_id, 'style="width:225px; display:inline;" class="form-control" onchange=\' window.location.href = "' . base_url('admin/company/reviews') . '/"+this.value; \' ', 'company_id', 'company_name');

        Template::add_js(base_url() . "assets/lib/jquery.showmore.min.js", true);
        Template::render('admin/company_reviews', $this->data);
    }

    public function import_csv_data()
    {
        $countData = $this->db->count_all('offers');

        if ($this->xls_upload()) {
            if ($countData != 0) {
                $this->db->truncate('offer_address');
                $this->db->truncate('offers');
            }
            $this->load->library('my_phpexcel');
            $this->uploaded_xls_file = $this->xls_file_data['full_path'];
            $inputFileType           = 'CSV';
            $inputFileName           = $this->uploaded_xls_file;
            $objReader               = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel             = $objReader->load($inputFileName);

            $worksheet = $objPHPExcel->getActiveSheet();

            $highestRow         = $worksheet->getHighestRow();
            $highestColumn      = $worksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            //get header name and informations

            $req_header = array(
                'mail_business_name'
                , 'business_name'
                , 'image'
                , 'region'
                , 'primary_category'
                , 'keywords'
                , 'trading_phone'
                , 'trading_address'
                , 'suburb'
                , 'state'
                , 'postcode'
                , 'website'
                , 'feature',
            );

            $csv_header_title     = array();
            $csv_header_title_col = array();

            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, 1);
                $val  = $cell->getValue();
                if (!empty($val)) {
                    $csv_header_title[]                     = strtolower($val);
                    $csv_header_title_col[strtolower($val)] = $col;
                }
            }

            //dd(array_diff($req_header, $csv_header_title));
            if (!empty(array_diff($req_header, $csv_header_title))) {
                $this->session->set_flashdata('error_message', 'Error csv file.');
                redirect(current_url());
                // echo 'error csv file';
                // die();
            }

            $log_file = FCPATH . 'application/logs/' . uniqid() . '.txt';
            file_put_contents($log_file, PHP_EOL . 'Import Errors: ' . PHP_EOL);

            for ($row = 2; $row <= $highestRow; ++$row) {

                $insArr = array();

                //for id
                $insArr['offer_id'] = isset($csv_header_title_col['offer_id']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_id'], $row)->getValue()) : '';

                //for title
                $insArr['title'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['mail_business_name'], $row)->getValue());

                $insArr['region']        = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['region'], $row)->getValue());
                $insArr['keywords']      = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['keywords'], $row)->getValue());
                $insArr['offer_website'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['website'], $row)->getValue());
                // $insArr['offer_price']     = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_price'], $row)->getValue());
                $insArr['offer_condition'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['gen_cond'], $row)->getValue());
                //for category
                $insArr['category'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['primary_category'], $row)->getValue());
                //$insArr['sub_category'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['subcategory'], $row)->getValue());

                //for image
                $insArr['image'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['image'], $row)->getValue());

                //for short_description
                $insArr['short_description'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['short_description'], $row)->getValue());

                //for description
                $insArr['offer_desc'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['description'], $row)->getValue());

                //for promocodes
                $insArr['offer_promocode_title_1']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_1'], $row)->getValue());
                $insArr['offer_promocode_1']             = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['code_1'], $row)->getValue());
                $insArr['offer_promocode_description_1'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['cond_1'], $row)->getValue());

                $insArr['offer_promocode_title_2']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_2'], $row)->getValue());
                $insArr['offer_promocode_2']             = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['code_2'], $row)->getValue());
                $insArr['offer_promocode_description_2'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['cond_2'], $row)->getValue());

                $insArr['offer_promocode_title_3']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_3'], $row)->getValue());
                $insArr['offer_promocode_3']             = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['code_3'], $row)->getValue());
                $insArr['offer_promocode_description_3'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['cond_3'], $row)->getValue());

                /* $insArr['offer_promocode_title_4']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_4'], $row)->getValue());
                $insArr['offer_promocode_4']             = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['code_4'], $row)->getValue());
                $insArr['offer_promocode_description_4'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['cond_4'], $row)->getValue());

                $insArr['offer_promocode_title_5']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_5'], $row)->getValue());
                $insArr['offer_promocode_5']             = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['code_5'], $row)->getValue());
                $insArr['offer_promocode_description_5'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['cond_5'], $row)->getValue());*/

                //for tag
                $insArr['tag'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['feature'], $row)->getValue());

                //for start_date
                $insArr['start_date'] = isset($csv_header_title_col['start_date']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['start_date'], $row)->getValue()) : '';

                //for end date
                //$insArr['end_date'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['end_date'], $row)->getValue());

                //for street_address
                // $insArr['street_address'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['street_address'], $row)->getValue());

                //for latitude
                $insArr['latitude'] = isset($csv_header_title_col['latitude']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['latitude'], $row)->getValue()) : '';

                //for longitude
                $insArr['longitude'] = isset($csv_header_title_col['longitude']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['longitude'], $row)->getValue()) : '';

                //for phone
                $insArr['phone'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['trading_phone'], $row)->getValue());

                //for postcode
                $insArr['post_code'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['postcode'], $row)->getValue());

                //for suburb
                $insArr['suburb']          = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['suburb'], $row)->getValue());
                $insArr['state']           = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['state'], $row)->getValue());
                $insArr['trading_address'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['trading_address'], $row)->getValue());
                $insArr['trading_phone']   = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['trading_phone'], $row)->getValue());

                //for status
                $insArr['status'] = 1/*trim($worksheet->getCellByColumnAndRow($csv_header_title_col['status'], $row)->getValue())*/;

                //for offer_name
                $insArr['offer_name'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['mail_business_name'], $row)->getValue());

                $this->error_csv = array();
                $this->_update_data($insArr);

                if (!empty($this->error_csv)) {

                    $data = PHP_EOL . ' Row ' . $row . ': ' . implode(PHP_EOL . "\t", $this->error_csv);
                    file_put_contents($log_file, $data, FILE_APPEND);
                    $this->session->set_flashdata('log_data', $log_file);
                }
                $insArr          = null;
                $this->error_csv = null;
                $data            = null;
            }

            unlink($this->uploaded_xls_file);
            $this->session->set_flashdata('success_message', 'Import Successfully Done.');
            redirect(current_url());
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);

        Template::set('title', 'Import csv');
        Template::set_base_title('Import csv');
        Template::render('admin/import_csv_form', $this->data);
    }

    public function _update_data($data)
    {
        /*echo "<pre>";
        print_r($data);
        echo "<pre>";
        die();*/
        $offer_id = (int) $data['offer_id'];
        //check offer id
        if (!$this->offer_model->get($offer_id)) {
            $offer_id = 0;
        }

        //check category
        $category_id     = null;
        $sub_category_id = null;
        if (!empty($data['category'])) {
            $category = $this->category_model->find_by('category_name', $data['category']);

            if ($category) {
                $category_id = $category->category_id;
            } else {
                // insert new category
                $category_id = $this->category_model->insert(array(
                    'category_name'      => $data['category'],
                    'category_parent_id' => 0,
                    'category_status'    => 1,
                ));
            }

            //for sub category
            /* if (!empty($data['sub_category'])) {
        $sub_category = $this->category_model->where('category_parent_id', $category_id)->find_by('category_name', $data['sub_category']);

        if ($sub_category) {
        $sub_category_id = $sub_category->category_id;
        } else {

        $sub_category_id = $this->category_model->insert(array(
        'category_name'      => $data['sub_category'],
        'category_parent_id' => $category_id,
        'category_status'    => 1,
        ));
        }
        }*/
        }

        $category_id = $sub_category_id ?: $category_id;

        $region_id = 0;
        if (!empty($data['region'])) {
            $region = $this->region_model->find_by('region_name', $data['region']);

            if ($region) {
                $region_id = $region->region_id;
            } else {
                // insert new region
                $region_id = $this->region_model->insert(array(
                    'region_name'   => $data['region'],
                    'region_status' => 1,
                ));
            }
        }

        //address
        $offer_address = array();
        if (!empty($data['trading_address']) or
            !empty($data['latitude']) or
            !empty($data['longitude']) or
            !empty($data['phone']) or
            !empty($data['state']) or
            !empty($data['post_code']) or
            !empty($data['suburb'])) {

            $offer_address = array(
                'oa_address'   => $data['trading_address'],
                'oa_latitude'  => $data['latitude'],
                'oa_longitude' => $data['longitude'],
                'oa_phone'     => $data['phone'],
                'oa_post_code' => $data['post_code'],
                'oa_suburb'    => $data['suburb'],
                'oa_state'     => $data['state'],
            );

            if ((empty($offer_address['oa_latitude']) || empty($offer_address['oa_longitude'])) and (
                !empty($data['trading_address']) or
                !empty($data['post_code']) or
                !empty($data['suburb'])
            )) {

                // get latitude longitude from address
                //{$offer_address['oa_address']}
                if ($geocode = geocode("{$offer_address['oa_suburb']} {$offer_address['oa_post_code']} {$offer_address['oa_state']}  australia")) {
                    //echo "<pre>"; print_r($geocode); exit;
                    $offer_address['oa_latitude']  = $geocode[0];
                    $offer_address['oa_longitude'] = $geocode[1];
                }
            }
            //echo "out"; exit;
        }

        // check if image exists
        if (!empty($data['image']) and file_exists(FCPATH . 'assets/uploads/temp_images/' . $data['image'])) {

            $data['image'] = move_file(FCPATH . 'assets/uploads/temp_images/', config_item('offer_image_root'), $data['image']);
        } else {
            $data['image'] = '';
        }

        //echo "<pre>"; print_r($data); exit;
        //offer data

        /* echo "<pre>";
        print_r($data);
        die();*/
        $offerArr = array(
            'offer_title' => $data['title']
            , 'trading_address' => $data['trading_address']
            , 'trading_phone' => $data['trading_phone']
            , 'suburb' => $data['suburb']
            , 'category_id' => $category_id
            , 'region_id' => $region_id
//          , 'offer_image' => $data['image']
            , 'offer_short_desc' => $data['short_description']
            , 'offer_desc' => "" //$data['description']
            , 'offer_tag' => $data['tag']
            , 'offer_price' => "" //$data['offer_price']
            , 'offer_condition' => $data['offer_condition']
            , 'gen_cond' => $data['offer_condition']
            , 'offer_website' => $data['offer_website']
            , 'offer_name' => $data['offer_name']
            , 'offer_valid_date' => "05/20/2019"
            , 'offer_status' => "1"
            , 'offer_desc' => $data['offer_desc'],
        );

        if ($data['image']) {
            $offerArr['offer_image'] = $data['image'];
        }

        $new_offer = false;
        if (!empty($offerArr['offer_title'])) {
            $new_offer           = true;
            $this->last_offer_id = null;
            //validation

            if (!$category_id) {
                $this->error_csv[] = 'Data Not Inserted. Error in category/subcategory. Category is required field.';
                return false;
            }
            //check valid date
            if (!empty($offerArr['offer_valid_date'])) {
                if ($vDate = DateTime::createFromFormat('d/m/Y', $offerArr['offer_valid_date'])) {
                    $offerArr['offer_valid_date'] = date('Y-m-d', $vDate->getTimestamp());
                } else {
                    $offerArr['offer_valid_date'] = date('Y-m-d');
                    $this->error_csv[]            = ' Invalid date';
                }
//                $end_date = !empty($data['offer_valid_date']) && _date_is_valid($data['end_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $data['offer_valid_date']))) : '';
                //        debug($offerArr['offer_valid_date']);die;
            }

            if ($offer_id == 0) {
                $offer_id            = $this->offer_model->insert($offerArr);
                $this->last_offer_id = $offer_id;
            } else {
                $this->offer_model->update($offer_id, $offerArr);
                $this->last_offer_id = $offer_id;

                $this->offer_model->delete_address($offer_id);

                $this->offer_model->delete_keywords($offer_id);
            }

            //insert keywords
            if ($this->last_offer_id and !empty($data['keywords'])) {

                $keywords = explode(',', $data['keywords']);

                $instkey = array();
                foreach ($keywords as $val) {
                    $instkey[] = array('type' => 1, 'type_id' => $offer_id, 'title' => trim($val));
                }

                $this->offer_model->insert_keywords($instkey);
            }

            // for promocodes
            if ($this->last_offer_id) {
                $opTitle     = array();
                $opPromocode = array();
                $opDesc      = array();

                if ((isset($data['offer_promocode_title_1']) and !empty($data['offer_promocode_title_1'])) or (isset($data['offer_promocode_1']) and !empty($data['offer_promocode_1'])) or (isset($data['offer_promocode_description_1']) and !empty($data['offer_promocode_description_1']))) {
                    $opTitle[1]     = isset($data['offer_promocode_title_1']) ? $data['offer_promocode_title_1'] : '';
                    $opPromocode[1] = isset($data['offer_promocode_1']) ? $data['offer_promocode_1'] : '';
                    $opDesc[1]      = isset($data['offer_promocode_description_1']) ? $data['offer_promocode_description_1'] : '';
                }

                if ((isset($data['offer_promocode_title_2']) and !empty($data['offer_promocode_title_2'])) or (isset($data['offer_promocode_2']) and !empty($data['offer_promocode_2'])) or (isset($data['offer_promocode_description_2']) and !empty($data['offer_promocode_description_2']))) {
                    $opTitle[2]     = isset($data['offer_promocode_title_2']) ? $data['offer_promocode_title_2'] : '';
                    $opPromocode[2] = isset($data['offer_promocode_2']) ? $data['offer_promocode_2'] : '';
                    $opDesc[2]      = isset($data['offer_promocode_description_2']) ? $data['offer_promocode_description_2'] : '';
                }

                if ((isset($data['offer_promocode_title_3']) and !empty($data['offer_promocode_title_3'])) or (isset($data['offer_promocode_3']) and !empty($data['offer_promocode_3'])) or (isset($data['offer_promocode_description_3']) and !empty($data['offer_promocode_description_3']))) {
                    $opTitle[3]     = isset($data['offer_promocode_title_3']) ? $data['offer_promocode_title_3'] : '';
                    $opPromocode[3] = isset($data['offer_promocode_3']) ? $data['offer_promocode_3'] : '';
                    $opDesc[3]      = isset($data['offer_promocode_description_3']) ? $data['offer_promocode_description_3'] : '';
                }

                if ((isset($data['offer_promocode_title_4']) and !empty($data['offer_promocode_title_4'])) or (isset($data['offer_promocode_4']) and !empty($data['offer_promocode_4'])) or (isset($data['offer_promocode_description_4']) and !empty($data['offer_promocode_description_4']))) {
                    $opTitle[4]     = isset($data['offer_promocode_title_4']) ? $data['offer_promocode_title_4'] : '';
                    $opPromocode[4] = isset($data['offer_promocode_4']) ? $data['offer_promocode_4'] : '';
                    $opDesc[4]      = isset($data['offer_promocode_description_4']) ? $data['offer_promocode_description_4'] : '';
                }

                if ((isset($data['offer_promocode_title_5']) and !empty($data['offer_promocode_title_5'])) or (isset($data['offer_promocode_5']) and !empty($data['offer_promocode_5'])) or (isset($data['offer_promocode_description_5']) and !empty($data['offer_promocode_description_5']))) {
                    $opTitle[5]     = isset($data['offer_promocode_title_5']) ? $data['offer_promocode_title_5'] : '';
                    $opPromocode[5] = isset($data['offer_promocode_5']) ? $data['offer_promocode_5'] : '';
                    $opDesc[5]      = isset($data['offer_promocode_description_5']) ? $data['offer_promocode_description_5'] : '';
                }

                if (!empty($opTitle) or !empty($opPromocode) or !empty($opDesc)) {
                    $this->offer_model->delete_promo_codes($this->last_offer_id);
                    $this->offer_promocodes($this->last_offer_id, $opTitle, $opPromocode, $opDesc);

                }

            }

        }

        if ($this->last_offer_id and !empty($offer_address)) {

            //update address
            $offer_address['offer_id'] = $this->last_offer_id;
            $this->offer_model->insert_offer_address($offer_address);
        } elseif ($this->last_offer_id and !$new_offer) {
            $this->error_csv[] = 'Data Not Inserted.';
        }

    }

    public function edit_csv()
    {

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);

        Template::set('title', 'Edit csv');
        Template::set_base_title('Edit csv');
        Template::render('admin/edit_csv_form', $this->data);
    }

    public function up_editable()
    {
        $countData = $this->db->count_all('offers');

        if ($this->xls_upload()) {
            /* if ($countData != 0) {
            $this->db->truncate('offer_address');
            $this->db->truncate('offers');
            }*/
            $this->load->library('my_phpexcel');
            $this->uploaded_xls_file = $this->xls_file_data['full_path'];
            $inputFileType           = 'CSV';
            $inputFileName           = $this->uploaded_xls_file;
            $objReader               = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel             = $objReader->load($inputFileName);

            $worksheet = $objPHPExcel->getActiveSheet();

            $highestRow         = $worksheet->getHighestRow();
            $highestColumn      = $worksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            //get header name and informations

            $req_header = array(
                'mail_business_name'
                , 'business_name'
                , 'image'
                , 'region'
                , 'primary_category'
                , 'keywords'
                , 'trading_phone'
                , 'trading_address'
                , 'suburb'
                , 'state'
                , 'postcode'
                , 'website'
                , 'feature'
            );

            $csv_header_title     = array();
            $csv_header_title_col = array();

            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, 1);
                $val  = $cell->getValue();
                if (!empty($val)) {
                    $csv_header_title[]                     = strtolower($val);
                    $csv_header_title_col[strtolower($val)] = $col;
                }
            }

            //dd(array_diff($req_header, $csv_header_title));
            if (!empty(array_diff($req_header, $csv_header_title))) {
                $this->session->set_flashdata('error_message', 'Error csv file.');
                redirect(current_url());
                // echo 'error csv file';
                // die();
            }

            $log_file = FCPATH . 'application/logs/' . uniqid() . '.txt';
            file_put_contents($log_file, PHP_EOL . 'Import Errors: ' . PHP_EOL);

            for ($row = 2; $row <= $highestRow; ++$row) {
               // if ($worksheet->getCellByColumnAndRow($csv_header_title_col['edited'], $row)->getValue() == "1") {

                    $insArr                                  = array();
                    $insArr['offer_id']                      = isset($csv_header_title_col['offer_id']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_id'], $row)->getValue()) : '';
                    $insArr['title']                         = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['mail_business_name'], $row)->getValue());
                    $insArr['region']                        = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['region'], $row)->getValue());
                    $insArr['keywords']                      = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['keywords'], $row)->getValue());
                    $insArr['offer_website']                 = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['website'], $row)->getValue());
                    $insArr['offer_condition']               = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['gen_cond'], $row)->getValue());
                    $insArr['category']                      = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['primary_category'], $row)->getValue());
                    $insArr['image']                         = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['image'], $row)->getValue());
                    $insArr['short_description']             = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['short_description'], $row)->getValue());
                    $insArr['offer_desc']                    = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['description'], $row)->getValue());
                    $insArr['offer_promocode_title_1']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_1'], $row)->getValue());
                    $insArr['offer_promocode_1']             = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['code_1'], $row)->getValue());
                    $insArr['offer_promocode_description_1'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['cond_1'], $row)->getValue());

                    $insArr['offer_promocode_title_2']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_2'], $row)->getValue());
                    $insArr['offer_promocode_2']             = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['code_2'], $row)->getValue());
                    $insArr['offer_promocode_description_2'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['cond_2'], $row)->getValue());

                    $insArr['offer_promocode_title_3']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['offer_3'], $row)->getValue());
                    $insArr['offer_promocode_3']             = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['code_3'], $row)->getValue());
                    $insArr['offer_promocode_description_3'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['cond_3'], $row)->getValue());
                    $insArr['tag']                           = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['feature'], $row)->getValue());
                    $insArr['start_date']                    = isset($csv_header_title_col['start_date']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['start_date'], $row)->getValue()) : '';
                    $insArr['latitude']                      = isset($csv_header_title_col['latitude']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['latitude'], $row)->getValue()) : '';
                    $insArr['longitude']                     = isset($csv_header_title_col['longitude']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['longitude'], $row)->getValue()) : '';
                    $insArr['phone']                         = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['trading_phone'], $row)->getValue());
                    $insArr['post_code']                     = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['postcode'], $row)->getValue());
                    $insArr['suburb']                        = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['suburb'], $row)->getValue());
                    $insArr['state']                         = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['state'], $row)->getValue());
                    $insArr['trading_address']               = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['trading_address'], $row)->getValue());
                    $insArr['trading_phone']                 = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['trading_phone'], $row)->getValue());
                    $insArr['status']                        = 1;
                    $insArr['offer_name']                    = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['mail_business_name'], $row)->getValue());

                    $this->error_csv = array();
                    $this->_update_data($insArr);
                    if (!empty($this->error_csv)) {

                        $data = PHP_EOL . ' Row ' . $row . ': ' . implode(PHP_EOL . "\t", $this->error_csv);
                        file_put_contents($log_file, $data, FILE_APPEND);
                        $this->session->set_flashdata('log_data', $log_file);
                    }
                    $insArr          = null;
                    $this->error_csv = null;
                    $data            = null;

                }
         //   }

            unlink($this->uploaded_xls_file);
            $this->session->set_flashdata('success_message', 'Edit Successfully Done.');
            redirect('admin/offers/edit_csv');
        }
    }

    public function xls_upload()
    {

        $config['upload_path']   = './assets/uploads/csv';
        $config['allowed_types'] = 'csv';
        $config['max_size']      = '1024';

        $this->load->library('upload', $config);
        //print_r($_FILES);
        if (!$this->upload->do_upload()) {
            //echo $this->upload->display_errors(); exit;
            return false;
        } else {
            $this->xls_file_data = $this->upload->data();
            return true;
        }
    }




    public function offer_csv()
    {
        $this->load->library('my_phpexcel');

        $sheet = $this->my_phpexcel->getActiveSheet();

        $this->my_phpexcel->getProperties()
            ->setCreator("Report")
            ->setLastModifiedBy("ADMIN")
            ->setTitle('Report')
            ->setSubject('Report')
            ->setDescription('Report')
            ->setKeywords("")
            ->setCategory("");

//        echo "<pre>";
        //        print_r($offer_arr);

        $this->my_phpexcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'offer_id')
            ->setCellValue('B1', 'mail_business_name')
            ->setCellValue('C1', 'business_name')
            ->setCellValue('D1', 'image')
            ->setCellValue('E1', 'region')
            ->setCellValue('F1', 'primary_category')
            ->setCellValue('G1', 'keywords')
            ->setCellValue('H1', 'trading_phone') //special desc.
            ->setCellValue('I1', 'trading_address') //tag
            ->setCellValue('J1', 'suburb') //end date
            ->setCellValue('K1', 'state') //status
            ->setCellValue('L1', 'postcode') //trading add
            ->setCellValue('M1', 'website') //trading phone
            ->setCellValue('N1', 'feature')
            ->setCellValue('O1', 'offer_1')
            ->setCellValue('P1', 'code_1')
            ->setCellValue('Q1', 'cond_1')
            ->setCellValue('R1', 'offer_2')
            ->setCellValue('S1', 'code_2')
            ->setCellValue('T1', 'cond_2')
            ->setCellValue('U1', 'offer_3')
            ->setCellValue('V1', 'code_3')
            ->setCellValue('W1', 'cond_3')
            ->setCellValue('X1', 'offer_4')
            ->setCellValue('Y1', 'code_4')
            ->setCellValue('Z1', 'cond_4')
            ->setCellValue('AA1', 'offer_5')
            ->setCellValue('AB1', 'code_5')
            ->setCellValue('AC1', 'cond_5')
            ->setCellValue('AD1', 'gen_cond')
            ->setCellValue('AE1', 'short_description')
            ->setCellValue('AF1', 'description')
        ;


        $checkBoxSelected = $this->input->post('selectedOffer');
        $formatter = explode(',', $checkBoxSelected);
        $club_id = 0;

        if (!empty($checkBoxSelected)){
          $offer_details = $this->offer_model->get_offer_csvf($club_id, $formatter);
        } else {
            $offer_details = $this->offer_model->get_offer_csv();
        }
       


        //$offer_details = $this->offer_model->get_offer_csv($club_id, $formatter);
        /*echo "<pre>";
        print_r($offer_details);
        die();*/
//         echo $this->db->last_query();
        //        debug($offer_details);die;

        if (!empty($offer_details)) {
            $offer_arr = array();
            foreach ($offer_details as $offer_detail) {
                if (!isset($offer_arr[$offer_detail->offer_id])) {
                    $offer_arr[$offer_detail->offer_id] = array(
                        'offer_id'           => $offer_detail->offer_id,
                        'mail_business_name' => $offer_detail->offer_title,
                        'business_name'      => $offer_detail->offer_title,
                        'image'              => '',
                        'region_name'        => $offer_detail->region_name ?: '',
                        'category_name'      => $offer_detail->category_name,
                        'keywords'           => $offer_detail->keywords ?: '',
                        'offer_website'      => $offer_detail->offer_website,
                        'offer_short_desc'   => strip_tags($offer_detail->offer_short_desc),
                        'offer_desc'         => strip_tags($offer_detail->offer_desc),
                        'offer_tag'          => $offer_detail->offer_tag,
                        'offer_valid_date'   => $offer_detail->offer_valid_date,
                        'offer_status'       => $offer_detail->offer_status,
                        'offer_condition'    => $offer_detail->offer_condition,
                        'offer_short_desc'   => $offer_detail->offer_short_desc,
                        'offer_desc'         => $offer_detail->offer_desc,
                        'op_title'           => '',
                        'op_promocode'       => '',
                        'op_description'     => '',
                    );
                    $off_po = $this->offer_model->get_csv_offer_postcode($offer_detail->offer_id);

                    if ($off_po) {
                        $offer_arr[$offer_detail->offer_id]['op_title']       = $off_po->op_title;
                        $offer_arr[$offer_detail->offer_id]['op_promocode']   = $off_po->op_promocode;
                        $offer_arr[$offer_detail->offer_id]['op_description'] = $off_po->op_description;

                    }

                }
                $off_add = $this->offer_model->get_csv_offer_address($offer_detail->offer_id);
                if ($off_add) {
                    $offer_arr[$offer_detail->offer_id]['address'][] = array(
                        'oa_address'   => $off_add->oa_address,
                        'oa_phone'     => $off_add->oa_phone,
                        'oa_post_code' => $off_add->oa_post_code,
                        'oa_suburb'    => $off_add->oa_suburb,
                        'oa_state'     => $off_add->oa_state,
                    );
                } else {
                    $offer_arr[$offer_detail->offer_id]['address'][] = array();
                }
            }

            //dd($off_add);
            $i = 2;
            foreach ($offer_arr as $offer) {

                $this->my_phpexcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $offer['offer_id'])
                    ->setCellValue('B' . $i, $offer['mail_business_name'])
                    ->setCellValue('C' . $i, $offer['mail_business_name'])
                    ->setCellValue('D' . $i, '')
                    ->setCellValue('E' . $i, $offer['region_name'])
                    ->setCellValue('F' . $i, $offer['category_name'])
                    ->setCellValue('G' . $i, $offer['keywords'])
                    ->setCellValue('H' . $i, $offer['offer_desc'])
                    ->setCellValue('I' . $i, $offer['offer_tag'])
                    ->setCellValue('J' . $i, date('d/m/Y', strtotime($offer['offer_valid_date'])))
                    ->setCellValue('K' . $i, $offer['offer_status'])
                    ->setCellValue('M' . $i, $offer['offer_website'])
                    ->setCellValue('N' . $i, $offer['offer_tag'])
                    ->setCellValue('AD' . $i, $offer['offer_condition'])
                    ->setCellValue('AE' . $i, $offer['offer_short_desc'])
                    ->setCellValue('AF' . $i, $offer['offer_desc']);

                $ttl  = explode('||', $offer['op_title']);
                $pcd  = explode('||', $offer['op_promocode']);
                $pdec = explode('||', $offer['op_description']);

                foreach ($ttl as $ind => $off) {
                    switch ($ind) {
                        case 0:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('O' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('P' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('Q' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        case 1:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('R' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('S' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('T' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        case 2:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('U' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('V' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('W' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        case 3:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('X' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('Y' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('Z' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        case 4:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('AA' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('AB' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('AC' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        default:
                            break;
                    }

                }

                if (!empty($offer['address'])) {
                    $add = explode('||', $offer['address'][0]['oa_address']);
                    $phn = explode('||', $offer['address'][0]['oa_phone']);
                    $sub = explode('||', $offer['address'][0]['oa_suburb']);
                    $pos = explode('||', $offer['address'][0]['oa_post_code']);
                    $ste = explode('||', $offer['address'][0]['oa_state']);
                    foreach ($add as $ind => $off) {
                        $j = $i + $ind;
                        $this->my_phpexcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $j, $offer['offer_id'])
                            ->setCellValue('I' . $j, @($add[$ind] ?: ''))
                            ->setCellValue('H' . $j, @($phn[$ind] ?: ''))
                            ->setCellValue('J' . $j, @($sub[$ind] ?: ''))
                            ->setCellValue('K' . $j, @($ste[$ind] ?: ''))
                            ->setCellValue('L' . $j, @($pos[$ind] ?: ''));

                        $j++;
                    }
                    $i = $j;
                }
            }
        }

        $this->my_phpexcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel5)
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        //header('Content-Type: application/vnd.ms-excel');
        // header('Content-Disposition: attachment;filename="Transaction_' . date('Y_m_d') . '.xls"');
        // header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        // header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        // header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        // header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        //  header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        //   header('Pragma: public'); // HTTP/1.0
        $objWriter = PHPExcel_IOFactory::createWriter($this->my_phpexcel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }

    public function offer_csv1()
    {
        $this->load->library('my_phpexcel');

        $sheet = $this->my_phpexcel->getActiveSheet();

        $this->my_phpexcel->getProperties()
            ->setCreator("Report")
            ->setLastModifiedBy("ADMIN")
            ->setTitle('Report')
            ->setSubject('Report')
            ->setDescription('Report')
            ->setKeywords("")
            ->setCategory("");

//        echo "<pre>";
        //        print_r($offer_arr);

        $this->my_phpexcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'offer_id')
            ->setCellValue('B1', 'mail_business_name')
            ->setCellValue('C1', 'business_name')
            ->setCellValue('D1', 'image')
            ->setCellValue('E1', 'region')
            ->setCellValue('F1', 'primary_category')
            ->setCellValue('G1', 'keywords')
            ->setCellValue('H1', 'trading_phone') //special desc.
            ->setCellValue('I1', 'trading_address') //tag
            ->setCellValue('J1', 'suburb') //end date
            ->setCellValue('K1', 'state') //status
            ->setCellValue('L1', 'postcode') //trading add
            ->setCellValue('M1', 'website') //trading phone
            ->setCellValue('N1', 'feature')
            ->setCellValue('O1', 'offer_1')
            ->setCellValue('P1', 'code_1')
            ->setCellValue('Q1', 'cond_1')
            ->setCellValue('R1', 'offer_2')
            ->setCellValue('S1', 'code_2')
            ->setCellValue('T1', 'cond_2')
            ->setCellValue('U1', 'offer_3')
            ->setCellValue('V1', 'code_3')
            ->setCellValue('W1', 'cond_3')
            ->setCellValue('X1', 'offer_4')
            ->setCellValue('Y1', 'code_4')
            ->setCellValue('Z1', 'cond_4')
            ->setCellValue('AA1', 'offer_5')
            ->setCellValue('AB1', 'code_5')
            ->setCellValue('AC1', 'cond_5')
            ->setCellValue('AD1', 'gen_cond')
            ->setCellValue('AE1', 'short_description')
            ->setCellValue('AF1', 'description')
            ->setCellValue('AG1', 'edited')
        ;
        $offer_details = $this->offer_model->get_offer_csv();
        /*echo "<pre>";
        print_r($offer_details);
        die();*/
//         echo $this->db->last_query();
        //        debug($offer_details);die;

        if (!empty($offer_details)) {
            $offer_arr = array();
            foreach ($offer_details as $offer_detail) {
                if (!isset($offer_arr[$offer_detail->offer_id])) {
                    $offer_arr[$offer_detail->offer_id] = array(
                        'offer_id'           => $offer_detail->offer_id,
                        'mail_business_name' => $offer_detail->offer_title,
                        'business_name'      => $offer_detail->offer_title,
                        'image'              => '',
                        'region_name'        => $offer_detail->region_name ?: '',
                        'category_name'      => $offer_detail->category_name,
                        'keywords'           => $offer_detail->keywords ?: '',
                        'offer_website'      => $offer_detail->offer_website,
                        'offer_short_desc'   => strip_tags($offer_detail->offer_short_desc),
                        'offer_desc'         => strip_tags($offer_detail->offer_desc),
                        'offer_tag'          => $offer_detail->offer_tag,
                        'offer_valid_date'   => $offer_detail->offer_valid_date,
                        'offer_status'       => $offer_detail->offer_status,
                        'offer_condition'    => $offer_detail->offer_condition,
                        'offer_short_desc'   => $offer_detail->offer_short_desc,
                        'offer_desc'         => $offer_detail->offer_desc,
                        'op_title'           => '',
                        'op_promocode'       => '',
                        'op_description'     => '',
                        'edited'             => '0',
                    );
                    $off_po = $this->offer_model->get_csv_offer_postcode($offer_detail->offer_id);

                    if ($off_po) {
                        $offer_arr[$offer_detail->offer_id]['op_title']       = $off_po->op_title;
                        $offer_arr[$offer_detail->offer_id]['op_promocode']   = $off_po->op_promocode;
                        $offer_arr[$offer_detail->offer_id]['op_description'] = $off_po->op_description;

                    }

                }
                $off_add = $this->offer_model->get_csv_offer_address($offer_detail->offer_id);
                if ($off_add) {
                    $offer_arr[$offer_detail->offer_id]['address'][] = array(
                        'oa_address'   => $off_add->oa_address,
                        'oa_phone'     => $off_add->oa_phone,
                        'oa_post_code' => $off_add->oa_post_code,
                        'oa_suburb'    => $off_add->oa_suburb,
                        'oa_state'     => $off_add->oa_state,
                        'edited'       => '0',
                    );
                } else {
                    $offer_arr[$offer_detail->offer_id]['address'][] = array();
                }
            }

            //dd($off_add);
            $i = 2;
            foreach ($offer_arr as $offer) {

                $this->my_phpexcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $offer['offer_id'])
                    ->setCellValue('B' . $i, $offer['mail_business_name'])
                    ->setCellValue('C' . $i, $offer['mail_business_name'])
                    ->setCellValue('D' . $i, '')
                    ->setCellValue('E' . $i, $offer['region_name'])
                    ->setCellValue('F' . $i, $offer['category_name'])
                    ->setCellValue('G' . $i, $offer['keywords'])
                    ->setCellValue('H' . $i, $offer['offer_desc'])
                    ->setCellValue('I' . $i, $offer['offer_tag'])
                    ->setCellValue('J' . $i, date('d/m/Y', strtotime($offer['offer_valid_date'])))
                    ->setCellValue('K' . $i, $offer['offer_status'])
                    ->setCellValue('M' . $i, $offer['offer_website'])
                    ->setCellValue('N' . $i, $offer['offer_tag'])
                    ->setCellValue('AD' . $i, $offer['offer_condition'])
                    ->setCellValue('AE' . $i, $offer['offer_short_desc'])
                    ->setCellValue('AF' . $i, $offer['offer_desc'])
                    ->setCellValue('AG' . $i, $offer['edited']);

                $ttl  = explode('||', $offer['op_title']);
                $pcd  = explode('||', $offer['op_promocode']);
                $pdec = explode('||', $offer['op_description']);

                foreach ($ttl as $ind => $off) {
                    switch ($ind) {
                        case 0:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('O' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('P' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('Q' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        case 1:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('R' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('S' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('T' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        case 2:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('U' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('V' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('W' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        case 3:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('X' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('Y' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('Z' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        case 4:
                            $this->my_phpexcel->setActiveSheetIndex(0)
                                ->setCellValue('AA' . $i, @($ttl[$ind] ?: ''))
                                ->setCellValue('AB' . $i, @($pcd[$ind] ?: ''))
                                ->setCellValue('AC' . $i, @($pdec[$ind] ?: ''))
                            ;
                            break;
                        default:
                            break;
                    }

                }

                if (!empty($offer['address'])) {
                    $add = explode('||', $offer['address'][0]['oa_address']);
                    $phn = explode('||', $offer['address'][0]['oa_phone']);
                    $sub = explode('||', $offer['address'][0]['oa_suburb']);
                    $pos = explode('||', $offer['address'][0]['oa_post_code']);
                    $ste = explode('||', $offer['address'][0]['oa_state']);
                    foreach ($add as $ind => $off) {
                        $j = $i + $ind;
                        $this->my_phpexcel->setActiveSheetIndex(0)
                            ->setCellValue('A' . $j, $offer['offer_id'])
                            ->setCellValue('I' . $j, @($add[$ind] ?: ''))
                            ->setCellValue('H' . $j, @($phn[$ind] ?: ''))
                            ->setCellValue('J' . $j, @($sub[$ind] ?: ''))
                            ->setCellValue('K' . $j, @($ste[$ind] ?: ''))
                            ->setCellValue('L' . $j, @($pos[$ind] ?: ''))
                            ->setCellValue('AG' . $j, @('0'));

                        $j++;
                    }
                    $i = $j;
                }
            }
        }

        $this->my_phpexcel->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Excel5)
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        //header('Content-Type: application/vnd.ms-excel');
        // header('Content-Disposition: attachment;filename="Transaction_' . date('Y_m_d') . '.xls"');
        // header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        // header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        // header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        // header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        //  header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        //   header('Pragma: public'); // HTTP/1.0
        $objWriter = PHPExcel_IOFactory::createWriter($this->my_phpexcel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }

}
