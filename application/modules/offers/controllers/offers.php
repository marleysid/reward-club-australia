<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Offers extends Front_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('offer_model', 'categories/category_model', 'advertisement/advertisement_model', 'deals/deal_model', 'region/region_model', 'reviews/review_model', 'settings/settings_model'));
        $this->load->library('pagination');
        //$this->load->library('Ajax_pagination');
        $this->load->library('session');

    }

    public function _remap($method, $params)
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else {
            return $this->index($method);
        }

    }

    public function index()
    {
        $this->session->set_userdata('referred_from', current_url());

        // echo "<pre>"; print_r($_SESSION); echo "</pre>";
        // echo "<pre>"; print_r($this->session->userdata); echo "</pre>";

        if ($this->input->get('location') != "") {
            $location = $this->input->get('location');
        } else {
            if (array_key_exists('customLocation', $this->session->userdata)) {
                $location = $this->session->userdata('customLocationId');
            } else {
                $location = '';
            }

        }

        // debug($_SERVER['QUERY_STRING']);
        $cat     = (array) $this->input->get('category');
        $sub_cat = (array) $this->input->get('sub_category');

//            $location = array_filter(explode('/', $this->input->get('location')));
        $latitude  = null;
        $longitude = null;
        $region    = 0;
        getCurrentAddress();
        $latitude  = $_SESSION['latitude'];
        $longitude = $_SESSION['longitude'];
        $distance  = $_SESSION['distance'];

        if ($this->input->get('location')) {
            $address = $this->input->get('location');
            $latlong = $this->get_lat_long($address);
            $map     = explode(',', $latlong);
            $mapLat  = $map[0];
            $mapLong = $map[1];

            $latitude  = $mapLat;
            $longitude = $mapLong;
        } else {
            if (array_key_exists('customLocationId', $this->session->userdata)) {
                $address = $this->session->userdata('customLocationId');
                $latlong = $this->get_lat_long($address);
                $map     = explode(',', $latlong);
                $mapLat  = $map[0];
                $mapLong = $map[1];

                $latitude  = $mapLat;
                $longitude = $mapLong;
            }
        }
        if ($this->input->get('distance') != "") {
            $distance = $this->input->get('distance');
            $_SESSION['distance'] = $distance;
        } else {
            $distance = $_SESSION['distance'];
        }
        //echo "here".$latitude; exit;
        $keyword = $this->input->get('keyword');

        $per_page   = (int) $this->input->get('page');
        $view_type  = $this->uri->segment(2) ? $this->uri->segment(2) : 'grid';
        $sort_order = $this->input->get('sort_option') ? $this->input->get('sort_option') : 'alpha';
        //debug($view_type);die;
        Template::add_js(get_asset('assets/frontend/js/cart_favourites.js'), true);
        if ($this->uri->segment(3) != '') {
            $config['per_page'] = $this->uri->segment(3);
        } else {
            $config['per_page'] = 9;
        }
        $count_rw = "";
        $result   = $this->offer_model->search_all($keyword, $cat, $sub_cat, $region, $location, $latitude, $longitude, $config['per_page'], $per_page, $sort_order, $distance, $count_rw);
        // debug($result);die;
        // echo $this->db->last_query();die;
        // debug($result);die;
        $count_rw             = "all";
        $config['total_rows'] = count($this->offer_model->search_all($keyword, $cat, $sub_cat, $region, $location, $latitude, $longitude, $config['per_page'], $per_page, $sort_order, $distance, $count_rw));

//        $config['base_url'] = site_url() . "information-for-members?{$_SERVER['QUERY_STRING']}";

        $get = $_GET;

        // unset the offset array item
        unset($get['page']);

        $config['base_url']       = current_url() . '/?' . http_build_query($get);
        $config['num_links']      = 2;
        $config['full_tag_open']  = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open']   = '<li>';
        $config['num_tag_close']  = '</li>';
        /* $config['cur_tag_open']      = "<li class='active'><a href='javascript:void(0)'>";*/
        $config['cur_tag_open']      = "<li class='active'><a href=''>";
        $config['cur_tag_close']     = "</a></li>";
        $config['next_tag_open']     = "<li class='next' >";
        $config['next_tagl_close']   = "</li>";
        $config['prev_tag_open']     = "<li class='prev'>";
        $config['prev_tagl_close']   = "</li>";
        $config['first_tag_open']    = "<li style='display:none;'>";
        $config['first_tagl_close']  = "</li>";
        $config['last_tag_open']     = "<li style='display:none;'>";
        $config['last_tagl_close']   = "</li>";
        $config['page_query_string'] = true;
        // $config['enable_query_strings'] = false;
        $config['query_string_segment'] = 'page';
        $config['first_link']           = '';
        $config['first_tag_open']       = '<div style="display:none;">';
        $config['first_tag_close']      = '</div>';
        $config['last_link']            = '';
        $config['first_tag_open']       = '<div style="display:none;">';
        $config['first_tag_close']      = '</div>';

        //echo $this->db->last_query();die;
        $this->pagination->initialize($config);
        if ($result != false) {
            $this->data['get_offers'] = $result;

        } else {
            $this->data['get_offers'] = array();
        }

        $this->data['category'] = $this->category_model->parent_cat();

        //debug($this->data['category']);die;

        $this->data['regions'] = $this->region_model->get_regions();

        // debug($this->data['categories']);
        // echo $this->db->last_query();die;
        $this->data['selected_cat']     = (array) $this->input->get('category');
        $this->data['selected_sub_cat'] = (array) $this->input->get('sub_category');
        $this->data['get_ads']          = $this->advertisement_model->get_left_side_ads();
//        echo $this->db->last_query();die;
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        $this->data['region_name']       = $this->region_model->get_region_name($this->input->get('region'));

        $this->data['total']    = $config['total_rows'];
        $this->data['per_page'] = $config['per_page'];
        $this->data['offest']   = $per_page;

        Template::add_css('assets/lib/jquery-raty/jquery.raty.css');
//        Template::add_js('assets/frontend/js/jquery.rateit.js');
        Template::add_js(base_url('assets/lib/jquery-raty/jquery.raty.js'));
        if ($view_type == 'list') {
            Template::render('frontend/offer_list_view', $this->data);
        } elseif ($view_type == 'map') {
            //$this->alltotal = $config['total_rows'];
            Template::render('frontend/map_view', $this->data);
        } else {
            Template::render('frontend/offer_grid_view', $this->data);
        }
    }

    public function map()
    {

        $cat      = (array) $this->input->get('category');
        $sub_cat  = (array) $this->input->get('sub_category');
        $location = array_filter(explode('/', $this->input->get('location')));

        $keyword      = $this->input->get('keyword');
        $category_idz = array_merge($cat, $sub_cat);

        $category_idz = array_unique(array_filter($category_idz));

        $category_idz = $this->offer_model->_filter_category($category_idz);

        $latitude  = null;
        $longitude = null;
        $region    = 0;
        $location  = '';

        if ($this->input->get('location')) {
            $location = $this->input->get('location');
            $location = array_filter(explode('/', $this->input->get('location')));
            $suburb   = @$location[0];
            $postcode = @$location[1];

            if ($postcode) {
                $this->db->where(array('postcode' => $postcode));
            }
            $add = $this->db->get_where('suburbs', array('suburb' => $suburb))->row();

            $latitude  = isset($add->lat) ? $add->lat : null;
            $longitude = isset($add->lon) ? $add->lon : null;

        } elseif ($this->input->get('current_location')) {
            $latitude  = $this->input->get('lat');
            $longitude = $this->input->get('lon');
        }

        if (!$longitude or !$longitude) {
            if ($this->input->get('location')) {
                $address = $this->input->get('location');
                $latlong = $this->get_lat_long($address);
                $map     = explode(',', $latlong);
                $mapLat  = $map[0];
                $mapLong = $map[1];

                $latitude  = $mapLat;
                $longitude = $mapLong;
            } else {
                if (array_key_exists('customLocationId', $this->session->userdata)) {
                    $address = $this->session->userdata('customLocationId');
                    $latlong = $this->get_lat_long($address);
                    $map     = explode(',', $latlong);
                    $mapLat  = $map[0];
                    $mapLong = $map[1];

                    $latitude  = $mapLat;
                    $longitude = $mapLong;
                }
            }
           /* $latitude  = '-33.867487';
            $longitude = '151.206990';*/
        }

        $this->data['user_location_lat'] = $latitude;
        $this->data['user_location_lon'] = $longitude;

        $result = $this->offer_model->nearby_offers($category_idz, $this->data['user_location_lat'], $this->data['user_location_lon'], 1000);

        //echo $this->db->last_query();die;
        //$total = count($result)-4;
        $rewards_lat = $this->settings_model->get_latlon('rewards_club_latitude');
        $rewards_lon = $this->settings_model->get_latlon('rewards_club_longitude');
        $locarray[]  = array('Rewards Club', $rewards_lat->value, $rewards_lon->value, 0, site_url());

        if ($result != false) {
            foreach ($result as $k => $res) {
                $locarray[] = array($res->offer_title, $res->oa_latitude, $res->oa_longitude, ++$k, site_url('offers/detail/offer/' . safe_b64encode($res->offer_id)));
            }

            //debug($this->data['location']);die;
        }

        $this->data['location'] = json_encode($locarray);
//        debug($this->data['location']);die;
        $this->data['category'] = $this->category_model->parent_cat();

        //debug($this->data['category']);die;

        $this->data['regions']     = $this->region_model->get_regions();
        $this->data['region_name'] = $this->region_model->get_region_name($this->input->get('region'));

        // debug($this->data['categories']);
        // echo $this->db->last_query();die;
        $this->data['selected_cat']      = (array) $this->input->get('category');
        $this->data['selected_sub_cat']  = (array) $this->input->get('sub_category');
        $this->data['get_ads']           = $this->advertisement_model->get_left_side_ads();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();

        Template::render('frontend/map_view', $this->data);
    }

    public function favourites()
    {

        $status = '';
        $id     = $this->uri->segment(4);
        $type   = $this->uri->segment(3);
        //echo $type;die;
        $member_id = $this->current_member->id;

        //  debug(count($this->db->query("select deal_id from deals where deal_id = '$id'")));die;
        //echo $member_id; echo $id;  die;
        //$query = $this->spdb->query("CALL sp_setFavourite(?,?,?)",array($id,$type,$member_id));
        // debug($this->db->query("select offer_id from offers where offer_id = '$id'")->num_rows() > 0);die;
        $fav = $this->offer_model->fav($id, $member_id);
        // echo $this->db->last_query();die;
        //debug($fav);die;
     
            if ($this->db->query("select offer_id from offers where offer_id = '$id'")->num_rows() > 0) {
                if ($fav == false) {
                    $this->db->insert("favourite", array('favourite_type' => $type,
                        'favourite_type_id'                                   => $id,
                        'member_id'                                           => $member_id));
                    $status = 1;
                    //$msg = $this->session->flashdata("Added to Favourites List");
                    echo json_encode(array('status' => $status));
                } else {
                    $status = 2;
                    echo json_encode(array('status' => $status));
                }
            }

    }

    public function cart()
    {
        $status     = '';
        $id         = $this->uri->segment(4);
        $type       = $this->uri->segment(3);
        $member_id  = $this->current_member->member_id;
        $cart       = $this->deal_model->cart($id, $member_id);
        $deals_quan = $this->deal_model->deals_quantity($id);

        //echo $this->db->last_query();die;

        if (count($this->db->query("select deal_id from deals where deal_id = '$id'")) > 0) {
            if ($cart == false) {
                if ($deals_quan->deal_quantity >= 1) {
                    $this->db->insert("cart_session", array('deal_id' => $id,
                        'member_id'                                       => $member_id,
                        'quantity'                                        => '1'));

                    // echo $this->db->last_query();die;
                    $this->db->set('deal_quantity', 'deal_quantity -1', false);
                    $this->db->where('deal_id', $id);
                    $this->db->update('deals');
                    //echo $this->db->last_query();die;
                    //echo 'success';die;
                    $status = 1;
                    echo json_encode(array('status' => $status));
                } else {
                    $status = 2;
                    echo json_encode(array('status' => $status));
                }
            } else {
                $status = 3;
                echo json_encode(array('status' => $status));
            }
        } else {
            $status = 0;

            echo json_encode(array('status' => $status));
        }
    }

    public function detail()
    {

        $type = $this->uri->segment(3);
        $id   = safe_b64decode($this->uri->segment(4));
        //dd($id);
        //echo $type;die;

        $this->data['detail'] = $this->offer_model->get_detail($id);

        $this->data['address'] = $this->offer_model->get_myaddress($id);
        $this->data['total']   = count($this->offer_model->get_all());
        //echo $this->db->last_query();die;
        $this->data['count_review'] = $this->review_model->total_review($id, 'offer');
        $this->data['get_reviews']  = $this->review_model->get_reviews($id, 'offer');

        $this->data['get_ads']           = $this->advertisement_model->ads_for_detail_page();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
//        echo $this->db->last_query();die;
        $this->data['category'] = $this->category_model->parent_cat();
        $this->data['regions']  = $this->region_model->get_regions();

        $this->data['postcode']         = $this->offer_model->get_postcodes($id);
        $this->data['selected_cat']     = (array) $this->input->get('category');
        $this->data['selected_sub_cat'] = (array) $this->input->get('sub_category');
        Template::add_css('assets/lib/jquery-raty/jquery.raty.css');
//        Template::add_js('assets/frontend/js/jquery.rateit.js');
        Template::add_js(base_url('assets/lib/jquery-raty/jquery.raty.js'));
        Template::add_js(base_url('assets/frontend/js/cart_favourites.js'));
        Template::render('frontend/detail', $this->data);
    }

    public function search_auto()
    {

        header('Content-Type: application/json');
        $this->load->model('offer_model');
        $input = strtolower($_GET['term']);
        $this->offer_model->get_autocomplete($input);

    }
    public function search_auto_refine()
    {

        header('Content-Type: application/json');
        $this->load->model('offer_model');
        $input = strtolower($_GET['term']);
        $this->offer_model->get_autocomplete_refine($input);
    }

    public function search_auto_suburbs()
    {
        header('Content-Type: application/json');
        $this->load->model('offer_model');
        $input = strtolower($_GET['term']);
        $this->offer_model->get_autocomplete_suburb($input);
    }

    public function searchOffer()
    {
        $keyword  = $this->input->get('keyword');
        $category = $this->input->get('category');

    }

    public function get_lat_long($address)
    {

        $address = str_replace(" ", "+", $address);

        $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
        $json = json_decode($json);

        // echo "<pre>"; print_r($json->results[0]->geometry->location->lat); echo "</pre>"; exit;
        // echo "<pre>"; print_r($json); echo "</pre>"; exit;
        //$lat  = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        //$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
        $lat  = $json->results[0]->geometry->location->lat;
        $long = $json->results[0]->geometry->location->lng;
        return $lat . ',' . $long;
    }

    public function locations()
    {
        $ip      = "49.195.15.60";
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        echo "<pre>";
        print_r(explode(",", $details->loc));

        die();
    }

}
