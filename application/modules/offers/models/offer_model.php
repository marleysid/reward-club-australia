<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Offer_model extends MY_Model {

    public $_search_total = 0;
    public $_order_by = 0;

    public function __construct() {
        parent::__construct();
        $this->table = 'offers';
        $this->field_prefix = 'offer_';
        $this->log_user = FALSE;
    }

    function get_all($return_type = 0) {
//      $this->select('rewards_company.*,rewards_category.*,concat(company_address," ", suburb," ",state," ",postcode) AS company_address', FALSE)
//              ->join('suburbs', 'suburbs.id = rewards_company.suburb_id')
//              ->join('rewards_category', 'rewards_category.category_id = rewards_company.category_id');
//      return parent::get_all($return_type);
        $this->db->select('offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_image`,offers.`offer_status`, category.`category_name`');
        $this->db->from('offers');
        $this->db->join('category', 'offers.category_id=category.category_id ', 'inner');
        $this->db->where("offers.club_id = '0'");
        $query = $this->db->get();
        return $query->result();
    }


   
    function offer_detail($id) {
        $this->db->select('offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_image`,offers.`offer_status`, category.`category_name`');
        $this->db->from('offers');
        $this->db->join('category', 'offers.category_id=category.category_id ', 'inner');
        $this->db->where("offers.club_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }

    function slider_detail($id) {
        $this->db->select("*")
                ->from("slider")
                ->where("slider_type_id = '$id' ");

        $query = $this->db->get();

        return $query->row();
    }

    function insert_offer_address($data) {
        $this->db->insert('offer_address', $data);
    }

    function delete_address($id) {
        return $query = $this->db->delete("offer_address", array("offer_id" => $id));
    }
    
    function delete_promo_codes($id) {
        return $query = $this->db->delete("offer_promocodes", array("offer_id" => $id));
    }

    function get_address($id) {
        $this->db->select("*")
                ->from("offer_address")
                ->where("offer_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }

    function get_suburb() {
        $query = $this->db->query("SELECT DISTINCT suburb FROM suburbs ORDER BY suburb ");
        return $query->result();
    }

    function suburb() {
        $query = $this->db->query("SELECT DISTINCT suburb FROM suburbs ORDER BY suburb limit 8");
        return $query->result();
    }

    function get_postcode() {
        $query = $this->db->query("SELECT DISTINCT postcode FROM suburbs ORDER BY postcode");
        return $query->result();
    }

    function get_state() {
        $query = $this->db->query("SELECT DISTINCT state FROM suburbs ORDER BY state ");
        return $query->result();
    }

    function state() {
        $query = $this->db->query("SELECT DISTINCT state FROM suburbs ORDER BY state");
        return $query->result();
    }

    function search($keyword, $category_idz, $postcode, $suburb, $latitude, $longitude, $limit, $offset, $order, $keyword_search = FALSE) {

        $order_list = array('alpha', 'nearest', 'new');
        $cat_idz = array();

        $order = in_array($order, $order_list) ? $order : 'alpha';

        switch ($order) {
            case 'alpha' :
                $order_by = 'offer_title';
                break;

            case 'nearest' :
                $order_by = 'distance';
                break;

            case 'new' :
                $order_by = 'offer_id desc';
                break;

            default :
                $order_by = 'offer_title';
                break;
        }

        $this->_order_by = $order;

        $category_idz = $this->_filter_category($category_idz);

        if (!$latitude and ! $longitude and ! empty($postcode) and ! empty($suburb)) {

            if ($suburb_postcode = $this->db->get_where('suburbs', array('postcode' => $postcode, 'suburb' => $suburb), 1)->row()) {
                $latitude = $suburb_postcode->lat;
                $longitude = $suburb_postcode->lon;
            }
        }

        $select = "SQL_CALC_FOUND_ROWS "
                . "offers.offer_id"
                . ", offer_title"
                . ',IF( offer_image IS NOT NULL AND offer_image <> "" ,  concat("' . base_url(config_item('offer_image_path')) . '/",offer_image), "") as offer_image'
                . ", offer_short_desc"
                . ", offer_tag"
                . ", DATE_FORMAT(`offer_valid_date`,'%d/%m/%Y') as offer_valid_date"
                . ", 1 as offer_new"
                .
                (($latitude and $longitude) ?
                        "
                ,   MIN(IF( oa_latitude IS NOT NULL AND oa_longitude IS NOT NULL,
                            (((acos(sin(({$latitude}*pi()/180)) * 
                            sin((oa_latitude*pi()/180))+cos(({$latitude}*pi()/180)) * 
                            cos((oa_latitude*pi()/180)) * cos((({$longitude} - oa_longitude)
                            *pi()/180))))*180/pi())*60*1.1515*1.609344)
                            , 999999999 ))
                            as distance
                " : ', 999999999 as distance'
                );

        $this->db->select($select, FALSE);

        $this->db->join('offer_address', 'offers.offer_id = offer_address.offer_id', 'left');

        $where = "";
        $filters = array();

        if ($keyword) {
            if (!$keyword_search) {
                $filters[] = " `offer_title` like '%{$this->db->escape_like_str($keyword)}%' ";
                $filters[] = " `offer_desc` like '%{$this->db->escape_like_str($keyword)}%' ";
                $filters[] = " `offer_short_desc` like '%{$this->db->escape_like_str($keyword)}%' ";
            } else {
                $filters[] = " `offer_title` like '{$this->db->escape_like_str($keyword)}%' ";
            }
        }
        $where = count($filters) > 0 ? "(" . implode(' OR ', $filters) . ")" : "";

        if (!empty($where))
            $this->db->where($where, NULL, FALSE);

        if (is_array($category_idz) and ! empty($category_idz))
            $this->db->where_in('category_id', $category_idz);

        if (!empty($postcode))
            $this->db->where('oa_post_code', $postcode);

        if (!empty($suburb))
            $this->db->where('oa_suburb', $suburb);


        $this->db->group_by('`offers`.offer_id');
        $this->db->order_by($order_by);

        $result = $this->db->get('offers', $limit, $offset);
        $return = $result->num_rows() > 0 ? $result->result() : FALSE;

        $this->_search_total = $this->db->query('SELECT FOUND_ROWS() as total')->row()->total;

        return $return;
    }

    function nearby_offers($category_idz, $latitude, $longitude, $max_distance) {
        $cat_idz = array();
        $category_idz = $this->_filter_category($category_idz);

        $latitude = (double) $latitude;
        $longitude = (double) $longitude;
        $max_distance = (int) $max_distance;

        $select = ""
                . "offers.offer_id"
                . ", offer_title"
                . ", oa_latitude"
                . ", oa_longitude"
                . "
                ,   (IF( oa_latitude IS NOT NULL AND oa_longitude IS NOT NULL,
                            (((acos(sin(({$latitude}*pi()/180)) * 
                            sin((oa_latitude*pi()/180))+cos(({$latitude}*pi()/180)) * 
                            cos((oa_latitude*pi()/180)) * cos((({$longitude} - oa_longitude)
                            *pi()/180))))*180/pi())*60*1.1515*1.609344)
                            , 999999999 ))
                            as distance
                "
        ;

        $this->db->select($select, FALSE);

        $this->db->join('offer_address', 'offers.offer_id = offer_address.offer_id', 'left');

        if (is_array($category_idz) and ! empty($category_idz))
            $this->db->where_in('category_id', $category_idz);

        $result = $this->db->having('distance <= ', $max_distance);

        $result = $this->db->get('offers');
//        echo $this->db->last_query();
//        debug($result->result());die;
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    function _filter_category($category_idz) {
        if(is_array($category_idz) and ! empty($category_idz)) {

            $categories = array();
            $sub_categories = array();

            $categories = array_map(function ($arg) {
                return $arg->category_id;
            }, $this->db->select('category_id')->where_in('category_id', $category_idz)->where('category_parent_id', 0)->get('category')->result());
            
            $sub_cats = array_diff($category_idz, $categories);
            
            if(!empty($categories)) $sub_categories = array_map(function ($arg) {
                    return $arg->category_id;
                }, $this->db->select('category_id')->where_in('category_parent_id', $categories)->get('category')->result());
            $sub_categories = array_merge($sub_cats, $sub_categories);
            
            return array_unique(array_filter(array_merge($categories, $sub_categories)));
        } else {
            return array();
        }
    }

    function search_offers($limit, $offset, $cat, $sub_cat, $location, $keyword) {
        if (!empty($cat)) {
            $this->db->where_in('category.category_id', $cat);
        }
        if (!empty($sub_cat)) {
            $this->db->where_in('category.category_id', $sub_cat);
        }
        $query = $this->db->select('SQL_CALC_FOUND_ROWS offers.offer_id AS id,
                                        offers.offer_title AS title,
                                        offers.offer_image AS image,
                                        offers.offer_short_desc AS short_desc,
                                        offers.offer_desc AS description ,category.category_name', FALSE)
                ->from('offers')
                ->join('offer_address', 'offers.offer_id = offer_address.offer_id', 'left')
                ->join('category', 'offers.category_id = category.category_id', 'left')
                ->where('(offers.offer_title like "%' . $keyword . '%"
                                       or offers.offer_short_desc like "%' . $keyword . '%"
                                       or offers.offer_desc like "%' . $keyword . '%" )'
                        . 'and (offer_address.oa_post_code like "%' . $location . '"'
                        . 'or offer_address.oa_suburb like "%' . $location . '")')
                ->limit($limit, $offset)
                ->order_by('offers.offer_title')
                ->get();



        if ($query->num_rows() > 0) {

            return $query->result();
        } else {
            return false;
        }
    }


  public function search_all($keyword, $cat, $sub_cat,$region, $location, $lat, $lon, $limit, $offset, $order, $distance,$count_rw, $keyword_search = TRUE) {
        
        $mykey1 = strtolower($keyword);
        $mykey = preg_replace('/[^a-zA-Z0-9 ]/', '', $mykey1);
       
       
        
        $order_list = array('alpha', 'nearest', 'new');
        $cat_idz = array();

        $order = in_array($order, $order_list) ? $order : 'alpha';

        switch ($order) {
            case 'alpha' :
                $order_by = 'title';
                break;

            case 'nearest' :
                $order_by = 'distance';
                break;

            case 'new' :
                $order_by = 'id desc';
                break;

            default :
                $order_by = 'title';
                break;
        }

        $special_deals = FALSE;

        if (($key = array_search('special_deals', $cat)) !== false) {
            $special_deals = TRUE;
            unset($cat[$key]);
        }

        $category_idz = array_merge($cat, $sub_cat);

        $category_idz = array_unique(array_filter($category_idz));

        $category_idz = $this->_filter_category($category_idz);
        
       
        //echo $latitude; exit;
        
        if($location) {
            $geo_data = geocode($location);

                $lat = $geo_data[0];
                $lon = $geo_data[1];
                
        }

        $latitude = $lat;
        $longitude = $lon;
        $cat_deal_cond = array();
        if (is_array($category_idz) and ! empty($category_idz)) {

            $cat_deal_cond[] = "( `category_id` IN (" . implode(',', $category_idz) . ")" .
                    " AND offer_deal.`type` = 'offer' )";

        }
         

        $sql_search = "SELECT q.*
                              FROM
                            (SELECT 
                              DISTINCT offer_deal.`title`, 
                              offer_deal.`id` AS id, 
                              image,
                              short_desc,
                              description,
                              website,
                              tag,
                              category_id,
                              valid_date,
                              price,
                              left_tag,
                              `offer_address`.`oa_suburb`,
                              quantity,
                              status,
                              created_date,
                              `offer_deal`.`type`, 
                              reviews.`review_rating`,";




      $sql_search .= ($latitude and $longitude) ?
                            
                              "((((acos(sin((".$latitude."*pi()/180)) * sin((`offer_address`.`oa_latitude`*pi()/180))+cos((".$latitude."*pi()/180)) * cos((`offer_address`.`oa_latitude`*pi()/180)) * cos(((".$longitude."- `offer_address`.`oa_longitude`)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as  distance" : "1 as distance";

    $sql_search .=          " FROM
                              (`offer_deal`) 
                              LEFT JOIN `offer_address` 
                                ON `offer_deal`.`id` = `offer_address`.`offer_id` 
                                AND `offer_deal`.`type` = 'offer' 
                              LEFT JOIN `reviews` 
                                  ON `offer_deal`.`id` = `reviews`.`review_type_id` 
                                  AND offer_deal.type = reviews.review_type
                              INNER JOIN `keywords` 
                                ON (
                                  `keywords`.`type_id` = `offer_deal`.`id`
                                  AND `offer_deal`.`type` = 'offer' 
                                  AND `keywords`.`type` = '1'
                                ) 
                            WHERE ";
        if (!empty($cat_deal_cond)) {
           $sql_search .=  implode(' OR ', $cat_deal_cond)." AND ";
            
        } 

       $sql_search .= " `offer_deal`.`valid_date` >= CURDATE()";
    if($keyword)
        $sql_search .=      "AND  ( `offer_deal`.`title` like '%".$mykey."%' )
                            OR ( `offer_deal`.`title` REGEXP '^$mykey'  )
                            OR ( `offer_deal`.`title` REGEXP '$mykey'  )
                            OR ( `offer_deal`.`title` = '$mykey'  )";


    
    $sql_search .=      " AND `offer_deal`.`status` != '0' 

                            HAVING `distance` <= $distance
                            ORDER BY distance 
                            ) AS q 
                            GROUP BY id";
    if($count_rw !="all"){
        $sql_search .= " Limit $offset, $limit";
    }                        
            //echo $sql_search; 
            $result = $this->db->query($sql_search);

            
          /*  echo "<pre>";
            print_r($this->db->last_query());
            die();*/

            return $result->num_rows() > 0 ? $result->result() : FALSE;
      
    }
    public function total_found_rows()
    {
        
    }







//    function get_detail($id) {
//        $query = $this->db->select("offers.offer_id,offers.offer_title,offers.offer_image,offers.offer_short_desc,offers.offer_desc,offers.offer_website,offers.offer_tag,offers.offer_price,offers.offer_condition, 'offer' as type, avg(reviews.review_rating) as review_rating", FALSE)
//                ->from("offers")
//                ->join("reviews", "offers.offer_id = reviews.review_type_id", "left")
//                ->where("offers.offer_id = '$id'and reviews.review_type= 'offer'")
//                ->get();
//
//        return $query->row();
//    }
    function get_detail($id) {

       $query = $this->db->select("offer_promocodes.*,offers.*, 'offer' as type, avg(reviews.review_rating) as review_rating", FALSE)
            ->from("offers")
            ->join("reviews", "offers.offer_id = reviews.review_type_id")
            ->join('offer_promocodes', 'offers.offer_id = offer_promocodes.offer_id')
            ->where("offers.offer_id = '$id'and reviews.review_type= 'offer' and offer_promocodes.offer_id='$id'")
            ->get();



//        echo "<pre>";
//        print_r($this->db->last_query());
//        die();
        return $query->row();
    }

public function fav($id, $member_id)
{

    $query = $this->db->select("*")
        ->from("favourite")
        ->where("favourite.favourite_type_id = '$id'and favourite.member_id= '$member_id'")
        ->get();



//        echo "<pre>";
//        print_r($this->db->last_query());
//        die();
    return $query->row();
}
// get all promotion codes

    public function get_postcodes($id)
    {
        $this->db->where('offer_id',$id);
        return $this->db->get('offer_promocodes')->result();
    }

    function get_myaddress($id)
    {
        $query = $this->db->select("offers.offer_id, offer_address.offer_id, offer_address.oa_address, offer_address.oa_suburb, offer_address.oa_phone")
                                ->from("offers")
                                ->join("offer_address", "offers.offer_id = offer_address.offer_id")
                                ->where("offers.offer_id = '$id'")
                                ->get();

        return $query->result();
    }



    function get_offer_csv($club_id = 0) {
        
        return $this->db->select('o.*,o.offer_id,c.category_name,ct.category_name as sub_category_name, r.region_name'
                . ', GROUP_CONCAT(DISTINCT k.title ORDER BY title ) AS `keywords`'
//                . ', GROUP_CONCAT( oa_address ORDER BY offer_address_id SEPARATOR "||" ) AS `oa_address`'
//                . ', GROUP_CONCAT( oa_phone ORDER BY offer_address_id SEPARATOR "||") AS `oa_phone`'
//                . ', GROUP_CONCAT( oa_post_code ORDER BY offer_address_id SEPARATOR "||") AS `oa_post_code`'
//                . ', GROUP_CONCAT( oa_suburb ORDER BY offer_address_id SEPARATOR "||") AS `oa_suburb`'
//                . ', GROUP_CONCAT( oa_state ORDER BY offer_address_id SEPARATOR "||") AS `oa_state`'
//                . ', GROUP_CONCAT( op_title ORDER BY op_id SEPARATOR "||") AS `op_title`'
//                . ', GROUP_CONCAT( op_promocode ORDER BY op_id SEPARATOR "||") AS `op_promocode`'
//                . ', GROUP_CONCAT( op_description ORDER BY op_id SEPARATOR "||") AS `op_description`'
                . ' ', FALSE)
                        ->from("offers as o")
//                        ->join("offer_address as oa", 'oa.offer_id=o.offer_id', 'left')
//                        ->join("offer_promocodes as op", 'op.offer_id=o.offer_id', 'left')
                        ->join("category as c", 'c.category_id=o.category_id', 'left')
                        ->join("category as ct", 'c.category_parent_id=ct.category_id', 'left')
                        ->join("keywords as k", 'type="1" and type_id=o.offer_id', 'left', FALSE)
                        ->join("regions as r", 'r.region_id=o.offer_id', 'left')
                        ->where('o.club_id', $club_id)
                        ->group_by('o.offer_id')
                        ->get()->result();
    }




        function get_offer_csvf($club_id = 0, $formatter) {

        return $this->db->select('o.*,o.offer_id,c.category_name,ct.category_name as sub_category_name, r.region_name'
                . ', GROUP_CONCAT(DISTINCT k.title ORDER BY title ) AS `keywords`'
//                . ', GROUP_CONCAT( oa_address ORDER BY offer_address_id SEPARATOR "||" ) AS `oa_address`'
//                . ', GROUP_CONCAT( oa_phone ORDER BY offer_address_id SEPARATOR "||") AS `oa_phone`'
//                . ', GROUP_CONCAT( oa_post_code ORDER BY offer_address_id SEPARATOR "||") AS `oa_post_code`'
//                . ', GROUP_CONCAT( oa_suburb ORDER BY offer_address_id SEPARATOR "||") AS `oa_suburb`'
//                . ', GROUP_CONCAT( oa_state ORDER BY offer_address_id SEPARATOR "||") AS `oa_state`'
//                . ', GROUP_CONCAT( op_title ORDER BY op_id SEPARATOR "||") AS `op_title`'
//                . ', GROUP_CONCAT( op_promocode ORDER BY op_id SEPARATOR "||") AS `op_promocode`'
//                . ', GROUP_CONCAT( op_description ORDER BY op_id SEPARATOR "||") AS `op_description`'
                . ' ', FALSE)
                        ->from("offers as o")
//                        ->join("offer_address as oa", 'oa.offer_id=o.offer_id', 'left')
//                        ->join("offer_promocodes as op", 'op.offer_id=o.offer_id', 'left')
                        ->join("category as c", 'c.category_id=o.category_id', 'left')
                        ->join("category as ct", 'c.category_parent_id=ct.category_id', 'left')
                        ->join("keywords as k", 'type="1" and type_id=o.offer_id', 'left', FALSE)
                        ->join("regions as r", 'r.region_id=o.offer_id', 'left')
                        ->where_in('o.offer_id', $formatter)
                        ->where('o.club_id', $club_id)
                        ->group_by('o.offer_id')
                        ->get()->result();
    }


    
    function get_csv_offer_address($id)
    {
        $res = $this->db->select(''                 
                . ', GROUP_CONCAT( oa_address ORDER BY offer_address_id SEPARATOR "||" ) AS `oa_address`'
                . ', GROUP_CONCAT( oa_phone ORDER BY offer_address_id SEPARATOR "||") AS `oa_phone`'
                . ', GROUP_CONCAT( oa_post_code ORDER BY offer_address_id SEPARATOR "||") AS `oa_post_code`'
                . ', GROUP_CONCAT( oa_suburb ORDER BY offer_address_id SEPARATOR "||") AS `oa_suburb`'
                . ', GROUP_CONCAT( oa_state ORDER BY offer_address_id SEPARATOR "||") AS `oa_state`'
                . ' ', FALSE)
                        ->from("offers as o")
                        ->join("offer_address as oa", 'oa.offer_id=o.offer_id', 'left')
                        ->where('o.offer_id', $id)
                        ->group_by('o.offer_id')
                        ->get();
        
        return $res->num_rows() > 0 ? $res->row() : FALSE;
        
    }
    
    function get_csv_offer_postcode($id)
    {
        
        $res = $this->db->select(''  
                . ', GROUP_CONCAT( op_title ORDER BY op_id SEPARATOR "||") AS `op_title`'
                . ', GROUP_CONCAT( op_promocode ORDER BY op_id SEPARATOR "||") AS `op_promocode`'
                . ', GROUP_CONCAT( op_description ORDER BY op_id SEPARATOR "||") AS `op_description`'
                . ' ', FALSE)
                        ->from("offers as o")
                        ->join("offer_promocodes as op", 'op.offer_id=o.offer_id', 'left')
                        ->where('o.offer_id', $id)
                        ->group_by('o.offer_id')
                        ->get();
        
        return $res->num_rows() > 0 ? $res->row() : FALSE;
        
    }
    
    function insert_keywords( $data) {
            return $this->db->insert_batch('keywords', $data);
    }
    
    function delete_keywords($id) {
        return $query = $this->db->delete("keywords", array('type'=>1,"type_id" => $id));
    }
    
    function get_keywords($id){
        $query = $this->db->select('title')
                          ->from('keywords')
                          ->where("type = '1' and type_id = '$id'")
                          ->get();
                  return $query->result();
    }
    
    function get_promocodes($id){
        $query = $this->db->select("*")
                          ->from("offer_promocodes")
                          ->where("offer_id = '$id'")
                          ->get();
                  return $query->result();
    }


    function get_autocomplete($input)
    {
//        $this->db->select('offer_title,offer_id');
//        $this->db->from('offers');
//        $this->db->like('offer_title',$input);
//        return $this->db->get()->result_array();

        $this->db->select('*');
        $this->db->like('title', $input,'after');
        $this->db->limit(10);
        $query = $this->db->get('keywords');

        if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $new_row['label']=htmlentities(stripslashes($row['title']));
                $new_row['value']=htmlentities(stripslashes($row['title']));// The stripslashes() function removes backslashes added by the addslashes() function.
                $row_set[] = $new_row; //build an array
            }
            echo json_encode($row_set); //format the array into json data
        }
//        $query = $this->db->select('offer_title,offer_id')
//            ->from('offers')
//            ->where("offer_title LIKE %$input%")
//            ->get();
//
//        return $query->result();
    }

    
    function get_autocomplete_refine($input)
    {
//        $this->db->select('offer_title,offer_id');
//        $this->db->from('offers');
//        $this->db->like('offer_title',$input);
//        return $this->db->get()->result_array();

        $this->db->select('*');
        $this->db->like('title', $input,'after');
        $query = $this->db->get('keywords');

        if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $new_row['label']=htmlentities(stripslashes($row['title']));
                $new_row['value']=htmlentities(stripslashes($row['title']));// The stripslashes() function removes backslashes added by the addslashes() function.
                $row_set[] = $new_row; //build an array
            }
            echo json_encode($row_set); //format the array into json data
        }
//        $query = $this->db->select('offer_title,offer_id')
//            ->from('offers')
//            ->where("offer_title LIKE %$input%")
//            ->get();
//
//        return $query->result();
    }

    function get_autocomplete_suburb($input){

        $this->db->select('suburb, dc');
        $this->db->like('suburb', $input,'after');
        $this->db->limit(10);
        $query = $this->db->get('suburbs');

        if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $new_row['label']=htmlentities(stripslashes($row['suburb'] . ', ' . $row['dc']));
                $new_row['value']=htmlentities(stripslashes($row['suburb'] . ', ' . $row['dc']));// The stripslashes() function removes backslashes added by the addslashes() function.
                $row_set[] = $new_row; //build an array
            }
            echo json_encode($row_set); //format the array into json data
        }
    }

    function deleteall($id)
    {
       $this->db->delete('offer_address', array('offer_id' => $id)); 
       $this->db->delete('keywords', array('type_id' => $id)); 
    }

}
