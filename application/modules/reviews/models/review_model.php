<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Review_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'reviews';
        $this->field_prefix = 'review_';
        $this->log_user = FALSE;
    }

    function offer_review($id = 0) {

        $this->db->select("offers.`offer_title`,reviews.*")
                ->from("reviews")
                ->join("offers", "reviews.review_type_id = offers.offer_id", "inner")
                ->where("reviews.`review_type` = 'offer'");

        if ($id)
            $this->db->where("reviews.review_type_id = '$id'");
        $query = $this->db->get();
        return $query->result();
//            $query = $this->db->query("SELECT offers.`offer_title`,reviews.* FROM offers
//INNER JOIN reviews 
//ON offers.`offer_id` = reviews.`review_type_id`
//WHERE reviews.`review_type` = 'offer'")->result();
//            return $query;
    }

    function deal_review($id = 0) {
        $this->db->select("deals.`deal_title`,reviews.*")
                ->from("reviews")
                ->join("deals", "reviews.review_type_id = deals.deal_id", "inner")
                ->where("reviews.`review_type` = 'deal'");

        if ($id)
            $this->db->where("reviews.review_type_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }

    function offer_review_detail($id) {
        $this->db->select("*")->from("reviews")->where("review_id = '$id'");
        $query = $this->db->get();
        return $query->row();
    }

    function review($id) {
        $query = $this->db->select("review_rating")
                ->from("reviews")
                ->where("review_type_id = '$id'")
                ->get();
        return $query->row();
    }
   
    function total_review($id,$type){
        $query = $this->db->select("count(review_rating) as total_review")
                          ->from("reviews")
                          ->where("review_type_id = '$id' and review_type = '$type'")
                          ->get();
        return $query->row();
    }
    
    function get_reviews($id,$type){
        $query = $this->db->select("*")
                          ->from("reviews")
                          ->where("review_type_id = '$id' and review_type = '$type' and review_status = '1'")
                          ->order_by('review_created_date','desc')
                          ->get();
        return $query->result();
    }
    function add_review($review_type,$review_type_id,$name, $title,$rating, $your_review){
     $this->db->insert('reviews',array('review_type' => $review_type,
                                          'review_type_id' => $review_type_id,
                                          'review_rating' =>  $rating,
                                          'review_title' => $title,
                                          'review' => $your_review,
                                          'reviewer_name' => $name
            
            ));
     return TRUE;
        
    }
    
   

}
