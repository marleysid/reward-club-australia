<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Deal Reviews</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Deal Title</th>
				<th>Review Title</th>
				<th>Review Rate </th>
				<th style="width:85px;" >Status</th>
				<th style="width:90px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all" >
			<?php if($reviews): ?>
				<?php foreach($reviews as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->deal_title; ?></td>
				<td><?php echo $val->review_title; ?></td>
				<td><?php echo $val->review_rating; ?></td>
				<td class="center"><input type="checkbox"  <?php echo $val->review_status ? "checked": ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('admin/reviews/toggle_status/'); ?>" data-id="<?php echo $val->review_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
                                <td class="center">
                                    <a class="review" data-toggle="modal" data-id="<?php echo $val->review_id; ?>" href="" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Review"><i class="fa fa-star"></i> </a>

                                </td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="myModalLabel">Review</h3>
            </div>

            <div class="modal-body" id="myModalbody">

            </div>
            <div class="modal-footer">

               
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(".review").on('click', function() {
        var id = $(this).data('id');
        //alert(id);
//         


        if (id)
        {
            $.ajax({
                    url: base_url + 'admin/reviews/review_detail/' + id,
                    dataType: "html",
                    success: function(data) {
                         $('#myModalbody').html(data);
                    $('#myModal').modal('show');
                        

                    },
                    error: function() {

                    }

                });
          
        }
        else
        {

        }

    });



    //end of document

</script>
