<h3>Search Offer</h3>
<form method="GET" action="<?php echo base_url('webservice/offers/search'); ?>"  role="form"  class="form-horizontal" >	
    <div class="form-group">
        <label class="col-sm-3 control-label">Category Id:</label>
        <div class="col-sm-9"><input class="form-control" type="text" name="category_id[]" value="" />Category id comma separated</div>
    </div>	
    <div class="form-group">
        <label class="col-sm-3 control-label">Postcode:</label>
        <div class="col-sm-9"><input class="form-control" type="text" name="postcode" value="" /></div>
    </div>	
    <div class="form-group">
        <label class="col-sm-3 control-label">Suburb:</label>
        <div class="col-sm-9"><input class="form-control" type="text" name="suburb" value="" /></div>
    </div>	
    <div class="form-group">
        <label class="col-sm-3 control-label">latitude:</label>
        <div class="col-sm-9"><input class="form-control" type="text" name="latitude" value="" /></div>
    </div>	
    <div class="form-group">
        <label class="col-sm-3 control-label">longitude:</label>
        <div class="col-sm-9"><input class="form-control" type="text" name="longitude" value="" /></div>
    </div>	
    <div class="form-group">
        <label class="col-sm-3 control-label">keyword:</label>
        <div class="col-sm-9"><input class="form-control" type="text" name="keyword" value="" /></div>
    </div>	
    
    <div class="form-group">
        <label class="col-sm-3 control-label">sort by :</label>
        <div class="col-sm-9">
            <select name="sort_by">
                <option>alpha</option>
                <option>nearest</option>
                <option>new</option>
            </select>
        </div>
    </div>
    <div class="form-group"><div class="col-sm-offset-3 col-sm-9"><input type="submit" class="btn btn-default" value="Submit" /></div></div>
</form>