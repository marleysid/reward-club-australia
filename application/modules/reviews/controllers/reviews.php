<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of login
 *
 * @author rabin
 */
class Reviews extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('review_model', 'advertisement/advertisement_model', 'offers/offer_model', 'deals/deal_model', 'orders/order_model'));
  
    }
     public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }
    function add_review(){
        $status = 0;
        $review_type = $this->input->post('type');
        $review_type_id = $this->input->post('review_type_id');
        $name = $this->input->post('name'); 
        $title = $this->input->post('title'); 
        $your_review = $this->input->post('your_review'); 
        $rating = $this->input->post('score'); 
        
        if($this->review_model->add_review($review_type,$review_type_id,$name, $title,$rating, $your_review) == TRUE){
            $status = 1;
            echo json_encode(array('status'=>$status));
        }
        else{
            $status = 0;
            echo json_encode(array('status'=>$status));
        }
       
      
    }
}