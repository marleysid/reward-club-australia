<div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5>Manage Subscription</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
            <thead>
                <tr>
                    <th>#</th>
                    <th> Name</th>
                    <th>Email </th>
                    
                </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all" >
                <?php if ($subscription): ?>
                    <?php foreach ($subscription as $key => $val): ?>
                        <tr class="odd">
                            <td><?php echo ++$key; ?></td>
                            <td><?php echo $val->name; ?></td>
                            <td><?php echo $val->email; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>

    </div>
</div>
