<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : ''; ?> CMS</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>">
                <div class="form-group <?php echo form_error('cms_title') ? 'has-error' : '' ?>">
                    <label for="cms_title" class="control-label col-lg-3">Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="offer_title" placeholder="Title" name="cms_title" class="form-control" value="<?php echo set_value("cms_title", $edit ? $cms_detail->cms_title : ''); ?>" >
                        <?php echo form_error('cms_title'); ?>
                    </div>
                </div>
              
                <div class="form-group <?php echo form_error('cms_content') ? 'has-error' : '' ?>">
                    <label for="cms_content" class="control-label col-lg-3"> Content *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="offer_desc" placeholder=" Content" name="cms_content" class="form-control"><?php echo  set_value("cms_content", $edit ? $cms_detail->cms_content : ''); ?></textarea>
 <?php echo display_ckeditor('cms_content'); ?>                    
<?php echo form_error('cms_content'); ?>
                    </div>
                </div>
                  <div class="form-group <?php echo form_error('cms_slug') ? 'has-error' : '' ?>">
                    <label for="cms_slug" class="control-label col-lg-3"> Slug *</label>
                    <div class="col-lg-7">
                        <input type="text" id="offer_desc" placeholder=" Slug" name="cms_slug" value="<?php echo  set_value("cms_slug", $edit ? $cms_detail->cms_slug : ''); ?>" class="form-control"<?php if($edit): ?>disabled<?php endif; ?>>
                  
<?php echo form_error('cms_slug'); ?>
                    </div>
                </div>

               <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
<?php echo anchor('admin/cms', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>



