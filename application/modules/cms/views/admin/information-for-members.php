<script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>


<form method="post" action="<?php echo base_url('admin/cms/content');?>">
<div class="col-lg-4">
   <div class="box">
      <header>
         <h5>Heading or Title</h5>
         <div class="toolbar">
         </div>
      </header>
      <div class="body" id="editor1" contenteditable="true">
         <p class="text-muted"><?php echo $get_content-> heading;?></p>
      </div>
      <input type="hidden" name="heading" id="heading" value="<?=$get_content-> heading;?>"/>
   </div>
</div>
<!-- /.col-lg-4 -->
<div class="col-lg-8">
   <div class="box">
      <header>
         <h5>Sub heading or title</h5>
         <div class="toolbar">
         </div>
      </header>
      <div class="body" id="editor2" contenteditable="true">
         <p><?php echo $get_content-> subheading;?></p>
      </div>
      <input type="hidden" name="subheading" id="subheading" value="<?=$get_content-> subheading;?>"/>
   </div>
</div>
<!-- /.col-lg-8 -->
</div><!-- /.row -->
<div class="row">
   <div class="col-lg-6">
      <div class="box">
         <header>
            <h5>Video</h5>
            <div class="toolbar">
            </div>
         </header>
         <div class="body clearfix">
            <iframe src="https://player.vimeo.com/video/10623511" width="640" height="284" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            <input type="hidden" name="video" id="video" value=""></input>
         </div>
      </div>
   </div>
</div>
<!-- /.row -->


<div class="row">
   <div class="col-lg-12">
      <div class="box">
         <header>
            <h5>Description</h5>
            <div class="toolbar">
            </div>
         </header>
         <div class="body"  id="editor3" contenteditable="true">
         <p><?php echo $get_content-> description;?>
</p>
      </div>
       <input type="hidden" name="description" id="description" value="<?=$get_content-> description;?>"/>
      </div>
   </div>
</div>
   <button class="btn btn-primary btn-sm" type="submit">Submit</button>
</form>
<!-- /.row -->


<!-- /.col-lg-12 -->
<script>
    CKEDITOR.inline( 'editor1',{
    on: {
        change: function(event) {
            var content = event.editor.getData();
            // $("#heading").val("");
            $("#heading").val(content);
        }
    }
   });
   //CKEDITOR.inline( 'editor2' );
    CKEDITOR.inline( 'editor2',{
    on: {
        change: function(event) {
            var content = event.editor.getData();
            // $("#subheading").val("");
            $("#subheading").val(content);
        }
    }
   });

   //CKEDITOR.inline( 'editor3' );
    CKEDITOR.inline( 'editor3',{
    on: {
        change: function(event) {
            var content = event.editor.getData();
            // $("#description").val("");
            $("#description").val(content);
        }
    }
   });



</script>
