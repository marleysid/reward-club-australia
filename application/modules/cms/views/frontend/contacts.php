
<div class="container">
    <div class="sub-page-banner-wrap maps" id="map" style="margin-top: 20px;"></div>



  <!--<div id="map" style="height: 400px; width: 100%" ></div>-->
  <div class="banner-slider">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
      <ul>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
      </ul>
    </div>
  </div>

    <div class="breadcrumb-wrap">
        <ul class="breadcrumb-list">
            <li><a href="<?php echo site_url();?>">home</a></li>
            <li>Contact</li>

        </ul>
    </div>
        <?php
        if (isset($userLoggedIn) and $userLoggedIn):
        ?>
        <div class="contact-information-wrap add">
            <h3>Contact Us</h3>
            <div class="contact-inner-holder">
                <div class="contact-left-block">
                    <form role="form" method="post" id="contactform" class ="contact">
                        <fieldset>
                            <div class="input-row">
                              <div class="wrap">
                                <input type="text"  placeholder="Name*" name="name" >
                              </div>
                              <div class="wrap">
                                <input type="text"  placeholder="Address" name="address" >
                              </div>

                            </div>
                            <div class="input-row add">
                              <div class="wrap">
                                <input type="text"  placeholder="Contact No."  name="phone">
                              </div>
                              <div class="wrap">
                                <input type="text" placeholder="Email Address*" name="email" >
                              </div>

                            </div>
                            <div class="input-row">

                                <input type="text"  class=" full" placeholder="subject" name="subject" >
                            </div>
                            <div class="input-row">

                                <textarea  cols="30" rows="10" placeholder="Message" name="msg" ></textarea>

                            </div>

<!--                            <input type="submit" name="submit"  value="submit" />-->
                            <button type="submit" name="submit">Submit enquiry</button>
                        </fieldset>


                    </form>
                </div>
           
                <div class="contact-right-block add">
                    <div class="additional-contact">
                        <div class="img-wrap"><a href="#"><?php if(file_exists($community->logo) and $community->logo!=null): ?><img  src="<?php echo imager($community->logo,200,80); ?>" /></a><?php endif; ?></a></div>
                        <strong class="title"><a href="#"><?php echo $community->title ?></a></strong>
                        <div class="additional-detail">
                            <ul class="contact-list">
                                <li class="location"><span class="icon">icon</span><address><?php echo $community->address ?></address></li>
                                <li class="post"><span class="icon">icon</span><address><?php echo $community->pobox ?></address></li>
                                <li class="phone-no"><span class="icon">icon</span><?php echo $community->contact_number ?></li>
                                <li class="email"><span class="icon">icon</span><a href="#"><?php echo $community->email ?></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <?php else: ?>

    <div class="contact-information-wrap">
        <h3>Get in touch</h3>
        <span class="info">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto</span>
        <div class="contact-inner-holder">
            <div class="contact-left-block">
                    <form role="form" method="post"  class="contact" id="contactform">
                    <fieldset>
                        <div class="input-row">
                            <div class="wrap">
                              <input type="text"  placeholder="Name*" name="name" >
                            </div>
                            <div class="wrap">
                              <input type="text"  placeholder="Address" name="address" >
                            </div>
                        </div>
                        <div class="input-row add">
                          <div class="wrap">
                            <input type="text"  placeholder="Contact No."  name="phone">
                          </div>
                          <div class="wrap">
                            <input type="text" placeholder="Email Address*" name="email" >
                          </div>
                            

                        </div>
                        <span>Choose a sample promotional & Support Material you want to enquire about:</span>
                        <div class="input-row">
                                <select class="type-select custom enquiry-category" id="select-sub-category" placeholder="Type of Enquiry" name="enquiry" required>

                                <option value="first">First</option>
                                <option value="second">Second</option>
                                <option value="third">Third</option>
                                <option value="fourth">Fourth</option>
                            </select>
                        </div>
                        <div class="input-row">

                            <input type="text"  class=" full" placeholder="subject" name="subject" >
                        </div>
                        <div class="input-row">

                            <textarea  cols="30" rows="10" placeholder="Message" name="msg" ></textarea>

                        </div>
<!--                        <button type="submit">Search</button>-->
                        <button type="submit" name="submit">Search</button>
                    </fieldset>
                </form>
            </div>
            <div class="contact-right-block"></div>
        </div>
    </div>

        <?php endif; ?>
</div>


<!---->
<!--<div class="partner-wrap">-->
<!--    <h3>Rewards Club Partners</h3>-->
<!--    <div class="partner-holder">-->
<!--        <div id="owl-partner" class="owl-carousel">-->
<!--            --><?php //if(isset($partners)): ?>
<!--            --><?php //foreach ($partners as $row): ?>
<!--            <div class="item">-->
<!--                <div class="image-wrap"> <img src="--><?php //echo $row->partners_icon ? 'assets/uploads/partners/' . $row->partners_icon : ''; ?><!--" class="img-responsive"></div>-->
<!--            </div>-->
<!--                --><?php //endforeach; ?>
<!--            --><?php //endif; ?><!--</div>-->
<!--        </div>-->
<!--    </div>-->


<div class="media-block add">
    <div class="featured-club-news-holder add">
        <h3><span>Rewards Club News</span></h3>
        <div id="owl-news" class="owl-carousel club-news">
            <?php if (isset($news_detail)): ?>
                <?php foreach ($news_detail as $news): ?>
                    <div>
                        <a href="<?php echo site_url('news').'/'.safe_b64encode($news->news_id) .'/'.strtolower(url_title($news->news_title,'-','TRUE')) ?>"> <img src="<?php echo imager($news->news_image ? 'assets/uploads/news/'.$news->news_image:'',218,106,1); ?>" class="img-responsive"></a>
                        <div class="info">
                            <span><?php echo date("d F", strtotime($news->news_create_date)); ?></span>
                            <h4><a href="<?php echo site_url('news').'/'.safe_b64encode($news->news_id).'/'.strtolower(url_title($news->news_title,'-','TRUE')) ?>"><?php echo character_limiter(ucwords($news->news_title), 20); ?></a></h4>
                            <p><?php echo character_limiter($news->news_excerpt, 75) ?> </p>
                            <!--                            <button class="btn btn-danger">Read More</button>-->
                            <?php echo anchor("news/".safe_b64encode($news->news_id).'/'.strtolower(url_title($news->news_title,'-','TRUE')), 'Read More', 'class="btn btn-danger"'); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>



<div class="modal " id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <!-- Modal content-->

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thanks</h4>
            </div>
            <div class="modal-body">

    <p>Your Query has been submitted. We will get back to you within 48 hours. :) </p>


                </div>
            </div>
        </div>

    </div>
</div>

<!-- media --> 
<script>
     $(function(){
       $('#contactform').validate({
           rules: {
               name: 'required',
               address: 'required',
               phone: 'required',
               subject: 'required',
               msg: 'required',
               email: {
                required: true,
                email: true
            },
            enquiry: 'required'
            
            
        },
        messages: {
            enquiry: 'Please select One'
        } 
       }); 
    });
    </script>
<script>
    $("#contactform").on('submit', function () {
        if ($(this).valid()) {
            var contactform = $("#contactform").serialize();

            if (contactform) {

                $.ajax({
                    type: 'post',
                    url: base_url + 'cms/addcontact',
                    data: contactform,
                    success: function (data) {
                        //console.log(data);

                        if (data.status == 1) {
                            // window.location.href = base_url
                            $('#myModal').modal('show');


                        } else {
                            alert("l");
                        }

                    },
                    error: function () {

                    },
                    dataType: 'json'


                });
            }
            else {
                alert('kjkd');
            }
        }
        return false;

    });
    </script>
<!--     <script type="text/javascript" language="JavaScript" src="http://j.maxmind.com/app/geoip.js"></script>-->
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>

        <script type="text/javascript">
            var map;
            function initialize() {

                var latlngPos = new google.maps.LatLng( <?php echo $latitude->value;?>,<?php echo $longitude->value; ?>);
                var options =
    {
        zoom: 15,
        center: latlngPos,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl: true,
        mapTypeControlOptions:
        {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            poistion: google.maps.ControlPosition.TOP_RIGHT,
            mapTypeIds: [google.maps.MapTypeId.ROADMAP,
              google.maps.MapTypeId.TERRAIN,
              google.maps.MapTypeId.HYBRID,
              google.maps.MapTypeId.SATELLITE]
        },
        navigationControl: true,
        navigationControlOptions:
        {
            style: google.maps.NavigationControlStyle.ZOOM_PAN
        },
        scaleControl: true,
        disableDoubleClickZoom: true,
        draggable: false,
        streetViewControl: true,
        draggableCursor: 'move'
    };
                map = new google.maps.Map(document.getElementById("map"), options);
                // Add Marker and Listener
                var latlng = new google.maps.LatLng(<?php echo $latitude->value;?>,<?php echo $longitude->value; ?>) ;
                var marker = new google.maps.Marker
    (
        {
            position: latlng,
            map: map,
            title: 'Click me'
        }
    );
                var infowindow = new google.maps.InfoWindow({
                    content: 'Rewards Club Australia'
                });
                google.maps.event.addListener(marker, 'click', function () {
                    // Calling the open method of the infoWindow 
                    infowindow.open(map, marker);
                });
            }
            window.onload = initialize;
    </script>