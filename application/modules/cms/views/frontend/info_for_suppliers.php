<!-- banner -->

<div class="container">
    <div class="banner"> <div class="sub-page-banner-wrap maps"><img src="<?php echo $info->cms_header_image ? 'assets/uploads/cms/' . $info->cms_header_image : ''; ?>"  class="img-responsive" alt="slide"></div>

    </div>


    <div class="breadcrumb-wrap">
        <ul class="breadcrumb-list">
            <li><a href="<?php echo site_url(); ?>" class="active">Home</a></li>
            <li>Information For Suppliers</li>
        </ul>
    </div>
    <div class="information-wrapper">
        <div class="video-wrapper add">
            <strong class="title"><?php echo $community->title ?></strong>
            <div class="company-logo-wrap"><?php if(file_exists($community->logo) and $community->logo!=null): ?><img  src="<?php echo imager($community->logo,200,80); ?>" /></a><?php endif; ?></div>


            <div class="information-detail-wrap add01">
                <p><?php echo $community->short_description ?></p>
                <p><?php echo $community->description ?></p></div>
            <div class="contact-wrap add01"><a href="#" data-toggle="modal" data-target="#submitBusiness">Submit your business</a></div>
        </div>
    </div>
</div>
<div class="features-bubbles-wrap">
    <div class="container">

        <ul class="bubbles-list">
            <?php if ($suppliers_ions): ?>
            <?php foreach ($suppliers_ions as $key => $val): ?>
            <li class="leading" ><a href="#"><span class="icon" style="background:#d91e26 url(<?php echo $val->icons ?>)  ">icon</span><span class="text"><?php echo $val->short_description; ?></span></a></li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>

    </div>
</div>
<div class="container">


    <div  class="modal " id="submitBusiness" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->

            <form id="addbusiness" method="post">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Submit your business</h4>
                    </div>
                    <div class="modal-body">
                        <div class="input-holder">
                            <label for="business">Business Name</label>
                            <input type="text" name="business_name" id="business" required>
                        </div>
                        <div class="input-holder">
                            <label for="street">Street Address</label>
                            <textarea id="street" name="address"></textarea>
                        </div>
                        <div class="input-holder">
                            <label for="post">Postcode</label>
                            <input type="text" id="post" name="postcode" required>
                        </div>
                        <div class="input-holder">
                            <label for="contact">Contact Name</label>
                            <input type="text" id="contact" name="contact_name"required>
                        </div>
                        <div class="input-holder">
                            <label for="phone">Phone Number</label>
                            <input type="text" id="phone" name="phone_number" required>
                        </div>
                        <div class="input-holder">
                            <label for="emailbox">Email</label>
                            <input type="email" id="emailbox" name="email" required>
                        </div>
                        <div class="input-holder">
                            <label for="offer">Offers Available at your business</label>
                            <textarea name="offers" id="offers" cols="30" rows="2" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" value="Submit">

                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal " id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Thanks</h4>
                </div>
                <div class="modal-body">

                    <p>Your Query has been submitted. We will get back to you within 48 hours. :) </p>


                </div>
            </div>
        </div>

    </div>
<div class="row">



    <div class="partner-wrap">
        <h3>Rewards Club Partners</h3>
        <div class="partner-holder">
            <div id="owl-partner" class="owl-carousel">
                <?php if(isset($partners)): ?>
                    <?php foreach ($partners as $row): ?>
                        <div class="item">
                            <div class="image-wrap"> <img src="<?php echo $row->partners_icon ? 'assets/uploads/partners/' . $row->partners_icon : ''; ?>" class="img-responsive"></div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?></div>
        </div>
    </div>

<div class="feature-service owl-carousel" id="owl_new" style="margin-top:60px;">
        <div class="container">
        <h3>&nbsp;</h3>
            <?php $this->load->view('deals/frontend/bottom_slider'); ?>
        </div>
    </div>
</div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/frontend/js/info-for-clubs.js'); ?>"></script>

<script type="text/javascript">
$(function(){
    $('.orders-above').on('click',function(){
        $('html, body').animate({
        scrollTop: $(".menu-detail").offset().top},
        'slow');
    });
      
//    $(document).scrollTo('.orders-above');
});
</script>
<!--<div class="feature-service suppliers-service">
  <div class="container">
  <h3><span></span></h3>
    <div id="owl-service" class="owl-carousel">
        <?php // if(isset($partners)): ?>
        <?php // foreach ($partners as $row): ?>
      <div class="item logo_img">
          <img src="<?php // echo imager($row->partners_icon ? 'assets/uploads/partners/' . $row->partners_icon : '', 231,112); ?>" class="img-responsive">
      </div>
        <?php // endforeach; ?>
        <?php // endif; ?>
      </div>
  </div>
</div>-->