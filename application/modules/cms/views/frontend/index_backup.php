
<!-- banner -->
<div class="banner"> 
<div class="slider"><img src="<?php echo imager($info->cms_header_image ? 'assets/uploads/cms/' . $info->cms_header_image : '', 1349, 295); ?>"  class="img-responsive" alt="slide"></div>
    <div class="banner-slider">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
            <ul>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
            </ul>
        </div>
    </div>
</div>
<!-- banner--> 

<!-- search-featured -->
<div class="page-content page-content-club">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            	<div class="menu-detail">
                	<ul>
                    <li><a href="<?php echo site_url(); ?>" class="active">Home</a></li>
                    <li><a href="<?php echo site_url('information-for-members'); ?>">Information For Members</a></li>
                </ul>
                </div>
            </div>
            
<!--            <div class="col-md-7 member-detail">
               <img src="assets/frontend/images/card_s.jpg"  class="img-responsive" alt="card detail">
              <p>Membership at leading local clubs now includes Rewards Club VIP discounts at no extra cost. Join or update your membership card to start saving with Rewards Club VIP today. </p>
              <p>You can also call in to collect an updated VIP Directory from the member stand located in the foyer of each club.</p>
               
            </div>-->
             <?php echo $info->cms_content; ?>
            <div class="ccol-xs-12 col-sm-12 col-md-6"> 
            <div class="vdo-md-cls" id="information-for-member-right-video-holder">
                <!-- featureCarousel -->
                <div class="embed-responsive embed-responsive-16by9 wowload fadeInLeft">
                    <iframe  class="embed-responsive-item" src="//player.vimeo.com/video/10623511?title=0" width="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>

                <!-- featureCarousel--> 
             </div>
            </div>
        </div>
    </div>
</div>





<div class="search-featured" id="search-featured">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <h4 class="clb_head"><b>Join a Local Club</b> ( FREE with membership at these leading clubs)</h4>
             </div>
            
                <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="search-club">
                    <div class="local-search club">
                        <h4>Search a Club</h4>
                        <form role="form" method="GET">
                            <div class="form-group">
                                <label class="lbl_cls">Post Code</label>
                                <input type="text" class="form-control input_txt postcode" value="<?php echo $this->input->get('postcode') ?>" name="postcode"  placeholder="Post Code" id="club_postcode">
                            </div>
                            <div class="form-group">
                                <label class="lbl_cls">State</label>
                                <select class="form-control input_txt" name="state" id="club_state">
                                    <option value="">Select state</option>
                                    <?php if(isset($get_state)): ?>
                                    <?php foreach ($get_state as $state): ?>
                                    <option <?php echo ($this->input->get('state') == $state->state) ? 'selected="selected"':'' ?> value="<?php echo $state->state; ?>"><?php echo $state->state; ?></option>
                                  <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="lbl_cls">Suburb</label>
                                <input type="text" class="form-control input_txt"  placeholder="Suburb" name="suburb" value="<?php echo $this->input->get('suburb') ?>" id="club_suburbs_select">
                            </div>
                            <div class="form-group">
                                <label class="lbl_cls">Club name</label>
                                <input type="Phone" class="form-control input_txt"  placeholder="Club name" name="club_name" id="keyword">
                            </div>
                            <div class="text-right">
                              
                                <input type="submit"  class="btn btn-danger" value="Search" />           
                            </div>
                        </form>
                    </div>
                    <div class="billing_available">
                        <?php if(isset($get_ads)): ?>
                        <a href=""><img src="<?php echo imager($get_ads->ad_link ? 'assets/upload/ad/'.$get_ads->ad_link:'',278,268); ?>" class="img-responsive"></a><?php endif; ?> </div>
                </div>
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-9 result-box_board">
                <span class="list-result-title"><b>Showing <?php echo ($total == 0 ? 0 : (($offset) ? $offset:'1'))  ?> - <?php echo (($offset + $per_page) < $total) ? ($offset+$per_page):$total; ?> of <?php echo $total ?> total results</b></span>
                    <div class="bg_grw_01 members-lists-holder">
                        <!--repeat div start here-->
                        <div class="container">
                        <?php if (isset($get_clubs) and count($get_clubs) > 0): ?>
                            <?php foreach ($get_clubs as $clubs): ?>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-9 padding-right-adjustment col-width-adjust-increase-white-holder decrease-padding-left">
                                <div class="whit_box">
                                    <h4><?php echo $clubs->club_name; ?></h4>
                                    <div class="" style="border-top:1px solid #f0f0f0; padding-top:10px;">
                                    	<span class="map-location"><?php echo ucwords($clubs->club_address); ?></span>
                                        <span class="contact-number"><?php echo $clubs->club_phone; ?></span>
                                    </div>
                                    
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-3 col-width-adjust-decrease-white-holder decrease-padding-left decrease-padding-right">
                                	<div class="whit_box">
                                    <a href="<?php echo prep_url($clubs->club_website); ?>" target="_blank">   <button class="view_website btn fx_font_siz">
                                        <span>View Website</span> <img src="<?php echo get_asset('assets/frontend/images/view_detail_arrow.png'); ?>">
                                      </button></a>
                                      </div>
                                </div>
                                </div>
                            <?php endforeach; ?>
                       
                         <?php else: ?>
                            <div class="row">
                                <div class="col-xs-12 ">
                                <div class="whit_box">
                                    No result found.
                                    
                                </div>
                                </div>
                            </div>
                        
                         <?php endif; ?>
                         </div>
                        

                        <!--repeat div end here--> <?php //echo $this->pagination->create_links(); ?><?php //echo $info->cms_second_block; ?>
                    </div>
                    
                       
                        <div class="list-pagination-holder clearfix">
                        <div class="col-xs-12 col-sm-12 col-md-3"></div>
                        <div class="col-xs-12 col-sm-12 col-md-5 pgn_wrp_clr">
                        
                            <?php echo $this->pagination->create_links(); ?>                          
                         </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 bg_nn_pgmrgn">
                            <div class="btn-group pull-right" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                	<h4 class="margin_right_wrp_bxt_itm">Items per page:</h4>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><span><?php echo $this->uri->segment(2) ? $this->uri->segment(2):'8' ?></span> <span class="icon-arrow-down-small-grey"></span>  </button>
                                    <ul class="dropdown-menu insrt_with" role="menu">
                                        <li ><a href="<?php echo site_url('information-for-members/8').'?'.$_SERVER['QUERY_STRING']; ?>">8</a></li>
                                        <li><a href="<?php echo site_url('information-for-members/10').'?'.$_SERVER['QUERY_STRING']; ?>">10</a></li>
                                        <li><a href="<?php echo site_url('information-for-members/14').'?'.$_SERVER['QUERY_STRING']; ?>">14</a></li>
                                        <li><a href="<?php echo site_url('information-for-members/16').'?'.$_SERVER['QUERY_STRING']; ?>">16</a></li>
                                    </ul>
                                </div>
                                         
                                
                            </div>
                            </div>

                        </div>
                        <div class="clearfix"><?php echo $info->cms_second_block; ?>
                        	
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="feature-service clearfix">
        <div class="container">
        <h3><span></span></h3>
            <?php $this->load->view('deals/frontend/bottom_slider'); ?>        

        </div>
    </div>
 </div>
    <!-- media --> 
    <script type="text/javascript">
      var _all_postcode_list = <?php echo json_encode($postcode); ?>;
        $(document).ready(function() {
//       $("#club_postcode").autocomplete({source:  function(request, response) {
//        var code = $.ui.autocomplete.filter(postcode, request.term);
//        
//        response(code.slice(0, 10));
//    },
//    change: function (event, ui) {
//            if (!ui.item) {
//                this.value = '';
//            }
//        }
//        });
//$("#club_postcode").autocomplete({source:postcode});

        });
    </script>
        <script src="<?php echo base_url('assets/lib/js_loader/script.min.js')?>" ></script>
        <script src="<?php echo base_url('assets/js/autocomplete.js')?>" ></script>

