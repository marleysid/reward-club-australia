
<div class="container">

    <div class="myclub-wrap">

        <?php
        if (isset($userLoggedIn) and $userLoggedIn):
        ?>

        <h3>My Club</h3>
        <div class="myclub-holder">
            <div id="owl-myClub" class="owl-carousel">
                <?php if($all_favourites): ?>
                <?php foreach($all_favourites as $key=>$val): ?>
                        <div class="item">
                            <div class="image-wrap"><?php if(file_exists($val->club_logo) and $val->club_logo!=null): ?><img  src="<?php echo imager($val->club_logo,170,170); ?>" /></a><?php endif; ?></div>
                            <div class="info-wrap">
                                <strong class="title"><a href="<?php echo site_url('cms/club_detail/'  . safe_b64encode($val->club_id)); ?>"><?php echo $val->club_name?></a></strong>
                                <span class="location"><?php echo $val->club_address?></span>
                            </div>
                        </div>

                    <?php endforeach; ?>
            </div>

                <?php endif; ?>

            <?php else: ?>
                <div class="sub-page-banner-wrap"> <div class="slider">

                            <img src=" <?php echo get_asset('assets/frontend/images/img21.jpg'); ?>" alt="banner">

                    </div>

                </div>
            <?php endif; ?>
            <div class="myclub-content-wrapper">
                <div class="inner-holder active">
                    <div id="owl-fullSlider" class="owl-carousel">
                        <div class="item">

                            <div class="image-wrap"><?php if(file_exists($detail->club_home_image_path) and $detail->club_home_image_path!=null): ?><img  src="<?php echo imager($detail->club_home_image_path,870,300); ?>" /></a><?php endif; ?></div>

                        </div>

                        <div class="item">
                            <div class="image-wrap"><?php if(file_exists($detail->club_home_image_path2) and $detail->club_home_image_path2!=null): ?><img  src="<?php echo imager($detail->club_home_image_path2,870,300); ?>" /><?php else: ?><img  src="<?php echo imager($detail->club_home_image_path,870,300); ?>" /><?php endif; ?></div>
                        </div>
                    </div>
                    <div class="inner-content">
                        <div class="heading">
                            <h3><?php echo $detail->club_name; ?></h3>
                            <span class="view-website"><a href="<?php echo $detail->club_website;?>" target="_blank">View Website</a></span>
                        </div>
                        <?php
                        if (isset($userLoggedIn) and $userLoggedIn):
                        ?>
                        <?php
                        $userDetail_id = $userDetail->id;
                        ?>
                        <?php
                        $club_id = $detail->club_id;
                        ?>
                        <?php

                        $userDetail1 = $userDetail->club_id;


                        if ($userDetail1 == 0 AND  empty($favourite) ):
                        if ( empty($favourite) ):

                            ?>

                        <div class="heading">
                            <span class="view-website"><a href="<?php echo base_url("cms/club_favourite/". $userDetail_id . "/" . $club_id); ?>" >Favourite</a></span>
                                   </div>
                        <?php endif; ?>
                        <?php endif; ?>
                        <?php endif; ?>
                        <div class="description-block">
                            <p><?php echo $detail->club_description;?></p>
                        </div>
                        <div class="detail-info-block">
                            <div class="left-info-block">
                                <?php if ($facilities): ?>
                                <h3>Facilities</h3>
                                <ul class="facilities">

                                        <?php foreach ($facilities as $key => $val): ?>
                                            <li><a href="#"><?php echo $val->label; ?></a></li>

                                        <?php endforeach; ?>
                                    <?php endif; ?>

                                </ul>
                                <div class="detail-contact-wrap">
                                    <ul class="contact-list">
                                        <?php if ($contacts): ?>

                                            <?php if (isset($contacts->email1)): ?>         <li class="mail"><a href="#"><span>icon</span><?php echo $contacts->email1; ?></a></li>    <?php endif; ?>
                                            <?php if (isset($contacts->email2)): ?>     <li class="email"><a href="#"><span>icon</span><?php echo $contacts->email2; ?></a></li>   <?php endif; ?>
                                            <?php if (isset($contacts->phonenumber)): ?>     <li class="phone-no"><span>icon</span><?php echo $contacts->phonenumber; ?></li>   <?php endif; ?>
                                            <?php if (isset($contacts->address)): ?>      <li class="location"><span>icon</span><address><?php echo $contacts->address; ?></address></li>   <?php endif; ?>


                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="right-info-block">
                                <?php if ($events): ?>
                                <h3>Events & News</h3>
                                <div id="scrollbar1">
                                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                                    <div class="viewport">
                                        <div class="overview">
                                            <ul class="events-news-list">

                                                    <?php foreach ($events as $key => $val): ?>

                                                <li>
                                                    <div class="events-date-wrap">
                                                        <span class="calendar-icon">icon</span>
                                                        <span class="date">21</span>
                                                        <span class="month">NOV</span>
                                                    </div>
                                                    <div class="events-news-detail">
                                                        <h4><?php echo $val->heading ?></h4>
                                                        <p> <?php echo $val->description ?> </p>
                                                        <span class="readmore"><a href="#">Read More</a></span>
                                                    </div>
                                                </li>
                                                <?php endforeach; ?>
                                                <?php endif; ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<script>


    $(document).ready(function () {
        $("#status").on('click', function () {
            if (!confirm("Are you sure to change status of this app_dashboard?"))
                return false;
        });
    });
    </script>