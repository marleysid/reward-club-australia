<!-- banner -->

<?php
if (isset($userLoggedIn) and $userLoggedIn):
?>
<?php
$clubid = $userDetail->club_id;

?>
<?php endif; ?>
<style type="text/css">
    /*.modal .modal-content {*/
        /*border: none;*/
        /*border-radius: 0;*/
        /*box-shadow: none;*/
        /*width: 840px;*/
    /*}*/
    /*.modal .modal-body {*/
        /*border: none;*/
        /*width: 680px;*/
    /*}*/
    /*.modal .modal-dialog {*/
        /*width: 471px;*/
        /*border: 3px solid #292929;*/
        /*height: 643px;*/
    /*}*/

</style>



<div class="container">

<div class="sub-page-banner-wrap"> <div class="slider">
<?php if (file_exists("assets/uploads/cms/" . $info->cms_header_image)): ?>
<img src="<?php echo $info->cms_header_image ? base_url().'assets/uploads/cms/' . $info->cms_header_image : ''; ?>"  class="img-responsive" alt="slide">
<?php else: ?>
    <img src=" <?php echo get_asset('assets/frontend/images/img21.jpg'); ?>" alt="banner">
<?php endif;?>
</div>

</div>
<!--<div class="sub-page-banner-wrap"><img src="--><?php //echo get_asset('assets/frontend/images/img21.jpg'); ?><!--"  alt="banner"></div>-->
<!-- banner--> 


<div class="breadcrumb-wrap">
        <ul class="breadcrumb-list">
            <li><a href="<?php echo site_url();?>">home</a></li>
            <li>Club Information</li>
        </ul>
    </div>
    <?php if(!isset($member_logged_in) || (isset($userDetail->club_id) && $userDetail->club_id !=0)){?>
    <?php
        if (isset($userLoggedIn) and $userLoggedIn):
        ?>
        <div class="profile-info-wrap">

        <div class="image-wrap"><img  src="<?php echo imager($info->cms_image,319,318); ?>" /></a>

                <?php
                $userclub = $userDetail->club_id;
                if ($userclub != 0):
                    ?>
                    <span class="edit" data-toggle="modal" data-target="#cmsimageedit" ><a  attr="<?php echo $info->cms_id;?>" href="#">Edit</a></span>

            <?php endif; ?>
            <?php endif; ?>
        </div>

        <div class="profile-description-wrap">

            <?php
            if (isset($userLoggedIn) and $userLoggedIn):
            ?>
                <?php
                $userclub = $userDetail->club_id;
                if ($userclub != 0):
                ?>
                    <h3>Profile</h3>
                    <div class="descrip-wrap">
                        <?php echo $info->cms_content; ?>
                    </div>
                        <span class="edit" data-toggle="modal" data-target="#cmscontentedit" ><a  class="cmscontentedit" attr="<?php echo $info->cms_id;?>" href="#">Edit</a></span>
            <?php endif; ?>
                <?php else: ?>
                <div class="container">

                    <div class="information-wrapper">
                        <div class="video-wrapper add">
                            <strong class="title">Does your club card put local businesses “on sale”?</strong>
                            <div class="information-detail-wrap add01">
                                <p>Excite, Attract, Engage, Retain, Reward and Developyour most valuable asset!<br> Talk to the experts about a sustainablestrategic alliance with local business.</p>
                                <p>Whilst traditional points programs work your top 10% harder, our Community Alliance program developsyour entire database with more frequent contact points, massive value and active usage of up to 50%.Join over 50 clubs with over 700,000 members using ongoing member discounts at over 1400 local stores.We  you will be happy with the alliance we build or we won’t charge you a cent!</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="partner-wrap">
    <h3>Join these leading clubs</h3>
    <div class="partner-holder">
        <div id="owl-partner" class="owl-carousel">

                <?php if(isset($club_logo)): ?>
                    <?php foreach ($club_logo as $row): ?>
                        <div class="item">
                            <div class="image-wrap"> <img src="<?php  echo imager($row->club_logo ? $row->club_logo : '', 182, 170,0); ?>" class="img-responsive"></div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>

    </div>
    <div class="contact-wrap add01"><a href="#" data-toggle="modal" data-target="#clubContact">Contact us about Rewards Club</a></div>
</div>
<div id="clubContact" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
    <form id="contact_club" method="post">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Contact Us</h4>
          </div>
          <div class="modal-body">
            <div class="input-holder">
                <label for="name">Name of Club</label>
                <input type="text" id="name" name="name_of_club">
            </div>
            <div class="input-holder">
                <label for="street">Street Address</label>
                <textarea id="street" name="street_address"></textarea>
            </div>
            <div class="input-holder">
                <label for="post">Postcode</label>
                <input type="text" id="post" name="postcode">
            </div>
            <div class="input-holder">
                <label for="contact">Contact Name</label>
                <input type="text" id="contact" name="contact_name">
            </div>
            <div class="input-holder">
                <label for="phone">Phone Number</label>
                <input type="text" id="phone" name="contact_number">
            </div>
            <div class="input-holder">
                <label for="emailbox">Email</label>
                <input type="email" id="emailbox" name="email">
            </div>
          </div>
          <div class="modal-footer">
              <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" value="Submit">
          </div>
        </div>
    </form>
  </div>
</div>
            <?php endif; ?>
        </div>
        <div class="video-holder club-desc-video">
            <div class="wowload">
                <?php echo $info->cms_second_block;?>
            </div>
        </div>
        

    </div>
    <?php } ?>
</div>
<!------------------------------------------------------------ Edit cms content ------------------------------------------------------->
<div class="edit-popup-wrapper">

    <div class="modal " id="cmscontentedit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->

            <form method="post" id="" >

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit CMS</h4>
                    </div>


                    <div class="modal-body">
                        <div class="input-holder <?php echo form_error('label') ? 'has-error' : '' ?>">
                            <input type="hidden" name="id" id="editid"  value="<?php echo $info->cms_id;?>">
                            <label for="label">Edit CMS</label>

<!--                            <input type="text" name="cms_content" id="cms_content" required>-->
                            <textarea type="text" id="cms_content" placeholder=" Content" name="cms_content" class="form-control"></textarea>
                            <?php echo display_ckeditor('cms_content'); ?>
                            <!--                                            --><?php //echo form_error('label'); ?>
                        </div>


                    </div>
                    <div class="modal-footer">

                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn cms_edit"  attr="<?php echo $info->cms_id;?>" value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!------------------------------------------------------------ Edit CMS Image ------------------------------------------------------->
<div class="edit-popup-wrapper">

    <div class="modal " id="cmsimageedit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <!-- Modal content-->

            <form method="post" id=""  enctype="multipart/form-data" action="cms/update_cmsimage">

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Image</h4>
                    </div>


                    <div class="modal-body">
<!--                        <div class="form-group  --><?php //echo @$logo_error ? 'has-error' : '' ?><!--">-->
<!--                            <label for="club_logo" class="control-label col-lg-3">Logo *</label>-->
<!--                            <div class="col-lg-7">-->
<!--                                <div class="fileinput fileinput-new" data-provides="fileinput">-->
<!--                            <span class="btn btn-default btn-file">-->
<!--                                <span class="fileinput-new">Select Image</span>-->
<!--                                <span class="fileinput-exists">Change</span>-->
<!--                                <input type="file" name="cms_image" id="cms_image">-->
<!--                            </span>-->
<!--                                    <span class="fileinput-filename"></span>-->
<!--                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>-->
<!--                                    --><?php //echo @$logo_error; ?>
<!--                                </div>-->
<!--                                --><?php //if (file_exists($info->cms_image)): ?>
<!--                                    <div>-->
<!--                                        <img src="--><?php //echo imager($info->cms_image, 128, 128); ?><!--" alt="club image" class="img-thumbnail">-->
<!---->
<!--                                    </div>-->
<!--                                --><?php //endif; ?>
<!--                            </div>-->
<!--                        </div>-->


                        <div class="form-group">
                            <input type="hidden" name="id" id="editid"  value="<?php echo $info->cms_id;?>">
                            <label for="edit_article_image">Image</label>
                            <input type="file"  placeholder="" name="cms_image" class="form-control" value="<?php echo $info->cms_image;?>" required>
                        </div>
                        <div>
                            <img src="<?php echo imager($info->cms_image, 128, 128); ?>" alt="club image" class="img-thumbnail">
                            <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                        </div>

                    </div>
                    <div class="modal-footer">

                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn "  attr="<?php echo $info->cms_id;?>"  value="Submit">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
if (isset($userLoggedIn) and $userLoggedIn):
?>

    <?php
    $userDetail = $userDetail->club_id;
    if ($userDetail != 0):
    ?>
        <!------------------------------------------------------- Add Facilities -------------------------------------------->
        <div class="container">
            <div class="list-wrap">
                <div class="block-heading">
                    <h3>Manage Facilities</h3>
                    <span class="add-wrap"><span class="add" ></span><a href="#" data-toggle="modal" data-target="#myModal"> Add Facility</a></span>


                </div>

                <div class="edit-popup-wrapper">

                        <div class="modal " id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->

                            <form method="post" id="addfacilities" action="cms/add_facility">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Facilities</h4>
                                    </div>

                                    <div class="modal-body">

                                        <div class="input-holder <?php echo form_error('condition') ? 'has-error' : '' ?>">

                                            <input type="hidden" name="club_user_id" id="label" value="<?php echo $userDetail; ?>">

                                        </div>
                                        <div class="input-holder ">
                                            <label for="label">Label</label>

                                            <input type="text" name="label" id="label" required>

                                        </div>

                                        <div class="input-holder <?php echo form_error('condition') ? 'has-error' : '' ?>">
                                            <label for="condition">condition</label>
                                            <input type="text"  name="condition" id="condition" required>

                                        </div>
                                        <div class="input-holder <?php echo form_error('decription') ? 'has-error' : '' ?>">
                                            <label for="description">description</label>
                                            <textarea id="description" name="description" cols="30" rows="2" required></textarea>

                                        </div>
                                    </div>
                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="facilitis_submit" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-------------------------------------------------------- Display Facilities -------------------------------------------------------------->
                <div id="scrollbar1">
                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                    <div class="viewport">
                        <div class="overview">
                            <div class="table-wrap">
                                <table>
                                    <thead>
                                    <tr>
                                        <th class="first">#</th>
                                        <th class="second">label</th>
                                        <th class="third">condition</th>
                                        <th class="fourth">description</th>
                                        <th class="fifth"></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php if ($clubfacilitydetail): ?>
                                        <?php foreach ($clubfacilitydetail as $key => $val): ?>
                                            <tr class="odd">
                                                <td class="first"><?php echo ++$key; ?></td>
                                                <td class="second"><?php echo $val->label; ?></td>
                                                <td class="third"><?php echo $val->condition; ?></td>
                                                <td class="fourth"><?php echo $val->description; ?></td>

                                                <td class="fifth">
                                                    <span class="edit" data-toggle="modal" data-target=".myFacilitiesedit" ><a  class="editfacilities" attr="<?php echo $val->id;?>" href="#">Edit</a></span>

                                                    <a href="<?php echo base_url('cms/deletefacilities/' . $val->id); ?>"  onclick='if (!confirm("Are you sure to delete?"))
                                                            return false;' data-toggle="tooltip" title="Delete"  <span class="delete">delete</span><i class="fa fa-times"></i> </a>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!----------------------------- Edit Facilities ------------------------->
            <div class="edit-popup-wrapper">

                <div class="modal myFacilitiesedit"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <!-- Modal content-->

                        <form method="post" id="editfacilities" action="cms/update_facilities">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Edit Facilities</h4>
                                </div>


                                <div class="modal-body">
                                    <div class="input-holder <?php echo form_error('label') ? 'has-error' : '' ?>">
                                        <input type="hidden" name="clubid" id="clbid"  value="<?php echo $clubid ?>">
                                        <input type="hidden" name="id" id="editfacilitiesid"  value="">
                                        <label for="label">Label</label>

                                        <input type="text" name="label" id="editfacilitieslabel" required>
                                        <!--                                            --><?php //echo form_error('label'); ?>
                                    </div>

                                    <div class="input-holder <?php echo form_error('condition') ? 'has-error' : '' ?>">
                                        <label for="condition">condition</label>
                                        <input type="text"  name="condition" id="editfacilitiescondition" required>
                                        <?php echo form_error('condition'); ?>
                                    </div>
                                    <div class="input-holder <?php echo form_error('decription') ? 'has-error' : '' ?>">
                                        <label for="description">description</label>
                                        <textarea id="editfacilitiesdescription" name="description" cols="30" rows="2" required></textarea>
                                        <?php echo form_error('decription'); ?>
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="facilitis_submit" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!----------------------------------------------- Add Events ------------------------------------------------------------------>
            <div class="list-wrap">
                <div class="block-heading">
                    <h3>Manage Events</h3>
                    <span class="add-wrap"><span class="add" ></span><a href="#" data-toggle="modal" data-target="#myEvent"> Add Event</a></span>

                </div>
                <div class="edit-popup-wrapper">

                    <div class="modal " id="myEvent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->

                            <form method="post" id="addevents" action="cms/add_events">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Events</h4>
                                    </div>

                                    <div class="modal-body">
                                        <div class="input-holder <?php echo form_error('condition') ? 'has-error' : '' ?>">

                                            <input type="hidden" name="club_user_id" id="label" value="<?php echo $userDetail; ?>">
                                            <?php echo form_error('condition'); ?>
                                        </div>


                                        <div class="input-holder <?php echo form_error('heading') ? 'has-error' : '' ?>">
                                            <label for="heading">Heading</label>
                                            <input type="text"  name="heading" id="heading" required>
                                            <?php echo form_error('heading'); ?>
                                        </div>
                                        <div class="input-holder <?php echo form_error('decription') ? 'has-error' : '' ?>">
                                            <label for="description">description</label>
                                            <textarea id="description" name="description" cols="30" rows="2" required></textarea>
                                            <?php echo form_error('decription'); ?>
                                        </div>
                                    </div>
                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="facilitis_submit" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!----------------------------------- Display Events --------------------------------------->
                <div id="scrollbar2">
                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                    <div class="viewport">
                        <div class="overview">
                            <div class="table-wrap">
                                <table>
                                    <thead>
                                    <tr>
                                        <th class="first">#</th>
                                        <th class="third">Heading</th>
                                        <th class="fourth">description</th>
                                        <th class="fifth"></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php if ($clubeventdetail): ?>
                                        <?php foreach ($clubeventdetail as $key => $val): ?>
                                            <tr class="odd">
                                                <td class="first"><?php echo ++$key; ?></td>

                                                <td class="third"><?php echo $val->heading; ?></td>
                                                <td class="fourth"><?php echo $val->description; ?></td>

                                                <td class="fifth">
                                                    <span class="edit" data-toggle="modal" data-target="#myEventedit" ><a  class="editevent" attr="<?php echo $val->id;?>" href="#">Edit</a></span>
                                                    <a href="<?php echo base_url('cms/deleteevent/' . $val->id); ?>"  onclick='if (!confirm("Are you sure to delete?"))
                                                            return false;' data-toggle="tooltip" title="Delete"  <span class="delete">delete</span><i class="fa fa-times"></i> </a>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!------------------------------------------------ Edit Events --------------------------------------------------------------------->
            <div class="edit-popup-wrapper">

                <div class="modal " id="myEventedit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <!-- Modal content-->

                        <form method="post" id="editevents" action="cms/update_events">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Edit Events</h4>
                                </div>


                                <div class="modal-body">
                                    <div class="input-holder <?php echo form_error('label') ? 'has-error' : '' ?>">
                                        <input type="hidden" name="id" id="editidevent"  value="">
                                        <input type="hidden" name="clubid" id="clbid"  value="<?php echo $clubid ?>">
                                        <label for="label">Heading</label>

                                        <input type="text" name="heading" id="editlabelevent" required>
                                        <!--                                            --><?php //echo form_error('label'); ?>
                                    </div>


                                    <div class="input-holder <?php echo form_error('decription') ? 'has-error' : '' ?>">
                                        <label for="description">description</label>
                                        <textarea id="editdescriptionevent" name="description" cols="30" rows="2" required></textarea>
                                        <?php echo form_error('decription'); ?>
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--------------------------------------------------------- Add Features ------------===============-------------------------->
            <div class="list-wrap add">
                <div class="block-heading">
                    <h3>Manage Featured Offers</h3>
                    <span class="add-wrap"><span class="add" ></span><a href="#" data-toggle="modal" data-target="#myfeature"> Add Features</a></span>

                </div>
                <div class="edit-popup-wrapper">

                    <div class="modal " id="myfeature" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->

                            <form method="post" id="addfeature" action="cms/add_features">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Events</h4>
                                    </div>

                                    <div class="modal-body">
                                        <div class="input-holder <?php echo form_error('condition') ? 'has-error' : '' ?>">

                                            <input type="hidden" name="club_user_id" id="label" value="<?php echo $userDetail; ?>">
                                            <?php echo form_error('condition'); ?>
                                        </div>
                                        <div class="input-holder <?php echo form_error('label') ? 'has-error' : '' ?>">
                                            <label for="label">Label</label>

                                            <input type="text" name="label" id="label" required>
                                            <!--                                            --><?php //echo form_error('label'); ?>
                                        </div>

                                        <div class="input-holder <?php echo form_error('condition') ? 'has-error' : '' ?>">
                                            <label for="condition">condition</label>
                                            <input type="text"  name="condition" id="condition" required>
                                            <?php echo form_error('condition'); ?>
                                        </div>
                                        <div class="input-holder <?php echo form_error('decription') ? 'has-error' : '' ?>">
                                            <label for="description">description</label>
                                            <textarea id="description" name="description" cols="30" rows="2" required></textarea >
                                            <?php echo form_error('decription'); ?>
                                        </div>
                                    </div>
                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="facilitis_submit" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!-------------------------------------- Display Features -------------------------------------->
                <div id="scrollbar3">
                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                    <div class="viewport">
                        <div class="overview">
                            <div class="table-wrap">
                                <table>
                                    <thead>
                                    <tr>
                                        <th class="first">#</th>
                                        <th class="second">label</th>
                                        <th class="third">condition</th>
                                        <th class="fourth">description</th>
                                        <th class="fifth"></th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php if ($clubfeaturedetail): ?>
                                        <?php foreach ($clubfeaturedetail as $key => $val): ?>
                                            <tr class="odd">
                                                <td class="first"><?php echo ++$key; ?></td>
                                                <td class="second"><?php echo $val->label; ?></td>
                                                <td class="third"><?php echo $val->condition; ?></td>
                                                <td class="fourth"><?php echo $val->description; ?></td>

                                                <td class="fifth">
                                                    <span class="edit" data-toggle="modal" data-target="#myfeatures" ><a  class="editfeatures" attr="<?php echo $val->id;?>" href="#">Edit</a></span>
                                                   <a href="<?php echo base_url('cms/deletefeatures/' . $val->id); ?>"  onclick='if (!confirm("Are you sure to delete?"))
                                                            return false;' data-toggle="tooltip" title="Delete"  <span class="delete">delete</span><i class="fa fa-times"></i> </a>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-------------------------------------- Edit Features -------------------------------------->
            <div class="edit-popup-wrapper">

                <div class="modal " id="myfeatures" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <!-- Modal content-->

                        <form method="post" id="editevents" action="cms/update_features">

                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Edit Features</h4>
                                </div>


                                <div class="modal-body">
                                    <div class="input-holder <?php echo form_error('label') ? 'has-error' : '' ?>">
                                        <input type="hidden" name="id" id="editfeatiresid"  value="">
                                        <input type="hidden" name="clubid" id="clbid"  value="<?php echo $clubid ?>">
                                        <label for="label">Label</label>

                                        <input type="text" name="label" id="editfeatureslabel" required>
                                        <!--                                            --><?php //echo form_error('label'); ?>
                                    </div>

                                    <div class="input-holder <?php echo form_error('condition') ? 'has-error' : '' ?>">
                                        <label for="condition">condition</label>
                                        <input type="text"  name="condition" id="editfeaturecondition" required>
                                        <?php echo form_error('condition'); ?>
                                    </div>
                                    <div class="input-holder <?php echo form_error('decription') ? 'has-error' : '' ?>">
                                        <label for="description">description</label>
                                        <textarea id="editfeaturesdescription" name="description" cols="30" rows="2" required></textarea>
                                        <?php echo form_error('decription'); ?>
                                    </div>
                                </div>
                                <div class="modal-footer">

                                    <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="" value="Submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-------------------------------------- Add Contact -------------------------------------->
            <div class="contact-detail-wrap">

        <?php
//        $userDetail = $userDetail->club_id;
        if ($clubcontactcount != 1):
            ?>
                <div class="block-heading">
                    <h3>Manage Contact Details</h3>
                    <span class="add-wrap"><span class="add" ></span><a href="#" data-toggle="modal" data-target="#myContact"> Add Contacts</a></span>


                </div>
            <?php endif; ?>
                <div class="edit-popup-wrapper">

                    <div class="modal " id="myContact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->

                            <form method="post" id="addcontacts" action="cms/add_contact">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Add Contacts</h4>
                                    </div>

                                    <div class="modal-body">
                                        <div class="input-holder <?php echo form_error('condition') ? 'has-error' : '' ?>">

                                            <input type="hidden" name="club_user_id" id="label" value="<?php echo $userDetail; ?>">
                                            <?php echo form_error('condition'); ?>
                                        </div>
                                        <div class="input-holder <?php echo form_error('email1') ? 'has-error' : '' ?>">
                                            <label for="label">Company Email</label>

                                            <input type="text" name="email1" id="email1" required>
                                            <!--                                            --><?php //echo form_error('label'); ?>
                                        </div>

                                        <div class="input-holder <?php echo form_error('email2') ? 'has-error' : '' ?>">
                                            <label for="condition">Email 2nd</label>
                                            <input type="text"  name="email2" id="email2" required>
                                            <?php echo form_error('email2'); ?>
                                        </div>
                                        <div class="input-holder <?php echo form_error('domain') ? 'has-error' : '' ?>">
                                            <label for="description">Domain</label>
                                            <input type="text" id="domain" name="domain" required>
                                            <?php echo form_error('domain'); ?>
                                        </div>
                                        <div class="input-holder <?php echo form_error('phonenumber') ? 'has-error' : '' ?>">
                                            <label for="description">Phone</label>
                                            <input type="text" id="phonenumber" name="phonenumber"  required>
                                            <?php echo form_error('phonenumber'); ?>
                                        </div>
                                        <div class="input-holder <?php echo form_error('address') ? 'has-error' : '' ?>">
                                            <label for="description">Address</label>
                                            <input type="text" id="address" name="address" required>
                                            <?php echo form_error('address'); ?>
                                        </div>
                                    </div>
                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" id="facilitis_submit" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!------------------------------------------------------------ Display Contact ------------------------------------------------------->
                <div class="contact-detail-inner-wrap">
                    <h4>Old Mate Lappo</h4>
                    <div class="contact-columns-holder">

                        <?php if ($clubcontactdetail): ?>

                        <ul class="contact-list">


                            <li class="mail"><span class="icon">icon</span><a href="#"> <?php echo $clubcontactdetail->email1; ?></a> <span class="edit" data-toggle="modal" data-target="#editemail1" ><a  class="edit_contact" attr="<?php echo $clubcontactdetail->id;?>" href="#">Edit</a></span></li>
                            <li class="email"><span class="icon">icon</span><a href="#"><?php echo $clubcontactdetail->email2; ?></a> <span class="edit" data-toggle="modal" data-target="#editemail2" ><a class="edit_contact" attr="<?php echo $clubcontactdetail->id;?>" href="#">Edit</a></span></li>
                            <li class="web"><span class="icon">icon</span><a href="#"> <?php echo $clubcontactdetail->domain; ?></a> <span class="edit" data-toggle="modal" data-target="#editdomain" ><a class="edit_contact" attr="<?php echo $clubcontactdetail->id;?>" href="#">Edit</a></span></li>
                            <li class="phone-no"><span class="icon">icon</span><span> <?php echo $clubcontactdetail->phonenumber; ?></span> <span class="edit" data-toggle="modal" data-target="#editphonenumber" ><a class="edit_contact" attr="<?php echo $clubcontactdetail->id;?>" href="#">Edit</a></span></li>
                            <li class="location"><span class="icon">icon</span><address> <?php echo $clubcontactdetail->address; ?></address><span class="edit" data-toggle="modal" data-target="#editaddress"><a class="edit_contact" attr="<?php echo $clubcontactdetail->id;?>"href="#">Edit</a></span></li>
                        </ul>

                        <?php endif; ?>


                    </div>
                </div>
                <!------------------------------------------------------------ Edit Contact Company Email ------------------------------------------------------->
                <div class="edit-popup-wrapper">

                    <div class="modal " id="editemail1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->

                            <form method="post" id="editemail1_edit">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Email1</h4>
                                    </div>


                                    <div class="modal-body">
                                        <div class="input-holder <?php echo form_error('label') ? 'has-error' : '' ?>">
                                            <input type="hidden" name="editcontactid" id="editcontactid"  value="<?php echo $clubcontactdetail->id;?>">
                                            <label for="label">Email 1</label>

                                            <input type="text" name="email1edit" id="email1edit" required>
                                            <!--                                            --><?php //echo form_error('label'); ?>
                                        </div>


                                    </div>
                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn club_contact_edit" attr="<?php echo $clubcontactdetail->id;?>"value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="edit-popup-wrapper">

                    <div class="modal " id="editemail2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->

                            <form method="post" id="editemail2_edit">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Email2</h4>
                                    </div>


                                    <div class="modal-body">
                                        <div class="input-holder <?php echo form_error('label') ? 'has-error' : '' ?>">
                                            <input type="hidden" name="id" id="editid"  value="">
                                            <label for="label">Email 2</label>

                                            <input type="text" name="email2edit" id="email2edit" required>
                                            <!--                                            --><?php //echo form_error('label'); ?>
                                        </div>


                                    </div>
                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn club_contact_edit" attr="<?php echo $clubcontactdetail->id;?>" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="edit-popup-wrapper">

                    <div class="modal " id="editdomain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->

                            <form method="post" id="editdomain_edit">

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Domain</h4>
                                    </div>


                                    <div class="modal-body">
                                        <div class="input-holder <?php echo form_error('label') ? 'has-error' : '' ?>">
                                            <input type="hidden" name="id" id="editid"  value="">
                                            <label for="label">Domain</label>

                                            <input type="text" name="domainedit" id="domainedit" required>
                                            <!--                                            --><?php //echo form_error('label'); ?>
                                        </div>


                                    </div>
                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn club_contact_edit"  attr="<?php echo $clubcontactdetail->id;?>" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="edit-popup-wrapper">

                    <div class="modal " id="editaddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->

                            <form method="post" id="editaddress_edit" >

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Address</h4>
                                    </div>


                                    <div class="modal-body">
                                        <div class="input-holder <?php echo form_error('label') ? 'has-error' : '' ?>">
                                            <input type="hidden" name="id" id="editid"  value="">
                                            <label for="label">Address</label>

                                            <input type="text" name="addressedit" id="addressedit" required>
                                            <!--                                            --><?php //echo form_error('label'); ?>
                                        </div>


                                    </div>
                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn club_contact_edit"  attr="<?php echo $clubcontactdetail->id;?>" value="Submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="edit-popup-wrapper">

                    <div class="modal " id="editphonenumber" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <!-- Modal content-->

                            <form method="post" id="editphonenumber_edit" >

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Edit Phone</h4>
                                    </div>


                                    <div class="modal-body">

                                        <div class="input-holder <?php echo form_error('phonnumberedit') ? 'has-error' : '' ?>">
                                            <input type="hidden" name="id" id="editid"  value="">
                                            <label for="label">Phone</label>

                                            <input type="text" name="phonnumberedit" id="phonnumberedit" required>
                                            <?php echo form_error('phonnumberedit'); ?>
                                        </div>


                                    </div>
                                    <div class="modal-footer">

                                        <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn club_contact_edit"  attr="<?php echo $clubcontactdetail->id;?>" value="Submit">
                                    </div>
                                </div>
                            </form>


                        </div>
                    </div>
                </div>
            </div>

        </div>

<?php else: ?>
    <div class="container"> 
    <div class="myclub-wrap">
        <h3>My Club</h3>
        <div class="myclub-holder">
            <div id="owl-myClub" class="owl-carousel">
                <?php 
                    if (!empty($clubs_all)){
                    foreach ($clubs_all as $key => $value) {                    
                ?>
                <div class="item <?php echo ($value->club_id == $club_detail->club_id) ? 'main-active' : '';?>">
                       <?php
                        $myChars = array(" ", "(", ")");
                        $replaceChars = array('-', '-', '-');
                    ?>
                    <div class="image-wrap">
                        <a href="<?php echo base_url('information-for-clubs/'.safe_b64encode($value->club_id)) . '/' . strtolower(str_replace($myChars, $replaceChars, $value->club_name));?>"><img src="<?php echo imager($value->club_home_image_path, 199, 199, 1); ?>"  alt="image04"></a>
                    </div>
                    <div class="info-wrap">
                 
                        <strong class="title"><a href="<?php echo base_url('information-for-clubs/'.safe_b64encode($value->club_id)) . '/' . strtolower(str_replace($myChars, $replaceChars, $value->club_name));?>"><?php echo substr($value->club_name, 0, 16)?></a></strong>
                        <span class="location"><?php echo $value->club_suburb;?></span>
                    </div>
                </div> 
                <?php } } ?>
                              
            </div>

            <div class="myclub-content-wrapper">
                <div class="inner-holder active">
                    <div id="owl-fullSlider" class="owl-carousel">
                        <div class="item">

                            <div class="image-wrap">
                            <img src="<?php echo imager($club_detail->club_home_image_path, 872, 300, 0); ?>"  alt="image04">
                            </div>
                        </div> 
                        <div class="item">
                            <div class="image-wrap"><img src="<?php echo imager($club_detail->club_home_image_path2, 872, 300, 0); ?>"  alt="image05"></div>
                        </div>       
                    </div>
                    <div class="inner-content">
                        <div class="heading">
                            <h3><?php echo $club_detail->club_name;?></h3>
                            <span class="view-website"><a href="<?php echo "http://" . $club_detail->club_website;?>" target="_blank">View Website</a></span>
                        </div>
                        <div class="description-block">
                            <p><?php echo $club_detail->club_description;?></p>
                        </div>
                        <div class="detail-info-block">
                            <div class="left-info-block">
                                <h3>Facilities</h3>
                                <ul class="facilities">
                                <?php if (!empty($facility)): ?>
                                <?php foreach($facility as $key => $val):?>
                                    <li><a href="#"><?php echo $val->label;?></a></li>
                                <?php endforeach;?>
                               <?php else:?>
                                Facilities not available
                               <?php endif;?>
                                </ul>
                                <div class="detail-contact-wrap">
                                    <ul class="contact-list">
                                        <li class="mail"><a href="#"><span>icon</span><?php echo (!empty($contact->email1) ? $contact->email1 : 'Not Available');?></a></li>
                                        <li class="email"><a href="#"><span>icon</span><?php echo $club_detail->fax;?></a></li>
                                        <li class="phone-no"><span>icon</span><?php echo $club_detail->club_phone;?></li>
                                        <li class="location"><span>icon</span><address><?php echo $club_detail->club_address;?></address></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="right-info-block">
                                <h3>Events & News</h3>
                                <div id="scrollbar1">
                                    <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                                    <div class="viewport">
                                        <div class="overview">
                                            <ul class="events-news-list">
                                            <?php if(!empty($event)):?>
                                            <?php foreach($event as $key => $val):?>
                                                <li>
                                                    <div class="events-date-wrap">
                                                        <span class="calendar-icon">icon</span>
                                                        <span class="date">
                                                        <?php 
                                                            $getDay = strtotime($val->posted_on);
                                                            echo date("d", $getDay);
                                                        ?>
                                                            
                                                        </span>
                                                        <span class="month"><?php echo strtoupper(date("M", $getDay));?></span>
                                                    </div>
                                                    <div class="events-news-detail">
                                                        <h4><?php echo $val->heading;?></h4>
                                                        <p><?php echo $val->description;?></p>
                                                    </div>
                                                </li>
                                            <?php endforeach;?>
                                            <?php else:?>
                                                <li>
                                                    <div class="events-date-wrap">
                                                        &nbsp;
                                                    </div>
                                                    <div class="events-news-detail">
                                                        <h4>No events currently</h4>
                                                    </div>
                                                </li>
                                            <?php endif;?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

<?php endif; ?>
<div class="media-block add">
    <div class="featured-club-news-holder add">
    <h3><span>Rewards Club News</span></h3>
    <div id="owl-news" class="owl-carousel club-news">
        <?php if (isset($news_detail)): ?>
        <?php foreach ($news_detail as $news): ?>
    <div>
    <a href="<?php echo site_url('news').'/'.safe_b64encode($news->news_id) .'/'.strtolower(url_title($news->news_title,'-','TRUE')) ?>"> <img src="<?php echo imager($news->news_image ? 'assets/uploads/news/'.$news->news_image:'',218,106,1); ?>" class="img-responsive"></a>
    <div class="info">
        <span><?php echo date("d F", strtotime($news->news_create_date)); ?></span>
        <h4><a href="<?php echo site_url('news').'/'.safe_b64encode($news->news_id).'/'.strtolower(url_title($news->news_title,'-','TRUE')) ?>"><?php echo character_limiter(ucwords($news->news_title), 20); ?></a></h4>
        <p><?php echo character_limiter($news->news_excerpt, 75) ?> </p>
        <!--                            <button class="btn btn-danger">Read More</button>-->
        <?php echo anchor("news/".safe_b64encode($news->news_id).'/'.strtolower(url_title($news->news_title,'-','TRUE')), 'Read More', 'class="btn btn-danger"'); ?>
    </div>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>
    </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo site_url('assets/frontend/js/info-for-clubs.js'); ?>"></script>
<!-- media -->
<script type="text/javascript">
$(function(){
    $('.orders-above').on('click',function(){
        $('html, body').animate({
        scrollTop: $(".menu-detail").offset().top},
        'slow');
    });

//    $(document).scrollTo('.orders-above');
});
</script>
<script type="text/javascript">
    $('.editevent').click(function (event)
    {
      var helpdiv = $(this).attr('attr');

        $.ajax({

            url:  base_url + 'cms/editevent',
            type: 'GET',
            dataType:'json',
            data:{'helpdiv': helpdiv},
            success: function (data) {
                var editid = data.id;
                var editLabel = data.heading;
                var editdescription = data.description;
                $('textarea#editdescriptionevent').val(editdescription);
//                console.log(editLabel);
                document.getElementById('editidevent').value = editid;
                document.getElementById('editlabelevent').value = editLabel;
                document.getElementById('editconditionevent').value = editcondition;


            }
        });

    });
</script>
<script type="text/javascript">
    $('.editfacilities').click(function (event)
    {
        var helpdiv = $(this).attr('attr');
        $.ajax({

            url:  base_url + 'cms/editfacilities',
            type: 'GET',
            dataType:'json',
            data:{'helpdiv': helpdiv},
            success: function (data) {
                var editid = data.id;
                var editLabel = data.label;
                var editcondition = data.condition;
                var editdescription = data.description;
                $('textarea#editfacilitiesdescription').val(editdescription);
//                console.log(editLabel);
                document.getElementById('editfacilitiesid').value = editid;
                document.getElementById('editfacilitieslabel').value = editLabel;
                document.getElementById('editfacilitiescondition').value = editcondition;
            }
        });

    });
</script>
<script type="text/javascript">
    $('.editfeatures').click(function (event)
    {
        var helpdiv = $(this).attr('attr');

        $.ajax({

            url:  base_url + 'cms/editfeatures',
            type: 'GET',
            dataType:'json',
            data:{'helpdiv': helpdiv},
            success: function (data) {
                var editfeatiresid = data.id;
                var editfeatureslabel = data.label;
                var editfeaturecondition = data.condition;
                var editfeaturesdescription = data.description;
                $('textarea#editfeaturesdescription').val(editfeaturesdescription);
//                console.log(editLabel);
                document.getElementById('editfeatiresid').value = editfeatiresid;
                document.getElementById('editfeatureslabel').value = editfeatureslabel;
                document.getElementById('editfeaturecondition').value = editfeaturecondition;


            }
        });

    });
</script>
<script type="text/javascript">
    $('.edit_contact').click(function (event)
    {

        var helpdiv = $('.edit_contact').attr('attr');

        $.ajax({

            url:  base_url + 'cms/editcontacts',
            type: 'GET',
            dataType:'json',
            data:{'helpdiv': helpdiv},
            success: function (data) {
                var editemail = data.email1;
                document.getElementById('email1edit').value = editemail;
                var editemail2 = data.email2;
                document.getElementById('email2edit').value = editemail2;
                var editaddress = data.address;
                document.getElementById('addressedit').value = editaddress;
                var editdomain = data.domain;
                document.getElementById('domainedit').value = editdomain;
                var editphone = data.phonenumber;
                document.getElementById('phonnumberedit').value = editphone;
            }
        });

    });
</script>
<script type="text/javascript">



        var validator = $('#editemail1_edit').validate({

            rules: {
                email1edit: {
                    required: true,
                    email: true
                }
            }


        });
        var validator = $('#editemail2_edit').validate({

            rules: {

                email2edit: 'required'

            }


        });
        var validator = $('#editdomain_edit').validate({

            rules: {
                domainedit:'required'

            }

        });
        var validator = $('#editaddress_edit').validate({

            rules: {
                addressedit: 'required'
            },
            messages: {
                address: 'Please write your address'
            }


        });
        var validator = $('#editphonenumber_edit').validate({

            rules: {
                phonnumberedit:'required'
            }


        });

</script>
<script type="text/javascript">

    //$('.club_contact_edit').click(function (event)
    $(".club_contact_edit").on('submit', function ()
    {
        if ($(this).valid()) {
            var id = $('#editcontactid').val();
            var email1 = $('#email1edit').val();
            var email2 = $('#email2edit').val();
            var address = $('#addressedit').val();
            var domain = $('#domainedit').val();
            var phone = $('#phonnumberedit').val();

            $.ajax({

                url:  base_url + 'cms/updatecontacts',
                type: 'POST',
                dataType:'json',
                data: {
                    'id':id,
                    'email1': email1,
                    'email2': email2,
                    'address':address,
                    'domain':domain,
                    'phone':phone
                },

                success: function (data) {
                    alert('Data updated successfully');
                }
            });
        } 
        //return false;
    });

</script>

<script type="text/javascript">
    $('.cmscontentedit').click(function (event)
    {

        var helpdiv = $('.cmscontentedit').attr('attr');

        $.ajax({

            url:  base_url + 'cms/editcms',
            type: 'GET',
            dataType:'json',
            data:{'helpdiv': helpdiv},
            success: function (data) {
//                alert(data.cms_content);
//                var cms_content = data.cms_content;
//                document.getElementById('cms_content').value = cms_content;
                CKEDITOR.instances['cms_content'].setData(data.cms_content);

            }
        });

    });
</script>
<script type="text/javascript">
    $('.cms_edit').click(function (event)
    {

        var id = $('#editid').val();
        var cms_value = CKEDITOR.instances['cms_content'].getData();
        $.ajax({

            url:  base_url + 'cms/updatecms',
            type: 'POST',
            dataType:'json',
            data: {
                'id':id,
                'cms_value': cms_value

            },

            success: function (data) {
                console.log(data);
                alert('Data updated successfully');
            }
        });

    });

</script>
<script type="text/javascript">
    $('.cmsimage_edit').click(function (event)
    {

        var id = $('#editid').val();
        var imgUrl = $('#cms_image').val();
        alert(imgUrl);
       alert(id);
        $.ajax({

            url:  base_url + 'cms/update_cmsimage',
            type: 'POST',
            dataType:'json',
            data: {
                'id':id,
                'img': img

            },

            success: function (data) {
                console.log(data);
                alert('Data updated successfully');
            }
        });

    });

</script>
<!--<script type="text/javascript" src="--><?php //echo site_url('assets/frontend/js/club_info.js'); ?><!--"></script>-->



