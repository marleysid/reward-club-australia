
                    <!--start of club result-->
                        <ul class="search-result-list">
                           <?php foreach ($get_clubs as $clubs): ?> 
                            <li>
                                <div class="search-result-block">
                                    <div class="image-wrap"><a href="<?php echo site_url('cms/club_detail/'  . safe_b64encode($clubs->club_id)).'/'.strtolower(url_title($clubs->club_name,'-','TRUE')) ?>">
                                    <?php if(file_exists($clubs->club_logo) and $clubs->club_logo!=null): ?><img  src="<?php echo $clubs->club_logo; ?>"  alt="logo" class="img-responsive"/><?php else: ?><img src="<?php echo get_asset('assets/frontend/images/logo.png'); ?>"  alt="holiday crown"><?php endif; ?></a></div>
                                    <div class="result-detail-wrap">
                                        <div class="result-detail-heading">
                                            <div class="result-title-wrap">
                                                <strong class="title"><a href="<?php echo site_url('cms/club_detail/'  . safe_b64encode($clubs->club_id)).'/'.strtolower(url_title($clubs->club_name,'-','TRUE')) ?>"><?php echo $clubs->club_name;?></a></strong>
                                                <strong class="sub-title"><?php echo $clubs->first_name.' '.$clubs->last_name;?></strong>
                                            </div>
                                            <span class="search-option"><a href="<?php echo site_url('cms/club_detail/'  . safe_b64encode($clubs->club_id)).'/'.strtolower(url_title($clubs->club_name,'-','TRUE')) ?>">Detail</a></span>
                                        </div>
                                        <div class="result-contact-info-wrap">
                                            <div class="address"><address>
                                            <?php echo ucfirst($clubs->club_address) ;?>
                                            <?php echo (!empty($clubs->club_suburb)) ? ', ' . $clubs->club_suburb : '';?>
                                            <?php echo $clubs->club_state ;?>
                                            <?php echo $clubs->club_postcode ;?>
                                            <!-- 185 George Street Liverpool NSW 2170 --> 
                                            </address>
                                            </div>
                                            <div class="contact-number-wrap">
                                                <span class="number-wrap"> <?php echo $clubs->club_phone;?> </span>
                                                <span class="fax-no-wrap">1 2 9602 9389 </span>
                                            </div>
                                            <div class="email-wrap"><a href="sendto:"><?php echo $clubs->club_website;?></a></div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    <!--end of club result-->

                     <?php echo $this->pagination->create_links(); ?>
                        