<div class="notification" style="display: <?php echo ((!isset($_GET['club_name'])) || (!isset($_GET['postcode'])) )? 'block': 'none' ;?>">
       
            <div class="club-result-wrap add">

                <strong class="club-result">Notifications <span class="result-num">(<?php echo $total;?>)</span></strong>
                
                <div class="search-result-inner-holder">
                    <form role="form" id="deletenotification" method="POST" action="<?php echo site_url();?>cms/deletenotification">
                    <div class="choices-wrap">
                        <ul class="choices-list">
                            <li><a data-link="<?php echo site_url();?>cms/mark_as_read" id="mark_as_read" style="cursor:pointer">Mark as unread</a></li>
                            <li><a href="javascript:viod();">Show most recent</a></li>
                            <!-- <li><a href="#">More</a></li> -->
                        </ul>
                        
                            <button type="submit" style="float: right"><span class="recycle-block"><a>delete</a></span></button>
                    </div>
                    <div class="notification-wrapper">
                        <div id="scrollbar1">
                            <div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
                            <div class="viewport">
                                <div class="overview">
                                    <input type="hidden" value="<?php echo $user_id;?>" name="generaluserid" id="userid">
                                    <ul class="notification-list">


                                        <?php if (!empty($get_all_clubs_desc)): ?>
                                            <?php foreach ($get_all_clubs_desc as $clubs): ?>
                                                <li>
                                                    <?php if($clubs->unread != "1"):?>
                                                        <input type="checkbox" id="<?php echo $clubs->id ?>" name="notification[]" value="<?php echo $clubs->id ?>">
                                                    <?php else :?>
                                                        <input type="checkbox" id="<?php echo $clubs->id ?>" name="notification[]" value="<?php echo $clubs->id ?>" checked >
                                                    <?php endif;?>
                                                    <label for="<?php echo $clubs->id ?>">
                                                        <span></span>
                                                        <strong class="company-logo">
                                                            <a href="<?php echo site_url('offers/detail/offers/'  . safe_b64encode($clubs->offer_id)).'/'.strtolower(url_title($clubs->offer_title,'-','TRUE')) ?>">

                                                                <?php if(file_exists('assets/uploads/offer_image/'.$clubs->offer_image) and $clubs->offer_image!=null): ?>
                                                                <img  src="<?php echo base_url('assets/uploads/offer_image/'.$clubs->offer_image); ?>"  alt="logo" class="img-responsive"/>
                                                                    <?php else: ?>
                                                                    <img src="<?php echo get_asset('assets/frontend/images/logo.png'); ?>" alt="logo" class="img-responsive"/> 
                                                                <?php endif; ?>
                                                            </a>
                                                        </strong>
                                                        <strong class="company-detail-block">
                                                            <strong class="company-detail-holder">
                                                                <strong class="company-name">
                                                                    <a href="<?php echo site_url('offers/detail/offers/'  . safe_b64encode($clubs->offer_id)).'/'.strtolower(url_title($clubs->offer_title,'-','TRUE')) ?>">
                                                                        <?php echo $clubs->offer_title;?>
                                                                    </a>
                                                                </strong>

                                                                <?php if($clubs->unread == "1"):?>
                                                                <strong class="detail-info" style="color: #5D5252;"><p><?php echo $clubs->notification ?></strong></p>
                                                                <?php else: ?>
                                                                    <strong class="detail-info"><p><?php echo $clubs->notification ?></p></strong>
                                                                <?php endif?>
                                                            </strong>

                                                            <?php if($clubs->unread == "1"):?>
                                                            <strong class="contact-company mail"><a href="#">email</a></strong>

                                                            <?php else: ?>
                                                                <strong class="contact-company print"><a href="#">email</a></strong>
                                                            <?php endif?>
                                                        </strong>
                                                    </label>
                                                </li>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                  
                                   
                                </div>

                            </div>
                        </div>
                    </div>
                      </form>
                    <?php echo $this->pagination->create_links(); ?>
                    

                </div>
            </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#mark_as_read").click(function(){
            var favorite = [];
            var id = $('#userid').val();
            $. each($("input[name='notification[]']:checked"), function(){
                favorite. push($(this). val());
            });


            //console.log(favorite); return false;
            
                $.ajax({
                url:  $(this).attr('data-link'),
                type: 'POST',
                dataType:'json',
                data:{'markid': favorite,
                    'id': id
                }

            })
        });
    });
</script>