
<div class="container member"> 
    <div class="sub-page-banner-wrap"><img src="<?php echo $info->cms_header_image ? 'assets/uploads/cms/' . $info->cms_header_image : ''; ?>"  alt="banner"></div>
    <div class="breadcrumb-wrap">
        <ul class="breadcrumb-list">
            <li><a href="<?php echo site_url();?>">home</a></li>
            <li>Information For Members</li>

        </ul>
    </div>
    <div class="information-wrapper">
        <div class="video-wrapper">
            <strong class="title"><?php echo $info->cms_title;?></strong>
            <?php if(!($userLoggedIn)): ?>
            <div class="video-holder">
                <div class="wowload">
                    <?php echo $info->cms_second_block;?>
                </div>
            </div>
        <?php endif; ?>
           <!--  <span class="highlight">Membership at leading local clubs now include Rewards Club local discounts at no extra cost.</span> -->
           
                <?php 
                echo $info->cms_content;
                    // $text = str_ireplace("<p>",'',$info->cms_content);
                    // $text = str_ireplace("</p>", "", $text);
                    // echo $text;
                ?>
            
           <!--  <span class="highlight">Should you have any issues using your card or would like to suggest new businesses you can call us on 1300 305 690 or email us.</span> -->
            <div class="contact-wrap"><a href="<?php echo base_url('contacts');?>">Contact Us</a></div>
        </div>
    </div>
</div>

<?php

//if (isset($userLoggedIn) and $userLoggedIn):
//
//
//    $userDetail1 = $userDetail->club_id;
//    echo $userDetail1;
//    ?>
<!---->
<!--    --><?php //if($userDetail1 == 0): ?>
<!---->
<!--    <p> tryhrt</p>-->



<?php if(!($userLoggedIn) || ($userLoggedIn && $userDetail->club_id != "0") || isset($_GET['club_name']) || isset($_GET['suburb'])): ?>

<div class="joining-local-club-wrap">
    <div class="container">
        <h3>Join Your Local Club</h3>
        <div class="search-result-wrapper" id="form-anchor">
            <form role="form" method="GET" action="#form-anchor">
                <div class="club-search-wrap">
                    <h4>Search Club's</h4>
                    <div class="search-inner-holder">
                        <strong class="title">Find club's in your area</strong>
                        <div class="input-wrapper">
                           <input type="text" placeholder="Enter club's name" name="club_name" id="clubname" value="<?php echo (isset($_GET['club_name']) ? ($_GET['club_name']) : '' ) ;?>">

                        
                        </div>
                        <div class="input-wrapper">

                           <!--   <input type="text" class="postcode" value="<?php echo $this->input->get('postcode') ?>" name="postcode"  placeholder="Enter Suburb or postcode" id="club_postcode"> -->
                           <input type="text" class="postcode" value="<?php echo $this->input->get('postcode') ?>" name="postcode"  placeholder="Enter Suburb or postcode" id="mysuburbpostcode">
                        </div>
                         <button type="submit">Search</button>
                    </div>
                </div>
            </form>

                <div class="club-result-wrap">
                    <div class="message_main"></div>
                    <strong class="club-result">Club Results: <span class="result-num"><?php echo count($get_clubs);?></span></strong>
                    <div class="search-option-wrap">
                        <form id="test" action="" method="get">
                        <div class="option-wrap">
                            <label for="filter">Filter by</label>

                            <select name="filterby" class="newcustom" id="filter">
                                <option value="club_name" <?php echo ( isset($_GET['filterby']) && $_GET['filterby'] == 'club_name' ? 'selected' : '') ;?>>Name</option>
                                <option value="club_address" <?php echo (isset($_GET['filterby']) && $_GET['filterby'] == 'club_address' ? 'selected' : '') ;?>>Address</option>
                                <option value="club_suburb" <?php echo ( isset($_GET['filterby']) && $_GET['filterby'] == 'club_suburb' ? 'selected' : '') ;?>>Suburb</option>
                            </select>
                             <!-- <input type="submit" value="Submit" id="submit" style="display: none;" /> -->


                        </div>

                        <div class="option-wrap">
                            <label for="sort">Sort by</label>
                            <select name="alpha" class="newcustom" id="sort">
                                <option value="alphabetical">Alphabetical</option>
                                <option value="nearest">Nearest</option>
                            </select>
                            <!-- <noscript><input type="submit" value="Submit"/></noscript> -->
                        </div>
                       </form>
                    </div>
                    <div class="search-result-inner-holder" id="test">
                    <!--start of club result-->
                        <ul class="search-result-list">
                           <?php foreach ($get_clubs as $clubs): ?>
                            <li>
                                <div class="search-result-block">
                                    <div class="image-wrap"><a href="<?php echo site_url('cms/club_detail/'  . safe_b64encode($clubs->club_id)).'/'.strtolower(url_title($clubs->club_name,'-','TRUE')) ?>">
                                    <?php if(file_exists($clubs->club_logo) and $clubs->club_logo!=null): ?><img  src="<?php echo $clubs->club_logo; ?>"  alt="logo" class="img-responsive"/><?php else: ?><img src="<?php echo get_asset('assets/frontend/images/logo.png'); ?>"  alt="holiday crown"><?php endif; ?></a></div>
                                    <div class="result-detail-wrap">
                                        <div class="result-detail-heading">
                                            <div class="result-title-wrap">
                                                <strong class="title"><a href="<?php echo site_url('cms/club_detail/'  . safe_b64encode($clubs->club_id)).'/'.strtolower(url_title($clubs->club_name,'-','TRUE')) ?>"><?php echo substr($clubs->club_name, 0,25);?></a></strong>
                                                <strong class="sub-title"><?php echo $clubs->first_name.' '.$clubs->last_name;?></strong>
                                            </div>
                                            <span class="search-option">
                                                <?php if($userLoggedIn){
                                                    //echo "<pre>"; print_r($member_list); echo "</pre>";
                                                    if(in_array($clubs->club_id,array_column($member_list,'club_id')) && in_array($member_id,array_column($member_list,'member_id'))){
                                                        ?>
                                                    <a class="link_status cursorhover">Joined</a>    
                                                    <?php 
                                                    }else{
                                                    ?>
                                                    <a data-link="<?php echo base_url("cms/join_club/" . $clubs->club_id . "/" . $user_id); ?>" class="link_status cursorhover join_club">Join Club</a>

                                                <?php } }else{?>
                                                <a href="<?php echo site_url('cms/club_detail/'  . safe_b64encode($clubs->club_id)).'/'.strtolower(url_title($clubs->club_name,'-','TRUE')) ?>">Detail</a>
                                                <?php } ?>

                                            </span>
                                        </div>
                                        <div class="result-contact-info-wrap">
                                            <div class="address"><address>
                                            <?php echo ucfirst(substr($clubs->club_address,0,12)) ;?>
                                            <?php echo (!empty($clubs->club_suburb)) ? ', ' . $clubs->club_suburb : '';?>
                                            <?php echo $clubs->club_state ;?>
                                            <?php echo $clubs->club_postcode ;?>
                                            <!-- 185 George Street Liverpool NSW 2170 -->
                                            <?php
                                                $url = $clubs->club_website;
                                                if (!preg_match("~^(?:f|ht)tps?://~i", $clubs->club_website)) {
                                                    $url = "http://" . $clubs->club_website;
                                                }
                                            ?>
                                            </address>
                                            </div>
                                            <div class="contact-number-wrap">
                                                <span class="number-wrap"> <?php echo $clubs->club_phone;?> </span>
                                                <span class="fax-no-wrap"><?php echo $clubs->club_phone;?></span>
                                            </div>
                                            <div class="email-wrap">
                                                <a href="<?php echo $url;?>" target="_blank">
                                                    <?php echo str_replace("http://", "", $clubs->club_website);?>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endforeach; ?>
                        </ul>
                    <!--end of club result-->

                     <?php echo $this->pagination->create_links(); ?>
                        <div class="pagination-wrap">
                        </div>
                        
                    </div>
                </div>



            </form>
        </div>
    </div>
</div>
<?php else:?>
    <div class="joining-local-club-wrap add">
        <div class="container">
            <h3></h3>
            <div class="search-result-wrapper add">
                <form role="form" method="GET" action="#form-anchor">
                    <div class="club-search-wrap">
                        <h4>Search Club's</h4>
                        <div class="search-inner-holder">
                            <strong class="title">Find club's in your area</strong>
                            <div class="input-wrapper">
                                <input type="text" placeholder="Enter club's name" name="club_name" id="clubname" value="<?php echo (isset($_GET['club_name']) ? ($_GET['club_name']) : '' ) ;?>">
                            </div>
                            <div class="input-wrapper">

                                <!--   <input type="text" class="postcode" value="<?php echo $this->input->get('postcode') ?>" name="postcode"  placeholder="Enter Suburb or postcode" id="club_postcode"> -->
                                <input type="text" class="postcode" value="<?php echo $this->input->get('postcode') ?>" name="postcode"  placeholder="Enter Suburb or postcode" id="suburbpostcode">
                            </div>
                            <button type="submit">Search</button>
                        </div>
                    </div>
                </form>

                <?php $this->load->view('notification'); ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="inner-slider-wrapper add01">
    <div id="inner-slider" class="owl-carousel">
        <?php $this->load->view('deals/frontend/bottom_slider'); ?>
    </div>
</div>


<script type="text/javascript">
      var _all_postcode_list = <?php echo json_encode($postcode); ?>;
        $(document).ready(function() {
            $(".join_club").click(function(){
                //console.log('jere'); 
                $(this).html('Joined');
                 $.ajax({
                    url:  $(this).attr('data-link'),
                    type: 'GET',
                    dataType:'json',
                    success: function (data) {
                        console.log(data);
                        $('.message_main').html('You are joined on related club');
                        $('html,body').animate({
                            scrollTop: $(".message_main").offset().top},
                            'slow');

                    }
                });
             });

        });
    </script>
        <script src="<?php echo base_url('assets/lib/js_loader/script.min.js')?>" ></script>
        <script src="<?php echo base_url('assets/js/autocomplete.js')?>" ></script>



        <script type="text/javascript">
        $(function() {
          // select.custom (custom named classed used with selectbox (dropdown)
            $("select.newcustom").each(function() {
                 var sb = new SelectBox({
                 selectbox: $(this),
                 height: 150,
                 width:208,
                 changeCallback: function() {
                        $("#test").submit();
                  }
                });
            });

          });
          // $('select.custom').change(function(){
             //      alert("hello");
             //    })
        </script>




<script type="text/javascript">
    $(document).ready(function(){
        var current_url = window.location.href.split('?');
        var newUrl = current_url[1];
        if(newUrl){
            $('html,body').animate({
            scrollTop: $(".club-search-wrap").offset().top},
            'slow');
        }

    })

</script>




<script>
        $(function () {
            //$(":input").inputmask();
            //$('.demo2').colorpicker({});

            // add autocomplete for suburb field
            new google.maps.places.Autocomplete(
                    document.getElementById('mysuburbpostcode'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>





