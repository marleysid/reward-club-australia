<html>
<body style="padding:0; margin:0; font-family:Helvetica, sans-serif;">

<table style="width:100%; max-width:600px; margin:0 auto;" width="600" cellpadding="0" cellspacing="0">

    <tr> <!-- header starts -->
        <td>
            <table style="height:60px; background:#323d45; padding:15px 40px;" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="color:#fff; font-size:16px; font-weight:700;">Rewards Club</td>
                    <td style="color:#fff; font-size:16px; font-weight:700; text-align:right;"> <?php  if(isset($info)): ?>  <?php echo $info; ?><?php endif; ?></td>
                </tr>
            </table>
        </td>
    </tr> <!-- header ends -->
    <tr> <!-- body container starts -->
        <td style="background:#fff; padding:20px;">
            <table style="background:#f7f7f7; margin:0 auto; padding:10px;" cellpadding="0" cellspacing="0" width="100%">

                <tr><td style="padding:30px 0 0 50px;"><h1 style="color:#323d45; font-size:20px; font-weight:700;">Hi Rewards Team,</h1></td></tr>
                <tr>
                    <td>
                        <p style="font-size:14px; color:#323d45; padding:0 40px 0 50px; line-height:25px;">
                            Please check Admin for correct details.
                        <ul>
                           <?php  if(isset($name)): ?>     <li>Name:      <?php echo $name ?>    </li> <?php endif; ?>
                            <?php  if(isset($address)): ?>   <li>Address:   <?php echo  $address ?>  </li><?php endif; ?>
                            <?php  if(isset($email)): ?>  <li>Email:     <?php echo  $email ?> </li><?php endif; ?>
                            <?php  if(isset($enquiry)): ?>  <li>Enquery:    <?php echo $enquiry ?></li><?php endif; ?>
                            <?php  if(isset($phone)): ?>  <li>Phone:     <?php echo $phone ?></li><?php endif; ?>
                            <?php  if(isset($subject)): ?>   <li>Subject:     <?php echo $subject ?></li><?php endif; ?>
                            <?php  if(isset($msg)): ?>  <li>Message:     <?php  echo  $msg ?></li><?php endif; ?>
                            <?php  if(isset($business_name)): ?>  <li>Business Name:     <?php  echo  $business_name ?></li><?php endif; ?>
                            <?php  if(isset($business_address)): ?>  <li>Address:     <?php  echo  $business_address ?></li><?php endif; ?>
                            <?php  if(isset($business_postcode)): ?>  <li>Postcode:     <?php  echo  $business_postcode ?></li><?php endif; ?>
                            <?php  if(isset($business_contact_name)): ?>  <li>Contact Name:     <?php  echo  $business_contact_name ?></li><?php endif; ?>
                            <?php  if(isset($business_phone_number)): ?>  <li>Phone Number:     <?php  echo  $business_phone_number ?></li><?php endif; ?>
                            <?php  if(isset($business_email)): ?>  <li>Email:     <?php  echo  $business_email ?></li><?php endif; ?>
                            <?php  if(isset($business_offers)): ?>  <li>Offer:     <?php  echo  $business_offers ?></li><?php endif; ?>

                            <?php  if(isset($contact_club_name_of_club)): ?>  <li>Contact Club Name:     <?php  echo  $contact_club_name_of_club ?></li><?php endif; ?>
                            <?php  if(isset($contact_club_street_address)): ?>  <li>Address:     <?php  echo  $contact_club_street_address ?></li><?php endif; ?>
                            <?php  if(isset($contact_club_postcode)): ?>  <li>Postcode:     <?php  echo  $contact_club_postcode ?></li><?php endif; ?>
                            <?php  if(isset($contact_club_contact_name)): ?>  <li>Contact Name:     <?php  echo  $contact_club_contact_name ?></li><?php endif; ?>
                            <?php  if(isset($contact_club_phone_number)): ?>  <li>Phone Number:     <?php  echo  $contact_club_phone_number ?></li><?php endif; ?>
                            <?php  if(isset($contact_club_email)): ?>  <li>Email:     <?php  echo  $contact_club_email ?></li><?php endif; ?>


                        </ul>
                        </p>

                    </td>
                </tr>
                <tr>
                    <td style="padding:10px 0 60px 50px;">

                        <p style="font-size:14px; color:#323d45; padding:0 40px 0 0px; line-height:25px;">

                            Many Thanks <br />
                            <a href="http://rewardsclub.com.au/" style="font-size:16px; color:#f85352;">
                                The Team at Rewards CLub
                            </a> <br />


                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr> <!-- body container ends -->
    <tr> <!-- footer start -->
        <td style="background:#f7f7f7; height:60px; border-top:1px solid #dcdcdc; padding:0 20px;">
            <p style="font-size:12px; color:#757576;">
                This Information is Private and Confidential and is not to be used by any party other than Rewards Club.
            </p>
        </td>
    </tr> <!-- footer ends -->
</table>
</body>
</html>