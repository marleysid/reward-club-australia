<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'cms';
        $this->field_prefix = 'cms_';
        $this->log_user = FALSE;
    }

    function cms_detail() {
        $this->db->select('*');
        $this->db->from('cms');
        $this->db->where("club_id = '0'");
        $this->db->where("is_deleted = '0'");
        $query = $this->db->get();
        return $query->result();
    }
	function cms_club_detail($id){
	$this->db->select('*');
        $this->db->from('cms');
        $this->db->where("club_id = '$id'");
        $this->db->where("is_deleted = '0'");
        $query = $this->db->get();
        return $query->result();
	
	}
//    function facilities()
//    {
//        $this->db->select('*');
//        $this->db->from('facility');
//       $query =$this->db->get();
//        return $query->result();
//    }
//    function events()
//    {
//        $this->db->select('*');
//        $this->db->from('event');
//        $query =$this->db->get();
//        return $query->result();
//    }
//    function features()
//    {
//        $this->db->select('*');
//        $this->db->from('feature');
//        $query =$this->db->get();
//        return $query->result();
//    }
//    function club_contact()
//    {
//        $this->db->select('*');
//        $this->db->from('club_contacts');
//        $query =$this->db->get();
//        return $query->result();
//    }
//    function count_club_contact()
//    {
//        $query = $this->db->query('SELECT * FROM club_contacts');
//
//        return $query->num_rows();
//
//    }
    function editevents($user)
    {
        $this->db->select('*');
        $this->db->from('event');
        $this->db->where("id = $user");
        $query= $this->db->get();
        return $query->row();

    }

    function editfacilities($user)
    {
        $this->db->select('*');
        $this->db->from('facility');
        $this->db->where("id = $user");
        $query= $this->db->get();
        return $query->row();
    }
    function editfeatures($user)
    {
        $this->db->select('*');
        $this->db->from('feature');
        $this->db->where("id = $user");
        $query= $this->db->get();
        return $query->row();
    }
    function editcontacts($user){
        $this->db->select('*');
        $this->db->from('club_contacts');
        $this->db->where("id = $user");
        $query= $this->db->get();
        return $query->row();
    }
    function update_events($data, $user){
        $this->db->where('id', $user);
        $this->db->update('event', $data);
    }
    function update_facilities($data, $user){
        $this->db->where('id', $user);
        $this->db->update('facility', $data);
    }
    function update_features($data, $user){
        $this->db->where('id', $user);
        $this->db->update('feature', $data);
    }
        function info_slug($slug){
              $this->db->select('*');
        $this->db->from('cms');
        $this->db->where("cms_slug = '$slug'");
        $query = $this->db->get();
        if($query->num_rows() > 0){
        return $query->row();
        }
        else{
            return false;
        }
        }
        
        function get_faqs(){
         $query =    $this->db->select('*')
                              ->from('faq')
                              ->get();
         
         return $query->result();
            
        }

        
       function deleteevent($id)
       {
           $this->db->where('id', $id);
           $this->db->delete('event');
         return;
       }
    function deletefacilities($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('facility');
        return;
    }
    function deletefeatures($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('feature');
        return;
    }
    function insert_business($data)
    {
        $this->db->insert('business', $data);
    }
    function insert_contact_club($data)
    {
        $this->db->insert('contact_clubs', $data);
    }
    function insert_content($data)
    {
        $this->db->update('content', $data);
    }
    function get_content()
    {
        $this->db->select('*');
        $this->db->from('content');
        $query= $this->db->get();
        return $query->row();
    }
    function get_autocomplete($input)
    {

        $this->db->select('*');
        $this->db->like('club_name', $input, 'after');
        $this->db->limit(10);
        $query = $this->db->get('clubs');

        if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $new_row['label']=htmlentities(stripslashes($row['club_name']));
                $new_row['value']=htmlentities(stripslashes($row['club_name']));// The stripslashes() function removes backslashes added by the addslashes() function.
                $row_set[] = $new_row; //build an array
            }
            echo json_encode($row_set); //format the array into json data
        }
    }



    function get_codes($input)
    {
        $this->db->select('*');
        $this->db->like('suburb', $input,'after');       
        $this->db->limit(10);
        $query = $this->db->get('suburbs');

        if($query->num_rows() > 0){
            foreach ($query->result_array() as $row){
                $new_row['label']=htmlentities(stripslashes($row['suburb'])) . '-' .  htmlentities(stripslashes($row['postcode'])) ;
                $new_row['value']=htmlentities(stripslashes($row['suburb'])) . '-'. htmlentities(stripslashes($row['postcode']));// The stripslashes() function removes backslashes added by the addslashes() function.
                $row_set[] = $new_row; //build an array
            }
            echo json_encode($row_set); //format the array into json data
        }
    }
    function editcms($user)
    {
        $this->db->select('*');
        $this->db->from('cms');
        $this->db->where("cms_id = $user");
        $query= $this->db->get();
        return $query->row();
    }

    function usernotification($data)
    {
        $this->db->insert('user_notification',$data);
    }
    function rewards_club_email()
    {
        $this->db->select('value');
        $this->db->from('settings');
        $this->db->where('key', 'rewards_club_email');
        $query= $this->db->get();
       return $query->row();
    }
}
