<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cms_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['cms'] = $this->cms_model->cms_detail();

        Template::render('admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_cms(0);
        }
        $this->data['edit'] = FALSE;

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);

        Template::render('admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('admin/cms');

        $this->data['cms_detail'] = $this->cms_model->get($id);
        //echo $this->db->last_query();die;
        $this->data['cms_detail'] || redirect('admin/cms/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_cms($id);
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);

        Template::render('admin/form', $this->data);
    }

    function _add_edit_cms($id) {
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;

        $data = $this->input->post();
        $this->form_validation->set_rules(
                array(
                    array('field' => 'cms_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'cms_content', 'label' => 'Content', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }
        $cms_image = NULL;
        if (isset($_FILES['cms_image']['name']) and $_FILES['cms_image']['name'] != '') {
            if ($result = upload_image('cms_image', config_item('club_image_root'), false)) {

                $cms_image = config_item('club_image_path') . $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return false;
            }
        } elseif ($id == 0) {

            $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
            return false;
        }

        unset($data['cms_image']);
        !$cms_image || ($data['cms_image'] = $cms_image);

        $cms_image_path = NULL;
        if (isset($_FILES['cms_header_image']['name']) and $_FILES['cms_header_image']['name'] != '') {

            if ($result = upload_image('cms_header_image', config_item('cms_image_root'), NULL)) {
                $image_path = config_item('cms_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);

                $cms_image_path = $result;
            } else {
//                 echo 'test';die;
                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }


        unset($data['cms_header_image']);

        !$cms_image_path || ($data['cms_header_image'] = $cms_image_path);


        if ($id == 0) {
            $this->cms_model->insert($data);
             $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_logo = NULL;
            $old_image = NULL;
            if ($cms_image_path) {
                $old_logo = $this->cms_model->get($id)->cms_header_image;
            }
            if ($cms_image) {
                $old_image = $this->cms_model->get($id)->cms_image;
            }
            $this->cms_model->update($id, $data);
            if ($old_logo and file_exists(config_item('cms_image_root') . $old_logo))
                unlink_file(config_item('cms_image_root') . $old_logo);

            if ($old_image and file_exists(config_item('cms_image') . $old_logo))
                unlink_file(config_item('cms_image') . $old_image);
            $this->cms_model->update($id, $data);

            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/cms/', 'refresh');
    }
    //Inforation for member cms
    public function information_for_members()
    {
        $this->data['get_content'] = $this->cms_model->get_content();

        Template::render('admin/information-for-members', $this->data);
    }

    public function information_for_suppliers()
    {
        $this->data['get_content'] = $this->cms_model->get_content();

        Template::render('admin/information-for-suppliers', $this->data);
    }



    public function content()
    {
//        echo "<pre>";
//        print_r($this->input->post());
//        die();
        $data = array(
            'heading' => $this->input->post('heading'),
            'subheading' => $this->input->post('subheading'),
            'description' => $this->input->post('description')

        );

        $this->cms_model->insert_content($data);
        redirect('admin/cms/', 'refresh');
    }

}
