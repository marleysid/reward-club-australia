<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Club_admin extends Club_Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('cms_model');
        $this->data['logo_error'] = '';
    }

//    public function _remap($method, $params) {
//        if (method_exists($this, $method)) {
//            return call_user_func_array(array($this, $method), $params);
//        } else
//            return $this->index($method);
//    }

    public function index() {
       // echo 'test';die;
		 $user = $this->ion_auth->user()->row(); 
        $this->data['cms'] = $this->cms_model->cms_club_detail($user->club_id);

        Template::render('club_admin/index', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('club_admin/cms');

        $this->data['cms_detail'] = $this->cms_model->get($id);
        //echo $this->db->last_query();die;
        $this->data['cms_detail'] || redirect('club_admin/cms/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->edit_cms($id);
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);

        Template::render('club_admin/form', $this->data);
    }

    function edit_cms($id) {
      $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
      
        $data = $this->input->post();
		 $user = $this->ion_auth->user()->row(); 
        $data['club_id'] = $user->club_id;
        
        $this->form_validation->set_rules(
                array(
                    array('field' => 'cms_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'cms_content', 'label' => 'Content', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }

        $this->cms_model->update($id, $data);

        $this->session->set_flashdata('success_message', 'Data updated successfully.');


        redirect('club_admin/cms/', 'refresh');
    }

}
