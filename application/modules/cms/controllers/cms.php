<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CMS extends Front_Controller {

    // public $data;

    function __construct() {
        parent::__construct();
        $this->load->model(array('cms_model', 'deals/deal_model', 'clubs/club_model', 'offers/offer_model', 'partners/partners_model', 'advertisement/advertisement_model', 'news/news_model', 'promotional_material/material_model','reviews/review_model','settings/settings_model','community/community_model','suppliers_icons/suppliers_icons_model','materials_enquired/enquiry_model'));
        $this->load->library(array('pagination', 'email'));
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index($slug) {

        $filterBy = (isset($_GET['filterby'])) ? $_GET['filterby'] : 'club_name' ;
        
        // echo 'test';
        // debug($this->input->post());die;
        $state = $this->input->get('state');
        $postcode = $this->input->get('postcode');
        $suburb = $this->input->get('suburb');
        $clubname = $this->input->get('club_name');
        $user_id =$this->input->get('generaluserid');
//      dd($clubname);
       // debug($clubname);die;

        $per_page = $this->input->get('per_page');
         if ($this->uri->segment(2) != '') {
            $config['per_page'] = $this->uri->segment(2);
        } else {
            $config['per_page'] = 4;
        }
       // $config['per_page'] = 2;
        $result = $this->club_model->search_clubs($config['per_page'], $per_page, $state, $postcode, $suburb, $clubname, $filterBy);
       // echo $this->db->last_query();die;
        $config['total_rows'] = $this->club_model->found_rows();
        if(!isset($_GET['club_name']) && $this->member_auth->logged_in() && !isset($_GET['filterby'])){
            
            $this->data['get_all_clubs_desc'] = $this->club_model->get_all_clubs_desc($this->member_auth->user()->id);
            $this->data['get_all_clubs_unread'] = $this->club_model->get_all_clubs_unread($this->member_auth->user()->id);

            $config['total_rows'] = count($this->club_model->get_all_clubs_desc($this->member_auth->user()->id));
            //echo "<pre>"; print_r($this->club_model->get_all_clubs_desc($this->member_auth->user()->id)); exit;
        }

//        $config['base_url'] = site_url() . "information-for-members?{$_SERVER['QUERY_STRING']}";
        $config['base_url'] = site_url() . "information-for-members?state={$state}&postcode={$postcode}&suburb={$suburb}&filterby={$filterBy}";
        $config['num_links'] = 4;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='javascript:void'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li class='next'>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li class='prev'>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li style='display:none;'>";
        $config['first_tag_close'] = "</li>";
        $config['last_tag_open'] = "<li style='display:none;'>";
        $config['last_tag_close'] = "</li>";
        $config['page_query_string'] = true;
        $config['enable_query_strings'] = true;

        $this->pagination->initialize($config);

        if ($result != false) {
            $this->data['get_clubs'] = $result;
            
        } else {
            $this->data['get_clubs'] = array();
        }

        if ($this->member_auth->logged_in()) {
            $userDetail = $this->member_auth->user();
            $this->data['member_id'] = $userDetail->id;            
            $this->db->where('member_id',$userDetail->id);
            $this->data['member_list'] = $this->db->get('club_members_connection')->result_array();

        }

        
        $this->data['club_lists'] = $this->club_model->get_all_clubs_all();
        //echo "<pre>"; print_r($)
        $this->data['count_clubs'] = $this->club_model->count_clubs();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        $this->data['get_content'] = $this->cms_model->get_content();
        
        

        $this->data['get_side_ads'] = $this->advertisement_model->get_left_side_ads();
        $this->data['get_suburb'] = $this->offer_model->suburb();
        $this->data['get_state'] = $this->offer_model->state();
        $this->data['total'] = $config['total_rows'];
        $this->data['per_page'] = $config['per_page'];
        $this->data['offset'] = $per_page;
        //$this->data['fax'] = $this->fax_model->fax_detail();
        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());
        $this->data['info'] = $this->cms_model->info_slug($slug);

        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
//        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
//        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);
        $this->template->add_title_segment('Information For Members');

        Template::render('frontend/index', $this->data);

    }
    
    function info_for_clubs() {
        $slug = $this->uri->segment(1);
        //debug($slug);die;
        if ($this->input->post()) {
            // debug($this->input->post());die;
            $this->data['first_name'] = $this->input->post('first_name');
            $this->data['last_name'] = $this->input->post('last_name');
            $this->data['email'] = $this->input->post('email');
            $this->data['contact_no'] = $this->input->post('contact_no');
            $this->data['address'] = $this->input->post('address');
            $this->data['enquiry_msg'] = $this->input->post('enquiry_msg');
            $material = $this->input->post('materials');

            $this->db->insert('promotional_enquiry', array('enquiry_first_name' => $this->data['first_name'],
                'enquiry_last_name' => $this->data['last_name'],
                'enquiry_email' => $this->data['email'],
                'enquiry_contact_no' => $this->data['contact_no'],
                'enquiry_address' => $this->data['address'],
                'enquiry_msg' => $this->data['enquiry_msg']
            ));

            $id = $this->db->insert_id();
            //echo $id;die;

            if (is_array($material)) {
                foreach ($material as $materials) {
                    $this->db->insert('promotional_material_enquiry', array('enquiry_id' => $id, 'material_id' => $materials));
                }
            }
        }
//        $this->data['countrows']= $this->cms_model->count_club_contact();
//        $this->data['facilities']= $this->cms_model->facilities();
//        $this->data['events']= $this->cms_model->events();
//        $this->data['features']= $this->cms_model->features();
//        $this->data['contacts1']= $this->cms_model->club_contact();
        if ($this->member_auth->logged_in()) {
                $this->data['member_logged_in'] = TRUE;
                $this->data["clubs_all"] = $this->club_model->get_clubs_for_members($this->member_auth->user()->id);
            }

        $this->data['info'] = $this->cms_model->info_slug($slug);
        $this->data['club_detail'] = $this->club_model->get_club_detail(4);
        $this->data['club_logo'] = $this->club_model->get_club_logo();
        $this->data['facility'] = $this->club_model->get_club_facilities(4);
        $this->data['event'] = $this->club_model->get_club_events(4);
         $this->data['contact'] = $this->club_model->get_club_contacts(4);
        $this->data['partners'] = $this->partners_model->get_partners();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        $this->data['get_bottom_ads'] = $this->advertisement_model->get_bottom_ads();
        $this->data['news_detail'] = $this->news_model->news();
        $this->data['material_detail'] = $this->material_model->material_detail();
        $this->template->add_title_segment('Information For Clubs');
        
        Template::add_js(base_url('assets/frontend/js/jquery.validate.js'), TRUE);
        Template::add_js(base_url('assets/frontend/js/additional-methods.js'), TRUE);
        Template::add_js(base_url('assets/frontend/js/enquiryform_validate.js'), TRUE);
        
        Template::render('frontend/info_for_clubs', $this->data);
    }

    public function club_detail_view($id){
       $id = safe_b64decode($id);
        $slug = $this->uri->segment(1);
        //debug($slug);die;
        if ($this->input->post()) {
            // debug($this->input->post());die;
            $this->data['first_name'] = $this->input->post('first_name');
            $this->data['last_name'] = $this->input->post('last_name');
            $this->data['email'] = $this->input->post('email');
            $this->data['contact_no'] = $this->input->post('contact_no');
            $this->data['address'] = $this->input->post('address');
            $this->data['enquiry_msg'] = $this->input->post('enquiry_msg');
            $material = $this->input->post('materials');

            $this->db->insert('promotional_enquiry', array('enquiry_first_name' => $this->data['first_name'],
                'enquiry_last_name' => $this->data['last_name'],
                'enquiry_email' => $this->data['email'],
                'enquiry_contact_no' => $this->data['contact_no'],
                'enquiry_address' => $this->data['address'],
                'enquiry_msg' => $this->data['enquiry_msg']
            ));

            $id = $this->db->insert_id();
            //echo $id;die;

            if (is_array($material)) {
                foreach ($material as $materials) {
                    $this->db->insert('promotional_material_enquiry', array('enquiry_id' => $id, 'material_id' => $materials));
                }
            }
        }
//        $this->data['countrows']= $this->cms_model->count_club_contact();
//        $this->data['facilities']= $this->cms_model->facilities();
//        $this->data['events']= $this->cms_model->events();
//        $this->data['features']= $this->cms_model->features();
//        $this->data['contacts1']= $this->cms_model->club_contact();
        if ($this->member_auth->logged_in()) {
                $this->data['member_logged_in'] = TRUE;
                $this->data["clubs_all"] = $this->club_model->get_clubs_for_members($this->member_auth->user()->id);
            } 
            
        $this->data['info'] = $this->cms_model->info_slug($slug);
        $this->data['partners'] = $this->partners_model->get_partners();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        $this->data['get_bottom_ads'] = $this->advertisement_model->get_bottom_ads();
        $this->data['news_detail'] = $this->news_model->news();
        $this->data['club_detail'] = $this->club_model->get_club_detail($id);
        $this->data['facility'] = $this->club_model->get_club_facilities($id);
        $this->data['event'] = $this->club_model->get_club_events($id);
        $this->data['contact'] = $this->club_model->get_club_contacts($id);
        $this->data['material_detail'] = $this->material_model->material_detail();
        $this->template->add_title_segment('Information For Clubs');
        
        Template::add_js(base_url('assets/frontend/js/jquery.validate.js'), TRUE);
        Template::add_js(base_url('assets/frontend/js/additional-methods.js'), TRUE);
        Template::add_js(base_url('assets/frontend/js/enquiryform_validate.js'), TRUE);
        
        Template::render('frontend/info_for_clubs', $this->data);
    }

    function info_for_suppliers() {
        $slug = $this->uri->segment(1);
        //debug($slug);die;
        if($this->input->post()){
             $this->data['first_name'] = $this->input->post('first_name');
            $this->data['last_name'] = $this->input->post('last_name');
            $this->data['email'] = $this->input->post('email');
            $this->data['contact_no'] = $this->input->post('contact_no');
            $this->data['address'] = $this->input->post('address');
            $this->data['enquiry_msg'] = $this->input->post('enquiry_msg');
            $material = $this->input->post('materials');

            $this->db->insert('promotional_enquiry',
                array('enquiry_first_name' => $this->data['first_name'],
                'enquiry_last_name' => $this->data['last_name'],
                'enquiry_email' => $this->data['email'],
                'enquiry_contact_no' => $this->data['contact_no'],
                'enquiry_address' => $this->data['address'],
                'enquiry_msg' => $this->data['enquiry_msg']
            ));

            $id = $this->db->insert_id();
            //echo $id;die;

            if (is_array($material)) {
                foreach ($material as $materials) {
                    $this->db->insert('promotional_material_enquiry', array('enquiry_id' => $id, 'material_id' => $materials));
                }
            }
        }
        $this->data['info'] = $this->cms_model->info_slug($slug);
        $this->data['partners'] = $this->partners_model->get_partners();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        $this->data['get_bottom_ads'] = $this->advertisement_model->get_bottom_ads();
        $this->data['material_detail'] = $this->material_model->material_detail();
        $this->data['community'] = $this->community_model->community_detail();
        $this->data['suppliers_ions'] = $this->suppliers_icons_model->supplier_detail();
        $this->template->add_title_segment('Information For Suppliers');
        Template::add_js(base_url('assets/frontend/js/jquery.validate.js'), TRUE);
        Template::add_js(base_url('assets/frontend/js/additional-methods.js'), TRUE);
        Template::add_js(base_url('assets/frontend/js/enquiryform_validate.js'), TRUE);
        Template::render('frontend/info_for_suppliers', $this->data);
    }
    public function mark_as_read(){
        $ids = $this->input->post('markid');
        $userid = $this->input->post('id');
        //echo "<pre>"; print_r($ids); exit;

      // $this->db->where('general_user_id', $id);
       //$this->db->delete('notification_unread');

        $this->db->where('general_user_id', $userid);
        $this->db->delete('notification_unread');
        foreach($ids as $not) {
           /* if (count($quer->row()) == "1"){
                if ($quer->row()->unread == "1"){
                    $unread = 0;
                } else {
                    $unread = 1;
                }
               
                $this->db->update('notification_unread', array(
                            'general_user_id' => $userid,
                            'notification_id' => $not,
                            'unread' => $unread
                    ));
            }*/
            $this->db->insert('notification_unread',
                array('general_user_id' => $userid,
                    'notification_id' => $not,
                    'unread' => "1"
                ));
            $this->db->where_not_in("general_user_id",$userid);
        }
        redirect('information-for-clubs');
    }
    function contact() {

//            $this->email->from('ruchitimilsina27@gmail.com', 'Ruchi Timilsina');
//            $this->email->to($this->data['email']);
//            $this->email->subject('Thank You');
//            $this->email->message('Thank you for your interest in Rewards Club');
//
//            $this->email->send();


        $slug = $this->uri->segment(1);
        //debug($slug);die;
        $this->data['info'] = $this->cms_model->info_slug($slug);
        $this->data['community'] = $this->community_model->community_detail();
        $this->data['partners'] = $this->partners_model->get_partners();
        $this->data['material_detail'] = $this->material_model->material_detail();
        $this->data['address'] = $this->settings_model->get_address();
        $this->data['latitude'] = $this->settings_model->get_latlon('rewards_club_latitude');
        $this->data['longitude'] = $this->settings_model->get_latlon('rewards_club_longitude');
        $this->data['news_detail'] = $this->news_model->news();
        $this->template->add_title_segment('Contact');
        Template::add_js(base_url('assets/frontend/js/jquery.validate.js'), TRUE);
        Template::add_js(base_url('assets/frontend/js/additional-methods.js'), TRUE);
        
        Template::render('frontend/contacts', $this->data);
    }
function addcontact()
{
    $this->data['name'] = $this->input->post('name');
    $this->data['email'] = $this->input->post('email');
    $this->data['address'] = $this->input->post('address');
    $this->data['phone'] = $this->input->post('phone');
    $this->data['enquiry'] = $this->input->post('enquiry');
    $this->data['subject'] = $this->input->post('subject');
    $this->data['msg'] = $this->input->post('msg');
    //debug( $this->data);die;
    $query= $this->db->insert('contacts', array('contacts_name' => $this->data['name'],
        'contacts_email' => $this->data['email'],
        'contacts_phone_number' => $this->data['phone'],
        'contacts_address' => $this->data['address'],
        'contacts_enquiry' => $this->data['enquiry'],
        'contacts_subject' => $this->data['subject'],
        'contacts_msg' => $this->data['msg']));

    if($query) {
        $value = $this->cms_model->rewards_club_email();
        $info = " Add Contact";
        $status = 1;
        $this->load->library('email');
//        $from = "info@rewards.com.au";
        $from = $_POST['email'];
        $message = $this->load->view('frontend/email_template',$this->data, $value, $info);
//        $message = "Hi {$this->input->post('contact_name')},
//                                hello hello ".
//
//            "";

//        $this->email->to($_POST['email'])
        $this->email->to($value->value)
            ->from($from, "Rewards")
            ->subject('welcome')
            ->message($message);

        $this->email->send();
    } else {
        $status = 0;
    }
    echo json_encode(array('status' => $status));
}
    function faq() {
        $this->data['faqs'] = $this->cms_model->get_faqs();
        $this->template->add_title_segment('FAQ');
        Template::render('frontend/faq', $this->data);
    }
    
    function privacy_policy(){
         $slug = $this->uri->segment(1);
        $this->data['policy'] = $this->cms_model->info_slug($slug);
        $this->template->add_title_segment('Privacy Policy');
        Template::render('frontend/privacy_policy',  $this->data);
    }
    
    function terms_conditions(){
         $slug = $this->uri->segment(1);
        $this->data['terms_conditions'] = $this->cms_model->info_slug($slug);
        $this->template->add_title_segment('Terms and Conditions');
        Template::render('frontend/terms_conditions',  $this->data);
    }

    function add_facility()
    {


        if($this->input->post()) {
            $this->data['condition'] = $this->input->post('condition');
            $this->data['label'] = $this->input->post('label');
            $this->data['description'] = $this->input->post('description');
            $this->data['club_user_id'] = $this->input->post('club_user_id');


            $this->db->insert('facility',
                array('condition' => $this->data['condition'],
                    'label' => $this->data['label'],
                    'description' => $this->data['description'],
                    'club_user_id' => $this->data['club_user_id']

                ));
        }
        redirect('information-for-clubs');
    }
    function add_events()
    {
        if($this->input->post()) {
            $this->data['heading'] = $this->input->post('heading');
            $this->data['description'] = $this->input->post('description');
            $this->data['club_user_id'] = $this->input->post('club_user_id');


            $this->db->insert('event',
                array('heading' => $this->data['heading'],
                    'description' => $this->data['description'],
                    'club_user_id' => $this->data['club_user_id']

                ));
        }
        redirect('information-for-clubs');
    }
    function add_features()
    {
        if($this->input->post()) {
            $this->data['condition'] = $this->input->post('condition');
            $this->data['label'] = $this->input->post('label');
            $this->data['description'] = $this->input->post('description');
            $this->data['club_user_id'] = $this->input->post('club_user_id');


            $this->db->insert('feature',
                array('condition' => $this->data['condition'],
                    'label' => $this->data['label'],
                    'description' => $this->data['description'],
                    'club_user_id' => $this->data['club_user_id']
                ));
        }
        redirect('information-for-clubs');
    }
    function add_contact()
    {
        if($this->input->post()) {
            $this->data['email1'] = $this->input->post('email1');
            $this->data['email2'] = $this->input->post('email2');
            $this->data['domain'] = $this->input->post('domain');
            $this->data['phonenumber'] = $this->input->post('phonenumber');
            $this->data['address'] = $this->input->post('address');
            $this->data['club_user_id'] = $this->input->post('club_user_id');

            $this->db->insert('club_contacts',
                array('email1' => $this->data['email1'],
                    'email2' => $this->data['email2'],
                    'domain' => $this->data['domain'],
                    'phonenumber' => $this->data['phonenumber'],
                    'address' => $this->data['address'],
                    'club_user_id' => $this->data['club_user_id']

                ));
        }
        redirect('information-for-clubs');
    }
//    function edit_facility()
//    {
//        if($this->input->post()) {
//            $this->data['condition'] = $this->input->post('condition');
//            $this->data['label'] = $this->input->post('label');
//            $this->data['description'] = $this->input->post('description');
//
//
//            $this->db->update('facility',
//                array('condition' => $this->data['condition'],
//                    'label' => $this->data['label'],
//                    'description' => $this->data['description']
//
//                ));
//            $this->db->where("id = $this->input->post('id');");
//        }
//        redirect('information-for-clubs');
//    }

    function editevent()
    {
        $user=$this->input->get("helpdiv");

        $this->data['editevents'] =$this->cms_model->editevents($user);
        echo json_encode($this->data['editevents']);
    }
    function editfacilities()
    {
        $user=$this->input->get("helpdiv");
        $this->data['editfacilities'] =$this->cms_model->editfacilities($user);
        echo json_encode($this->data['editfacilities']);
    }
    function editfeatures()
    {
        $user=$this->input->get("helpdiv");
        $this->data['editfeatures'] =$this->cms_model->editfeatures($user);
        echo json_encode($this->data['editfeatures']);
    }
    function editcontacts()
    {
        $user=$this->input->get("helpdiv");
        $this->data['editcontacts'] =$this->cms_model->editcontacts($user);
        echo json_encode($this->data['editcontacts']);
    }
    function updatecontacts()
    {
        if($this->input->post()) {
        $this->data['email1'] = $this->input->post("email1");
        $this->data['email2'] = $this->input->post("email2");
        $this->data['address'] = $this->input->post("address");
        $this->data['domain'] = $this->input->post("domain");
        $this->data['phone'] = $this->input->post("phone");

        $this->db->update('club_contacts',
            array('email1' => $this->data['email1'],
                'email2' => $this->data['email2'],
                'address' => $this->data['address'],
                'domain' => $this->data['domain'],
                'phonenumber' => $this->data['phone']

            ));
        $this->db->where("id = $this->input->post('id');");
            redirect('information-for-clubs');
    }

    }
    function editcms()
    {
        $user=$this->input->get("helpdiv");
        $this->data['editcms'] =$this->cms_model->editcms($user);
        echo json_encode($this->data['editcms']);
    }
    function updatecms()
    {
        if ($this->input->post()) {
            $this->data['cms_content'] = $this->input->post("cms_value");

            $this->db->update('cms',
                array('cms_content' => $this->data['cms_content']

                ));
            $this->db->where("id = $this->input->post('id');");
//          echo json_encode(['data' =>"1"]);
        }
    }
    function update_cmsimage()
    {
    $data = $this->input->post();
        $id = $this->input->post('id');

        $cms_image = NULL;
        if (isset($_FILES['cms_image']['name']) and $_FILES['cms_image']['name'] != '') {
            if ($result = upload_image('cms_image', config_item('club_image_root'), false)) {

                $cms_image = config_item('club_image_path') . $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return false;
            }
        } elseif ($id == 0) {

            $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
            return false;
        }

        unset($data['cms_image']);
        !$cms_image || ($data['cms_image'] = $cms_image);

        $old_logo = NULL;
        $old_image = NULL;
        $this->cms_model->update($id, $data);

//        if ($old_image and file_exists(config_item('cms_image') . $old_logo))
//            unlink_file(config_item('cms_image') . $old_image);
//        $this->cms_model->update($id, $data);

        redirect('information-for-clubs');

    }

    function update_events()
    {
        $user=$this->input->post('id');
        $clubid = $this->input->post('clubid');

        $data = array(
            'heading' => $this->input->post('heading'),
            'description' => $this->input->post('description')

        );

        $this->cms_model->update_events($data, $user);
        $this->db->insert('notification',
            array('club_id' => $clubid,
                'active' => "1",
                'event_id' => $user,
            ));
        redirect('information-for-clubs');
    }
    function update_facilities()
    {
        $user=$this->input->post('id');
        $clubid = $this->input->post('clubid');
        $data = array(
            'condition' => $this->input->post('condition'),
            'label' => $this->input->post('label'),
            'description' => $this->input->post('description')

        );

        $this->cms_model->update_facilities($data, $user);
        $this->db->insert('notification',
            array('club_id' => $clubid,
                'active' => "1",
                'facility_id'=> $user
            ));
        redirect('information-for-clubs');
    }
    function update_features()
    {
        $user=$this->input->post('id');
        $clubid = $this->input->post('clubid');
        $data = array(
            'condition' => $this->input->post('condition'),
            'label' => $this->input->post('label'),
            'description' => $this->input->post('description')

        );

        $this->cms_model->update_features($data, $user);
        $this->db->insert('notification',
            array('club_id' => $clubid,
                'active' => "1",
                'feature_id'=> $user
            ));
        redirect('information-for-clubs');

    }
    function deleteevent($id){
        $this->cms_model->deleteevent($id);
        redirect('information-for-clubs');
    }
    function deletefacilities($id)
    {
        $this->cms_model->deletefacilities($id);
        redirect('information-for-clubs');
    }
    function deletefeatures($id)
    {
        $this->cms_model->deletefeatures($id);
        redirect('information-for-clubs');
    }
    function add_business()
    {

        $data = array(
            'business_name' => $this->input->post('business_name'),
            'business_address' => $this->input->post('address'),
            'business_postcode' => $this->input->post('postcode'),
            'business_contact_name' => $this->input->post('contact_name'),
            'business_phone_number' => $this->input->post('phone_number'),
            'business_email' => $this->input->post('email'),
            'business_offers' => $this->input->post('offers')
        );

       $query= $this->cms_model->insert_business($data);

        if($query) {
            $status = 0;
        } else {
            $info = " Add business";
            $value = $this->cms_model->rewards_club_email();
            $status = 1;
            $this->load->library('email');
//        $from = "info@rewards.com.au";
            $from = $_POST['email'];
            $message = $this->load->view('frontend/email_template',$data, $value, $info);
//        $message = "Hi {$this->input->post('contact_name')},
//                                hello hello ".
//
//            "";

//        $this->email->to($_POST['email'])
            $this->email->to($value->value)
                ->from($from, "Rewards")
                ->subject('welcome')
                ->message($message);

            $this->email->send();
        }
        echo json_encode(array('status' => $status));



    }
    function add_contact_club()
    {

        $data = array(
            'contact_club_name_of_club' => $this->input->post('name_of_club'),
            'contact_club_street_address' => $this->input->post('street_address'),
            'contact_club_postcode' => $this->input->post('postcode'),
            'contact_club_contact_name' => $this->input->post('contact_name'),
            'contact_club_phone_number' => $this->input->post('contact_number'),
            'contact_club_email' => $this->input->post('email')

        );

        $query= $this->cms_model->insert_contact_club($data);

        if($query) {
            $status = 0;
        } else {
            $info = " Add contact";
            $value = $this->cms_model->rewards_club_email();
            $status = 1;
            $this->load->library('email');
//        $from = "info@rewards.com.au";
            $from = $_POST['email'];
            $message = $this->load->view('frontend/email_template',$data, $value, $info);
//        $message = "Hi {$this->input->post('contact_name')},
//                                hello hello ".
//
//            "";

//        $this->email->to($_POST['email'])
            $this->email->to($value->value)
                ->from($from, "Rewards")
                ->subject('welcome')
                ->message($message);

            $this->email->send();
        }
        echo json_encode(array('status' => $status));

    }
    function club_detail()
    {
        $id   = safe_b64decode($this->uri->segment(3));

        $this->data['detail']  = $this->club_model->get_club_detail($id);
        $this->data['events']  = $this->club_model->get_club_events($id);
        $this->data['facilities']  = $this->club_model->get_club_facilities($id);
        $this->data['features']  = $this->club_model->get_club_features($id);
        $this->data['contacts']  = $this->club_model->get_club_contacts($id);
        $this->data['all_favourites']  = $this->club_model->get_favourites();
//
  //   dd($this->data['all_favourites']);
//        dd($id);
        Template::render('frontend/club_detail', $this->data);
    }
    function club_favourite($userid, $clubid)
    {

        $data = array(
            'user_id' => $userid,
            'club_id' => $clubid,
            'status'  => "1"
        );

        $this->club_model->insert_favourite($data);
        redirect('information-for-members');

    }


    function clubs()
    {
        header('Content-Type: application/json');
        $input = strtolower($_GET['term']);
        $this->cms_model->get_autocomplete($input);
    }


    function postcodes()
    {
        header('Content-Type: application/json');
        $input = strtolower($_GET['term']);
        $this->cms_model->get_codes($input);
    }
    function deletenotification()
    {

        $notification = $this->input->post("notification");
        $userid = $this->input->post("generaluserid");

        foreach($notification as $not) {

            $this->db->insert('user_notification',
                array('general_user_id' => $userid,
                    'notification_id' => $not

                ));
        }


       redirect('information-for-members');

    }





    public function content()
    {
        dd("hello");
    }

   public function add_enquiry(){
         $firstname = $this->input->post('first_name');
            $lastname = $this->input->post('last_name');
            $email = $this->input->post('email');
            $contact_no = $this->input->post('contact_no');
            $address = $this->input->post('address');
            $enquiry_msg = $this->input->post('enquiry_msg');
            $material = $this->input->post('materials');

          if($this->enquiry_model->post_enquiry($firstname,$lastname,$email,$contact_no,$address,$enquiry_msg,$material) == TRUE){
              $id = $this->db->insert_id();
             if (is_array($material)) {
                foreach ($material as $materials) {
                    $this->db->insert('promotional_material_enquiry', array('enquiry_id' => $id, 'material_id' => $materials));
                }
            }
             $status = 1;
            echo json_encode(array('status' => $status));
          }else{
              $status = 0;
            echo json_encode(array('status' => $status));
          }

            
    }
    public function join_club($id, $userid)
    {
        
        $data = array(
                'club_id' => $id,
                'member_id' => $userid
            );
        $query = $this->db->insert('club_members_connection', $data);
        if($query){
            echo "1";
        }else{
            echo "0";
        }
        exit;
        // $notification = $this->db->select('*')
        //     ->from('clubs')
        //     ->where('generaluser_id_join', $userid)
        //     ->where('club_id', $id)
        //     ->get();

        // $row = $notification->row();

        // if ($row->club_join_status == 0)
        //     $status = 1;
        // else
        //     $status = 0;
        // $this->db->where("club_id", $id);
        // $this->db->set("club_join_status", $status);
        // $this->db->set("generaluser_id_join",$userid);
        // $this->db->update("clubs");
        // $this->session->set_flashdata('successMessage', 'Status has been changed successfully');
        // redirect('information-for-members');
    }




   /* public function clubdetail($id)
    {
        $detail = $this->club_model->get_club_detail($id);

    }*/
}
