<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Feedback_model extends MY_Model {

    public $_search_total = 0;
    public $_order_by = 0;

    public function __construct() {
        parent::__construct();
        $this->table = 'feedback';
        $this->key = 'feedback_id';
    }

}
