<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of feedback
 *
 * @author rabin
 */
class Feedback extends Front_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('feedback_model');
    }

    public function index() {
         $status = 0;
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->form_validation->set_rules(array(
            array(
                'field' => 'first_name',
                'label' => 'Name',
                'rules' => 'required|xss_clean|max_length[75]'
            ),
           
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|xss_clean|max_length[75]|valid_email'
            ),
            array(
                'field' => 'feedback',
                'label' => 'Feedback',
                'rules' => 'required|xss_clean|max_length[1000]'
            ),
        ));
        if ($this->form_validation->run() === TRUE) {
             $insertData = array(
                'feedback_name' => $this->input->post('first_name'),
                'feedback_email' => $this->input->post('email'),
                'feedback_content' => $this->input->post('feedback'),
                'feedback_status' => '1'
            );
            if ($this->feedback_model->insert($insertData)) {
                 $status = 1;
                echo json_encode(array('status'=>$status));
            } else{
                //$this->session->set_flashdata('msg', 'Error saving feedback.');
                $status = 0;
                $msg = $this->session->set_flashdata('msg', 'Error saving feedback.');
               echo json_encode(array('status'=>$status, 'msg'=> $msg));
            }
//            redirect('feedback', 'refresh');
        }
        else{
            $status = 0;
                $msg = validation_errors();
                echo json_encode(array('status'=>$status, 'msg'=> $msg));
        }
       
        $this->data = array();

    }

}
