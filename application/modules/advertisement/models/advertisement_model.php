<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advertisement_model extends MY_Model
{
	public function __construct()
	{        
        parent::__construct();
		$this->table = 'advertisement';
        $this->field_prefix = 'ad_';
		$this->log_user = FALSE;
	}
	
        function get_bottom_ads(){
            $query  = $this->db->select('*')
                               ->from('advertisement')
                               ->where("ad_location = 'bottom' and ad_status='1'")
                               ->order_by('rand()')
                               ->limit(1)
                               ->get();
            return $query->row();
        }
        
        function get_left_side_ads(){
            $query  = $this->db->select('*')
                               ->from('advertisement')
                               ->where("ad_location = 'left_side' and ad_status='1'")
                               ->order_by('rand()')
                               ->limit(1)
                               ->get();
            return $query->row();
        }
          function get_right_side_ads(){
            $query  = $this->db->select('*')
                               ->from('advertisement')
                               ->where("ad_location = 'right_side' and ad_status='1'")
                               ->order_by('rand()')
                               ->limit(1)
                               ->get();
            return $query->row();
        }
        function ads_for_detail_page(){
            $query  = $this->db->select('*')
                               ->from('advertisement')
                               ->where("ad_location = 'left_side' and ad_status='1'")
                               ->order_by('rand()')
                               ->limit(3)
                               ->get();
            return $query->result();
        }
}
