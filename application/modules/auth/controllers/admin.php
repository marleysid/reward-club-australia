<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->lang->load('auth');
        $this->load->model('clubs/club_model');
        $this->load->model('ion_auth_model');
       	$title = "Manage Users";
        Template::set('title', $title);
        Template::add_title_segment($title);
    }

    public function club() {
     //  $this->data['users'] = $this->ion_auth->users('2')->result();
       $this->data['users'] = $this->ion_auth_model->get_user_detail();
      // echo $this->db->last_query();die;
        Template::render('admin/list', $this->data);
    }
    
     function add()
    {
        if ($this->input->post()) {
            $this->_add_edit_user(0);
        }	
		
		//$this->data['user_types'] = $this->ion_auth->groups()->result();
                $this->data['club_types'] = $this->club_model->get_all();
		
        $this->data['edit'] = FALSE;
        Template::render('admin/form', $this->data);
    }
    
    function edit($id = 0)
    {
        $id = (int) $id;

        $id || redirect('admin/user');
		
		//$this->data['user_types'] = $this->ion_auth->groups()->result();
                $this->data['club_types'] = $this->club_model->get_all();
		$this->data['user_groups'] = $this->ion_auth->get_users_groups($id)->result();		
		//print_r($this->data['user_groups']);die();
        $this->data['user_detail'] = $this->ion_auth->user($id)->result();
        $this->data['user_detail'] || redirect('admin/auth/');
        $this->data['user_detail'] = $this->data['user_detail'][0];
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_user($id);
        }	

        Template::render('admin/form', $this->data);
    }
    
    function _add_edit_user($id)
    {
        $this->load->library('form_validation');

        $data = $this->input->post();
        

        $this->form_validation->set_rules(
                array(
						array('field'=>'first_name', 'label'=>'First Name', 'rules'=>'trim|required|xss_clean'),
						array('field'=>'last_name', 'label'=>'Last Name', 'rules'=>'trim|required|xss_clean'),
						array('field'=>'active', 'label'=>'Active', 'rules'=>'trim|xss_clean'),
                )
        );

        if ($id == '0')
        {
                $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|unique[users.email]|xss_clean');
                $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
                $this->form_validation->set_rules('conf_pass', 'Confirm Password', 'required|matches[password]');
        }
        else
        {
                $_POST['id'] = $id;
                $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|unique[users.email,users.id]|xss_clean');
                
                //update the password if it was posted
                if ($this->input->post('password'))
                {
                        $this->form_validation->set_rules('password', 'Password', 'trim|strip_tags|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');
                        $this->form_validation->set_rules('conf_pass', 'Confirm Password', 'matches[password]');

                        $data['user_password'] = $this->input->post('password');
                }

        }

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }
        $data['username'] = $data['email'];
       
        if ($id == 0) {
             $data['club_id'] = $data['club_type'];

            
            $this->config->set_item('email_activation', FALSE, 'ion_auth');

            if($this->ion_auth->register($data['username'], $data['password'], $data['email'], $data, array('2'))) {
                $this->session->set_flashdata('success_message', $this->ion_auth->messages());
                
            } else {
                $this->session->set_flashdata('error_message', $this->ion_auth->messages());
                
            }
            
        } else {

            $this->ion_auth->update($id, $data);
			
			// First we removed this user from all groups 
//			$this->ion_auth->remove_from_group(NULL, $id);
//			// then add him to selected group
//			$this->ion_auth->add_to_group($this->input->post('user_type') , $id );
						
            $this->session->set_flashdata('success_message', $this->ion_auth->messages());
        }

        redirect('admin/auth/club', 'refresh');
    }

    //change password
    function change_password() {
        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('old_password', $this->lang->line('change_password_validation_old_password_label'), 'required');
            $this->form_validation->set_rules('new_password', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

            if ($this->form_validation->run() == false) {
//				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
                $this->session->set_flashdata('error_message', validation_errors());
                redirect(current_url(), 'refresh');
            } else {
                $identity = $this->session->userdata($this->config->item('identity', 'ion_auth'));

                $change = $this->ion_auth->change_password($identity, $this->input->post('old_password'), $this->input->post('new_password'));

                if ($change) {
                    //if the password was successfully changed
                    $this->session->set_flashdata('success_message', $this->ion_auth->messages());
                } else {
                    $this->session->set_flashdata('error_message', $this->ion_auth->errors());
                }
                redirect(current_url(), 'refresh');
            }
        }
        $this->data['old_password'] = array(
            'name' => 'old_password',
            'id' => 'old_password',
            'type' => 'password',
            'placeholder' => lang('change_password_old_password_label'),
            'class' => 'form-control',
        );
        $this->data['new_password'] = array(
            'name' => 'new_password',
            'id' => 'new_password',
            'type' => 'password',
            'placeholder' => lang('change_password_new_password_label'),
            'class' => 'form-control',
        );
        $this->data['new_password_confirm'] = array(
            'name' => 'new_confirm',
            'id' => 'new_confirm',
            'type' => 'password',
            'placeholder' => lang('change_password_new_password_confirm_label'),
            'class' => 'form-control',
        );

        //render
        Template::render('admin/change_password', $this->data);
    }
    
        function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->load->model(array('ion_auth_model'));
            $this->ion_auth_model->update($id, array('active' => $status));
            
            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

}
