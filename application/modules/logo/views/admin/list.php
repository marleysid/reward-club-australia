<div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5>Manage Logo</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
            <thead>
                <tr>
                    <th>#</th>

                    <th>Image</th>

                    <th style="width:90px;" >Settings</th>
                </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all" >
            <?php if ($supplier): ?>

                    <tr class="odd">
                        <td>1</td>
                        <td><?php if(file_exists($supplier->logo) and $supplier->logo!=null): ?><a title="<?php echo $supplier->logo; ?>" href="<?php echo base_url($supplier->logo); ?>" class="img-popup"><img class="img-thumbnail"  src="<?php echo imager($supplier->logo,80, 80); ?>" /></a><?php endif; ?></td>

                        <td class="center">
                            <a href="<?php echo base_url('admin/logo/edit/' . $supplier->id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>


                        </td>
                    </tr>

            <?php endif; ?>
            </tbody>
        </table>

    </div>
</div>
