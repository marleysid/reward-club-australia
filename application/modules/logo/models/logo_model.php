<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logo_model extends MY_Model {

   
    public function __construct() {
        parent::__construct();
        $this->table = 'logo';
//        $this->field_prefix = 'suburbs_';
        $this->log_user = FALSE;
    }
    
    function logo_detail() {
         $this->db->select('*');
        $this->db->from('logo');

        $query = $this->db->get();
        return $query->row();
    }
     
    function delete_logo($id){
         return $query = $this->db->delete('logo',  array('id' => $id));

    }

}
