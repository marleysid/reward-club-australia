<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Enquiry_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'promotional_enquiry';
        $this->field_prefix = 'enquiry_';
        $this->log_user = FALSE;
    }

  
    function enquiry_detail(){
        $query = $this->db->select('concat_ws(" ",promotional_enquiry.enquiry_first_name,promotional_enquiry.enquiry_last_name) as enquiry_name,promotional_enquiry.enquiry_email,promotional_enquiry.enquiry_msg,group_concat(promotional_material.material_name) as material_name', FALSE)
                          ->from('promotional_enquiry')
                          ->join('promotional_material_enquiry','promotional_enquiry.enquiry_id = promotional_material_enquiry.enquiry_id','left')
                          ->join('promotional_material','promotional_material_enquiry.material_id = promotional_material.material_id','left')
                          ->group_by('promotional_enquiry.enquiry_id')
                          ->order_by('promotional_enquiry.enquiry_created_date desc')
                          ->get();
        
        return $query->result();
                
    } 
    function post_enquiry($firstname,$lastname,$email,$contact_no,$address,$enquiry_msg){
        $query =   $this->db->insert('promotional_enquiry', array('enquiry_first_name' => $firstname,
                'enquiry_last_name' => $lastname,
                'enquiry_email' => $email,
                'enquiry_contact_no' => $contact_no,
                'enquiry_address' => $address,
                'enquiry_msg' => $enquiry_msg
            ));
        return TRUE;
    }
                        
    
}