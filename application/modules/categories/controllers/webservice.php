<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Webservice_model $webservice_model
 */
class Webservice extends Web_Service_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('email');
//		$this->load->model('group_model');
    }
    
    function index()
    {
//        if ($this->input->post()) {
            
            $query = $this->db->query("CALL sp_getCategory(0)");

            $msg = array(
                'categories' => array_map(function($arg) {
                        $arg->category_icon = ($arg->category_icon and file_exists(config_item('category_image_path').$arg->category_icon)) ? base_url(config_item('category_image_path').$arg->category_icon) : "";
                        return $arg;
                    }, $query->result())
            );
            return $this->response('0001', $msg);
//        } else {
//            $this->load->view('categories', array('title' => 'Login'));
//        }
        
    }
    
    function sub_categories_form()
    {
        $this->load->view('categories', array('title' => 'Get Sub categories'));
    }
    
    function sub_categories()
    {
//        if ($this->input->post()) {
//            if ( $category_id = $this->input->post('category_id') ) {
                $category_id = $this->input->post('category_id');
                $catIdz = array_filter(explode(',', $category_id));
                if(empty($catIdz)) {
                    $query = $this->db->query("CALL sp_getCategory(0)");

                    $catIdz = array_map(function($arg) {
                                return $arg->category_id;
                            }, $query->result());
                    
                }
                
                $sub_categories = array();
                
                foreach($catIdz as $v) {
                    $query = $this->db->query("CALL sp_getCategory({$v})");
                    $this->parent_id = $v;
                    $sub_categories = array_merge($sub_categories, array_map(function($arg) {
                        $arg->parent_category_id = $this->parent_id;
                        $arg->category_icon = ($arg->category_icon and file_exists(config_item('category_image_path').$arg->category_icon)) ? base_url(config_item('category_image_path').$arg->category_icon) : "";
                        
                        return $arg;
                    }, $query->result()));
                }

                $msg = array(
                    'sub_categories' => $sub_categories
                );
                return $this->response('0001', $msg);
                
//            } else {
//                return $this->response('0000');
//            }
            
//        } else {
//            $this->load->view('categories', array('title' => 'Get Sub categories'));
//        }
        
    }
    
}