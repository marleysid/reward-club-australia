<div class="box">
    <header>
        <div class="icons"><i class="fa fa-edit"></i></div>
        <h5><?php echo $edit ? 'Edit' : 'Add'; ?> category</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data">

            <div class="form-group <?php echo form_error('category_name') ? 'has-error' : '' ?>">
                <label for="text1" class="control-label col-lg-4">Name *</label>
                <div class="col-lg-8">
                    <input type="text" id="category_name" placeholder="Name" name="category_name" class="form-control" value="<?php echo set_value("category_name", $edit ? $category_detail->category_name : ''); ?>" >
                    <?php echo form_error('category_name'); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="category_icon" class="control-label col-lg-4">Image *<br> <br> (maximum image size 13*13 or same proportional)</label>
                <div class="col-lg-8">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select Image</span> 
                            <span class="fileinput-exists">Change</span> 
                            <input type="file" name="category_icon" >
                        </span> 
                        <span class="fileinput-filename"></span> 
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                        <?php echo @$logo_error; ?>
                    </div>
                    <?php if ($edit and file_exists(config_item('category_image_path') . $category_detail->category_icon)): ?>
                        <div>
                            <br/><br/>
                            <img src="<?php echo base_url(config_item('category_image_path') . $category_detail->category_icon); ?>" style="width:80px;" alt="category image" class="img-thumbnail">
                           <!--  <button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button> -->
                            <?php echo form_error('category_icon'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
<!--            <div class="form-group">-->
<!--                <label for="text1" class="control-label col-lg-4">Parent</label>-->
<!--                <div class="col-lg-8">-->
<!--                    <select name="category_parent_id" class="form-control" id="cat-parent">-->
<!--                        <option value="0">Parent Category</option>-->
<!--                        --><?php //if (isset($category_parent)): ?>
<!---->
<!--                            --><?php //foreach ($category_parent as $row): ?>
<!---->
<!--                                <option value='--><?php //echo $row->category_id; ?><!--' --><?php //echo set_select('category_parent_id', $row->category_id, ($edit and $category_detail->category_parent_id == $row->category_id) ? TRUE : FALSE); ?>
<!--                                --><?php
////                                                if ($edit and $category_detail->category_parent_id == $row->category_id) {
////                                                    echo "selected=\"selected\" ";
////                                                };
//                                ?><!-- >-->
<!--                                    --><?php //echo $row->category_name; ?><!--</option>-->
<!---->
<!--                            --><?php //endforeach; ?>
<!--                        </select>-->
<!--                    --><?php //endif; ?>
<!---->
<!--                </div>-->
<!--            </div>-->
            <!--		  <div class="form-group">
                                    <label for="tags" class="control-label col-lg-4">Status *</label>
                                    <div class="col-lg-8">
                                            <input type="checkbox" name="category_status" class="switch switch-small"  value="1" <?php // echo set_checkbox('category_status', '1', ($edit) ? ($category_detail->category_status ? TRUE : FALSE) : TRUE);      ?> data-on-color="success" data-off-color="danger" >
            <?php // echo form_error('category_status');    ?>
                                    </div>
                              </div> /.form-group -->
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                    <?php echo $edit ? anchor('admin/categories', 'Cancel', 'class="btn btn-warning"') : ''; ?>
                </div>
            </div>
        </form>		
    </div>
</div>
<script>
    $(document).ready(function() {
        $(document).on('change', '#cat-parent', function() {
            var parent_id = $('#cat-parent').val();
//    alert(sub_cat_id);
            if (parent_id != '0') {
//    alert('test');
                $("#cat-icon").hide();
            } else {
                $("#cat-icon").show();
            }
        });
<?php if ($edit): ?>
var parent_id = $('#cat-parent').val();
if (parent_id != '0') {
//    alert('test');
                $("#cat-icon").hide();
            } else {
                $("#cat-icon").show();
            }
<?php endif; ?>
    });

</script>