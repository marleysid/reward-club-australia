<?php
class Admin extends Admin_Controller 
{
	var $data;
	
	function __construct() {
		parent::__construct();	
		
	}

	public function index() 
	{
        Template::render('admin/dashboard');
	
	}
	
	function weekly_schedule()
	{
		//send weekly schedule
		if($this->input->post('submit')) {
			$this->load->model('users/user_model');
			$this->load->model('general_model');
			if($users = $this->ion_auth->select('user_id,user_firstname,user_lastname,user_email')->where(array('user_active'=>'1','user_newsletter_subscription'=>'1'))->users(GROUP_GENERAL_USER)->result()) {
				// debug($users);die;
				$this->load->model('classes/classes_model');

				$this->load->library('email');
				$this->load->library('encrypt');
				$this->load->model('email_templates/email_template_model', '_e_template');
				$templates = $this->_e_template->find_by('template_slug','weekly_calendar');

				$from	   = Settings::get('site_email');
				$from_name = Settings::get('site_title');
				$subject = $templates->template_subject;				

				$this->data['base_url'] = base_url();
				$this->data['content'] = $templates->template_content;	

				$links = '';
				$cnt = 0;
				$home_links = $this->general_model->get_home_links();
				if(is_array($home_links) && !empty($home_links)):
					foreach($home_links as $home_link)
					{
						$cnt++;
						$links .= '<a href="'.site_url().$home_link->article_slug.'.html" style="color:#fff; text-decoration:none; font-size:12px;">'.$home_link->article_title.'</a>';
						if($cnt < count($home_links))
						{
							$links .= '&nbsp; | &nbsp;';
						}
					}
				endif;

				$this->data['footer_links'] = $links;

				foreach($users as $k=>$v) {
	//				debug($v);
					$sdate = date('Y-m-d',$strsdate = strtotime('this monday'));
					$edate = date('Y-m-d',strtotime('next sunday',$strsdate));
	//				$day = date('w')+1;
					if($classes = $this->classes_model->get_trainee_classes($v->user_id,NULL,NULL,"(class_term_start_date <= '{$edate}' AND (class_term_end_date >= '{$sdate}' || class_term_end_date='0000-00-00'))", TRUE)) {
	//					debug($classes);
						$events = array();
						$dates = array();
						$start_day = 1;
	//					date('w');
						$code = urlencode($this->encrypt->encode($v->user_id));
						$this->data['unsubscribe_link'] = $this->data['base_url']."unsubscribe?c={$code}&e={$v->user_email}";

						foreach($classes as $class) {

							$class_day = $class->class_day_id-1;
							//update the start date according with the first day
							if( $start_day <= $class_day ) {
								$diff = $class_day - $start_day;
								$class_date = strtotime("+{$diff} day", strtotime($sdate));
							} elseif( $start_day > $class_day ) {
								$diff = 7 + $class_day - $start_day;
								$class_date = strtotime("+{$diff} day", strtotime($sdate));
							} 

							if(strtotime($class->class_term_start_date) > $class_date) {
								continue;
							} elseif (strtotime($class->class_term_end_date) < $class_date && $class->class_term_end_date != '0000-00-00') {
								continue;
							}


							$dates[$class->day_order]['title'] = date('D jS',$class_date);
							$events[$class->day_order][] = $class;

						}
						ksort($dates);
						ksort($events);
						$this->data['dates'] = array_slice($dates,0,7,TRUE);
						$this->data['events'] = array_slice($events,0,7,TRUE);

						$body = $this->load->view('cron/email/weekly_calendar', $this->data, TRUE);
	//					$this->load->view('email/weekly_calendar', $this->data);
	//					$to = 'nikesh@ebpearls.com';
						$to = $v->user_email;
						$subject = $templates->template_subject;

						$this->email->set_mailtype('html');
						$this->email->from($from, $from_name);
						$this->email->to($to);
						$this->email->subject($subject);
						$this->email->message($body);
						$this->email->send();

					}

				}
				$this->session->set_flashdata('success_message','Weekly Schedule sent successfully.');
				redirect(uri_string(),'refresh');
			} else {
				$this->session->set_flashdata('error_message','No users');
				redirect(uri_string(),'refresh');
				
			}
			
		}
		
		$title = "Weekly Schedule";

        Template::set('title',$title);
        Template::add_title_segment($title);
		
		Template::set('breadcrum',array('Admin'=>'admin', $title=>'weekly_schedule'));
		
		Template::render('admin/weekly_schedule');	
		
	}
	
}
