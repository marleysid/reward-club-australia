<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Suppliers_icons_model extends MY_Model {

   
    public function __construct() {
        parent::__construct();
        $this->table = 'suppliers_icons';
//        $this->field_prefix = 'suburbs_';
        $this->log_user = FALSE;
    }
    
    function supplier_detail() {
         $this->db->select('*');
        $this->db->from('suppliers_icons');

        $query = $this->db->get();
        return $query->result();
    }
     
    function delete_suppliers($id){
         return $query = $this->db->delete('suppliers_icons',  array('id' => $id));

    }

}
