<div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5>Manage Suppliers Icons</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
            <thead>
                <tr>
                    <th>#</th>
                    <th> Description</th>
                    <th>Image</th>

                    <th style="width:90px;" >Settings</th>
                </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all" >
            <?php if ($supplier): ?>
                <?php foreach ($supplier as $key => $val): ?>
                    <tr class="odd">
                        <td><?php echo ++$key; ?></td>
                        <td><?php echo $val->short_description; ?></td>
                        <td><?php if(file_exists($val->icons) and $val->icons!=null): ?><a title="<?php echo $val->icons; ?>" href="<?php echo base_url($val->icons); ?>" class="img-popup"><img class="img-thumbnail" style="background-color: black" src="<?php echo imager($val->icons,80, 80); ?>" /></a><?php endif; ?></td>

                        <td class="center">
                            <a href="<?php echo base_url('admin/suppliers_icons/edit/' . $val->id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
                            <a href="<?php echo base_url('admin/suppliers_icons/delete/' . $val->id); ?>" class="btn btn-default btn-xs btn-round" onclick='if (!confirm("Are you sure to delete?"))
                                                            return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>

                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>

    </div>
</div>
