<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> Suppliers Icons</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
<!--            --><?php //echo dd(uri_string()); ?>
            <form class="form-horizontal" method="POST"  enctype="multipart/form-data" action="<?php echo base_url(uri_string()); ?>">

                <div class="form-group <?php echo form_error('postcode') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Short Description *</label>
                    <div class="col-lg-7">
                        <input type="text" id="short_description" placeholder="Short Description" name="short_description" class="form-control" value="<?php echo set_value("short_description", $edit ? $suppliers_detail->short_description : ''); ?>" >
                        <?php echo form_error('postcode'); ?>
                    </div>
                </div>

                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="image_suburb" class="control-label col-lg-3">Image *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="icons">
                            </span>
                            (maximum image size 80 *80 or same proportional)
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists($suppliers_detail->icons)): ?>
                            <div>
                                <img src="<?php echo imager($suppliers_detail->icons, 128, 128); ?>" alt="club image" class="img-thumbnail">
                                <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
<?php echo anchor('admin/suppliers_icons', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>


