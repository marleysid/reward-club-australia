<div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5> Members</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <div class="clearfix"></div>
        <br>
        <table class="table table-bordered table-condensed table-hover table-striped dataTable" id="rewards_tbl">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Card Number</th>
                    <th>Active</th>
                    <th>Settings</th>
                </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all">
                <?php if ($members): ?>
                    <?php foreach ($members as $key => $val): ?>
                        <tr>
                            <td><?php echo ++$key; ?></td>
                            <td><?php echo $val->first_name . ' ' . $val->last_name; ?></td>
                            <td><?php echo $val->email; ?></td>
                            <td><?php echo $val->card_no; ?></td>
        <!--				<td><span class="member-num" id="cm-<?php //echo $val->cm_id; ?>"><?php //echo $val->membership_number;  ?></span></td>
                            <td><span class="member-exp-date" id="cmed-<?php //echo $val->cm_id; ?>"><?php //echo $val->cm_expire_date?date('F Y',  strtotime($val->cm_expire_date)):'';  ?></span></td>
                            -->				<td><input type="checkbox"  <?php echo $val->active ? "checked" : ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('admin/members/toggle_status/'); ?>" data-id="<?php echo $val->id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
                            <td>
                               <a href="<?php echo base_url('admin/members/edit/'.$val->id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
                               <a href="<?php echo base_url('admin/members/delete/'.$val->id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
					     
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="updateMembershipNumberModal" tabindex="-1" role="dialog" aria-labelledby="updateMembershipNumberModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Update Membership Number</h4>
            </div>
            <div class="modal-body">
                <form id="updateMembershipNumberForm" action="<?php echo base_url('admin/members/update_membership_number'); ?>">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label for="membership_number">Membership Number</label>
                            <input type="hidden" id="cm_id" name="cm_id"/>
                            <input type="text" class="form-control" id="membership_number" name="membership_number" placeholder="Membership Number">
                            <span class="help-block" id="mn_error"></span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-4">
                            <label for="cm_expiry_date">Expiry Date</label>
                            <div data-date-format="yyyy-mm" id="dpMonths" data-date-viewmode="months"  data-date-minviewmode="months" data-date="<?php // echo date('Y-m');  ?>" id="dp3" class="input-group input-append date ">
                                <input type="text" readonly="" id="cm_expiry_date" name="cm_expiry_date"  value="" class="form-control">
                                <span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span> 
                            </div>
                                  <!--<input type="text" class="form-control" id="cm_expiry_date" name="cm_expiry_date" placeholder="Expiry Date">-->
                            <span class="help-block" id="ed_error"></span>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="submitMembershipNumberForm" >Save </button>
            </div>
        </div>
    </div>
</div>

<div class="hide" style="background-color: rgb(255, 255, 255); opacity: 0.7; width: 559.767px; height: 533.767px; position: absolute; top: 0px; left: 0px; z-index: 99999; display: block;" class="ajax_overlay"><div class="ajax_loader"></div></div>
<style>
    .datepicker {z-index: 9999}
</style>
<script>
    $(function() {
        _target_block = '';
        $(document).on('click', '.updateMembershipNumber', function() {
            $('#updateMembershipNumberForm #cm_id').val($(this).data('club_mem_id'));
            $('#updateMembershipNumberForm #membership_number').val($(this).data('mem_num'));
            $('#membership_number').parent().removeClass('has-error');
            $('#mn_error').text('');
            $('#updateMembershipNumberForm #cm_expiry_date').val($(this).data('mem_ed'));
            $('#dpMonths').data('date', $(this).data('mem_ed'));
            $('#cm_expiry_date').parent().removeClass('has-error');
            $('#ed_error').text('');
            _target_block = $(this);
            $('#updateMembershipNumberModal').modal('show');
        });
        $(document).on('click', '#submitMembershipNumberForm', function() {
            if ($.trim($('#membership_number').val()) == '') {
                $('#mn_error').text('Membership Number is required.');
                $('#membership_number').parent().addClass('has-error');
                return false;
            }
            $('#membership_number').parent().removeClass('has-error');
            $('#mn_error').text('');
            if ($.trim($('#cm_expiry_date').val()) == '') {
                $('#ed_error').text('Expiry Date is required.');
                $('#cm_expiry_date').parent().parent().addClass('has-error');
                return false;
            }
            $('#cm_expiry_date').parent().parent().removeClass('has-error');
            $('#ed_error').text('');
            $.ajax({
                data: $('#updateMembershipNumberForm').serialize(),
                type: 'POST',
                url: base_url + 'admin/members/update_membership_number',
                beforeSend: function() {
                    $('#submitMembershipNumberForm').addClass('disabled').html('<i class="fa fa-spinner fa-spin"></i> Saving...');
                },
                success: function(data) {
                    $('#submitMembershipNumberForm').removeClass('disabled').html('Save');
                    if (data.status == 'ok') {
                        _target_block.data('mem_num', data.membership_number);
                        _target_block.parents('tr:first').find('span.member-num').html(data.membership_number);
                        _target_block.data('mem_ed', $('#cm_expiry_date').val());
                        _target_block.parents('tr:first').find('span.member-exp-date').html(data.expiry_date);

                        $('#updateMembershipNumberModal').modal('hide');
                        alert(data.message);
                    } else {
                        alert(data.message);
                    }
                },
                error: function() {
                    $('#submitMembershipNumberForm').removeClass('disabled').html('Save');
                    alert('Error Occurred.');
                }
            });
        });
        $(".club-member-details").fancybox({
            type: 'ajax',
        });
        $(".club-member-payment").fancybox({
            type: 'ajax',
        });
        $('#dpMonths').datepicker();
//	$(document).on('click','.club-member-details', function() {
//		
//	});
    });
</script>