<div class="col-lg-12">
	<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>
		<h5><?php echo $edit ? 'Edit' : 'Add'; ?> Members</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data">
		  <div class="form-group <?php echo form_error('member_first_name')?'has-error':''?>">
			<label for="member_first_name" class="control-label col-lg-3">First Name </label>
			<div class="col-lg-7">
                            <input type="text"  name="member_first_name" class="form-control" value="<?php echo set_value("member_first_name", $edit ? $member_detail->first_name : ''); ?>" disabled>
				<?php echo form_error('member_first_name'); ?>
			</div>
		  </div>
                     <div class="form-group <?php echo form_error('member_last_name')?'has-error':''?>">
			<label for="member_last_name" class="control-label col-lg-3">Last Name </label>
			<div class="col-lg-7">
				<input type="text" name="member_last_name" class="form-control" value="<?php echo set_value("member_last_name", $edit ? $member_detail->last_name : ''); ?>" disabled>
				<?php echo form_error('member_last_name'); ?>
			</div>
		  </div>
                    <div class="form-group <?php echo form_error('member_email')?'has-error':''?>">
			<label for="member_email" class="control-label col-lg-3">Email </label>
			<div class="col-lg-7">
				<input type="text"   name="member_email" class="form-control" value="<?php echo set_value("member_email", $edit ? $member_detail->email : ''); ?>" disabled>
				<?php echo form_error('member_email'); ?>
			</div>
		  </div>
                     <div class="form-group <?php echo form_error('member_contact_no')?'has-error':''?>">
			<label for="member_contact_no" class="control-label col-lg-3">Contact Number </label>
			<div class="col-lg-7">
				<input type="text" name="member_contact_no" class="form-control" value="<?php echo set_value("member_contact_no", $edit ? $member_detail->contact_no : ''); ?>" disabled>
				<?php echo form_error('member_contact_no'); ?>
			</div>
		  </div>
                    <div class="form-group <?php echo form_error('member_postcode')?'has-error':''?>">
			<label for="member_postcode" class="control-label col-lg-3">PostCode </label>
			<div class="col-lg-7">
				<input type="text"  name="member_postcode" class="form-control" value="<?php echo set_value("member_postcode", $edit ? $member_detail->postcode : ''); ?>" disabled>
				<?php echo form_error('member_postcode'); ?>
			</div>
		  </div>
                    <div class="form-group <?php echo form_error('member_suburb')?'has-error':''?>">
			<label for="member_suburb" class="control-label col-lg-3">Suburb </label>
			<div class="col-lg-7">
				<input type="text"  name="member_suburb" class="form-control" value="<?php echo set_value("member_suburb", $edit ? $member_detail->suburb : ''); ?>" disabled>
				<?php echo form_error('member_suburb'); ?>
			</div>
		  </div>
                    <div class="form-group <?php echo form_error('member_address')?'has-error':''?>">
			<label for="member_address" class="control-label col-lg-3">Address </label>
			<div class="col-lg-7">
				<input type="text" name="member_address" class="form-control" value="<?php echo set_value("member_address", $edit ? $member_detail->address : ''); ?>" disabled>
				<?php echo form_error('member_address'); ?>
			</div>
		  </div>
                    <div class="form-group <?php echo form_error('member_card_number')?'has-error':''?>">
			<label for="member_card_number" class="control-label col-lg-3">Card Number </label>
			<div class="col-lg-7">
				<input type="text" name="member_card_number" class="form-control" value="<?php echo set_value("member_card_number", $edit ? $member_detail->card_no : ''); ?>">
				<?php echo form_error('member_card_number'); ?>
			</div>
		  </div>
		
		  <div class="form-group">
			  <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
				  <?php echo anchor('admin/members', 'Cancel', 'class="btn btn-warning"'); ?>
			  </div>
		  </div>
		</form>		
	</div>
</div>
</div>

