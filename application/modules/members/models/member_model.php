<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'users';

        $this->log_user = FALSE;
    }


    function member_detail(){
        $query =   $this->db->select("id ,first_name,last_name, email,card_no, active, membership_type_id")
            ->from("users")
            ->where('member_deleted != "1"  AND membership_type_id != "0"')
            ->get();

        return $query->result();
    }

    function delete_member($id){
        $result = $this->db->update('users',array('member_deleted' => '1'), array('id' => $id));
        return $result;
    }

    function current_member_detail($id){
        $query = $this->db->select("*")
            ->from("users")
            ->where("id = '$id'")
            ->get();
        return $query->row();
    }

}
