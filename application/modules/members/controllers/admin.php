<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Member_model $member_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('member_model');
        $this->load->helper('text');
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
//		$this->data['members'] = $this->member_model->get_all($club);

//        $this->load->model('clubs/club_model');
//		$this->data['clubs'] = render_select($this->club_model->get_all(), 'club_id', $club, 'style="width:225px; display:inline;" class="form-control" onchange=\' window.location.href = "'.base_url('admin/members').'/"+this.value; \' ', 'club_id', 'club_name');
        $this->data['members'] = $this->member_model->member_detail();
        
        Template::add_css(base_url() . "assets/lib/datepicker/css/datepicker.css");
        Template::add_js(base_url() . "assets/lib/datepicker/js/bootstrap-datepicker.js", TRUE);

        Template::render('admin/index', $this->data);
    }

//    function add() {
//        if ($this->input->post()) {
//            $this->_add_edit_member(0);
//        }
//
//        $this->data['edit'] = FALSE;
//
//        $this->load->model('company/company_model');
////		$this->data['companies'] = render_select($this->company_model->get_all(), 'company_id', set_value('company_id', FALSE), 'class="form-control" ', 'company_id', 'company_name');
//
//        Template::add_css(base_url() . "assets/lib/datepicker/css/datepicker.css");
//        Template::add_js(base_url() . "assets/lib/datepicker/js/bootstrap-datepicker.js", TRUE);
//
//        Template::render('admin/form', $this->data);
//    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('admin/members');

        $this->data['member_detail'] = $this->member_model->get($id);
        $this->data['member_detail'] || redirect('admin/member/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_edit_member($id);
        }

//        $this->load->model('company/company_model');
//        $this->data['companies'] = render_select($this->company_model->get_all(), 'company_id', set_value('company_id', $this->data['member_detail']->company_id), 'class="form-control" ', 'company_id', 'company_name');
//
        Template::add_css(base_url() . "assets/lib/datepicker/css/datepicker.css");
        Template::add_js(base_url() . "assets/lib/datepicker/js/bootstrap-datepicker.js", TRUE);

        Template::render('admin/form', $this->data);
    }

    function _edit_member($id) {
        
        $data = $this->input->post();

       $data['active'] = '1' ;

         
            $this->member_model->update($id, $data);
            $this->session->set_flashdata('success_message', 'Data updated successfully.');
       

        redirect('admin/members/', 'refresh');
    }


    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->member_model->update($id, array('active' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }
//
//    function delete_club_member($id) {
//        if ($this->member_model->delete_club_member($id)) {
//            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
//        } else {
//            $this->session->set_flashdata('error_message', 'Data deletion failed.');
//        }
//        redirect($_SERVER['HTTP_REFERER']? : 'admin/clubs/', 'refresh');
//    }
    
      function delete($id) {
        if ($this->member_model->delete_member($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect( 'admin/members/');
    }

}
