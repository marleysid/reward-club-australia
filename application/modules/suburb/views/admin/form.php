<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> Suburb's</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
<!--            --><?php //echo dd(uri_string()); ?>
            <form class="form-horizontal" method="POST"  enctype="multipart/form-data" action="<?php echo base_url(uri_string()); ?>">

                <div class="form-group <?php echo form_error('postcode') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Postcode *</label>
                    <div class="col-lg-7">
                        <input type="text" id="postcode" placeholder="Postcode" name="postcode" class="form-control" value="<?php echo set_value("postcode", $edit ? $suburb_detail->postcode : ''); ?>" >
                        <?php echo form_error('postcode'); ?>
                    </div>
                </div>
		
                <div class="form-group <?php echo form_error('suburb') ? 'has-error' : '' ?>">
                    <label for="faq_desc" class="control-label col-lg-3"> Suburb *</label>
                    <div class="col-lg-7">
                      <!--   <textarea type="text" id="suburb" placeholder=" suburb" name="suburb" class="form-control"><?php echo  set_value("suburb", $edit ? $suburb_detail->suburb : ''); ?></textarea> -->
                      <input type="text" name="suburb" id="suburb" placeholder="suburb" class="form-control" value="<?php echo  set_value("suburb", $edit ? $suburb_detail->suburb : ''); ?>">
                      
<?php echo form_error('suburb'); ?>
                    </div>
                </div>

                <div class="form-group <?php echo form_error('state') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">State *</label>
                    <div class="col-lg-7">
                        <input type="text" id="state" placeholder="State" name="state" class="form-control" value="<?php echo set_value("state", $edit ? $suburb_detail->state : ''); ?>" >
                        <?php echo form_error('state'); ?>
                    </div>
                </div>

                <div class="form-group <?php echo form_error('dc') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">DC *</label>
                    <div class="col-lg-7">
                        <input type="text" id="dc" placeholder="Dc" name="dc" class="form-control" value="<?php echo set_value("dc", $edit ? $suburb_detail->dc : ''); ?>" >
                        <?php echo form_error('dc'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('type') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Type *</label>
                    <div class="col-lg-7">
                        <input type="text" id="type" placeholder="Type" name="type" class="form-control" value="<?php echo set_value("type", $edit ? $suburb_detail->type : ''); ?>" >
                        <?php echo form_error('type'); ?>
                    </div>
                </div>

                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="image_suburb" class="control-label col-lg-3">Image *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image_suburb">
                            </span>
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists($suburb_detail->image_suburb)): ?>
                            <div>
                                <img src="<?php echo imager($suburb_detail->image_suburb, 128, 128); ?>" alt="club image" class="img-thumbnail">
                                <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
<!--                 <div class="form-group <?php echo form_error('lat') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Latitude *</label>
                    <div class="col-lg-7">
                        <input type="text" id="latitude" placeholder="latitude" name="lat" class="form-control" value="<?php echo set_value("lat", $edit ? $suburb_detail->lat : ''); ?>" >
                        <?php echo form_error('lat'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('lon') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Longitude *</label>
                    <div class="col-lg-7">
                        <input type="text" id="entertainment_longitude" placeholder="Longitude" name="lon" class="form-control" value="<?php echo set_value("lon", $edit ? $suburb_detail->lon : ''); ?>" >
                        <?php echo form_error('lon'); ?>
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
<?php echo anchor('admin/suburb', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>

 <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>



<script>
        $(function () {
            //$(":input").inputmask();
            //$('.demo2').colorpicker({});

            // add autocomplete for suburb field
            new google.maps.places.Autocomplete(
                    document.getElementById('suburb'),
                    {
                        'componentRestrictions': {
                            'country': 'au'
                        },
                        'types': ['(cities)']
                    });
        });
    </script>

