<div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5>Manage Suburb</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
      <div style="padding:10px;float:right">
    <a class="btn btn-info btn-xs"href="<?php echo site_url('admin/suburb/suburb_csv') ?>">Download CSV</a>
    </div><br><br>
        <table class="table table-bordered table-condensed table-hover table-striped dataTable" id="suburb_tbl">
            <thead>
                <tr>
                    <th>#</th>
                    <th> Suburb</th>
                    <th>Image</th>                    
                    <th>State </th>
                    <th>DC </th>
                    <th>Setting</th>
                </tr>
            </thead>

            <tbody>
                
            </tbody>
          
        </table>

    </div>
</div>

<script type="text/javascript">
/*$(document).ready(function() {
    $('#suburb_tbl').dataTable({
        processing: true,
        serverSide: true,
        ajax: {
            "url": '<?php echo base_url();?>admin/suburb/detail',
            "type": "POST",
        },
        columns: [
            { data: "id" },
            {data : "suburb"},
        ]
    });
});*/





 $(document).ready(function () {
        var oTable = $('#suburb_tbl').dataTable({
            "bProcessing": true,
           // "bServerSide": true,
            "sAjaxSource": '<?php echo base_url();?>admin/suburb/detail',
            //"sPaginationType": "full_numbers",
            "iDisplayStart ": 20,
            "sAjaxDataProp" : "data",
            "aoColumns":[
                    {"mDataProp": "id"},
                    {"mDataProp": "suburb"},
                    {"mData": null, "mRender": function(data, type, full){
                        if(full.image_suburb)
                            return '<img class="img-thumbnail" src="<?php echo base_url();?>'+full.image_suburb+'" style="width:280px; height:90px;"/>';
                        else
                            return '';
                    }},
                    {"mDataProp": "state"},
                    {"mDataProp": "dc"},
                    {"mData": null, "mRender": function(data, type, full){
                         return '<a href="<?php echo base_url("admin/suburb/edit/");?>/'+ full.id +'" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a> <a href="<?php echo base_url("admin/suburb/delete/");?>/'+ full.id +'" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="delete" onclick="return confirm('+'Are you sure?'+')"><i class="fa fa-times"></i> </a>';

                    }},
                ],

 
            "fnInitComplete": function () {
                oTable.fnAdjustColumnSizing();
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                $.ajax
                ({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            }
        });
    });


</script>
