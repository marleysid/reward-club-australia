<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Suburb_model extends MY_Model  {

   
    public function __construct() {
        parent::__construct();
        $this->table = 'suburbs';
//        $this->field_prefix = 'suburbs_';
        $this->log_user = FALSE;
    }
    
    function suburb_detail() {
         $this->db->select('*');
        $this->db->from('suburbs');

        $query = $this->db->get();
        return $query->result();
    }
     
    function delete_subrub($id){
         return $query = $this->db->delete('suburbs',  array('id' => $id));

    }

    function importData($data)
    {
         $this->db->insert('suburbs', $data);
    }


    function get_suburb_csv()
    {
        $this->db->select('*');
        $this->db->from('suburbs');
        $query = $this->db->get();
        return $query->result();

    }

       function new_suburb_detail() {
         $this->db->select('id, suburb, image_suburb, state, dc');
        $this->db->from('suburbs');

        $query = $this->db->get();
        return $query->result();
    }

}
