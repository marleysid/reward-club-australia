<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * @property Company_model $company_model
 * @property Review_model $review_model
 */
class Admin extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('suburb_model');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params)
    {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else {
            return $this->index($method);
        }

    }

    public function index()
    {
        $this->data['suburb'] = $this->suburb_model->suburb_detail();
//        dd($this->data['suburb']);
        Template::render('admin/index', $this->data);
    }

    public function import_csv_data()
    {

        if ($this->xls_upload()) {
            $this->load->library('my_phpexcel');
            $this->uploaded_xls_file = $this->xls_file_data['full_path'];
//          $inputFileType = 'Excel2007';
            $inputFileType = 'CSV';
//          $sheetname = 'Gyms';
            $inputFileName = $this->uploaded_xls_file;
            $objReader     = PHPExcel_IOFactory::createReader($inputFileType);

//              $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
            $objPHPExcel = $objReader->load($inputFileName);

            $worksheet = $objPHPExcel->getActiveSheet();

            $highestRow         = $worksheet->getHighestRow();
            $highestColumn      = $worksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            //get header name and informations

            $req_header = array(
                'postcode'
                , 'suburb'
                , 'state'
                , 'dc'
                , 'type'
                , 'image_suburb'
                , 'lat'
                , 'lon',
            );

            $csv_header_title     = array();
            $csv_header_title_col = array();

            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, 1);
                $val  = $cell->getValue();
                if (!empty($val)) {
                    $csv_header_title[]                     = strtolower($val);
                    $csv_header_title_col[strtolower($val)] = $col;
                }
            }

            // dd(array_diff($req_header, $csv_header_title));
            if (!empty(array_diff($req_header, $csv_header_title))) {
                $this->session->set_flashdata('error_message', 'Error csv file.');
                redirect(current_url());
                // echo 'error csv file';
                // die();
            }

            $log_file = FCPATH . 'application/logs/' . uniqid() . '.txt';
            file_put_contents($log_file, PHP_EOL . 'Import Errors: ' . PHP_EOL);

            for ($row = 2; $row <= $highestRow; ++$row) {

                $insArr = array();

                //for id
                $insArr['id'] = isset($csv_header_title_col['id']) ? trim($worksheet->getCellByColumnAndRow($csv_header_title_col['id'], $row)->getValue()) : '';

                //for title
                $insArr['postcode'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['postcode'], $row)->getValue());

                $insArr['suburb']       = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['suburb'], $row)->getValue());
                $insArr['state']        = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['state'], $row)->getValue());
                $insArr['dc']           = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['dc'], $row)->getValue());
                $insArr['type']         = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['type'], $row)->getValue());
                $insArr['image_suburb'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['image_suburb'], $row)->getValue());
                $insArr['lat']          = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['lat'], $row)->getValue());
                //for category
                $insArr['lon'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['lon'], $row)->getValue());

                $this->error_csv = array();
                $this->_update_data($insArr);

                if (!empty($this->error_csv)) {

                    $data = PHP_EOL . ' Row ' . $row . ': ' . implode(PHP_EOL . "\t", $this->error_csv);
                    file_put_contents($log_file, $data, FILE_APPEND);
                    $this->session->set_flashdata('log_data', $log_file);
                }

                $insArr          = null;
                $this->error_csv = null;
                $data            = null;
            }

            unlink($this->uploaded_xls_file);
            $this->session->set_flashdata('success_message', 'Import Successfully Done.');
            redirect(current_url());
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);

        Template::set('title', 'Import csv');
        Template::set_base_title('Import csv');
        Template::render('admin/import_csv_form', $this->data);
    }

    public function _update_data($data)
    {
        if (!empty($data)) {
            $this->suburb_model->importData($data);
        }
    }

    public function xls_upload()
    {
        $config['upload_path']   = './assets/uploads/csv';
        $config['allowed_types'] = 'csv';
        $config['max_size']      = '1024';

        $this->load->library('upload', $config);
        //print_r($_FILES);
        if (!$this->upload->do_upload()) {
            //echo $this->upload->display_errors(); exit;
            return false;
        } else {
            $this->xls_file_data = $this->upload->data();
            //debug($this->xls_file_data);
            return true;
        }
    }

    public function add()
    {
        if ($this->input->post()) {
            $this->_add_edit_suburb(0);

        }

        $this->data['edit'] = false;

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", true);
        Template::add_js(base_url() . "assets/js/autocomplete.js", true);

        Template::render('admin/form', $this->data);
    }

    public function edit($id = 0)
    {
        $id = (int) $id;

        //  $id || redirect('admin/faq');

        $this->data['suburb_detail'] = $this->suburb_model->get($id);

        $this->data['edit'] = true;

        if ($this->input->post()) {
            $this->_add_edit_suburb($id);
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", true);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", true);
        Template::add_js(base_url() . "assets/js/autocomplete.js", true);

        Template::render('admin/form', $this->data);
    }

    public function _add_edit_suburb($id)
    {

        $this->load->library('form_validation');
        $this->form_validation->CI = &$this;

        $data = $this->input->post();

        $latng = get_lat_long_generic($this->input->post('suburb'));

        $data['lat'] = $latng["lat"];
        $data['lon'] = $latng["long"];


        $this->form_validation->set_rules(
            array(
                array('field' => 'postcode', 'label' => 'Postcode', 'rules' => 'required|trim|integer|xss_clean'),
                array('field' => 'suburb', 'label' => 'suburb', 'rules' => 'required|trim|xss_clean'),
                array('field' => 'state', 'label' => 'State', 'rules' => 'required|trim|xss_clean'),
                array('field' => 'dc', 'label' => 'DC', 'rules' => 'required|trim|xss_clean'),
                array('field' => 'type', 'label' => 'Type', 'rules' => 'required|trim|xss_clean'),
             /*   array('field' => 'lat', 'label' => 'latitude', 'rules' => 'required|trim|xss_clean'),
                array('field' => 'lon', 'label' => 'longitude', 'rules' => 'required|trim|xss_clean'),*/

            )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === false) {
            return false;
        }

        $club_image_path = null;
        if (isset($_FILES['image_suburb']['name']) and $_FILES['image_suburb']['name'] != '') {
            if ($result = upload_image('image_suburb', config_item('club_image_root'), false)) {
                $image_path = config_item('club_image_path') . $result;
                $club_image_path = config_item('club_image_path') . $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return false;
            }
        } elseif ($id == 0) {

            $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
            return false;
        }

        unset($data['image_suburb']);
        !$club_image_path || ($data['image_suburb'] = $club_image_path);

        if ($id == 0) {
            $this->suburb_model->insert($data);
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_image = null;
            if ($club_image_path) {
                $old_image = $this->suburb_model->get($id)->image_suburb;
            }
            $this->suburb_model->update($id, $data);
            if ($old_image and file_exists($old_image)) {
                unlink_file($old_image);
            }

            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/suburb/', 'refresh');
    }

    public function toggle_status()
    {
        if ($this->input->is_ajax_request()) {
            $id     = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->offer_model->update($id, array('offer_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    public function delete($id)
    {
        if ($this->suburb_model->delete_subrub($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/suburb/', 'refresh');
    }

    public function suburb_csv()
    {
        $this->load->library('my_phpexcel');

        $sheet = $this->my_phpexcel->getActiveSheet();
        $this->my_phpexcel->setActiveSheetIndex(0);
        $this->my_phpexcel->getProperties()
            ->setCreator("Report")
            ->setLastModifiedBy("ADMIN")
            ->setTitle('Report')
            ->setSubject('Report')
            ->setDescription('Report')
            ->setKeywords("")
            ->setCategory("");

//        echo "<pre>";
        //        print_r($offer_arr);

        $this->my_phpexcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'id')
            ->setCellValue('B1', 'postcode')
            ->setCellValue('C1', 'suburb')
            ->setCellValue('D1', 'state')
            ->setCellValue('E1', 'dc')
            ->setCellValue('F1', 'type')
            ->setCellValue('G1', 'image_suburb')
            ->setCellValue('H1', 'lat')
            ->setCellValue('I1', 'lon');
        $offer_details = $this->suburb_model->get_suburb_csv();

        if (!empty($offer_details)) {
            $offer_arr = array();
            foreach ($offer_details as $offer_detail) {
                $offer_arr[] = array(
                    'id'           => $offer_detail->id,
                    'postcode'     => $offer_detail->postcode,
                    'suburb'       => $offer_detail->suburb,
                    'state'        => $offer_detail->state,
                    'dc'           => $offer_detail->dc,
                    'type'         => strip_tags($offer_detail->type),
                    'image_suburb' => strip_tags($offer_detail->image_suburb),
                    'lat'          => $offer_detail->lat,
                    'lon'          => $offer_detail->lon,

                );
            }

            $i = 2;
            // echo "<pre>"; print_r($offer_arr); exit;
            foreach ($offer_arr as $offer) {
                $this->my_phpexcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $i, $offer['id'])
                    ->setCellValue('B' . $i, $offer['postcode'])
                    ->setCellValue('C' . $i, $offer['suburb'])
                    ->setCellValue('D' . $i, $offer['state'])
                    ->setCellValue('E' . $i, $offer['dc'])
                    ->setCellValue('F' . $i, $offer['type'])
                    ->setCellValue('G' . $i, $offer['image_suburb'])
                    ->setCellValue('H' . $i, $offer['lat'])
                    ->setCellValue('I' . $i, $offer['lon']);
                $i++;
            }
        }

        // Redirect output to a client’s web browser (Excel5)
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");
        $objWriter = PHPExcel_IOFactory::createWriter($this->my_phpexcel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }


    public function detail()
    {
        header('Content-Type: application/json');
       $mydata = $this->suburb_model->new_suburb_detail();
        $response = array(
            'data' => $mydata
            );
        echo json_encode($response);
    }

}
