<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Community_model extends MY_Model {

   
    public function __construct() {
        parent::__construct();
        $this->table = 'community';
//        $this->field_prefix = 'suburbs_';
        $this->log_user = FALSE;
    }
    
    function community_detail() {
         $this->db->select('*');
        $this->db->from('community');

        $query = $this->db->get();
        return $query->row();
    }
     
    function delete_subrub($id){
         return $query = $this->db->delete('community',  array('id' => $id));

    }

}
