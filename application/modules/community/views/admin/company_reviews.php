<style>
	.showmore { line-height:18px; }
.showmore_content { position:relative; overflow:hidden; }           
.showmore_trigger { width:100%; height:45px; line-height:45px; cursor:pointer; }
.showmore_trigger span { display:block; }
</style>
<div class="col-lg-12"><div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Reviews</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<div><strong>Company: &nbsp;</strong> <?php print $companies; ?></div>
		<div class="clearfix"></div>
		<br>
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="reviews_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Reviewer Name</th>
				<th>Email</th>
				<th>Phone </th>
				<th>Comment </th>
				<th style="width:85px;" >Rating</th>
				<th style="width:85px;" >Status</th>
				<th style="width:85px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all">
			<?php if($reviews): ?>
				<?php foreach($reviews as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->reviewer_name; ?></td>
				<td><?php echo $val->reviewer_email; ?></td>
				<td><?php echo $val->reviewer_phone; ?></td>
				<td><p class="showmore"><?php echo $val->comment; ?></p></td>
				<td><?php echo $val->rating; ?></td>
				<td><input type="checkbox"  <?php echo $val->status ? "checked": ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('admin/company/toggle_review_status/'); ?>" data-id="<?php echo $val->rating_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
				<td class="center">
					<a href="<?php echo base_url('admin/company/delete_review/'.$val->rating_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete Review"><i class="fa fa-times"></i> </a>
				</td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>
</div>
<script>
$(function (){
//	$('.showmore').showMore({
//          speedDown: 300,
//          speedUp: 300,
//          height: '20px',
//          showText: 'Show more',
//          hideText: 'Show less'
//     });
});
</script>