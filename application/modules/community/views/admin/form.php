<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> Community's</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
<!--            --><?php //echo dd(uri_string()); ?>
            <form class="form-horizontal" method="POST"  enctype="multipart/form-data" action="<?php echo base_url(uri_string()); ?>">

                <div class="form-group <?php echo form_error('title') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="title" placeholder="Title" name="title" class="form-control" value="<?php echo set_value("title", $edit ? $community_detail->title : ''); ?>" >
                        <?php echo form_error('title'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('address') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Address *</label>
                    <div class="col-lg-7">
                        <input type="text" id="address" placeholder="Address" name="address" class="form-control" value="<?php echo set_value("address", $edit ? $community_detail->address : ''); ?>" >
                        <?php echo form_error('address'); ?>
                    </div>
                </div>
		


                <div class="form-group <?php echo form_error('pobox') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">PO BOX *</label>
                    <div class="col-lg-7">
                        <input type="text" id="pobox" placeholder="Pobox" name="pobox" class="form-control" value="<?php echo set_value("pobox", $edit ? $community_detail->pobox : ''); ?>" >
                        <?php echo form_error('pobox'); ?>
                    </div>
                </div>

                <div class="form-group <?php echo form_error('contact_number') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Contact Number *</label>
                    <div class="col-lg-7">
                        <input type="text" id="contact_number" placeholder="Contact Number" name="contact_number" class="form-control" value="<?php echo set_value("contact_number", $edit ? $community_detail->contact_number : ''); ?>" >
                        <?php echo form_error('dc'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('email') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Email *</label>
                    <div class="col-lg-7">
                        <input type="text" id="email" placeholder="Email" name="email" class="form-control" value="<?php echo set_value("email", $edit ? $community_detail->email : ''); ?>" >
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('description') ? 'has-error' : '' ?>">
                    <label for="faq_desc" class="control-label col-lg-3"> Description *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="description" placeholder="Description " name="description" class="form-control"><?php echo  set_value("description", $edit ? $community_detail->description : ''); ?></textarea>

                        <?php echo form_error('description'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('short_description') ? 'has-error' : '' ?>">
                    <label for="faq_desc" class="control-label col-lg-3"> Short Description *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="short_description" placeholder=" Short Description " name="short_description" class="form-control"><?php echo  set_value("short_description", $edit ? $community_detail->description : ''); ?></textarea>

                        <?php echo form_error('short_description'); ?>
                    </div>
                </div>
                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="logo" class="control-label col-lg-3">Image *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="logo">
                            </span>
                            (maximum image size 200*80 or same proportional)
                            <span class="fileinput-filename"></span>
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists($community_detail->logo)): ?>
                            <div>
                                <img src="<?php echo imager($community_detail->logo, 128, 128); ?>" alt="club image" class="img-thumbnail">
                                <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                            </div>
                        <?php endif; ?>
                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
<?php echo anchor('admin/community', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>


