<div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5>Manage Suburb</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
            <thead>
                <tr>

                    <th>Title</th>
                    <th> Address</th>
                    <th>PO BOX</th>
                    <th>Contact Number </th>
                    <th>Email </th>
                    <th>Logo </th>
                    <th>description </th>
                    <th>Short Description </th>
                    <th style="width:90px;" >Settings</th>
                </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all" >
            <?php if ($community): ?>

                    <tr class="odd">

                        <td><?php echo $community->title; ?></td>
                        <td><?php echo $community->address; ?></td>
                        <td><?php echo $community->pobox; ?></td>
                        <td><?php echo $community->contact_number; ?></td>
                        <td><?php echo $community->email; ?></td>
                        <td><?php if(file_exists($community->logo) and $community->logo!=null): ?><a title="<?php echo $community->logo; ?>" href="<?php echo base_url($community->logo); ?>" class="img-popup"><img class="img-thumbnail" src="<?php echo imager($community->logo,80, 80); ?>" /></a><?php endif; ?></td>
                        <td><?php echo $community->description; ?></td>
                        <td><?php echo $community->short_description; ?></td>
                        <td class="center">
                            <a href="<?php echo base_url('admin/community/edit/' . $community->id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
                            <a href="<?php echo base_url('admin/community/delete/' . $community->id); ?>" class="btn btn-default btn-xs btn-round" onclick='if (!confirm("Are you sure to delete?"))
                                                            return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>

                        </td>
                    </tr>

            <?php endif; ?>
            </tbody>
        </table>

    </div>
</div>
