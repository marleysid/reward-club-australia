<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('community_model');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['community'] = $this->community_model->community_detail();
//        dd($this->data['suburb']);
        Template::render('admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_community(0);
        }

        $this->data['edit'] = FALSE;

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);

        Template::render('admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        //  $id || redirect('admin/faq');

        $this->data['community_detail'] = $this->community_model->get($id);

        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_community($id);
        }



        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);

        Template::render('admin/form', $this->data);
    }

    function _add_edit_community($id) {

        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;

        $data = $this->input->post();
        $this->form_validation->set_rules(
                array(
                    array('field' => 'address', 'label' => 'address', 'rules' => 'required'),
                    array('field' => 'pobox', 'label' => 'pobox', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'contact_number', 'label' => 'contact_number', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'email', 'label' => 'email', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'title', 'label' => 'title', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'short_description', 'label' => 'short_description', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'description', 'label' => 'description', 'rules' => 'required|trim|xss_clean'),
                  
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }


        $club_image_path = NULL;
        if (isset($_FILES['logo']['name']) and $_FILES['logo']['name'] != '') {
            if ($result = upload_image('logo', config_item('club_image_root'), false)) {
                $image_path = config_item('club_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
//				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'));
//                }
                $club_image_path = config_item('club_image_path') . $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }elseif ($id == 0) {

            $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
            return FALSE;
        }

        unset($data['logo']);
        !$club_image_path || ($data['logo'] = $club_image_path);

        if ($id == 0) {
            $this->community_model->insert($data);
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_image = NULL;
            if ($club_image_path) {
                $old_image = $this->community_model->get($id)->image_suburb;
            }
            $this->community_model->update($id, $data);
            if ($old_image and file_exists($old_image))
                unlink_file($old_image);
            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/community/', 'refresh');

    }

   
    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->offer_model->update($id, array('offer_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    
    function delete($id) {
       if ($this->community_model->delete_subrub($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/community/', 'refresh');
    }

   
    
}
