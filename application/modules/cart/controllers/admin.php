<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * @property Reward_model $reward_model 
 */
class Admin extends Admin_Controller 
{	
	function __construct() {
		parent::__construct();	
		$this->load->model('reward_model');
		$this->load->helper('text');
		
	}
	
	public function _remap($method, $params )
	{
		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		} else 
			return $this->index($method);
		
	}

	public function index() 
	{		
		$this->data['rewards'] = $this->reward_model->get_all();
		
        Template::render('admin/index', $this->data);
	
	}
	
	function add() 
	{		
		if($this->input->post()) {
			$this->_add_edit_reward(0);
		}
		
		$this->data['edit'] = FALSE;
		
		$this->load->model('company/company_model');
		$this->data['companies'] = render_select($this->company_model->get_all(), 'company_id', set_value('company_id', FALSE), 'class="form-control" ', 'company_id', 'company_name');
		
		Template::add_css( base_url()."assets/lib/datepicker/css/datepicker.css");
		Template::add_js( base_url()."assets/lib/datepicker/js/bootstrap-datepicker.js", TRUE);		
		
		Template::render('admin/form', $this->data);
		
	}
	
	function edit($id = 0) 
	{
		$id = (int) $id;
		
		$id || redirect('admin/rewards');
		
		$this->data['rewards_detail'] = $this->reward_model->get($id);
		$this->data['rewards_detail'] || redirect('admin/reward/');
		$this->data['edit'] = TRUE;
		
		if($this->input->post()) {
			$this->_add_edit_reward($id);
		}
		
		$this->load->model('company/company_model');
		$this->data['companies'] = render_select($this->company_model->get_all(), 'company_id', set_value('company_id', $this->data['rewards_detail']->company_id), 'class="form-control" ', 'company_id', 'company_name');
		
		Template::add_css( base_url()."assets/lib/datepicker/css/datepicker.css");
		Template::add_js( base_url()."assets/lib/datepicker/js/bootstrap-datepicker.js", TRUE);	
		
		Template::render('admin/form', $this->data);
		
	}
	
	function _add_edit_reward($id) 
	{
        $this->load->library('form_validation');
		
		$data = $this->input->post();
		
        $this->form_validation->set_rules(
                    array(
                        array('field'=>'rewards_name', 'label'=>'Name', 'rules'=>'required|trim|min_length[2]|xss_clean'),
                        array('field'=>'rewards_text', 'label'=>'Text', 'rules'=>'trim'),
						array('field'=>'rewards_valid_date', 'label'=>'Valid till', 'rules'=>'trim|required|xss_clean'),
                        array('field'=>'company_id', 'label'=>'Company', 'rules'=>'trim|required|xss_clean'),
                        array('field'=>'rewards_status', 'label'=>'Status', 'rules'=>'trim|xss_clean'),
                    )
                );
		
        $this->form_validation->set_error_delimiters('<span class="help-block">','</span>');
        
		if ($this->form_validation->run($this) === FALSE)
		{
			return FALSE;
		}
		
		$data['reward_status'] = $this->input->post('reward_status') ? '1' : '0';
		
		if ($id == 0)
		{
			$this->reward_model->insert($data);
			$this->session->set_flashdata('success_message','Data inserted successfully.');
			
		}
		else
		{
			$this->reward_model->update($id, $data);
			$this->session->set_flashdata('success_message','Data updated successfully.');
						
		}
		
		redirect('admin/rewards/','refresh');
		
	}
	
	function toggle_status() 
	{
		if($this->input->is_ajax_request()) {
			$id =  $this->input->post('id');
			$status =  $this->input->post('status') == 'true' ? '1' : '0' ;
			$this->reward_model->update($id, array('rewards_status'=>$status));
			
			$this->output->set_content_type('application/json')->set_output(json_encode(array('status'=>'ok')));
		}
	}
	
	function delete($id) 
	{
		if($this->reward_model->delete($id)) {
			$this->session->set_flashdata('success_message','Data deleted successfully.');			
		} else {
			$this->session->set_flashdata('error_message','Data deletion failed.');			
		}
		redirect('admin/rewards/','refresh');
	}
	
}
