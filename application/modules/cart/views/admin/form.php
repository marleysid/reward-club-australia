<div class="col-lg-12">
	<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>
		<h5><?php echo $edit ? 'Edit' : 'Add'; ?> rewards</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data">
		  <div class="form-group <?php echo form_error('rewards_name')?'has-error':''?>">
			<label for="rewards_name" class="control-label col-lg-3">Name *</label>
			<div class="col-lg-7">
				<input type="text" id="rewards_name" placeholder="Name" name="rewards_name" class="form-control" value="<?php echo set_value("rewards_name", $edit ? $rewards_detail->rewards_name : ''); ?>" >
				<?php echo form_error('rewards_name'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('rewards_text')?'has-error':''?>">
			<label for="rewards_text" class="control-label col-lg-3">Rewards Text</label>
			<div class="col-lg-7">
				<textarea id="rewards_text" placeholder="Rewards Text" name="rewards_text" class="form-control" ><?php echo set_value("rewards_text", $edit ? $rewards_detail->rewards_text : ''); ?></textarea>
				<?php echo form_error('rewards_text'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('rewards_valid_date')?'has-error':''?>">
			<label for="rewards_valid_date" class="control-label col-lg-3">Valid till</label>
			<div class="col-lg-2">
			  <div class="input-group input-append date" id="rewards_valid_date" data-date="<?php echo set_value("rewards_valid_date", $edit ? $rewards_detail->rewards_valid_date : ''); ?>" data-date-format="yyyy-mm-dd">
				<input class="form-control" type="text" name="rewards_valid_date" value="<?php echo set_value("rewards_valid_date", $edit ? $rewards_detail->rewards_valid_date : ''); ?>" readonly>
				<span class="input-group-addon add-on"><i class="fa fa-calendar"></i></span> 
			  </div>
				<?php echo form_error('rewards_valid_date'); ?>
			</div>
		  </div>
		  <div class="form-group">
			<label for="company_id" class="control-label col-lg-3">Rewards Company *</label>
			<div class="col-lg-7">
				<?php echo $companies; ?>
				<?php echo form_error('company_id'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group">
			<label for="rewards_status" class="control-label col-lg-3">Status *</label>
			<div class="col-lg-7">
				<input type="checkbox" name="rewards_status" class="switch switch-small"  value="1" <?php echo set_checkbox('rewards_status', '1', ($edit) ? ($rewards_detail->rewards_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >
				<?php echo form_error('rewards_status'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group">
			  <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
				  <?php echo anchor('admin/rewards', 'Cancel', 'class="btn btn-warning"'); ?>
			  </div>
		  </div>
		</form>		
	</div>
</div>
</div>

<script>
$(function () {
	$('#rewards_valid_date').datepicker(
			);
});
</script>