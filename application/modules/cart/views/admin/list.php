<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage Rewards</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="rewards_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Rewards Title</th>
				<th style="width:20%" >Rewards Text</th>
				<th>Rewards Company</th>
				<th style="width:85px;" >Status</th>
				<th style="width:90px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all">
			<?php if($rewards): ?>
				<?php foreach($rewards as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->rewards_name; ?></td>
				<td><?php echo character_limiter($val->rewards_text, 20); ?></td>
				<td><?php echo $val->company_name; ?></td>
				<td class="center"><input type="checkbox"  <?php echo $val->rewards_status ? "checked": ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('admin/rewards/toggle_status/'); ?>" data-id="<?php echo $val->rewards_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
				<td class="center"><a href="<?php echo base_url('admin/rewards/edit/'.$val->rewards_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
						<a href="<?php echo base_url('admin/rewards/delete/'.$val->rewards_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
				</td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>