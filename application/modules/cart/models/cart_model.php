<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reward_model extends MY_Model
{
	public function __construct()
	{        
        parent::__construct();
		$this->table = 'rewards';
        $this->field_prefix = 'rewards_';
		$this->log_user = FALSE;
	}
	
	function get_all($return_type = 0)
	{
		$this->select("*")
				->join('rewards_company', 'rewards_company.company_id = rewards.company_id');
		return parent::get_all($return_type);

	}
        
}
