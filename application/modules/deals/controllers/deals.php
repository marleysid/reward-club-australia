<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Deals extends Front_Controller {

    // public $data;

    function __construct() {
        parent::__construct();
        $this->load->model(array('deal_model', 'advertisement/advertisement_model', 'offers/offer_model', 'reviews/review_model', 'orders/order_model'));
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    function cart_detail($id = 0) {
        if (!($this->member_auth->logged_in())) {
            redirect('/');
        } else {
            $id = $this->current_member->member_id;
            $this->data['cart_detail'] = $this->deal_model->cart_detail($id);
//            debug($this->data['cart_detail']);die;
            $this->data['total_items'] = $this->deal_model->found_rows();
//            $deals_quan = $this->deal_model->deal_quantity($this->data['cart_detail']->deal_id);
             //echo $this->db->last_query();die;
//             debug($deals_quan);die;
            
            $this->data['total_amount'] = $this->order_model->total_amount($id);
        Template::add_js(base_url('assets/frontend/js/cart_favourites.js'));
            $this->template->add_title_segment('Cart Detail');
            Template::render('frontend/cart_detail', $this->data);
        }
    }

    function remove_from_cart() {
        if (!($this->member_auth->logged_in())) {
            echo 'please login';
        } else {
            $member_id = $this->current_member->member_id;
            $deal_id = $this->uri->segment(3);
            $this->db->delete('cart_session', array('deal_id' => $deal_id, 'member_id' => $member_id));
            $quantity = $this->deal_model->deals_quantity($deal_id);
            $this->db->update('deals', array('deal_quantity' => ($quantity->deal_quantity + 1)), array('deal_id' => $deal_id));
            //echo $this->db->last_query();die;
            redirect('deals/cart_detail');
        }
    }

    function favourites() {
        if (!($this->member_auth->logged_in())) {
            redirect('/');
        } else {
            $id = $this->current_member->member_id;
            $this->data['favourites'] = $this->deal_model->favourites($id);
             $this->data['total_items'] = $this->deal_model->found_rows();
            // debug($this->data['total_items']);die;
            // echo $this->db->last_query();die;
            $this->data['side_ads'] = $this->advertisement_model->get_right_side_ads($id);
            $this->template->add_title_segment('Favourite');
            Template::add_css('assets/lib/jquery-raty/jquery.raty.css');
            Template::add_js(base_url('assets/lib/jquery-raty/jquery.raty.js'));
            Template::add_js(base_url('assets/frontend/js/cart_favourites.js'));
            Template::render('frontend/favourites', $this->data);
        }
    }
    function remove_from_favourite(){
        if (!($this->member_auth->logged_in())) {
            echo 'please login';
        } else {
            $member_id = $this->current_member->member_id;
            $id = $this->uri->segment(4);
            $fav_type = $this->uri->segment(3);
            $this->db->delete('favourite', array('favourite_type' => $fav_type,'favourite_type_id' => $id, 'member_id' => $member_id));
            //echo $this->db->last_query();die;
            redirect('deals/favourites');
            
        }
    }

    function detail() {
        $id = safe_b64decode($this->uri->segment(3));
        // echo $id;die;
        $this->data['slider_detail'] = $this->deal_model->get_bottom_slider_detail($id);
        //echo $this->db->last_query();die;
        $this->data['count_review'] = $this->review_model->total_review($id, 'deal');
        $this->data['get_ads'] = $this->advertisement_model->ads_for_detail_page();
        $this->data['category'] = $this->category_model->parent_cat();
        $this->data['regions'] = $this->region_model->get_regions();
        $this->data['selected_cat'] = $this->input->get('category');
        $this->data['selected_sub_cat'] = $this->input->get('sub_category');
        Template::add_css('assets/lib/jquery-raty/jquery.raty.css');
//        Template::add_js('assets/frontend/js/jquery.rateit.js');
        Template::add_js(base_url('assets/lib/jquery-raty/jquery.raty.js'));
        Template::add_js(base_url('assets/frontend/js/cart_favourites.js'));

        Template::render('frontend/detail', $this->data);
    }

    function quantity_change() {
         $insrtdb = $this->load->database('webservice', TRUE);
         if (!($this->member_auth->logged_in())) {
            echo 'please login';
        } else {
        $status = '';
        $quantity = (int) $this->uri->segment(3);
        $id = (int) $this->uri->segment(4);
//        $deals_quan = $this->deal_model->deals_quantity($id);
        $member_id = $this->current_member->member_id;
//        $prev_quan = $this->deal_model->check_prev_quan($id,  $member_id);
//        $diff_quan = (int)$quantity - $prev_quan->quantity;
       // echo $diff_quan;die;
        $query = $insrtdb->query("CALL sp_updateCart('$member_id','$id','$quantity')");
        
        if($query->num_rows() > 0){
            $result = $query->row();
            if($result->status == 1){
             $total_price =   $this->order_model->total_amount($member_id);
             $total_amt = $total_price->total;
//             debug($total_price);die;
//                echo $total_price;die;
                $status = 1;
                echo json_encode(array('status' => $status, 'amount' => $total_amt));
            }elseif($result->status == 3){
                $status = 3;
                echo json_encode(array('status' => $status));
            }elseif($result->status == 4){
                $status = 4;
                echo json_encode(array('status' => $status));
            }
        }
//        if ($deals_quan->deal_quantity >= 1) {
//            $this->deal_model->quantity_change($quantity, $id,$member_id);
//           // echo $this->db->last_query();die;
//            if($quantity > $prev_quan){
////            $this->db->set('deal_quantity', 'deal_quantity -1', FALSE);
////            $this->db->where('deal_id', $id);
////            $this->db->update('deals');
//              $this->db->update('deals',array('deal_quantity' => ($deals_quan->deal_quantity +$diff_quan)),array('deal_id' => $id));
//           
//            }
//            elseif($quantity < $prev_quan){
////                $this->db->set('deal_quantity', 'deal_quantity -1', FALSE);
////            $this->db->where('deal_id', $id);
////            $this->db->update('deals');
//            $this->db->update('deals',array('deal_quantity' => ($deals_quan->deal_quantity - $diff_quan)),array('deal_id' => $id));
//            }
////            echo $this->db->last_query();die;
//            
//            $status = 1;
//            echo json_encode(array('status' => $status));
//        } else {
//            $status = 0;
//            echo json_encode(array('status' => $status));
//        }
    }
    }

}
