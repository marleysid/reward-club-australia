<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Admin extends Admin_Controller {

    public $error_csv = array();

    function __construct() {
        parent::__construct();
        $this->load->model('deal_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['deals'] = $this->deal_model->get_all();

        Template::render('admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_deal(0);
        }

        $this->data['edit'] = FALSE;


        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/bootstrap-datetimepicker.css");
        Template::add_css(base_url() . "assets/css/datepicker.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datetimepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery.numeric.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery.numeric.min.js", TRUE);



        Template::render('admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('admin/deals');

        $this->data['deal_detail'] = $this->deal_model->get($id);
        $this->data['slider_detail'] = $this->deal_model->slider_detail($id);
//        echo $this->db->last_query();die;
        // debug($this->data['slider_detail']);die;

        $this->data['deal_detail'] || redirect('admin/deals/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_deal($id);
        }


        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/bootstrap-datetimepicker.css");
        Template::add_css(base_url() . "assets/css/datepicker.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datetimepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery.numeric.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery.numeric.min.js", TRUE);


        Template::render('admin/form', $this->data);
    }

    function _add_edit_deal($id) {

        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $data = $this->input->post();
        if($vDate = DateTime::createFromFormat('m/d/Y', $this->input->post('deal_offer_expire'))) {
            $data['deal_offer_expire'] = date('Y-m-d', $vDate->getTimestamp());        
            
        } else {
            $data['deal_offer_expire'] = date('Y-m-d');            

        } 
//        $data['deal_offer_expire'] = date("Y-m-d", strtotime($this->input->post('deal_offer_expire')));
        // debug($data);die;
        $this->form_validation->set_rules(
                array(
                    array('field' => 'deal_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'deal_short_title', 'label' => 'Short Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'deal_price', 'label' => 'Deal Price', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'deal_short_desc', 'label' => 'Short Description', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'deal_desc', 'label' => 'Description', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'deal_tag', 'label' => 'Tag', 'rules' => 'required|trim|min_length[5]|xss_clean'),
                    array('field' => 'deal_left_tag', 'label' => 'Left Tag', 'rules' => 'required'),
                    array('field' => 'deal_offer_expire', 'label' => 'Offer Expiry Date', 'rules' => 'required'),
                    array('field' => 'deal_timer_expire', 'label' => 'Timer Expiry Date', 'rules' => 'required'),
                    array('field' => 'deal_quantity', 'label' => 'Quantity', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'slider_bottom', 'label' => 'Bottom', 'rules' => 'trim'),
                    array('field' => 'slider_top', 'label' => 'Top', 'rules' => 'trim'),
                    array('field' => 'slider_featured', 'label' => 'Featured', 'rules' => 'trim'),
                    array('field' => 'deal_status', 'label' => 'Status', 'rules' => 'trim'),
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            //echo 'here';die;
            return FALSE;
        }

        $deal_image_path = NULL;
        if (isset($_FILES['deal_image']['name']) and $_FILES['deal_image']['name'] != '') {
            if ($result = upload_image('deal_image', config_item('deal_image_root'), FALSE)) {
                $image_path = config_item('deal_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
////				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'), TRUE);
//                }
                $deal_image_path = $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        } elseif ($id == 0) {

            $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
            return FALSE;
        }

        unset($data['deal_image']);

        !$deal_image_path || ($data['deal_image_path'] = $deal_image_path);
        $test = $this->input->post();

        // $data['deal_timer_expire'] = str_replace(',','',$test);
        // debug($data['deal_timer_expire']);die;
        $data['deal_status'] = $this->input->post('deal_status') ? '1' : '0';


        if ($id == 0) {
            // echo 'here';die;
            $featured = $this->input->post('slider_featured') ? '1' : '0';
            $top = $this->input->post('slider_top') ? '1' : '0';
            $bottom = $this->input->post('slider_bottom') ? '1' : '0';

            $id = $this->deal_model->insert($data);
            $this->db->insert('slider', array('slider_type_id' => $id,
                'slider_type' => 'deal',
                'slider_top' => $top,
                'slider_bottom' => $bottom,
                'slider_featured' => $featured));
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {

            $old_logo = NULL;
            if ($deal_image_path) {
                $old_logo = $this->deal_model->get($id)->deal_image_path;
            }
            $this->deal_model->update($id, $data);

            $this->db->delete('slider', array('slider_type_id' => $id));
            // echo $this->db->query();die;
            $featured = $this->input->post('slider_featured') ? '1' : '0';
            $top = $this->input->post('slider_top') ? '1' : '0';
            $bottom = $this->input->post('slider_bottom') ? '1' : '0';

            $this->db->insert('slider', array('slider_type_id' => $id,
                'slider_type' => 'deal',
                'slider_top' => $top,
                'slider_bottom' => $bottom,
                'slider_featured' => $featured));

            if ($old_logo and file_exists(config_item('deal_image_root') . $old_logo))
                unlink_file(config_item('deal_image_root') . $old_logo);


            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/deals/', 'refresh');
    }

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->deal_model->update($id, array('deal_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function toggle_review_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->load->model(array('review_model'));
            $this->review_model->update($id, array('status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function delete($id) {
        $old_logo = $this->deal_model->get($id)->deal_image_path;

        if ($this->deal_model->delete($id)) {
            if ($old_logo and file_exists(config_item('deal_image_root') . $old_logo))
                unlink_file(config_item('deal_image_root') . $old_logo);
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/deals/', 'refresh');
    }

    function deals_csv() {
        $this->load->library('my_phpexcel');

        $sheet = $this->my_phpexcel->getActiveSheet();

        $this->my_phpexcel->getProperties()
                ->setCreator("Report")
                ->setLastModifiedBy("ADMIN")
                ->setTitle('Report')
                ->setSubject('Report')
                ->setDescription('Report')
                ->setKeywords("")
                ->setCategory("");

//        echo "<pre>";
//        print_r($offer_arr);




        $this->my_phpexcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'deal_id')
                ->setCellValue('B1', 'title')
                ->setCellValue('C1', 'price')
                ->setCellValue('D1', 'short_description')
                ->setCellValue('E1', 'description')
                ->setCellValue('F1', 'right_tag')
                ->setCellValue('G1', 'left_tag')
                ->setCellValue('H1', 'start_date')
                ->setCellValue('I1', 'end_date')
                ->setCellValue('J1', 'quantity')
                ->setCellValue('K1', 'status')
                ->setCellValue('L1', 'image');
        $deal_details = $this->deal_model->deal_csv();
        //debug($deal_details,true);

        if (!empty($deal_details)) {
            $i = 2;
            foreach ($deal_details as $deal) {
                $this->my_phpexcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $i, $deal['deal_id'])
                        ->setCellValue('B' . $i, $deal['deal_title'])
                        ->setCellValue('C' . $i, $deal['deal_price'])
                        ->setCellValue('D' . $i, strip_tags($deal['deal_short_desc']))
                        ->setCellValue('E' . $i, strip_tags($deal['deal_desc']))
                        ->setCellValue('F' . $i, $deal['deal_tag'])
                        ->setCellValue('G' . $i, $deal['deal_left_tag'])
                        ->setCellValue('H' . $i, date('d/m/Y', strtotime($deal['deal_created_date'])))
                        ->setCellValue('I' . $i, date('d/m/Y', strtotime($deal['deal_offer_expire'])))
                        ->setCellValue('J' . $i, $deal['deal_quantity'])
                        ->setCellValue('K' . $i, $deal['deal_status'])
                        ->setCellValue('L' . $i, '');
                $i++;
            }
        }





        // Redirect output to a client’s web browser (Excel5)
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $objWriter = PHPExcel_IOFactory::createWriter($this->my_phpexcel, 'CSV');
        $objWriter->save('php://output');
        exit;
    }

    function import_csv_data() {
        if ($this->xls_upload()) {
            $this->load->library('my_phpexcel');
            $this->uploaded_xls_file = $this->xls_file_data['full_path'];
//			$inputFileType = 'Excel2007';
            $inputFileType = 'CSV';
//			$sheetname = 'Gyms';
            $inputFileName = $this->uploaded_xls_file;
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);

//				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
            $objPHPExcel = $objReader->load($inputFileName);

            $worksheet = $objPHPExcel->getActiveSheet();




            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn();

            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            //get header name and informations
            $this->my_phpexcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'deal_id')
                    ->setCellValue('B1', 'title')
                    ->setCellValue('C1', 'price')
                    ->setCellValue('D1', 'short_description')
                    ->setCellValue('E1', 'description')
                    ->setCellValue('F1', 'right_tag')
                    ->setCellValue('G1', 'left_tag')
                    ->setCellValue('H1', 'start_date')
                    ->setCellValue('I1', 'end_date')
                    ->setCellValue('J1', 'quantity')
                    ->setCellValue('K1', 'status')
                    ->setCellValue('L1', 'image');

            $req_header = array(
                'deal_id'
                , 'title'
                , 'price'
                , 'short_description'
                , 'description'
                , 'right_tag'
                , 'left_tag'
                , 'start_date'
                , 'end_date'
                , 'quantity'
                , 'status'
                , 'image'
            );

            $csv_header_title = array();
            $csv_header_title_col = array();

            for ($col = 0; $col < $highestColumnIndex; ++$col) {
                $cell = $worksheet->getCellByColumnAndRow($col, 1);
                $val = $cell->getValue();
                if (!empty($val)) {
                    $csv_header_title[] = strtolower($val);
                    $csv_header_title_col[strtolower($val)] = $col;
                }
            }

            // debug($csv_header_title);
//            if (!empty(array_diff($req_header, $csv_header_title)))
//                die('error csv file');
            $log_file = FCPATH . 'application/logs/' . uniqid() . '.txt';
            file_put_contents($log_file, PHP_EOL . 'Import Errors: ' . PHP_EOL);

            $insBatArr = array();
            for ($row = 2; $row <= $highestRow; ++$row) {

                $insArr = array();
                $insArr['row_number'] = $row;
                //for deal_id
                $insArr['deal_id'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['deal_id'], $row)->getValue());

                //for title
                $insArr['title'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['title'], $row)->getValue());

                //for price
                $insArr['price'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['price'], $row)->getValue());

                //for short_description
                $insArr['short_description'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['short_description'], $row)->getValue());
                $insArr['description'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['description'], $row)->getValue());

                //for left_tag
                $insArr['left_tag'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['left_tag'], $row)->getValue());

                //for right_tag
                $insArr['right_tag'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['right_tag'], $row)->getValue());

                //for start_date
                $insArr['start_date'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['start_date'], $row)->getValue());


                //for end date
                $insArr['end_date'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['end_date'], $row)->getValue());

                //for quantity
                $insArr['quantity'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['quantity'], $row)->getValue());

                $insArr['status'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['status'], $row)->getValue());

                $insArr['image'] = trim($worksheet->getCellByColumnAndRow($csv_header_title_col['image'], $row)->getValue());


                $this->error_csv = array();
                $this->_update_data($insArr);
                if (!empty($this->error_csv)) {

                    $data = PHP_EOL . ' Row ' . $row . ': ' . implode(', ', $this->error_csv);
                    file_put_contents($log_file, $data, FILE_APPEND);
                    $this->session->set_flashdata('log_data', $log_file);
                }
            }

            unlink($this->uploaded_xls_file);

            $this->session->set_flashdata('success_message', 'Import Successfully Done.');
            redirect(current_url());
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);

        Template::set('title', 'Import csv');
        Template::set_base_title('Import csv');
        Template::render('admin/import_csv_form', $this->data);
    }

    function _update_data($data) {

        $id = (int) $data['deal_id'];

        $left_tag = $data['left_tag'];
        if (strtolower($left_tag) == 'quantity' || strtolower($left_tag) == 'timer') {
            // debug($datas);
            
            //check valid date
            if(!empty($data['end_date'])) {
                if($vDate = DateTime::createFromFormat('d/m/Y', $data['end_date'])) {
                    $end_date = date('Y-m-d', $vDate->getTimestamp());
                } else {
                    $end_date = date('Y-m-d');
//                    $this->error_csv[] = ' Invalid date';
                
                } 
//                $end_date = !empty($data['offer_valid_date']) && _date_is_valid($data['end_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $data['offer_valid_date']))) : '';
//        debug($offerArr['offer_valid_date']);die;
                
            }
            
//            $end_date = !empty($data['end_date']) && $this->_date_is_valid($data['end_date']) ? date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date']))) : '';
            $datas = array(
                'deal_title' => $data['title'],
                'deal_price' => $data['price'],
                'deal_tag' => $data['right_tag'],
                'deal_left_tag' => $left_tag,
                'deal_created_date' => date('Y-m-d H:i:s'),
                'deal_offer_expire' => $end_date,
                'deal_short_desc' => $data['short_description'],
                'deal_desc' => $data['description'],
                'deal_quantity' => $data['quantity'],
                'deal_status' => $data['status']
            );

            if (!empty($data['image']) and file_exists(FCPATH . 'assets/uploads/temp_deal_images/' . $data['image'])) {
                $data['image'] = move_file(FCPATH . 'assets/uploads/temp_deal_images/', config_item('deal_image_root'), $data['image']);
            } else {
                $data['image'] = '';
            }
        
            if($data['image']) {
                $datas['deal_image_path'] = $data['image'];
            }


            if (empty($id)) {
                $this->db->insert('deals', $datas);
            } else {
                //check deal id
                $deal = $this->deal_model->get($id);
                if (!empty($deal)) {


                    $this->db->where('deal_id', $id);
                    $this->db->update('deals', $datas);
                    //remove already exist image if new image is updated.
                    if (file_exists(config_item('deal_image_root') . $deal->deal_image_path)) {
                        unlink_file(config_item('deal_image_root') . $deal->deal_image_path);
                    }
                }
            }
        } else {

            $this->error_csv[] = 'Cannot add/update.Left tag must have value either quantity or timer.';
            return false;
// $this->session->set_flashdata('error_message', 'Import failed.Left tag must be either quantity or timer.');
        }
    }

    function xls_upload() {
        $config['upload_path'] = './assets/uploads/csv';
        $config['allowed_types'] = '*';
        $config['max_size'] = '1024';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload()) {
            return false;
        } else {
            $this->xls_file_data = $this->upload->data();
            //debug($this->xls_file_data);
            return true;
        }
    }

    function _date_is_valid($str) {
        if (substr_count($str, '/') == 2) {
            list($d, $m, $y) = explode('/', $str);
            return checkdate($m, $d, sprintf('%04u', $y));
        }

        return false;
    }

}
