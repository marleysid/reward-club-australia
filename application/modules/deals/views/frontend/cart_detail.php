    <div class="container"> 
        <ol class="breadcrumb">
            <li class="active"><a href="<?php echo site_url('deals/cart'); ?>">My Cart: </a></li>
            <li> <?php echo $total_items? $total_items:'0'; ?> &nbsp;Items</li>
        </ol>
        <div class="row">
				
 
              <?php if (!empty($cart_detail)): ?>
                       
                    <div class="col-md-9 col-xs-12 col-sm-12 cart-list-holder padding-right-12"> 
                    <div class="row">
                        <!--repeated start here -->
                           <?php foreach ($cart_detail as $cart): ?>
                            <?php $total_quan =  $cart->deal_quantity ? $cart->deal_quantity:'0' +  $cart->quantity ; 
//                            echo debug($total_quan);die; ?>
                                <div class="col-xs-12 col-sm-12 col-md-12 btn-margin-35">
                                    <div class="col-xs-12 col-sm-12 col-md-4 bg_nn_pgmrgn"> <a href="<?php echo site_url('offers/detail/deal/'.safe_b64encode($cart->deal_id)); ?>" class="thumb-holder"><img src="<?php echo imager($cart->deal_image_path ? 'assets/uploads/deal_image/' . $cart->deal_image_path : '', 279, 197,1); ?>" class="img-responsive"></a> </div>
                                    <div class="col-xs-12 col-sm-12 col-md-8 with_resuld_board bg_grw_01 padding-top-bottom-9">
                                        <div class="col-xs-12 col-sm-12 col-md-8 padding-left-7 margin-buttom-10 padding-right ">
                                        <div class="whit_box">
                                            <h4><?php echo $cart->deal_title; ?></h4>
                                            <p><?php echo character_limiter($cart->deal_short_desc, 100); ?> </p>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-4 padding-left-right-8">
                                        <div class="whit_box fix_with_right">
                                            <h4 style="padding-bottom:0; text-align:left;" class="total-value-holder"><a href="#" >AU <label class="calc_total" data-price="<?php echo $cart->deal_price ; ?>"><?php echo ($cart->deal_price * $cart->quantity); ?></label></a></h4>
                                            <div class="quantity-holder" style="text-align:left; overflow:hidden; margin-bottom:10px;">
                                            	<label style="display:inline-block; float:left;"> QTY:</label>
                                                <div class="quantity_dropdown" style="display: none">
                                                    <select value="quan_dropdown" class="quan-dropdown-holder" data-old-value="<?php  echo $cart->quantity; ?>" >
                                                    <?php if(!empty($total_quan)): ?>
                                                    <?php for($key = 1;$key <= $total_quan;$key++){ ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $key; ?></option>
                                                    <?php } ?>
                                                    <?php endif; ?>
                                                </select>
                                                </div>
                                                <input style="border:0px; width:50px; background:none; float:left; margin-left:10px; position: relative; top:0; text-align:center;" type="text" class="quantity_box"  name="quantity" value="<?php  echo $cart->quantity; ?>" data-old-value="<?php  echo $cart->quantity; ?>" disabled />
                                                <a style="float:right;" onclick="return confirm('Are you sure?')" href="<?php echo site_url().'deals/remove_from_cart/'.$cart->deal_id; ?>"><img src="<?php echo base_url('assets/frontend/images/delete.png'); ?>" style="width: 20px;" ></a>
                                            </div>
                                           
                                            
                                            
                                            <button class="view_website btn fx_font_siz heart_bg edit_button"> <span style="float:left">Edit</span> <span style="float:right;"> <img src="<?php echo base_url('assets/frontend/images/icon-edit-small.png'); ?>" > </span></button>
                                            <button class="view_website btn fx_font_siz heart_bg save_button" data-id="<?php echo $cart->deal_id; ?>"> Save </button>
                                            <button class="view_website btn fx_font_siz favourite"  data-type="<?php echo 'deal'; ?>" data-id="<?php echo $cart->deal_id ?>">  <span style="float:left">Favourite This</span>  <span style="float:right;"><img src="<?php echo base_url('assets/frontend/images/favourites-icon-smal-white-bg.png'); ?>" ></span> </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                         <!--repeated end here --> 
						</div>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 padding-left-12">
                        <div class="cart-total-holder padding-top-bottom-9">
                            <div class="whit_box">
                                <h4 class="display-total-price">TOTAL: <span>(excludes delivery)</span></h4>
                                <a href="#" class="total">$<label id="order_total_price"><?php echo $total_amount->total; ?></label></a>
                            </div>
                        </div>
                        <a class="btn-continue-shopping" href="<?php echo site_url('offers'); ?>"><button class="view_website btn fx_font_siz heart_bg"> Continue Shopping </button></a>
                        <a class="btn-payment" href="<?php echo site_url('orders'); ?>"><button class="view_website btn fx_font_siz">Pay Securely Now </button></a>
                    </div>
           

                <!--pagination start here --> 

                <!--pagination end here --> 

            <?php else:?>
            <div class="col-md-12">
            <h4>  No items in your cart</h4>
            </div> 
              <?php endif; ?>
    
        </div>
    </div>

<script type="text/javascript">
$(function(){
    $('.save_button').hide();
   $('.edit_button').on('click',function(){
       //$(".whit_box fix_with_right").siblings('.quantity_dropdown').css("display","block");
       $(this).siblings('.quantity-holder').children('.quantity_dropdown').show();
   
       $(this).siblings('.quantity-holder').find('.quantity_box').hide();
//       $(this).siblings('.quantity-holder').find('.quantity_box').removeAttr('disabled');
//       $(this).siblings('.quantity-holder').find('.quantity_box').css('border','1px solid').focus();

       $(this).hide();
       $(this).siblings('.save_button').show();
   }); 
   
   $('.save_button').on('click',function(){
//       $(this).siblings('.quantity-holder').find('.quantity_box').show();
        $(this).siblings('.quantity-holder').children('.quantity_dropdown').hide();
//       $(this).siblings('.quantity-holder').find('.quantity_box').attr('disabled',true);
//       $(this).siblings('.quantity-holder').find('.quantity_box').css('border','none');

//       $('.quantity_box').parent().css('border','2px');
       $(this).siblings('.edit_button').show();
       $(this).hide();
       
//      var quantity = $(this).siblings('.quantity-holder').find('.quantity_box').val();
      var quantity = $(this).siblings('.quantity-holder').find('.quan-dropdown-holder option:selected').val();
//      alert(quantity);
      var id = $(this).data('id');
      var amount_per_quan = $(this).siblings('.total-value-holder').find('.calc_total').data('price');
//      alert(amount_per_quan);
//      alert(quantity * amount_per_quan);
      
      var _this_save = $(this);
//    alert(quantity);
 if (quantity) {

                        $.ajax({
                            type: 'post',
                            url:  base_url + 'deals/quantity_change/' + quantity + '/' + id,
                            success: function(data) {
                               // console.log(data);
//                                console.log(_this_save);
                               if(data.status == 1){
                                   _this_save.siblings('.quantity-holder').find('.quantity_box').data('old-value', quantity).val(quantity);
                                    _this_save.siblings('.quantity-holder').find('.quantity_box').show();
        
                                   _this_save.siblings('.total-value-holder').find('.calc_total').html(quantity * amount_per_quan);
                                  $('#order_total_price').html(data.amount);
//                                   $('.calc_total').val(quantity * amount_per_quan);
                                   alert('Quantity updated to your cart');
                               }else if(data.status == 3){
                                   _this_save.siblings('.quantity-holder').find('.quantity_box').val(_this_save.siblings('.quantity-holder').find('.quantity_box').data('old-value'));
                                     _this_save.siblings('.quantity-holder').find('.quantity_box').show();
        
                                   alert('Invalid deal');
                               }else if(data.status == 4){
                                   _this_save.siblings('.quantity-holder').find('.quantity_box').val(_this_save.siblings('.quantity-holder').find('.quantity_box').data('old-value'));
                                    _this_save.siblings('.quantity-holder').find('.quantity_box').show();
        
                                    alert('Sorry! Out of Stock');
                               } 
                            },
                            error: function() {
                                alert('Adding quantity failed');
                            },
                            dataType : 'json'
                            
                        });
                    }
                    else {
//                            alert('hi');
                    }
   });
});
</script>