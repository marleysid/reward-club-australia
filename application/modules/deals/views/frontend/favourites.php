<div class="container"> 
 <ol class="breadcrumb">
      <li><a href="<?php echo site_url('deals/favourites'); ?>">Favourites</a></li>
      <li class="active"><?php echo $total_items? $total_items:'0';?> &nbsp;Items</li>
   
  </ol>
  <div class="row">
   <div class="col-md-10 favourites-list-holder"> 
          <div class="row">
      
       <!--repeated start here -->
       <?php if(!empty($favourites)): ?>
       <?php foreach ($favourites as $fav): ?>
       <div class="col-xs-12 col-sm-12 col-md-12 btn-margin-35">

        <div class="col-xs-12 col-sm-12 col-md-4 bg_nn_pgmrgn "> 
       
         <div class="tag"><img class="price" src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="img-responsive"><span><?php echo space2br($fav->tag); ?></span></div>
          <a href="<?php echo site_url('offers/detail/'.$fav->type.'/'.safe_b64encode($fav->id)); ?>" class="thumb-holder"><img src="<?php if ($fav->type == 'deal')
                        echo imager($fav->image ? 'assets/uploads/deal_image/' . $fav->image : '', 279, 197,1);
                    else
                        echo imager($fav->image ? 'assets/uploads/offer_image/' . $fav->image : '', 279, 197,1);
                  ?>" class="img-responsive"></a> </div>
        <div class="col-xs-12 col-sm-12 col-md-8 with_resuld_board bg_grw_01 padding-top-bottom-9">
          <div class="col-xs-12 col-sm-12 col-md-8 padding-left-7 padding-right">
         	<div class="whit_box">
            <h4><?php echo $fav->title; ?></h4>
            <p><?php echo $fav->short_desc; ?></p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 padding-left-right-8">
          	<div class="whit_box fix_with_right fav-holder">
            	<div class="icon-favourites-big-red-holder"><img src="<?php echo base_url('assets/frontend/images/icon-favourites-big-red.png'); ?>" ></div>
               
                                           
            <div  data-score="<?php echo $fav->review; ?>" class="rate" style="padding:10px 0;"></div> 
              <h4><?php echo $fav->total_review; ?> Reviews </h4>
               <a style="display:block; margin-bottom:8px;" onclick="return confirm('Are you sure?')" href="<?php echo site_url().'deals/remove_from_favourite/'.$fav->type.'/'.$fav->id; ?>"><img src="<?php echo base_url('assets/frontend/images/delete.png'); ?>" style="width: 20px;" ></a>
          
              <a href="<?php echo site_url('offers/detail/'.$fav->type.'/'.safe_b64encode($fav->id)); ?>"><button class="view_website btn fx_font_siz"> View Details <img src="<?php echo get_asset('assets/frontend/images/view_detail_arrow.png'); ?>"  > </button></a>
              </div>
          </div>
        </div>
        </div>
      
       <?php endforeach; ?>
        <?php else:?>
            <h4>  No items in your favourites</h4>
           
       <?php endif; ?>
       </div>
      <!--repeated end here -->
      <!--pagination start here -->
      
      <!--pagination end here -->

    
    
    </div>
    <div class="col-md-2 ">
        <?php if(isset($get_ads)): ?>
      <a href="#"><img src="<?php echo imager($side_ads->ad_image_path ? 'assets/uploads/ad/'.$side_ads->ad_image_path:'',161,602); ?>" class="img-responsive"></a>
 
    	<?php endif; ?>
    </div>
  </div>
</div>

<script>
    $(function(){
       $('.rate').raty( {path:  base_url + 'assets/lib/jquery-raty/images',
                         readOnly: true,
       score: function() {
    return $(this).attr('data-score');
  }});  
    });
   
    </script>