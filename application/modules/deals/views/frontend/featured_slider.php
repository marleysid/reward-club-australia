<?php //foreach($is_featured_address as $is_featured_address): ?>
<!---->
<!--    --><?php //echo $is_featured_address->oa_suburb ?>
<?php //endforeach; ?>
<div class="feature-wrapper">
    <?php if(isset($is_featured)): ?>
        <div class="featured-deals-holder">
            <!-- featureCarousel -->
            <h3>Featured</h3>
            <div id="featureCarousel" class="owl-carousel">
                <?php foreach($is_featured as $featured): ?>
                    <div class="item">
                        <a href="<?php echo site_url('offers/detail/offer/' . safe_b64encode($featured->offer_id).'/'.strtolower(url_title($featured->offer_title,'-','TRUE'))); ?>"><img src="<?php echo imager($featured->offer_image ? 'assets/uploads/offer_image/'.$featured->offer_image:'',463,216, 1); ?>" class="img-responsive" alt="slide" style="height: 190px;"></a>
                        <div class="info-wrap">
                            <strong class="title"><a href="<?php echo site_url('offers/detail/offer/' . safe_b64encode($featured->offer_id).'/'.strtolower(url_title($featured->offer_title,'-','TRUE'))); ?>"><?php echo $featured->offer_title ?></a>
                                <?php //echo $featured->oa_suburb ?>
                            </strong>
                            <span class="location"><?php echo $featured->oa_suburb; ?></span>
                        </div>

                        <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm_01"><?php echo space2br($featured->offer_tag); ?></span>
                    </div>

                <?php endforeach; ?>
            </div>
        </div>
    <?php endif; ?>
</div>



