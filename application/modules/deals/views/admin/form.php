<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> Deals</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>" enctype="multipart/form-data" id="append">
                <div class="form-group <?php echo form_error('deal_title') ? 'has-error' : '' ?>">
                    <label for="deal_title" class="control-label col-lg-3">Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="deal_title" placeholder="Title" name="deal_title" class="form-control" value="<?php echo set_value("deal_title", $edit ? $deal_detail->deal_title : ''); ?>" >
                        <?php echo form_error('deal_title'); ?>
                    </div>
                </div>
                 <div class="form-group <?php echo form_error('deal_short_title') ? 'has-error' : '' ?>">
                    <label for="deal_short_title" class="control-label col-lg-3">Short Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="deal_short_title" placeholder="Short Title" name="deal_short_title" class="form-control" value="<?php echo set_value("deal_short_title", $edit ? $deal_detail->deal_short_title : ''); ?>" >
                        <?php echo form_error('deal_short_title'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('deal_price') ? 'has-error' : '' ?>">
                    <label for="deal_price" class="control-label col-lg-3">Price *</label>
                    <div class="col-lg-7">
                        <input type="text" id="deal_price" placeholder="Price" name="deal_price" class="form-control" value="<?php echo set_value("deal_price", $edit ? $deal_detail->deal_price : ''); ?>" >
                        <?php echo form_error('deal_price'); ?>
                    </div>
                </div>

                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="deal_image" class="control-label col-lg-3">Image *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span> 
                                <span class="fileinput-exists">Change</span> 
                                <input type="file" name="deal_image">
                            </span> 
                            <span class="fileinput-filename"></span> 
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists(config_item('deal_image_path') . $deal_detail->deal_image_path)): ?>
                            <div>
                                <img src="<?php echo imager(config_item('deal_image_path') . $deal_detail->deal_image_path, 128, 128); ?>" alt="deal image" class="img-thumbnail">
                                <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                            </div>
                        <?php endif; ?>
                    </div>
                </div><!-- /.form-group -->		  
                <div class="form-group <?php echo form_error('deal_short_desc') ? 'has-error' : '' ?>">
                    <label for="deal_short_desc" class="control-label col-lg-3">Short Description *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="short_desc" placeholder="Short Description" name="deal_short_desc" class="form-control"><?php echo set_value("deal_short_desc", $edit ? $deal_detail->deal_short_desc : ''); ?></textarea>
                        <?php echo form_error('deal_short_desc'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('deal_desc') ? 'has-error' : '' ?>">
                    <label for="deal_desc" class="control-label col-lg-3"> Description *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="offer_desc" placeholder=" Description" name="deal_desc" class="form-control"><?php echo set_value("deal_desc", $edit ? $deal_detail->deal_desc : ''); ?></textarea>
                        <?php echo display_ckeditor('deal_desc'); ?>
                        <?php echo form_error('deal_desc'); ?>
                    </div>
                </div>

                <div class="form-group <?php echo form_error('deal_tag') ? 'has-error' : '' ?>">
                    <label for="deal_tag" class="control-label col-lg-3">Right Tag *</label>
                    <div class="col-lg-7">
                        <input type="text" id="offer_tag" placeholder="Tag" name="deal_tag" class="form-control" value="<?php echo set_value("deal_tag", $edit ? $deal_detail->deal_tag : ''); ?>" >
                        <?php echo form_error('deal_tag'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('deal_left_tag') ? 'has-error' : '' ?>">
                    <label for="deal_tag" class="control-label col-lg-3"> Left Tag *</label>
                    <div class="col-lg-7">
                        <select name="deal_left_tag" class="form-control">
                            <option value="">Select</option>
                            <option value="quantity"<?php
                            if ($edit) {
                                if ($deal_detail->deal_left_tag == "quantity") {
                                    echo "selected=\"selected\" ";
                                }
                            };
                            ?>>Quantity</option>
                            <option value="timer"<?php
                            if ($edit) {
                                if ($deal_detail->deal_left_tag == "timer") {
                                    echo "selected=\"selected\" ";
                                }
                            };
                            ?>>Timer</option>
                        </select>
                         <?php echo form_error('deal_left_tag'); ?>
                    </div>
                   
                </div>
                <?php
//                if($edit){
//                $date = str_replace('-','/',$deal_detail->deal_offer_expire);}?>
                <div class="form-group <?php echo form_error('deal_offer_expire') ? 'has-error' : '' ?>">
                    <label for="deal_offer_expire" class="control-label col-lg-3"> Offer Expiry Date *</label>
                    <div class="col-lg-7">
                        <input type="text" id="deal_offer_expire" placeholder="Date" name="deal_offer_expire" class="form-control" value="<?php echo set_value("deal_offer_expire", $edit ? date('m/d/Y',  strtotime($deal_detail->deal_offer_expire)) : ''); ?>" >
                         <?php echo form_error('deal_offer_expire'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('deal_timer_expire') ? 'has-error' : '' ?>">
                    <label for="deal_expire_date" class="control-label col-lg-3"> Timer Expiry Date *</label>

                    <div class="col-lg-7">
                        <input  class="form-control" type="text" id="deal_expire_date" placeholder="Date" name="deal_timer_expire"  id="datetimepicker" value="<?php echo set_value("deal_timer_expire", $edit ? $deal_detail->deal_timer_expire : ''); ?>">
                        <?php echo form_error('deal_timer_expire'); ?>
                    </div>
                </div>

                <div class="form-group <?php echo form_error('deal_quantity') ? 'has-error' : '' ?>">
                    <label for="deal_quantity" class="control-label col-lg-3"> Quantity *</label>
                    <div class="col-lg-7">
                        <input type="text" id="deal_quantity" placeholder="Quantity" name="deal_quantity" class="form-control" value="<?php echo set_value("deal_quantity", $edit ? $deal_detail->deal_quantity : ''); ?>" >
                        <?php echo form_error('deal_quantity'); ?>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="slider_featured" class="control-label col-lg-3">Is Featured</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="slider_featured" class="switch switch-small <?php echo (string)(($edit and isset($slider_detail->slider_featured)) ? ($slider_detail->slider_featured ? 1 : 0) : 1) ?>"  value="1" <?php echo  set_checkbox('slider_featured', '1', ($edit and isset($slider_detail->slider_featured)) ? ($slider_detail->slider_featured ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                    </div>
                </div>

                 <div class="form-group">
                    <label for="slider_top" class="control-label col-lg-3">Top Slider</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="slider_top" class="switch switch-small"  value="1" <?php echo set_checkbox('slider_top', '1', ($edit and isset($slider_detail->slider_top)) ? ($slider_detail->slider_top ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                    </div>
                </div>
                <div class="form-group">
                    <label for="slider_bottom" class="control-label col-lg-3">Bottom Slider</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="slider_bottom" class="switch switch-small"  value="1" <?php echo set_checkbox('slider_bottom', '1', ($edit and isset($slider_detail->slider_bottom)) ? ($slider_detail->slider_bottom ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                    </div>
                </div>
                <div class="form-group">
                    <label for="deal_status" class="control-label col-lg-3">Status </label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="deal_status" class="switch switch-small"  value="1" <?php echo set_checkbox('deal_status', '1', ($edit) ? ($deal_detail->deal_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                        <?php echo anchor('admin/deals', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>


<!--        <link href="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/d004434a5ff76e7b97c8b07c01f34ca69e635d97/build/css/bootstrap-datetimepicker.css" rel="stylesheet">

                        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
                        <script src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/v4.0.0/src/js/bootstrap-datetimepicker.js"></script>-->
<script type="text/javascript">
    $(document).ready(function() {
        $('#deal_expire_date').datetimepicker({
            format: "YYYY-MM-DD, HH:mm:ss"
           // moment().format("YYYY-MMM-DDD, HH:mm:ss");
        }
                
                );
    });
</script>
<script>
    $(document).ready(function() {
         $("#deal_quantity").numeric();
         $("#deal_price").numeric();
          $('#deal_offer_expire').datepicker(
                );
    });

</script>
