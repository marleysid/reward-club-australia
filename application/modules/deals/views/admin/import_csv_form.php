<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $title; ?></h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body"><div class="textbox class">
<span style="padding:15px; font-size:15px;">To upload required images <a href="<?php echo site_url().'admin/media/manage_media/deal';?>" >Click here</a>.</span>
    <br>    
    <br>    
    
                <form action="<?php echo current_url(); ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                        <label for="page_image" class="control-label col-lg-2">Csv file </label>
                        <div class="col-lg-10">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn btn-default btn-file">
                                    <span class="fileinput-new">Select file</span> 
                                    <span class="fileinput-exists">Change</span> 
                                    <input type="file" name="userfile">
                                </span> 
                                <span class="fileinput-filename"></span> 
                                <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                                <?php echo @$logo_error; ?>
                            </div>
                        </div>
                    </div><!-- /.form-group -->	
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                            <?php echo anchor('admin/location', 'Cancel', 'class="btn btn-warning"'); ?>
                        </div>
                    </div>
  <!--            <p>
                  <label>Upload File :</label>
                  <br />
                  <input type="file" size="40" class="text" name="userfile" value="" required />
              </p>
              <br />
              <p>
                  <input type="submit" class="btn btn-primary" value="Submit" />
                  <a class="btn btn-warning" href="<?php echo site_url("admin/classes"); ?>"><i class="icon-refresh icon-white">&nbsp;</i>&nbsp;Cancel</a>
              </p>-->
                </form>
            </div>
        </div>
        <?php if ($log_file = $this->session->flashdata('log_data')): ?>
            <pre>
                <?php echo file_get_contents($log_file); ?>
            </pre>
        <?php endif; ?>
    </div>
</div>