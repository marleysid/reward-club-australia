<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Deal_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'deals';
        $this->field_prefix = 'deal_';
        $this->log_user = FALSE;
    }

    function cart_detail($id) {
        $this->db->select("SQL_CALC_FOUND_ROWS deals.deal_id,deals.deal_title,deal_price,deals.deal_quantity,SUM(cart_session.quantity * deals.`deal_price`) AS total,deals.deal_short_desc,deal_image_path,cart_session.quantity",false)
                ->from("deals")
                ->join("cart_session", "deals.deal_id = cart_session.deal_id", "inner")
                ->join("members", "cart_session.member_id = members.member_id", "inner")
                ->where("cart_session.member_id = '$id'")
                ->group_by("deals.deal_id")
                ->order_by("deals.deal_title");

        $query = $this->db->get();

        return $query->result();
    }
   

    function favourites($id) {

        $query = $this->db->query(" (SELECT 
  SQL_CALC_FOUND_ROWS
  deals.`deal_id` AS id,
  deals.`deal_title` AS title,
  deals.`deal_short_desc` AS short_desc,
  deals.`deal_image_path` AS image,
  deals.deal_tag as tag,
  favourite.favourite_type as type,
  avg(reviews.review_rating) as review,
  count(reviews.review_rating) as total_review
FROM
  deals 
  INNER JOIN favourite 
    ON deals.`deal_id` = favourite.`favourite_type_id` 
    left join reviews
    on deals.deal_id = reviews.review_type_id and reviews.review_type = 'deal'
WHERE favourite.`favourite_type` = 'deal' AND favourite.`member_id` = '$id'
    group by deals.deal_id
    order by deals.deal_title) 
UNION
(SELECT
  offers.`offer_id`,
  offers.`offer_title`,
  offers.`offer_short_desc`,
  offers.`offer_image`,
  offers.offer_tag,
  favourite.favourite_type as type,
  avg(reviews.review_rating),
  count(reviews.review_rating)
  FROM offers
  INNER JOIN favourite
  ON offers.`offer_id` = favourite.`favourite_type_id`
  left join reviews
  on offers.offer_id = reviews.review_type_id and reviews.review_type = 'offer'
  WHERE favourite.`favourite_type` = 'offer' AND favourite.`member_id` = '$id'
      group by offers.offer_id
      order by offers.offer_title
)", FALSE);
        return $query->result();
    }

    function slider_detail($id) {
        $this->db->select("*")
                ->from("slider")
                ->where("slider_type_id = '$id' ");

        $query = $this->db->get();

        return $query->row();
    }

    function is_featured() {
      
        $this->db->select("offers.*,offer_address.*")
                ->from("offers")
                ->join("offer_address","offer_address.offer_id = offers.offer_id")
                ->join("slider", "offers.offer_id = slider.slider_type_id")
                ->where("slider.slider_featured = '1' and offers.offer_status != '0' ");
        $query = $this->db->get();

        return $query->result();
    }
    

    function is_featured_suburb() {
      $lat      = $_SESSION['latitude'];
      $lon      = $_SESSION['longitude'];
      $distance = $_SESSION['distance'];
     
      /*$query_main = "SELECT 
        `offers`.*, `offer_address`.*,
        ROUND(3959 * ACOS(
          COS(RADIANS($lat)) * COS(
            RADIANS(`offer_address`.`oa_latitude`)
          )
        ) * COS(
          RADIANS(`offer_address`.`oa_longitude`) - RADIANS($lon)
        ) + SIN(RADIANS($lat)) * SIN(
          RADIANS(`offer_address`.`oa_latitude`)
        ),
        1) AS `distance` FROM (`offers`) 
        JOIN `offer_address` 
          ON `offers`.`offer_id` = `offer_address`.`offer_id` 
        JOIN `slider` 
          ON `offers`.`offer_id` = `slider`.`slider_type_id` WHERE `slider`.`slider_featured` = '1' 
        AND offers.offer_status != '0' HAVING distance > $distance 
      ";
      */
      $query_main = "SELECT 
        `offers`.*, `offer_address`.*,
        3956*2 * ASIN ( SQRT (POWER(SIN(($lat - `offer_address`.`oa_latitude`)*pi()/180 / 2),2) + COS($lat * pi()/180) * COS(`offer_address`.`oa_latitude` *pi()/180) * POWER(SIN(($lon - `offer_address`.`oa_longitude`) *pi()/180 / 2), 2) ) ) as distance FROM (`offers`) 
        JOIN `offer_address` 
          ON `offers`.`offer_id` = `offer_address`.`offer_id` 
        JOIN `slider` 
          ON `offers`.`offer_id` = `slider`.`slider_type_id` WHERE `slider`.`slider_featured` = '1' 
        AND offers.offer_status != '0' HAVING distance <= $distance 
      ";
        // $this->db->select("offers.*, ROUND(6371 * ACOS(COS(RADIANS($lat)) * COS(RADIANS(offer_address.oa_latitude))) * COS(RADIANS(offer_address.oa_longitude) - RADIANS($lon)) + SIN(RADIANS($lat)) * SIN(RADIANS(offer_address.oa_latitude)), 1) as `distance`")
        //         ->from("offers")
        //         ->join("offer_address","offers.offer_id = offer_address.offer_id")
        //         ->join("slider", "offers.offer_id = slider.slider_type_id")
        //         ->where("slider.slider_featured = '1' and offers.offer_status != '0' ");
        // $query = $this->db->get();
        $query = $this->db->query($query_main);
        //echo $this->db->last_query(); exit;
        return $query->result();
    }

    function get_top_slider() {
        $query = $this->db->query("(SELECT deals.deal_id AS id,deals.`deal_title` AS title,deals.`deal_short_desc` AS short_desc,
deals.`deal_desc` AS description,deals.`deal_image_path` AS image,deals.`deal_offer_expire`  AS offer_expire, slider.slider_type_id,'deal' as type FROM deals
INNER JOIN slider
ON deals.`deal_id` = slider.`slider_type_id`
WHERE slider.`slider_type` = 'deal' AND deals.`deal_status` = '1' AND CURRENT_DATE <= deals.`deal_offer_expire` AND slider.`slider_top` = '1')
UNION
(SELECT offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_desc`,offers.`offer_image`,offers.`offer_valid_date`,slider.slider_type_id,'offer' as type FROM offers
INNER JOIN slider
ON offers.`offer_id` = slider.`slider_type_id`
WHERE slider.`slider_type` = 'offer' AND offers.`offer_status` = '1' AND CURRENT_DATE <= offers.`offer_valid_date`  AND slider.`slider_top` = '1')");

        return $query->result();
    }


    function get_bottom_slider() {
       $loc = $this->session->userdata('customLocation');
       if($loc)
          $loc_query = "AND offer_address.`oa_suburb` LIKE '$loc'";
        else
          $loc_query = "";

        $query = $this->db->query("
(SELECT DISTINCT offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_desc`,offers.`offer_image`,offers.`offer_valid_date`, 'offer' as type, slider.`slider_type`,slider.slider_type_id, offer_address.oa_suburb FROM offers
INNER JOIN slider
ON offers.`offer_id` = slider.`slider_type_id`
INNER JOIN offer_address
ON offers.`offer_id` = offer_address.`offer_id`
WHERE slider.`slider_type` = 'offer' AND offers.`offer_status` = '1' AND CURRENT_DATE <= offers.`offer_valid_date`  AND slider.`slider_bottom` = '1'
$loc_query
group by slider_type_id
)");
        return $query->result();
    }


    function get_bottom_slider_suburb()
    {
      $lat = $_SESSION['latitude'];
      $lon = $_SESSION['longitude'];
      $distance = $_SESSION['distance'];
      $query = $this->db->query("
        (SELECT DISTINCT 
          offers.`offer_id`,
          offers.`offer_title`,
          offers.`offer_short_desc`,
          offers.`offer_desc`,
          offers.`offer_image`,
          offers.`offer_valid_date`,
          'offer' AS type,
          slider.`slider_type`,
          slider.slider_type_id,
          offer_address.oa_suburb,
           ((((acos(sin((".$lat."*pi()/180)) * sin((`offer_address`.`oa_latitude`*pi()/180))+cos((".$lat."*pi()/180)) * cos((`offer_address`.`oa_latitude`*pi()/180)) * cos(((".$lon."- `offer_address`.`oa_longitude`)*pi()/180))))*180/pi())*60*1.1515*1.609344)) as  distance
        FROM
          offers 
          INNER JOIN slider 
            ON offers.`offer_id` = slider.`slider_type_id` 
          INNER JOIN offer_address 
            ON offers.`offer_id` = offer_address.`offer_id` 
        WHERE slider.`slider_type` = 'offer' 
          AND offers.`offer_status` = '1' 
          AND CURRENT_DATE <= offers.`offer_valid_date` 
          AND slider.`slider_bottom` = '1' HAVING distance <= $distance
        ORDER BY offer_address.`oa_suburb` ASC)
        ");
        //echo $this->db->last_query(); exit;
        return $query->result();
    }



     /*function get_bottom_slider_suburb()
    {
      $lat = $_SESSION['latitude'];
      $lon = $_SESSION['longitude'];
      $distance = $_SESSION['distance'];
      $query = $this->db->query("
        (SELECT DISTINCT 
          offers.`offer_id`,
          offers.`offer_title`,
          offers.`offer_short_desc`,
          offers.`offer_desc`,
          offers.`offer_image`,
          offers.`offer_valid_date`,
          'offer' AS type,
          slider.`slider_type`,
          slider.slider_type_id,
          offer_address.oa_suburb,
          3956*2 * ASIN ( SQRT (POWER(SIN(($lat - `offer_address`.`oa_latitude`)*pi()/180 / 2),2) + COS($lat * pi()/180) * COS(`offer_address`.`oa_latitude` *pi()/180) * POWER(SIN(($lon - `offer_address`.`oa_longitude`) *pi()/180 / 2), 2) ) ) as distance
        FROM
          offers 
          INNER JOIN slider 
            ON offers.`offer_id` = slider.`slider_type_id` 
          INNER JOIN offer_address 
            ON offers.`offer_id` = offer_address.`offer_id` 
        WHERE slider.`slider_type` = 'offer' 
          AND offers.`offer_status` = '1' 
          AND CURRENT_DATE <= offers.`offer_valid_date` 
          AND slider.`slider_bottom` = '1' HAVING distance <= $distance
        ORDER BY offer_address.`oa_suburb` ASC)
        ");
        echo $this->db->last_query(); exit;
        return $query->result();
    }*/



    function get_bottom_slider_detail($id) {
        $query = $this->db->query("(SELECT deals.deal_id AS id,deals.`deal_title` AS title,deals.`deal_short_desc` AS short_desc,
deals.`deal_desc` AS description,deals.`deal_image_path` AS image,deals.`deal_offer_expire`  AS offer_expire, slider.`slider_type`,slider.slider_type_id,avg(reviews.review_rating) as review_rating FROM deals
INNER JOIN slider
ON deals.`deal_id` = slider.`slider_type_id`
left join reviews
on deals.deal_id = reviews.review_type_id
WHERE slider.`slider_type` = 'deal' AND slider.`slider_type_id` = '$id'
    group by deals.deal_id)
UNION
(SELECT offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_desc`,offers.`offer_image`,offers.`offer_valid_date`,slider.`slider_type`,slider.slider_type_id,avg(reviews.review_rating) as review_rating FROM offers
INNER JOIN slider
ON offers.`offer_id` = slider.`slider_type_id`
left join reviews
on offers.offer_id = reviews.review_type_id
WHERE slider.`slider_type` = 'offer' AND slider.`slider_type_id` = '$id' group by offers.offer_id)");

        return $query->row();
    }

    function cart($id, $member_id) {
        $query = $this->db->select("deal_id")
                ->from("cart_session")
                ->where("deal_id = '$id' and member_id = '$member_id'")
                ->get();
        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
   

    function deals_quantity($id) {
        $query = $this->db->select("deal_quantity")
                ->from("deals")
                ->where("deal_id = '$id'")
                ->get();
        return $query->row();
    }

    function quantity_change($quantity, $id,$member_id) {
        $this->db->update('cart_session', array('quantity' => $quantity), array('deal_id' => $id, 'member_id' =>$member_id));
    }
    function check_prev_quan($id,$member_id){
        $query = $this->db->select("quantity")
                ->from("cart_session")
                ->where("deal_id = '$id' and member_id ='$member_id'")
                ->get();
        return $query->row();
    }
    

    function get_detail($id) {
        $query = $this->db->select("deals.deal_id,deals.deal_title,deals.deal_image_path,deals.deal_short_desc,deals.deal_desc,deals.deal_website, 'deal' as type,avg(reviews.review_rating) as review_rating", FALSE)
                ->from("deals")
                ->join("reviews", "deals.deal_id = reviews.review_type_id", "left")
                ->where("deal_id = '$id' and reviews.review_type = 'deal'")
                ->get();
        return $query->row();
    }

    function deal_csv() {
        return $this->db->select()
                        ->from("deals")
                        ->get()->result_array();
    }
    
    function deal_quantity($id){
        $query = $this->db->select("deal_quantity")
                          ->from("deals")
                          ->where("deal_id = '$id'")
                          ->get();
        return $query->row();
    }
   

}
