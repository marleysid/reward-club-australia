<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of login
 *
 * @author rabin
 */
class Login extends Front_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->model('deals/deal_model');
        $this->form_validation->CI = &$this;
        $this->template->set_layout('template/frontend_template');
    }

    public function index()
    {
//        if ($this->member_auth->logged_in())
        //            redirect('welcome');
        $status     = 0;
        $this->data = array();

        $this->form_validation->set_rules(array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'valid_email|xss_clean|trim|required|max_length[200]',
            ),
            array(
                'field'    => 'password',
                'password' => 'Password',
                'rules'    => 'trim|xss_clean|required|max_length[50]',
            ),
        ));

        if ($this->form_validation->run() == true) {
            $email    = $this->input->post('email');
            $password = $this->input->post('password');
            if ($this->member_auth->login($email, $password)) {
                $status      = 1;
                $url = base_url();
               
                echo json_encode(array('status' => $status, 'redirect_url' => $url));
            } else {
                $status = 0;
                $msg    = $this->member_auth->get_errors();
                echo json_encode(array('status' => $status, 'msg' => $msg));
            }
        } else {
            $status = 0;
            $msg    = validation_errors();
            echo json_encode(array('status' => $status, 'msg' => $msg));
        }

        //$this->template->render('frontend/login', $this->data);
    }

    public function register()
    {
//       if ($this->member_auth->logged_in())
        //          redirect('welcome');
        // echo'hello';die;
        // echo $this->input->post('first_name');
        $status     = 0;
        $this->data = array();

        $this->form_validation->set_rules(array(
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'xss_clean|trim|required|max_length[200]',
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'xss_clean|trim|required|max_length[200]',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'valid_email|xss_clean|trim|required|max_length[200]|unique[users.email]',
            ),
            array(
                'field' => 'postcode',
                'label' => 'Postcode',
                'rules' => 'xss_clean|trim|required|max_length[10]',
            ),
            array(
                'field' => 'terms_conditions',
                'label' => 'terms_conditions',
                'rules' => 'xss_clean|trim|required',
            ),
            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'xss_clean|trim|max_length[500]',
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'trim|xss_clean|required|max_length[50]|matches[password]',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|xss_clean|required|max_length[50]',
            ),
            array(
                'field' => 'suburb',
                'label' => 'suburb',
                'rules' => 'trim|xss_clean|required|max_length[50]',
            ),
        ));

        if ($this->form_validation->run() == true) {
            $email           = $_POST['email'];
            $password        = $_POST['password'];
            $activation_code = sha1(md5(microtime()));
            $additionalData  = array(
                'first_name'         => $this->input->post('first_name'),
                'last_name'          => $this->input->post('last_name'),
                'contact_no'         => $this->input->post('mobile'),
                'postcode'           => $this->input->post('postcode'),
                'address'            => $this->input->post('address'),
                'suburb'             => $this->input->post('suburb'),
                'membership_type_id' => $this->input->post('membership_type'),
                'activation_code'    => $activation_code,

            );

            if ($userId = $this->member_auth->register($email, $password, $additionalData)) {
                $this->session->set_flashdata('msg', $this->member_auth->get_messages());
                $status = 1;

                $email_data = array(
                    'first_name' => $this->input->post('first_name'),
                    'url_link'   => site_url('activate/' . $userId . '/' . $activation_code),
                );

                if ($status == 1) {
                    $this->load->library('email');
                    $from = "info@rewardsclub.com.au";

                    $message = $this->load->view('frontend/email_template', $email_data, true);
                    // . anchor('activate/' . $userId . '/' . $activation_code, 'click here To activate') .
                    // "";
                    //$message = "hello";
                    // echo $message; exit;
                    $this->email->to($_POST['email']);
                    $this->email->from($from, "Rewards Club");
                    $this->email->subject("welcome to rewards club");
                    $this->email->message($message);
                    $this->email->send();

                    // echo $this->email->print_debugger();
                }
                echo json_encode(array('status' => $status));
            } else {
                $status = 0;
                $msg    = $this->member_auth->get_errors();
                echo json_encode(array('status' => $status, 'msg' => $msg));
            }
        } else {
            $status = 0;
            $msg    = validation_errors();
            echo json_encode(array('status' => $status, 'msg' => $msg));
        }
    }

    public function edit_profile()
    {
        $status     = 0;
        $this->data = array();

        $this->form_validation->set_rules(array(
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'xss_clean|trim|required|max_length[200]',
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'xss_clean|trim|required|max_length[200]',
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'valid_email|xss_clean|trim|required|max_length[200]',
            ),
            array(
                'field' => 'postcode',
                'label' => 'Postcode',
                'rules' => 'xss_clean|trim|required|max_length[10]',
            ),
            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'xss_clean|trim|max_length[500]',
            ),
        ));

        if ($this->form_validation->run() == true) {
            // echo 'hi';die;
            $id                = $this->current_member->id;
            $email             = $_POST['email'];
            $first_name        = $this->input->post('first_name');
            $last_name         = $this->input->post('last_name');
            $member_contact_no = $this->input->post('mobile');
            $member_postcode   = $this->input->post('postcode');
            $member_address    = $this->input->post('address');
            $suburb            = $this->input->post('suburb');

            if ($userId = $this->member_auth->edit_profile($id, $email, $first_name, $last_name, $member_contact_no, $member_postcode, $member_address, $suburb) == true) {
                //echo 'hi';die;
                //                $this->data['msg'] = $this->member_auth->get_message();
                //                $this->session->set_flashdata('msg', $this->member_auth->get_messages());
                $this->session->set_userdata('customLocation', $suburb);
                $status = 1;

//                redirect('user/login');

                echo json_encode(array('status' => $status));
            } else {
                //echo $this->db->last_query();die;

                $status = 0;
                $msg    = $this->member_auth->get_errors();
                echo json_encode(array('status' => $status, 'msg' => $msg));
            }
        } else {
            $status = 0;
            $msg    = validation_errors();
            // echo $msg;die;
            echo json_encode(array('status' => $status, 'msg' => $msg));
        }
    }

    public function change_password()
    {
        $insrtdb    = $this->load->database('webservice', true);
        $status     = 0;
        $this->data = array();
        $this->form_validation->set_rules(array(
            array(
                'field' => 'old_password',
                'label' => 'Old Password',
                'rules' => 'trim|xss_clean|required|max_length[50]',
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'trim|xss_clean|required|max_length[50]|matches[password]',
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'trim|xss_clean|required|max_length[50]',
            ),
        ));
        if ($this->form_validation->run() == true) {
            $id = $this->current_member->id;

            /* $password = encryptPassword($this->input->post('password'));
            $old_password = encryptPassword($this->input->post('old_password'));*/

            $password     = $this->input->post('password');
            $old_password = $this->input->post('old_password');

            if ($this->member_auth->change_password($old_password, $id)) {
                //update password
                $this->member_auth->new_pasword_hash($password, $id);
                $msg = "Changed Sucessfully";
                echo json_encode(array('status' => $status, 'msg' => $msg));
            } else {
                $msg = "Your old password didn't match";
                echo json_encode(array('status' => $status, 'msg' => $msg));
            }

            /* $query = $insrtdb->query("CALL sp_changePassword('$id','$old_password','$password')");

        $insrtdb->reconnect();
        $result = $query->row();
        if ($result->status == 1) {
        $status = 1;
        echo json_encode(array('status' => $status));
        } elseif($result->status == 0) {
        $status = 0;
        $msg = "Your old password didn't match";
        echo json_encode(array('status' => $status, 'msg' => $msg));
        }
        } else {
        $status = 0;
        $msg = validation_errors();
        echo json_encode(array('status' => $status, 'msg' => $msg));
        }*/
        }

    }

    public function logout()
    {
        if ($this->member_auth->logged_in()) {
            $this->member_auth->logout();
            $this->session->set_flashdata('msg', $this->member_auth->get_messages());
            $sess_array = $this->session->all_userdata();
            foreach ($sess_array as $key => $val) {
                if ($key != 'session_id'
                    && $key != 'last_activity'
                    && $key != 'ip_address'
                    && $key != 'user_agent') {
                    $this->session->unset_userdata($key);
                }

            }

            $this->session->unset_userdata('latitude');
            $this->session->unset_userdata('longitude');

        }
        redirect('');
    }

    public function forgot_password()
    {

        if ($this->input->is_ajax_request() && $this->input->post()) {
            $insrtdb = $this->load->database('webservice', true);
            // debug($_POST);
            if ($this->_valid_email()) {
                $this->load->library('email');
                $email = $this->input->post('user-email');
                $query = $insrtdb->query("CALL sp_checkmember('$email')");
                $insrtdb->reconnect();

                $result = $query->row();

                if ($result->status == 1) {
                    $token             = $this->member_auth->hashCode(20);
                    $temp_pass         = $this->member_auth->hashCode(8);
                    $encrypt_temp_pass = $this->member_auth->encryptPassword($temp_pass);
                    $url               = site_url("member/reset_password/{$result->member_id}/{$token}");
                    // echo $url;
                    //$this->db->reconnect();
                    $insrtdb = $this->load->database('webservice', true);
                    $insrtdb->query("CALL sp_assigntoken('$email','$token','$encrypt_temp_pass')");
                    $insrtdb->reconnect();

                    $to            = $email;
                    $subject       = "Password Reset";
                    $password_data = array(
                        'password_link' => $url,
                    );
                    $message = $this->load->view('frontend/email_template', $password_data, true);
//                    $message = "<p>Reset Password.</p>";
                    ////                $message .= "<p>Your new password is: ".$temp_pass."</p>";
                    //                    $message .= "<p>Please <a href='{$url}' >click here</a> to change the password.</p>";
                    //                    // $message .= "<label><a href=\"{$url}\" target=\"_blank\">{$url}</a></label>";

                    $from = "info@rewards.com.au";

                    $this->email->to($to)
                        ->from($from, 'Rewards Club App')
                        ->subject($subject)
                        ->message($message);
                    $this->email->send();
                    echo json_encode(array(
                        'status' => 'success',
                    ));
                    exit();

//                    if ($this->email->send()) {
                    //                        echo json_encode(array(
                    //                            'status' => 'success'
                    //                        ));
                    //                        exit();
                    //                    } else {
                    //                        echo json_encode(array(
                    //                            'status' => 'not_sent',
                    //                        ));
                    //                        exit();
                    //                    }
                } else {
                    echo json_encode(array(
                        'status' => 'not_found',
                    ));
                    exit();
                }
            } else {

                echo json_encode(array(
                    'status' => 'error',
                    'errors' => array(
                        'user-email' => form_error('user-email', '<div class= "error show-error">', '</div>'),
                    ))
                );
                exit();
            }
        }
    }

    public function reset_password($user_id, $token)
    {
        $insrtdb = $this->load->database('webservice', true);
        $user    = $insrtdb->where('change_password_token', $token)->where('member_id', $user_id)->get('members')->row(); //pass the code to profile

        if (is_object($user)) {
            $this->load->library(array('form_validation', 'session'));

            $this->form_validation->set_rules('new', 'New Password', 'required|min_length[4]|matches[new_confirm]');
            $this->form_validation->set_rules('new_confirm', "Confirm Password", 'required');

            if ($this->form_validation->run() == false) {
                //display the form
                //set the flash data error message if there is one
                $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

                $this->data['min_password_length'] = 6;
                $this->data['new_password']        = array(
                    'name' => 'new',
                    'id'   => 'new',
                    'type' => 'password',
                );
                $this->data['new_password_confirm'] = array(
                    'name' => 'new_confirm',
                    'id'   => 'new_confirm',
                    'type' => 'password',
                );
            } else {
                $identity = (int) $user_id;

                $new_pass = $this->input->post('new');

                $encrypt_newpass = encryptPassword($new_pass);

                $query = $insrtdb->query("CALL sp_resetPassword('{$identity}','{$encrypt_newpass}')");
                $insrtdb->reconnect();
                $result = $query->row();
                if ($result->status == 1) {

                    $this->data['content'] = "Your password has been changed.";
                } else {

                    $this->data['content'] = "<p>Sorry, your link couldn't be validated.</p>";
                    //$this->data['content'] .= "<p>You can now close this window.</p>";
                }
            }
        } else {

            $this->data['content'] = "<p>Sorry, your link couldn't be validated.</p>";
        }
        $this->template->add_title_segment('Information For Members');

        Template::render('frontend/forgot_password', $this->data);
//        $this->load->view('frontend/forgot_password', $this->data);
    }

    public function _valid_email()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('user-email', 'Email', 'required|valid_email');
        return $this->form_validation->run($this);
    }

//    function member_check($email) {
    //        $result = $this->member_m
    //        if ($result) {
    //            $this->CI->form_validation->set_message('email_check', "The %s field does not have our email.");
    //            return FALSE;
    //        } else {
    //            return TRUE;
    //        }
    //    }

    public function activate($member_id, $activation_code)
    {
        $uid         = (int) $member_id;
        $checkuserid = $this->db->query("select id from users where id = '$uid' and activation_code = '$activation_code'");

        if ($checkuserid->num_rows() > 0) {
            $this->db->update('users', array('active' => '1'), array('id' => $uid));
            $this->db->update('members', array('activation_code' => ''), array('member_id' => $uid));

            $member_email = $this->db->select('email,first_name')->from('users')->where("id = '$uid'")->get();

            $member_email = $member_email->row();
            $this->load->library('email');
            // echo "<pre>"; print_r($member_email); exit;
            $to      = $member_email->email;
            $subject = "Welcome to Rewards Club";

            $message = "<html>
                                <body>
                                    <p><b>Hi {$member_email->first_name}, </b></p>
                                    <p>
                                        Welcome!!!
                                    </p>
                                </body>
                                </html>";

            $from = "info@rewards.com.au";
            $this->email->to($member_email->email)
                ->from($from, "Rewards")
                ->subject($subject)
                ->message($message);

            $this->email->send();

            echo $content = "<p>Your account is activated now.Thank you</p>";
        } else {

            echo $content = "<p>Invalid Link</p>";
        }
    }

}
