<table style="width:100%; max-width:600px; margin:0 auto;" width="600" cellpadding="0" cellspacing="0">

    <tr> <!-- header starts -->
        <td>
            <table style="height:60px; background:#323d45; padding:15px 40px;" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="color:#fff; font-size:16px; font-weight:700;">Rewards Club</td>
                         </tr>
            </table>
        </td>
    </tr> <!-- header ends -->
    <tr> <!-- body container starts -->
        <td style="background:#fff; padding:20px;">
            <table style="background:#f7f7f7; margin:0 auto; padding:10px;" cellpadding="0" cellspacing="0" width="100%">


                <?php  if(isset($first_name)): ?>
                <tr><td style="padding:30px 0 0 50px;"><h1 style="color:#323d45; font-size:20px; font-weight:700;">Hi <?php echo $first_name;?>,</h1></td></tr>
                <?php endif; ?>
                <tr>
                    <?php  if(isset($url_link)): ?>
                    <td>
                        <p style="font-size:14px; color:#323d45; padding:0 40px 0 50px; line-height:25px;">
                            Welcome to Rewards Club! Please activate the account. <br/>

                            <a href="<?php echo $url_link;?>">Click Here</a>
                        
                        </p>

                    </td>

                    <?php endif; ?>


                    <?php  if(isset($password_link)): ?>
                        <td>
                            <p style="font-size:14px; color:#323d45; padding:0 40px 0 50px; line-height:25px;">
                                Welcome to Rewards Club! Please Change Your password<br/>

                                <a href="<?php echo $password_link;?>">Click Here</a>

                            </p>

                        </td>

                    <?php endif; ?>
                </tr>
                <tr>
                    <td style="padding:10px 0 60px 50px;">

                        <p style="font-size:14px; color:#323d45; padding:0 40px 0 0px; line-height:25px;">

                            Thank You <br />
                            <a href="http://rewardsclub.com.au/" style="font-size:16px; color:#f85352;">
                            Rewards CLub Team
                            </a> <br />

                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr> <!-- body container ends -->
    <tr> <!-- footer start -->
        <td style="background:#f7f7f7; height:60px; border-top:1px solid #dcdcdc; padding:0 20px;">
            <p style="font-size:12px; color:#757576;">
                This Information is Private and Confidential and is not to be used by any party other than Rewards Club.
            </p>
        </td>
    </tr> <!-- footer ends -->
</table>
