<?php
if (isset($error) && $error) {
    echo $error;
}
echo form_open();
?>
<div>
    <label>First Name</label><input type="text" name="first_name" value="<?php echo set_value('first_name'); ?>"/>
    <?php echo form_error('first_name'); ?>
</div>
<div>
    <label>last Name</label><input type="text" name="last_name" value="<?php echo set_value('last_name'); ?>"/>
    <?php echo form_error('last_name'); ?>
</div>
<div>
    <label>member_contact_no</label><input type="text" name="mobile" value="<?php echo set_value('mobile'); ?>"/>
    <?php echo form_error('mobile'); ?>
</div>
<div>
    <label>member_postcode</label><input type="text" name="postcode" value="<?php echo set_value('postcode'); ?>"/>
    <?php echo form_error('postcode'); ?>
</div>
<div>
    <label>member_address</label><input type="text" name="address" value="<?php echo set_value('address'); ?>"/>
    <?php echo form_error('address'); ?>
</div>
<div>
    <label>Email</label><input type="email" name="email" value="<?php echo set_value('email'); ?>"/>
    <?php echo form_error('email'); ?>
</div>
<div>
    <label>Password</label><input type="password" name="password" value="<?php echo set_value('password'); ?>"/>
    <?php echo form_error('password'); ?>
</div>
<div>
    <label>Confirm Password</label><input type="password" name="confirm_password" value="<?php echo set_value('confirm_password'); ?>"/>
    <?php echo form_error('confirm_password'); ?>
</div>
<div>
    <label>membership_type_id</label>
    <select name="membership_type">
        <option value="" >---Membership Type---</option>
        <?php
        if (is_array($membership_types) and count($membership_types) > 0):
            foreach ($membership_types as $value):
                ?>
                <option value="<?php echo $value->membership_type_id; ?>" <?php echo set_select('membership_type', $value->membership_type_id); ?>>
                    <?php echo $value->membership_title; ?>
                </option>
                <?php
            endforeach;
        endif;
        ?>

    </select>
    <?php echo form_error('membership_type'); ?>
</div>
<div>
    <input type="submit" name="login" value="Login"/>
</div>
<?php echo form_close(); ?>