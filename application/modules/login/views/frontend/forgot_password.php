<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-12">

            <?php if (isset($content)): ?>
            <?php echo $content ?>
            <?php else: ?><h3><span>Change Password</span></h3>
            <div class=" col-sm-offset-4 col-md-4">
            <?php echo $message?'
                <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>'.$message.'
                </div>':''; 
            ?>
            </div>
            <?php echo form_open('', 'autocomplete="off" class="form-horizontal"'); ?>
            <div class="form-group">
                <label for="new-password" class="col-sm-4 control-label">New Password:</label>
                <div class="col-sm-4">
                    <?php echo form_input($new_password, '', 'class="col-sm-2 form-control"'); ?>
                </div>
            </div>
            <div class="form-group">
                <label for="new-password" class="control-label col-sm-4">Confirm Password:</label>
                <div class="col-sm-4">
                    <?php echo form_input($new_password_confirm, '', 'class="form-control"'); ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-4">
                    <button type="submit" class="btn btn-danger  pull-right">Submit</button>
                </div>
            </div>

            <?php echo form_close(); ?>
            
            <?php endif; ?>
        </div>
    </div>
</div>
    
