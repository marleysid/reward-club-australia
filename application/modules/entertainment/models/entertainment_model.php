<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Entertainment_model extends MY_Model {

    public $_search_total = 0;
    public $_order_by = 0;

    public function __construct() {
        parent::__construct();
        $this->table = 'club_entertainment';
        $this->field_prefix = 'entertainment_';
        $this->log_user = FALSE;
    }
    function entertainment_detail($id){
          $this->db->select('*');
        $this->db->from('club_entertainment');
        $this->db->where("club_id = '$id'");
        $this->db->where("is_deleted = '0'");
        $query = $this->db->get();
        return $query->result();
    }

	function delete_entertainment($id){
	return $query =	$this->db->update('club_entertainment',array('is_deleted' => '1'),array('entertainment_id' => $id));
	
	}
         function contact_detail($id) {
        $this->db->select('contact_info_website,contact_info_phone_number');
        $this->db->from('contact_info');
        $this->db->where("contact_info_type_id = '$id'");
        $this->db->where("contact_info_type = 'entertainment'");
        $query = $this->db->get();
        return $query->row();
    }
 
}
