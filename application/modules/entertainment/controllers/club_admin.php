<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Club_admin extends Club_Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('entertainment_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $user = $this->ion_auth->user()->row(); 
        $this->data['entertainment'] = $this->entertainment_model->entertainment_detail($user->club_id);

        Template::render('club_admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_entertainment(0);
        }

        $this->data['edit'] = FALSE;

           
        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
       Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);

        Template::render('club_admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        //$id || redirect('club_admin/entertainment');

        $this->data['en_detail'] = $this->entertainment_model->get($id);
         $this->data['contact_detail'] = $this->entertainment_model->contact_detail($id);
       
        //$this->data['entertainment'] || redirect('club_admin/entertainment/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_entertainment($id);
        }

      
        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);

        Template::render('club_admin/form', $this->data);
    }

    function _add_edit_entertainment($id) {
        //debug($this->input->post('offer_image_path'));
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
		$data = $this->input->post();
       // debug($this->input->post());die;
        $user = $this->ion_auth->user()->row(); 
       // debug($user->club_id);die;
        $data['club_id'] = $user->club_id;
        
  $this->form_validation->set_rules(
                array(
                    array('field' => 'entertainment_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'entertainment_hero_title', 'label' => 'Hero Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'entertainment_desc', 'label' => ' Description', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'entertainment_schedule', 'label' => ' Schedule', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    
                   
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }
        

        $entertainment_image_path = NULL;
        if (isset($_FILES['entertainment_image']['name']) and $_FILES['entertainment_image']['name']!='') {
            if ($result = upload_image('entertainment_image', config_item('entertainment_image_root'), FALSE)) {
                $image_path = config_item('entertainment_image_path') . $result;
                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
//				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'));
//                }
                $entertainment_image_path = $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }elseif($id == 0) {
            
                $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
                return FALSE;
            
        }

        unset($data['entertainment_image']);

        !$entertainment_image_path || ($data['entertainment_image'] = $entertainment_image_path);

        $data['entertainment_status'] = $this->input->post('entertainment_status') ? '1' : '0';
        
        if ($id == 0) {
             $en_id = $this->entertainment_model->insert($data);
            
             $this->db->insert('contact_info',array( 'contact_info_type_id' => $en_id,
                                                    'contact_info_type' => 'entertainment',
                                                    'contact_info_website' =>  prep_url($this->input->post('website')),
                                                    'contact_info_phone_number' =>$this->input->post('phone_no') ));
           
            // echo $this->db->last_query();die;
            
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_logo = NULL;
            if ($entertainment_image_path) {
                $old_logo = $this->entertainment_model->get($id)->entertainment_image;
            }
            $this->entertainment_model->update($id, $data);
            if ($old_logo and file_exists(config_item('entertainment_image_root') . $old_logo))
                unlink_file(config_item('entertainment_image_root') . $old_logo);

            $this->db->delete('contact_info',array('contact_info_type_id' => $id,
                                                    'contact_info_type' => 'entertainment'));
            $this->db->insert('contact_info',array( 'contact_info_type_id' => $id,
                                                    'contact_info_type' => 'entertainment',
                                                    'contact_info_website' =>  prep_url($this->input->post('website')),
                                                    'contact_info_phone_number' =>$this->input->post('phone_no') ));
          
            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('club_admin/entertainment/', 'refresh');
    }
    
    

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->entertainment_model->update($id, array('entertainment_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function toggle_review_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->load->model(array('review_model'));
            $this->review_model->update($id, array('status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function delete($id) {
        $old_logo = $this->entertainment_model->get($id)->entertainment_image;

        if ($this->entertainment_model->delete_entertainment($id)) {
            if ($old_logo and file_exists(config_item('entertainment_image_root') . $old_logo))
                unlink_file(config_item('entertainment_image_root') . $old_logo);
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('club_admin/entertainment/', 'refresh');
    }

 

   

}
