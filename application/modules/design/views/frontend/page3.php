<div class="container"> 
    <div class="sub-page-banner-wrap"><img src="<?php echo get_asset('assets/frontend/images/img21.jpg'); ?>"  alt="banner"></div>
    <div class="breadcrumb-wrap">
    	<ul class="breadcrumb-list">
    		<li><a href="#">home</a></li>
    		<li>Club Information</li>
    	</ul>
    </div>
    <div class="myclub-wrap">
        <h3>My Club</h3>
        <div class="myclub-holder">
            <div id="owl-myClub" class="owl-carousel">
                <div class="item">
                    <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img04.jpg'); ?>"  alt="image04"></div>
                    <div class="info-wrap">
                        <strong class="title"><a href="#">Penrith Panthers</a></strong>
                        <span class="location">PENRITH</span>
                    </div>
                </div> 
                <div class="item">
                    <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img05.jpg'); ?>"  alt="image05"></div>
                    <div class="info-wrap">
                        <strong class="title"><a href="#">Old Mate Lappo</a></strong>
                        <span class="location">GLENBROOK</span>
                    </div>
                </div>
                <div class="item">
                    <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img06.jpg'); ?>"  alt="image06"></div>
                    <div class="info-wrap">
                        <strong class="title"><a href="#">Pioneer Tavern</a></strong>
                        <span class="location">SOUTH PENRITH</span>
                    </div>
                </div>
                <div class="item">
                    <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img07.jpg'); ?>"  alt="image07"></div>
                    <div class="info-wrap">
                        <strong class="title"><a href="#">Pioneer Tavern</a></strong>
                        <span class="location">SOUTH PENRITH</span>
                    </div>
                </div> 
                <div class="item">
                    <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img07.jpg'); ?>"  alt="image07"></div>
                    <div class="info-wrap">
                        <strong class="title"><a href="#">Pioneer Tavern</a></strong>
                        <span class="location">SOUTH PENRITH</span>
                    </div>
                </div>  
                <div class="item">
                    <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img07.jpg'); ?>"  alt="image07"></div>
                    <div class="info-wrap">
                        <strong class="title"><a href="#">Pioneer Tavern</a></strong>
                        <span class="location">SOUTH PENRITH</span>
                    </div>
                </div>              
            </div>
            <div class="myclub-content-wrapper">
            	<div class="inner-holder active">
            		<div id="owl-fullSlider" class="owl-carousel">
		                <div class="item">
		                    <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img22.jpg'); ?>"  alt="image04"></div>
		                </div> 
		                <div class="item">
		                    <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img22.jpg'); ?>"  alt="image05"></div>
		                </div>
		                <div class="item">
		                    <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img22.jpg'); ?>"  alt="image06"></div>
		                </div>          
		            </div>
		            <div class="inner-content">
		            	<div class="heading">
			            	<h3>Old Mate Lappo</h3>
			            	<span class="view-website"><a href="#">View Website</a></span>
			            </div>
			            <div class="description-block">
			            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt tellus non eleifend maximus. Nunc vehicula facilisis risus nec bibendum. Mauris consectetur nulla sapien, congue pretium tellus placerat et. Integer nec justo fermentum, blandit magna ultricies, hendrerit leo. Etiam eget lacus aliquam, vulputate tellus id, sollicitudin augue. Nulla hendrerit eget dui convallis aliquet. Ut nulla est, pretium eget egestas at, finibus ac ex.</p>
			            </div>
			            <div class="detail-info-block">
			            	<div class="left-info-block">
			            		<h3>Facilities</h3>
		            			<ul class="facilities">
		            				<li><a href="#">Sports Lounge</a></li>
		            				<li><a href="#">Bottle Shop</a></li>
		            				<li><a href="#">300 Seat Bistro</a></li>
		            				<li><a href="#">TAB Facility</a></li>
		            				<li><a href="#">Coffe Shop</a></li>
		            				<li><a href="#">Events & Perfomers</a></li>
		            				<li><a href="#">Corporate Functions</a></li>
		            			</ul>
			            		<div class="detail-contact-wrap">
			            			<ul class="contact-list">
			            				<li class="mail"><a href="#"><span>icon</span>service@oldmatelappo.com.au</a></li>
			            				<li class="email"><a href="#"><span>icon</span>service@oldmatelappo.com.au</a></li>
			            				<li class="phone-no"><span>icon</span>(02)8262 1910</li>
			            				<li class="location"><span>icon</span><address>132 Stanley Street, Glenbrook 2010</address></li>
			            			</ul>
			            		</div>
			            	</div>
			            	<div class="right-info-block">
			            		<h3>Events & News</h3>
			            		<div id="scrollbar1">
									<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
						            <div class="viewport">
						                <div class="overview">
						            		<ul class="events-news-list">
						            			<li>
						            				<div class="events-date-wrap">
						            					<span class="calendar-icon">icon</span>
						            					<span class="date">21</span>
						            					<span class="month">NOV</span>
						            				</div>
						            				<div class="events-news-detail">
						            					<h4>heading</h4>
						            					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt tellus non eleifend maximus. Nunc vehicula facilisis risus nec bibe ndumlor sit ame estibulum tincidunt tell. </p>
						            					<span class="readmore"><a href="#">Read More</a></span>
						            				</div>
						            			</li>
						            			<li>
						            				<div class="events-date-wrap">
						            					<span class="calendar-icon">icon</span>
						            					<span class="date">21</span>
						            					<span class="month">NOV</span>
						            				</div>
						            				<div class="events-news-detail">
						            					<h4>heading</h4>
						            					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt tellus non eleifend maximus. Nunc vehicula facilisis risus nec bibe ndumlor sit ame estibulum tincidunt tell. </p>
						            					<span class="readmore"><a href="#">Read More</a></span>
						            				</div>
						            			</li>
						            			<li>
						            				<div class="events-date-wrap">
						            					<span class="calendar-icon">icon</span>
						            					<span class="date">21</span>
						            					<span class="month">NOV</span>
						            				</div>
						            				<div class="events-news-detail">
						            					<h4>heading</h4>
						            					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt tellus non eleifend maximus. Nunc vehicula facilisis risus nec bibe ndumlor sit ame estibulum tincidunt tell. </p>
						            					<span class="readmore"><a href="#">Read More</a></span>
						            				</div>
						            			</li>
						            			<li>
						            				<div class="events-date-wrap">
						            					<span class="calendar-icon">icon</span>
						            					<span class="date">21</span>
						            					<span class="month">NOV</span>
						            				</div>
						            				<div class="events-news-detail">
						            					<h4>heading</h4>
						            					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tincidunt tellus non eleifend maximus. Nunc vehicula facilisis risus nec bibe ndumlor sit ame estibulum tincidunt tell. </p>
						            					<span class="readmore"><a href="#">Read More</a></span>
						            				</div>
						            			</li>
						            		</ul>
						            	</div>
						            </div>
							    </div>
			            	</div>
			            </div>
		            </div>
            	</div>
            </div>
        </div>
    </div>
</div>
<!-- media -->
<div class="media-block add">
    <div class="featured-club-news-holder add">
	    <h3><span>Rewards Club News</span></h3>
	    <div id="owl-news" class="owl-carousel club-news">	        
		    <div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
	    	<div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
	    	<div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
	    	<div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
    	</div>
    </div>
</div>

