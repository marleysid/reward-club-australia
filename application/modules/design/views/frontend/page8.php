<div class="container">     
    <div class="breadcrumb-wrap add01">
        <ul class="breadcrumb-list">
            <li><a href="#">home</a></li>
            <li>Discounts</li>
        </ul>
    </div>
    <div class="two-columns-wrapper">
        <div class="left-column-wrapper">
            <form action="#">
                <fieldset>
                    <div class="input-wrap">
                        <label for="word">Keyword</label>
                        <input type="text" id="word" placeholder="Enter Keyword for Deals">
                    </div>
                    <div class="input-wrap">
                        <label for="region">Search a different region</label>
                        <input type="text" id="region" placeholder="Enter Suburb or Postcode">
                    </div>
                    <div class="input-wrap">
                        <label for="distance">Maximum distance</label>
                        <div class="distance-slider">
                            <div class="distance">
                                <span class="nearest">1km</span>
                                <span class="farest">25km</span>
                            </div>
                            <input id="ex1" data-slider-id='ex1Slider' type="text" data-slider-min="0" data-slider-max="25" data-slider-step="1" data-slider-value="14"/>
                        </div>
                    </div>
                    <div class="input-wrap">
                        <h3>Offer category</h3>
                        <div class="category-wrap">
                            <ul class="category-list">
                                <li class="all">
                                    <input type="checkbox" id="all">
                                    <label for="all"><span></span><strong class="icon">icon</strong><strong>All</strong></label>
                                </li>
                                <li class="automotive">
                                    <input type="checkbox" id="auto">
                                    <label for="auto"><span></span><strong class="icon">icon</strong><strong>automotive</strong></label>
                                </li>
                                <li class="cafe">
                                    <input type="checkbox" id="cafe">
                                    <label for="cafe"><span></span><strong class="icon">icon</strong><strong>Cafe and Takeaway</strong></label>
                                </li>
                                <li class="health">
                                    <input type="checkbox" id="health">
                                    <label for="health"><span></span><strong class="icon">icon</strong><strong>Health and Beauty</strong></label>
                                </li>
                                <li class="holiday">
                                    <input type="checkbox" id="holiday">
                                    <label for="holiday"><span></span><strong class="icon">icon</strong><strong>Holidays</strong></label>
                                </li>
                                <li class="home">
                                    <input type="checkbox" id="home">
                                    <label for="home"><span></span><strong class="icon">icon</strong><strong>Home and Garden</strong></label>
                                </li>
                                <li class="leisure">
                                    <input type="checkbox" id="leisure">
                                    <label for="leisure"><span></span><strong class="icon">icon</strong><strong>Leisure and Adventure</strong></label>
                                </li>
                                <li class="newoffer">
                                    <input type="checkbox" id="offer">
                                    <label for="offer"><span></span><strong class="icon">icon</strong><strong>New Offers</strong></label>
                                </li>
                                <li class="online">
                                    <input type="checkbox" id="online">
                                    <label for="online"><span></span><strong class="icon">icon</strong><strong>Online Shopping</strong></label>
                                </li>
                                <li class="restaurant">
                                    <input type="checkbox" id="restaurant">
                                    <label for="restaurant"><span></span><strong class="icon">icon</strong><strong>Restaurants</strong></label>
                                </li>
                                <li class="shopping">
                                    <input type="checkbox" id="shopping">
                                    <label for="shopping"><span></span><strong class="icon">icon</strong><strong>Shopping and Fashion</strong></label>
                                </li>
                                <li class="special">
                                    <input type="checkbox" id="special">
                                    <label for="special"><span></span><strong class="icon">icon</strong><strong>Special Deals</strong></label>
                                </li>
                                <li class="sports">
                                    <input type="checkbox" id="sports">
                                    <label for="sports"><span></span><strong class="icon">icon</strong><strong>Sports and Leisure Retail</strong></label>
                                </li>
                            </ul>
                        </div>
                        <button tyoe="submit">Search</button>
                    </div>
                </fieldset>
            </form>
        </div>
        <div class="right-column-wrapper">
            <div class="result-heading">
                <div class="total-result"><span>1- 10 of 34 total results</span></div>
                <div class="sorting-options-wrap">
                    <ul class="sorting-list">
                        <li class="grid"><span class="icon">icon</span><a href="#">Grid</a></li>
                        <li class="list active"><span class="icon">icon</span><a href="#">List</a></li>
                        <li class="map"><span class="icon">icon</span><a href="#">Map</a></li>
                    </ul>
                </div>
                <div class="sort-by-wrap">
                    <label for="sort">Sort By</label>
                    <select name="sort" id="sort">
                        <option value="ap">Alphabetical</option>
                        <option value="ap">Alphabetical</option>
                        <option value="ap">Alphabetical</option>
                    </select>
                </div>
            </div>
            <div class="result-text"><span>Rewards Club local discounts are subject to change, are available at listed stores only and may exclude specials and other offers.</span></div>
           
            <div class="result-holder add01">
                
                <div class="result-list-view">
                    <div class="result-list-inner-view">
                        <div class="image-wrap"><img class="price" src="http://localhost/rewards/assets/frontend/images/img35.jpg"></div>
                        <div class="result-info-block">
                            <strong class="title"><a href="#">Domino’s Pizza</a></strong>
                            <span class="discount-text">(25% Discount**) <br> on Traditional Pizzas</span>
                            <span class="subtitle">3 Traditional Pizzas from</span>
                            <span class="price">$31.95*</span>
                        </div>
                    </div>
                    
                    <div class="result-list-button-holder">
                        <span class="view-website"><a href="#">View Website</a></span>
                        <div class="condition-wrap"><span>Conditions: This offer is valid for purchase more than $30.00</span></div>
                    </div>
                </div>


                <div class="participant-wrap">
                    <h3>Participating stores</h3>
                    <div class="participant-list-wrap">
                        <table class="participant-table">
                            <thead>
                                <tr>
                                    <th class="first">#</th>
                                    <th class="second">Suburb</th>
                                    <th class="third">Address</th>
                                    <th class="fourth">Phone Number</th>
                                </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="first">1</td>
                                <td class="second">Lorem ipsum </td>
                                <td class="third">Lorem ipsum </td>
                                <td class="fourth">Lorem ipsum</td>
                            </tr>
                            <tr>
                                <td class="first">2</td>
                                <td class="second">Lorem ipsum </td>
                                <td class="third">Lorem ipsum </td>
                                <td class="fourth">Lorem ipsum</td>
                            </tr>
                            <tr>
                                <td class="first">3</td>
                                <td class="second">Lorem ipsum </td>
                                <td class="third">Lorem ipsum </td>
                                <td class="fourth">Lorem ipsum</td>
                            </tr>
                            <tr>
                                <td class="first">4</td>
                                <td class="second">Lorem ipsum </td>
                                <td class="third">Lorem ipsum </td>
                                <td class="fourth">Lorem ipsum</td>
                            </tr>
                            <tr>
                                <td class="first">5</td>
                                <td class="second">Lorem ipsum </td>
                                <td class="third">Lorem ipsum </td>
                                <td class="fourth">Lorem ipsum</td>
                            </tr>
                            <tr>
                                <td class="first">6</td>
                                <td class="second">Lorem ipsum </td>
                                <td class="third">Lorem ipsum </td>
                                <td class="fourth">Lorem ipsum</td>
                            </tr>
                            <tr>
                                <td class="first">7</td>
                                <td class="second">Lorem ipsum </td>
                                <td class="third">Lorem ipsum </td>
                                <td class="fourth">Lorem ipsum</td>
                            </tr>
                            <tr>
                                <td class="first">8</td>
                                <td class="second">Lorem ipsum </td>
                                <td class="third">Lorem ipsum </td>
                                <td class="fourth">Lorem ipsum</td>
                            </tr>
                            <tr>
                                <td class="first"></td>
                                <td class="second"></td>
                                <td class="third"></td>
                                <td class="fourth"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="print-offer-wrap">
                    <div class="social-sharing-wrap">
                        <span class="twitter"><a href="#">twitter</a></span>
                        <span class="facebook"><a href="#">facebook</a></span>
                        <span class="googleplus"><a href="#">googleplus</a></span>
                    </div>
                    <span class="print-button"><a href="#"><span class="icon">icon</span>Print Offer</a></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="inner-slider-wrapper add01">
    <div id="inner-slider" class="owl-carousel">
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img12.jpg'); ?>"  alt="image02"></div>
        </div> 
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img13.jpg'); ?>"  alt="image03"></div>
        </div>    
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img14.jpg'); ?>"  alt="image03"></div>
        </div>    
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img15.jpg'); ?>"  alt="image03"></div>
        </div>    
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img16.jpg'); ?>"  alt="image03"></div>
        </div>              
    </div>
</div>




