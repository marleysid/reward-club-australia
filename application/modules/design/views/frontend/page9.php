<div class="container"> 
    <div class="sub-page-banner-wrap maps"><img src="<?php echo get_asset('assets/frontend/images/img36.jpg'); ?>"  alt="banner"></div>
    <div class="breadcrumb-wrap">
        <ul class="breadcrumb-list">
            <li><a href="#">home</a></li>
            <li>Information For Suppliers</li>
        </ul>
    </div>
    <div class="information-wrapper">
        <div class="video-wrapper add">
            <strong class="title">The Community Alliance Invitation</strong>
            <div class="company-logo-wrap"><img src="<?php echo get_asset('assets/frontend/images/img37.png'); ?>"  alt="image36"></div>
            <div class="information-detail-wrap add01">
                <p>By providing a special offer to members, your business can receive FREE ongoing advertising to attract thousands of local club members as well as members from other regions.</p>
                <p>Members visiting your business will simply show their valid membership card to receive your special member offer. (Offers are subject to approval and limits apply to the number of similar businesses promoted.) As well as ongoing FREE advertising in club brochures, websites, posters, banners and more, you will also receive FREE staff discount cards so that your employees can enjoy the local discounts for as long as you are advertised in the program.</p>
            </div>            
            <div class="contact-wrap add01"><a href="#" data-toggle="modal" data-target="#submitBusiness">Submit your business</a></div>
        </div>
    </div>
</div>
<div class="features-bubbles-wrap">
    <div class="container">
        <ul class="bubbles-list">
            <li class="leading"><a href="#"><span class="icon">icon</span><span class="text">Leading <br> business <br> alliance</span></a></li>
            <li class="cup"><a href="#"><span class="icon">icon</span><span class="text">Create loyal customers</span></a></li>
            <li class="user"><a href="#"><span class="icon">icon</span><span class="text">Limited competition</span></a></li>
            <li class="message"><a href="#"><span class="icon">icon</span><span class="text">Over 12 years promoting local business</span></a></li>
            <li class="megaphone"><a href="#"><span class="icon">icon</span><span class="text">Free local advertising</span></a></li>
        </ul>
    </div>
</div>
<div class="partner-wrap">
    <h3>Rewards Club Partners</h3>
    <div class="partner-holder">
        <div id="owl-partner" class="owl-carousel">
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img26.jpg'); ?>"  alt="image04"></div>
            </div> 
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img27.jpg'); ?>"  alt="image05"></div>
            </div>
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img28.jpg'); ?>"  alt="image06"></div>
            </div>
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img29.jpg'); ?>"  alt="image07"></div>
            </div> 
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img30.jpg'); ?>"  alt="image07"></div>
            </div> 
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img30.jpg'); ?>"  alt="image07"></div>
            </div>              
        </div> 
    </div>
</div>
<div id="submitBusiness" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
    <form action="#">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Submit your business</h4>
          </div>
          <div class="modal-body">          
            <div class="input-holder">
                <label for="business">Business Name</label>
                <input type="text" id="business">
            </div>
            <div class="input-holder">
                <label for="street">Street Address</label>
                <textarea id="street"></textarea>
            </div>
            <div class="input-holder">
                <label for="post">Postcode</label>
                <input type="text" id="post">
            </div>
            <div class="input-holder">
                <label for="contact">Contact Name</label>
                <input type="text" id="contact">
            </div>
            <div class="input-holder">
                <label for="phone">Phone Number</label>
                <input type="text" id="phone">
            </div>
            <div class="input-holder">
                <label for="emailbox">Email</label>
                <input type="email" id="emailbox">
            </div>
            <div class="input-holder">
                <label for="offer">Offers Available at your business</label>
                <textarea name="" id="offer" cols="30" rows="2"></textarea>
            </div>          
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
          </div>
        </div>
    </form>
  </div>
</div>



