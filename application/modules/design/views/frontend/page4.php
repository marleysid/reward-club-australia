<div class="container"> 
    <div class="sub-page-banner-wrap"><img src="<?php echo get_asset('assets/frontend/images/img23.jpg'); ?>"  alt="banner"></div>
    <div class="breadcrumb-wrap">
    	<ul class="breadcrumb-list">
    		<li><a href="#">home</a></li>
    		<li>Club Information</li>
    	</ul>
    </div>
    <div class="profile-info-wrap">
    	<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img24.jpg'); ?>"  alt="image">   </div>
    	<div class="profile-description-wrap">
    		<h3>Profile</h3>
    		<div class="descrip-wrap">
    			<p>By adding our logo to club membership cards, we help clubs build, activate and retain valuable members through a unique alliance with local businesses.</p>
    			<p>Our VIP offers at almost 1000 businesses, save club members a total of more than $120 million annually. With the best ongoing offers in the market from the most established and respected businesses, it doesn't take long to see how our average annual member saving is $300 and can go well into the thousands.</p>
    			<p>Rewards Club membership is included on your membership card when you join selected local clubs. </p>
    			<p>If you are involved in a Club in Sydney or NSW regional areas, ask us how we can customise Rewards Club and develop a fully co-branded benefits program for your Club members.</p>
    			<p>If there is not a participating club in your area with Rewards Club included with membership, you can join the Rewards Club direct from just $50 for a 12 month membership.</p>
    			<p>By combining the best clubs with the best retailers and delivering great results for both, you end up with the best member benefits program in the market. We welcome feedback from members about your experiences at any time.</p>
    		</div>
    	</div>
    </div>
    <div class="list-wrap">
    	<div class="block-heading">
    		<h3>Manage Facilities</h3>
    		<span class="add-wrap"><a href="#">+ Add Facility</a></span>
    	</div>
    	<div id="scrollbar1">
			<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
            <div class="viewport">
                <div class="overview">
			    	<div class="table-wrap">
			    		<table>
			    			<thead>
			    				<tr>
			    					<th class="first">#</th>
			    					<th class="second">Title</th>
			    					<th class="third">Title</th>
			    					<th class="fourth">Title</th>
			    					<th class="fifth"></th>
			    				</tr>
			    			</thead>
			    			<tbody>
							<tr>
								<td class="first">1</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit" data-toggle="modal" data-target="#myModal">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">2</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">3</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">4</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">5</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">6</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">7</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">8</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">9</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">10</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">11</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">12</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							</tbody>
			    		</table>
			    	</div>
			    </div>
			</div>
		</div>
    </div>
    <div class="list-wrap">
    	<div class="block-heading">
    		<h3>Manage Events</h3>
    		<span class="add-wrap"><a href="#">+ Add Event</a></span>
    	</div>
    	<div id="scrollbar2">
			<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
            <div class="viewport">
                <div class="overview">
			    	<div class="table-wrap">
			    		<table>
			    			<thead>
			    				<tr>
			    					<th class="first">#</th>
			    					<th class="second">Title</th>
			    					<th class="third">Title</th>
			    					<th class="fourth">Title</th>
			    					<th class="fifth"></th>
			    				</tr>
			    			</thead>
			    			<tbody>
							<tr>
								<td class="first">1</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">2</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">3</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">4</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">5</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">6</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">7</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">8</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">9</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">10</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">11</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">12</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							</tbody>
			    		</table>
			    	</div>
			    </div>
			</div>
		</div>
    </div>
    <div class="list-wrap add">
    	<div class="block-heading">
    		<h3>Manage Featured Offers</h3>
    		<span class="add-wrap"><a href="#">+ Add Features</a></span>
    	</div>
    	<div id="scrollbar3">
			<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
            <div class="viewport">
                <div class="overview">
			    	<div class="table-wrap">
			    		<table>
			    			<thead>
			    				<tr>
			    					<th class="first">#</th>
			    					<th class="second">Title</th>
			    					<th class="third">Title</th>
			    					<th class="fourth">Title</th>
			    					<th class="fifth"></th>
			    				</tr>
			    			</thead>
			    			<tbody>
							<tr>
								<td class="first">1</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">2</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">3</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">4</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">5</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">6</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">7</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">8</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">9</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">10</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">11</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							<tr>
							  	<td class="first">12</td>
								<td class="second">Lorem ipsum </td>
								<td class="third">Lorem ipsum </td>
								<td class="fourth">Lorem ipsum dolor sit amet, consectetur.</td>
								<td class="fifth"><span class="edit">edit</span> <span class="delete">delete</span></td>
							</tr>
							</tbody>
			    		</table>
			    	</div>
			    </div>
			</div>
		</div>
    </div>
    <div class="contact-detail-wrap">
    	<div class="block-heading">
    		<h3>Manage Contact Details</h3>
    		<span class="add-wrap"><a href="#">+ Add Contacts</a></span>
    	</div>
    	<div class="contact-detail-inner-wrap">
    		<h4>Old Mate Lappo</h4>
    		<div class="contact-columns-holder">
    			<ul class="contact-list">
    				<li class="mail"><span class="icon">icon</span><a href="#">service@oldmatelappo.com.au</a> <span class="edit"><a href="#">Edit</a></span></li>
    				<li class="email"><span class="icon">icon</span><a href="#">service@oldmatelappo.com.au</a> <span class="edit"><a href="#">Edit</a></span></li>
    				<li class="email"><span class="icon">icon</span><a href="#">oldmatelappo.com.au</a> <span class="edit"><a href="#">Edit</a></span></li>
    				<li class="phone-no"><span class="icon">icon</span><span>(02)8262 1910</span> <span class="edit"><a href="#">Edit</a></span></li>
    				<li class="location"><span class="icon">icon</span><address>132 Stanley Street, Glenbrook 2010</address> <span class="edit"><a href="#">Edit</a></span></li>
    			</ul>
    		</div>
    	</div>
    </div>
</div>
<!-- media -->
<div class="media-block add">
    <div class="featured-club-news-holder add">
	    <h3><span>Rewards Club News</span></h3>
	    <div id="owl-news" class="owl-carousel club-news">	        
		    <div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
	    	<div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
	    	<div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
	    	<div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
    	</div>
    </div>
</div>
<!-- Modal -->
<div class="edit-popup-wrapper">
	<div id="myModal" class="modal fade" role="dialog">
	  	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form action="#">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title">Edit Club Information</h4>
		      </div>
		      <div class="modal-body">	        
				<div class="input-holder">
					<label for="label">Label</label>
					<input type="text" id="label">
				</div>
				<div class="input-holder">
					<label for="condition">condition</label>
					<input type="text" id="condition">
				</div>
				<div class="input-holder">
					<label for="description">description</label>
					<textarea name="" id="description" cols="30" rows="2"></textarea>
				</div>	        
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Update Info</button>
		      </div>
		    </div>
		</form>
	  </div>
	</div>
</div>


