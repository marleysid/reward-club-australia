<div class="container">     
    <div class="breadcrumb-wrap add01">
    	<ul class="breadcrumb-list">
    		<li><a href="#">home</a></li>
    		<li>Discounts</li>
    	</ul>
    </div>
    <div class="two-columns-wrapper">
        <div class="left-column-wrapper">
            <form action="#">
                <fieldset>
                    <div class="input-wrap">
                        <label for="word">Keyword</label>
                        <input type="text" id="word" placeholder="Enter Keyword for Deals">
                    </div>
                    <div class="input-wrap">
                        <label for="region">Search a different region</label>
                        <input type="text" id="region" placeholder="Enter Suburb or Postcode">
                    </div>
                   

                    <div class="input-wrap">
                        <label for="distance">Maximum distance</label>
                        <div class="distance-slider">
                            <div class="distance">
                                <span class="nearest">1km</span>
                                <span class="farest">25km</span>
                            </div>
                            <div class="handler-holder">                                
                                <div id="handler"></div>
                                <div class="handler-value-wrap">
                                  <input type="text" name="distance" id="amount" readonly />
                                </div>
                            </div>
                        </div>
                    </div>

                  

                    <div class="input-wrap">
                        <h3>Offer category</h3>
                        <div class="category-wrap">
                            <ul class="category-list">
                                <li class="all">
                                    <input type="checkbox" id="all">
                                    <label for="all"><span></span><strong class="icon">icon</strong><strong>All</strong></label>
                                </li>
                                <li class="automotive">
                                    <input type="checkbox" id="auto">
                                    <label for="auto"><span></span><strong class="icon">icon</strong><strong>automotive</strong></label>
                                </li>
                                <li class="cafe">
                                    <input type="checkbox" id="cafe">
                                    <label for="cafe"><span></span><strong class="icon">icon</strong><strong>Cafe and Takeaway</strong></label>
                                </li>
                                <li class="health">
                                    <input type="checkbox" id="health">
                                    <label for="health"><span></span><strong class="icon">icon</strong><strong>Health and Beauty</strong></label>
                                </li>
                                <li class="holiday">
                                    <input type="checkbox" id="holiday">
                                    <label for="holiday"><span></span><strong class="icon">icon</strong><strong>Holidays</strong></label>
                                </li>
                                <li class="home">
                                    <input type="checkbox" id="home">
                                    <label for="home"><span></span><strong class="icon">icon</strong><strong>Home and Garden</strong></label>
                                </li>
                                <li class="leisure">
                                    <input type="checkbox" id="leisure">
                                    <label for="leisure"><span></span><strong class="icon">icon</strong><strong>Leisure and Adventure</strong></label>
                                </li>
                                <li class="newoffer">
                                    <input type="checkbox" id="offer">
                                    <label for="offer"><span></span><strong class="icon">icon</strong><strong>New Offers</strong></label>
                                </li>
                                <li class="online">
                                    <input type="checkbox" id="online">
                                    <label for="online"><span></span><strong class="icon">icon</strong><strong>Online Shopping</strong></label>
                                </li>
                                <li class="restaurant">
                                    <input type="checkbox" id="restaurant">
                                    <label for="restaurant"><span></span><strong class="icon">icon</strong><strong>Restaurants</strong></label>
                                </li>
                                <li class="shopping">
                                    <input type="checkbox" id="shopping">
                                    <label for="shopping"><span></span><strong class="icon">icon</strong><strong>Shopping and Fashion</strong></label>
                                </li>
                                <li class="special">
                                    <input type="checkbox" id="special">
                                    <label for="special"><span></span><strong class="icon">icon</strong><strong>Special Deals</strong></label>
                                </li>
                                <li class="sports">
                                    <input type="checkbox" id="sports">
                                    <label for="sports"><span></span><strong class="icon">icon</strong><strong>Sports and Leisure Retail</strong></label>
                                </li>
                            </ul>
                        </div>
                        <button type="submit">Search</button>
                    </div>
                </fieldset>
            </form>
        </div> <!--end of two left wrapper-->



        <div class="right-column-wrapper"> 
            <div class="result-heading">
                <div class="total-result"><span>1- 10 of 34 total results</span></div>
              
                <div class="sorting-options-wrap">
                    <ul class="sorting-list">
                        <li class="grid"><span class="icon">icon</span><a href="#">Grid</a></li>
                        <li class="list"><span class="icon">icon</span><a href="#">List</a></li>
                        <li class="map"><span class="icon">icon</span><a href="#">Map</a></li>
                    </ul>
                </div>
               
                <div class="sort-by-wrap">
                    <label for="sort">Sort By</label>
                    <select name="sort" id="sort">
                        <option value="ap">Alphabetical</option>
                        <option value="ap">Alphabetical</option>
                        <option value="ap">Alphabetical</option>
                    </select>
                </div>
            </div>


            <div class="result-text"><span>Rewards Club local discounts are subject to change, are available at listed stores only and may exclude specials and other offers.</span></div>
            <div class="result-holder">
                <ul class="result-item-list">
                    <li>
                        <div class="img-wrap">
                            <a href="#"><img src="<?php echo get_asset('assets/frontend/images/img32.jpg'); ?>"  alt="image32"></a>
                            <img class="price" src="http://localhost/rewards/assets/frontend/images/price-img.png">
                            <span class="prc-text">50%<br>OFF</span>
                        </div>
                        <div class="info-wrap">
                            <span>Subway super saver Tuesday.</span>
                        </div>
                    </li>
                    <li>
                        <div class="img-wrap">
                            <a href="#"><img src="<?php echo get_asset('assets/frontend/images/img33.jpg'); ?>"  alt="image32"></a>
                            <img class="price" src="http://localhost/rewards/assets/frontend/images/price-img.png">
                            <span class="prc-text">30%<br>OFF</span>
                        </div>
                        <div class="info-wrap">
                            <span>Subway super saver Tuesday.</span>
                        </div>
                    </li>
                    <li>
                        <div class="img-wrap">
                            <a href="#"><img src="<?php echo get_asset('assets/frontend/images/img34.jpg'); ?>"  alt="image32"></a>
                            <img class="price" src="http://localhost/rewards/assets/frontend/images/price-img.png">
                            <span class="prc-text">best<br>price</span>
                        </div>
                        <div class="info-wrap">
                            <span>Subway super saver Tuesday.</span>
                        </div>
                    </li>
                    <li>
                        <div class="img-wrap">
                            <a href="#"><img src="<?php echo get_asset('assets/frontend/images/img32.jpg'); ?>"  alt="image32"></a>
                            <img class="price" src="http://localhost/rewards/assets/frontend/images/price-img.png">
                            <span class="prc-text">50%<br>OFF</span>
                        </div>
                        <div class="info-wrap">
                            <span>Subway super saver Tuesday.</span>
                        </div>
                    </li>
                    <li>
                        <div class="img-wrap">
                            <a href="#"><img src="<?php echo get_asset('assets/frontend/images/img33.jpg'); ?>"  alt="image32"></a>
                            <img class="price" src="http://localhost/rewards/assets/frontend/images/price-img.png">
                            <span class="prc-text">30%<br>OFF</span>
                        </div>
                        <div class="info-wrap">
                            <span>Subway super saver Tuesday.</span>
                        </div>
                    </li>
                    <li>
                        <div class="img-wrap">
                            <a href="#"><img src="<?php echo get_asset('assets/frontend/images/img34.jpg'); ?>"  alt="image32"></a>
                            <img class="price" src="http://localhost/rewards/assets/frontend/images/price-img.png">
                            <span class="prc-text">best<br>price</span>
                        </div>
                        <div class="info-wrap">
                            <span>Subway super saver Tuesday.</span>
                        </div>
                    </li>
                    <li>
                        <div class="img-wrap">
                            <a href="#"><img src="<?php echo get_asset('assets/frontend/images/img32.jpg'); ?>"  alt="image32"></a>
                            <img class="price" src="http://localhost/rewards/assets/frontend/images/price-img.png">
                            <span class="prc-text">50%<br>OFF</span>
                        </div>
                        <div class="info-wrap">
                            <span>Subway super saver Tuesday.</span>
                        </div>
                    </li>
                    <li>
                        <div class="img-wrap">
                            <a href="#"><img src="<?php echo get_asset('assets/frontend/images/img33.jpg'); ?>"  alt="image32"></a>
                            <img class="price" src="http://localhost/rewards/assets/frontend/images/price-img.png">
                            <span class="prc-text">30%<br>OFF</span>
                        </div>
                        <div class="info-wrap">
                            <span>Subway super saver Tuesday.</span>
                        </div>
                    </li>
                    <li>
                        <div class="img-wrap">
                            <a href="#"><img src="<?php echo get_asset('assets/frontend/images/img34.jpg'); ?>"  alt="image32"></a>
                            <img class="price" src="http://localhost/rewards/assets/frontend/images/price-img.png">
                            <span class="prc-text">best<br>price</span>
                        </div>
                        <div class="info-wrap">
                            <span>Subway super saver Tuesday.</span>
                        </div>
                    </li>
                </ul>
            </div>
           

            <div class="pagination-wrap">
                <nav>
                    <ul class="pagination">
                        <li>
                            <a class="prev" href="#" aria-label="Previous">
                            <span aria-hidden="true">prev</span>
                        </a>
                        </li>
                        <li class="active" ><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li>
                            <a class="next" href="#" aria-label="Next">
                            <span aria-hidden="true">next</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div class="pagination-items-option-wrap">
                    <label for="item-option">Items per page</label>
                    <select name="item" id="item-option" class="item-number">
                        <option value="opt1">5</option>
                        <option value="opt2">10</option>
                        <option value="opt3">15</option>
                    </select>
                </div>
            </div> <!--end of pagination-->

            


        </div>  <!--end of right column wrapper-->
    </div> <!--end of two column wrapper-->
</div> <!--end of container-->


<div class="inner-slider-wrapper add01">
    <div id="inner-slider" class="owl-carousel">
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img12.jpg'); ?>"  alt="image02"></div>
        </div> 
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img13.jpg'); ?>"  alt="image03"></div>
        </div>    
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img14.jpg'); ?>"  alt="image03"></div>
        </div>    
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img15.jpg'); ?>"  alt="image03"></div>
        </div>    
        <div class="item">
            <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img16.jpg'); ?>"  alt="image03"></div>
        </div>              
    </div>
</div>




<script type="text/javascript">
    /*function getLeftProperty()
    {
        var leftValue = $('.ui-slider-handle').css('left');
        $('.handler-value-wrap').css('left', leftValue);
    }*/
</script>










