<div class="container"> 
    <div class="sub-page-banner-wrap"><img src="<?php echo get_asset('assets/frontend/images/img09.jpg'); ?>"  alt="banner"></div>
    <div class="breadcrumb-wrap">
    	<ul class="breadcrumb-list">
    		<li><a href="#">home</a></li>
    		<li>Information For Members</li>
    	</ul>
    </div>
    <div class="information-wrapper">
	    <div class="video-wrapper">
			<strong class="title">For Amazing local discountys - Join your local club.</strong>
			<div class="video-holder"><img src="<?php echo get_asset('assets/frontend/images/img10.jpg'); ?>"  alt="video"></div>
			<span class="highlight">Membership at leading local clubs now include Rewards Club local discounts at no extra cost.</span>
			<div class="information-detail-wrap">
				<p>Join or upgrade your membership card at a participating club to receive instant discounts of up to 50% at thousands of local businesses.
Collect your membership card with the red Rewards Club logo and you are good to start saving. Simply show your card at any of the businesses listed in your local club directory or on this website for great ongoing savings all year. Please note that 
all offers are subject to change and may exclude specials and other offers unless stated. Offers are available at participating advertised stores only.</p>
			</div>
			<span class="highlight">Should you have any issues using your card or would like to suggest new businesses you can call us on 1300 305 690 or email us.</span>
			<div class="contact-wrap"><a href="#">Contact Us</a></div>
		</div>
	</div>
</div> 
<div class="joining-local-club-wrap">
	<div class="container">
		<h3>Join Your Local Club</h3>
		<div class="search-result-wrapper">
			<form action="#">
				<div class="club-search-wrap">
					<h4>Search Club's</h4>
					<div class="search-inner-holder">
						<strong class="title">Find club's in your area</strong>
						<div class="input-wrapper">
							<input type="text" placeholder="Enter club's name">
						</div>
						<div class="input-wrapper">
							<input type="text" placeholder="Enter suburb or postcode">
						</div>
						<button type="submit">Search</button>
					</div>
				</div>
				<div class="club-result-wrap">
					<strong class="club-result">Club Results: <span class="result-num">4</span></strong>
					<div class="search-option-wrap">
						<div class="option-wrap">
							<label for="filter">Filter by</label>
							<select name="filter-by" id="filter">
								<option value="op1">op1</option>
								<option value="op2">op2</option>
								<option value="op3">op3</option>
							</select>
						</div>
						<div class="option-wrap">
							<label for="sort">Sort by</label>
							<select name="sort-by" id="sort">
								<option value="near1">Nearest</option>
								<option value="near2">op2</option>
								<option value="near3">op3</option>
							</select>
						</div>
					</div>
					<div class="search-result-inner-holder">
						<ul class="search-result-list">
							<li>
								<div class="search-result-block">
									<div class="image-wrap"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img11.png'); ?>"  alt="logo"></a></div>
									<div class="result-detail-wrap">
										<div class="result-detail-heading">
											<div class="result-title-wrap">
												<strong class="title"><a href="#">Club liverpool NSW</a></strong>
												<strong class="sub-title">Ex Servos Club</strong>
											</div>
											<span class="search-option"><a href="#">Search</a></span>
										</div>
										<div class="result-contact-info-wrap">
											<div class="address"><address>185 George Street Liverpool NSW 2170 </address></div>
											<div class="contact-number-wrap">
												<span class="number-wrap"> +61 2 9822 4555 </span>
												<span class="fax-no-wrap">1 2 9602 9389 </span>
											</div>
											<div class="email-wrap"><a href="sendto:">clubliverpool.com.au</a></div>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="search-result-block">
									<div class="image-wrap"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img11.png'); ?>"  alt="logo"></a></div>
									<div class="result-detail-wrap">
										<div class="result-detail-heading">
											<div class="result-title-wrap">
												<strong class="title"><a href="#">Club liverpool NSW</a></strong>
												<strong class="sub-title">Ex Servos Club</strong>
											</div>
											<span class="search-option"><a href="#">Search</a></span>
										</div>
										<div class="result-contact-info-wrap">
											<div class="address"><address>185 George Street Liverpool NSW 2170 </address></div>
											<div class="contact-number-wrap">
												<span class="number-wrap"> +61 2 9822 4555 </span>
												<span class="fax-no-wrap">1 2 9602 9389 </span>
											</div>
											<div class="email-wrap"><a href="sendto:">clubliverpool.com.au</a></div>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="search-result-block">
									<div class="image-wrap"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img11.png'); ?>"  alt="logo"></a></div>
									<div class="result-detail-wrap">
										<div class="result-detail-heading">
											<div class="result-title-wrap">
												<strong class="title"><a href="#">Club liverpool NSW</a></strong>
												<strong class="sub-title">Ex Servos Club</strong>
											</div>
											<span class="search-option"><a href="#">Search</a></span>
										</div>
										<div class="result-contact-info-wrap">
											<div class="address"><address>185 George Street Liverpool NSW 2170 </address></div>
											<div class="contact-number-wrap">
												<span class="number-wrap"> +61 2 9822 4555 </span>
												<span class="fax-no-wrap">1 2 9602 9389 </span>
											</div>
											<div class="email-wrap"><a href="sendto:">clubliverpool.com.au</a></div>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="search-result-block">
									<div class="image-wrap"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img11.png'); ?>"  alt="logo"></a></div>
									<div class="result-detail-wrap">
										<div class="result-detail-heading">
											<div class="result-title-wrap">
												<strong class="title"><a href="#">Club liverpool NSW</a></strong>
												<strong class="sub-title">Ex Servos Club</strong>
											</div>
											<span class="search-option"><a href="#">Search</a></span>
										</div>
										<div class="result-contact-info-wrap">
											<div class="address"><address>185 George Street Liverpool NSW 2170 </address></div>
											<div class="contact-number-wrap">
												<span class="number-wrap"> +61 2 9822 4555 </span>
												<span class="fax-no-wrap">1 2 9602 9389 </span>
											</div>
											<div class="email-wrap"><a href="sendto:">clubliverpool.com.au</a></div>
										</div>
									</div>
								</div>
							</li>
						</ul>
						<div class="pagination-wrap">
							<nav>
								<ul class="pagination">
									<li>
										<a class="prev" href="#" aria-label="Previous">
										<span aria-hidden="true">prev</span>
									</a>
									</li>
									<li class="active" ><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li>
										<a class="next" href="#" aria-label="Next">
										<span aria-hidden="true">next</span>
										</a>
									</li>
								</ul>
							</nav>
							<div class="pagination-items-option-wrap">
								<label for="item-option">Items per page</label>
								<select name="item" id="item-option" class="item-number">
									<option value="opt1">5</option>
									<option value="opt2">10</option>
									<option value="opt3">15</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="inner-slider-wrapper">
	<div id="inner-slider" class="owl-carousel">
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img12.jpg'); ?>"  alt="image02"></div>
		</div> 
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img13.jpg'); ?>"  alt="image03"></div>
		</div>    
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img14.jpg'); ?>"  alt="image03"></div>
		</div>    
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img15.jpg'); ?>"  alt="image03"></div>
		</div>    
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img16.jpg'); ?>"  alt="image03"></div>
		</div>              
	</div>
</div>

