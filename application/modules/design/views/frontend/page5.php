<div class="container"> 
    <div class="sub-page-banner-wrap maps"><img src="<?php echo get_asset('assets/frontend/images/img25.jpg'); ?>"  alt="banner"></div>
    <div class="breadcrumb-wrap">
    	<ul class="breadcrumb-list">
    		<li><a href="#">home</a></li>
    		<li>Contact</li>
    	</ul>
    </div>
    <div class="contact-information-wrap">
    	<h3>Get in touch</h3>
    	<span class="info">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto</span>
    	<div class="contact-inner-holder">
    		<div class="contact-left-block">    			
    			<form action="#" class="contact">
    				<fieldset>
    					<div class="input-row">
    						<input type="text" placeholder="Name">
    						<input type="text" placeholder="Address">
    					</div>
    					<div class="input-row">
    						<input type="text" placeholder="Contact no.">
    						<input type="email" placeholder="Email address">
    					</div>
    					<span>Choose a sample promotional & Support Material you want to enquire about:</span>
    					<div class="input-row">
    						<select class="type-select">
    							<option value="opt1">option1</option>
    							<option value="opt2">option2</option>
    							<option value="opt3">option3</option>
    							<option value="opt4">option4</option>
    							<option value="opt5">option5</option>
    						</select>
    					</div>
    					<div class="input-row">
    						<input class="full" type="text" placeholder="Subject">
    					</div>
    					<div class="input-row">
    						<textarea name="" id="" cols="30" rows="10" placeholder="Message"></textarea>
    					</div>
    					<button type="submit">Search</button>
    				</fieldset>
    			</form>
    		</div>
    		<div class="contact-right-block"></div>
    	</div>
    </div>
</div>
<div class="partner-wrap">
    <h3>Rewards Club Partners</h3>
    <div class="partner-holder">
        <div id="owl-partner" class="owl-carousel">
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img26.jpg'); ?>"  alt="image04"></div>
            </div> 
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img27.jpg'); ?>"  alt="image05"></div>
            </div>
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img28.jpg'); ?>"  alt="image06"></div>
            </div>
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img29.jpg'); ?>"  alt="image07"></div>
            </div> 
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img30.jpg'); ?>"  alt="image07"></div>
            </div> 
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img30.jpg'); ?>"  alt="image07"></div>
            </div>              
        </div> 
    </div>
</div>




