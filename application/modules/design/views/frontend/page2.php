<div class="container"> 
    <div class="sub-page-banner-wrap"><img src="<?php echo get_asset('assets/frontend/images/img17.jpg'); ?>"  alt="banner"></div>
    <div class="breadcrumb-wrap">
    	<ul class="breadcrumb-list">
    		<li><a href="#">home</a></li>
    		<li>Information For Members</li>
    	</ul>
    </div>
    <div class="information-wrapper">
	    <div class="video-wrapper add">
			<strong class="title">For Amazing local discounts – show your Club local discounts card.</strong>
			<span class="highlight">Rewards Club local discounts and offers are included on your Club membership card.</span>
			<div class="information-detail-wrap">
				<p>Your Club membership card with the red Rewards Club logo is ready to go. Simply show your card at any of the businesses listed in your local club directory or on this website for great ongoing savings all year. Please note that all offers are subject to change and may exclude specials and other offers unless stated. Offers are available at participating advertised stores only.</p>
			</div>
			<span class="highlight">Should you have any issues using your card or would like to suggest new businesses you can call us on 1300 305 690 or email us.</span>
			<div class="contact-wrap"><a href="#">Contact Us</a></div>
		</div>
	</div>
</div> 
<div class="joining-local-club-wrap add">
	<div class="container">
		<h3>Notifications</h3>
		<div class="search-result-wrapper add">
			<form action="#">
				<div class="club-search-wrap add">
					<h4>Find clubs in your area</h4>
					<div class="search-inner-holder">
						<strong class="title">Add another Club</strong>
						<div class="input-wrapper">
							<input type="text" placeholder="Suburb or Postcode">
						</div>
						<div class="input-wrapper">
							<input type="text" placeholder="Type Club Name">
						</div>
						<button type="submit">Search</button>
					</div>
				</div>
				<div class="club-result-wrap add">
					<strong class="club-result">Notifications <span class="result-num">(124)</span></strong>
					<div class="search-option-wrap">
						<div class="option-wrap">
							<label for="filter">Filter by</label>
							<select name="filter-by" id="filter">
								<option value="op1">op1</option>
								<option value="op2">op2</option>
								<option value="op3">op3</option>
							</select>
						</div>
						<div class="option-wrap">
							<label for="sort">Sort by</label>
							<select name="sort-by" id="sort">
								<option value="near1">Nearest</option>
								<option value="near2">op2</option>
								<option value="near3">op3</option>
							</select>
						</div>
					</div>
					<div class="search-result-inner-holder">
						<div class="choices-wrap">
							<ul class="choices-list">
								<li><a href="#">Mark as unread</a></li>
								<li><a href="#">Show most recent</a></li>
								<li><a href="#">More</a></li>
							</ul>
							<span class="recycle-block"><a href="#">delete</a></span>
						</div>
						<div class="notification-wrapper">
							<div id="scrollbar1">
								<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
					            <div class="viewport">
					                <div class="overview">
										<ul class="notification-list">
											<li>
												<input type="checkbox" id="opt1" name="notification">
												<label for="opt1">
													<span></span>
													<strong class="company-logo"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img18.png'); ?>"  alt="logo-company"></a></strong>
													<strong class="company-detail-block">
														<strong class="company-detail-holder">
															<strong class="company-name"><a href="#">Dominos Pizza</a></strong>
															<strong class="detail-info"><p>25% Discount on all pizzas this Saturday. Check our great range of Pizza’s today, you’ll love em!</p></strong>
														</strong>
														<strong class="contact-company print"><a href="#">email</a></strong>
													</strong>										
												</label>
											</li>
											<li>
												<input type="checkbox" id="opt2" name="notification">
												<label for="opt2">
													<span></span>
													<strong class="company-logo"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img19.png'); ?>"  alt="logo-company"></a></strong>
													<strong class="company-detail-block">
														<strong class="company-detail-holder">
															<strong class="company-name"><a href="#">McDonald’s</a></strong>
															<strong class="detail-info"><p>25% Discount on all pizzas this Saturday. Check our great range of Pizza’s today, you’ll love em!</p></strong>
														</strong>
														<strong class="contact-company mail"><a href="#">email</a></strong>
													</strong>										
												</label>
											</li>
											<li>
												<input type="checkbox" id="opt3" name="notification">
												<label for="opt3">
													<span></span>
													<strong class="company-logo"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img20.png'); ?>"  alt="logo-company"></a></strong>
													<strong class="company-detail-block">
														<strong class="company-detail-holder">
															<strong class="company-name"><a href="#">McDonald’s</a></strong>
															<strong class="detail-info"><p>25% Discount on all pizzas this Saturday. Check our great range of Pizza’s today, you’ll love em!</p></strong>
														</strong>
														<strong class="contact-company mail"><a href="#">email</a></strong>
													</strong>										
												</label>
											</li>
											<li>
												<input type="checkbox" id="opt4" name="notification">
												<label for="opt4">
													<span></span>
													<strong class="company-logo"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img18.png'); ?>"  alt="logo-company"></a></strong>
													<strong class="company-detail-block">
														<strong class="company-detail-holder">
															<strong class="company-name"><a href="#">Dominos Pizza</a></strong>
															<strong class="detail-info"><p>25% Discount on all pizzas this Saturday. Check our great range of Pizza’s today, you’ll love em!</p></strong>
														</strong>
														<strong class="contact-company mail"><a href="#">email</a></strong>
													</strong>										
												</label>
											</li>
											<li>
												<input type="checkbox" id="opt5" name="notification">
												<label for="opt5">
													<span></span>
													<strong class="company-logo"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img19.png'); ?>"  alt="logo-company"></a></strong>
													<strong class="company-detail-block">
														<strong class="company-detail-holder">
															<strong class="company-name"><a href="#">McDonald’s</a></strong>
															<strong class="detail-info"><p>25% Discount on all pizzas this Saturday. Check our great range of Pizza’s today, you’ll love em!</p></strong>
														</strong>
														<strong class="contact-company mail"><a href="#">email</a></strong>
													</strong>										
												</label>
											</li>
											<li>
												<input type="checkbox" id="opt6" name="notification">
												<label for="opt6">
													<span></span>
													<strong class="company-logo"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img20.png'); ?>"  alt="logo-company"></a></strong>
													<strong class="company-detail-block">
														<strong class="company-detail-holder">
															<strong class="company-name"><a href="#">McDonald’s</a></strong>
															<strong class="detail-info"><p>25% Discount on all pizzas this Saturday. Check our great range of Pizza’s today, you’ll love em!</p></strong>
														</strong>
														<strong class="contact-company mail"><a href="#">email</a></strong>
													</strong>										
												</label>
											</li>
											<li>
												<input type="checkbox" id="opt7" name="notification">
												<label for="opt7">
													<span></span>
													<strong class="company-logo"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img18.png'); ?>"  alt="logo-company"></a></strong>
													<strong class="company-detail-block">
														<strong class="company-detail-holder">
															<strong class="company-name"><a href="#">Dominos Pizza</a></strong>
															<strong class="detail-info"><p>25% Discount on all pizzas this Saturday. Check our great range of Pizza’s today, you’ll love em!</p></strong>
														</strong>
														<strong class="contact-company mail"><a href="#">email</a></strong>
													</strong>										
												</label>
											</li>
											<li>
												<input type="checkbox" id="opt8" name="notification">
												<label for="opt8">
													<span></span>
													<strong class="company-logo"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img19.png'); ?>"  alt="logo-company"></a></strong>
													<strong class="company-detail-block">
														<strong class="company-detail-holder">
															<strong class="company-name"><a href="#">McDonald’s</a></strong>
															<strong class="detail-info"><p>25% Discount on all pizzas this Saturday. Check our great range of Pizza’s today, you’ll love em!</p></strong>
														</strong>
														<strong class="contact-company mail"><a href="#">email</a></strong>
													</strong>										
												</label>
											</li>
											<li>
												<input type="checkbox" id="opt9" name="notification">
												<label for="opt9">
													<span></span>
													<strong class="company-logo"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img20.png'); ?>"  alt="logo-company"></a></strong>
													<strong class="company-detail-block">
														<strong class="company-detail-holder">
															<strong class="company-name"><a href="#">McDonald’s</a></strong>
															<strong class="detail-info"><p>25% Discount on all pizzas this Saturday. Check our great range of Pizza’s today, you’ll love em!</p></strong>
														</strong>
														<strong class="contact-company mail"><a href="#">email</a></strong>
													</strong>										
												</label>
											</li>
										</ul>
									</div>
					            </div>
						    </div>
						</div>
						<div class="pagination-wrap">
							<nav>
								<ul class="pagination">
									<li>
										<a class="prev" href="#" aria-label="Previous">
										<span aria-hidden="true">prev</span>
									</a>
									</li>
									<li class="active" ><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#">3</a></li>
									<li>
										<a class="next" href="#" aria-label="Next">
										<span aria-hidden="true">next</span>
										</a>
									</li>
								</ul>
							</nav>
							<div class="pagination-items-option-wrap">
								<label for="item-option">Items per page</label>
								<select name="item" id="item-option" class="item-number">
									<option value="opt1">5</option>
									<option value="opt2">10</option>
									<option value="opt3">15</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<div class="inner-slider-wrapper">
	<div id="inner-slider" class="owl-carousel">
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img12.jpg'); ?>"  alt="image02"></div>
		</div> 
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img13.jpg'); ?>"  alt="image03"></div>
		</div>    
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img14.jpg'); ?>"  alt="image03"></div>
		</div>    
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img15.jpg'); ?>"  alt="image03"></div>
		</div>    
		<div class="item">
			<div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/img16.jpg'); ?>"  alt="image03"></div>
		</div>              
	</div>
</div>