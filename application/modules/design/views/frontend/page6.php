<div class="container">     
    <div class="breadcrumb-wrap add">
    	<ul class="breadcrumb-list">
    		<li><a href="#">home</a></li>
    		<li>Contact</li>
    	</ul>
    </div>
    <div class="contact-information-wrap add">
    	<h3>Contact Us</h3>    	
    	<div class="contact-inner-holder">
    		<div class="contact-left-block">    			
    			<form action="#" class="contact">
    				<fieldset>
    					<div class="input-row">
    						<input type="text" placeholder="Name">
    						<input type="text" placeholder="Address">
    					</div>
    					<div class="input-row add">
    						<input type="text" placeholder="Contact no.">
    						<input type="email" placeholder="Email address">
    					</div>
    					<div class="input-row">
    						<input class="full" type="text" placeholder="Subject">
    					</div>
    					<div class="input-row">
    						<textarea name="" id="" cols="30" rows="10" placeholder="Message"></textarea>
    					</div>
    					<button type="submit" data-target="#offerModal" data-toggle="modal">Submit enquiry</button>
    				</fieldset>
    			</form>
    		</div>
    		<div class="contact-right-block add">
    			<div class="additional-contact">
    				<div class="img-wrap"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/img31.png'); ?>"  alt="banner"></a></div>
    				<strong class="title"><a href="#">Community Alliance</a></strong>
    				<div class="additional-detail">
    					<ul class="contact-list">
    						<li class="location"><span class="icon">icon</span><address>Suite 103, Lvl 1, 270 Pacific Highway, Crows Nest NSW</address></li>
    						<li class="post"><span class="icon">icon</span><address>PO Box 255, Neutral Bay NSW 2089 Australia</address></li>
    						<li class="phone-no"><span class="icon">icon</span>1300 305 690</li>
    						<li class="email"><span class="icon">icon</span><a href="#">info@rewardsclub.com.au</a></li>
    					</ul>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
</div>
<!-- media -->
<div class="media-block">
    <div class="featured-club-news-holder add">
	    <h3><span>Rewards Club News</span></h3>
	    <div id="owl-news" class="owl-carousel club-news">	        
		    <div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
	    	<div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
	    	<div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
	    	<div>
			    <a href=""> <img src="http://localhost/rewards/imager/218-106-1/assets/uploads/news/85af15be1592031ccf5bdcbf47442c2f.jpg" class="img-responsive"></a>
			    <div class="info">
			        <span>03 Apr</span>        
			        <h4><a href="">Rewards Club Now Provide…</a></h4>       
			        <p>Lorem ipsum dolor sit amet, consectetur adipisirewardsng elit, sed do eiusmod… </p>
			        <!--<button class="btn btn-danger">Read More</button>-->
			        <a class="btn btn-danger" href="#">Read more</a>
			    </div>
	    	</div>
    	</div>
    </div>
</div>
<!-- offer-Modal -->
<div class="offer-popup-wrapper">
	<div id="offerModal" class="modal fade" role="dialog">
	  	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form action="#">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
		      	<div class="offer-img-wrap"><img src="<?php echo get_asset('assets/frontend/images/img39.jpg'); ?>"  alt="image39"></div>     
				<div class="offer-content-block">
					<h2>Get the latest offers!</h2>
					<strong class="subtitle">Subscribe to our emails to receive the latest offers near you.</strong>
					<div class="input-holder">
						<label for="name">First Name</label>
						<input type="text" id="name">
					</div>
					<div class="input-holder">
						<label for="postcode">Postcode</label>
						<input type="text" id="postcode">
					</div>
					<div class="input-holder">
						<label for="email">Email</label>
						<input type="email" id="email">
					</div>
				</div>
		      </div>
		      <div class="modal-footer">
		        <button type="button" data-target="#thanksModal" data-toggle="modal" class="btn btn-default" data-dismiss="modal">Submit</button>
		      </div>
		    </div>
		</form>
	  </div>
	</div>
</div>
<!-- offer-Modal -->
<div class="thanks-popup-wrapper">
	<div id="thanksModal" class="modal fade" role="dialog">
	  	<div class="modal-dialog">
	    <!-- Modal content-->
	    <form action="#">
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
		      	<h2>Thanks</h2>
		      	<span>Your enquiry has been submitted. We’ll get back to you within 48 hours. :)</span>
		      </div>
		    </div>
		</form>
	  </div>
	</div>
</div>



