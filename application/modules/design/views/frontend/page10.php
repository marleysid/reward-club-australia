<div class="container"> 
    <div class="sub-page-banner-wrap maps"><img src="<?php echo get_asset('assets/frontend/images/img38.jpg'); ?>"  alt="banner"></div>
    <div class="breadcrumb-wrap">
        <ul class="breadcrumb-list">
            <li><a href="#">home</a></li>
            <li>Information For Clubs</li>
        </ul>
    </div>
    <div class="information-wrapper">
        <div class="video-wrapper add">
            <strong class="title">Does your club card put local businesses “on sale”?</strong>
            <div class="information-detail-wrap add01">
                <p>Excite, Attract, Engage, Retain, Reward and Developyour most valuable asset!<br> Talk to the experts about a sustainablestrategic alliance with local business.</p>
                <p>Whilst traditional points programs work your top 10% harder, our Community Alliance program developsyour entire database with more frequent contact points, massive value and active usage of up to 50%.Join over 50 clubs with over 700,000 members using ongoing member discounts at over 1400 local stores.We  you will be happy with the alliance we build or we won’t charge you a cent!</p>
            </div>            
        </div>
    </div>
</div>
<div class="partner-wrap">
    <h3>Join these leading clubs</h3>
    <div class="partner-holder">
        <div id="owl-partner" class="owl-carousel">
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/club-logo01.png'); ?>"  alt="clublogo"></div>
            </div> 
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/club-logo02.png'); ?>"  alt="clublogo"></div>
            </div>
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/club-logo03.png'); ?>"  alt="clublogo"></div>
            </div>
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/club-logo04.png'); ?>"  alt="clublogo"></div>
            </div> 
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/club-logo05.png'); ?>"  alt="clublogo"></div>
            </div> 
            <div class="item">
                <div class="image-wrap"><img src="<?php echo get_asset('assets/frontend/images/club-logo01.png'); ?>"  alt="clublogo"></div>
            </div>              
        </div> 
    </div>
    <div class="contact-wrap add01"><a href="#" data-toggle="modal" data-target="#clubContact">Contact us about Rewards Club</a></div>
</div>
<div id="clubContact" class="modal fade" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
    <form action="#">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Contact Us</h4>
          </div>
          <div class="modal-body">          
            <div class="input-holder">
                <label for="name">Name of Club</label>
                <input type="text" id="name">
            </div>
            <div class="input-holder">
                <label for="street">Street Address</label>
                <textarea id="street"></textarea>
            </div>
            <div class="input-holder">
                <label for="post">Postcode</label>
                <input type="text" id="post">
            </div>
            <div class="input-holder">
                <label for="contact">Contact Name</label>
                <input type="text" id="contact">
            </div>
            <div class="input-holder">
                <label for="phone">Phone Number</label>
                <input type="text" id="phone">
            </div>
            <div class="input-holder">
                <label for="emailbox">Email</label>
                <input type="email" id="emailbox">
            </div>     
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Submit</button>
          </div>
        </div>
    </form>
  </div>
</div>



