<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Design extends Front_Controller {

    // public $data;

    function __construct() {
        parent::__construct();
        
    }
    

    public function index(){
        $this->template->add_title_segment('Page1');
        Template::render('frontend/page1');
    }
    public function page1(){
        $this->template->add_title_segment('Page1');
        Template::render('frontend/page1');
    }
    public function page2(){
        $this->template->add_title_segment('Page2');
        Template::render('frontend/page2');
    }
    public function page3(){
        $this->template->add_title_segment('Page3');
        Template::render('frontend/page3');
    }
    public function page4(){
        $this->template->add_title_segment('Page4');
        Template::render('frontend/page4');
    }
    public function page5(){
        $this->template->add_title_segment('Page5');
        Template::render('frontend/page5');    
    }
    public function page6(){
        $this->template->add_title_segment('Page6');
        Template::render('frontend/page6');
    }
    public function page7(){
        $this->template->add_title_segment('Page7');
        Template::render('frontend/page7');
    }
    public function page8(){
        $this->template->add_title_segment('Page8');
        Template::render('frontend/page8');
    }
    public function page9(){
        $this->template->add_title_segment('Page9');
        Template::render('frontend/page9');
    }
    public function page10(){
        $this->template->add_title_segment('Page10');
        Template::render('frontend/page10');
    }
}
