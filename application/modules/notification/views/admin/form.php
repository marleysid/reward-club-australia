<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>

		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>

	<div class="body">
		<form class="form-horizontal" method="POST" action="notification/add_notification">

		    <div class="form-group <?php echo form_error('key')?'has-error':''?>">
			<label for="key" class="control-label col-lg-4">Offers</label>
				<div class="col-lg-7">
				<select class="input-sm" name="offers">
				  <?php if($notification_details): ?>
				  <?php foreach($notification_details as $key1): ?>

				  <option value="<?php echo $key1->offer_id?>"><?php echo $key1->offer_title ?></option>
			<?php endforeach ?>
				  <?php endif ?>
			  </select>
</div>

	</div>


	<div class="form-group <?php echo form_error('postcode') ? 'has-error' : '' ?>">
		<label for="faq_title" class="control-label col-lg-3">notification</label>
		<div class="col-lg-7">
			<input type="text" id="notification" placeholder="notification" name="notification" class="form-control" value="" >
			<?php echo form_error('postcode'); ?>
		</div>
	</div>



			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
					<?php echo anchor('admin/notification', 'Cancel', 'class="btn btn-warning"'); ?>
				</div>
			</div>
</form>
	</div>
</div>