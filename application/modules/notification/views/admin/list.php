<style>
    table { white-space: normal;}
</style>
<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage Settings</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
            
		<table class="table table-bordered table-condensed table-hover table-striped dataTable sortableTable " id="category_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Key</th>
				<th>Value</th>

			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all">
			<?php if($settings): ?>
				<?php foreach($settings as $key1=>$val1): ?>
			<tr id="category_<?php echo $val1->offer_id;?>">
				<td><?php echo ++$key1; ?></td>
				<td><?php echo $val1->offer_id; ?></td>
				<td><?php echo $val1->offer_title; ?></td>

			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
		
	</div>
</div>