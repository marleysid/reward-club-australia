<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Notification_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'offers';
        $this->field_prefix = '';
        $this->log_user = FALSE;
    }

    function get_offers(){
        $query = $this->db->select('*')
                          ->from('offers')
                          ->get();
        return $query->result();
    }
  function insert_notification($data)
  {
      $this->db->insert('notification_offer', $data);
  }

}
