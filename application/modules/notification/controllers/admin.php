<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Settings_model $settings_model
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('notification_model');

        $title = "Notification";
        Template::set('title', $title);
        Template::add_title_segment($title);
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {

            $this->data['notification_details'] = $this->notification_model->get_offers();
        Template::render('admin/form', $this->data);
    }

    function _manage_setting() {
        $this->data['settings'] = $this->notification_model->get_offers();
    }

    function add_notification()
    {
         $data = array(
             'offer_id' => $this->input->post("offers"),
             'notification' => $this->input->post("notification")
         );

         $this->notification_model->insert_notification($data);
        $this->session->set_flashdata('success_message', 'Notification Set Successfully.');
        redirect('admin/notification');

    }



}
