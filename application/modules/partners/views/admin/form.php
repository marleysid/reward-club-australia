<div class="box">
    <header>
        <div class="icons"><i class="fa fa-edit"></i></div>
        <h5><?php echo $edit ? 'Edit' : 'Add'; ?> Partners</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
            
            <div class="form-group <?php echo form_error('partners_name') ? 'has-error' : '' ?>">
                <label for="text1" class="control-label col-lg-4">Name *</label>
                <div class="col-lg-8">
                    <input type="text" id="partners_name" placeholder="Name" name="partners_name" class="form-control" value="<?php echo set_value("partners_name", $edit ? $partners->partners_name : ''); ?>" >
                    <?php echo form_error('partners_name'); ?>
                </div>
            </div>
             <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                <label for="partners_icon" class="control-label col-lg-4">Icon *</label>
                <div class="col-lg-8">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select Image</span> 
                            <span class="fileinput-exists">Change</span> 
                            <input type="file" name="partners_icon">
                        </span> 
                        <span class="fileinput-filename"></span> 
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                        <?php echo @$logo_error; ?>
                    </div>
                    <?php if ($edit and file_exists(config_item('partners_image_path') . $partners->partners_icon)): ?>
                        <div>
                            <img src="<?php echo base_url(config_item('partners_image_path') . $partners->partners_icon); ?>" height="64" alt="partners image" class="img-thumbnail">
                            <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                         <?php echo form_error('partners_icon'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
           
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                    <?php echo $edit ? anchor('admin/partners', 'Cancel', 'class="btn btn-warning"') : ''; ?>
                </div>
            </div>
        </form>		
    </div>
</div>