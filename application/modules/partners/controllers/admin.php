<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Category_model $category_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('partners_model');
        $this->load->helper('image_helper');
        
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index($id = 0) {
        $id = (int) $id;
        $this->data['edit'] = FALSE;
        if ($id) {
            $this->data['partners'] = $this->partners_model->get($id);
            //echo $this->db->last_query();die;
            //debug( $this->data['partners_detail']);die;
           // $this->data['partners_detail'] || redirect('admin/partners/');
            $this->data['edit'] = TRUE;
        }
        if ($this->input->post()) {
            $this->_add_edit_partners($id);
        }
       $this->_manage_partners();
        
        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::render('admin/index', $this->data);
    }

    function _manage_partners() {
        $this->data['partners_detail'] = $this->partners_model->get_all(); //$this->category_model->get_category_parent();
//        $this->data['categories'] = $this->category_model->get_all();
    }

    function _add_edit_partners($id) {
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;

        $data = $this->input->post();
if($id) {
    $_POST['partners_id'] = $id;
        $this->form_validation->set_rules(
                array(
                    array('field' => 'partners_name', 'label' => ' Name', 'rules' => 'required|trim|min_length[2]|unique[category.category_name,category.category_id]'.'|xss_clean| '),
                    
                )
        );
} else {
        $this->form_validation->set_rules(
                array(
                    array('field' => 'partners_name', 'label' => ' Name', 'rules' => 'required|trim|min_length[2]|unique[category.category_name]'.'|xss_clean| '),
                    
                )
        );
}
        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }

        $category_image_path = NULL;
   
        if (isset($_FILES['partners_icon']['name']) and $_FILES['partners_icon']['name'] != '') {
            if ($result = upload_image('partners_icon', config_item('partners_image_root'), FALSE)) {
                $image_path = config_item('partners_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
////				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'), TRUE);
//                }
                $category_image_path = $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }elseif($id == 0) {
            
                $this->data['logo_error'] = '<span class="help-block">Icon is required</span>';
                return FALSE;
            
        }

        unset($data['partners_icon']);
        !$category_image_path || ($data['partners_icon'] = $category_image_path);
        
        if ($id == 0) {
            $this->partners_model->insert($data);
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $this->partners_model->update($id, $data);
            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/partners/');
    }

  function delete($id) {
        if ($this->partners_model->delete($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/partners/');
    }

}
