<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Subscription_model extends MY_Model {

   
    public function __construct() {
        parent::__construct();
        $this->table = 'faq';
        $this->field_prefix = 'faq_';
        $this->log_user = FALSE;
    }
    
     function display_subscription(){
        $query = $this->db->select("name,email")
                ->from("newsletter_subscription")
                ->get();
        return $query->result();
    }
    

}
