<div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5>Manage FAQ's</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
            <thead>
                <tr>
                    <th>#</th>
                    <th> FAQ Questions</th>
                    <th>Answers </th>
                    <th style="width:90px;" >Settings</th>
                </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all" >
                <?php if ($faq): ?>
                    <?php foreach ($faq as $key => $val): ?>
                        <tr class="odd">
                            <td><?php echo ++$key; ?></td>
                            <td><?php echo $val->faq_title; ?></td>
                            <td><?php echo $val->faq_desc; ?></td>
                            <td class="center">
                                <a href="<?php echo base_url('admin/faq/edit/' . $val->faq_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
                                <a href="<?php echo base_url('admin/faq/delete/' . $val->faq_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if (!confirm("Are you sure to delete?"))
                                                            return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>

                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>

    </div>
</div>
