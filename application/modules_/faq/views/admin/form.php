<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> FAQ's</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>">
                <div class="form-group <?php echo form_error('faq_title') ? 'has-error' : '' ?>">
                    <label for="faq_title" class="control-label col-lg-3">Questions *</label>
                    <div class="col-lg-7">
                        <input type="text" id="entertainment_title" placeholder="Questions" name="faq_title" class="form-control" value="<?php echo set_value("faq_title", $edit ? $faq_detail->faq_title : ''); ?>" >
                        <?php echo form_error('faq_title'); ?>
                    </div>
                </div>
		
                <div class="form-group <?php echo form_error('faq_desc') ? 'has-error' : '' ?>">
                    <label for="faq_desc" class="control-label col-lg-3"> Answers *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="entertainment_name" placeholder=" Answers" name="faq_desc" class="form-control"><?php echo  set_value("faq_desc", $edit ? $faq_detail->faq_desc : ''); ?></textarea>
                      
<?php echo form_error('faq_desc'); ?>
                    </div>
                </div>
                 
              
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
<?php echo anchor('admin/faq', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>


