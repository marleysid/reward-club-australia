<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Club_admin extends Club_Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('contact_model');
       $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $user = $this->ion_auth->user()->row(); 
        $this->data['contacts'] = $this->contact_model->contact_club_detail($user->club_id);

        Template::render('club_admin/index', $this->data);
    }

   
}
