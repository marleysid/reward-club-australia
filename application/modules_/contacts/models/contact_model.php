<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact_model extends MY_Model {

    public $_search_total = 0;
    public $_order_by = 0;

    public function __construct() {
        parent::__construct();
        $this->table = 'contacts';
        $this->field_prefix = 'contacts_';
        $this->log_user = FALSE;
    }

    function contact_detail() {
        $this->db->select('*');
        $this->db->from('contacts');
        $this->db->where("club_id = '0'");
        $this->db->where("is_deleted = '0'");
        $query = $this->db->get();
        return $query->result();
    }

    function contact_club_detail($id) {
        $this->db->select('*');
        $this->db->from('contacts');
        $this->db->where("club_id = '$id'");
        $this->db->where("is_deleted = '0'");
        $query = $this->db->get();
        return $query->result();
    }

}
