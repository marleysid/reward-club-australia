<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage Contacts</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Email</th>
				<th>Message</th>
				<th>Phone Number</th>
				
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all" >
			<?php if($contacts): ?>
				<?php foreach($contacts as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->contacts_name; ?></td>
				<td><?php echo $val->contacts_email; ?></td>
				<td><?php echo word_limiter($val->contacts_msg,45); ?></td>
                                <td><?php echo $val->contacts_phone_number; ?></td>
				
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>
