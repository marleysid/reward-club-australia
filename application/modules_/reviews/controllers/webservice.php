<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Webservice_model $webservice_model
 */
class Webservice extends Web_Service_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('email');
//		$this->load->model('group_model');
    }
    
    function offer()
    {
        if ($this->input->post()) {
            $offer_id = (int) $this->input->post('offer_id');

            $offset         = (int) $this->input->post('offset');
            $limit          = (int) 5;
            
            if(!$offer_id ) 
                    return $this->response('0000');

            $reviews = $this->db->select(' SQL_CALC_FOUND_ROWS 
                `review_id`
                , `review_type_id` as offer_id
                , `review_rating` as rating
                , `review_title` as title
                , `review` 
                , `reviewer_name` as name
                , DATE_FORMAT(`review_created_date`,"%e %b %Y") as reviewed_date
                ', FALSE)
                    ->where(array('review_type'=>'offer', 'review_type_id'=>$offer_id, 'review_status'=>'1'))
                    ->order_by('review_created_date', 'desc')
                    ->get('reviews', $limit, $offset);
            
        
            $total      = $this->db->query('SELECT FOUND_ROWS() as total')->row()->total;
            $offset     = $offset + $limit;
            $remaining  = $total - $offset;
            $remaining  = $remaining < 0 ? 0 : $remaining;
            $next_link  = ($total > $offset ) ? (current_url().'?'.$_SERVER['QUERY_STRING'].'&offset='.($offset)) : '';
        
            $msg = array(
                    'remaining_reviews' => (int) $remaining,
                    'total_reviews' => (int) $total,
                    'offset' => (int) ( $offset > $total ? $total : $offset ),
                    'reviews' => $reviews->result()
                );
            return $this->response('0001', $msg);
        } else {
            $this->load->view('webservice/offer', array('title' => 'Login'));
        }
        
    }
    
    function deal()
    {
        if ($this->input->post()) {
            $deal_id = (int) $this->input->post('deal_id');

            $offset         = (int) $this->input->post('offset');
            $limit          = (int) 5;
            
            if(!$deal_id ) 
                    return $this->response('0000');

            $reviews = $this->db->select(' SQL_CALC_FOUND_ROWS 
                `review_id`
                , `review_type_id` as deal_id
                , `review_rating` as rating
                , `review_title` as title
                , `review` 
                , `reviewer_name` as name
                , DATE_FORMAT(`review_created_date`,"%e %b %Y") as reviewed_date
                ', FALSE)
                    ->where(array('review_type'=>'deal', 'review_type_id'=>$deal_id, 'review_status'=>'1'))
                    ->order_by('review_created_date', 'desc')
                    ->get('reviews', $limit, $offset);
            
        
            $total      = $this->db->query('SELECT FOUND_ROWS() as total')->row()->total;
            $offset     = $offset + $limit;
            $remaining  = $total - $offset;
            $remaining  = $remaining < 0 ? 0 : $remaining;
            $next_link  = ($total > $offset ) ? (current_url().'?'.$_SERVER['QUERY_STRING'].'&offset='.($offset)) : '';
        
            $msg = array(
                    'remaining_reviews' => (int) $remaining,
                    'total_reviews' => (int) $total,
                    'offset' => (int) ( $offset > $total ? $total : $offset ),
                    'reviews' => $reviews->result()
                );
            return $this->response('0001', $msg);
        } else {
            $this->load->view('webservice/deal', array('title' => 'Login'));
        }
        
    }
    
    function sub_categories()
    {
        if ($this->input->post()) {            
            if ( $category_id = $this->input->post('category_id') ) {
                
                $catIdz = array_filter(explode(',', $category_id));
                $sub_categories = array();
                
                foreach($catIdz as $v) {
                    $query = $this->db->query("CALL sp_getCategory({$v})");
                    $this->parent_id = $v;
                    $sub_categories = array_merge($sub_categories, array_map(function($arg) {
                        $arg->parent_category_id = $this->parent_id;
                        $arg->category_icon = ($arg->category_icon and file_exists(config_item('category_image_path').$arg->category_icon)) ? base_url(config_item('category_image_path').$arg->category_icon) : "";
                        
                        return $arg;
                    }, $query->result()));
                }

                $msg = array(
                    'sub_categories' => $sub_categories
                );
                return $this->response('0001', $msg);
                
            } else {
                return $this->response('0000');
            }
            
        } else {
            $this->load->view('categories', array('title' => 'Get Sub categories'));
        }
        
    }
    
}