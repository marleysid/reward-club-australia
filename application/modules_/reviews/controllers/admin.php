<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('review_model');
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['reviews'] = $this->review_model->offer_review();

        Template::render('admin/index', $this->data);
    }

    function offers($id = 0) {
        $this->data['reviews'] = $this->review_model->offer_review();
        if ($id) {
            $id = $this->uri->segment(4);
            $this->data['reviews'] = $this->review_model->offer_review($id);
        }
        Template::render('admin/index', $this->data);
    }
    
     function deals($id = 0) {
        $this->data['reviews'] = $this->review_model->deal_review();
        if ($id) {
            $id = $this->uri->segment(4);
            $this->data['reviews'] = $this->review_model->deal_review($id);
        }
                  
        Template::render('admin/deals', $this->data);
    }

    function review_detail() {
        $id = $this->uri->segment(4);
        $data = $this->review_model->offer_review_detail($id);
       // echo $this->db->last_query();die;
        $result = '';
        $result = '<table>'
                . '<tr><td><label>Review Title:</label>' . $data->review_title . '</td>'
                . '<tr><td><label>Review Rating:</label>' . $data->review_rating . '</td>'
                . '<tr><td><label>Review :</label>' . $data->review . '</td>'
                . '<tr><td><label>Reviewer:</label>' . $data->reviewer_name . '</td>'
                . '</table>';

        echo $result;
    }

    function offer_review_detail() {
        $id = $this->uri->segement(4);
        $this->data['review'] = $this->review_model->offer_review_detail($id);
        redirect('admin/index');
    }

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->review_model->update($id, array('review_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

}
