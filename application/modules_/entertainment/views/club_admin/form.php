<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> Entertainment</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>" enctype="multipart/form-data" id="append">
                <div class="form-group <?php echo form_error('entertainment_title') ? 'has-error' : '' ?>">
                    <label for="entertainment_title" class="control-label col-lg-3">Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="entertainment_title" placeholder="Title" name="entertainment_title" class="form-control" value="<?php echo set_value("entertainment_title", $edit ? $en_detail->entertainment_title : ''); ?>" >
                        <?php echo form_error('entertainment_title'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('entertainment_hero_title') ? 'has-error' : '' ?>">
                    <label for="entertainment_hero_title" class="control-label col-lg-3">Hero Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="entertainment_hero_title" placeholder="Title" name="entertainment_hero_title" class="form-control" value="<?php echo set_value("entertainment_hero_title", $edit ? $en_detail->entertainment_hero_title : ''); ?>" >
                        <?php echo form_error('entertainment_hero_title'); ?>
                    </div>
                </div>

                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="entertainment_image" class="control-label col-lg-3">Image *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span> 
                                <span class="fileinput-exists">Change</span> 
                                <input type="file" name="entertainment_image">
                            </span> 
                            <span class="fileinput-filename"></span> 
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists(config_item('entertainment_image_path') . $en_detail->entertainment_image)): ?>
                            <div>
                                <img src="<?php echo imager(config_item('entertainment_image_path') . $en_detail->entertainment_image, 128, 128); ?>" alt="entertainment image" class="img-thumbnail">
                                <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                            </div>
                        <?php endif; ?>
                    </div>
                </div><!-- /.form-group -->		  

                <div class="form-group <?php echo form_error('entertainment_name') ? 'has-error' : '' ?>">
                    <label for="entertainment_name" class="control-label col-lg-3"> Name *</label>
                    <div class="col-lg-7">
                        <input type="text" id="entertainment_name" placeholder="Description" name="entertainment_name" class="form-control" value="<?php echo set_value("entertainment_name", $edit ? $en_detail->entertainment_name : ''); ?>" >

                        <?php echo form_error('entertainment_name'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('website') ? 'has-error' : '' ?>">
                    <label for="website" class="control-label col-lg-3"> Website </label>
                    <div class="col-lg-7">
                        <input type="text" id="en_website" placeholder="Website" name="website" class="form-control" value="<?php echo set_value("website", $edit ? $contact_detail->contact_info_website : ''); ?>" >

                        <?php echo form_error('website'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('phone_no') ? 'has-error' : '' ?>">
                    <label for="website" class="control-label col-lg-3"> Phone Number </label>
                    <div class="col-lg-7">
                        <input type="text" id="entertainment_number" placeholder="Number" name="phone_no" class="form-control" value="<?php echo set_value("phone_no", $edit ? $contact_detail->contact_info_phone_number : ''); ?>" >

                        <?php echo form_error('phone_no'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="entertainment_status" class="control-label col-lg-3">Status *</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="entertainment_status" class="switch switch-small"  value="1" <?php echo set_checkbox('entertainment_status', '1', ($edit) ? ($en_detail->entertainment_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                        <?php echo anchor('club_admin/entertainment', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>



