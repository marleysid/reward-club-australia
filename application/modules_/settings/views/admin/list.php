<style>
    table { white-space: normal;}
</style>
<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage Settings</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
            
		<table class="table table-bordered table-condensed table-hover table-striped dataTable sortableTable " id="category_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Key</th>
				<th>Value</th>
				<th style="width:90px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all">
			<?php if($settings): ?>
				<?php foreach($settings as $key1=>$val1): ?>
			<tr id="category_<?php echo $val1->id;?>">
				<td><?php echo ++$key1; ?></td>
				<td><?php echo $val1->key; ?></td>
				<td><?php echo $val1->value; ?></td>
				<td class="center"><a href="<?php echo base_url('admin/settings/'.$val1->id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
				</td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
		
	</div>
</div>