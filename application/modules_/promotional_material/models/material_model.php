<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Material_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'promotional_material';
        $this->field_prefix = 'material_';
        $this->log_user = FALSE;
    }

    function material_detail(){
    $query =     $this->db->select("*")
                          ->from("promotional_material")
                          ->where("material_status = '1'")
                          ->get();
        return $query->result();
                
    }
    
    function delete($id) {
     return   $this->db->delete('promotional_material',array('material_id' => $id));
    }
}