<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('material_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['materials'] = $this->material_model->material_detail();

        Template::render('admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_material(0);
        }

        $this->data['edit'] = FALSE;


        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        


        Template::render('admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $this->data['material_detail'] = $this->material_model->get($id);

         $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_material($id);
        }


        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        


        Template::render('admin/form', $this->data);
    }

    function _add_edit_material($id) {

        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $data = $this->input->post();
        $this->form_validation->set_rules(
                array(
                    array('field' => 'material_name', 'label' => 'Name', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'material_desc', 'label' => 'Description', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'material_status', 'label' => 'Status', 'rules' => 'required|trim|xss_clean')
                    
                     )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }
       $material_image_path = NULL;
        if (isset($_FILES['material_image']['name']) and $_FILES['material_image']['name'] != '') {
            if ($result = upload_image('material_image', config_item('material_image_root'), FALSE)) {
                $image_path = config_item('material_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
////				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'), TRUE);
//                }
                $material_image_path = $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }

        unset($data['material_image']);

        !$material_image_path || ($data['material_image'] = $material_image_path);
        // debug($news_image_path);die;
       
      
        $data['material_status'] = $this->input->post('material_status') ? '1' : '0';

        if ($id == 0) {
            $this->material_model->insert($data);
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_logo = NULL;
            if ($material_image_path) {
                $old_logo = $this->material_model->get($id)->material_image;
            }
            $this->material_model->update($id, $data);
            if ($old_logo and file_exists(config_item('material_image_root') . $old_logo))
                unlink_file(config_item('material_image_root') . $old_logo);


            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/promotional_material/', 'refresh');
    }

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->material_model->update($id, array('material_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }


    function delete($id) {
        $old_logo = $this->material_model->get($id)->material_image;

        if ($this->material_model->delete($id)) {
            if ($old_logo and file_exists(config_item('material_image_root') . $old_logo))
                unlink_file(config_item('material_image_root') . $old_logo);
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/promotional_material/', 'refresh');
    }

}
