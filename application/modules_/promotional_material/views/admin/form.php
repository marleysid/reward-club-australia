<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> Material</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>" enctype="multipart/form-data" id="append">
                <div class="form-group <?php echo form_error('material_name') ? 'has-error' : '' ?>">
                    <label for="material_name" class="control-label col-lg-3">Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="material_name" placeholder="Name" name="material_name" class="form-control" value="<?php echo set_value("material_name", $edit ? $material_detail->material_name : ''); ?>" >
                        <?php echo form_error('material_name'); ?>
                    </div>
                </div>
                

                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="material_image" class="control-label col-lg-3">Image *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span> 
                                <span class="fileinput-exists">Change</span> 
                                <input type="file" name="material_image">
                            </span> 
                            <span class="fileinput-filename"></span> 
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists(config_item('material_image_path') . $material_detail->material_image)): ?>
                            <div>
                                <img src="<?php echo imager(config_item('material_image_path') . $material_detail->material_image, 128, 128); ?>" alt="material image" class="img-thumbnail">
                                </div>
                        <?php endif; ?>
                    </div>
                </div><!-- /.form-group -->		  
                <div class="form-group <?php echo form_error('material_desc') ? 'has-error' : '' ?>">
                    <label for="material_desc" class="control-label col-lg-3">Description *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="material_desc" placeholder="Description" name="material_desc" class="form-control"><?php echo set_value("material_desc", $edit ? $material_detail->material_desc : ''); ?></textarea>
                        <?php echo form_error('material_desc'); ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="material_status" class="control-label col-lg-3">Status *</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="material_status" class="switch switch-small"  value="1" <?php echo set_checkbox('material_status', '1', ($edit) ? ($material_detail->material_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >

                    </div>
                </div>


                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                        <?php echo anchor('admin/promotional_material', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>

