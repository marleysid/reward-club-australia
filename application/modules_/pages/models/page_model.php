<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Page_model extends MY_Model
{
	public function __construct()
	{        
        parent::__construct();
		$this->table = 'pages';
        $this->field_prefix = '';
		$this->log_user = FALSE;
	}
	
}
