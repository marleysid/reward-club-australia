<div class="col-lg-12">
	<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>
		<h5><?php echo $edit ? 'Edit' : 'Add'; ?> club</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data">
		  <div class="form-group <?php echo form_error('club_name')?'has-error':''?>">
			<label for="club_name" class="control-label col-lg-3">Name *</label>
			<div class="col-lg-7">
				<input type="text" id="club_name" placeholder="Name" name="club_name" class="form-control" value="<?php echo set_value("club_name", $edit ? $club_detail->club_name : ''); ?>" >
				<?php echo form_error('club_name'); ?>
			</div>
		  </div>
		  <div class="form-group">
			<label for="region_id" class="control-label col-lg-3">Rewards Region *</label>
			<div class="col-lg-7">
				<?php echo $regions; ?>
				<?php echo form_error('region_id'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group <?php echo form_error('suburb_id')?'has-error':''?>">
			<label for="suburb" class="control-label col-lg-3">Suburb *</label>
			<div class="col-lg-7">
				<input type="text" id="suburb" placeholder="Suburb" name="club_suburb" class="form-control" value="<?php echo set_value("club_suburb", $edit ? $club_detail->suburb : ''); ?>" >
				<input type="hidden" id="suburb_id"  name="suburb_id"  value="<?php echo set_value("suburb_id", $edit ? $club_detail->suburb_id : ''); ?>" >
				<?php echo form_error('suburb_id'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('club_address')?'has-error':''?>">
			<label for="club_address" class="control-label col-lg-3">Street Address</label>
			<div class="col-lg-7">
				<textarea type="text" id="club_address" placeholder="Street Address" name="club_address" class="form-control"  ><?php echo set_value("club_address", $edit ? $club_detail->club_address : ''); ?></textarea>
				<?php echo form_error('club_address'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('club_detail')?'has-error':''?>">
			<label for="club_detail" class="control-label col-lg-3">Club Detail</label>
			<div class="col-lg-7">
				<textarea type="text" id="club_detail" placeholder="Club Detail" name="club_detail" class="form-control"  ><?php echo set_value("club_detail", $edit ? $club_detail->club_detail : ''); ?></textarea>
				<?php echo form_error('club_detail'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('club_website')?'has-error':''?>">
			<label for="club_website" class="control-label col-lg-3">Website</label>
			<div class="col-lg-7">
				<input type="text" id="club_website" placeholder="Website" name="club_website" class="form-control" value="<?php echo set_value("club_website", $edit ? $club_detail->club_website : ''); ?>" >
				<?php echo form_error('club_website'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('club_phone')?'has-error':''?>">
			<label for="club_phone" class="control-label col-lg-3">Phone</label>
			<div class="col-lg-7">
				<input type="text" id="club_phone" placeholder="Phone" name="club_phone" class="form-control" value="<?php echo set_value("club_phone", $edit ? $club_detail->club_phone : ''); ?>" >
				<?php echo form_error('club_phone'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('club_price')?'has-error':''?>">
			<label for="club_price" class="control-label col-lg-3">Club Membership Price *</label>
			<div class="col-lg-7">
				<input type="text" id="club_price" placeholder="Club Membership Price" name="club_price" class="form-control" value="<?php echo set_value("club_price", $edit ? $club_detail->club_price : ''); ?>" >
				<?php echo form_error('club_price'); ?>
			</div>
		  </div>
		  <div class="form-group">
			<label for="club_status" class="control-label col-lg-3">Status *</label>
			<div class="col-lg-7">
				<input type="checkbox" name="club_status" class="switch switch-small"  value="1" <?php echo set_checkbox('club_status', '1', ($edit) ? ($club_detail->club_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >
				<?php echo form_error('club_status'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group">
			  <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
				  <?php echo anchor('admin/clubs', 'Cancel', 'class="btn btn-warning"'); ?>
			  </div>
		  </div>
		</form>		
	</div>
</div>
</div>