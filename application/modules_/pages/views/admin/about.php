<div class="col-lg-12">
	<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>
		<h5>Edit</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data">
		  <div class="form-group <?php echo form_error('page_title')?'has-error':''?>">
			<label for="page_title" class="control-label col-lg-2">Title *</label>
			<div class="col-lg-7">
				<input type="text" id="page_title" placeholder="Page TItle" name="page_title" class="form-control" value="<?php echo set_value("page_title", $about_us_detail->page_title ?: ''); ?>" >
				<?php echo form_error('page_title'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('page_content')?'has-error':''?>">
			<label for="page_content" class="control-label col-lg-2">Content</label>
			<div class="col-lg-10">
				<textarea type="text" id="page_content" placeholder="Content" name="page_content" class="form-control"  ><?php echo set_value("page_content", $about_us_detail->page_content ?: ''); ?></textarea>
				<?php echo display_ckeditor('page_content'); ?>
					<?php echo form_error('page_content'); ?>
			</div>
		  </div>
		  <div class="form-group">
			<label for="page_status" class="control-label col-lg-2">Status *</label>
			<div class="col-lg-7">
				<input type="checkbox" name="page_status" class="switch switch-small"  value="1" <?php echo set_checkbox('page_status', '1', $about_us_detail->page_status ? TRUE : FALSE); ?> data-on-color="success" data-off-color="danger" >
				<?php echo form_error('page_status'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group">
			  <div class="col-sm-offset-2 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
			  </div>
		  </div>
		</form>		
	</div>
</div>
</div>