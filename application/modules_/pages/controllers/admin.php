<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property club_model $club_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('page_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function about() {
        $id = 1;
        if ($this->input->post()) {

            $this->load->library('form_validation');
            
            $data = $this->input->post();
            
            $this->form_validation->set_rules(
                    array(
                        array('field' => 'page_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                        array('field' => 'page_content', 'label' => 'Content', 'rules' => 'trim|xss_clean'),
                        array('field' => 'page_status', 'label' => 'Status', 'rules' => 'trim'),
                    )
            );

            $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

            if ($this->form_validation->run($this) === TRUE) {

                $data['page_status'] = $this->input->post('page_status') ? '1' : '0';
                
                $this->page_model->update($id, $data);
                $this->session->set_flashdata('success_message', 'Data updated successfully.');

                redirect(uri_string());
            }
        }
        $this->load->helper(array('form', 'ckeditor'));
        $this->data['about_us_detail'] = $this->page_model->get($id);

        Template::render('admin/about', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_club(0);
        }

        $this->data['edit'] = FALSE;

        $this->load->model(array('region/region_model'));
        $this->data['regions'] = render_select($this->region_model->get_all(), 'region_id', set_value('region_id', $this->data['edit'] ? $this->data['club_detail']->region_id : FALSE), 'class="form-control" ', 'region_id', 'region_name');

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);

        Template::render('admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('admin/clubs');

        $this->data['club_detail'] = $this->club_model->get($id);
        $this->data['club_detail'] || redirect('admin/club/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_club($id);
        }
        $this->load->model(array('region/region_model'));
        $this->data['regions'] = render_select($this->region_model->get_all(), 'region_id', set_value('region_id', $this->data['edit'] ? $this->data['club_detail']->region_id : FALSE), 'class="form-control" ', 'region_id', 'region_name');

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);

        Template::render('admin/form', $this->data);
    }

    function _add_edit_club($id) {
        $this->load->library('form_validation');
//		$this->form_validation->CI =& $this;
        $data = $this->input->post();
//		debug($data);die;
        $this->form_validation->set_rules(
                array(
                    array('field' => 'club_name', 'label' => 'Name', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'region_id', 'label' => 'Region', 'rules' => 'trim|xss_clean'),
                    array('field' => 'club_suburb', 'label' => 'Suburb', 'rules' => 'trim'),
                    array('field' => 'suburb_id', 'label' => 'Suburb', 'rules' => 'trim|callback__check_suburb'),
                    array('field' => 'club_address', 'label' => 'Street Address', 'rules' => 'trim|xss_clean'),
                    array('field' => 'club_detail', 'label' => 'Detail', 'rules' => 'trim|xss_clean'),
                    array('field' => 'club_website', 'label' => 'Website', 'rules' => 'trim|xss_clean'),
                    array('field' => 'club_phone', 'label' => 'Phone', 'rules' => 'trim|xss_clean'),
                    array('field' => 'club_price', 'label' => 'Club Membership Price ', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'club_status', 'label' => 'Status', 'rules' => 'trim|xss_clean'),
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }

        $data['club_status'] = $this->input->post('club_status') ? '1' : '0';

        if ($id == 0) {
            $this->club_model->insert($data);
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $this->club_model->update($id, $data);
            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/clubs/', 'refresh');
    }

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->club_model->update($id, array('club_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function delete($id) {
        if ($this->club_model->delete($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/clubs/', 'refresh');
    }

    function _check_suburb($str) {
        if (!empty($_POST['club_suburb']) && !empty($str) && $str && $str != 0) {

            return TRUE;
        } else {
            $this->form_validation->set_message('_check_suburb', 'Please select %s from the suggestion.');
            return FALSE;
        }
    }

}
