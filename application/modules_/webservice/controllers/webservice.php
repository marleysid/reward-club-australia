<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Webservice_model $webservice_model
 */
class Webservice extends Web_Service_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('image');
    }

    public function _remap($method, $params = array()) {
        $method = $method;
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        }
        $this->error();
    }

    public function index() {
        redirect('developer_view');
    }

    public function get_categories() {

        $this->response('0001', array('categories' => $this->db
                ->select('category_id, category_title, concat("' . base_url(config_item('category_image_path')) . '/",category_icon) as category_icon, is_deleted', FALSE)
                ->where('is_deleted !=','1', FALSE)
//                ->order_by('category_title')
                ->get('category')->result()));
    }

    function get_data_form() {
        $this->load->view('get_data');
    }

    function get_data() {
//        if ($this->input->get()) {
        $this->load->helper('date');
        $datetime = $this->input->get('datetime');
        
        $all_data = TRUE;

        if ($datetime) {
            $a = new DateTimeZone(date_default_timezone_get());
            $datetime = date('Y-m-d H:i:s', ($datetime + $a->getOffset(new DateTime(date('Y-m-d H:i:s', $datetime)))));
            $all_data = FALSE;
        
        }

        $query = $this->db->query("CALL sp_getUpdatedData({$this->db->escape($datetime)})");

        $result = $query->result();

        $arr = array(
            'categories' => array(),
            'recipe' => array(),
            'attributes' => array(),
            'featured' => array(),
            'cms' => array()
        );
        foreach ($result as $key => $val) {
            switch ($val->type) {
                case 'category':
                    $arr['categories'][] = $val->type_id;
                    break;
                case 'recipe':
                    $arr['recipe'][] = $val->type_id;
                    break;
                case 'attribute':
                    $arr['attributes'][] = $val->type_id;
                    break;
                case 'featured':
                    $arr['featured'][] = $val->type_id;
                    break;
                case 'cms':
                    $arr['cms'][] = $val->type_id;
                    break;
                default :
                    break;
            }
        }
        $arr = array_map(function($arg) {
            return array_unique($arg);
        }, $arr);
        $code = '0001';

        $return = array(
//            'date_time' => strtotime(gmdate('Y-m-d H:i:s')),
            'date_time' => strtotime($this->db->query('select UTC_TIMESTAMP() as datetime')->row()->datetime),
            'categories' => (($arr['categories'] and is_array($arr['categories'])) ? $this->db->select('category_id, category_title, category_status, concat("' . base_url(config_item('category_image_path')) . '/",category_icon) as category_icon, is_deleted', FALSE)->where_in('category_id', $arr['categories'])->where($all_data ? 'is_deleted !=' : '1','1', FALSE)->get('category')->result() : array()),
            'recipe' => //array(),
            ((FALSE or $arr['recipe'] and is_array($arr['recipe'])) ?
                    array_map(function($arg) {
                        $arg->recipe_attributes = array_filter(explode(',', $arg->recipe_attributes));

                        $arg->recipe_images = array_map(function($args) {
                            return ($args and file_exists(FCPATH . $args)) ? imager($args, 480, 270) : '';
                        }, array_filter(explode(',', $arg->recipe_images)));

                        $arg->recipe_ingredient_categories = $this->db->select('
                                        `ic_id` as `id`
                                        , `recipe_id` as `recipe_id`
                                        , `ic_title` as `title`
                                    ')
                                ->where('recipe_id', $arg->recipe_id)
                                ->get('ingredient_categories')
                                ->result();
                        
                        $arg->recipe_ingredients = $this->db->select('
                                    `ingredient_id`
                                    , ingredient_category_id
                                    , `recipe_id`
                                    , `ingredient_name`
                                    , `ingredient_number`
                                    , `ingredient_unit`
                                    ')
                                ->where('recipe_ingredients.recipe_id', $arg->recipe_id)
                                ->get('recipe_ingredients')
                                ->result();
                        
                        return $arg;
                    }, $this->db->select(''
                                            . 'recipe.`recipe_id`'
                                            . ', `category_id`'
                                            . ', `recipe_nutritional_info`'
                                            . ', `recipe_nutritional_info_title`'
                                            . ', `recipe_name`'
                                            . ', `recipe_desc`'
                                            . ', `recipe_methods`'
                                            . ', `recipe_method_note`'
                                            . ', `recipe_served`'
                                            . ', `recipe`.`is_deleted`'
                                           . ', GROUP_CONCAT(DISTINCT attribute_id) AS `recipe_attributes`'
                                            . ', GROUP_CONCAT(DISTINCT ri_image) AS `recipe_images`'
                                            , FALSE)
                                    ->join('recipe_attributes', 'recipe.recipe_id = recipe_attributes.recipe_id', 'left')
                                    ->join('recipe_image', 'recipe.recipe_id = recipe_image.recipe_id', 'left')
                                    ->where_in('recipe.recipe_id', $arr['recipe'])
                                    ->where($all_data ? 'is_deleted !=' : '1','1', FALSE)
                                    ->group_by('recipe.recipe_id')
                                    ->get('recipe')->result()
                    ) : array()),
            'attributes' => (($arr['attributes'] and is_array($arr['attributes'])) ? $this->db->select('attribute_id, attribute_title, attribute_abb')->where_in('attribute_id', $arr['attributes'])->get('attributes')->result() : array()),
            'featured' =>(($arr['featured'] and is_array($arr['featured'])) ? $this->get_featured_recipe($arr['featured']) : array()),// array()//(($arr['featured'] and is_array($arr['cms'])) ? $this->db->select('cms_id, cms_title, cms_desc', FALSE)->where_in('id', $arr['setting'])->get('setting')->result() : array()),
                'cms' => (($arr['cms'] and is_array($arr['cms'])) ? $this->_check_cms_update($arr['cms']) : array() )//(($arr['cms'] and is_array($arr['cms'])) ? $this->db->select('cms_id, cms_title, cms_desc', FALSE)->where_in('id', $arr['setting'])->get('setting')->result() : array()),
        );
                    
        return $this->response($code, $return);
//        } else {
//            $this->load->view('get_data');
//        }
    }
    
    function get_featured_recipe($idz)
    {
        $res = $this->db->select('recipe_id ')->where_in('featured_id', $idz )->get('featured_recipe');
        if($res->num_rows() > 0 ) {
            return $res->result();
        } else {
            $return = new stdClass();
            $return->recipe_id = 0;
            return array(
                    $return
            );
            
        }
    }
    
    function _check_cms_update($cms_idz = Array(1, 2))
    {
        $return = array();
        
        if(is_array($cms_idz) and in_array(1, $cms_idz))
            $return[] = $this->db->select(' cms_id,cms_title,cms_desc,IF( cms_image IS NOT NULL AND cms_image <> "" ,  concat("'.base_url(config_item('cms_image_path')).'/",cms_image), "") as cms_image ', FALSE)->get_where('cms', array('cms_id' => 1))->row();
        
        if(is_array($cms_idz) and in_array(2, $cms_idz))
            $return[] = $this->db->select(' cms_id,cms_title,cms_desc,IF( cms_image IS NOT NULL AND cms_image <> "" ,  concat("'.base_url(config_item('cms_image_path')).'/",cms_image), "") as cms_image ', FALSE)->get_where('cms', array('cms_id' => 2))->row();
        
        return $return;
    }

    function get_recipe() {


        array_map(function($arg) {
            $arg->recipe_attributes = array_filter(explode(',', $arg->recipe_attributes));

            $arg->recipe_images = array_map(function($args) {
                return ($args and file_exists(FCPATH . $args)) ? imager($args, 480, 270) : '';
            }, array_filter(explode(',', $arg->recipe_images)));

                        $arg->recipe_ingredient_categories = $this->db->select('
                                        `ic_id` as `id`
                                        , `recipe_id` as `recipe_id`
                                        , `ic_title` as `title`
                                    ')
                                ->where('recipe_id', $arg->recipe_id)
                                ->get('ingredient_categories')
                                ->result();
                        
            $arg->recipe_ingredients = $this->db->select('
                    `ingredient_id`
                    , ingredient_category_id
                    , `recipe_id`
                    , `ingredient_name`
                    , `ingredient_number`
                    , `ingredient_unit`
                    ')
                    ->where('recipe_ingredients.recipe_id', $arg->recipe_id)
                    ->get('recipe_ingredients')
                    ->result();

            return $arg;
        }, $this->db->select(''
                                . 'recipe.`recipe_id`'
                                . ', `category_id`'
                                . ', `recipe_nutritional_info`'
                                . ', `recipe_nutritional_info_title`'
                                . ', `recipe_name`'
                                . ', `recipe_desc`'
                                . ', `recipe_methods`'
                                . ', `recipe_method_note`'
                                . ', `recipe_served`'
                                . ', `is_deleted`'
                                . ', GROUP_CONCAT(DISTINCT attribute_id) AS `recipe_attributes`'
                                . ', GROUP_CONCAT(DISTINCT ri_image) AS `recipe_images`'
                                , FALSE)
                        ->join('recipe_attributes', 'recipe.recipe_id = recipe_attributes.recipe_id', 'left')
                        ->join('recipe_image', 'recipe.recipe_id = recipe_image.recipe_id', 'left')
                        ->where_in('recipe.recipe_id', $arr['recipe'])
                        ->where('is_deleted !=','1', FALSE)
                        ->group_by('recipe.recipe_id')
                        ->get('recipe')->result()
        );
    }

    function recipe_detail() {
        if ($this->input->post()) {
            $recipe_id = $this->input->post('recipe_id');

            $query = $this->db->query(
                    "SELECT 
                        recipe.`recipe_id`
                        ,recipe.`category_id`
                        ,recipe.`recipe_name`
                        ,recipe.`recipe_desc`
                        ,recipe.`recipe_nutritional_info`
                        , recipe. `recipe_nutritional_info_title`                                            
                        ,recipe.`recipe_methods`
                        , recipe.`recipe_method_note`
                        ,recipe.`recipe_served`
                        ,recipe.is_deleted
                        ,GROUP_CONCAT(attributes.`attribute_id`) AS attributes
                        ,IF( recipe_image.ri_image IS NOT NULL AND recipe_image.ri_image <> '' ,  concat( '".base_url(config_item('recipe_image.ri_image'))."',recipe_image.ri_image), '') as ri_image
                         FROM recipe
                         LEFT JOIN recipe_attributes
                         ON recipe.`recipe_id` = recipe_attributes.`recipe_id`
                         LEFT JOIN attributes
                         ON recipe_attributes.`attribute_id` = attributes.`attribute_id`
                         LEFT JOIN recipe_image
                         ON recipe.`recipe_id` = recipe_image.`recipe_id`
                         WHERE recipe.`recipe_id` = '{$recipe_id}'
                         GROUP BY recipe.`recipe_id`"
                    );
            //echo $this->db->last_query();die;
            if ($recipe_id and $query->num_rows() > 0) {
                $query = $query->row();
                $msg = array('recipe' => array('id' => $query->recipe_id,
                        'category_id' => $query->category_id,
                        'title' => $query->recipe_name,
                        'image' => $query->ri_image,
                        'description' => $query->recipe_desc,
                        'nutritional_information' => $query->recipe_nutritional_info,
                        'nutritional_information_title' => $query->recipe_nutritional_info_title,
                        'methods' => $query->recipe_methods,
                        'method_note' => $query->recipe_method_note,
                        'recipe_served' => $query->recipe_served,
                        'is_deleted' => $query->is_deleted,
                        'recipe_ingredient_categories' => $this->db->select('
                                        `ic_id` as `id`
                                        , `recipe_id` as `recipe_id`
                                        , `ic_title` as `title`
                                    ')
                                ->where('recipe_id', $recipe_id)
                                ->get('ingredient_categories')
                                ->result(),
                        'ingredients' => $this->db->query("select * from recipe_ingredients where recipe_id = '$recipe_id'")->result(),
                        'attributes' => $query->attributes
                ));
                return $this->response('0001', $msg);
            } else {
                return $this->response('0000');
            }
        } else {
            $this->load->view('recipe_detail', array('title' => 'Recipe Detail'));
        }
    }
    
    function ingredient_form() {
        $this->load->view('ingredient_detail', array('title' => 'Ingredient Detail'));
    }
    
    function ingredient_detail(){
        $ing_id = $this->input->get('ingredient_id');
 
        
        $query = $this->db->select("*")
                          ->from("recipe_ingredients")
                          ->where("ingredient_id = '$ing_id'")
                          ->get();
       
        if($ing_id and $query->num_rows() > 0){
             $query = $query->row();
                $msg = array('ingredients' => array('ingredient_id' => $query->ingredient_id,
                        'recipe_id' => $query->recipe_id,
                        'ingredient_name' => $query->ingredient_name,
                        'ingredient_number' => $query->ingredient_number,
                        'ingredient_unit' => $query->ingredient_unit
                        
                ));
                return $this->response('0001', $msg);
        }
        else{
              return $this->response('0000');
        }
    }

    function recipe_form() {
        $this->load->view('recipe_list', array('title' => 'Recipe List'));
    }

    function recipe_list() {

        $category_id = $this->input->get('category_id');
        $offset = $this->input->get('offset');
        $limit = (int) 10;
        $keyword = $this->input->get('keyword');
        $attributes = explode(',',$this->input->get('attributes') );
        $attr = array_filter($attributes);
       

        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if(!empty($attr)){
            $this->db->where_in('recipe_attributes.attribute_id',$attributes);
           
        }
     
        if($keyword){
            $this->db->like('recipe_name',$keyword);
        }
        
        $recipe_list = $this->db->select(' SQL_CALC_FOUND_ROWS 
                recipe.recipe_id
                , recipe.recipe_name
                ,recipe.is_deleted
                ,GROUP_CONCAT(attributes.attribute_id) AS attributes
                , GROUP_CONCAT(DISTINCT ri_image) AS `recipe_images`
                
                ', FALSE)
                ->from('recipe')
                ->join('recipe_attributes', 'recipe.recipe_id = recipe_attributes.recipe_id', 'left')
                ->join('attributes', 'recipe_attributes.attribute_id = attributes.attribute_id', 'left')
                ->join('recipe_image', 'recipe.recipe_id = recipe_image.recipe_id', 'left')
                ->where('recipe.is_deleted !=','1', FALSE)
                ->group_by('recipe.recipe_name', 'desc')
                ->order_by("recipe.recipe_name")
                ->order_by("count(recipe_attributes.attribute_id)",'desc')
                ->limit($limit, $offset)
                ->get();


        $total = $this->db->query('SELECT FOUND_ROWS() as total')->row()->total;
        $offset = $offset + $limit;
        $remaining = $total - $offset;
        $remaining = $remaining < 0 ? 0 : $remaining;
        $next_link = ($total > $offset ) ? (current_url() . '?' . $_SERVER['QUERY_STRING'] . '&offset=' . ($offset)) : '';


//            $msg = array('recipe' => array($this->db->query("SELECT SQL_CALC_FOUND_ROWS  recipe.`recipe_id`, recipe.`recipe_name`,GROUP_CONCAT(attributes.`attribute_abb`) AS attributes FROM recipe 
//LEFT JOIN recipe_attributes
//ON recipe.`recipe_id` = recipe_attributes.`recipe_id`
//LEFT JOIN attributes
//ON recipe_attributes.`attribute_id` = attributes.`attribute_id`GROUP BY recipe.`recipe_name`")->result()));

        $msg = array(
            'remaining_recipe' => (int) $remaining,
            'total_recipe' => (int) $total,
            'offset' => (int) ( $offset > $total ? $total : $offset ),
            'recipe' => array_map(function($arg) {
                
                        $arg->recipe_images = array_map(function($args) {
                            return ($args and file_exists(FCPATH . $args)) ? imager($args, 480, 270) : '';
                        }, array_filter(explode(',', $arg->recipe_images)));


                        return $arg;
                    },$recipe_list->result())
        );

        return $this->response('0001', $msg);
    }

    function featured_recipe() {
        $res = $this->db->query("SELECT recipe.`recipe_id`, recipe.`recipe_name`,GROUP_CONCAT(attributes.`attribute_id`) AS attributes FROM recipe 
                                                    LEFT JOIN recipe_attributes
                                                    ON recipe.`recipe_id` = recipe_attributes.`recipe_id`
                                                    LEFT JOIN attributes
                                                    ON recipe_attributes.`attribute_id` = attributes.`attribute_id`
                                                    INNER JOIN featured_recipe
                                                    ON recipe.`recipe_id` = featured_recipe.`recipe_id`
                                                    group by recipe.recipe_id");
        
        if($res->num_rows() > 0 ) {
            $msg = array('recipe' => $res->result());
            
        } else {
            $return = new stdClass();
            $return->recipe_id = 0;
            
            $msg = array('recipe' => array($return));
            
        }
        
        return $this->response('0001', $msg);
    }
    
    function cms_tips(){
        $msg = array('tips_page' => $this->db->query('select cms_id,cms_title,cms_desc,IF( cms_image IS NOT NULL AND cms_image <> "" ,  concat("'.base_url(config_item('cms_image_path')).'/",cms_image), "") as cms_image from cms where cms_id = "2"')->row());
         return $this->response('0001', $msg);
    }
    
    function cms_about(){
         $msg = array('about_page' => $this->db->query('select cms_id,cms_title,cms_desc,IF( cms_image IS NOT NULL AND cms_image <> "" ,  concat("'.base_url(config_item('cms_image_path')).'/",cms_image), "") as cms_image from cms where cms_id = "1"')->row());
         return $this->response('0001', $msg);
    }
    
  

}
