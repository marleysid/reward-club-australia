<?php

if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class webservice_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();

	}

	function hashCode($length = 32)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
    
    public function encryptPassword($password)
    {
		if (empty($password))
		{
			return FALSE;
		}
        return  sha1($password);
    }
	
	function checkLogin($user_id, $hash_code, $device_id, $device_type)
	{
		$query = $this->db->query("CALL sp_checkUserLoggedIn('{$user_id}','{$device_id}','{$hash_code}','{$device_type}')");
		return $query->row()->status == 1 ? $query->row() : FALSE;
	}
	
	public function _filter_data($table, $data, $db = NULL)
	{
		if(!$db)	$db = $this->db;
		$filtered_data = array();
		$columns = $db->list_fields($table);

		if (is_array($data))
		{
			foreach ($columns as $column)
			{
				if (array_key_exists($column, $data))
					$filtered_data[$column] = $data[$column];
			}
		}

		return $filtered_data;
	}
	
}