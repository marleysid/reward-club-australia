<h3><?php echo $title?></h3>
<form method="POST" action="<?php echo base_url(uri_string()); ?>" id="register_form"  role="form"  class="form-horizontal" >
    <div class="form-group">
	<label class="col-sm-3 control-label">User Id:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="user_id" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">hash Code:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="hash_code" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Device Id:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="device_id" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Device Type:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="device_type" value="" />1: iphone, 2: android</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label"> * First Name:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="first_name" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label"> Last Name:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="last_name" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Date of Birth:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="user_dob" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Address:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="address" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Suburb:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="suburb" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label"> * Email:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="email" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Mobile:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="mobile" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Mother Photo:</label>
	<div class="col-sm-9"><input type="file" name="users_image_path"  /></div>
	</div>
<!--    <div class="form-group">
	<label class="col-sm-3 control-label">&nbsp;</label>
	<div class="col-sm-9"><span id="add_form" style="cursor:pointer; float:right;">Add</span></div>    
    </div>-->
    <div class="form-group">
    <label class="col-sm-3 control-label">Baby's Data:</label>
    <div class="col-sm-9"><textarea class="form-control" name="babies_data" /></textarea>format: [{"name":"test1|test2","dob":"2014-01-01","image":""}]</div>
    
    </div>

<!--    <div class="dynamic_form_placeholder">
        <div class="dynamic_form">
            <div class="form-group">
            <label class="col-sm-3 control-label">Baby's Name:</label>
            <div class="col-sm-9"><input class="form-control" type="text" name="name[]" value="" /></div>
            </div>
            <div class="form-group">
            <label class="col-sm-3 control-label">Baby's Date Of Birth:</label>
            <div class="col-sm-9"><input type="text" name="dob[]" value=""  /></div>
            </div>
            <div class="form-group">
            <label class="col-sm-3 control-label">Upload Baby Photo Here:</label>
            <div class="col-sm-9"><input type="file" name="userfile_0"  /></div>
            </div>
        </div>
    </div>    -->
	<div class="form-group"><div class="col-sm-offset-3 col-sm-9"><input type="submit" class="btn btn-default" value="Submit" /></div></div>
</form>
<div name="dynamic_form" id="dynamic_form" style="display:none;">	
    <div class="dynamic_form">
        <div class="form-group">
        <label class="col-sm-3 control-label">Baby's Name:</label>
        <div class="col-sm-9"><input class="form-control" type="text" name="name[]" value="" /></div>
        </div>
        <div class="form-group">
        <label class="col-sm-3 control-label">Baby's Date Of Birth:</label>
        <div class="col-sm-9"><input type="text" name="dob[]" value=""  /></div>
        </div>
        <div class="form-group">
        <label class="col-sm-3 control-label">Upload Baby Photo Here:</label>
        <div class="col-sm-9"><input type="file" name="userfile"  /><span class="remove_dynamic_field" style="cursor:pointer; float:right;">Remove</span></div>
        </div>
    </div>    
</div>
<script type="text/javascript">
function label_upload()
{
	var cnt = 0;
	$('.dynamic_form_placeholder').find(':input[type=file]').each(function(){
		$(this).attr('name','userfile_'+cnt);
		cnt++;
	});
}
$(function(){
	
	$('#add_form').on('click',function(){
		var dynamic_table = $('#dynamic_form').html();
		$('.dynamic_form_placeholder').append(dynamic_table);
		label_upload();
	});
	$(document).on('click','.remove_dynamic_field',function(){
		$(this).parents('.dynamic_form').remove();
		label_upload();
	});
});
</script>