<h3>Feedback</h3>
<form method="POST" action="<?php echo base_url(uri_string()); ?>"  role="form" enctype="multipart/form-data"  class="form-horizontal" >
	
	<div class="form-group">
	<label class="col-sm-3 control-label">Name:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="name" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Email:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="email" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Type of problem:</label>
	<div class="col-sm-9"><?php echo $problem_types; ?></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Phone Number:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="phone_no" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Location:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="location" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Notes:</label>
        <div class="col-sm-9"><textarea class="form-control" type="text" name="notes" ></textarea></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Image:</label>
        <div class="col-sm-9"><input type="file" name="image" ></textarea></div>
	</div>
	<div class="form-group"><div class="col-sm-offset-3 col-sm-9"><input type="submit" class="btn btn-default" value="Submit" /></div></div>
</form>