<h3>Online product purchase</h3>
<form method="POST" action="<?php echo base_url(uri_string()); ?>"  role="form"  class="form-horizontal" >
	
	<div class="form-group">
	<label class="col-sm-3 control-label">Product:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="product" value="" /> <span class="help-block" style="white-space: normal;" >Array of product id and quantity in JSON format ( [{product_id:1, quantity:3},{product_id:2, quantity:5}] )</span></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Name:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="name" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Delivery Address:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="delivery_address" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Amount:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="amount" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Email:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="email" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Contact No:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="contact_no" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Quantity:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="quantity" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">PaymentId:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="paymentid" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Replydata:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="replydata" value="" /></div>
	</div>
	<div class="form-group"><div class="col-sm-offset-3 col-sm-9"><input type="submit" class="btn btn-default" value="Submit" /></div></div>
</form>