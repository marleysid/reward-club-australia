<h3>Company Review </h3>
<form method="POST" action="<?php echo base_url(uri_string()); ?>"  role="form"  class="form-horizontal" >
	
	<div class="form-group">
	<label class="col-sm-3 control-label">Company Id:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="company_id" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Rating:</label>
	<div class="col-sm-9">
		<select class="form-control" name="rating">
			<option>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
			<option>5</option>
		</select>
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Comment:</label>
	<div class="col-sm-9"><textarea  class="form-control" name="comment"  ></textarea></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Email:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="email" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Name:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="name" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Phone:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="phone" value="" /></div>
	</div>
	<div class="form-group"><div class="col-sm-offset-3 col-sm-9"><input type="submit" class="btn btn-default" value="Submit" /></div></div>
</form>