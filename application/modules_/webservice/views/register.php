<h3><?php echo $title?></h3>
<form method="POST" action="<?php echo base_url(uri_string()); ?>" id="register_form"  role="form"  class="form-horizontal" >	
	<div class="form-group">
	<label class="col-sm-3 control-label"> * Name:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="name" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label"> * Email:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="email" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label"> * Password:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="password" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Vehicle plate number:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="vehicle_plate_number" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Postcode:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="postcode" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Suburb:</label>
	<div class="col-sm-9"><input class="form-control" type="text" name="suburb" value="" /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">User Photo:</label>
	<div class="col-sm-9"><input type="file" name="users_image_path"  /></div>
	</div>
	<div class="form-group">
	<label class="col-sm-3 control-label">Vehicle Photo:</label>
	<div class="col-sm-9"><input type="file" name="vehicle_image_path"  /></div>
	</div>
	<div class="form-group"><div class="col-sm-offset-3 col-sm-9"><input type="submit" class="btn btn-default" value="Submit" /></div></div>
</form>