<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Member Detail</h5>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="member_detail_tbl" style="white-space: nowrap">
		<tbody role="alert" aria-live="polite" aria-relevant="all">
			<?php if($member_detail):?>
			<tr>
				<th>Member Name</th>
				<td><?php echo "$member_detail->member_first_name $member_detail->member_last_name"; ?></td>
			</tr>
			<tr>
				<th>Member Email</th>
				<td><?php echo "$member_detail->member_email"; ?></td>
			</tr>
			<tr>
				<th>Date of Birth</th>
				<td><?php echo date('jS F, Y',  strtotime($member_detail->member_dob)); ?></td>
			</tr>
			<tr>
				<th>Contact Number</th>
				<td><?php echo "$member_detail->member_contact_no"; ?></td>
			</tr>
			<tr>
				<th>Member Details</th>
				<td><?php echo "$member_detail->member_details"; ?></td>
			</tr>
			<tr>
				<th>Club</th>
				<td><?php echo "$member_detail->club_name"; ?></td>
			</tr>
			<tr>
				<th>Membership Number</th>
				<td><?php echo "$member_detail->membership_number"; ?></td>
			</tr>
			<?php endif; ?>
		</tbody>
		</table>
	</div>
</div>