<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Payment Details</h5>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="member_detail_tbl" >
		  <thead>
			<tr>
				<th>#</th>
				<th style="white-space: nowrap;">Amount Paid</th>
				<th style="white-space: nowrap;">Transaction Id</th>
				<th style="white-space: nowrap;">Transaction Status</th>
				<th style="white-space: nowrap;">Payment Date</th>
			</tr>
		  </thead>
		<tbody role="alert" aria-live="polite" aria-relevant="all">
			<?php if($payment_detail):?>
				<?php foreach($payment_detail as $key=>$val): ?>
				<tr>
					<td><?php echo ++$key; ?></td>
					<td><?php echo $val->amount_paid; ?></td>
					<td><?php echo $val->txnId; ?></td>
					<td><?php echo $val->txnState; ?></td>
					<td><?php echo $val->payment_date; ?></td>
				</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr><td colspan="5">No Payment Details.</td></tr>
			<?php endif; ?>
		</tbody>
		</table>
	</div>
</div>