<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'members';
        $this->field_prefix = 'member_';
        $this->log_user = FALSE;
    }

//    function get_all($club_id = false, $return_type = 0) {
//        $this->select("*,CONCAT(member_first_name,' ',member_last_name) AS name, GROUP_CONCAT( club_name ) AS clubs", FALSE)
//                ->join('club_member', 'membership.member_id = club_member.member_id')
//                ->join('clubs', 'club_member.club_id = clubs.club_id');
//        if ($club_id)
//            $this->where('clubs.club_id', $club_id);
//        $this->db->group_by('`membership`.member_id');
//
//        return parent::get_all($return_type);
//    }
    
    function member_detail(){
     $query =   $this->db->select("member_id ,member_first_name,member_last_name, member_email,member_card_number, member_active")
                         ->from("members")
                         ->where('member_deleted != "1"')
                         ->get();
    
     return $query->result();
    }

//    function update_membership_number($cm_id, $m_number, $expiry_date = NULL) {
//        if ($expiry_date)
//            $this->db->set('cm_expire_date', $expiry_date);
//        $this->db->update('club_member', array('membership_number' => $m_number), "cm_id = {$cm_id}");
//    }
//
//    function check_membership_number($cm_id, $m_number) {
////		$res = $this->db->get_where('club_member', "cm_id = {$cm_id}")->row();
//
//        $result = $this->db->get_where('club_member', "cm_id != {$cm_id} AND club_id = (SELECT club_id from club_member where cm_id = {$cm_id} ) AND membership_number LIKE '{$m_number}'");
//
//        return $result->num_rows() > 0 ? TRUE : FALSE;
//    }
//
//    function get_club_member_detail($cm_id) {
//        $res = $this->db->join('membership', 'club_member.member_id = membership.member_id')
//                ->join('clubs', 'club_member.club_id = clubs.club_id')
//                ->where('club_member.cm_id', $cm_id)
//                ->get('club_member');
//
//        return $res->num_rows() > 0 ? $res->row() : FALSE;
//    }
//
//    function get_member_payment_detail($cm_id) {
//        $res = $this->db->where('cm_id', $cm_id)
//                ->order_by('payment_date', 'desc')
//                ->get('membership_payment');
//
//        return $res->num_rows() > 0 ? $res->result() : FALSE;
//    }
//
//    function delete_club_member($cm_id) {
//        $result = $this->db->delete('club_member', array('cm_id' => $cm_id));
//
//        return $result ? TRUE : FALSE;
//    }
//    
    function delete_member($id){
        $result = $this->db->update('members',array('member_deleted' => '1'), array('member_id' => $id));
        return $result;
    }
    
    function current_member_detail($id){
        $query = $this->db->select("*")
                          ->from("members")
                          ->where("member_id = '$id'")
                          ->get();
        return $query->row();
    }

}
