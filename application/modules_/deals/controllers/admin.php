<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('deal_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['deals'] = $this->deal_model->get_all();

        Template::render('admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_deal(0);
        }

        $this->data['edit'] = FALSE;


        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/bootstrap-datetimepicker.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datetimepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery.numeric.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery.numeric.min.js", TRUE);



        Template::render('admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('admin/deals');

        $this->data['deal_detail'] = $this->deal_model->get($id);
        $this->data['slider_detail'] = $this->deal_model->slider_detail($id);
        //echo $this->db->last_query();die;
        
       // debug($this->data['slider_detail']);die;

        $this->data['deal_detail'] || redirect('admin/deals/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_deal($id);
        }


        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/bootstrap-datetimepicker.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datetimepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery.numeric.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery.numeric.min.js", TRUE);



        Template::render('admin/form', $this->data);
    }

    function _add_edit_deal($id) {
       
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $data = $this->input->post();
       // debug($data);die;
        $this->form_validation->set_rules(
                array(
                    array('field' => 'deal_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'deal_price', 'label' => 'Deal Price', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'deal_short_desc', 'label' => 'Short Description', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'deal_desc', 'label' => 'Description', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'deal_tag', 'label' => 'Tag', 'rules' => 'required|trim|min_length[5]|xss_clean'),
                    array('field' => 'deal_left_tag', 'label' => 'Left Tag', 'rules' => 'required'),
                    array('field' => 'deal_offer_expire', 'label' => 'Offer Expiry Date', 'rules' => 'required'),
                    array('field' => 'deal_timer_expire', 'label' => 'Timer Expiry Date', 'rules' => 'required'),
                    array('field' => 'deal_quantity', 'label' => 'Quantity', 'rules' => 'required|trim|xss_clean'),
                    array('field' => 'slider_bottom', 'label' => 'Bottom', 'rules' => 'trim'),
                    array('field' => 'slider_top', 'label' => 'Top', 'rules' => 'trim'),
                    array('field' => 'slider_featured', 'label' => 'Featured', 'rules' => 'trim'),
                    array('field' => 'deal_status', 'label' => 'Status', 'rules' => 'trim'),
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            //echo 'here';die;
            return FALSE;
        }

        $deal_image_path = NULL;
        if (isset($_FILES['deal_image']['name']) and $_FILES['deal_image']['name'] != '') {
            if ($result = upload_image('deal_image', config_item('deal_image_root'), FALSE)) {
                $image_path = config_item('deal_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
////				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'), TRUE);
//                }
                $deal_image_path = $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }elseif($id == 0) {
            
                $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
                return FALSE;
            
        }

        unset($data['deal_image']);

        !$deal_image_path || ($data['deal_image_path'] = $deal_image_path);
        $test = $this->input->post();
        
       // $data['deal_timer_expire'] = str_replace(',','',$test);
       // debug($data['deal_timer_expire']);die;
        $data['deal_status'] = $this->input->post('deal_status') ? '1' : '0';
        
 
        if ($id == 0) {
           // echo 'here';die;
            $featured = $this->input->post('slider_featured')? '1' : '0';
            $top = $this->input->post('slider_top') ? '1' : '0';
            $bottom = $this->input->post('slider_bottom') ? '1' : '0';
           
           $id =  $this->deal_model->insert($data);
           $this->db->insert('slider',array('slider_type_id' => $id,
                                               'slider_type' => 'deal',
                                                'slider_top' => $top,
                                                'slider_bottom' =>$bottom,
                                                'slider_featured' => $featured));
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
           
            $old_logo = NULL;
            if ($deal_image_path) {
                $old_logo = $this->deal_model->get($id)->deal_image_path;
            }
            $this->deal_model->update($id, $data);
            
            $this->db->delete('slider',array('slider_type_id' => $id));
           // echo $this->db->query();die;
             $featured = $this->input->post('slider_featured')? '1' : '0';
            $top = $this->input->post('slider_top') ? '1' : '0';
            $bottom = $this->input->post('slider_bottom') ? '1' : '0';
            
            $this->db->insert('slider',array('slider_type_id' => $id,
                                               'slider_type' => 'deal',
                                                'slider_top' => $top,
                                                'slider_bottom' =>$bottom,
                                                'slider_featured' => $featured));
           
            if ($old_logo and file_exists(config_item('deal_image_root') . $old_logo))
                unlink_file(config_item('deal_image_root') . $old_logo);


            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/deals/', 'refresh');
    }

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->offer_model->update($id, array('offer_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function toggle_review_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->load->model(array('review_model'));
            $this->review_model->update($id, array('status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function delete($id) {
        $old_logo = $this->deal_model->get($id)->deal_image_path;

        if ($this->deal_model->delete($id)) {
            if ($old_logo and file_exists(config_item('deal_image_root') . $old_logo))
                unlink_file(config_item('deal_image_root') . $old_logo);
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/deals/', 'refresh');
    }

}
