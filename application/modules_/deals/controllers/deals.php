<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Deals extends Front_Controller {

    // public $data;

    function __construct() {
        parent::__construct();
        $this->load->model(array('deal_model', 'advertisement/advertisement_model', 'offers/offer_model','reviews/review_model'));
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    function cart_detail($id = 0) {
        if (!($this->member_auth->logged_in())) {
            echo 'please login';
        } else {
            $id = $this->current_member->member_id;
            $this->data['cart_detail'] = $this->deal_model->cart_detail($id);
            // echo $this->db->last_query();die;
            $this->template->add_title_segment('Cart Detail');
            Template::render('frontend/cart_detail', $this->data);
        }
    }

    function favourites() {
        if (!($this->member_auth->logged_in())) {
            echo 'please login';
        } else {
            $id = $this->current_member->member_id;
            $this->data['favourites'] = $this->deal_model->favourites($id);
            $this->data['side_ads'] = $this->advertisement_model->get_right_side_ads($id);
            $this->template->add_title_segment('Favourite');
            Template::add_css('assets/lib/jquery-raty/jquery.raty.css');
            Template::add_js(base_url('assets/lib/jquery-raty/jquery.raty.js'));
            Template::add_js(base_url('assets/frontend/js/cart_favourites.js'));
            Template::render('frontend/favourites', $this->data);
        }
    }

    function detail() {
        $id = safe_b64decode($this->uri->segment(3));
        // echo $id;die;
        $this->data['slider_detail'] = $this->deal_model->get_bottom_slider_detail($id);
        //echo $this->db->last_query();die;
        $this->data['count_review'] = $this->review_model->total_review($id, 'deal');
        $this->data['get_ads'] = $this->advertisement_model->ads_for_detail_page();
         $this->data['category'] = $this->category_model->parent_cat();
        $this->data['regions'] = $this->region_model->get_regions();
        $this->data['selected_cat'] = $this->input->get('category');
        $this->data['selected_sub_cat'] = $this->input->get('sub_category');
        Template::add_css('assets/lib/jquery-raty/jquery.raty.css');
//        Template::add_js('assets/frontend/js/jquery.rateit.js');
        Template::add_js(base_url('assets/lib/jquery-raty/jquery.raty.js'));
        Template::add_js(base_url('assets/frontend/js/cart_favourites.js'));
       
        Template::render('frontend/detail', $this->data);
    }

    function quantity_change() {
        $status = '';
        $quantity = $this->uri->segment(3);
        $id = $this->uri->segment(4);
        $deals_quan = $this->deal_model->deals_quantity($id);

        if ($deals_quan->deal_quantity >= 1) {
            $this->deal_model->quantity_change($quantity, $id);
            $this->db->set('deal_quantity', 'deal_quantity -1', FALSE);
            $this->db->where('deal_id', $id);
            $this->db->update('deals');

            $status = 1;
            echo json_encode(array('status' => $status));
        } else {
            $status = 0;
            echo json_encode(array('status' => $status));
        }
    }

}
