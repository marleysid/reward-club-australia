<div id="FoodCarousel" class="carousel slide" data-ride="carousel">
                 <div class="carousel-inner">
                     <?php $flag = 0; ?>
                   <?php if(isset($get_top_slider)): ?>
                     <?php foreach($get_top_slider as $top_slider): ?>
                     <div class="item <?php if($flag++ == 0) echo 'active'; else  echo 'height-full'; ?>">
                         <h2><span><?php echo ucwords($top_slider->title); ?></span></h2>
                        <small>Date Offer Expires: <?php echo $top_slider->offer_expire; ?></small>
                        <p><?php echo $top_slider->short_desc; ?></p>
                        <button class="btn btn-default"><a href="<?php echo site_url('offers/detail/'.$top_slider->type.'/' . safe_b64encode($top_slider->id)); ?>">Read More</a></button>
                        <?php if ($top_slider->type == 'deal' and isset($userLoggedIn) and $userLoggedIn) : ?>
                        <button class="btn btn-danger add-to-cart" data-type="<?php echo $top_slider->type; ?>" data-id="<?php echo $top_slider->id; ?>">Add to Cart</button>
                        <?php elseif($top_slider->type == 'offer' and isset($userLoggedIn) and $userLoggedIn): ?>
                        <?php else: ?>
                        <button class="btn btn-danger" data-type="<?php echo $top_slider->type; ?>" data-id="<?php echo $top_slider->id; ?>"><a href="" data-toggle="modal" data-target="#loginModal" data-whatever="@getbootstrap">Add to Cart</a></button>
                       
                            <?php endif; ?>
                      </div>
                    <?php endforeach; ?>
                     <?php endif; ?>
<!--                    <div class="item  height-full">
                        <h2><span>Trek 3 Series Mens Bike</span></h2>
                        <small>Date Offer Expires: 25/12/14</small>
                        <p>70% off Trek 3 Series Mens Bike. Get in quick, this weekend only.</p>
                        <button class="btn btn-default">Read More</button><button class="btn btn-danger">Read More</button>
                    </div>
                    <div class="item  height-full">
                        <h2><span>Trek 3 Series Mens Bike</span></h2>
                        <small>Date Offer Expires: 25/12/14</small>
                        <p>70% off Trek 3 Series Mens Bike. Get in quick, this weekend only.</p>
                        <button class="btn btn-default">Read More</button><button class="btn btn-danger">Read More</button>
                    </div>-->
                     <a class="left carousel-control" href="#FoodCarousel" role="button" data-slide="prev"><img src="<?php echo get_asset('assets/frontend/images/left.png'); ?>"></a>
                <a class="right carousel-control" href="#FoodCarousel" role="button" data-slide="next"><img src="<?php echo get_asset('assets/frontend/images/right.png'); ?>"></a>
           
                </div>
              
                <!-- Controls -->
                </div>
