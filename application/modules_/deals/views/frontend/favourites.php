<div class="banner">
  <div class="slider"><img src="<?php echo get_asset('assets/frontend/images/banner_of_club_member.jpg'); ?>"  class="img-responsive" alt="slide"></div>
  <div class="banner-slider">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
      <ul>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
      </ul>
    </div>
  </div>
</div>
<div class="col-md-12">
<div class="container"> 
  <!-- breadcrumbs -->
  <ol class="breadcrumb">
    <li><a href="#">Favourites</a></li>
   
  </ol>
</div>
</div>
<div class="col-md-12">
<div class="container"> 
  <div class="row">
   <div class="col-md-10"> 
    <div class="col-sm-12">
   
      
       <!--repeated start here -->
       <?php if(isset($favourites)): ?>
       <?php foreach ($favourites as $fav): ?>
       <div class="col-md-12 bg_nn_pgmrgn bt_mrgn_wrp">
        <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> 
       
         <div class="best_price_wrp"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="img-responsive"></a></div>
          <a href="<?php echo site_url('offers/detail/'.$fav->type.'/'.safe_b64encode($fav->id)); ?>" class="best_img_sprt_clr_cgng"><img src="<?php if ($fav->type == 'deal')
                        echo imager($fav->image ? 'assets/uploads/deal_image/' . $fav->image : '', 241, 170);
                    else
                        echo imager($fav->image ? 'assets/uploads/offer_image/' . $fav->image : '', 241, 170);
                  ?>" class="img-responsive"></a> </div>
        <div class="col-md-9 with_resuld_board bg_grw_01">
          <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">
         
            <h4><?php echo $fav->title; ?></h4>
            <p><?php echo $fav->short_desc; ?></p>
          </div>
          <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
            <h4>
             <a href=""><span class="glyphicon glyphicon-heart"></span></a><br />
            <span  data-score="<?php echo $fav->review; ?>" class="rate"></span> <br />
              <?php echo $fav->total_review; ?> Reviews </h4>
              
          
            <button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-arrow-right"></span> </button>
          </div>
        </div>
      </div>
       <?php endforeach; ?>
       <?php endif; ?>
      <!--repeated end here -->
      <!--pagination start here -->
      
      <!--pagination end here -->
    </div>
    
    
    </div>
    <div class="col-md-2 ">
      <a href="#"><img src="<?php echo imager($side_ads->ad_image_path ? 'assets/uploads/ad/'.$side_ads->ad_image_path:'',161,602); ?>" class="img-responsive"></a>
 
    	
    </div>
  </div>
</div>
</div>
<script>
    $(function(){
       $('.rate').raty( {path:  base_url + 'assets/lib/jquery-raty/images',
                         readOnly: true,
       score: function() {
    return $(this).attr('data-score');
  }});  
    });
   
    </script>