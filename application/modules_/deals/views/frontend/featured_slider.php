     <div id="featureCarousel" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                         <?php $flag = 0; ?>
                        <?php if (isset($is_featured)): ?>
                            <?php foreach ($is_featured as $featured): ?> 

                                <div class="item <?php if($flag++ == 0) echo 'active'; else  echo 'height-full'; ?>">
                                    <a href="<?php echo site_url('offers/detail/deal/' . safe_b64encode($featured->deal_id)); ?>"><img src="<?php echo imager($featured->deal_image_path ? 'assets/uploads/deal_image/'.$featured->deal_image_path:'',483,225); ?>" class="img-responsive" alt="slide" style="height: 225px;"></a>
                                    <div class="captions">
                                        <h4><?php echo $featured->deal_title; ?></h4>
                                        <?php echo $featured->deal_short_desc; ?>
                                    </div>

                                </div>
                            <?php endforeach; ?>
                        <?php endif; ?>  
                    </div>

                    <!-- Controls -->
                    <a class="left carousel-control" href="#featureCarousel" role="button" data-slide="prev"><img src="<?php echo get_asset('assets/frontend/images/left.png'); ?>"></a>
                    <a class="right carousel-control" href="#featureCarousel" role="button" data-slide="next"><img src="<?php echo get_asset('assets/frontend/images/right.png'); ?>"></a>
                    <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm_01"><?php echo $featured->deal_tag; ?></span>


                </div>
