<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Webservice extends Web_Service_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
    }

    function detail()
    {
        if($this->input->post()) {
            $deal_id = $this->input->post('deal_id');
            $query = $this->db->query("CALL sp_getDeals('$deal_id') ");
            // debug($query->result());die;
            if($deal_id and $query->num_rows() > 0) {
                $query = $query->row();
                $msg = array('deals' => array('id' => $query->deal_id,
                                'title' => $query->deal_title,
                                'image' => base_url(config_item('deal_image_path').$query->deal_image_path),
                                'short_description' => $query->deal_short_desc,
                                'description' => $query->deal_desc,
                                'right_tag' => $query->deal_tag,
                                'left_tag' => $query->deal_left_tag,
                                'offer_expire' => $query->deal_offer_expire,
                                'timer_expire' => $query->deal_timer_expire,
                                'current_date_time' => date('Y-m-d H:i:s'),
                                'price' => $query->deal_price,
                                'quantity' => $query->deal_quantity,
                                'average_rating' => (int) $query->rating,
                                "address" => array(
                                        'street' => 'Rewards Club',
                                        'phone' => '564684156',
                                        'latitude' => '12',
                                        'longitude' => '12',
                                ),
                ));

                return $this->response('0001', $msg);
            } else {
                return $this->response('0000');
            }
        } else {
            $this->load->view('webservice/deals', array('title' => 'Deals Detail'));
        }
    }

    function deal_list()
    {

        $limit = (int) 3;
        $offset = (int) $this->input->get('offset');

        $deals = $this->db->select(' SQL_CALC_FOUND_ROWS 
               deal_id
               ,deal_title
               ,deal_tag
               ,deal_left_tag
               ,deal_short_desc
               ,IF( deal_image_path IS NOT NULL AND deal_image_path <> "" ,  concat("'.base_url(config_item('deal_image_path')).'/",deal_image_path), "") as deal_image
               ,deal_offer_expire
               ,deal_quantity
               ,deal_timer_expire', FALSE)
                ->from('deals')
                ->limit($limit, $offset)
                ->get();


        $total = $this->db->query('SELECT FOUND_ROWS() as total')->row()->total;
        $offset = $offset + $limit;
        $remaining = $total - $offset;
        $remaining = $remaining < 0 ? 0 : $remaining;
        $next_link = ($total > $offset ) ? (current_url().'?'.$_SERVER['QUERY_STRING'].'&offset='.($offset)) : '';


//            $msg = array('recipe' => array($this->db->query("SELECT SQL_CALC_FOUND_ROWS  recipe.`recipe_id`, recipe.`recipe_name`,GROUP_CONCAT(attributes.`attribute_abb`) AS attributes FROM recipe 
//LEFT JOIN recipe_attributes
//ON recipe.`recipe_id` = recipe_attributes.`recipe_id`
//LEFT JOIN attributes
//ON recipe_attributes.`attribute_id` = attributes.`attribute_id`GROUP BY recipe.`recipe_name`")->result()));

        $msg = array(
                'remaining_offer' => (int) $remaining,
                'total_offer' => (int) $total,
                'next_link' => $next_link,
                'offset' => (int) ( $offset > $total ? $total : $offset ),
                'deals' => $deals->result()
        );

        return $this->response('0001', $msg);
    }

    function add_to_cart()
    {
        if($this->input->post()) {
            if(!($member = $this->is_logged_in())) {
                return $this->response('0004');
            }
            $this->load->library('form_validation');
            $this->form_validation->set_rules(
                    array(
                            array('field' => 'deal_id', 'label' => 'deal_id', 'rules' => 'trim|required|xss_clean'),
                            array('field' => 'quantity', 'label' => 'quantity', 'rules' => 'trim|required|xss_clean'),
                    )
            );
            if($this->form_validation->run($this) === FALSE) {
                return $this->response('0000');
            }
            $deal_id = $this->input->post('deal_id');
            $quantity = $this->input->post('quantity');
            
            $query = $this->db->query("CALL sp_addToCart(?, ?, ?) ", array( $member->member_id, $deal_id, $quantity));
            
            if($query->num_rows() > 0) {
                $result = $query->row();
                if ($result->status == 1) {
                    $code = '0001';
                    
                } elseif ($result->status == 2) {
                    $code = '0003';
                    
                } elseif ($result->status == 3) {
                    $code = '0010';
                    
                } elseif ($result->status == 4) {
                    $code = '0011';
                    
                } else {
                    $code = '0000';
                    
                }
                
                return $this->response($code);
            } else {
                return $this->response('0000');
            }
            
        } else {
            
            $this->load->view('webservice/add_to_cart', array('title' => 'add to cart'));
        }
    }
    
    function cart_items()
    {
        if($this->input->post()) {
            if(!($member = $this->is_logged_in())) {
                return $this->response('0004');
            }
            
            $query = $this->db->query("CALL sp_getCartItems(?) ", array( $member->member_id));
            
            $results = array_map( function ($arg) {
                $arg->deal_image_path = ($arg->deal_image_path and file_exists(config_item('deal_image_root').$arg->deal_image_path)) ? base_url(config_item('deal_image_path').$arg->deal_image_path) : '';
                return $arg;
            }, $query->result());
            
            return $this->response('0001', array('items' => $results));
            
        } else {
            
            $this->load->view('webservice/cart_items', array('title' => 'Cart Items'));
        }
        
    }

    function delete_cart_item()
    {
        if($this->input->post()) {
            if(!($member = $this->is_logged_in())) {
                return $this->response('0004');
            }
            $this->load->library('form_validation');
            $this->form_validation->set_rules(
                    array(
                            array('field' => 'deal_id', 'label' => 'deal_id', 'rules' => 'trim|required|xss_clean'),
                    )
            );
            if($this->form_validation->run($this) === FALSE) {
                return $this->response('0000');
            }
            $deal_id = $this->input->post('deal_id');
            
            $query = $this->db->query("CALL sp_deleteCartItem(?, ?) ", array( $member->member_id, $deal_id));
            
            if($query->num_rows() > 0) {
                $result = $query->row();
                if ($result->status == 1) {
                    $code = '0001';
                    
                } elseif ($result->status == 2) {
                    $code = '0003';
                    
                } elseif ($result->status == 3) {
                    $code = '0010';
                    
                } elseif ($result->status == 4) {
                    $code = '0012';
                    
                } else {
                    $code = '0000';
                    
                }
                
                return $this->response($code);
            } else {
                return $this->response('0000');
            }
            
        } else {
            
            $this->load->view('webservice/delete_cart', array('title' => 'delete cart item'));
        }
    }
    
    function test_test()
    {
        $this->load->library('MyPaypal');
        
        if($result = $this->mypaypal->get_paypal_payment("PAY-3D4440040S850632KKOFJZAI")) {
            
            $result = $result->toArray();debug($result);
            $transaction = $result['transactions'];

            $txnId = $transaction[0]['related_resources'][0]['sale']['id'];
            $txnState = $transaction[0]['related_resources'][0]['sale']['state'];
            $txnAmount = $transaction[0]['amount']['total'];
            $txnCur = $transaction[0]['amount']['currency'];

        } else {
             echo 'here';die;
            debug();
        }
        
    }

    function cart_checkout()
    {
        if($this->input->post()) {
            if(!($member = $this->is_logged_in())) {
                return $this->response('0004');
            }

            $payment_id = urldecode($this->input->post('paymentid'));
            $replydata = urldecode($this->input->post('replydata'));

            // check paypal payment
            $this->load->library('MyPaypal');

            if($result = $this->mypaypal->get_paypal_payment("PAY-3D4440040S850632KKOFJZAI")) 
            {
                $result = $result->toArray();
                $transaction = $result['transactions'];

                $txnId = $transaction[0]['related_resources'][0]['sale']['id'];
                $txnState = $transaction[0]['related_resources'][0]['sale']['state'];
                $txnAmount = $transaction[0]['amount']['total'];
                $txnCur = $transaction[0]['amount']['currency'];
                
            } else {
                
                return $this->response('0013');
            }
            
            $name = urldecode($this->input->post('name'));
            $delivery_address = urldecode($this->input->post('delivery_address'));
            $amount = $this->input->post('amount');
            $email_address = urldecode($this->input->post('email'));
            $contact_number = urldecode($this->input->post('contact_no'));
//				$quantity = (int) $this->input->post('quantity');				
//				$path = APPPATH.'logs/online_orders/orders.txt';
//				$message = "\r\n".date('Y-m-d')
////					. "\t|\t{$user_detail->email}\t\t\t"
//					. "|\t{$payment_id}\t"
//					. "|\t{$txnId}\t"
//					. "|\t{$txnState}\t\t\t|\t"
//					. "|\t{$txnAmount} {$txnCur}\t\t\t|\t"
//					. trim(preg_replace('/\s+/', ' ', $replydata))."\r\n";
//				@file_put_contents($path, $message, FILE_APPEND | LOCK_EX);

            $query = $this->db->query("CALL sp_setOnlineOrder ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
                    array($member->member_id, $name, $delivery_address, $txnAmount, $email_address, $contact_number, $payment_id, $replydata, $txnId, $txnState ));

            if($query->num_rows() > 0) {
                $results = $query->row();
                if($results->status == '0') {
                    $code = '0000';
                    
                } else {
                    
                    $this->load->library('email');
                    $message = "<p>Thank you for your payment.</p>"
                            ."<p>This email is to confirm your payment submitted on ".date('F jS, Y')."</p>"
                            ."<p>Details:<br>"
                            ."<strong>Transaction Id</strong>: {$txnId}<br>"
                            ."<strong>Transaction Amount</strong>: $ {$txnAmount} {$txnCur}<br>"
                            ."</p>"
                            ."<p>Rewards Club</p>";

//					$this->email->to($user_detail->email)
//						->from("info@quint.com")
//						->subject("Thank you for your payment")
//						->message($message);
//
//					$this->email->send();

                    $code = '0001';
                }
            } else {
                $code = '0000';
            }
            
            return $this->response($code);
        } else {
            $this->load->view('webservice/checkout', array('title' => 'checkout'));
        }
    }

}
