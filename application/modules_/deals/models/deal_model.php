<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Deal_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'deals';
        $this->field_prefix = 'deal_';
        $this->log_user = FALSE;
    }

    function cart_detail($id) {
        $this->db->select("deals.deal_id,deals.deal_title,deal_price,deals.deal_short_desc,deal_image_path,cart_session.quantity")
                ->from("deals")
                ->join("cart_session", "deals.deal_id = cart_session.deal_id", "inner")
                ->join("members", "cart_session.member_id = members.member_id", "inner")
                ->where("cart_session.member_id = '$id'");

        $query = $this->db->get();

        return $query->result();
    }

    function favourites($id) {

        $query = $this->db->query("(SELECT 
  deals.`deal_id` AS id,
  deals.`deal_title` AS title,
  deals.`deal_short_desc` AS short_desc,
  deals.`deal_image_path` AS image,
  favourite.favourite_type as type,
  avg(reviews.review_rating) as review,
  count(reviews.review_rating) as total_review
FROM
  deals 
  INNER JOIN favourite 
    ON deals.`deal_id` = favourite.`favourite_type_id` 
    left join reviews
    on deals.deal_id = reviews.review_type_id and reviews.review_type = 'deal'
WHERE favourite.`favourite_type` = 'deal' AND favourite.`member_id` = '$id') 
UNION
(SELECT 
  offers.`offer_id`,
  offers.`offer_title`,
  offers.`offer_short_desc`,
  offers.`offer_image`,
  favourite.favourite_type as type,
  avg(reviews.review_rating),
  count(reviews.review_rating)
  FROM offers
  INNER JOIN favourite
  ON offers.`offer_id` = favourite.`favourite_type_id`
  left join reviews
  on offers.offer_id = reviews.review_type_id and reviews.review_type = 'offer'
  WHERE favourite.`favourite_type` = 'offer' AND favourite.`member_id` = '$id'
)",FALSE);
        return $query->result();
    }

    function slider_detail($id) {
        $this->db->select("*")
                ->from("slider")
                ->where("slider_type_id = '$id' ");

        $query = $this->db->get();

        return $query->row();
    }

    function is_featured() {
        $this->db->select("deals.*")
                ->from("deals")
                ->join("slider", "deals.deal_id = slider.slider_type_id", "inner")
                ->where("slider.slider_featured = '1'");


        $query = $this->db->get();

        return $query->result();
    }

    function get_top_slider() {
        $query = $this->db->query("(SELECT deals.deal_id AS id,deals.`deal_title` AS title,deals.`deal_short_desc` AS short_desc,
deals.`deal_desc` AS description,deals.`deal_image_path` AS image,deals.`deal_offer_expire`  AS offer_expire, slider.slider_type_id,'deal' as type FROM deals
INNER JOIN slider
ON deals.`deal_id` = slider.`slider_type_id`
WHERE slider.`slider_type` = 'deal' AND deals.`deal_status` = '1' AND CURRENT_DATE <= deals.`deal_offer_expire` AND slider.`slider_top` = '1')
UNION
(SELECT offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_desc`,offers.`offer_image`,offers.`offer_valid_date`,slider.slider_type_id,'offer' as type FROM offers
INNER JOIN slider
ON offers.`offer_id` = slider.`slider_type_id`
WHERE slider.`slider_type` = 'offer' AND offers.`offer_status` = '1' AND CURRENT_DATE <= offers.`offer_valid_date`  AND slider.`slider_top` = '1')");

        return $query->result();
    }

    function get_bottom_slider() {
        $query = $this->db->query("(SELECT deals.deal_id AS id,deals.`deal_title` AS title,deals.`deal_short_desc` AS short_desc,
deals.`deal_desc` AS description,deals.`deal_image_path` AS image,deals.`deal_offer_expire`  AS offer_expire, slider.`slider_type`,slider.slider_type_id FROM deals
INNER JOIN slider
ON deals.`deal_id` = slider.`slider_type_id`
WHERE slider.`slider_type` = 'deal' AND deals.`deal_status` = '1' AND CURRENT_DATE <= deals.`deal_offer_expire` AND slider.`slider_bottom` = '1')
UNION
(SELECT offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_desc`,offers.`offer_image`,offers.`offer_valid_date`,slider.`slider_type`,slider.slider_type_id FROM offers
INNER JOIN slider
ON offers.`offer_id` = slider.`slider_type_id`
WHERE slider.`slider_type` = 'offer' AND offers.`offer_status` = '1' AND CURRENT_DATE <= offers.`offer_valid_date`  AND slider.`slider_bottom` = '1')");

        return $query->result();
    }

    function get_bottom_slider_detail($id) {
        $query = $this->db->query("(SELECT deals.deal_id AS id,deals.`deal_title` AS title,deals.`deal_short_desc` AS short_desc,
deals.`deal_desc` AS description,deals.`deal_image_path` AS image,deals.`deal_offer_expire`  AS offer_expire, slider.`slider_type`,slider.slider_type_id,avg(reviews.review_rating) as review_rating FROM deals
INNER JOIN slider
ON deals.`deal_id` = slider.`slider_type_id`
left join reviews
on deals.deal_id = reviews.review_type_id
WHERE slider.`slider_type` = 'deal' AND slider.`slider_type_id` = '$id'
    group by deals.deal_id)
UNION
(SELECT offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_desc`,offers.`offer_image`,offers.`offer_valid_date`,slider.`slider_type`,slider.slider_type_id,avg(reviews.review_rating) as review_rating FROM offers
INNER JOIN slider
ON offers.`offer_id` = slider.`slider_type_id`
left join reviews
on offers.offer_id = reviews.review_type_id
WHERE slider.`slider_type` = 'offer' AND slider.`slider_type_id` = '$id' group by offers.offer_id)");

        return $query->row();
    }
   
    
    function cart($id,$member_id){
       $query = $this->db->select("deal_id")
                        ->from("cart_session")
                        ->where("deal_id = '$id' and member_id = '$member_id'")
                        ->get();
      if($query->num_rows() > 0){
          return TRUE;
      }
      else{
          return FALSE;
      }
    }
    
    function deals_quantity($id){
            $query = $this->db->select("deal_quantity")
                        ->from("deals")
                        ->where("deal_id = '$id'")
                        ->get();
      return $query->row();
    }
    function quantity_change($quantity,$id){
        $this->db->update('cart_session',array('quantity' =>$quantity),array('deal_id' => $id));
    }
    
    function get_detail($id){
        $query = $this->db->select("deals.deal_id,deals.deal_title,deals.deal_image_path,deals.deal_short_desc,deals.deal_desc,deals.deal_website, 'deal' as type,avg(reviews.review_rating) as review_rating",FALSE)
                        ->from("deals")
                        ->join("reviews","deals.deal_id = reviews.review_type_id","left")
                        ->where("deal_id = '$id' and reviews.review_type = 'deal'")
                        ->get();
        return $query->row();
    }

}
