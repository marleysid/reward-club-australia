<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Offers extends Front_Controller {

    // public $data;

    function __construct() {
        parent::__construct();
        $this->load->model(array('offer_model', 'categories/category_model', 'advertisement/advertisement_model', 'deals/deal_model', 'region/region_model', 'reviews/review_model','settings/settings_model'));
        $this->load->library('pagination');
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    function index() {
        //debug($_SERVER['QUERY_STRING']);
        $cat = (array) $this->input->get('category');
        $sub_cat = (array) $this->input->get('sub_category');
        $location = array_filter(explode('/', $this->input->get('location')));
        //debug($location);die;
        $keyword = $this->input->get('keyword');
        $per_page = (int) $this->input->get('page');
        $view_type = $this->uri->segment(2) ? $this->uri->segment(2) : 'list';
        $sort_order  = $this->input->get('sort_option') ? $this->input->get('sort_option'):'title' ;
        //debug($view_type);die;
        Template::add_js(get_asset('assets/frontend/js/cart_favourites.js'), TRUE);
        if ($this->uri->segment(3) != '') {
            $config['per_page'] = $this->uri->segment(3);
        } else {
            $config['per_page'] = 8;
        }
        $result = $this->offer_model->search_all($keyword, $cat, $sub_cat, $location, $config['per_page'], $per_page, $sort_order, $keyword);
//        debug($result);die;
        //  echo $this->db->last_query();die;
        // debug($result);die;
        $config['total_rows'] = $this->offer_model->found_rows();
//        $config['base_url'] = site_url() . "information-for-members?{$_SERVER['QUERY_STRING']}";

        $get = $_GET;

        // unset the offset array item
        unset($get['page']);

        $config['base_url'] = current_url() . '/?' . http_build_query($get);
        $config['num_links'] = 5;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='javascript:void(0)'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['page_query_string'] = true;
//        $config['enable_query_strings'] = true;
        $config['query_string_segment'] = 'page';
        $config['first_link'] = 'First';
        $config['first_tag_open'] = '<div>';
        $config['first_tag_close'] = '</div>';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<div>';
        $config['first_tag_close'] = '</div>';



        //echo $this->db->last_query();die;
        $this->pagination->initialize($config);
        if ($result != false) {
            $this->data['get_offers'] = $result;
            //debug($this->data['get_offers']);die;
//            foreach ($result as $res){
//            $testarray[] = array( $res->title, $res->latitude,  $res->longitude);
//            
//            }
//            $this->data['location'] = json_encode($testarray);
            // debug($this->data['location']);die;
        } else {
            $this->data['get_offers'] = array();
        }

//        if(!empty($location)){
//            $this->data['user_location'] = $this->region_model->get_latlon($location[0],$location[1]);
//            //debug($this->data['user_location']);die;
//        } else{
//             $this->data['user_location_lat'] = $suburb;
//             $this->data['user_location_lon'] = $postcode;
//           
//           
//        }
        // echo $this->db->last_query();die;
        $this->data['category'] = $this->category_model->parent_cat();

        //debug($this->data['category']);die;

        $this->data['regions'] = $this->region_model->get_regions();

        // debug($this->data['categories']);
        //echo $this->db->last_query();die;
        $this->data['selected_cat'] = (array) $this->input->get('category');
        $this->data['selected_sub_cat'] = (array) $this->input->get('sub_category');
        $this->data['get_ads'] = $this->advertisement_model->get_left_side_ads();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        
        $this->data['total'] = $config['total_rows'];
        $this->data['per_page'] = $config['per_page'];
        $this->data['offest'] = $per_page;
        
        
        Template::add_css('assets/lib/jquery-raty/jquery.raty.css');
//        Template::add_js('assets/frontend/js/jquery.rateit.js');
        Template::add_js(base_url('assets/lib/jquery-raty/jquery.raty.js'));
        if ($view_type == 'grid') {
            Template::render('frontend/offer_grid_view', $this->data);
        } elseif ($view_type == 'map') {
            Template::render('frontend/map_view', $this->data);
        } else {
            Template::render('frontend/offer_list_view', $this->data);
        }
    }

    function map() {
         $cat = (array) $this->input->get('category');
        $sub_cat = (array) $this->input->get('sub_category');
        $location = array_filter(explode('/', $this->input->get('location')));
         $keyword = $this->input->get('keyword');
         $category_idz = array_merge($cat, $sub_cat);
        
        $category_idz = array_unique(array_filter($category_idz));

        $category_idz = $this->offer_model->_filter_category($category_idz);
        
       
        // debug($location);
        $ip = $this->region_model->get_client_ip();
         // $ip = '116.68.212.122';
          //$ip = '54.252.105.153';
        $add = @unserialize(file_get_contents('http://ip-api.com/php/' . $ip));
        if (!empty($location)) {
            $suburb = $location[0];
        } else {
            $suburb = $add['lat'];
        }
        if (!empty($location)) {
            $postcode = $location[1];
        } else {
            $postcode = $add['lon'];
        }
//        $result = $this->offer_model->nearby_offers($category_idz, $suburb, $postcode,10);
      

        if (!empty($location)) {
            $this->data['user_location'] = $this->region_model->get_latlon($location[0], $location[1]);
             $result = $this->offer_model->nearby_offers($category_idz, $this->data['user_location']->lat, $this->data['user_location']->lon,100);
       
            //debug($this->data['user_location']);die;
        } else {
            $this->data['user_location_lat'] = $suburb;
            $this->data['user_location_lon'] = $postcode;
            $result = $this->offer_model->nearby_offers($category_idz, $this->data['user_location_lat'], $this->data['user_location_lon'],100);
       
        }
        //echo $this->db->last_query();die;
            
            $rewards_lat = $this->settings_model->get_latlon('rewards_club_latitude');
            $rewards_lon = $this->settings_model->get_latlon('rewards_club_longitude');
            $locarray[] = array($rewards_lat->value, $rewards_lon->value);
          if ($result != false) {
            foreach ($result as $res) {
                $locarray[] = array($res->offer_title, $res->oa_latitude, $res->oa_longitude);
            }
            
             //debug($this->data['location']);die;
        } 
            $this->data['location'] = json_encode($locarray);
//        debug($this->data['location']);die;
        $this->data['category'] = $this->category_model->parent_cat();

        //debug($this->data['category']);die;

        $this->data['regions'] = $this->region_model->get_regions();

        // debug($this->data['categories']);
        // echo $this->db->last_query();die;
        $this->data['selected_cat'] = (array) $this->input->get('category');
        $this->data['selected_sub_cat'] = (array) $this->input->get('sub_category');
        $this->data['get_ads'] = $this->advertisement_model->get_left_side_ads();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
     
        Template::render('frontend/map_view', $this->data);
    }

    function favourites() {

        $status = '';
        $id = $this->uri->segment(4);
        $type = $this->uri->segment(3);
        //echo $type;die;
        $member_id = $this->current_member->member_id;
        //  debug(count($this->db->query("select deal_id from deals where deal_id = '$id'")));die;
        //echo $member_id; echo $id;  die;
        //$query = $this->spdb->query("CALL sp_setFavourite(?,?,?)",array($id,$type,$member_id));
        // debug($this->db->query("select offer_id from offers where offer_id = '$id'")->num_rows() > 0);die;
        $fav = $this->offer_model->fav($id, $type, $member_id);
        // echo $this->db->last_query();die;
        //debug($fav);die;
        if ($type == 'offer') {
            if ($this->db->query("select offer_id from offers where offer_id = '$id'")->num_rows() > 0) {
                if ($fav == FALSE) {
                    $this->db->insert("favourite", array('favourite_type' => $type,
                        'favourite_type_id' => $id,
                        'member_id' => $member_id));
                    $status = 1;
                    //$msg = $this->session->flashdata("Added to Favourites List");
                    echo json_encode(array('status' => $status));
                }
            }
        } elseif ($type == 'deal') {
            if (count($this->db->query("select deal_id from deals where deal_id = '$id'")) > 0) {
                //echo 'hi';
                if ($fav == FALSE) {
                    $this->db->insert("favourite", array('favourite_type' => $type,
                        'favourite_type_id' => $id,
                        'member_id' => $member_id));
                    $status = 1;
                    echo json_encode(array('status' => $status));
                } else {
                    $status = 2;
                    echo json_encode(array('status' => $status));
                }
            }
        } else {
            $status = 0;

            echo json_encode(array('status' => $status));
        }
    }

    function cart() {
        $status = '';
        $id = $this->uri->segment(4);
        $type = $this->uri->segment(3);
        $member_id = $this->current_member->member_id;
        $cart = $this->deal_model->cart($id, $member_id);
        $deals_quan = $this->deal_model->deals_quantity($id);

        //echo $this->db->last_query();die;

        if (count($this->db->query("select deal_id from deals where deal_id = '$id'")) > 0) {
            if ($cart == FALSE) {
                if ($deals_quan->deal_quantity >= 1) {
                    $this->db->insert("cart_session", array('deal_id' => $id,
                        'member_id' => $member_id,
                        'quantity' => '1'));

                    // echo $this->db->last_query();die;
                    $this->db->set('deal_quantity', 'deal_quantity -1', FALSE);
                    $this->db->where('deal_id', $id);
                    $this->db->update('deals');
                    //echo $this->db->last_query();die;
                    //echo 'success';die;
                    $status = 1;
                    echo json_encode(array('status' => $status));
                } else {
                    $status = 2;
                    echo json_encode(array('status' => $status));
                }
            } else {
                $status = 3;
                echo json_encode(array('status' => $status));
            }
        } else {
            $status = 0;

            echo json_encode(array('status' => $status));
        }
    }

    function detail() {
        $type = $this->uri->segment(3);
        $id = safe_b64decode($this->uri->segment(4));
        //echo $type;die;
        if ($type == 'deal') {
            $this->data['detail'] = $this->deal_model->get_detail($id);
           // echo $this->db->last_query();die;
            $this->data['count_review'] = $this->review_model->total_review($id, 'deal');
            //echo $this->db->last_query();die;
        } else {
            $this->data['detail'] = $this->offer_model->get_detail($id);
            $this->data['count_review'] = $this->review_model->total_review($id, 'offer');
            //echo $this->db->last_query();die;
        }
        //debug($this->data['detail']);die;
        // echo $this->db->last_query();die;
        $this->data['get_ads'] = $this->advertisement_model->ads_for_detail_page();
        $this->data['category'] = $this->category_model->parent_cat();
        $this->data['regions'] = $this->region_model->get_regions();
        $this->data['selected_cat'] = (array) $this->input->get('category');
        $this->data['selected_sub_cat'] = (array) $this->input->get('sub_category');
        Template::add_css('assets/lib/jquery-raty/jquery.raty.css');
//        Template::add_js('assets/frontend/js/jquery.rateit.js');
        Template::add_js(base_url('assets/lib/jquery-raty/jquery.raty.js'));
        Template::add_js(base_url('assets/frontend/js/cart_favourites.js'));
        Template::render('frontend/detail', $this->data);
    }

}