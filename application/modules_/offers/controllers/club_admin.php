<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Club_admin extends Club_Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('offer_model');
        $this->load->model('categories/category_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $user = $this->ion_auth->user()->row();
        $this->data['offers'] = $this->offer_model->offer_detail($user->club_id);

        Template::render('club_admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_offer(0);
        }

        $this->data['edit'] = FALSE;

        $this->load->model(array('category/category_model'));
        $this->data['categories'] = render_select($this->category_model->get_parent_category(), 'category_id', set_value('category_id', $this->data['edit'] ? $this->data['offer_detail']->category_id : FALSE), 'class="form-control" id="parentcat" ', 'category_id', 'category_name',array('set_top_blank' => true));
//		$this->data['regions'] = render_select($this->region_model->get_all(), 'region_id', set_value('region_id',$this->data['edit'] ? $this->data['company_detail']->region_id : FALSE), 'class="form-control" ', 'region_id', 'region_name');
//       
        $this->data['suburb'] = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());

        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/datepicker.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);



        Template::render('club_admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('club_admin/offers');

        $this->data['offer_detail'] = $this->offer_model->get($id);
        $this->data['address'] = $this->offer_model->get_address($id);
        $this->data['category_info'] = $this->category_model->get($this->data['offer_detail']->category_id);
        $this->data['suburb'] = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());

        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());

        $this->data['offer_detail'] || redirect('club_admin/offers/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_offer($id);
        }

        //debug($this->category_model->get_category_detail($id));die;
        if ($this->data['category_info']->category_parent_id == 0) {
            $this->data['categories'] = render_select($this->category_model->get_category_detail(), 'category_id', $this->data['edit'] ? $this->data['offer_detail']->category_id : FALSE, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank'=> true));
            $this->data['sub_categories'] = render_select($this->category_model->get_category_detail($this->data['category_info']->category_id), 'category_id',  FALSE, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank'=> true));
           
            
        } else {
            $this->data['categories'] = render_select($this->category_model->get_category_detail(), 'category_id', $this->data['edit'] ? $this->data['category_info']->category_parent_id : FALSE, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank'=> true));
            $this->data['sub_categories'] = render_select($this->category_model->get_category_detail($this->data['category_info']->category_parent_id), 'category_id', $this->data['edit'] ? $this->data['offer_detail']->category_id : FALSE, 'class="form-control" id="parentcat" ', 'category_id', 'category_name', array('set_top_blank'=> true));
        
             
        }


        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/datepicker.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/moment-with-locales.js", TRUE);
        Template::add_js(base_url() . "assets/js/bootstrap-datepicker.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);


        Template::render('admin/form', $this->data);
    }

    function _add_edit_offer($id) {
        //debug($this->input->post('offer_image_path'));
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        // debug($this->input->post());die;
        $user = $this->ion_auth->user()->row();
        // debug($user->club_id);die;
        $data['club_id'] = $user->club_id;
        $data['offer_title'] = $this->input->post('offer_title');
        // debug($this->offer_model->user_detail($user->id));die;
        if ($this->input->post('sub_category_id') == null) {
            $data['category_id'] = $this->input->post('category_id');
        } else {
            $data['category_id'] = $this->input->post('sub_category_id');
        }
        $data['offer_short_desc'] = $this->input->post('offer_short_desc');
        $data['offer_desc'] = $this->input->post('offer_desc');
        $data['offer_tag'] = $this->input->post('offer_tag');
        $data['offer_valid_date'] =date("Y-m-d", strtotime($this->input->post('offer_valid_date')) );


//        $data = $this->input->post();
        $this->form_validation->set_rules(
                array(
                    array('field' => 'offer_title', 'label' => 'Title', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'category_id', 'label' => 'Category', 'rules' => 'required'),
                    array('field' => 'offer_short_desc', 'label' => 'Short Description', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'offer_desc', 'label' => ' Description', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'offer_tag', 'label' => ' Tag', 'rules' => 'required|trim|min_length[5]|xss_clean'),
                    array('field' => 'offer_valid_date', 'label' => ' Date', 'rules' => 'required'),
                    array('field' => 'offer_status', 'label' => 'Status', 'rules' => 'trim'),
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }

        $offer_image_path = NULL;
        if (isset($_FILES['offer_image']['name']) and $_FILES['offer_image']['name'] != '') {
            if ($result = upload_image('offer_image', config_item('offer_image_root'), FALSE)) {
                $image_path = config_item('offer_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
////				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'));
//                }
                $offer_image_path = $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
            
        }elseif($id == 0) {
            
                $this->data['logo_error'] = '<span class="help-block">Image is required</span>';
                return FALSE;
            
        }

        unset($data['offer_image']);

        !$offer_image_path || ($data['offer_image'] = $offer_image_path);

        $data['offer_status'] = $this->input->post('offer_status') ? '1' : '0';

        if ($id == 0) {
            // echo 'test';die;
            $offer_id = $this->offer_model->insert($data);
            // echo $this->db->last_query();die;
            $offer_address = $this->input->post('offer_address');
            $offer_latitude = $this->input->post('offer_lat');
            $offer_longitude = $this->input->post('offer_lon');
            $offer_phone = $this->input->post('offer_phone');
            $offer_post_code = $this->input->post('offer_post_code');
            $offer_suburb = $this->input->post('offer_suburb');

            $this->offer_address($offer_id, $offer_address, $offer_latitude, $offer_longitude, $offer_phone, $offer_post_code, $offer_suburb);

            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_logo = NULL;
            if ($offer_image_path) {
                $old_logo = $this->offer_model->get($id)->offer_image;
            }
            $this->offer_model->update($id, $data);
            if ($old_logo and file_exists(config_item('offer_image_root') . $old_logo))
                unlink_file(config_item('offer_image_root') . $old_logo);

            $this->offer_model->delete_address($id);
            $offer_address = $this->input->post('offer_address');
            $offer_latitude = $this->input->post('offer_lat');
            $offer_longitude = $this->input->post('offer_lon');
            $offer_phone = $this->input->post('offer_phone');
            $offer_post_code = $this->input->post('offer_post_code');
            $offer_suburb = $this->input->post('offer_suburb');


            $this->offer_address($id, $offer_address, $offer_latitude, $offer_longitude, $offer_phone, $offer_post_code, $offer_suburb);

            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('club_admin/offers/', 'refresh');
    }

    function offer_address($id, $offer_address, $offer_latitude, $offer_longitude, $offer_phone, $offer_post_code, $offer_suburb) {

        $batchArray = array();

        if (is_array($offer_address)) {
            foreach ($offer_address as $k => $add) {

                $batchArray[$k]['oa_address'] = $add;
                $batchArray[$k]['offer_id'] = $id;
            }
        }
        if (is_array($offer_latitude)) {
            foreach ($offer_latitude as $k => $lat) {
                $batchArray[$k]['oa_latitude'] = $lat;
            }
        }
        if (is_array($offer_longitude)) {
            foreach ($offer_longitude as $k => $lon) {
                $batchArray[$k]['oa_longitude'] = $lon;
            }
        }
        if (is_array($offer_phone)) {
            foreach ($offer_phone as $k => $phone) {
                $batchArray[$k]['oa_phone'] = $phone;
            }
        }
        if (is_array($offer_post_code)) {
            foreach ($offer_post_code as $k => $offer_post_code) {
                $batchArray[$k]['oa_post_code'] = $offer_post_code;
            }
        }
        if (is_array($offer_suburb)) {
            foreach ($offer_suburb as $k => $offer_suburb) {
                $batchArray[$k]['oa_suburb'] = $offer_suburb;
            }
        }
        if (!empty($batchArray))
            $this->db->insert_batch('offer_address', $batchArray);
    }

    function sub_cat_info() {
        $id = $this->uri->segment(4);
        $data = $this->category_model->get_subcategory($id);
        $result = '';
        $result = '<select id="ChildCat" class="form-control" name="sub_category_id">';
         $result .= '<option value=""></option>';
           
        foreach ($data as $c) {
            $result .= '<option value="' . $c->category_id . '">' . $c->category_name . '</option>';
        }
        $result.='</select>';
        echo $result;
    }

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->offer_model->update($id, array('offer_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function toggle_review_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->load->model(array('review_model'));
            $this->review_model->update($id, array('status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function delete($id) {
        $old_logo = $this->offer_model->get($id)->offer_image;

        if ($this->offer_model->delete($id)) {
            if ($old_logo and file_exists(config_item('offer_image_root') . $old_logo))
                unlink_file(config_item('offer_image_root') . $old_logo);
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('club_admin/offers/', 'refresh');
    }

    function reviews($company_id = NULL) {
        $company_id || redirect('admin/clubs');

        $this->load->model(array('review_model'));

        $this->data['reviews'] = $this->review_model->find_all_by('company_id', $company_id);
        $this->data['companies'] = render_select($this->company_model->get_all(), 'company_id', $company_id, 'style="width:225px; display:inline;" class="form-control" onchange=\' window.location.href = "' . base_url('admin/company/reviews') . '/"+this.value; \' ', 'company_id', 'company_name');

        Template::add_js(base_url() . "assets/lib/jquery.showmore.min.js", TRUE);
        Template::render('admin/company_reviews', $this->data);
    }

}
