<?php

if(!defined('BASEPATH')) exit('No direct script access allowed');

class Offer_model extends MY_Model
{

    public $_search_total = 0;
    public $_order_by = 0;

    public function __construct()
    {
        parent::__construct();
        $this->table = 'offers';
        $this->field_prefix = 'offer_';
        $this->log_user = FALSE;
    }

    function get_all($return_type = 0)
    {
//		$this->select('rewards_company.*,rewards_category.*,concat(company_address," ", suburb," ",state," ",postcode) AS company_address', FALSE)
//				->join('suburbs', 'suburbs.id = rewards_company.suburb_id')
//				->join('rewards_category', 'rewards_category.category_id = rewards_company.category_id');
//		return parent::get_all($return_type);
        $this->db->select('offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_image`,offers.`offer_status`, category.`category_name`');
        $this->db->from('offers');
        $this->db->join('category', 'offers.category_id=category.category_id ', 'inner');
        $this->db->where("offers.club_id = '0'");
        $query = $this->db->get();
        return $query->result();
    }

    function offer_detail($id)
    {
        $this->db->select('offers.`offer_id`,offers.`offer_title`,offers.`offer_short_desc`,offers.`offer_image`,offers.`offer_status`, category.`category_name`');
        $this->db->from('offers');
        $this->db->join('category', 'offers.category_id=category.category_id ', 'inner');
        $this->db->where("offers.club_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }

    function slider_detail($id)
    {
        $this->db->select("*")
                ->from("slider")
                ->where("slider_type_id = '$id' ");

        $query = $this->db->get();

        return $query->row();
    }

    function delete_address($id)
    {
        return $query = $this->db->delete("offer_address", array("offer_id" => $id));
    }

    function get_address($id)
    {
        $this->db->select("*")
                ->from("offer_address")
                ->where("offer_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }

    function get_suburb()
    {
        $query = $this->db->query("SELECT DISTINCT suburb, id FROM suburbs ORDER BY id ");
        return $query->result();
    }
    
      function suburb()
    {
        $query = $this->db->query("SELECT DISTINCT suburb, id FROM suburbs ORDER BY id limit 8");
        return $query->result();
    }

    function get_postcode()
    {
        $query = $this->db->query("SELECT DISTINCT postcode, id FROM suburbs ORDER BY id");
        return $query->result();
    }

    function get_state()
    {
        $query = $this->db->query("SELECT DISTINCT state, id FROM suburbs ORDER BY id ");
        return $query->result();
    }
    
    function state()
    {
        $query = $this->db->query("SELECT DISTINCT state, id FROM suburbs ORDER BY id limit 8");
        return $query->result();
    }

    function search($keyword, $category_idz, $postcode, $suburb, $latitude, $longitude, $limit, $offset, $order, $keyword_search = FALSE)
    {

        $order_list = array('alpha', 'nearest', 'new');
        $cat_idz = array();

        $order = in_array($order, $order_list) ? $order : 'alpha';

        switch($order) {
            case 'alpha' :
                $order_by = 'offer_title';
                break;

            case 'nearest' :
                $order_by = 'distance';
                break;

            case 'new' :
                $order_by = 'offer_id desc';
                break;

            default :
                $order_by = 'offer_title';
                break;
        }

        $this->_order_by = $order;

        $category_idz = $this->_filter_category($category_idz);

        if(!$latitude and ! $longitude and ! empty($postcode) and ! empty($suburb)) {

            if($suburb_postcode = $this->db->get_where('suburbs', array('postcode' => $postcode, 'suburb' => $suburb), 1)->row()) {
                $latitude = $suburb_postcode->lat;
                $longitude = $suburb_postcode->lon;
            }
        }

        $select = "SQL_CALC_FOUND_ROWS "
                ."offers.offer_id"
                .", offer_title"
                .',IF( offer_image IS NOT NULL AND offer_image <> "" ,  concat("'.base_url(config_item('offer_image_path')).'/",offer_image), "") as offer_image'
                .", offer_short_desc"
                .", offer_tag"
                .", DATE_FORMAT(`offer_valid_date`,'%d/%m/%Y') as offer_valid_date"
                .", 1 as offer_new"
                .
                (($latitude and $longitude) ?
                        "
                ,   MIN(IF( oa_latitude IS NOT NULL AND oa_longitude IS NOT NULL,
                            (((acos(sin(({$latitude}*pi()/180)) * 
                            sin((oa_latitude*pi()/180))+cos(({$latitude}*pi()/180)) * 
                            cos((oa_latitude*pi()/180)) * cos((({$longitude} - oa_longitude)
                            *pi()/180))))*180/pi())*60*1.1515*1.609344)
                            , 999999999 ))
                            as distance
                " : ', 999999999 as distance'
                );

        $this->db->select($select, FALSE);

        $this->db->join('offer_address', 'offers.offer_id = offer_address.offer_id', 'left');

        $where = "";
        $filters = array();

        if($keyword) {
            if(!$keyword_search) {
                $filters[] = " `offer_title` like '%{$this->db->escape_like_str($keyword)}%' ";
                $filters[] = " `offer_desc` like '%{$this->db->escape_like_str($keyword)}%' ";
                $filters[] = " `offer_short_desc` like '%{$this->db->escape_like_str($keyword)}%' ";
            } else {
                $filters[] = " `offer_title` like '{$this->db->escape_like_str($keyword)}%' ";
            }
        }
        $where = count($filters) > 0 ? "(".implode(' OR ', $filters).")" : "";

        if(!empty($where)) $this->db->where($where, NULL, FALSE);

        if(is_array($category_idz) and ! empty($category_idz)) $this->db->where_in('category_id', $category_idz);

        if(!empty($postcode)) $this->db->where('oa_post_code', $postcode);

        if(!empty($suburb)) $this->db->where('oa_suburb', $suburb);


        $this->db->group_by('`offers`.offer_id');
        $this->db->order_by($order_by);

        $result = $this->db->get('offers', $limit, $offset);
        $return = $result->num_rows() > 0 ? $result->result() : FALSE;

        $this->_search_total = $this->db->query('SELECT FOUND_ROWS() as total')->row()->total;

        return $return;
    }

    function nearby_offers($category_idz, $latitude, $longitude, $max_distance)
    {
        $cat_idz = array();
        $category_idz = $this->_filter_category($category_idz);

        $latitude = (double) $latitude;
        $longitude = (double) $longitude;
        $max_distance = (int) $max_distance;

        $select = ""
                ."offers.offer_id"
                .", offer_title"
                .", oa_latitude"
                .", oa_longitude"
                ."
                ,   (IF( oa_latitude IS NOT NULL AND oa_longitude IS NOT NULL,
                            (((acos(sin(({$latitude}*pi()/180)) * 
                            sin((oa_latitude*pi()/180))+cos(({$latitude}*pi()/180)) * 
                            cos((oa_latitude*pi()/180)) * cos((({$longitude} - oa_longitude)
                            *pi()/180))))*180/pi())*60*1.1515*1.609344)
                            , 999999999 ))
                            as distance
                "
        ;

        $this->db->select($select, FALSE);

        $this->db->join('offer_address', 'offers.offer_id = offer_address.offer_id', 'left');

        if(is_array($category_idz) and ! empty($category_idz)) $this->db->where_in('category_id', $category_idz);

        $result = $this->db->having('distance <= ', $max_distance);

        $result = $this->db->get('offers');
//        echo $this->db->last_query();
//        debug($result->result());die;
        return $result->num_rows() > 0 ? $result->result() : FALSE;
    }

    function _filter_category($category_idz)
    {
        if(is_array($category_idz) and ! empty($category_idz)) {

            $categories = array();
            $sub_categories = array();

            $categories = array_map(function ($arg) {
                return $arg->category_id;
            }, $this->db->select('category_id')->where_in('category_id', $category_idz)->where('category_parent_id', 0)->get('category')->result());

            if(!empty($categories)) $sub_categories = array_map(function ($arg) {
                    return $arg->category_id;
                }, $this->db->select('category_id')->where_in('category_parent_id', $categories)->get('category')->result());

            return array_unique(array_filter(array_merge($categories, $sub_categories)));
        } else {
            return array();
        }
    }

    function search_offers($limit, $offset, $cat, $sub_cat, $location, $keyword)
    {
        if(!empty($cat)) {
            $this->db->where_in('category.category_id', $cat);
        }
        if(!empty($sub_cat)) {
            $this->db->where_in('category.category_id', $sub_cat);
        }
        $query = $this->db->select('SQL_CALC_FOUND_ROWS offers.offer_id AS id,
                                        offers.offer_title AS title,
                                        offers.offer_image AS image,
                                        offers.offer_short_desc AS short_desc,
                                        offers.offer_desc AS description ,category.category_name', FALSE)
                ->from('offers')
                ->join('offer_address', 'offers.offer_id = offer_address.offer_id', 'left')
                ->join('category', 'offers.category_id = category.category_id', 'left')
                ->where('(offers.offer_title like "%'.$keyword.'%"
                                       or offers.offer_short_desc like "%'.$keyword.'%"
                                       or offers.offer_desc like "%'.$keyword.'%" )'
                        .'and (offer_address.oa_post_code like "%'.$location.'"'
                        .'or offer_address.oa_suburb like "%'.$location.'")')
                ->limit($limit, $offset)
                ->order_by('offers.offer_title')
                ->get();



        if($query->num_rows() > 0) {

            return $query->result();
        } else {
            return false;
        }
    }

    function search_all($keyword, $cat, $sub_cat, $location,$limit, $offset,  $order, $keyword_search = FALSE)
    {
        $order_list = array('alpha', 'nearest', 'new');
        $cat_idz = array();

        $order = in_array($order, $order_list) ? $order : 'alpha';

        switch($order) {
            case 'alpha' :
                $order_by = 'title';
                break;

            case 'nearest' :
                $order_by = 'distance';
                break;

            case 'new' :
                $order_by = 'id desc';
                break;

            default :
                $order_by = 'title';
                break;
        }
        $special_deals = FALSE;
        
        if(($key = array_search('special_deals', $cat)) !== false) {
            $special_deals = TRUE;
            unset($cat[$key]);
        }
        
        $category_idz = array_merge($cat, $sub_cat);
        
        $category_idz = array_unique(array_filter($category_idz));

        $category_idz = $this->_filter_category($category_idz);
        
        $latitude = NULL;
        $longitude = NULL;
        
        $select = " SQL_CALC_FOUND_ROWS "
                ." id "
                .", title "
                .', image'
                .", short_desc"
                .", description"                
                .", website"
                .", tag"
                .", category_id"
                .", valid_date"
                .", price"
                .", left_tag"
                .", quantity"
                .", status"
                .", created_date"
                .", type"
                .", avg(reviews.review_rating) as review"
                .", count(reviews.review_rating) as total_review"  
                .
                (($latitude and $longitude) ?
                        "
                ,   MIN(IF( oa_latitude IS NOT NULL AND oa_longitude IS NOT NULL,
                            (((acos(sin(({$latitude}*pi()/180)) * 
                            sin((oa_latitude*pi()/180))+cos(({$latitude}*pi()/180)) * 
                            cos((oa_latitude*pi()/180)) * cos((({$longitude} - oa_longitude)
                            *pi()/180))))*180/pi())*60*1.1515*1.609344)
                            , 999999999 ))
                            as distance
                " : ', 999999999 as distance'
                );                   

        $this->db->select($select, FALSE);

        $this->db->join('offer_address', 'offer_deal.id = offer_address.offer_id AND offer_deal.type = "offer"', 'left');
        $this->db->join('reviews', 'offer_deal.id = reviews.review_type_id AND offer_deal.type = reviews.review_type ', 'left');
        
        $where = "";
        $filters = array();

        if($keyword) {
            if(!$keyword_search) {
                $filters[] = " `title` like '%{$this->db->escape_like_str($keyword)}%' ";
                $filters[] = " `description` like '%{$this->db->escape_like_str($keyword)}%' ";
                $filters[] = " `short_desc` like '%{$this->db->escape_like_str($keyword)}%' ";
            } else {
                $filters[] = " `title` like '{$this->db->escape_like_str($keyword)}%' ";
            }
        }
        
        $where = count($filters) > 0 ? "(".implode(' OR ', $filters).")" : "";

        if(!empty($where)) $this->db->where($where, NULL, FALSE);

        if(is_array($category_idz) and ! empty($category_idz)) {
            $this->db->where_in('category_id', $category_idz);
            
            $this->db->where('type','offer');
        }
        if($special_deals) {
            $this->db->where('type','deal');

        }

        if(!empty($postcode)) $this->db->where('oa_post_code', $postcode);

        if(!empty($suburb)) $this->db->where('oa_suburb', $suburb);


        $this->db->group_by('`offer_deal`.id, offer_deal.type');
        $this->db->order_by($order_by);

        $result = $this->db->get('offer_deal', $limit, $offset);
        $return = $result->num_rows() > 0 ? $result->result() : FALSE;

        return $return;
        
        
        $join_offer_arr[] = " LEFT JOIN offer_address ON offers.`offer_id` = offer_address.`offer_id` and type= 'offer'";
        
                
        $query = $this->db->query('(SELECT 
  offers.offer_id AS id,
  offers.offer_title AS title,
  offers.offer_image AS image,
  offers.offer_short_desc AS short_desc,
  offers.offer_desc AS description,
  offer_address.oa_latitude as latitude,
  offer_address.oa_longitude as longitude,
  "offer" as type,
  avg(reviews.review_rating) as review,
  count(reviews.review_rating) as total_review
FROM
  offers 
  LEFT JOIN offer_address 
    ON offers.`offer_id` = offer_address.`offer_id` 
  LEFT JOIN category 
    ON offers.`category_id` = category.`category_id`
    left join reviews
    on offers.offer_id = reviews.review_type_id and reviews.review_type = "offer"
WHERE (
    offers.offer_title LIKE "%' . $keyword . '%" 
    OR offers.offer_short_desc LIKE "%' . $keyword . '%" 
    OR offers.offer_desc LIKE "%' . $keyword . '%"
  ) 
  AND (
    offer_address.oa_suburb LIKE "%' . $suburb . '%" 
    and offer_address.oa_post_code LIKE "%' . $postcode . '%")
        OR(category.category_id IN ("'.$cat.'"))
        OR(category.category_id IN ("'.$sub_cat.'"))
            
and 
offers.offer_status != "0"
    
group by offers.offer_id
  ) 
UNION
(SELECT 
  deals.`deal_id`,
  deals.`deal_title`,
  deals.`deal_image_path`,
  deals.`deal_short_desc`,
  deals.deal_desc,
  (select value from settings where settings.key = "rewards_club_latitude") as latitude,
  (select value from settings where settings.key = "rewards_club_longitude") as longitude,
  "deal" as type,
  avg(reviews.review_rating) as review,
  count(reviews.review_rating) as total_review
FROM
  deals
  left join reviews
    on deals.deal_id = reviews.review_type_id and reviews.review_type = "deal"
  WHERE 
    deals.`deal_title` LIKE "%' . $keyword . '%" 
    OR deals.`deal_short_desc` LIKE "%' . $keyword . '%" 
    OR deals.`deal_desc` LIKE "%' . $keyword . '%"
    and deals.deal_status != "0"
   group by deals.deal_id       
  )
  
  LIMIT ' . $offset . ',' . $limit . '
  ',FALSE);

        if ($query->num_rows() > 0) {

            return $query->result();
        } else {
            return false;
        }
    }
    
    
  function fav($id,$type,$member_id){
      $query = $this->db->select("favourite_type_id")
                        ->from("favourite")
                        ->where("favourite_type_id = '$id' and favourite_type = '$type' and member_id = '$member_id'")
                        ->get();
      if($query->num_rows() > 0){
          return TRUE;
      }
      else{
          return FALSE;
      }
  }
    function get_detail($id){
        $query = $this->db->select("offers.offer_id,offers.offer_title,offers.offer_image,offers.offer_short_desc,offers.offer_desc,offers.offer_website,offers.offer_tag, 'offer' as type, avg(reviews.review_rating) as review_rating",FALSE)
                        ->from("offers")
                        ->join("reviews","offers.offer_id = reviews.review_type_id","left")
                        ->where("offers.offer_id = '$id'and reviews.review_type= 'offer'")
                        ->get();
        
        return $query->row();
    }

}
