
<form method="get" action="<?php echo site_url('offers') ?>" id="refinesearchform">
                <div class="local-search club">
                    <h4>Keyword</h4>
                    <!--<form role="form">-->
                        <div class="form-group">
                            <input type="Phone" class="form-control"  placeholder="Business Name/Product/Offer" id="keyword" value="<?php echo $this->input->get('keyword');?>" name="keyword">

                        </div>
                    <!--</form>-->
                </div>
                <br />
               
                <div class="local-search club">
                    <h4>Search a different region</h4>
                    <!--<form role="form">-->
                        <div class="form-group">
                            <label class="lbl_cls">Region</label>

                                <select class="form-control input_txt" name="region">
                                    <option value="">Select a Region</option>
                                    <?php
                                    if ($regions):
                                        foreach ($regions as $value):
                                            echo '<option value="' . $value->region_id . '" '.(($this->input->get('region') and $this->input->get('region') == $value->region_id) ? ' selected="selected" ' : ' ' ).' >' . $value->region_name . '</option>';
                                        endforeach;
                                    endif;
                                    ?>
                                </select>

                        </div>
                    <!--</form>-->
                </div>
                <br />
                <div class="local-search club refine_search">
            	<h4>Refine Search</h4>
                <ul id="nav">
                    <li>
                        <a href="javascript:void(0)"><input type="checkbox" name="category[]" value="<?php echo 'special_deals'; ?>" <?php if(in_array('special_deals',$selected_cat)){ echo "checked"; }?> class="srch_bt_bx"><img src="<?php echo base_url('assets/frontend/images/gft.png') ?>">Special deals</a>
                    </li>
                      <?php if(isset($category)): ?>
                    <?php foreach($category as $cat): ?>
              
                <li>
                  
                    <a href="javascript:void(0)"><input type="checkbox" name="category[]" value="<?php echo $cat->category_id; ?>" <?php if(in_array($cat->category_id,$selected_cat)){ echo "checked"; }?> class="srch_bt_bx"><img src="<?php echo imager($cat->category_icon ? 'assets/uploads/category/'.$cat->category_icon:'', 25, 13); ?>"><?php echo $cat->category_name; ?></a>
                <?php  $sub_category = $this->category_model->get_subcategory($cat->category_id);

                ?>	
                     <?php if(count($sub_category) > 0): ?>
                          
                    <ul class="subs">
                             <?php foreach($sub_category as $sub_cat): ?>
                        <li><a href="javascript:void(0)"><input type="checkbox" name="sub_category[]" value="<?php echo $sub_cat->category_id; ?>" <?php if(in_array($sub_cat->category_id,$selected_sub_cat) ) { echo "checked";} ?> class="srch_bt_bx"><img src="<?php echo imager($sub_cat->category_icon ? 'assets/uploads/category/'.$sub_cat->category_icon:'', 25, 13); ?>"><?php echo $sub_cat->category_name; ?></a></li>
                          <?php endforeach; ?>
                        </ul>
                        <?php endif; ?>
                   
                </li>
                 <?php endforeach; ?>
                   <?php endif; ?>
                <input type="hidden" name="sort_option" value="" id="sort_option">
                <li>
                     <input type="submit" class="btn btn-primary btn-danger rgt_pp_btn" value="Search" />
              </li>
                 </ul>
            </div>  
                <br />
                <br />
                <?php  if(isset($get_ads->ad_image_path)):?>
                <div class="pull-left"> <img src="<?php echo imager($get_ads->ad_image_path ? 'assets/uploads/ad/' . $get_ads->ad_image_path : '', 278, 268); ?>" class="img-responsive"> </div>
                <?php endif; ?>
              </form>