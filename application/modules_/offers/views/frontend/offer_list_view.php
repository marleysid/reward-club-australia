<div class="banner">
    <div class="slider"><img src="<?php echo get_asset('assets/frontend/images/banner_of_club_member.jpg'); ?>"  class="img-responsive" alt="slide"></div>
    <div class="banner-slider">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
            <ul>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="container"> 
        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="#">Library</a></li>
            <li class="active">Data</li>
        </ol>
        <div class="row">
            <div class="col-sm-3">
                <?php $this->load->view('refine_search'); ?>

            </div>
            <div>

            </div>
            <div class="col-sm-9">

                <div class="col-md-12 bg_nn_pgmrgn">
                    <div class="col-sm-3 pdn_mrgn_nn">
                        <h5>Showing <?php echo ($offest) ? ($offest):'1' ?> - <?php echo (($offest + $per_page)< $total) ? ($offest+$per_page):$total ?> of <?php echo $total ?> total results</h5>
                    </div>
                    <div class="col-sm-5 pdn_mrgn_nn">
                        <ul class="deal_page_map">
                            <li><strong>View as</strong></li>
                            <li>
                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/grid?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-large"></span> </button>
                                Grid
                            </li>
                            <li>
                                <button type="button"  onclick="window.location.href = '<?php echo site_url() . 'offers/list?' . $_SERVER['QUERY_STRING']; ?>'" class="active btn btn-default btn-sm"> <span class="glyphicon glyphicon-list"></span> </button>
                                List</li>
                            <li>Map
                                <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/map?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-map-marker"></span> </button>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-4 ">
                        <div class="local-search club bg_nn_pgmrgn">
                            <form role="form">
                                <div class="form-group">
                                    <label class="lbl_cls">Sort By</label>
                                    <select class="form-control input_txt clr" id="sortby">
                                        <option value="new" >New</option>
                                        <option value="alpha" >Alphabetical</option>
                                        <option value="nearest" >Nearest</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <!--repeated start here -->
                <?php if (isset($get_offers)): ?>
                    <?php foreach ($get_offers as $offers): ?>

                        <div class="col-md-12 bg_nn_pgmrgn bt_mrgn_wrp">
                            <div class="col-md-3 bg_nn_pgmrgn tp_btn_mrgn_wrp"> <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price" ><span><?php echo $offers->tag; ?></span>
                                <a href="<?php echo site_url('offers/detail/' . $offers->type . '/' . safe_b64encode($offers->id)); ?>" class="best_img_sprt_clr_cgng"><img src="<?php if ($offers->type == 'offer') {
                    echo imager($offers->image ? 'assets/uploads/offer_image/' . $offers->image : '', 223, 158, 2);
                } else {
                    echo imager($offers->image ? 'assets/uploads/deal_image/' . $offers->image : '', 223, 158, 2);
                } ?>" class="img-responsive"></a> </div>
                            <div class="col-md-9 with_resuld_board bg_grw_01">
                                <div class="col-md-9 whit_box cmn_wrp_wht_pdgn">

                                    <h4><?php echo $offers->title; ?> </h4>
                                    <p><?php echo word_limiter($offers->short_desc, 45); ?> </p>
                                </div>
                                <div class="col-md-3 whit_box fix_with_right flt_right_wrp">
                                    <span  data-score="<?php echo $offers->review; ?>" class="rates"></span>
                                    <h4 class="rating"> <br />
                                    <?php echo $offers->total_review; ?> Reviews </h4>
        <?php if (isset($userLoggedIn) and $userLoggedIn): ?>
                                        <!--<a href="#" class="btn btn-danger">View Website &nbsp;&nbsp;<span class="glyphicon glyphicon-paperclip"></span></a>-->
                                        <button class="view_website btn fx_font_siz heart_bg "> Favourite this <span class="glyphicon glyphicon-heart favourite" data-type="<?php echo $offers->type; ?>" data-id="<?php echo $offers->id; ?>"></span> </button>
        <?php else: ?>
                                        <button class="view_website btn fx_font_siz heart_bg " data-toggle="modal" data-target="#loginModal" data-whatever="@getbootstrap">Favourite this<span class="glyphicon glyphicon-heart "></span></button>

                        <?php endif; ?>
              <!--<a href="" target="_blank"><button class="view_website btn fx_font_siz"> View Website <span class="glyphicon glyphicon-arrow-right"></span> </button></a>-->
                                </div>
                            </div>
                        </div>
    <?php endforeach; ?>
<?php endif; ?>

                <!--repeated end here -->
                <!--pagination start here -->
                <div class="col-md-12">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-9 bg_nn_pgmrgn">
                        <div class="col-md-7 pgn_wrp_clr">
                            <ul class="pagination">
<?php echo $this->pagination->create_links(); ?>
                                <!--                                <li><button type="button" class="btn btn-default btn-sm">
                                          <span class="glyphicon glyphicon-arrow-left"></span>
                                        </button></li>
                                                                <li><a href="#">1</a></li>
                                                                <li><a href="#">2</a></li>
                                                                <li><a href="#">3</a></li>
                                                                <li><a href="#">4</a></li>
                                                                <li><a href="#">5</a></li>
                                                                <li><button type="button" class="btn btn-default btn-sm">
                                          <span class="glyphicon glyphicon-arrow-right"></span>
                                        </button></li>-->
                            </ul>
                        </div>
                        <div class="col-md-4 bg_nn_pgmrgn pull-righ">
                            <div class="btn-group" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <h4 class="margin_right_wrp_bxt_itm">Items per page:</h4>
                                    <button type="button"  class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?php echo $this->uri->segment(3) ? $this->uri->segment(3):'8' ?> <span class="caret"></span> </button>
                                    <ul class="dropdown-menu insrt_with" role="menu">
                                        <li><a href="<?php echo site_url() . 'offers/list/8?' . $_SERVER['QUERY_STRING']; ?>" >8</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/list/10?' . $_SERVER['QUERY_STRING']; ?>" >10</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/list/14?' . $_SERVER['QUERY_STRING']; ?>" >14</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/list/16?' . $_SERVER['QUERY_STRING']; ?>" >16</a></li>

                                    </ul>
                                </div>


                            </div>
                        </div>

                    </div>
                </div>
                <!--pagination end here -->
            </div>
        </div>
    </div>
    <div class="feature-service">
        <div class="container">
<?php $this->load->view('deals/frontend/bottom_slider'); ?>
        </div>
    </div>
</div>
<script>
    $(function() {
        $('.rates').raty({path: base_url + 'assets/lib/jquery-raty/images',
            readOnly: true,
            score: function() {
                return $(this).attr('data-score');
            }});

        $('#sortby').on('change', function() {
            var option = $(this).val();
            //alert (option);
            $('#sort_option').val(option);
            $('.rgt_pp_btn').trigger('click');

        });
    });

</script>
