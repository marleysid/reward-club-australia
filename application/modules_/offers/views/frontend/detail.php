<div class="container">
	<!-- breadcrumbs -->
	<ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>">Home</a></li>
  	<li><a href="#">Library</a></li>
  	<li class="active">Data</li>
	</ol>


	<div class="row">
		<div class="col-sm-3">
               <?php $this->load->view('refine_search'); ?>
        </div>
           
		<div class="col-sm-9">
			<div class="info-block pull-left">
                        <img src="<?php if($detail->type == 'deal'){echo imager($detail->deal_image_path ? 'assets/uploads/deal_image/'.$detail->deal_image_path:'',335,225);} else{echo imager($detail->offer_image ? 'assets/uploads/offer_image/'.$detail->offer_image:'',335,225);} ?>">
			<div class="info-links">
            <div class="pull-left">
                <span  data-score="<?php echo $detail->review_rating; ?>" class="rating"></span>
          <br /><br /><?php echo $count_review->total_review; ?> Reviews</div>
				<div class="pull-right">
                                      <?php if (isset($userLoggedIn) and $userLoggedIn): ?>
                                    <!--<a href="#" class="btn btn-danger">View Website &nbsp;&nbsp;<span class="glyphicon glyphicon-paperclip"></span></a>-->
                                    <a href="#" class="btn btn-default">Favourite this&nbsp;&nbsp;<span class="glyphicon glyphicon-heart favourite" data-type="<?php echo $detail->type; ?>" data-id="<?php echo ($detail->type == 'deal') ? $detail->deal_id: $detail->offer_id; ?>"></span></a>
                                    <?php else: ?>
                                    <a class="favourites btn btn-default" data-toggle="modal" data-target="#loginModal" data-whatever="@getbootstrap">Favourite this&nbsp;&nbsp;<span class="glyphicon glyphicon-heart favourite" data-type="<?php echo $detail->type; ?>"></span></a>
                                <?php endif; ?>
                                </div>
			</div>
			</div>
                    <h1 class="page-title"><?php echo ($detail->type == 'deal') ?  ucwords($detail->deal_title):  ucwords($detail->offer_title); ?></h1>
<!--			<h5 class="sub-title">Bowral, 4/350 Bong Bong Street (Also see Dapto, Nowra, Wollongong, Shellharbour & Campbelltown.)</h5>
			<p>*Simply show your Club Member card to save 20% on frames, lenses, contacts, accessorises, prescription and non-prescription sunglasses from OPSM's $400 or below range or up to $80 off OPSM's above $400 range. </p>
			<p>Offer excludes eye tests, gift cards, Chanel, Tiffany &nbsp; Co., Bvlgari, Oliver Peoples, Paul Smith, Tag Heuer products and Gold & Wood products. Cannot be used in conjunction with special offer packages sale items, health fund associated discounts or any other discount or benefit from any source other than a rebate from a health fund.</p>
			<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium  esta gone Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab </p>
			<blockquote>“Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa 		    quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit 		    aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. ”</blockquote>
			<p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut </p>-->
                        <?php echo ($detail->type == 'deal') ? $detail->deal_desc: $detail->offer_desc; ?>
          <div class="col-md-12">
               <?php if(isset($get_ads)): ?>
                  <?php foreach ($get_ads as $ads): ?>
              <div class="col-md-4"> 
                  <div class="billing_available"> 
                      <img src="<?php echo imager($ads->ad_image_path ? 'assets/uploads/ad/'.$ads->ad_image_path:'',267,258 ); ?>" class="img-responsive">
            </div>
              </div>
              <?php endforeach; ?>
              <?php endif; ?>
<!--              <div class="col-md-4">
                  <div class="billing_available red_bg_box_wrp"> 
                      <img src="<?php //echo get_asset('assets/frontend/images/off_contact_lenses.jpg'); ?>" class="img-responsive">
          	<br />
          <img src="<?php //echo get_asset('assets/frontend/images/promo_code.jpg'); ?>" class="img-responsive">
            <button class="bg_find_out btn">Shop now</button>
            <p>*Conditions_apply.</p>
          </div>
              </div>
              <div class="col-md-4">
                  <div class="billing_available blue_bg_box_wrp">
                      <img src="<?php// echo get_asset('assets/frontend/images/off_.jpg'); ?>" class="img-responsive">
            <button class="bg_find_out btn brdr_bx">Find Out More</button>
            <p>*Conditions_apply.</p>
          </div>
              </div>-->
		</div>		
	</div>
    </div>

</div>

<script>
    $(function(){
       $('.rating').raty( {path: '<?php echo site_url("assets/lib/jquery-raty/images");?>',
                           readOnly: true,
       score: function() {
    return $(this).attr('data-score');
  }});  
    });
   
    </script>
