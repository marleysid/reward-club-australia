<div class="container">
	<!-- breadcrumbs -->
	 <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <?php if($this->input->get('region')): ?>
            <li><?php echo  $region_name->region_name; ?></li>
            <?php endif; ?>
            <li class="active">VIP Deals</li>
        </ol>


	<div class="row">
		<div class="col-sm-3">
   <!--      	<div class="local-search club">
            	<h4>Search a different region</h4>
            	 <form role="form">
                        <div class="form-group">
                            <label class="lbl_cls">Region</label>

                            <select class="form-control input_txt" name="region">
                                <?php if (isset($regions)): ?>
                                    <?php foreach ($regions as $reg): ?>
                                        <option value="<?php echo $reg->region_id; ?>"><?php echo $reg->region_name; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>

                        </div>
                    </form>
            </div>
            -->
   
            
     <?php $this->load->view('refine_search'); ?>
         <!--<img src="<?php // echo get_asset('assets/frontend/images/bulk_01.jpg'); ?>" class="img-responsive">-->
        </div>
       
		<div class="col-sm-9">
                 
        <div class="col-sm-3 pdn_mrgn_nn"> <h5>Showing 1- 10 of 34 total results</h5></div>
        <div class="col-sm-5 pdn_mrgn_nn">
        <ul class="deal_page_map">
        	<li><strong>View as</strong></li>
            <li><button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/grid?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-th-large"></span>
        </button>Grid</li>
            <li> <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/list?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-list"></span>
        </button>List</li>
            <li>Map <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/map?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm <?php echo ($this->uri->segment(2) == 'map') ? 'active':'' ?>">
          <span class="glyphicon glyphicon-map-marker"></span>
        </button></li>
        </ul>
        </div>
         <div id="map" style="height: 450px; width: 100%; margin: 0px;padding: 0px"></div>	
	  
        <div class="col-sm-4 ">    
		<div class="local-search club bg_nn_pgmrgn"><form role="form">
<!--                 	<div class="form-group">
                <label class="lbl_cls">Sort By</label>
                 <select class="form-control input_txt clr">
                                        <option value="new" >New</option>
                                        <option value="alpha" >Alphabetical</option>
                                        <option value="nearest" >Nearest</option>
                                    </select>
              </div>-->
                 </form></div>	</div>
        </ul>
        <br />
        <br />
<!--       		<img src="<?php echo get_asset('assets/frontend/images/map.jpg'); ?>" class="img-responsive">
		-->
			
          		
	</div>
    </div>

</div>
<div class="feature-service">
  <div class="container">
     <?php $this->load->view('deals/frontend/bottom_slider'); ?>
  </div>
</div>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
<script>

    var locations =  <?php echo $location; ?>;
    //alert(locations);
//      ['Bondi Beach', -33.890542, 151.274856, 4],
//      ['Coogee Beach', -33.923036, 151.259052, 5],
//      ['Cronulla Beach', -34.028249, 151.157507, 3],
//      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
//      ['Maroubra Beach', -33.950198, 151.259302, 1]
    <?php if(isset($user_location)): ?>
    var lat = <?php echo $user_location->lat; ?>;
    var lon = <?php echo $user_location->lon; ?>;
        <?php else: ?>
            var lat = <?php echo $user_location_lat; ?>;
             var lon = <?php echo $user_location_lon; ?>;
   
             <?php endif; ?>
   
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: new google.maps.LatLng(lat, lon),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }

</script>