<div class="banner">
    <div class="slider"><img src="<?php echo get_asset('assets/frontend/images/banner_of_club_member.jpg'); ?>"  class="img-responsive" alt="slide"></div>
    <div class="banner-slider">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
            <ul>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="container"> 
        <!-- breadcrumbs -->
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <li><a href="#">Library</a></li>
            <li class="active">Data</li>
        </ol>
        <div class="row">
            <div class="col-sm-3">
               <?php $this->load->view('refine_search'); ?>
            </div>
            <div class="col-sm-9"> 
                <!--repeated start here -->
                <div class="col-md-12 bg_nn_pgmrgn">
                    <div class="col-sm-3 pdn_mrgn_nn">
                        <h5>Showing <?php echo ($offest) ? ($offest):'1' ?> - <?php echo (($offest + $per_page) < $total) ? ($offest+$per_page):$total ?> of <?php echo $total ?> total results</h5>
                    </div>
                    <div class="col-sm-5 pdn_mrgn_nn">
                        <ul class="deal_page_map">
                            <li><strong>View as</strong></li>
                            <li>
                                <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/grid?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm <?php echo ($this->uri->segment(2) == 'grid') ? 'active':'' ?>"> <span class="glyphicon glyphicon-th-large"></span> </button>
                                Grid</li>
                            <li>
                                <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/list?' . $_SERVER['QUERY_STRING']; ?>'" class=" btn btn-default btn-sm <?php echo ($this->uri->segment(2) != 'grid') ? 'active':'' ?>"> <span class="glyphicon glyphicon-list"></span> </button>
                                List</li>
                            <li>Map
                                <button type="button" onclick="window.location.href = '<?php echo site_url() . 'offers/map?' . $_SERVER['QUERY_STRING']; ?>'" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-map-marker"></span> </button>
                            </li>
                        </ul>
                    </div>
                    <div class="col-sm-4 ">
                        <div class="local-search club bg_nn_pgmrgn">
                            <form role="form">
                                <div class="form-group">
                                    <label class="lbl_cls">Sort By</label>
                                    <select class="form-control input_txt clr">
                                        <option value="new" >New</option>
                                        <option value="alpha" >Alphabetical</option>
                                        <option value="nearest" >Nearest</option>
                                    </select>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 tp_btn_mrgn_wrp bg_nn_pgmrgn">
                    <?php if (isset($get_offers)): ?>
                        <?php foreach ($get_offers as $offers): ?>
                            <div class="col-sm-4">
                                <button class="hvr">
                                    <img src="<?php echo get_asset('assets/frontend/images/best_price_button.png'); ?>" class="price"><span class="prc_btm"><?php echo $offers->tag; ?></span>
                                    <img src="<?php if($offers->type == 'offer'){echo imager($offers->image ? 'assets/uploads/offer_image/' . $offers->image : '', 267, 130, 2); } else{echo imager($offers->image ? 'assets/uploads/deal_image/' . $offers->image : '', 267, 130, 2);} ?>" class="img-responsive opct">
                                    <div class="with_resuld_board">
                                        <div class="whit_box hdgn_dfn_wrp">
                                            <h4><strong><?php echo $offers->title; ?></strong></h4>
                                        </div>

                                    </div>
                                    <div id="msg">

                                    </div>
                                    <div class="light_grw light_grw_lght_wth none_dspl">
                                        <div class="whit_box hdgn_dfn_wrp red_bg"><?php if ($offers->type == 'deal' and isset($userLoggedIn) and $userLoggedIn) : ?><a href="">
                                                    <span class= "glyphicon glyphicon-shopping-cart add-to-cart" data-type="<?php echo $offers->type; ?>" data-id="<?php echo $offers->id; ?>"></span></a>
                                           <?php elseif($offers->type == 'offer' and isset($userLoggedIn) and $userLoggedIn): ?>
                                             <?php else: ?>
                                                <a href="" data-toggle="modal" data-target="#loginModal" data-whatever="@getbootstrap"><span class="glyphicon glyphicon-cart"></span> </a>
                                            <?php endif; ?> 
                                            <?php if (isset($userLoggedIn) and $userLoggedIn): ?>
                                                <a href="" data-toggle="modal"  data-id="<?php echo $offers->id; ?>"><span class="glyphicon glyphicon-heart favourite" data-type="<?php echo $offers->type; ?>" data-id="<?php echo $offers->id; ?>"></span> </a>
                                            <?php else: ?>
                                                <a href="" data-toggle="modal" data-target="#loginModal" data-whatever="@getbootstrap"><span class="glyphicon glyphicon-heart"></span> </a>
                                            <?php endif; ?>
                                            <a href="<?php echo site_url('offers/detail/' . $offers->type . '/' . safe_b64encode($offers->id)); ?>">View Product</a></div>
                                    </div>
                                </button>
                            </div>


                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <!--repeated end here --> 
                <!--pagination start here -->
                <div class="col-md-12">
                    <div class="col-md-3"> </div>
                    <div class="col-md-9 bg_nn_pgmrgn">
                        <div class="col-md-7 pgn_wrp_clr">
                            <ul class="pagination">
                                <?php echo $this->pagination->create_links(); ?>
                                <!--            <li>
                                              <button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-arrow-left"></span> </button>
                                            </li>
                                            <li><a href="#">1</a></li>
                                            <li><a href="#">2</a></li>
                                            <li><a href="#">3</a></li>
                                            <li><a href="#">4</a></li>
                                            <li><a href="#">5</a></li>
                                            <li>
                                              <button type="button" class="btn btn-default btn-sm"> <span class="glyphicon glyphicon-arrow-right"></span> </button>
                                            </li>-->
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <div class="btn-group" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                    <h4 class="margin_right_wrp_bxt_itm">Items per page:</h4>
                                    <button type="button"class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?php echo $this->uri->segment(3) ? $this->uri->segment(3):'8' ?> <span class="caret"></span> </button>
                                    <ul class="dropdown-menu insrt_with" role="menu" >
                                       <li><a href="<?php echo site_url() . 'offers/grid/8?'.$_SERVER['QUERY_STRING']; ?>" >8</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/grid/10?'.$_SERVER['QUERY_STRING']; ?>" >10</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/grid/14?'.$_SERVER['QUERY_STRING']; ?>" >14</a></li>
                                        <li><a href="<?php echo site_url() . 'offers/grid/16?'.$_SERVER['QUERY_STRING']; ?>" >16</a></li>
                                         </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--pagination end here --> 
            </div>
        </div>
    </div>
    <div class="feature-service">
        <div class="container">
            <?php $this->load->view('deals/frontend/bottom_slider'); ?>
        </div>
    </div>
</div>
