<div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5>Manage Feedbacks</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Show</th>
                    <th>Settings</th>

                </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all" >
                <?php if ($data): ?>
                    <?php foreach ($data as $key => $val): ?>
                        <tr class="odd">
                            <td><?php echo ++$key; ?></td>
                            <td><?php echo $val->feedback_first_name; ?></td>
                            <td><?php echo $val->feedback_email; ?></td>
                            <td><?php echo word_limiter($val->feedback_content, 45); ?></td>
                            <td class="center"><input type="checkbox"  <?php echo $val->feedback_status ? "checked" : ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('admin/feedback/toggle_status/'); ?>" data-id="<?php echo $val->feedback_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
                            <td class="center">
                                <a href="<?php echo base_url('admin/feedback/delete/' . $val->feedback_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>

    </div>
</div>
