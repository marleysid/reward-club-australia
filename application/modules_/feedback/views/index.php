
<?php
if ($this->session->flashdata('msg')) {
    echo "<div>" . $this->session->flashdata('msg') . "</div>";
}
?>
<?php echo form_open();
?>
<div>
    <div>
        <label>First Name:</label>
        <div>
            <input name="first_name" value="<?php echo set_value('first_name'); ?>"/>
        </div>
        <div>
            <?php echo form_error('first_name'); ?>
        </div>
    </div>
    <div>
        <label>Last Name:</label>
        <div>
            <input name="last_name" value="<?php echo set_value('last_name'); ?>"/>
        </div>
        <div>
            <?php echo form_error('last_name'); ?>
        </div>
    </div>
    <div>
        <label>Email:</label>
        <div>
            <input name="email" value="<?php echo set_value('email'); ?>"/>
        </div>
        <div>
            <?php echo form_error('email'); ?>
        </div>
    </div>
    <div>
        <label>Feedback:</label>
        <div>
            <textarea name="feedback"><?php echo set_value('feedback'); ?></textarea> 
        </div>
        <div>
            <?php echo form_error('feedback'); ?>
        </div>
    </div>
    <div>
        <input type="submit" name="Submit" value="Submit"/>
    </div>
</div>
<?php echo form_close(); ?>