<?php
class Admin extends Admin_Controller {

	function __construct() {
		parent::__construct();		
	}
	
	public function index() {
		show_error('No Service');
	}
    
    function elfinder_init() {
        $this->load->helper('path');
        $opts = array(
            // 'debug' => true, 
            'roots' => array(
//                array(
//                    'driver' => 'LocalFileSystem',
//                    'path' => FCPATH . 'assets/images', // path to files (REQUIRED)
//                    'URL' => base_url() . 'assets/images', // URL to files (REQUIRED),
//					'alias' => 'Cms Images',
//                // more elFinder options here
//                ),
//                array(
//                    'driver' => 'LocalFileSystem',
//                    'path' => FCPATH . 'assets/item_assets', // path to files (REQUIRED)
//                    'URL' => base_url() . 'assets/item_assets/', // URL to files (REQUIRED),
//					'alias' => 'Item Assets',
//                // more elFinder options here
//                ),
                array(
                    'driver' => 'LocalFileSystem',
                    'path' => FCPATH . 'assets/uploads/cms', // path to files (REQUIRED)
                    'URL' => base_url() . 'assets/uploads/cms', // URL to files (REQUIRED),
					'alias' => 'Images',
                // more elFinder options here
                ),
            )
        );
        $this->load->library('media/elfinder_lib', $opts);
//        $this->load->helper('media/elfinder_lib');
      
    }
    
    function manage_media(){
        $this->load->helper('media/elfinder');
        $this->template->load('layouts/admin', 'elfinder');
    }
	
	function editor_browser()
	{
//        $this->load->helper('media/elfinder');
        $this->load->view('elfinder_editor_browser');		
	}
	
}
?>