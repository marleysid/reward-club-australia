<?php
class Media_model extends MY_Model {

    
    function getMediaFilesList($limit = 5) {
		$dir = $this->config->item('upload_root');
		$items = $this->ListFiles($dir);	
		usort($items,  array($this, 'date_compare'));
		$data = array_slice($items, 0, $limit);
		return $data;
    }
	
	
	function getMenuItems() {
		$dir = $this->config->item('upload_root');
		$items = $this->ListFiles($dir);	
		usort($items,  array($this, 'date_compare'));
		$data = array_slice($items, 0, 5);
		return $data;
    }

	function getSearchResult($q, $limit = 20) {
		/* Media Search Goes here */
	}
	
	
	public function ListFiles($dir) {

		if($dh = opendir($dir)) {
	
			$files = Array();
			$inner_files = Array();
	
			while($file = readdir($dh)) {
				if($file != "." && $file != ".." && $file[0] != '.') {
					if(is_dir($dir . "/" . $file)) {
						$inner_files = $this->ListFiles($dir . "/" . $file);
						if(is_array($inner_files)) $files = array_merge($files, $inner_files); 
					} else {
						$data['name'] = $dir . '/' . $file;
						$data['timestamp'] = filemtime( $dir . "/" . $file);
						array_push($files, $data);
					}
				}
			}
			closedir($dh);
			return $files;
		}
	}
	
	function date_compare($a, $b) {
    	return $b['timestamp'] - $a['timestamp'];
	}
	

}
?>