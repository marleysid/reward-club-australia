<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Partners_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'partners';
        $this->field_prefix = 'partners_';
        $this->log_user = FALSE;
    }
    
    function get_partners(){
     $query =   $this->db->select('*')
                         ->from('partners')
                         ->get();
     
     return $query->result();
        
        
    }

   

}
