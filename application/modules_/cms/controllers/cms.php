<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CMS extends Front_Controller {

    // public $data;

    function __construct() {
        parent::__construct();
        $this->load->model(array('cms_model', 'deals/deal_model', 'clubs/club_model', 'offers/offer_model', 'partners/partners_model', 'advertisement/advertisement_model', 'news/news_model', 'promotional_material/material_model','reviews/review_model'));
        $this->load->library(array('pagination', 'email'));
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index($slug) {
        // echo 'test';
        // debug($this->input->post());die;
        $state = $this->input->get('state');
        $postcode = $this->input->get('postcode');
        $suburb = $this->input->get('suburb');
        $clubname = $this->input->get('club_name');
       // debug($clubname);die;
        $per_page = $this->input->get('per_page');
         if ($this->uri->segment(2) != '') {
            $config['per_page'] = $this->uri->segment(2);
        } else {
            $config['per_page'] = 8;
        }
       // $config['per_page'] = 2;
        $result = $this->club_model->search_clubs($config['per_page'], $per_page, $state, $postcode, $suburb, $clubname);
       // echo $this->db->last_query();die;
        $config['total_rows'] = $this->club_model->found_rows();
//        $config['base_url'] = site_url() . "information-for-members?{$_SERVER['QUERY_STRING']}";
        $config['base_url'] = site_url() . "information-for-members?state={$state}&postcode={$postcode}&suburb={$suburb}&club_name={$clubname}";
        $config['num_links'] = 5;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = "</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='javascript:void'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";
        $config['page_query_string'] = true;
        $config['enable_query_strings'] = true;

        $this->pagination->initialize($config);

        if ($result != false) {
            $this->data['get_clubs'] = $result;
            
        } else {
            $this->data['get_clubs'] = array();
        }
        
        $this->data['club_lists'] = $this->club_model->get_all_clubs();
        $this->data['count_clubs'] = $this->club_model->count_clubs();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        // $this->data['get_clubs'] = $this->club_model->get_all_clubs();
        $this->data['get_side_ads'] = $this->advertisement_model->get_left_side_ads();
        $this->data['get_suburb'] = $this->offer_model->suburb();
        $this->data['get_state'] = $this->offer_model->state();
        $this->data['total'] = $config['total_rows'];
        $this->data['per_page'] = $config['per_page'];
        $this->data['offset'] = $per_page;
       
        $this->data['postcode'] = array_map(function ($v) {
            return $v->postcode;
        }, $this->offer_model->get_postcode());
        $this->data['info'] = $this->cms_model->info_slug($slug);
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
//        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
//        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);
        $this->template->add_title_segment('Information For Members');

        Template::render('frontend/index', $this->data);
    }

    function info_for_clubs() {
        $slug = $this->uri->segment(1);
        //debug($slug);die;
        if ($this->input->post()) {
            // debug($this->input->post());die;
            $this->data['first_name'] = $this->input->post('first_name');
            $this->data['last_name'] = $this->input->post('last_name');
            $this->data['email'] = $this->input->post('email');
            $this->data['contact_no'] = $this->input->post('contact_no');
            $this->data['address'] = $this->input->post('address');
            $this->data['enquiry_msg'] = $this->input->post('enquiry_msg');
            $material = $this->input->post('materials');

            $this->db->insert('promotional_enquiry', array('enquiry_first_name' => $this->data['first_name'],
                'enquiry_last_name' => $this->data['last_name'],
                'enquiry_email' => $this->data['email'],
                'enquiry_contact_no' => $this->data['contact_no'],
                'enquiry_address' => $this->data['address'],
                'enquiry_msg' => $this->data['enquiry_msg']
            ));

            $id = $this->db->insert_id();
            //echo $id;die;

            if (is_array($material)) {
                foreach ($material as $materials) {
                    $this->db->insert('promotional_material_enquiry', array('enquiry_id' => $id, 'material_id' => $materials));
                }
            }
        }
        $this->data['info'] = $this->cms_model->info_slug($slug);
        $this->data['partners'] = $this->partners_model->get_partners();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        $this->data['get_bottom_ads'] = $this->advertisement_model->get_bottom_ads();
        $this->data['news_detail'] = $this->news_model->news();
        $this->data['material_detail'] = $this->material_model->material_detail();
        $this->template->add_title_segment('Information For Clubs');

        Template::render('frontend/info_for_clubs', $this->data);
    }

    function info_for_suppliers() {
        $slug = $this->uri->segment(1);
        //debug($slug);die;
        if($this->input->post()){
             $this->data['first_name'] = $this->input->post('first_name');
            $this->data['last_name'] = $this->input->post('last_name');
            $this->data['email'] = $this->input->post('email');
            $this->data['contact_no'] = $this->input->post('contact_no');
            $this->data['address'] = $this->input->post('address');
            $this->data['enquiry_msg'] = $this->input->post('enquiry_msg');
            $material = $this->input->post('materials');

            $this->db->insert('promotional_enquiry', array('enquiry_first_name' => $this->data['first_name'],
                'enquiry_last_name' => $this->data['last_name'],
                'enquiry_email' => $this->data['email'],
                'enquiry_contact_no' => $this->data['contact_no'],
                'enquiry_address' => $this->data['address'],
                'enquiry_msg' => $this->data['enquiry_msg']
            ));

            $id = $this->db->insert_id();
            //echo $id;die;

            if (is_array($material)) {
                foreach ($material as $materials) {
                    $this->db->insert('promotional_material_enquiry', array('enquiry_id' => $id, 'material_id' => $materials));
                }
            }
        }
        $this->data['info'] = $this->cms_model->info_slug($slug);
        $this->data['partners'] = $this->partners_model->get_partners();
        $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        $this->data['get_bottom_ads'] = $this->advertisement_model->get_bottom_ads();
        $this->data['material_detail'] = $this->material_model->material_detail();
        $this->template->add_title_segment('Information For Suppliers');
       // Template::add_js('assets/frontend/js/jquery-1.11.1.min.js');
        //Template::add_js('assets/frontend/js/jquery.validate.min.js');
        //Template::add_js('assets/frontend/js/additional-methods.min.js');
        Template::render('frontend/info_for_suppliers', $this->data);
    }

    function contact() {
        if ($this->input->post()) {
            $this->data['name'] = $this->input->post('name');
            $this->data['email'] = $this->input->post('email');
            $this->data['address'] = $this->input->post('address');
            $this->data['phone'] = $this->input->post('phone');
            $this->data['enquiry'] = $this->input->post('enquiry');
            $this->data['subject'] = $this->input->post('subject');
            $this->data['msg'] = $this->input->post('msg');
            //debug( $this->data);die;
            $this->db->insert('contacts', array('contacts_name' => $this->data['name'],
                'contacts_email' => $this->data['email'],
                'contacts_phone_number' => $this->data['phone'],
                'contacts_address' => $this->data['address'],
                'contacts_enquiry' => $this->data['enquiry'],
                'contacts_subject' => $this->data['subject'],
                'contacts_msg' => $this->data['msg']));


//            $this->email->from('ruchitimilsina27@gmail.com', 'Ruchi Timilsina');
//            $this->email->to($this->data['email']);
//            $this->email->subject('Thank You');
//            $this->email->message('Thank you for your interest in Rewards Club');
//
//            $this->email->send();
        }
        $slug = $this->uri->segment(1);
        //debug($slug);die;
        $this->data['info'] = $this->cms_model->info_slug($slug);
        $this->data['partners'] = $this->partners_model->get_partners();
        $this->data['material_detail'] = $this->material_model->material_detail();
        $this->template->add_title_segment('Contact');
        Template::render('frontend/contacts', $this->data);
    }

    function faq() {
        $this->data['faqs'] = $this->cms_model->get_faqs();
        $this->template->add_title_segment('FAQ');
        Template::render('frontend/faq', $this->data);
    }
    
    function privacy_policy(){
         $slug = $this->uri->segment(1);
        $this->data['policy'] = $this->cms_model->info_slug($slug);
        $this->template->add_title_segment('Privacy Policy');
        Template::render('frontend/privacy_policy',  $this->data);
    }
    
    function terms_conditions(){
         $slug = $this->uri->segment(1);
        $this->data['terms_conditions'] = $this->cms_model->info_slug($slug);
        $this->template->add_title_segment('Terms and Conditions');
        Template::render('frontend/terms_conditions',  $this->data);
    }

}
