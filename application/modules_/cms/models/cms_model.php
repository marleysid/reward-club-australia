<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cms_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'cms';
        $this->field_prefix = 'cms_';
        $this->log_user = FALSE;
    }

    function cms_detail() {
        $this->db->select('*');
        $this->db->from('cms');
        $this->db->where("club_id = '0'");
        $this->db->where("is_deleted = '0'");
        $query = $this->db->get();
        return $query->result();
    }
	function cms_club_detail($id){
	$this->db->select('*');
        $this->db->from('cms');
        $this->db->where("club_id = '$id'");
        $this->db->where("is_deleted = '0'");
        $query = $this->db->get();
        return $query->result();
	
	}

        function info_slug($slug){
              $this->db->select('*');
        $this->db->from('cms');
        $this->db->where("cms_slug = '$slug'");
        $query = $this->db->get();
        if($query->num_rows() > 0){
        return $query->row();
        }
        else{
            return false;
        }
        }
        
        function get_faqs(){
         $query =    $this->db->select('*')
                              ->from('faq')
                              ->get();
         
         return $query->result();
            
        }
        
       
    

}
