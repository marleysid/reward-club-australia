
<!-- banner -->

<div class="banner"> <div class="slider"><img src="<?php echo imager($info->cms_header_image ? 'assets/uploads/cms/' . $info->cms_header_image : '', 1349, 295); ?>"  class="img-responsive" alt="slide"></div>
    <div class="banner-slider">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
            <ul>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
            </ul>
        </div>
    </div>
</div>
<!-- banner--> 

<!-- search-featured -->
<div class="search-featured" id="search-featured">
    <div class="container">
        <div class="row">
            <div class="col-md-12 menu-detail">
                <ul>
                    <li><a href="<?php echo site_url(); ?>" class="active">Home</a></li>
                    <li><a href="#">Information For Clubs</a></li>
                </ul>
            </div>

            <?php echo $info->cms_content; ?>


            <div class="col-md-12 enquiry_box_wrp">
                <a href="#"><img src="<?php echo imager($get_bottom_ads->ad_image_path ? 'assets/uploads/ad/' . $get_bottom_ads->ad_image_path : '', 598, 49); ?>" class="img-responsive"></a>
            </div>

        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="container">
        <h3><span>Enquiry to get promotional material from Rewards Club.</span></h3>
       <?php echo $info->cms_second_block; ?>



        <div class="advanced-search">
            <form role="form" method="post">

                <div class="col-md-4">

                    <input type="text" class="form-control nt_wth_incld" placeholder="first name *" name="first_name" required />


                    <input type="text" class="form-control nt_wth_incld"  placeholder="E-mail address*" name="email" required />

                </div>

                <div class="col-md-4">
                    <input type="text"  class="form-control nt_wth_incld" placeholder="sur name *" name="last_name" required/>


                    <input type="text" class="form-control nt_wth_incld" placeholder="contact number" name="contact_no"  />
                </div>


                <div class="col-md-4">
                    <input type="text" class="form-control nt_wth_incld" placeholder="address *" name="address" required/>


                   

                </div>  
                <div class="col-md-12"><h4 class="left_align">Choose a sample promotional &amp; Support Material you want to enquire about.</h4> <h4 class="right_align"><u>See order supplies above</u></h4></div>

                <div class="col-md-12 mrgn_tp_btn_wrp">
                    <?php if (isset($material_detail)): ?>
                        <?php foreach ($material_detail as $materials): ?>
                            <div class="col-md-3">
                                <div class="window_register_decal_box">
                                    <img src="<?php echo imager(($materials->material_image) ? 'assets/uploads/promotional_materials/'.$materials->material_image:'',257,218) ;?>"/>
                                    <label> <input type="checkbox" class="active" name="materials[]" value="<?php echo $materials->material_id;?>" /><?php echo $materials->material_name; ?></label>
                                    
                                </div>

                            </div>

                            <!--                    <div class="col-md-3 counter_stands">
                                                    <div class="counter_stands_box"><label><input type="checkbox" class="active" />Window / Register decal</label></div>
                                                </div>
                                                <div class="col-md-3 deal_card">
                                                    <div class="deal_card_box"><label><input type="checkbox" class="active" />Window / Register decal</label></div>
                                                </div>
                                                <div class="col-md-3 shop_front_welcome_banner">
                                                    <div class="shop_front_welcome_banner_box"><label><input type="checkbox" class="active" />Window / Register decal</label></div>
                                                </div>-->
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
                <div class="col-md-12">

                    <div class="form-group">
                        <textarea class="form-control nt_wth_incld" rows="5" id="comment" placeholder="Enquiry" name="enquiry_msg"></textarea>
                    </div>
                </div>


                <div class="text-right"> <input type="submit" name="submit" class="btn btn-danger" value="Search" /></div>
            </form>
        </div>    


    </div>
</div>
<div class="feature-service">
    <div class="container">
        <div id="owl-service" class="owl-carousel margn_wrp_fx_bx">
            <?php if (isset($partners)): ?>
                <?php foreach ($partners as $row): ?>
                    <div class="item logo_img">
                        <img src="<?php echo imager($row->partners_icon ? 'assets/uploads/partners/' . $row->partners_icon : '', 231, 112); ?>" class="img-responsive">
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="media-block">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 club-news">

                <div class="feature-service">
                    <div class="container"><h3><span>Rewards Club News</span></h3>
                        <div id="owl-service_01" class="owl-carousel margn_wrp_fx_bx brd_nn_01">
                            <?php if (isset($news_detail)): ?>
                                <?php foreach ($news_detail as $news): ?>

                                    <div class="item logo_img"><img src="<?php echo imager($news->news_image ? 'assets/uploads/news/' . $news->news_image : '', 218, 112); ?>" class="img-responsive">
                                        <div class="info"> <span><?php echo date("d M", strtotime($news->news_create_date)); ?></span>
                                            <h4><?php echo ucwords($news->news_title); ?></h4>
                                            <p><?php echo word_limiter($news->news_excerpt, 45) ?> </p>
                                             <?php echo anchor("news/".safe_b64encode($news->news_id), 'Read More', 'class="btn btn-danger"'); ?> </div>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
      <!--              <div class="item logo_img"><img src="<?php // echo get_asset('assets/frontend/images/img1_01.jpg');   ?>" class="img-responsive">
                      <div class="info"> <span>5 november</span>
                        <h4>Get your Club into Rewards Club</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                        <a href="#" class="btn btn-danger">Read More</a> </div>
                    </div>
                    <div class="item logo_img"><img src="<?php // echo get_asset('assets/frontend/images/img1_01.jpg');   ?>" class="img-responsive">
                      <div class="info"> <span>5 november</span>
                        <h4>Get your Club into Rewards Club</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                        <a href="#" class="btn btn-danger">Read More</a> </div>
                    </div>
                    <div class="item logo_img"><img src="<?php //echo get_asset('assets/frontend/images/img1_01.jpg');   ?>" class="img-responsive">
                      <div class="info"> <span>5 november</span>
                        <h4>Get your Club into Rewards Club</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                        <a href="#" class="btn btn-danger">Read More</a> </div>
                    </div>
                    <div class="item logo_img"><img src="<?php // echo get_asset('assets/frontend/images/img1_01.jpg');   ?>" class="img-responsive">
                      <div class="info"> <span>5 november</span>
                        <h4>Get your Club into Rewards Club</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                        <a href="#" class="btn btn-danger">Read More</a> </div>
                    </div>
                    <div class="item logo_img"><img src="<?php //echo get_asset('assets/frontend/images/img1_01.jpg');   ?>" class="img-responsive">
                      <div class="info"> <span>5 november</span>
                        <h4>Get your Club into Rewards Club</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                        <a href="#" class="btn btn-danger">Read More</a> </div>
                    </div>
                    <div class="item logo_img"><img src="<?php // echo get_asset('assets/frontend/images/img1_01.jpg');   ?>" class="img-responsive">
                      <div class="info"> <span>5 november</span>
                        <h4>Get your Club into Rewards Club</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                        <a href="#" class="btn btn-danger">Read More</a> </div>
                    </div>
                    <div class="item logo_img"><img src="<?php //echo get_asset('assets/frontend/images/img1_01.jpg');   ?>" class="img-responsive">
                      <div class="info"> <span>5 november</span>
                        <h4>Get your Club into Rewards Club</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...  </p>
                        <a href="#" class="btn btn-danger">Read More</a> </div>
                    </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- media --> 
