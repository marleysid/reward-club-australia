<!-- banner -->

<div class="banner">
  <div class="slider"><img src="<?php echo imager($info->cms_header_image ? 'assets/uploads/cms/' . $info->cms_header_image : '', 1349, 295);?>"  class="img-responsive" alt="slide"></div>
  <div class="banner-slider">
    <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
      <ul>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
        <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
      </ul>
    </div>
  </div>
</div>
<!-- banner--> 

<!-- search-featured -->
<div class="search-featured" id="search-featured">
  <div class="container">
    <div class="col-md-6 member-detail club_for_member_wrp">
      <h3><span>Get in Touch</span></h3>
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto</p>
      <div class="advanced-search">
          <form role="form" method="post">
        <div class="col-md-12 pdn_mrgn_nn">
         <div class="col-md-6 pdn_mrgn_nn_right">
          	<div class="form-group">
                    <input type="Phone" class="form-control nt_wth_incld" placeholder="Name" name="name" />
                    <input type="Phone" class="form-control nt_wth_incld"  placeholder="Contact No."  name="phone"/>
          </div>
         </div>
          <div class="col-md-6 pdn_mrgn_nn">
          	<div class="form-group">
                    <input type="Phone"  class="form-control nt_wth_incld" placeholder="Address" name="address" />
                    <input type="Phone" class="form-control nt_wth_incld" placeholder="Email Address" name="email" />
          </div>
          </div>
        </div>
          <div class="form-group">
            <h4 class="left_align"><em>Choose a sample promotional &amp; Support Material you want to enquire about.</em></h4>
          </div>
          <div class="form-group">
                          <label class="wth_prcnt">Type of Enquiry</label>
                          <select class="form-control wth_prcnt_01" id="select-sub-category" name="enquiry">
                                <option Type of Enquiry></option> 
                                <option value="first">First</option>
                                <option value="second">Second</option>
                                <option value="third">Third</option>
                                <option value="fourth">Fourth</option>
                            </select>
                        </div>
              <div class="form-group">
                  <input type="Phone"  class="form-control nt_wth_incld" placeholder="subject" name="subject" />
            </div>
                <div class="form-group">
                    <textarea type="Phone"  class="form-control nt_wth_incld" placeholder="Message" name="msg" ></textarea>
            </div>
            
          <div class="text-right">
<!--            <button class="btn btn-danger">search</button>-->
                 <input type="submit" name="submit" class="btn btn-danger" value="Search" />           
                   
          </div>
              
        </form>
      </div>
    </div>
    <div class="col-md-6 vdo-md-cls"> 
      <!-- featureCarousel -->
      <ul class="adrs_wrp_vip">
      <li><img src="<?php echo get_asset('assets/frontend/images/your_vip_card.jpg'); ?>"></li>
        <li> <a href="#"> <span class="glyphicon glyphicon-map-marker"></span> </a> Suite 103, Lvl 1, 270 Pacific Highway, Crows Nest NSW</li>
        <li><a href="#"> <span class="glyphicon glyphicon-credit-card"></span> </a> PO Box 255, Neutral Bay NSW 2089 Australia</li>
         <li></li>
        <li> <a href="#"> <span class="glyphicon glyphicon-earphone"></span> </a> 1300 305 690</li>
        <li> <a href="#"> <span class="glyphicon glyphicon-print"></span> </a>1300 884 208</li>
        <li><a href="#"> <span class="glyphicon glyphicon-envelope"></span> </a> info@rewardsclub.com.au</li>
         <p>Rewards Club VIP benefits are included on selected club memberships. 
For information about your local club <a><u>click here</u></a></p>
      </ul>
     
      <h4>Sample Promotional &amp; Support Material <span>(see Order Supplies above)</span></h4>
      
           <ul class="adrs_wrp_vip_01">
               <?php if(isset($material_detail)): ?>
               <?php foreach ($material_detail as $materials): ?>
      <li>
        <img src="<?php echo imager($materials->material_image ? 'assets/uploads/promotional_materials/'.$materials->material_image:'',152,127); ?>" class="img-responsive" >
      </li>
    <?php endforeach; ?>
      <?php endif; ?>
           </ul>

      <!-- featureCarousel--> 
    </div>
    
    
    
  </div>
</div>
</div>

<div class="feature-service">
  <div class="container margn_wrp_fx_bx">
    <h3 class="mrgn_top_02"><span>Rewards Clubs Partners</span></h3>
    <div id="owl-service" class="owl-carousel brd_nn">
       <?php if(isset($partners)): ?>
        <?php foreach ($partners as $row): ?>
      <div class="item logo_img">
          <img src="<?php echo imager($row->partners_icon ? 'assets/uploads/partners/' . $row->partners_icon : '', 231,112); ?>" class="img-responsive">
      </div>
        <?php endforeach; ?>
        <?php endif; ?></div>
  </div>
</div>

<!-- media --> 