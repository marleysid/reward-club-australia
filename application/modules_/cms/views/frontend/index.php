<!--<div class="container">
    <div class="row">
                <div class="col-sm-3"></div>
                
                <div class="col-sm-9">
                    <div class="info-block pull-left"><img src="<?php //echo base_url().'assets/uploads/cms/'.$info->cms_header_image;    ?>"></div>
                        <h1 class="page-title"><?php //echo $info->cms_title;    ?></h1>
                        <p><?php //echo $info->cms_content;    ?></p>
                        
                </div>	
              
        </div>
</div>-->

<!-- banner -->
<div class="banner"> 
<div class="slider"><img src="<?php echo imager($info->cms_header_image ? 'assets/uploads/cms/' . $info->cms_header_image : '', 1349, 295); ?>"  class="img-responsive" alt="slide"></div>
    <div class="banner-slider">
        <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 social_menu_sld">
            <ul>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/fb_icn-btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/twitter_icn_btn.png'); ?>"></a></li>
                <li class="left-rounded box-whte"><a href="#"><img src="<?php echo get_asset('assets/frontend/images/yutbe_icn_btn.png'); ?>"></a></li>
            </ul>
        </div>
    </div>
</div>
<!-- banner--> 

<!-- search-featured -->
<div class="search-featured" id="search-featured">
    <div class="container">
        <div class="row">
            <div class="col-md-12 menu-detail">
                <ul>
                    <li><a href="<?php echo site_url(); ?>" class="active">Home</a></li>
                    <li><a href="<?php echo site_url('information-for-members'); ?>">Information For Members</a></li>
                </ul>
            </div>
<!--            <div class="col-md-7 member-detail">
               <img src="assets/frontend/images/card_s.jpg"  class="img-responsive" alt="card detail">
              <p>Membership at leading local clubs now includes Rewards Club VIP discounts at no extra cost. Join or update your membership card to start saving with Rewards Club VIP today. </p>
              <p>You can also call in to collect an updated VIP Directory from the member stand located in the foyer of each club.</p>
               
            </div>-->
             <?php echo $info->cms_content; ?>
            <div class="col-md-5 vdo-md-cls"> 
                <!-- featureCarousel -->
                <div class="embed-responsive embed-responsive-16by9 wowload fadeInLeft">
                    <iframe  class="embed-responsive-item" src="//player.vimeo.com/video/10623511?title=0" width="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                </div>

                <!-- featureCarousel--> 
            </div>
        </div>
    </div>
</div>
<div class="search-featured" id="search-featured">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="clb_head"><b>Join a Local Club</b> ( FREE with membership at these leading clubs)</h4>
                <div class="col-md-4 search-club">
                    <div class="local-search club">
                        <h4>Search a Club</h4>
                        <form role="form" method="GET">
                            <div class="form-group">
                                <label class="lbl_cls">Post Code</label>
                                <input type="text" class="form-control input_txt postcode" name="postcode"  placeholder="Post Code" id="club_postcode">
                            </div>
                            <div class="form-group">
                                <label class="lbl_cls">State</label>
                                <select class="form-control input_txt" name="state">
                                    <option value=""></option>
                                    <?php if(isset($get_state)): ?>
                                    <?php foreach ($get_state as $state): ?>
                                    <option value="<?php echo $state->id; ?>"><?php echo $state->state; ?></option>
                                  <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="lbl_cls">Suburb</label>
                                <select class="form-control input_txt" name="suburb">
                                       <option value=""></option>
                                     <?php if(isset($get_suburb)): ?>
                                    <?php foreach ($get_suburb as $suburb): ?>
                                    <option value="<?php echo $suburb->id; ?>"><?php echo $suburb->suburb; ?></option>
                                  <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="lbl_cls">Club name</label>
                                <input type="Phone" class="form-control input_txt"  placeholder="Camden RSL Club" name="club_name" id="keyword">
                            </div>
                            <div class="text-right">
                                <!--                                <button class="btn btn-danger">search</button>-->
                    <input type="submit"  class="btn btn-danger" value="Search" />           
                     <?php //echo anchor('information-for-members', 'Search', 'class="btn btn-danger"','name="submit"'); ?>
                            </div>
                        </form>
                    </div>
                    <div class="billing_available"> <img src="<?php echo get_asset('assets/frontend/images/bulk_01.jpg'); ?>" class="img-responsive"> </div>
                </div>
                <div class="col-md-8 result-box_board"><span><b>Showing <?php echo ($offset) ? $offset:'1' ?> - <?php echo (($offset + $per_page) < $total) ? ($offset+$per_page):$total; ?> of <?php echo $total ?> total results</b></span>
                    <div class="col-md-12 bg_grw_01 with_resuld_board">
                        <!--repeat div start here-->
                        <?php if (isset($get_clubs)): ?>
                            <?php foreach ($get_clubs as $clubs): ?>
                                <div class="col-md-8 whit_box">
                                    <h4><?php echo $clubs->club_name; ?></h4>
                                    <p> <span class="glyphicon glyphicon-map-marker"></span><?php echo ucwords($clubs->club_address); ?> <span class="glyphicon glyphicon-earphone"></span><?php echo $clubs->club_phone; ?> </p>
                                </div>
                                <div class="col-md-4 whit_box fix_with_right">
<!--                                    <h4>Rate&nbsp;&nbsp; <a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star"></span></a><a href="#" ><span class="glyphicon glyphicon-star un_select_clr"></span></a></h4>-->
                                    <a href="<?php echo prep_url($clubs->club_website); ?>" target="_blank">   <button class="view_website btn fx_font_siz">
                                        View Website <span class="glyphicon glyphicon-arrow-right"></span> 
                                      </button></a>
                                </div>
                            <?php endforeach; ?>
                       
                         <?php endif; ?>
                        

                        <!--repeat div end here--> <?php //echo $this->pagination->create_links(); ?><?php //echo $info->cms_second_block; ?></p>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-2">
                            
                        </div>
                        <div class="col-md-10 bg_nn_pgmrgn">
                        <div class="col-md-7 pgn_wrp_clr">
                        <ul class="pagination">
                            <?php echo $this->pagination->create_links(); ?>
<!--                                <li><button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-arrow-left"></span>
        </button></li>
                                <li><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><button type="button" class="btn btn-default btn-sm">
          <span class="glyphicon glyphicon-arrow-right"></span>
        </button></li>-->
                            </ul>
                         </div>
                            <div class="col-md-4 bg_nn_pgmrgn pull-righ">
                            <div class="btn-group" role="group" aria-label="...">
                                <div class="btn-group" role="group">
                                	<h4 class="margin_right_wrp_bxt_itm">Items per page:</h4>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><?php echo $this->uri->segment(2) ? $this->uri->segment(2):'8' ?> <span class="caret"></span> </button>
                                    <ul class="dropdown-menu insrt_with" role="menu">
                                        <li ><a href="<?php echo site_url('information-for-members/8').'?'.$_SERVER['QUERY_STRING']; ?>">8</a></li>
                                        <li><a href="<?php echo site_url('information-for-members/10').'?'.$_SERVER['QUERY_STRING']; ?>">10</a></li>
                                        <li><a href="<?php echo site_url('information-for-members/14').'?'.$_SERVER['QUERY_STRING']; ?>">14</a></li>
                                        <li><a href="<?php echo site_url('information-for-members/16').'?'.$_SERVER['QUERY_STRING']; ?>">16</a></li>
                                    </ul>
                                </div>
                                         
                                
                            </div>
                            </div>

                        </div>
                        <?php echo $info->cms_second_block; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="feature-service">
        <div class="container">
            <?php $this->load->view('deals/frontend/bottom_slider'); ?>        

        </div>
    </div>
 </div>
    <!-- media --> 
    <script type="text/javascript">
        $(document).ready(function() {
      var postcode = <?php echo json_encode($postcode); ?>;
//       $("#club_postcode").autocomplete({source:  function(request, response) {
//        var code = $.ui.autocomplete.filter(postcode, request.term);
//        
//        response(code.slice(0, 10));
//    },
//    change: function (event, ui) {
//            if (!ui.item) {
//                this.value = '';
//            }
//        }
//        });
$("#club_postcode").autocomplete({source:postcode});

        });
    </script>

