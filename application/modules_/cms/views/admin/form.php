<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : ''; ?> CMS</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>"  enctype="multipart/form-data">
                <div class="form-group <?php echo form_error('cms_title') ? 'has-error' : '' ?>">
                    <label for="cms_title" class="control-label col-lg-3">Title *</label>
                    <div class="col-lg-7">
                        <input type="text" id="offer_title" placeholder="Title" name="cms_title" class="form-control" value="<?php echo set_value("cms_title", $edit ? $cms_detail->cms_title : ''); ?>" >
                        <?php echo form_error('cms_title'); ?>
                    </div>
                </div>
                  <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="cms_header_image" class="control-label col-lg-3">Image </label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span> 
                                <span class="fileinput-exists">Change</span> 
                                <input type="file" name="cms_header_image">
                            </span> 
                            <span class="fileinput-filename"></span> 
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists(config_item('cms_image_path') . $cms_detail->cms_header_image)): ?>
                            <div>
                                <img src="<?php echo imager(config_item('cms_image_path') . $cms_detail->cms_header_image, 128, 128); ?>" alt="cms image" class="img-thumbnail">
                               </div>
                        <?php endif; ?>
                    </div>
                </div>
              
                <div class="form-group <?php echo form_error('cms_content') ? 'has-error' : '' ?>">
                    <label for="cms_content" class="control-label col-lg-3"> Content *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="offer_desc" placeholder=" Content" name="cms_content" class="form-control"><?php echo  set_value("cms_content", $edit ? $cms_detail->cms_content : ''); ?></textarea>
                     <?php echo display_ckeditor('cms_content'); ?>
<?php echo form_error('cms_content'); ?>
                    </div>
                </div>
                 <div class="form-group <?php echo form_error('cms_second_block') ? 'has-error' : '' ?>">
                    <label for="cms_second_block" class="control-label col-lg-3"> Second Block *</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="offer_desc" placeholder=" Second Block" name="cms_second_block" class="form-control"><?php echo  set_value("cms_second_block", $edit ? $cms_detail->cms_second_block : ''); ?></textarea>
                     <?php echo display_ckeditor('cms_second_block'); ?>
<?php echo form_error('cms_content'); ?>
                    </div>
                </div>
                  <div class="form-group <?php echo form_error('cms_slug') ? 'has-error' : '' ?>">
                    <label for="cms_slug" class="control-label col-lg-3"> Slug *</label>
                    <div class="col-lg-7">
                        <input type="text" id="offer_desc" placeholder=" Slug" name="cms_slug" class="form-control" value="<?php echo  set_value("cms_slug", $edit ? $cms_detail->cms_slug : ''); ?>" <?php if($edit){?>disabled<?php } ?>>
                  
<?php echo form_error('cms_slug'); ?>
                    </div>
                </div>

               <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
<?php echo anchor('admin/cms', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>

            </form>		
        </div>

    </div>
</div>



