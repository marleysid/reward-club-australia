<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('order_model');
		$this->load->helper('image');
        $this->data['logo_error'] = '';
		
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['orders'] = $this->order_model->detail();

        Template::render('admin/index', $this->data);
    }
	function detail($id) {
		$this->data['order_detail'] = $this->order_model->order_detail($id);
		$this->data['ordered_items'] = $this->order_model->get_ordered_items($id);
		//$this->load->view('admin/order_detail',$this->data);
			if($this->input->post('order_status_submit')) {
			$order_status = ($this->input->post('order_status') == 'Complete') ? 'Complete' : 'Process' ;
			$this->db->update('orders',array('order_status'=>$order_status),array('order_id' => $id));
			
			$this->session->set_flashdata('success_message','Order Status updated.');			
			redirect(current_url());
			
			
		}
		Template::render('admin/order_detail', $this->data);
	}

   

}
