<div class="col-md-12">
  <div class="container"> 
    <!-- breadcrumbs -->
    <ol class="breadcrumb">
      <li>My Order summary</li>
    </ol>
    <form method="post" action="<?php echo site_url('orders'); ?>">
   <div class="col-md-12">
    <div class="col-md-2">
    </div>
    <div class="col-md-9">
    <div class="col-md-8 border_wrp_bx_">
        <?php if(isset($items)): ?>
        <?php foreach ($items as $item): ?>
    <div class="brd_btm_dfn">
      <div class="col-md-4 fnt_n_clr_fx"> <p><?php echo $item->deal_title; ?></p>
        <span>QTY: <?php echo $item->quan; ?></span></div>
      <div class="col-md-2 fnt_n_clr_fx_rgt"><p>$<?php echo $item->deal_price; ?></p></div>
     </div>
        <?php endforeach; ?>
        <?php endif; ?>
     
      <div class="brd_btm_dfn">
      <div class="col-md-4 fnt_n_clr_fx"> <p>Sub total</p></div>
      <div class="col-md-2 fnt_n_clr_fx_rgt"><p>$<?php echo $total->total; ?></p></div>
      
      <div class="col-md-4 fnt_n_clr_fx"> <p>Standard Shipping $50</p>
       <span> Est. delivery between Wed 26 Nov. - Tues 2 Dec </span>
       </div>
      <div class="col-md-2 fnt_n_clr_fx_rgt">
          <p><input type="checkbox" class="check_bx_pp" name="shipping" value="1" id="shipping"  /></p>
      </div>
      <div class="col-md-4 fnt_n_clr_fx"> <p>In-store pick up - FREE</p> </div>
      <div class="col-md-2 fnt_n_clr_fx_rgt">
          <p><input type="checkbox" class="check_bx_pp" name="shipping" value="0" id="in-store"/></p>
      </div>
      </div>
         <?php echo form_error('shipping'); ?>
     
        
      <div class="col-md-4 fnt_n_clr_fx"><h3> TOTAL:</h3> </div>
      <div class="col-md-2 fnt_n_clr_fx_rgt total_amount"><h3></h3></div>
    </div>
  
  <div class="col-md-8 brd_btm_tp_dfn">
    <h4>DELIVERY OPTIONS</h4>
  </div>

  <div class="col-md-8 border_wrp_bx_">
<!--    <div class="col-md-4 fnt_n_clr_fx"><p>In-store pick up - FREE</p></div>
    <div class="col-md-2 fnt_n_clr_fx_rgt">
      <p><input class="check_bx_pp" type="checkbox" /></p>
    </div>-->
    <div class="col-md-4 fnt_n_clr_fx"><p>Delivery</p> </div>
    <div class="col-md-2 fnt_n_clr_fx_rgt"><h3 class="hd_wrp">Edit/Add
            <input class="check_bx_pp" type="checkbox" id="delivery"/></h3>
    </div>
    <div class="col-md-6 dlivery_btn_box_wrp "><textarea class="address" name="delivery_address" readonly><?php echo ucwords($address->member_address); ?>,&nbsp;<?php echo $address->member_postcode; ?>,&nbsp;<?php echo  ucwords($address->member_suburb); ?> </textarea></div>
  </div>
  
  <div class="col-md-8 brd_btm_tp_dfn">
    <h4>Checkout</h4>
  </div>
  
  <div class="col-md-8 border_wrp_bx_">
<!--    <div class="col-md-6 fnt_n_clr_fx"><p>Select your method of payment:</p></div>-->
<!--    <div class="col-md-6 fnt_n_clr_fx"><h5>PAY BY CREDIT CARD
      <input type="checkbox" class="check_bx_pp" /></h5>
    </div>-->
<!--    <div class="col-md-6 crd_wrp">
      <ul>
        <li><a href="#"><img src="<?php echo get_asset('assets/frontend/images/master_card.png'); ?>" class="img-responsive"></a></li>
        <li><a href="#"><img src="<?php echo get_asset('assets/frontend/images/master_card.png'); ?>" class="img-responsive"></a></li>
        <li><a href="#"><img src="<?php echo get_asset('assets/frontend/images/master_card.png'); ?>" class="img-responsive"></a></li>
        <li><a href="#"><img src="<?php echo get_asset('assets/frontend/images/master_card.png'); ?>" class="img-responsive"></a></li>
        <li><a href="#"><img src="<?php echo get_asset('assets/frontend/images/master_card.png'); ?>" class="img-responsive"></a></li>
      </ul>
    </div>-->
    
  
  <div class="col-md-8 witdh_whole"> 
  	<div class="col-md-6 brd_tp_dfn"> <input class="btn check_out" type="submit" value="Checkout" name="submit" /></div>
  </div>
  </div>
  </div>
    </form>


</div>
<script type="text/javascript">
    $(function(){
       $('#shipping').on('click',function(){
           $('#in-store').attr('checked', false);
           var shipping = $(this).is(':checked')? 1:0;
           if(shipping == 1){
            var total = <?php echo $total->total; ?> + <?php echo $shipping_charge->value; ?>;
          $('.total_amount h3').html(total);
           }
           else{
            var total = <?php echo $total->total; ?>;
            //alert(total);
            $('.total_amount h3').html(total);
           }
           
           
       });
       $('#in-store').on('click',function(){
        $('#shipping').attr('checked', false);
        var total = <?php echo $total->total; ?>;
        $('.total_amount h3').html(total);
          
       });
       $('#delivery').on('click',function(){
            $('#shipping').attr('checked', true);
            var total = <?php echo $total->total; ?> + 60;
        $('.total_amount h3').html(total);
          var delivery = $(this).is(':checked')? 1:0;
          if(delivery == 1){
           $('.address').removeAttr('readonly');   
    
    }
    else{
        $('.address').attr('disabled',true);
    }
         
       
       });
    });
            
</script>