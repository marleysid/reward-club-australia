<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage Orders</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
		  <thead>
			<tr>
				<th>#</th>
				<th>Name</th>
				<th>Email</th>
				<th>Amount</th>
				<th>In-store PickUp</th>
				<th>Delivery Address</th>
				<th style="width:90px;" >Settings</th>
				
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all" >
			<?php if($orders): ?>
				<?php foreach($orders as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->member_first_name; ?></td>
				<td><?php echo $val->member_email; ?></td>
				<td><?php echo $val->amount; ?></td>
				<td><?php echo $val->in_store_pickup == 0 ? 'No':'Yes'; ?></td>
                                <td><?php echo $val->delivery_address; ?></td>
								 <td class="center">
					<a href="<?php echo base_url('admin/orders/detail/'.$val->order_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Detail"><i class="fa fa-edit"></i> </a>
					
                                </td>
				
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>
