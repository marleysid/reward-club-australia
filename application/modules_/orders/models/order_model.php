<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_model extends MY_Model {


    public function __construct() {
        parent::__construct();
        $this->table = 'orders';
        $this->log_user = FALSE;
    }

    function detail(){
         $this->db->select('orders.*,members.member_first_name,members.member_email');
        $this->db->from('orders');
        $this->db->join('members','orders.member_id = members.member_id','inner');
        $query = $this->db->get();
        return $query->result();
    }
    function order_detail($id) {
        $this->db->select('orders.*,members.member_first_name,members.member_email,members.member_contact_no');
        $this->db->from('orders');
        $this->db->join('members','orders.member_id = members.member_id','inner');
        $this->db->where("orders.order_id = '$id'");
        $query = $this->db->get();
        return $query->row();
    }
	function get_ordered_items($id) {
		$this->db->select('ordered_items.*,deals.deal_title');
        $this->db->from('ordered_items');
         $this->db->join('deals','ordered_items.deal_id = deals.deal_id','inner');
        $this->db->where("ordered_items.order_id = '$id'");
        $query = $this->db->get();
        return $query->result();
	
	}
        function cart_items($id){
            $query = $this->db->select("deals.deal_title,deals.deal_price,cart_session.quantity as quan")
                             ->from("deals")
                             ->join("cart_session","deals.deal_id = cart_session.deal_id","inner")
                             ->where("cart_session.member_id = '$id'")
                             ->get();
            return $query->result();
        }
        
        function total_amount($id){
            $query = $this->db->select("sum(cart_session.quantity * deals.deal_price) as total")
                             ->from("deals")
                             ->join("cart_session","deals.deal_id = cart_session.deal_id","inner")
                             ->where("cart_session.member_id = '$id'")
                             ->get();
            return $query->row();
        }
        
        function currrent_member_address($id){
            $query = $this->db->select("member_address,member_postcode,member_suburb")
                              ->from("members")
                              ->where("member_id = '$id'")
                              ->get();
            return $query->row();
        }
        
        function shipping_charge(){
            $query = $this->db->select("value")
                              ->from("settings")
                              ->where("key = 'delivery_charge'")
                              ->get();
            return $query->row();
        }
        
     
        function temp_order_data($pay_key){
            $query = $this->db->select("*")
                              ->from("temp_order")
                              ->where("payment_id = '$pay_key'")
                              ->get();
            return $query->row();
        }
        
        function get_ordered_deals($id){
            $query = $this->db->select("deals.deal_id,cart_session.quantity,deals.deal_price")
                             ->from("deals")
                             ->join("cart_session","deals.deal_id = cart_session.deal_id","inner")
                             ->where("cart_session.member_id = '$id'")
                             ->get();
            return $query->row();
        }

   
}
