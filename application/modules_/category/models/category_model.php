<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'category';
        $this->field_prefix = 'category_';
        $this->log_user = FALSE;
    }


    
    function get_subcategory($id){
        $this->db->select("category_id,category_name")->from("category")->where("category_parent_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_parent_category(){
       $this->db->select("category_id,category_name")->from("category")->where("category_parent_id = '0'");
        $query = $this->db->get();
        return $query->result();
    }
     function get_category_detail($id=0) {
        $this->db->select("*")
                ->from("category")
                 ->where("category_parent_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }
    
    function category_delete($id){
        return $query = $this->db->update('category',array('is_deleted' => '1'),array('category_id' => $id));
    }

}
