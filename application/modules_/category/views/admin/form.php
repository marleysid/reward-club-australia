<div class="box">
    <header>
        <div class="icons"><i class="fa fa-edit"></i></div>
        <h5><?php echo $edit ? 'Edit' : 'Add'; ?> category</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <form class="form-horizontal" method="POST" enctype="multipart/form-data">
            <div class="form-group <?php echo form_error('category_name') ? 'has-error' : '' ?>">
                <label for="text1" class="control-label col-lg-4">Title *</label>
                <div class="col-lg-8">
                    <input type="text" id="category_name" placeholder="Name" name="category_name" class="form-control" value="<?php echo set_value("category_name", $edit ? $category_detail->category_title : ''); ?>" >
                    <?php echo form_error('category_name'); ?>
                </div>
            </div>

            <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                <label for="category_icon" class="control-label col-lg-4">Image *</label>
                <div class="col-lg-8">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <span class="btn btn-default btn-file">
                            <span class="fileinput-new">Select Image</span> 
                            <span class="fileinput-exists">Change</span> 
                            <input type="file" name="category_icon">
                        </span> 
                        <span class="fileinput-filename"></span> 
                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                        <?php echo @$logo_error; ?>
                    </div>
                    *PNG
                    <?php if ($edit and file_exists(config_item('category_image_path') . $category_detail->category_icon)): ?>
                        <div>
                            <img src="<?php echo imager(config_item('category_image_path') . $category_detail->category_icon, 128, 128); ?>" alt="category image" class="img-thumbnail">
                            <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                    <?php echo $edit ? anchor('admin/category', 'Cancel', 'class="btn btn-warning"') : ''; ?>
                </div>
            </div>
        </form>		
    </div>
</div>