<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Category_model $category_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('category_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index($id = 0) {
        $id = (int) $id;
        $this->data['edit'] = FALSE;
        if ($id) {
            $this->data['category_detail'] = $this->category_model->where('is_deleted','0')->get($id);
            $this->data['category_detail'] || redirect('admin/category/');
            $this->data['edit'] = TRUE;
        }
        if ($this->input->post()) {
            $this->_add_edit_category($id);
        }
        $this->_manage_categories();

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::render('admin/index', $this->data);
    }

    function _manage_categories() {
        $this->data['categories'] = $this->category_model->where('is_deleted','0')->get_all();
    }

    function _add_edit_category($id) {
//        debug($this->input->post());die;
        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;


        if ($id) {
            $_POST['category_id'] = $id;
            $this->form_validation->set_rules(
                    array(
                        array('field' => 'category_name', 'label' => 'Category Name', 'rules' => 'required|trim|min_length[2]|unique[category.category_name,category.category_id]' . '|xss_clean| '),
                    )
            );
        } else {
            $this->form_validation->set_rules(
                    array(
                        array('field' => 'category_name', 'label' => 'Category Name', 'rules' => 'required|trim|min_length[2]|unique[category.category_name]' . '|xss_clean| '),
                    )
            );
        }
        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }

        $category_image_path = NULL;

        if (isset($_FILES['category_icon']['name']) and $_FILES['category_icon']['name'] != '') {
            if ($result = upload_image('category_icon', config_item('category_image_root'), FALSE,'png')) {
                $image_path = config_item('category_image_path') . $result;
                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
//				resize of file uploaded
                if ($width > 800 || $height > 600) {
                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'), TRUE);
                }
                $category_image_path = $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }

//        unset($data['category_icon']);
        !$category_image_path || ($data['category_icon'] = $category_image_path);

        $data['category_status'] = '1';
        $data['category_title'] = $this->input->post('category_name');
       // $data['category_color_code'] = $this->input->post('color_code');


        if ($id == 0) {
            $this->category_model->insert($data);

            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $this->category_model->update($id, $data);
            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/category/', 'refresh');
    }

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->category_model->update($id, array('category_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function delete($id) {
        if ($this->category_model->category_delete($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/category/', 'refresh');
    }

}
