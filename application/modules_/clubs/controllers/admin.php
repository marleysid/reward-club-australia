<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property club_model $club_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('club_model', 'offers/offer_model'));
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['clubs'] = $this->club_model->get_all();

        Template::render('admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_club(0);
        }
        $this->data['suburb'] = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());
        
         $this->data['postcode'] = array_map(function ($v)
        {
          return $v->postcode;
        },$this->offer_model->get_postcode());
        
         $this->data['state'] = array_map(function ($v)
        {
          return $v->state;
        },$this->offer_model->get_state());
       // debug($this->offer_model->get_suburb());die;
        $this->data['edit'] = FALSE;


        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);


        Template::render('admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('admin/clubs');

        $this->data['club_detail'] = $this->club_model->get($id);
        $this->data['suburb'] = array_map(function ($v) {
            return $v->suburb;
        }, $this->offer_model->get_suburb());
         $this->data['postcode'] = array_map(function ($v)
        {
          return $v->postcode;
        },$this->offer_model->get_postcode());
        
         $this->data['state'] = array_map(function ($v)
        {
          return $v->state;
        },$this->offer_model->get_state());
        
        $this->data['club_detail'] || redirect('admin/club/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_club($id);
        }

        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/jquery-ui.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/autocomplete.js", TRUE);
        Template::add_js(base_url() . "assets/js/chosen.jquery.min.js", TRUE);
        Template::add_js(base_url() . "assets/js/jquery-ui.js", TRUE);



        Template::render('admin/form', $this->data);
    }

    function _add_edit_club($id) {
        $this->load->library('form_validation');
//		$this->form_validation->CI =& $this;
        $data = $this->input->post();
//		debug($data);die;
        $this->form_validation->set_rules(
                array(
                    array('field' => 'club_name', 'label' => 'Name', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'club_address', 'label' => 'Street Address', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'club_suburb', 'label' => 'Suburb', 'rules' => 'required'),
                    array('field' => 'club_postcode', 'label' => 'PostCode', 'rules' => 'required'),
                    array('field' => 'club_state', 'label' => 'State', 'rules' => 'required'),
                    array('field' => 'club_privacy_policy', 'label' => 'Privacy Policy', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'club_website', 'label' => 'Website', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'club_phone', 'label' => 'Phone', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'club_status', 'label' => 'Status', 'rules' => 'trim|xss_clean'),
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }
        $club_image_path = NULL;
        if (isset($_FILES['club_home_image_path']['name']) and $_FILES['club_home_image_path']['name'] != '') {
            if ($result = upload_image('club_home_image_path', config_item('club_image_root'), false)) {
                $image_path = config_item('club_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
//				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'));
//                }
                $club_image_path = config_item('club_image_path') . $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }

        unset($data['club_home_image_path']);
        !$club_image_path || ($data['club_home_image_path'] = $club_image_path);


        $club_logo = NULL;
        if (isset($_FILES['club_logo']['name']) and $_FILES['club_logo']['name'] != '') {
            if ($result = upload_image('club_logo', config_item('club_image_root'), FALSE)) {
                $image_path = config_item('club_image_path') . $result;
//                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
////				resize of file uploaded
//                if ($width > 800 || $height > 600) {
//                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'));
//                }
                $club_logo = config_item('club_image_path') . $result;
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }

        unset($data['club_logo']);
        !$club_logo || ($data['club_logo'] = $club_logo);

        $data['club_status'] = $this->input->post('club_status') ? '1' : '0';

        if ($id == 0) {
            $data['club_code'] = $this->generate_clubcode();
            $this->club_model->insert($data);
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
            $old_image = NULL;
            $old_logo = NULL;
            if ($club_image_path) {
                $old_image = $this->club_model->get($id)->club_home_image_path;
            }
            if ($club_logo) {
                $old_logo = $this->club_model->get($id)->club_logo;
            }
            // debug($this->club_model->get($id)->club_home_image_path);die;
            $this->club_model->update($id, $data);
            if ($old_image and file_exists($old_image))
                unlink_file($old_image);
            if ($old_logo and file_exists($old_logo))
                unlink_file($old_logo);
            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

        redirect('admin/clubs/', 'refresh');
    }

    function generate_clubcode() {
        $random_no = '';
        for ($i = 0; $i < 5; $i++) {
            $random_no.= rand(8, 25);
        }
        $query = $this->db->query("select * from clubs where club_code = '$random_no'");
        if ($query->num_rows() > 0) {
            $query = $query->row();

            $random_no = $this->generate_clubcode();
        }
        return $random_no;
    }

    function toggle_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->club_model->update($id, array('club_status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function delete($id) {
        if ($this->club_model->delete($id)) {
            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/clubs/', 'refresh');
    }

    function _check_suburb($str) {
        if (!empty($_POST['club_suburb']) && !empty($str) && $str && $str != 0) {

            return TRUE;
        } else {
            $this->form_validation->set_message('_check_suburb', 'Please select %s from the suggestion.');
            return FALSE;
        }
    }

}
