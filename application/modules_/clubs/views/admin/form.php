<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5><?php echo $edit ? 'Edit' : 'Add'; ?> club</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>
        <div class="body">
            <form class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="form-group <?php echo form_error('club_name') ? 'has-error' : '' ?>">
                    <label for="club_name" class="control-label col-lg-3">Name *</label>
                    <div class="col-lg-7">
                        <input type="text" id="club_name" placeholder="Name" name="club_name" class="form-control" value="<?php echo set_value("club_name", $edit ? $club_detail->club_name : ''); ?>" >
                        <?php echo form_error('club_name'); ?>
                    </div>
                </div>
                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="club_home_image_path" class="control-label col-lg-3">Image *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span> 
                                <span class="fileinput-exists">Change</span> 
                                <input type="file" name="club_home_image_path">
                            </span> 
                            <span class="fileinput-filename"></span> 
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists($club_detail->club_home_image_path)): ?>
                            <div>
                                <img src="<?php echo imager($club_detail->club_home_image_path, 128, 128); ?>" alt="club image" class="img-thumbnail">
                                <!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                    <label for="club_logo" class="control-label col-lg-3">Logo *</label>
                    <div class="col-lg-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <span class="btn btn-default btn-file">
                                <span class="fileinput-new">Select Image</span> 
                                <span class="fileinput-exists">Change</span> 
                                <input type="file" name="club_logo">
                            </span> 
                            <span class="fileinput-filename"></span> 
                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                            <?php echo @$logo_error; ?>
                        </div>
                        <?php if ($edit and file_exists($club_detail->club_logo)): ?>
                            <div>
                                <img src="<?php echo imager($club_detail->club_logo, 128, 128); ?>" alt="club image" class="img-thumbnail">

                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('club_address') ? 'has-error' : '' ?>">
                    <label for="club_name" class="control-label col-lg-3">Address *</label>
                    <div class="col-lg-7">
                        <input type="text" id="club_name" placeholder="Address" name="club_address" class="form-control" value="<?php echo set_value("club_address", $edit ? $club_detail->club_address : ''); ?>" >
                        <?php echo form_error('club_address'); ?>
                    </div>
                </div>
                <div class="form-group<?php echo form_error('club_suburb') ? 'has-error' : '' ?>">
                    <label for="suburb" class="control-label col-lg-3">Suburb *</label>
                    <div class="col-lg-7">
                        <input type="text" id="suburb" placeholder="Suburb" name="club_suburb" class="form-control suburbs" value="<?php echo set_value("suburb", $edit ? $club_detail->club_suburb : ''); ?>" >
                         <?php echo form_error('club_suburb'); ?>
                    </div>
                </div>
                <div class="form-group<?php echo form_error('club_postcode') ? 'has-error' : '' ?>">
                    <label for="club_postcode" class="control-label col-lg-3">Post Code *</label>
                    <div class="col-lg-7">
                        <input type="text" id="postcode" placeholder="Post Code" name="club_postcode" class="form-control postcode" value="<?php echo set_value("club_postcode", $edit ? $club_detail->club_postcode : ''); ?>" >
                         <?php echo form_error('club_postcode'); ?>
                    </div>
                </div>
                  <div class="form-group<?php echo form_error('club_state') ? 'has-error' : '' ?>">
                    <label for="club_state" class="control-label col-lg-3">State *</label>
                    <div class="col-lg-7">
                        <input type="text" id="state" placeholder="State" name="club_state" class="form-control state" value="<?php echo set_value("offer_post_code", $edit ? $club_detail->club_state : ''); ?>" >
                         <?php echo form_error('club_state'); ?>
                    </div>
                </div>


                <div class="form-group <?php echo form_error('club_privacy_policy') ? 'has-error' : '' ?>">
                    <label for="club_detail" class="control-label col-lg-3">Club Privacy Policy</label>
                    <div class="col-lg-7">
                        <textarea type="text" id="club_detail" placeholder="Club Privacy Policy" name="club_privacy_policy" class="form-control"  ><?php echo set_value("club_privacy_policy", $edit ? $club_detail->club_privacy_policy : ''); ?></textarea>
                        <?php echo form_error('club_privacy_policy'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('club_website') ? 'has-error' : '' ?>">
                    <label for="club_website" class="control-label col-lg-3">Website</label>
                    <div class="col-lg-7">
                        <input type="text" id="club_website" placeholder="Website" name="club_website" class="form-control" value="<?php echo set_value("club_website", $edit ? $club_detail->club_website : ''); ?>" >
                        <?php echo form_error('club_website'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo form_error('club_phone') ? 'has-error' : '' ?>">
                    <label for="club_phone" class="control-label col-lg-3">Phone</label>
                    <div class="col-lg-7">
                        <input type="text" id="club_phone" placeholder="Phone" name="club_phone" class="form-control" value="<?php echo set_value("club_phone", $edit ? $club_detail->club_phone : ''); ?>" >
                        <?php echo form_error('club_phone'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="club_status" class="control-label col-lg-3">Status *</label>
                    <div class="col-lg-7">
                        <input type="checkbox" name="club_status" class="switch switch-small"  value="1" <?php echo set_checkbox('club_status', '1', ($edit) ? ($club_detail->club_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >
                        <?php echo form_error('club_status'); ?>
                    </div>
                </div><!-- /.form-group -->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                        <?php echo anchor('admin/clubs', 'Cancel', 'class="btn btn-warning"'); ?>
                    </div>
                </div>
            </form>		
        </div>
    </div>
</div>
<script>
    $(function() {
        var suburb = <?php echo json_encode($suburb); ?>;
        $(".suburbs").autocomplete({source: function(request, response) {
                var results = $.ui.autocomplete.filter(suburb, request.term);

                response(results.slice(0, 10));
            },
            change: function(event, ui) {
                if (!ui.item) {
                    this.value = '';
                }
            }
        });
         var postcode = <?php echo json_encode($postcode); ?>;
        $(".postcode").autocomplete({source:  function(request, response) {
        var code = $.ui.autocomplete.filter(postcode, request.term);
        
        response(code.slice(0, 10));
    },
    change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
            }
        }
        });
        var state = <?php echo json_encode($state); ?>;
        $(".state").autocomplete({source:  function(request, response) {
        var code = $.ui.autocomplete.filter(state, request.term);
        
        response(code.slice(0, 10));
    },
    change: function (event, ui) {
            if (!ui.item) {
                this.value = '';
            }
        }
        });
    });
</script>