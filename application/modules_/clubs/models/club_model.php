<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Club_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'clubs';
        $this->field_prefix = 'club_';
        $this->log_user = FALSE;
    }

    function get_all_clubs() {
        $query = $this->db->select('*')
                ->from('clubs')
                ->get();

        return $query->result();
    }
    
    

    function search_clubs($limit, $offset, $state, $postcode, $suburb, $clubname) {
  $query =       $this->db->select(' SQL_CALC_FOUND_ROWS *', FALSE)
                          ->from('clubs')
                          ->where('club_suburb like "%' . $suburb . '%"
                                  and club_name like "%' . $clubname . '%"
                                  and club_postcode like "%' . $postcode . '%"
                                  and club_state  like "%' . $state . '%"'
                                  . 'and club_status != "0"')
                          ->limit($limit,$offset)
                          ->order_by('club_name')
                          ->get();
        if ($query->num_rows() > 0) {

            return $query->result();
        } else {
            return false;
        }
    }
    
    function count_clubs(){
         return $this->db->count_all("clubs");
    }
    
    function club_url($id){
       $query = $this->db->select("club_website")
                         ->from("clubs")
                         ->where("club_id = '$id'")
                         ->get();
                 return $query->row();
    }

}
