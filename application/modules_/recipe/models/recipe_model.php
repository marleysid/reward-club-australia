<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Recipe_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'recipe';
        $this->field_prefix = 'recipe_';
        $this->log_user = FALSE;
    }

    function recipe_detail() {
        $this->db->select('recipe.*,recipe_image.ri_image,category.category_title,featured_recipe.featured_id ');
        $this->db->from('recipe');
        $this->db->join('recipe_image', 'recipe.recipe_id = recipe_image.recipe_id', 'left');
        $this->db->join('category', 'recipe.category_id = category.category_id','inner');
        $this->db->join('featured_recipe', 'recipe.recipe_id = featured_recipe.recipe_id','left');
        $this->db->where('recipe.is_deleted = "0"');
        $query = $this->db->get();
        return $query->result();
    }

    function get_ingredients($id) {
        $this->db->select("*")
                ->from("recipe_ingredients")
                ->where("recipe_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }

    function get_image($id) {
        $this->db->select("*")
                ->from("recipe_image")
                ->where("recipe_id = '$id'");
        $query = $this->db->get();
        return $query->row();
    }

    function delete_featured() {
//        $this->db->delete('featured_recipe')->where('1','1');
        $this->db->query('DELETE FROM featured_recipe');
    }

    function featured_recipe() {
        $this->db->select("*");
        $this->db->from("featured_recipe");
        $query = $this->db->get();
        return $query->row();
    }
    function delete_ingredients($id) {
       return $query = $this->db->delete("recipe_ingredients", array("recipe_id" => $id));
  
    }
    function delete_image($id) {
       return $query = $this->db->delete("recipe_image", array("recipe_id" => $id));
  
    }
    function get_attributes(){
        $this->db->select("attribute_id,attribute_title")->from("attributes");
        $query = $this->db->get();
        return $query->result();
    }
   
    function selected_attributes($id){
         $this->db->select("attribute_id")
                ->from("recipe_attributes")
                ->where("recipe_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }
    
    function delete_attr($id){
      return $query = $this->db->delete("recipe_attributes", array("recipe_id" => $id));  
    }
    function delete_recipe($id){
//      $query =  $this->db->query("update recipe set is_deleted = '1' where recipe_id = '$id'");
//        return $query;
        return $query = $this->db->update('recipe',array('is_deleted'=> '1'),array('recipe_id' => $id));
    }
    
    function check_ingredients($id,$ing_name,$ing_unit,$ing_no){
      $query =   $this->db->select("recipe_id")
                ->from("recipe_ingredients")
                ->where("recipe_id = '$id'")
                ->get();
      
        return $query->result();
    }
    
    function delete_recipe_image($id)
    {
        $this->db->delete('recipe_image', array('recipe_id'=>$id),1);
        $this->db->update('recipe', array('modified_date'=>'CURRENT_TIMESTAMP()'), array('recipe_id'=>$id));
        return TRUE;
    }
    
    
    
}
