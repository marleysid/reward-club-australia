<div class="col-lg-12">
    <div class="box">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5> Recipe</h5>
            <div class="toolbar">
                <nav style="padding: 8px;">
                    <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                        <i class="fa fa-minus"></i>
                    </a> 
                    <a class="btn btn-default btn-xs full-box" href="javascript:;">
                        <i class="fa fa-expand"></i>
                    </a> 
                </nav>
            </div>
        </header>

        <div class="body" >
            <form class="form-horizontal" method="POST" action="<?php echo base_url(uri_string()); ?>" enctype="multipart/form-data" id="append">
                <div  role="tabpanel">
                    <ul class="nav nav-tabs" role="tablist">
                        <li><a aria-controls="recipe" role="tab" data-toggle="tab" href="#recipe"><?php echo $edit ? 'Edit' : 'Add'; ?> Recipe</a></li>
                        <li><a href="#ingredient-categories"  aria-controls="ingredient-categories" role="tab" data-toggle="tab">Ingredient categories</a></li>
                        <li><a href="#ingredients"  aria-controls="ingredients" role="tab" data-toggle="tab">Ingredients</a></li>

                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="recipe">

                            <div class="form-group <?php echo form_error('recipe_name') ? 'has-error' : '' ?>">
                                <label for="recipe_name" class="control-label col-lg-3">Name *</label>
                                <div class="col-lg-7">
                                    <input type="text" id="offer_title" placeholder="Name" name="recipe_name" class="form-control" value="<?php echo set_value("recipe_name", $edit ? $recipe_detail->recipe_name : ''); ?>" >
                                    <?php echo form_error('recipe_name'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category_id" class="control-label col-lg-3"> Category *</label>
                                <div class="col-lg-7">
                                    <?php echo $categories; ?>

                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category_id" class="control-label col-lg-3"> Attributes </label>
                                <div class="col-lg-7">
                                    <select name="recipe_attributes[]"  class="form-control attributes"  multiple tabindex="4">
                                       <option value=""></option> 
                                         <?php if($attributes): ?>
                                        <?php foreach($attributes as $attr): ?>
                                         
                                        <option value="<?php echo $attr->attribute_id; ?>" <?php
                                        if($edit){
                                                if (in_array($attr->attribute_id,$selected_attr)) {
                                                    echo "selected=\"selected\" ";
                                        }};
                                                ?> ><?php echo $attr->attribute_title; ?></option>  
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>  
                                   
                                </div>
                            </div>

                            <div class="form-group  <?php echo @$logo_error ? 'has-error' : '' ?>">
                                <label for="offer_image" class="control-label col-lg-3">Image </label>
                                <div class="col-lg-7">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <span class="btn btn-default btn-file">
                                            <span class="fileinput-new">Select Image</span> 
                                            <span class="fileinput-exists">Change</span> 
                                            <input type="file" name="recipe_image">
                                        </span> 
                                        <span class="fileinput-filename"></span> 
                                        <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
                                        <?php echo @$logo_error; ?>
                                    </div>
                                    <div>* square image recommended.</div>
                                    <?php if ($edit and $image_detail and file_exists(($image_detail->ri_image))): ?>
                                        <div>
                                            <img src="<?php echo imager($image_detail->ri_image, 128, 128, 3); ?>" alt="recipe image" class="img-thumbnail">
                                            <button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close delete-img" type="button" data-id="<?php echo $image_detail->recipe_id?>" title="Delete image">×</button>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div><!-- /.form-group -->		  

                            <div class="form-group <?php echo form_error('recipe_desc') ? 'has-error' : '' ?>">
                                <label for="offer_desc" class="control-label col-lg-3"> Description *</label>
                                <div class="col-lg-7">
                                    <textarea rows="10" type="text" placeholder=" Description" name="recipe_desc" class="form-control"><?php echo set_value("recipe_desc", $edit ? $recipe_detail->recipe_desc : ''); ?></textarea>
                                    <?php echo form_error('recipe_desc'); ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo form_error('recipe_nutritional_info_title') ? 'has-error' : '' ?>">
                                <label for="recipe_nutritional_info_title" class="control-label col-lg-3"> Nutritional Info Title</label>
                                <div class="col-lg-7">
                                    <input type="text" placeholder=" Information Title" name="recipe_nutritional_info_title" class="form-control" value="<?php echo set_value("recipe_nutritional_info_title", $edit ? $recipe_detail->recipe_nutritional_info_title : ''); ?>" >
                                    <?php echo form_error('recipe_nutritional_info_title'); ?>
                                </div>
                            </div>
                            <div class="form-group <?php echo form_error('recipe_nutritional_info') ? 'has-error' : '' ?>">
                                <label for="recipe_nutritional_info" class="control-label col-lg-3"> Nutritional Info *</label>
                                <div class="col-lg-7">
                                    <textarea rows="10" type="text" placeholder=" Information" name="recipe_nutritional_info" class="form-control"><?php echo set_value("recipe_nutritional_info", $edit ? $recipe_detail->recipe_nutritional_info : ''); ?></textarea>
                                    <?php echo form_error('recipe_nutritional_info'); ?>
                                </div>
                            </div>

                            <div class="form-group <?php echo form_error('recipe_method') ? 'has-error' : '' ?>">
                                <label for="recipe_method" class="control-label col-lg-3"> Method *</label>
                                <div class="col-lg-7">
                                    <textarea rows="10" type="text" placeholder="Method" name="recipe_method" class="form-control"><?php echo set_value("recipe_method", $edit ? $recipe_detail->recipe_methods : ''); ?></textarea>
                                    <?php echo form_error('recipe_method'); ?>
                                    Separate the steps by pipeline "|"
                                </div>
                            </div>
                            
                            <div class="form-group <?php echo form_error('recipe_method_note') ? 'has-error' : '' ?>">
                                <label for="recipe_method_note" class="control-label col-lg-3"> Method Note </label>
                                <div class="col-lg-7">
                                    <textarea rows="4" type="text" placeholder="Method Note" name="recipe_method_note" class="form-control"><?php echo set_value("recipe_method_note", $edit ? $recipe_detail->recipe_method_note : ''); ?></textarea>
                                    <?php echo form_error('recipe_method_note'); ?>
                                    Separate the steps by pipeline "|"
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="recipe_served" class="control-label col-lg-3"> No of served </label>
                                <div class="col-lg-7">
                                    <input type="text"  placeholder="No of served" name="recipe_served" class="form-control" value="<?php echo set_value("recipe_served", $edit ? $recipe_detail->recipe_served : ''); ?>" >

                                </div>
                            </div>


                        </div>
                        <div role="tabpanel" class="tab-pane" id="ingredient-categories">

                            <div>
                                <fieldset>

                                                <div class="form-group">
                                                    <label for="ingredient_name" class="control-label col-lg-3"> Ingredient Categories *</label>
                                                    <div class="col-lg-7">
                                                        <select id="ingredient_category" multiple data-role="tagsinput">
                                                        </select>
                                                    </div>
                                                </div>
                                            </fieldset>
                                
                                <?php // if (isset($ingredients_detail)): ?>
                                    <?php // foreach ($ingredients_detail as $detail): ?>
<!--                                        <div class="source_tags">
                                            <fieldset>
                                                <input type="hidden"  name="ingredient_id[]" value="<?php echo $detail->ingredient_id ; ?>" >

                                                <div class="form-group">
                                                    <label for="ingredient_name" class="control-label col-lg-3"> Ingredient's Name *</label>
                                                    <div class="col-lg-7">
                                                        
                                                        <input type="text"  placeholder="Name" name="ingredient_name[]" class="form-control" value="<?php echo set_value("ingredient_name", $edit ? $detail->ingredient_name : ''); ?>" >

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ingredient_number" class="control-label col-lg-3"> Ingredient's Number *</label>
                                                    <div class="col-lg-7">
                                                        <input type="text"  placeholder="Number" name="ingredient_number[]" class="form-control" value="<?php echo set_value("ingredient_number", $edit ? $detail->ingredient_number : ''); ?>" >

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ingredient_unit" class="control-label col-lg-3"> Ingredient's Unit *</label>
                                                    <div class="col-lg-7">
                                                        <input type="text"  placeholder="Unit" name="ingredient_unit[]" class="form-control" value="<?php echo set_value("ingredient_unit", $edit ? $detail->ingredient_unit : ''); ?>" >

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-8">
                                                        <a href="#" class="remove_fields">Remove</a><br><br>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>-->
                                    <?php // endforeach; ?>
                                <?php // endif; ?>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <button class="btn btn-default " id="add-ingredient-category" type="button"   >Add Ingredient Category</button></div></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="ingredients">

                            <div id="source" >
                                <?php if (isset($ingredients_detail)): ?>
                                    <?php foreach ($ingredients_detail as $detail): ?>
                                        <div class="source_tags">
                                            <fieldset>
                                                <input type="hidden"  name="ingredient_id[]" value="<?php echo $detail->ingredient_id ; ?>" >

                                                <div class="form-group">
                                                    <label for="ingredient_name" class="control-label col-lg-3"> Ingredient's Name *</label>
                                                    <div class="col-lg-7">
                                                        
                                                        <input type="text"  placeholder="Name" name="ingredient_name[]" class="form-control" value="<?php echo set_value("ingredient_name", $edit ? $detail->ingredient_name : ''); ?>" >

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ingredient_number" class="control-label col-lg-3"> Ingredient's Number *</label>
                                                    <div class="col-lg-7">
                                                        <input type="text"  placeholder="Number" name="ingredient_number[]" class="form-control" value="<?php echo set_value("ingredient_number", $edit ? $detail->ingredient_number : ''); ?>" >

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="ingredient_unit" class="control-label col-lg-3"> Ingredient's Unit *</label>
                                                    <div class="col-lg-7">
                                                        <input type="text"  placeholder="Unit" name="ingredient_unit[]" class="form-control" value="<?php echo set_value("ingredient_unit", $edit ? $detail->ingredient_unit : ''); ?>" >

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-8">
                                                        <a href="#" class="remove_fields">Remove</a><br><br>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-8">
                                    <button class="btn btn-info3 " type="button"  class="btn btn-default" style="padding: 5px 10px; font-size: 12px;
                                            line-height: 1.5; border-radius: 3px;" >Add More Ingredients</button></div></div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
                                <?php echo anchor('admin/recipe', 'Cancel', 'class="btn btn-warning"'); ?>
                            </div>
                        </div>

                    </div>

                </div>
            </form>
        </div>

    </div>
</div>
</script>
<script src="<?php echo base_url('assets') . '/js/jquery.tagsinput.js' ?>"></script>

<script>
    $(document).ready(function() {
        $('#add-ingredient-category').click(function(e) {

            e.preventDefault();
            var app_html = $('#ingredient_category_form').html();
            tot = $('#source').append(app_html);
        });
        $(document).on('click', '.remove_category_fields', function(e) {
            e.preventDefault();
            $(this).parents('.source_tags:first').remove();
            
        });
        
        $('.btn-info3').click(function(e) {

            e.preventDefault();
            var app_html = $('#ingredients_form').html();
            tot = $('#source').append(app_html);
        });
        $(document).on('click', '.remove_fields', function(e) {
            e.preventDefault();
            $(this).parents('.source_tags:first').remove();
            
        });

    });
</script>
<div style="display:none" id='ingredient_category_form'>
    <div class="source_tags">
        <fieldset>
            <div class="form-group">
                <label for="ingredient_category_title" class="control-label col-lg-3"> Title *</label>
                <div class="col-lg-7">
                    <input type="text"  placeholder="Title" name="ingredient_category_title[]" class="form-control" value="" >
                    <a href="#" class="remove_category_fields">Remove</a>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<div style="display:none" id='ingredients_form'>
    <div class="source_tags">
        <fieldset>
            <div class="form-group">
                <label for="ingredient_name" class="control-label col-lg-3"> Ingredient's Name *</label>
                <div class="col-lg-7">
                    <input type="text" id="short_desc" placeholder="Name" name="ingredient_name[]" class="form-control" value="" >

                </div>
            </div>
            <div class="form-group">
                <label for="ingredient_number" class="control-label col-lg-3"> Ingredient's Number *</label>
                <div class="col-lg-7">
                    <input type="text" id="short_desc" placeholder="Number" name="ingredient_number[]" class="form-control" value="" >

                </div>
            </div>
            <div class="form-group">
                <label for="ingredient_unit" class="control-label col-lg-3"> Ingredient's Unit *</label>
                <div class="col-lg-7">
                    <input type="text" id="short_desc" placeholder="Unit" name="ingredient_unit[]" class="form-control" value="" >

                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-8">
                    <a href="#" class="remove_fields">Remove</a><br><br>
                </div>
            </div>
        </fieldset>
    </div>
</div>
<script>
$(document).ready(function(){
	$(".attributes").chosen();
    
    $('.delete-img').on('click', function(){
        if(confirm('Are you sure you want to delete?')) {
            var _id = $(this).data('id'),
                _this = this;
            $.post( "<?php echo site_url('admin/recipe/delete_image'); ?>/" + _id, function( data ) {
                
                if(data)
                    $(_this).parent().remove();
              });
            
        }
    });
    
    $('#ingredient_category').on('beforeItemAdd', function(event) {
    console.log(event);
      // event.item: contains the item
      // event.cancel: set to true to prevent the item getting added
    });
});

</script>