<div class="box">
    <header>
        <div class="icons"><i class="fa fa-table"></i></div>
        <h5>Manage Recipe</h5>
        <div class="toolbar">
            <nav style="padding: 8px;">
                <a class="btn btn-default btn-xs collapse-box" href="javascript:;">
                    <i class="fa fa-minus"></i>
                </a> 
                <a class="btn btn-default btn-xs full-box" href="javascript:;">
                    <i class="fa fa-expand"></i>
                </a> 
            </nav>
        </div>
    </header>
    <div class="body">
        <table class="table table-bordered table-condensed table-hover table-striped dataTable" id="company_tbl">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th> Category</th>
                    <th>Recipe Image</th>
                    <th> Description </th>
                    <th style="width:85px;" >Featured</th>
                    <th style="width:90px;" >Settings</th>
                </tr>
            </thead>

            <tbody role="alert" aria-live="polite" aria-relevant="all" >
                <?php if ($recipe): ?>
                    <?php foreach ($recipe as $key => $val): ?>
                        <tr class="odd">
                            <td><?php echo ++$key; ?></td>
                            <td><?php echo $val->recipe_name; ?></td>
                            <td><?php echo $val->category_title; ?></td>
                            <td><?php if (file_exists($val->ri_image) and $val->ri_image != null): ?><a title="<?php echo $val->recipe_name; ?>" href="<?php echo base_url($val->ri_image); ?>" class="img-popup"><img class="img-thumbnail" src="<?php echo imager($val->ri_image, 80, 80, 3); ?>" /></a><?php endif; ?></td>
                            <td><?php echo word_limiter($val->recipe_desc, 45); ?></td>
                            <td class="center"><input type="checkbox" name="my-checkbox" <?php if($val->featured_id) echo 'checked'; ?>  class="ajax-toggle switch" data-toggle-href="<?php echo base_url('admin/recipe/toggle_feature/'); ?>" data-id="<?php echo $val->recipe_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
                            <td class="center">
                                <a href="<?php echo base_url('admin/recipe/edit/' . $val->recipe_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
                                <a href="<?php echo base_url('admin/recipe/delete/' . $val->recipe_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if (!confirm("Are you sure to delete?"))
                                            return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>

                            </td>
                        </tr>
                    <?php endforeach; ?>
<?php endif; ?>
            </tbody>
        </table>

    </div>
</div>
<script>
    $(document).ready(function() {
        $('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', function(event,data) {
        var feature = $(this).is(':checked');
         if(feature==true){
            $('input[name="my-checkbox"]').not(this).bootstrapSwitch('state', false, false);
         }
        });
    });

</script>