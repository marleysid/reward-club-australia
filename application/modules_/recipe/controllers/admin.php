<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * @property Company_model $company_model 
 * @property Review_model $review_model 
 */
class Admin extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('recipe_model');
        $this->load->model('category/category_model');
        $this->load->helper('image');
        $this->data['logo_error'] = '';
    }

    public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }

    public function index() {
        $this->data['recipe'] = $this->recipe_model->recipe_detail();

        Template::render('admin/index', $this->data);
    }

    function add() {
        if ($this->input->post()) {
            $this->_add_edit_recipe(0);
        }

        $this->data['edit'] = FALSE;

        $this->load->model(array('category/category_model'));
        $this->data['categories'] = render_select($this->category_model->get_all(), 'category_id', set_value('category_id'), 'class="form-control" id="parentcat" ', 'category_id', 'category_title');
	//$this->data['attributes'] = render_select($this->recipe_model->get_attributes(), 'region_id', set_value('attribute_id',$this->data['edit'] ? $this->data['company_detail']->region_id : FALSE), 'class="form-control" ', 'attribute_id', 'attribute_title');
	//$this->data['attributes'] = render_select($this->recipe_model->get_attributes(), 'atrribute_id', set_value('attribute_id'), 'class="form-control  attributes" ', 'attribute_id', 'attribute_title');
//        
        $this->data['attributes'] = $this->recipe_model->get_attributes();
        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
        Template::add_css(base_url() . "assets/css/chosen.min.css");
        Template::add_js(base_url() . "assets/js/chosen.jquery.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");
        Template::add_js(base_url() . "assets/lib/bootstrap-tagsinput/bootstrap-tagsinput.js", TRUE);
        Template::add_css(base_url() . "assets/lib/bootstrap-tagsinput/bootstrap-tagsinput.css");

        Template::render('admin/form', $this->data);
    }

    function edit($id = 0) {
        $id = (int) $id;

        $id || redirect('admin/offers');

        $this->data['recipe_detail'] = $this->recipe_model->get($id);
        $this->data['ingredients_detail'] = $this->recipe_model->get_ingredients($id);
        $this->data['image_detail'] = $this->recipe_model->get_image($id);
        // $this->data['category_info'] = $this->category_model->get($this->data['recipe_id']->category_id);
        //echo ($this->data['recipe_detail']->category_id);die;
        $this->data['categories'] = render_select($this->category_model->get_all(), 'category_id', set_value('category_id', $this->data['recipe_detail']->category_id), 'class="form-control"  ', 'category_id', 'category_title');

        $this->data['attributes'] = $this->recipe_model->get_attributes();
        $this->data['selected_attr'] = array_map(function ($arg) {
            return $arg->attribute_id;
        }, $this->recipe_model->selected_attributes($id));
    //  debug($this->data['selected_attr']);
        $this->data['recipe_detail'] || redirect('admin/recipe/');
        $this->data['edit'] = TRUE;

        if ($this->input->post()) {
            $this->_add_edit_recipe($id);
        }




        Template::add_css(base_url() . "assets/lib/jasny/css/jasny-bootstrap.min.css");
        Template::add_css(base_url() . "assets/css/chosen.min.css");
        Template::add_js(base_url() . "assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
         Template::add_js(base_url() . "assets/js/chosen.jquery.min.js", TRUE);
        Template::add_css(base_url() . "assets/lib/jQuery-Autocomplete/content/styles.css");


        Template::render('admin/form', $this->data);
    }

    function _add_edit_recipe($id) {

        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;

        $data['recipe_name'] = $this->input->post('recipe_name');
        $data['category_id'] = $this->input->post('category_id');
        $data['recipe_desc'] = $this->input->post('recipe_desc');
        $data['recipe_nutritional_info'] = $this->input->post('recipe_nutritional_info');
        $data['recipe_nutritional_info_title'] = $this->input->post('recipe_nutritional_info_title');
        $data['recipe_methods'] = $this->input->post('recipe_method');
        $data['recipe_method_note'] = $this->input->post('recipe_method_note');
//        debug(json_encode(explode('||',$method)));die;
        // $data['recipe_methods'] = json_encode(explode('||',$method));
        $data['recipe_served'] = $this->input->post('recipe_served');
        

//        $data = $this->input->post();
        $this->form_validation->set_rules(
                array(
                    array('field' => 'recipe_name', 'label' => 'Name', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'recipe_desc', 'label' => 'Description', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'recipe_method', 'label' => 'Method', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    array('field' => 'recipe_nutritional_info', 'label' => 'Nutritional Information', 'rules' => 'required|trim|min_length[2]|xss_clean'),
                    
                )
        );

        $this->form_validation->set_error_delimiters('<span class="help-block">', '</span>');

        if ($this->form_validation->run($this) === FALSE) {
            return FALSE;
        }
//        debug($_FILES);die;
        $recipe_image_path = NULL;
        if (isset($_FILES['recipe_image']['name']) and $_FILES['recipe_image']['name'] != '') {
            if ($result = upload_image('recipe_image', config_item('recipe_image_root'), FALSE)) {
                $image_path = config_item('recipe_image_path') . $result;
                list($width, $height, $imgtype, $attr) = getimagesize($image_path);
//				resize of file uploaded
                if ($width > 800 || $height > 600) {
                    create_thumb($image_path, $image_path, array('w' => '800', 'h' => '600'), TRUE);
                }
                $recipe_image_path = $image_path;
                
                $image_size_diff =  abs( $width - $height ) ;
                if( $image_size_diff > 5 )
                    $this->session->set_flashdata('warning_message', 'The uploaded image is not square format. It is recommended to use square image.');
                
            } else {

                $this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">', '</span>');
                return FALSE;
            }
        }
        !$recipe_image_path || ($data['recipe_image'] = $recipe_image_path);


        if ($id == 0) {
            $recipe_id = $this->recipe_model->insert($data);

            $ing_name = $this->input->post('ingredient_name');
            $ing_number = $this->input->post('ingredient_number');
            $ing_unit = $this->input->post('ingredient_unit');
            
            $attribute = $this->input->post('recipe_attributes');
            
            $this->recipe_ingredients($recipe_id, $ing_name, $ing_number, $ing_unit);
            if (isset($data['recipe_image'])) {
                $test = array('recipe_id' => $recipe_id, 'ri_image' => $data['recipe_image']);
                $this->db->insert('recipe_image', $test);
            }
           if($attribute){
               foreach($attribute as $attr){
                   $recipe_attr = array('recipe_id' => $recipe_id, 'attribute_id' => $attr);
                   $this->db->insert('recipe_attributes', $recipe_attr);
               }
           }
            
            $this->session->set_flashdata('success_message', 'Data inserted successfully.');
        } else {
          //  debug($this->input->post());die;
            $this->recipe_model->update($id, $data);
           
            $old_logo = NULL;
             
            if ($recipe_image_path) {
                $old_logo = @$this->recipe_model->get_image($id)->ri_image;
                $this->recipe_model->delete_image($id);
                $this->db->insert('recipe_image', array('recipe_id' => $id, 'ri_image' => $recipe_image_path));
          
                if ($old_logo and file_exists($old_logo))
                    unlink_file( $old_logo);
        
            }
           // debug($this->recipe_model->get_image($id)->ri_image);die;
           $attribute = $this->input->post('recipe_attributes');
            
           $this->recipe_model->delete_attr($id);
            
            if($attribute){
               foreach($attribute as $attr){
                   $recipe_attr = array('recipe_id' => $id, 'attribute_id' => $attr);
                   $this->db->insert('recipe_attributes', $recipe_attr);
               }
           }

           // $this->recipe_model->delete_ingredients($id);
           $ing_id = $this->input->post('ingredient_id');
            $ing_name = $this->input->post('ingredient_name');
            $ing_number = $this->input->post('ingredient_number');
            $ing_unit = $this->input->post('ingredient_unit');
            

            $this->recipe_ingredients($id, $ing_name, $ing_number, $ing_unit, $ing_id);

            $this->session->set_flashdata('success_message', 'Data updated successfully.');
        }

       redirect('admin/recipe/', 'refresh');
    }

    function recipe_ingredients($id, $name, $number, $unit, $ing_id=null) {

        $batchArray = array();
        $updatebatchArray = array();
        
        if (is_array($name)) {
            foreach ($name as $k => $title) {

                if(isset($ing_id[$k]) and !empty($ing_id[$k]) ) {
                    $updatebatchArray[$k]['recipe_id'] = $id;
                    $updatebatchArray[$k]['ingredient_id'] = $ing_id[$k];
                    $updatebatchArray[$k]['ingredient_name'] = $title;
                    $updatebatchArray[$k]['ingredient_number'] = $number[$k];
                    $updatebatchArray[$k]['ingredient_unit'] = $unit[$k];
                } else {
                    $batchArray[$k]['recipe_id'] = $id;
                    $batchArray[$k]['ingredient_name'] = $title;
                    $batchArray[$k]['ingredient_number'] = $number[$k];
                    $batchArray[$k]['ingredient_unit'] = $unit[$k];
                    
                }
            }
        }
//        if (is_array($number)) {
//            foreach ($number as $k => $no) {
//                $batchArray[$k]['ingredient_number'] = $no;
//            }
//        }
//        if (is_array($unit)) {
//            foreach ($unit as $k => $u) {
//                $batchArray[$k]['ingredient_unit'] = $u;
//            }
//        }

        if (!empty($batchArray))
            $this->db->insert_batch('recipe_ingredients', $batchArray);
        
        if (!empty($updatebatchArray))
            $this->db->update_batch('recipe_ingredients', $updatebatchArray, 'ingredient_id');
        
        
    }

    function toggle_feature() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $featured = $this->input->post('status') == 'true' ? '1' : '0';
            $test = $this->recipe_model->featured_recipe();
            if ($featured == '1' && $test == null) {
                $this->db->insert('featured_recipe', array('recipe_id' => $id));
            } elseif ($featured == '1' && $test != null) {
                $this->recipe_model->delete_featured();
                $this->db->insert('featured_recipe', array('recipe_id' => $id));
            } elseif ($featured == '0') {
                $this->recipe_model->delete_featured();
            }

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function toggle_review_status() {
        if ($this->input->is_ajax_request()) {
            $id = $this->input->post('id');
            $status = $this->input->post('status') == 'true' ? '1' : '0';
            $this->load->model(array('review_model'));
            $this->review_model->update($id, array('status' => $status));

            $this->output->set_content_type('application/json')->set_output(json_encode(array('status' => 'ok')));
        }
    }

    function delete($id) {
        if ($this->recipe_model->delete_recipe($id)) {

            $this->session->set_flashdata('success_message', 'Data deleted successfully.');
        } else {
            $this->session->set_flashdata('error_message', 'Data deletion failed.');
        }
        redirect('admin/recipe/', 'refresh');
    }

    function delete_image($id) {
        if ($this->recipe_model->delete_recipe_image($id)) {

            echo json_encode(array('status'=> 1));
        } else {
            echo json_encode(array('status'=> 0));
        }
    }

}
