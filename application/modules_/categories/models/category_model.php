<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'category';
        $this->field_prefix = 'category_';
        $this->log_user = FALSE;
    }

    function get_category_parent() {
        $this->db->select('c1.category_id,c1.category_name,c2.category_name as parent');
        $this->db->from('category c1');
        $this->db->join('category c2','c1.category_parent_id=c2.category_id ','left');
//        $query = $this->db->query("SELECT c1.category_id,c1.category_name,c2.category_name AS parent FROM category c1
//                LEFT JOIN category c2 
//                ON c1.`category_parent_id`=c2.`category_id`");
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_subcategory($id){
        $this->db->select("category_id,category_name,category_icon")->from("category")->where("category_parent_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }
    
    function get_parent_category(){
       $this->db->select("category_id,category_name")->from("category")->where("category_parent_id = '0' and category_status = '1'");
        $query = $this->db->get();
        return $query->result();
    }
     function get_category_detail($id=0) {
        $this->db->select("*")
                ->from("category")
                 ->where("category_parent_id = '$id'");
        $query = $this->db->get();
        return $query->result();
    }
    function parent_cat(){
       $query =  $this->db->select('category_name, category_id,category_icon')
                          ->from("category")
                          ->where('category_status', '1')
                          ->where('category_parent_id', '0')
                          ->order_by('category_name', 'desc')
                          ->get();
    return $query->result();
    }
}
