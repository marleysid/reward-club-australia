
<?php
if (isset($error) && $error) {
    echo $error;
}
if ($this->session->flashdata('msg')) {
    echo $this->session->flashdata('msg');
}
echo form_open();
?>

<div>
    <label>Email</label><input type="email" name="email" value="<?php echo set_value('email'); ?>"/>
    <?php echo form_error('email'); ?>
</div>
<div>
    <label>Password</label><input type="password" name="password" value="<?php echo set_value('password'); ?>"/>
    <?php echo form_error('password'); ?>
</div>
<div>
    <input type="submit" name="login" value="Login"/>
</div>
<div>
    <a href="<?php echo site_url('user/register'); ?>" >Register</a>
</div>
<!--<div>
    <a href="<?php echo site_url('user/forgot_password'); ?>" >Forgot Password</a>
</div>-->
<?php echo form_close(); ?>