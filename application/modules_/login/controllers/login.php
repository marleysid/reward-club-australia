<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of login
 *
 * @author rabin
 */
class Login extends Front_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('form_validation');
        $this->form_validation->CI = & $this;
        $this->template->set_layout('template/frontend_template');
    }

    public function index() {
//        if ($this->member_auth->logged_in())
//            redirect('welcome');
        $status = 0;
        $this->data = array();

        $this->form_validation->set_rules(array(
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'valid_email|xss_clean|trim|required|max_length[200]'
            ),
            array(
                'field' => 'password',
                'password' => 'Password',
                'rules' => 'trim|xss_clean|required|max_length[50]'
            ),
        ));

        if ($this->form_validation->run() == TRUE) {
            $email = $this->input->post('email');
            $password = $this->input->post('password');
            if ($this->member_auth->login($email, $password)) {
//                echo 'user logged in';
                //redirect to previous url if redirect is set
//                if ($this->input->get('redirect'))
//                    redirect($_SERVER['HTTP_REFERER']);
//                redirect('welcome');
                 $status = 1;
                echo json_encode(array('status'=>$status));
            }
            else{
                $status = 0;
                $msg = $this->member_auth->get_errors();
                echo json_encode(array('status'=>$status, 'msg'=> $msg));
            }
            //login error occured
           
        }
        else{
            $status = 0;
                $msg = validation_errors();
                echo json_encode(array('status'=>$status, 'msg'=> $msg));
        }
        //$this->template->render('frontend/login', $this->data);
    }

    public function register() {
//       if ($this->member_auth->logged_in())
//          redirect('welcome');

       // echo'hello';die;
       // echo $this->input->post('first_name');
       $status = 0;
        $this->data = array();

        $this->form_validation->set_rules(array(
            array(
                'field' => 'first_name',
                'label' => 'First Name',
                'rules' => 'xss_clean|trim|required|max_length[200]'
            ),
            array(
                'field' => 'last_name',
                'label' => 'Last Name',
                'rules' => 'xss_clean|trim|required|max_length[200]'
            ),
            
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'valid_email|xss_clean|trim|required|max_length[200]|unique[members.member_email]'
            ),
            array(
                'field' => 'postcode',
                'label' => 'Postcode',
                'rules' => 'xss_clean|trim|required|max_length[10]'
            ),
            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'xss_clean|trim|max_length[500]'
            ),
            array(
                'field' => 'membership_type',
                'label' => 'Membership Type',
                'rules' => 'xss_clean|trim|max_length[12]|integer|required'
            ),
            array(
                'field' => 'confirm_password',
                'password' => 'Confirm Password',
                'rules' => 'trim|xss_clean|required|max_length[50]|matches[password]'
            ),
            array(
                'field' => 'password',
                'password' => 'Password',
                'rules' => 'trim|xss_clean|required|max_length[50]'
            ),
        ));

        if ($this->form_validation->run() == TRUE) {
           // echo 'hi';die;
            $email = $_POST['email'];
            $password = $_POST['password'];
            $additionalData = array(
                'member_first_name' => $this->input->post('first_name'),
                'member_last_name' => $this->input->post('first_name'),
                'member_contact_no' => $this->input->post('mobile'),
                'member_postcode' => $this->input->post('postcode'),
                'member_address' => $this->input->post('address'),
                'membership_type_id' => $this->input->post('membership_type')
            );
            if ($userId = $this->member_auth->register($email, $password, $additionalData)) {
               // echo 'hi';die;
//                $this->data['msg'] = $this->member_auth->get_message();
                $this->session->set_flashdata('msg', $this->member_auth->get_messages());
                $status = 1;
//                redirect('user/login');
                echo json_encode(array('status'=>$status));
            } else {
                $status = 0;
                $msg = $this->member_auth->get_errors();
                echo json_encode(array('status'=>$status, 'msg'=> $msg));
            }
        } else {
                $status = 0;
                $msg = validation_errors();
                echo json_encode(array('status'=>$status, 'msg'=> $msg));
            
        }
        
    }

    public function logout() {
        if ($this->member_auth->logged_in()) {
            $this->member_auth->logout();
            $this->session->set_flashdata('msg', $this->member_auth->get_messages());
        }
        redirect('');
    }

    public function forgot_password() {
        
    }

}
