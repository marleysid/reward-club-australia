<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News extends Front_Controller {

   // public $data;
    
    function __construct() {
        parent::__construct();
        $this->load->model(array('news_model','advertisement/advertisement_model'));
         
    }
     public function _remap($method, $params) {
        if (method_exists($this, $method)) {
            return call_user_func_array(array($this, $method), $params);
        } else
            return $this->index($method);
    }
 
    public function index() {
        $id = safe_b64decode($this->uri->segment(2));
        $this->data['news_detail'] = $this->news_model->news_detail_page($id);
        $this->data['get_ads'] = $this->advertisement_model->ads_for_detail_page();
        Template::render('frontend/index', $this->data);
    }
    function subscribe(){
        $name = $this->input->post('member_name');
        $email = $this->input->post('member_email');
        $check_subscribe = $this->news_model->check_subscription($email);
        if($check_subscribe == FALSE){
            $this->db->insert('newsletter_subscription',array('name' => $name,
                                                               'email' => $email));
            $this->session->set_flashdata('Thank you for subscription');
        } else{
            $this->session->set_flashdata('You have already subscribed');
        }
        redirect('/');
    }
}