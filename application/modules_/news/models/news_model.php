<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class News_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'news';
        $this->field_prefix = 'news_';
        $this->log_user = FALSE;
    }

    function news_detail() {
        $query = $this->db->select("*")
                ->from("news")
                ->where("is_deleted = '0'")
                ->order_by("news_create_date desc")
                ->get();

        return $query->result();
    }

    function news() {
        $query = $this->db->select("*")
                ->from("news")
                ->where("is_deleted = '0'")
                ->where("news_status = '1'")
                ->order_by("news_create_date desc")
                ->get();

        return $query->result();
    }

    function news_detail_page($id) {
        $query = $this->db->select("*")
                ->from("news")
                ->where("news_id = '$id'")
                ->get();
        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    function delete_news($id) {
        return $query = $this->db->update('news', array('is_deleted' => '1'), array('news_id' => $id));
    }

    function check_subscription($email) {
        $query = $this->db->select("newsletter_id")
                ->from("newsletter_subscription")
                ->where("email = '$email'")
                ->get();

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
  

}
