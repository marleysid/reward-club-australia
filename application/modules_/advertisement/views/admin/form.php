<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>
		<h5><?php echo $edit ? 'Edit' : 'Add'; ?> Advertisement</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data">
		  <div class="form-group <?php echo form_error('ad_name')?'has-error':''?>">
			<label for="text1" class="control-label col-lg-4">Name *</label>
			<div class="col-lg-8">
				<input type="text" id="ad_name" placeholder="Name" name="ad_name" class="form-control" value="<?php echo set_value("ad_name", $edit ? $advertisement_detail->ad_name : ''); ?>" >
				<?php echo form_error('ad_name'); ?>
			</div>
		  </div>
		  <div class="form-group">
			<label for="ad_link" class="control-label col-lg-4">Ad Link *</label>
			<div class="col-lg-8">
				<input type="text" id="ad_link" placeholder="Ad Link" name="ad_link" class="form-control" value="<?php echo set_value("ad_link", $edit ? $advertisement_detail->ad_link : ''); ?>" >
				<?php echo form_error('ad_link'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group">
			<label for="tags" class="control-label col-lg-4">Location *</label>
			<div class="col-lg-8">
				<?php  echo $ad_location; ?>
				<?php echo form_error('ad_location'); ?>
			</div>
		  </div>  
		  <div class="form-group  <?php echo @$logo_error?'has-error':''?>">
			  <label for="ad_image_path" class="control-label col-lg-4">Advertisement<br> Image *</label>
			<div class="col-lg-8">
			  <div class="fileinput fileinput-new" data-provides="fileinput">
				<span class="btn btn-default btn-file">
				<span class="fileinput-new">Select Image</span> 
				<span class="fileinput-exists">Change</span> 
				  <input type="file" name="ad_image_path">
				</span> 
				<span class="fileinput-filename"></span> 
				<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
				<?php echo @$logo_error; ?>
                ( 320 * 100 )
			  </div>
			  <?php if($edit and file_exists(config_item('ad_image_path').$advertisement_detail->ad_image_path)): ?>
				<div>
					<img  width="150" height="150"  src="<?php echo base_url(config_item('ad_image_path').$advertisement_detail->ad_image_path); ?>" alt="company image" class="img-thumbnail">
					<!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
				</div>
				<?php endif; ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group">
			<label for="tags" class="control-label col-lg-4">Status *</label>
			<div class="col-lg-8">
				<input type="checkbox" name="ad_status" class="switch switch-small"  value="1" <?php echo set_checkbox('ad_status', '1', ($edit) ? ($advertisement_detail->ad_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >
				<?php echo form_error('ad_status'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group">
			  <div class="col-sm-offset-4 col-sm-9"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
				  <?php echo $edit ? anchor('admin/advertisement', 'Cancel', 'class="btn btn-warning"'):''; ?>
			  </div>
		  </div>
		</form>		
	</div>
</div>