<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
 * @property Region_model $advertisement_model 
 * @property General_model $general_model
 */
class Admin extends Admin_Controller 
{	
	function __construct() {
		parent::__construct();	
		$this->load->model('advertisement_model');
		$this->load->helper('image');
		$this->data['logo_error'] = '';
		
	}
	
	public function _remap($method, $params )
	{
		if (method_exists($this, $method))
		{
			return call_user_func_array(array($this, $method), $params);
		} else 
			return $this->index($method);
		
	}

	public function index($id = 0) 
	{
		$id = (int) $id;
		$this->load->model('general_model');
		$this->data['edit'] = FALSE;
		if($id) {
			$this->data['advertisement_detail'] = $this->advertisement_model->get($id);
			$this->data['advertisement_detail'] || redirect('admin/advertisement/');
			$this->data['edit'] = TRUE;
		}
		if($this->input->post()) {
			$this->_add_edit_advertisement($id);
		}
		$this->_manage_advertisements();
		
		$this->data['ad_location'] = form_dropdown('ad_location', config_item('ad_location'), set_value('ad_location',$this->data['edit'] ? $this->data['advertisement_detail']->ad_location : FALSE), 'class="form-control" ');
		Template::add_css( base_url()."assets/lib/jasny/css/jasny-bootstrap.min.css");
		Template::add_js( base_url()."assets/lib/jasny/js/jasny-bootstrap.min.js", TRUE);
		
        Template::render('admin/index', $this->data);
	
	}
	
	function _manage_advertisements()
	{
		$this->data['advertisements'] = $this->advertisement_model->get_all();
		
	}
	
	function _add_edit_advertisement($id) 
	{
        $this->load->library('form_validation');
		
		$data = $this->input->post();
		
        $this->form_validation->set_rules(
                    array(
                        array('field'=>'ad_name', 'label'=>'Name', 'rules'=>'required|trim|min_length[2]|xss_clean'),
//                        array('field'=>'ad_location', 'label'=>'Location', 'rules'=>'required|trim'),
                        array('field'=>'ad_status', 'label'=>'Status', 'rules'=>'trim|xss_clean'),
                    )
                );
		
        $this->form_validation->set_error_delimiters('<span class="help-block">','</span>');
        
		if ($this->form_validation->run($this) === FALSE)
		{
			return FALSE;
		}
		
		$ad_image_path = NULL;
		if($this->input->post('ad_image_path') and $data['ad_image_path']['name']) {
			if($result = upload_image('ad_image_path', config_item('ad_image_root'),FALSE)) {
				$ad_image_path = $result;
			} else {
				$this->data['logo_error'] = $this->upload->display_errors('<span class="help-block">','</span>');
				return FALSE;
			}
		} elseif ($id == 0) {
			$this->data['logo_error'] = '<span class="help-block">Advertisement logo is required.</span>';
			return FALSE;			
		}
		
		unset($data['ad_image_path']);
		
		!$ad_image_path || ($data['ad_image_path'] = $ad_image_path);
		
		$data['ad_status'] = $this->input->post('ad_status') ? '1' : '0';
        
        $data['ad_link'] = prep_url($this->input->post('ad_link')); 
		
		if ($id == 0)
		{
			$this->advertisement_model->insert($data);
			$this->session->set_flashdata('success_message','Data inserted successfully.');
			
		}
		else
		{
//            debug($data);die;
			$this->advertisement_model->update($id, $data);
			$this->session->set_flashdata('success_message','Data updated successfully.');
						
		}
		
		redirect('admin/advertisement/','refresh');
		
	}
	
	function toggle_status() 
	{
		if($this->input->is_ajax_request()) {
			$id =  $this->input->post('id');
			$status =  $this->input->post('status') == 'true' ? '1' : '0' ;
			$this->advertisement_model->update($id, array('ad_status'=>$status));
			
			$this->output->set_content_type('application/json')->set_output(json_encode(array('status'=>'ok')));
		}
	}
	
	function delete($id) 
	{
		if($this->advertisement_model->delete($id)) {
			$this->session->set_flashdata('success_message','Data deleted successfully.');			
		} else {
			$this->session->set_flashdata('error_message','Data deletion failed.');			
		}
		redirect('admin/advertisement/','refresh');
	}
	
}
