<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Manage Products</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="product_tbl">
		  <thead>
			<tr>
				<th style="width:30px;" >#</th>
				<th>Product Name</th>
				<th>Price</th>
				<th>Product Details </th>
				<th style="width:125px;" >Product Image </th>
				<th style="width:85px;" >Status</th>
				<th style="width:90px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all">
			<?php if($products): ?>
				<?php foreach($products as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo ++$key; ?></td>
				<td><?php echo $val->product_name; ?></td>
				<td><?php echo $val->product_price; ?></td>
				<td><?php echo character_limiter($val->product_details, 20); ?></td>
				<td style="text-align: center;"><?php if(file_exists(config_item('product_image_path').$val->product_image_path)): ?><a href="<?php echo base_url(config_item('product_image_path').$val->product_image_path); ?>" class="img-popup" title="<?php echo $val->product_name; ?>"><img class="img-thumbnail" src="<?php if(file_exists(config_item('product_image_path').$val->product_image_path)) echo imager(config_item('product_image_path').$val->product_image_path,80, 80); ?>" /></a><?php endif; ?></td>
				<td style="text-align: center;"><input type="checkbox"  <?php echo $val->product_status ? "checked": ''; ?> class="ajax-toggle switch" data-toggle-href="<?php echo base_url('admin/products/toggle_status/'); ?>" data-id="<?php echo $val->product_id; ?>" data-size="mini" data-on-color="success" data-off-color="danger" ></td>
				<td style="text-align: center;">
					<!--<a href="<?php // echo base_url('admin/products/orders/'.$val->product_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Orders"><i class="fa fa-shopping-cart"></i> </a>-->
					<a href="<?php echo base_url('admin/products/edit/'.$val->product_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i> </a>
					<a href="<?php echo base_url('admin/products/delete/'.$val->product_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
				</td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>