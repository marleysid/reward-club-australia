<div class="col-lg-12">
	<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>
		<h5><?php echo $edit ? 'Edit' : 'Add'; ?> product</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<form class="form-horizontal" method="POST" enctype="multipart/form-data">
		  <div class="form-group <?php echo form_error('product_name')?'has-error':''?>">
			<label for="product_name" class="control-label col-lg-3">Name *</label>
			<div class="col-lg-7">
				<input type="text" id="product_name" placeholder="Name" name="product_name" class="form-control" value="<?php echo set_value("product_name", $edit ? $product_detail->product_name : ''); ?>" >
				<?php echo form_error('product_name'); ?>
			</div>
		  </div>
		  <div class="form-group <?php echo form_error('product_price')?'has-error':''?>">
			<label for="product_price" class="control-label col-lg-3">Price *</label>
			<div class="col-lg-7">
				<input type="text" id="product_price" placeholder="Price" name="product_price" class="form-control" value="<?php echo set_value("product_price", $edit ? $product_detail->product_price : ''); ?>" >
				<?php echo form_error('product_price'); ?>
			</div>
		  </div>
		  <div class="form-group">
			<label for="category_id" class="control-label col-lg-3">Category *</label>
			<div class="col-lg-7">
				<?php echo $categories; ?>
				<?php echo form_error('category_id'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group <?php echo form_error('product_details')?'has-error':''?>">
			<label for="product_details" class="control-label col-lg-3">Product detail</label>
			<div class="col-lg-7">
				<textarea id="product_details" placeholder="Product detail" name="product_details" class="form-control" ><?php echo set_value("product_details", $edit ? $product_detail->product_details : ''); ?></textarea>
				<?php echo form_error('product_details'); ?>
			</div>
		  </div>
		  <div class="form-group  <?php echo @$logo_error?'has-error':''?>">
			<label for="product_image_path" class="control-label col-lg-3">Product Image *</label>
			<div class="col-lg-7">
			  <div class="fileinput fileinput-new" data-provides="fileinput">
				<span class="btn btn-default btn-file">
				<span class="fileinput-new">Select Image</span> 
				<span class="fileinput-exists">Change</span> 
				  <input type="file" name="product_image_path">
				</span> 
				<span class="fileinput-filename"></span> 
				<a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a> 
				<?php echo @$logo_error; ?>
			  </div>
			  <?php if($edit and file_exists(config_item('product_image_path').$product_detail->product_image_path)): ?>
				<div>
					<img src="<?php echo imager(config_item('product_image_path').$product_detail->product_image_path,128, 128); ?>" alt="product image" class="img-thumbnail">
					<!--<button style="background: none repeat scroll 0 0 #0189D3;color: #111;padding:0 2px;opacity: 0.5;position: absolute;" class="close" type="button" data-id="31" title="Delete image">×</button>-->
				</div>
				<?php endif; ?>
			</div>
		  </div><!-- /.form-group -->		  
		  <div class="form-group">
			<label for="product_status" class="control-label col-lg-3">Status *</label>
			<div class="col-lg-7">
				<input type="checkbox" name="product_status" class="switch switch-small"  value="1" <?php echo set_checkbox('product_status', '1', ($edit) ? ($product_detail->product_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >
				<?php echo form_error('product_status'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group">
			  <div class="col-sm-offset-3 col-sm-8"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
				  <?php echo anchor('admin/products', 'Cancel', 'class="btn btn-warning"'); ?>
			  </div>
		  </div>
		</form>		
	</div>
</div>
</div>