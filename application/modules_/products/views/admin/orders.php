<div class="col-lg-12">
	<div class="box">
	<header>
		<div class="icons"><i class="fa fa-table"></i></div>
		<h5>Orders</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<table class="table table-bordered table-condensed table-hover table-striped dataTable" id="order_tbl">
		  <thead>
			<tr>
				<th>Order Id</th>
				<!--<th>Product Name</th>-->
				<th>Customer's Name </th>
				<th>Email</th>
				<!--<th>Delivery Address </th>-->
				<!--<th>Contact Number </th>-->
				<th>Amount Paid </th>
				<!--<th>Quantity </th>-->
				<!--<th>Transaction Id </th>-->
				<th>Order Status </th>
				<th>Payment State </th>
				<th>Order Date </th>
				<th style="width:90px;" >Settings</th>
			</tr>
		  </thead>

		<tbody role="alert" aria-live="polite" aria-relevant="all" >
			<?php if($orders): ?>
				<?php foreach($orders as $key=>$val): ?>
			<tr class="odd">
				<td><?php echo $val->order_id; ?></td>
				<!--<td><?php // echo $val->product_name; ?></td>-->
				<td><?php echo $val->name; ?></td>
				<td><?php echo $val->email_address; ?></td>
				<!--<td><?php // echo $val->delivery_address; ?></td>-->
				<!--<td><?php // echo $val->contact_number; ?></td>-->
				<td><?php echo $val->amount; ?></td>
				<!--<td><?php // echo $val->quantity; ?></td>-->
				<!--<td><?php echo $val->txnId; ?></td>-->
				<td><?php echo $val->order_status; ?></td>
				<td><?php echo $val->txnState; ?></td>
				<td><?php echo $val->order_date; ?></td>
				<td style="text-align: center;">
					<a href="<?php echo base_url('admin/products/order/'.$val->order_id); ?>" class="btn btn-default btn-xs btn-round" data-toggle="tooltip" title="View Details"><i class="fa fa-edit"></i> </a>
					<a href="<?php echo base_url('admin/products/delete_order/'.$val->order_id); ?>" class="btn btn-default btn-xs btn-round" onclick='if(!confirm("Are you sure to delete?")) return false;' data-toggle="tooltip" title="Delete"><i class="fa fa-times"></i> </a>
				</td>
			</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
		</table>
		
	</div>
</div>
</div>