<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends MY_Model
{
	public function __construct()
	{        
        parent::__construct();
		$this->table = 'online_orders';
        $this->field_prefix = 'order_';
		$this->log_user = FALSE;
	}
	
	function get_ordered_products($order_id = NULL)
	{
		$result = $this->db->select('*')
			->join('ordered_product', 'products.product_id = ordered_product.product_id')
			->where('order_id',(int) $order_id)
			->get('products');
		
		return $result->num_rows() > 0 ? $result->result() : FALSE;
	}
	
}
