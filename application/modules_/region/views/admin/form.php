<div class="box">
	<header>
		<div class="icons"><i class="fa fa-edit"></i></div>
		<h5><?php echo $edit ? 'Edit' : 'Add'; ?> region</h5>
		<div class="toolbar">
		  <nav style="padding: 8px;">
			<a class="btn btn-default btn-xs collapse-box" href="javascript:;">
			  <i class="fa fa-minus"></i>
			</a> 
			<a class="btn btn-default btn-xs full-box" href="javascript:;">
			  <i class="fa fa-expand"></i>
			</a> 
		  </nav>
		</div>
	</header>
	<div class="body">
		<form class="form-horizontal" method="POST">
		  <div class="form-group <?php echo form_error('region_name')?'has-error':''?>">
			<label for="text1" class="control-label col-lg-4">Name *</label>
			<div class="col-lg-8">
				<input type="text" id="region_name" placeholder="Name" name="region_name" class="form-control" value="<?php echo set_value("region_name", $edit ? $region_detail->region_name : ''); ?>" >
				<?php echo form_error('region_name'); ?>
			</div>
		  </div>
		
		  <div class="form-group">
			<label for="tags" class="control-label col-lg-4">Status </label>
			<div class="col-lg-8">
				<input type="checkbox" name="region_status" class="switch switch-small"  value="1" <?php echo set_checkbox('region_status', '1', ($edit) ? ($region_detail->region_status ? TRUE : FALSE) : TRUE); ?> data-on-color="success" data-off-color="danger" >
				<?php echo form_error('region_status'); ?>
			</div>
		  </div><!-- /.form-group -->
		  <div class="form-group">
			  <div class="col-sm-offset-4 col-sm-9"><input type="submit" name="submit" class="btn btn-primary" value="Submit" />
				  <?php echo $edit ? anchor('admin/region', 'Cancel', 'class="btn btn-warning"'):''; ?>
			  </div>
		  </div>
		</form>		
	</div>
</div>