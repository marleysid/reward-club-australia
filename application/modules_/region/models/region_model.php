<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Region_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'regions';
        $this->field_prefix = 'region_';
        $this->log_user = FALSE;
    }

    function get_regions() {
        $query = $this->db->select('region_id,region_name')
                ->from("regions")
                ->where('region_status', '1')
                ->get();
        return $query->result();
    }

    function get_latlon($suburb, $postcode) {
        $query = $this->db->select("lat,lon")
                ->from("suburbs")
                ->where("suburb = '$suburb' and postcode = '$postcode'")
                ->get();
        return $query->row();
    }

    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
