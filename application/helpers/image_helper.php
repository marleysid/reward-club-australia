<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function imager($img_path, $width, $height, $aspect_ratio = NULL) {
    $CI = &get_instance();

//		$CI->load->library('encryption');
    $no_img = false;
    if (!$img_path or ! file_exists($img_path)){
    	$no_img = true;
        $img_path = 'assets/img/no-image.png';
        }
    $img_fpath = 'imager/';
    $img_fpath .= $width . "-" . $height . ($aspect_ratio ? "-" . $aspect_ratio : '');
    $img_path = $img_path; //$CI->encryption->encode($img_path);
    // return !$no_img ? site_url() . $img_fpath . '/' . $img_path : "http://ci.draftserver.com/rewardsapp/" . $img_fpath . '/' . $img_path ;
    return site_url() . $img_fpath . '/' . $img_path;
}


function newimager($img_path) {
    $CI = &get_instance();
//      $CI->load->library('encryption');
    $no_img = false;
    if (!$img_path or ! file_exists($img_path)){
        $no_img = true;
        $img_path = 'assets/img/no-image.png';
        }
    $img_fpath = 'imager/';
    $img_path = $img_path; //$CI->encryption->encode($img_path);
    // return !$no_img ? site_url() . $img_fpath . '/' . $img_path : "http://ci.draftserver.com/rewardsapp/" . $img_fpath . '/' . $img_path ;
    return site_url() . $img_fpath . '/' . $img_path;
}


/* End of file image_helper.php */
/* Location: ./application/helpers/image_helper.php */
