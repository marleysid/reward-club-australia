<?php
/**
 * TimThumb by Ben Gillbanks and Mark Maunder
 * Based on work done by Tim McDaniels and Darren Hoyt
 * http://code.google.com/p/timthumb/
 * 
 * GNU General Public License, version 2
 * http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 *
 * Examples and documentation available on the project homepage
 * http://www.binarymoon.co.uk/projects/timthumb/
 */

/*
        -----TimThumb CONFIGURATION-----
        You can either edit the configuration variables manually here, or you can 
        create a file called timthumb-config.php and define variables you want
        to customize in there. It will automatically be loaded by timthumb.
        This will save you having to re-edit these variables everytime you download
        a new version of timthumb.

*/
						// Maximum image height
define ('NOT_FOUND_IMAGE', FCPATH.'assets/img/no-image.png');			// Image to serve if any 404 occurs 
define ('ERROR_IMAGE', FCPATH.'assets/img/no-image.png');									// Image to serve if an error occurs instead of showing error message
