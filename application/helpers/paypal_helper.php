<?php

require APPPATH."third_party/paypal/bootstrap.php";

use PayPal\Api\Payment;
use PayPal\Api\Sale;

function get_paypal_payment($paymentId, &$error) 
{
	global $cred;

	Payment::setCredential($cred);    
	error_reporting(0);
    
	try {
		$payment = Payment::get($paymentId);
        
	} catch (\PPConnectionException $ex) {
		$error = $ex->getMessage();
		return FALSE;
	}
	
	return $payment;
	
}
