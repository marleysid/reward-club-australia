<?php
include_once "../libraries/geoip.inc";

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

if (!defined('get_asset')) {

    function get_asset($filePath)
    {
        return base_url($filePath);
    }

}

if (!defined('dd')) {

    function dd($data, $die = true)
    {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
        $die && die;
    }

}

function admin_url($uri = '')
{
    $CI = &get_instance();
    return site_url('admin') . '/' . $uri;
}

function image_url($uri = '')
{
    $CI = &get_instance();
    return base_url() . 'assets/img/' . $uri . '/';
}

function upload_image($image, $target, $thumb = array('dest' => '', 'size' => array('w' => 257, 'h' => 218), 'ratio' => false), $prev_img = null)
{

    $CI = &get_instance();
    initialize_upload($target);
    if ($CI->upload->do_upload($image)) {
        if ($prev_img) {
            if (is_file($target . $prev_img)) {
                @unlink($target . $prev_img);
            }

        }

        $data       = $CI->upload->data();
        $image      = $data['file_name'];
        $image_path = $data['full_path'];
        $image_name = $data['raw_name'];
        $image_ext  = $data['file_ext'];

        if ($thumb) {
//$thumb_size = array('w' => 200, 'h' =>220);
            if ($thumb['dest']) {
                $dest = $thumb['dest'];
            } else {
                $dest = $target;
            }

            create_thumb($image_path, $dest . $image, $thumb['size'], $thumb['ratio']);
        }
        return $image;
    } else {
//        $CI->session->set_flashdata('error_message', $CI->upload->display_errors());
        return false;
//return $CI->upload->display_errors();
    }
}

function initialize_upload($path, $max_size = '0', $max_width = '0', $max_height = '0')
{
    $CI                      = &get_instance();
    $config['upload_path']   = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size']      = $max_size;
    $config['max_width']     = $max_width;
    $config['max_height']    = $max_height;
    $config['encrypt_name']  = true;

    $CI->load->library('upload', $config);
}

function create_thumb($src, $dest, $size, $ratio = false)
{
    $CI = &get_instance();

    $config['image_library'] = 'gd2';
    $config['source_image']  = $src;
    $config['new_image']     = $dest;
    $config['create_thumb']  = true;
    if ($ratio) {
        $config['maintain_ratio'] = true;
    } else {
        $config['maintain_ratio'] = false;
    }

    $config['thumb_marker'] = '';

    $config['width']  = $size['w'];
    $config['height'] = $size['h'];

    $CI->load->library('image_lib');
    $CI->image_lib->initialize($config);

    $CI->image_lib->resize();
}

function upload_file($file, $target, $file_type, $prev_file = null, $max_size = '102400')
{

    $CI                      = &get_instance();
    $config['upload_path']   = $target;
    $config['allowed_types'] = $file_type;
    $config['max_size']      = $max_size;

    $CI->load->library('upload', $config);

    if ($CI->upload->do_upload($file)) {
        if ($prev_file) {
            if (is_file($target . $prev_file)) {
                @unlink($target . $prev_file);
            }

        }

        $data = $CI->upload->data();
        $file = $data['file_name'];

        return $file;
    } else {
        $CI->session->set_flashdata('error_message', $CI->upload->display_errors());
        return false;
//return $CI->upload->display_errors();
    }
}

function generateCaptcha()
{
    $CI = &get_instance();
    $CI->load->plugin('captcha');

    $vals = array(
        'word'        => '',
        'word_length' => 4,
        'img_path'    => './assets/img/captcha/',
        'img_url'     => image_url('captcha'),
        'font_path'   => './system/fonts/georgiab.ttf',
        'img_width'   => '112',
        'img_height'  => 52,
        'expiration'  => 3600,
    );
    $cap = create_captcha($vals);

    return $cap;
}

function unlink_files($fullPathOfFileArray)
{
    foreach ($fullPathOfFileArray as $fullPathOfFile) {
        unlink_file($fullPathOfFile);
    }
}

function unlink_file($fullPathOfFile)
{
    @unlink($fullPathOfFile);
}

function is_active_module($url_module = null)
{
    $CI = &get_instance();

    $module = $CI->router->fetch_module();
    return ($url_module === $module) ? "active" : "";
}

function encryptPassword($password)
{
    if (empty($password)) {
        return false;
    }
    return sha1($password);
}

function safe_b64encode($string)
{

    $data = base64_encode($string);
    $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
    return $data;
}

function safe_b64decode($string)
{
    $data = str_replace(array('-', '_'), array('+', '/'), $string);
    $mod4 = strlen($data) % 4;
    if ($mod4) {
        $data .= substr('====', $mod4);
    }
    return base64_decode($data);
}

// function to geocode address, it will return false if unable to geocode address
function geocode($address)
{

    // url encode the address
    $address = urlencode($address);

    // google map geocode api url
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address={$address}";

    // get the json response
    $resp_json = file_get_contents($url);

    // decode the json
    $resp = json_decode($resp_json, true);

    // response status will be 'OK', if able to geocode given address
    if ($resp['status'] == 'OK') {

        // get the important data
        $lati              = $resp['results'][0]['geometry']['location']['lat'];
        $longi             = $resp['results'][0]['geometry']['location']['lng'];
        $formatted_address = $resp['results'][0]['formatted_address'];

        // verify if data is complete
        if ($lati && $longi && $formatted_address) {

            // put the data in the array
            $data_arr = array();

            array_push(
                $data_arr,
                $lati,
                $longi,
                $formatted_address
            );

            return $data_arr;

        } else {
            return false;
        }

    } else {
        return false;
    }
}

function move_file($source_full_path, $destination_path, $file_name, &$error)
{
    $ext = pathinfo($file_name, PATHINFO_EXTENSION);

    $flag = true;
    do {
        mt_srand();
        $newfilename = md5(uniqid(mt_rand())) . '.' . $ext;

        // check if file exists
        if (!file_exists($destination_path . $newfilename)) {
            $flag = false;
            break;
        }

    } while ($flag);

    if (file_exists($source_full_path . $file_name)) {
        rename($source_full_path . "/" . $file_name, $destination_path . $newfilename);

        return $newfilename;
    }

    $error = 'Image error';

    return '';

}

function _date_is_valid($str)
{
    if (substr_count($str, '/') == 2) {
        list($d, $m, $y) = explode('/', $str);
        return checkdate($m, $d, sprintf('%04u', $y));
    }

    return false;
}

function space2br($str)
{
    return str_replace(' ', '<br />', $str);
}

function getCurrentAddress()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }



if (strlen($ip) >= 10) {
        $details               = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        $latlong               = explode(",", $details->loc);
        if(!isset($_SESSION['latitude']) && !isset($_SESSION['longitude']) && !isset($_SESSION['distance'])){
            $_SESSION['latitude']  = $latlong[0];
            $_SESSION['longitude'] = $latlong[1];
            $_SESSION['distance']  = 15;
        }

        return $details->city . ' ,' . $details->region;
        
 } else {
        $ip = "49.195.15.60";
        $details               = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));
        $latlong               = explode(",", $details->loc);
        if(!isset($_SESSION['latitude']) && !isset($_SESSION['longitude']) && !isset($_SESSION['distance'])){
            $_SESSION['latitude']  = $latlong[0];
            $_SESSION['longitude'] = $latlong[1];
            $_SESSION['distance']  = 15;
        }
        return $details->city . ' ,' . $details->region;
    }
    
}

function get_lat_long_generic($address)
{

    $address = str_replace(" ", "+", $address);

    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
    $json = json_decode($json);

    // echo "<pre>"; print_r($json->results[0]->geometry->location->lat); echo "</pre>"; exit;
    // echo "<pre>"; print_r($json); echo "</pre>"; exit;
    //$lat  = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    //$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
    $lat  = $json->results[0]->geometry->location->lat;
    $long = $json->results[0]->geometry->location->lng;
    return array('lat' => $lat, 'long' => $long);
}
//get offers by id in helpers
function get_offer_by_id($id)
{
    $CI = &get_instance();
    $CI->db->where('offer_id', $id);
    return $CI->db->get('offer_address')->result();
}
