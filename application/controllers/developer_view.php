<?php

if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * @property Webservice_model $webservice_model
 */
class Developer_view extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();		
		
		if(ENVIRONMENT == 'production') {
			exit('Access denied');
		}

	}

	public function index()
	{
        $links = array(
                array('link' => base_url('webservice/recipe_detail'), 'title' => 'Recipe Detail'),
                array('link' => base_url('webservice/recipe_form'), 'title' => 'Recipe List'),
                array('link' => base_url('webservice/featured_recipe'), 'title' => 'Featured Recipe'),
                array('link' => base_url('webservice/get_categories'), 'title' => 'Get Categories'),
                array('link' => base_url('webservice/get_data_form'), 'title' => 'Get Data'),
                array('link' => base_url('webservice/ingredient_form'), 'title' => 'Ingredient Detail'),

                array('link' => base_url('webservice/cms_tips'), 'title' => 'Tips Page Detail'),
                array('link' => base_url('webservice/cms_about'), 'title' => 'About Page Detail'),
//                array('link' =>'', 'title' => ''),
        );
		$this->load->view('developer_view', array('links' => $links));
	}
	
}
