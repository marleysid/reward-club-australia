<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Description of ajax
 *
 * @author rabin
 */
class Ajax extends MX_Controller {

    function __construct() {
        if (!$this->input->is_ajax_request()) {
            $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array()));
            return;
        }
    }

    public function get_sub_category() {
        $parent_id = (int) $this->input->get('category_id');
        $parent_id || $parent_id = -1;
        $this->load->model('categories/category_model');
        $_data = $this->category_model
                ->select('category_id, category_name')
                ->where('category_parent_id', $parent_id)
                ->order_by('category_name')
                ->get_all();
        $data = array();
        
        if ($_data)
            foreach ($_data as $value) {
                $data[] = array(
                            'value' => $value->category_id,
                            'title' => ucfirst($value->category_name)
                        );
            }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

	function get_suburb_postcode()
	{
		$like = strtoupper($this->input->get('query'));
		if($like) {
			// 			print_r($like);
			// 			die;
				
			$select = 'id as id,concat(suburb,"/",postcode) as value';
			if(is_numeric($like)) {
				$res = $this->db->distinct()->select($select, FALSE)->like('postcode', $like)->order_by('suburb')->order_by('postcode')->order_by('state')->get('suburbs')->result();

			} else {
				$like = explode(',', $like);
				$suburb = trim($like[0]);
				$state = isset($like[1]) ? trim($like[1]) : FALSE ;
				$postcode = isset($like[2]) ? trim($like[2]) : FALSE ;

				$this->db->distinct()->select($select, FALSE)->like('suburb', $suburb);
				if($state)
					$this->db->like('state', $state);
				if($postcode)
					$this->db->like('postcode', $postcode);

				$res = $this->db->order_by('suburb')->order_by('postcode')->order_by('state')->get('suburbs')->result_array();

			}
		} else {
			$res[] = NULL;
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('suggestions'=>$res)));

	}
    
	function get_suburbs()
	{
		$like = strtoupper($this->input->get('query'));
		if($like) {
			// 			print_r($like);
			// 			die;
				
			$select = ' distinct concat(suburb) as value';
//			if(is_numeric($like)) {
//				$res = $this->db->distinct()->select($select, FALSE)->like('postcode', $like)->order_by('suburb')->order_by('postcode')->order_by('state')->get('suburbs')->result();
//
//			} else {
				$like = explode(',', $like);
				$suburb = trim($like[0]);
				$state = $this->input->post('state');//isset($like[1]) ? trim($like[1]) : FALSE ;
				$postcode = $this->input->post('post_code');// isset($like[2]) ? trim($like[2]) : FALSE ;

				$this->db->distinct()->select($select, FALSE)->like('suburb', $suburb);
				if($state)
					$this->db->like('state', $state);
				if($postcode)
					$this->db->like('postcode', $postcode);

				$res = $this->db->order_by('suburb')->order_by('postcode')->order_by('state')->get('suburbs')->result_array();

//			}
		} else {
			$res[] = NULL;
		}
		$this->output
			->set_content_type('application/json')
			->set_output(json_encode(array('suggestions'=>$res)));

	}
}
