<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Imager extends CI_Controller {

	/**
	 * Simply redirects all calls to the index() method.
	 *
	 * @param string $file The name of the image to return.
	 */
	public function _remap()
	{
		$this->index();
	}//end _remap()

	//--------------------------------------------------------------------

	/**
	 * Performs the image processing based on the parameters provided and set it in timthumb
	 *
	 * @param $other_params should be in format w-h-q-a-zc-f-s-cc-ct
	 * @param $src source image
	 * TimThumb Parameters
		stands for				|		Values						 |		What it does
		------------------------|------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		src	source				| url to image						 | Tells TimThumb which image to resize › TimThumb basic properties tutorial
		w width					| the width to resize to			 | Remove the width to scale proportionally (will then need the height) › TimThumb width tutorial
		h height				| the height to resize to			 | Remove the height to scale proportionally (will then need the width) › TimThumb height tutorial
		q quality				| 0 – 100							 | Compression quality. The higher the number the nicer the image will look. I wouldn’t recommend going any higher than about 95 else the image will get too large › TimThumb image quality tutorial
		a alignment				| c, t, l, r, b, tl, tr, bl, br		 | Crop alignment. c = center, t = top, b = bottom, r = right, l = left. The positions can be joined to create diagonal positions › TimThumb crop position tutorial
		zc zoom / crop			| 0, 1, 2, 3						 | Change the cropping and scaling settings › TimThumb crop scaling tutorial
		f filters				| too many to mention				 | Let’s you apply image filters to change the resized picture. For instance you can change brightness/ contrast or even blur the image › TimThumb image filter tutorial
		s sharpen				|									 | Apply a sharpen filter to the image, makes scaled down images look a little crisper › tutorial
		cc canvas colour		| hexadecimal colour value (#ffffff) | Change background colour. Most used when changing the zoom and crop settings, which in turn can add borders to the image.
		ct canvas transparency 	| true (1)							 | Use transparency and ignore background colour
	 */
	public function index()
	{
		$this->load->helper('timthumb');
		$other_params = $this->uri->segment(2);
		$params = explode('-',$other_params);
		$src = str_replace('imager/'.$other_params.'/', '', uri_string());
		timthumb::$_get['src'] = base_url($src);
		!isset($params[0]) || empty($params[0]) || timthumb::$_get['w']     = $params[0];
		!isset($params[1]) || empty($params[1]) || timthumb::$_get['h']     = $params[1];
		!isset($params[2]) || empty($params[2]) || timthumb::$_get['zc']    = $params[2];
		!isset($params[3]) || empty($params[3]) || timthumb::$_get['q']     = $params[3];
		!isset($params[4]) || empty($params[4]) || timthumb::$_get['a']     = $params[4];
		!isset($params[5]) || empty($params[5]) || timthumb::$_get['f']     = $params[5];
		!isset($params[6]) || empty($params[6]) || timthumb::$_get['s']     = $params[6];
		!isset($params[7]) || empty($params[7]) || timthumb::$_get['cc']    = $params[7];
		!isset($params[8]) || empty($params[8]) || timthumb::$_get['ct']    = $params[8];
		
		timthumb::start();
	}//end index()

	//--------------------------------------------------------------------

}//end class
