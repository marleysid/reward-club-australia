<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Test extends Front_Controller {

    public function index() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('Test');
        $this->template->render('test');
    }

    public function info_for_clubs() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('Information For Clubs');
        $this->template->render('info_for_clubs');
    }

    public function info_for_suppliers() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('Information For suppliers');
        $this->template->render('info_for_suppliers');
    }
    
    public function contact() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('Contact');
        $this->template->render('contact');
    }
     public function faq() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('FAQ');
        $this->template->render('faq');
    }


    public function deal_page_map() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('FAQ');
        $this->template->render('deal_page_map');
    }
    public function deal_page_list() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('FAQ');
        $this->template->render('deal_page_list');
    }
    public function deal_page() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('FAQ');
        $this->template->render('deal_page');
    }
     public function cartpage() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('Cart');
        $this->template->render('cartpage');
    }
     public function favourite() {
//        $this->load->view('welcome_message');
        $this->template->add_title_segment('Favourite');
        $this->template->render('favourite');
    }
     public function securepay() {
//        $this->load->view('welcome_message');
       
        $this->template->add_title_segment('Securepay');
        $this->template->render('securepay',  $this->data);
        //echo $this->data['paypal'];
    }
    
    function payment(){
        if($this->input->post()){
             $this->load->library('form_validation');
            
            $this->form_validation->CI =& $this;
			$this->form_validation->set_rules(
                array(
                    array('field' => 'items', 'label' => 'First Name', 'rules' => 'trim|required'),
                    
                )
            );
                        $this->form_validation->set_error_delimiters('<p class="error">','</p>');
                        
                        $this->load->library('paypal');
                        $data = array('member_id' => $this->current_member->member_id,
                        'email' =>  $this->current_member->member_email,
                        'firstname' =>  $this->current_member->member_first_name,
                        'lastname' =>  $this->current_member->member_last_name,
                        'delivery_address' => 'abcd',
                        'email_address' =>  $this->current_member->member_email,
                        'contact_number' => '24',
                        'total_amount' => '234234',
                        'amount' => '2424',
                        'printer_amount' => '24',
                        'currency' => 'AUD',
                        'desc' => ' online orders',
                        'other_data' => '',
                        'items' => 'abcd'
                        );
                        
                        $response = $this->paypal->payment($data);
                
                if($response == FALSE) {
                    redirect('rewards/payment_fail');
                    
                } else {
                    
                    $name = $data['firstname'];
                    $delivery_address = $data['delivery_address'];
                    $email_address = $data['email_address'];
                    $contact_number = $data['contact_number'];

                    $total_amount = $data['total_amount'];
                    $amount = $data['amount'];
                    $printer_amount = $data['printer_amount'];
                    
                    $paykey = $response['PayKey'];
                    
                   // $query = $this->db->query("CALL sp_setOnlineOrder ({this->current_member->member_id},'{$name}','{$this->db->escape_like_str($delivery_address)}','{$email_address}','{$contact_number}', '{$paykey}', '{$total_amount}','{$quint_amount}','{$printer_amount}')");

                    redirect($response['RedirectURL']);
                   
                    
                }
			
        }
    }
    
     function get_tweets(){
//    $this->config->load('twitter');
//		$connection = $this->twitteroath->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('request_token'), $this->session->userdata('request_token_secret'));
//                $content = $connection->get('account/verify_credentials');
//                echo $content;
//         $settings = array(
//    'oauth_access_token' => "9KvQ3HA4SUevTar8DLsGtT90lwvuuqp7GYG80FsU",
//    'oauth_access_token_secret' => "wyTMFtAWAA3m3hmSUcindaECtrcEUa3XRXlmhmr6WIiOt",
//    'consumer_key' => "lclEMTZSEAlAw4Su8K5Xw",
//    'consumer_secret' => "BbSc7BVRTmLmVYkP9Cc2vU4HvliyBm3Cf4LEjFBGVQ"
//);
//         $url = 'https://api.twitter.com/1.1/followers/ids.json';
//$getfield = '?username=J7mbo';
//$requestMethod = 'GET';
//$twitter = new TwitterAPIExchange($settings);
//$response = $twitter->setGetfield($getfield)
//    ->buildOauth($url, $requestMethod)
//    ->performRequest();
//
//var_dump(json_decode($response));die;
         $tweets = $this->twitterfetcher->getTweets(array(
    'consumerKey'       => 'lclEMTZSEAlAw4Su8K5Xw',
    'consumerSecret'    => 'BbSc7BVRTmLmVYkP9Cc2vU4HvliyBm3Cf4LEjFBGVQ',
    'accessToken'       => '113386778-9KvQ3HA4SUevTar8DLsGtT90lwvuuqp7GYG80FsU',
    'accessTokenSecret' => 'wyTMFtAWAA3m3hmSUcindaECtrcEUa3XRXlmhmr6WIiOt',
    'usecache'          => '',
    'count'             => 0,
    'numdays'           => 50
));
print_r($tweets[0]->text);
}
   
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */