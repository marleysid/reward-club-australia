<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Welcome extends Front_Controller
{

    public $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('news/news_model', 'categories/category_model', 'deals/deal_model', 'offers/offer_model', 'region/region_model'));
        $this->load->library('pagination');
        $this->load->library('session');
    }

    public function index()
    {

        

//        $this->load->view('welcome_message');
        $this->data['news_detail'] = $this->news_model->news();
        $this->data['categories']  = $this->category_model
            ->select('category_name, category_id')
            ->where('category_status', '1')
            ->where('category_parent_id', '0')
            ->order_by('category_name')
            ->get_all();
        getCurrentAddress();

/*
        echo $loc = $this->session->userdata('suburb');
        $this->session->userdata('customLocation', $loc);*/
        if ($this->input->post()) {
            if (($this->input->post('searchsuburb')) != $this->session->userdata('suburb')){
                $region      = $this->input->post('searchsuburb');
                $this->session->userdata('customLocation', $region);
            } else{
                $region = $this->session->userdata('suburb');
                $this->session->userdata('customLocation', $region);
            }
            $locArray = explode(',', $region);
            $allregion = $locArray['0'];
        
                if ($region != "") {
                    $img = $this->db->query("SELECT * FROM suburbs where suburb = '$region'")->row();
                    
                 /*   if ($img->image_suburb != "");{
                        $this->session->set_userdata('image',$img->image_suburb);
                    }*/

                    if ($this->input->post('searchsuburb') == $this->session->userdata('suburb')){
                        $sub = $this->session->userdata('suburb');
                    } else {
                        $sub = $this->input->post('searchsuburb');
                    }

                    $current_loc = get_lat_long_generic($sub);


                    //echo "<pre>"; print_r($current_loc); exit;
                    $_SESSION['latitude']= $current_loc['lat'];
                    $_SESSION['longitude'] = $current_loc['long'];
                    $_SESSION['distance'] =  $this->input->post('km');  

                    $this->session->set_userdata('distance', $this->input->post('km'));                          
                    $this->session->set_userdata('customLocationId', $region);
                    $this->session->set_userdata('customLocation',$region);
                    $this->session->set_userdata('onlysuburb', $allregion);
                } else {
                    $this->session->unset_userdata('customLocation');
                    $this->session->unset_userdata('customLocationId');
                     $this->session->unset_userdata('image');
                }

                  //echo "<pre>"; print_r($this->session->all_userdata()); exit;

               
               // redirect('/');
        } 

        if (!empty($this->session->userdata('onlysuburb'))){
             $getLocation = $this->region_model->get_region_name($this->session->userdata('onlysuburb'));
             $pCode = $getLocation->postcode;
            /* if (!empty($pCode)){
                $pCode = $getLocation->postcode;
             } else {
                $pCode = "";
             }*/
             $this->session->set_userdata('pcode', $pCode);
        } 

       


        // for featured section
        if(isset($_SESSION['latitude']) && isset($_SESSION['longitude']) && isset($_SESSION['distance']))
            $this->data['is_featured'] = $this->deal_model->is_featured_suburb();
        else
            $this->data['is_featured'] = $this->deal_model->is_featured();
        
        //bottom slider
        if(isset($_SESSION['latitude']) && isset($_SESSION['longitude']) && isset($_SESSION['distance']))
            $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider_suburb();
        else
            $this->data['get_bottom_slider'] = $this->deal_model->get_bottom_slider();
        
        //all home page favorites
        if(isset($_SESSION['latitude']) && isset($_SESSION['longitude']) && isset($_SESSION['distance']))
            $this->data['all_favourites']  = $this->club_model->get_local_clubs();   
        else
            $this->data['all_favourites']  = $this->club_model->get_favourites();

        $this->template->add_title_segment('Welcome');

//        Template::add_js('http://maps.google.com/maps/api/js?sensor=false', TRUE);
        Template::add_js(get_asset('assets/frontend/js/search.js'), true);
        Template::add_js(base_url('assets/frontend/js/cart_favourites.js'));
        //$this->template->render('welcome_message', $data);
        Template::render('welcome_message', $this->data);
    }


    public function resetLocation()
    {

                $this->session->unset_userdata('customLocation');
                $this->session->unset_userdata('pcode');
                redirect('/');
            
    }



  

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
