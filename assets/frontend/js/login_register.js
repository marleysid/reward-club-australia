$(document).ready(function () {
    var url;
    $.validator.setDefaults({
        ignore: []
    });
    var validator = $('#registerform').validate({
        rules: {
            first_name: 'required',
            last_name: 'required',
            suburb:'required',
            email: {
                required: true,
                email: true
            },
            mobile: {
                digits: true
            },
            password: {required: true,
                minlength: 5},
            confirm_password: 'required',
            postcode: 'required',
            terms_conditions: 'required'
        },
        messages: {
            terms_conditions: 'Please agree to our terms and conditions'
        },
        errorPlacement: function (error, element) {

            error.appendTo(element.parent());
//            error.appendTo ( element.parents('#uniform-terms_n_condition').next('.tnc'));
        },
    });

    var validate = $('#loginform').validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: 'required',
        }
    });
    $("#changeloc").validate({
        rules: {
            searchsuburb: {
                required: true,
                searchsuburb: "Please enter the suburb here."
            }
        }
    });
//       });
    $("#closeform").click(function () {

        validator.resetForm();
    });
    $("#closelogin").click(function () {

        validate.resetForm();
    });
    $('#loginreset').click(function () {
        //alert('test');
        validate.resetForm();



    });
    $('#registerreset').click(function () {
        //alert('test');
        validator.resetForm();


    });


    $("#registerform").on('submit', function () {
        console.log("submitted");
        if ($(this).valid()) {
            var registerdata = $("#registerform").serialize();

            if (registerdata) {

                $.ajax({
                    type: 'post',
                    url: base_url + 'user/register',
                    data: registerdata,
                    success: function (data) {
                        //console.log(data);


                        if (data.status == 1) {
                            // window.location.href = base_url
                            $('#successModal').modal('show');
                            $("#registerModal").modal('hide');


                        } else {
                            $('#error_msg').html(data.msg);
                        }

                    },
                    error: function () {

                    },
                    dataType: 'json'


                });
            }
            else {
                alert('No Data filled on Form please fill first');
            }
        }
        return false;

    });
    $("#closesuccessmsg").click(function () {
        window.location.href = base_url
    });

    $("#loginform").on('submit', function () {
        if ($(this).valid()) {
            var logindata = $("#loginform").serialize();
            var redirect_url = $('#redirecturl').val();
//            alert(redirect_url);
            if (logindata) {

                $.ajax({
                    dataType: 'json',
                    type: 'post',
                    url: base_url + 'user/login',
                    data: logindata,
                    success: function (data) {
                        console.log(data.url);
                        if (data.status == 1) {
                            // alert(redirect_url);
//                           window.location.href = window.location.href;
                            if (redirect_url == '') {
//                               alert('test');
                                window.location.href = window.location.href;
                            }
                            else {
                                window.location.href = redirect_url;
                            }

                        } else {
                            $('#login_error_msg').html(data.msg);
                        }
                    },
                    error: function () {
                        return false;
                    }

                });

            }
        }
        return false;
    });

    $('#loginModal').on('show.bs.modal', function (e) {
        // console.log(e);
        validate.resetForm();
        $('#loginform')[0].reset();
        var url = $(e.relatedTarget).data('url');
        $('#redirecturl').val(url);
        //alert(url);
    });

    $('#registerModal').on('show.bs.modal', function (e) {
        // console.log(e);
//      $('#registerform').trigger('#registerreset');
        validator.resetForm();
        $('#registerform')[0].reset();
//$(this).removeData('bs.modal');

    });

    var url;
    $.validator.setDefaults({
        ignore: []
    });
    var editvalidator = $('#editform').validate({
        rules: {
            first_name: 'required',
            last_name: 'required',
            email: {
                required: true,
                email: true
            },
            mobile: {
                digits: true
            },
            password: {required: true,
                minlength: 5},
            confirm_password: 'required',
            postcode: 'required',
            membership_type: 'required'

        }
    });


    var validatornew = $("#changelocation").validate({
        rules: {
            mynewsuburb: {
                required: true
            }
        }
    });

    var passwordvalidate = $('#passwordform').validate({
        rules: {
            password: {required: true,
                minlength: 5},
            confirm_password: 'required',
            old_password: 'required'
        }
    });

    $('#changepassword').click(function (e) {
        $('#editModal').modal('hide');

    });

    $("#passwordform").on('submit', function () {
        if ($(this).valid()) {
            var passworddata = $("#passwordform").serialize();
            //  alert(data);
            if (passworddata) {

                $.ajax({
                    type: 'post',
                    url: base_url + 'login/change_password',
                    data: passworddata,
                    success: function (data) {
//                                    console.log(data);


                        if (data.status == 1) {
                            //  alert('success');
                            // window.location.href = base_url
                            $('#emailModal').modal('hide');
                            $('#passwordsuccessModal').modal('show');


                        } else {
                            $('.password_error_msg').html(data.msg);
                        }

                    },
                    error: function () {
                        //alert('hi');
                    },
                    dataType: 'json'


                });
            }
            else {

            }
        }
        return false;


    });
    $('#emailModal').on('show.bs.modal', function (e) {
        passwordvalidate.resetForm();
        $('#passwordform')[0].reset();

    });




    $("#editform").on('submit', function () {
        $('#editModal').modal('hide');
        if ($(this).valid()) {
            var editeddata = $("#editform").serialize();
            //  alert(data);
            if (editeddata) {

                $.ajax({
                    type: 'post',
                    url: base_url + 'login/edit_profile',
                    data: editeddata,
                    success: function (data) {
                        //console.log(data);


                        if (data.status == 1) {
                            // window.location.href = base_url
                            $('#editsuccessModal').modal('show');


                        } else {
                            $('#edit_error_msg').html(data.msg);
                        }

                    },
                    error: function () {

                    },
                    dataType: 'json'


                });
            }
            else {

            }
        }
        return false;


    });



    $("#close-editsuccessmsg").click(function () {
        window.location.href = window.location.href;
    });

    $("#closeedit").click(function () {

        editvalidator.resetForm();
    });

    $('#register_modal_link').on('click', function () {

        $('#loginModal').modal('hide');
        $('#registerModal').modal('show');
    })

    $('#forget_password').click(function (e) {
        e.preventDefault();
        $('#loginModal').modal('hide');
        $('#forget_passwordModal').modal('show');
        //$('#loginModal').modal('hide');
    });
    $('#go_to_login').click(function (e) {
        e.preventDefault();
        $('#forget_passwordModal').modal('hide');
        $('#loginModal').modal('show');
    });
    $('#forget_passwordform').submit(function (e) {
        e.preventDefault();
        $('#forget_passwordform .error').remove();
        var datas = $('#forget_passwordform').serialize();
        var action_url = base_url + "login/forgot_password";
        $('#forget_password_btn').val('pending...').attr('disabled', true);
        $.ajax({
            url: action_url,
            type: 'POST',
            data: datas,
            dataType: 'json',
            success: function (result) {

                if (result.status == 'success') {
                    $('#forget_password_btn').val('Request password reset').attr('disabled', false);
                    $('#successForgetPasswordModal').modal('show');
                    window.location.href = base_url

                    //alert('success');
                    //$('#add_hosting_div').modal('hide')
                    //  $('#host-id').append("<option value='" + result.insert_data.insert_id + "'>" + result.insert_data.host_name + "</option>");
                    // $('#host-id').val(result.insert_data.insert_id).trigger("chosen:updated");
                } else if (result.status == 'not_found') {
                    $('#user-email').after('<div class="error error-display">Invalid email.</div>');
                    $('#forget_password_btn').val('Request password reset').attr('disabled', false);
                }
                else if (result.status == 'not_sent') {
                    $('#user-email').after('<div class="error error-display">Something went wrong.</div>');
                    $('#forget_password_btn').val('Request password reset').attr('disabled', false);
                }
                else {
                    $.each(result.errors, function (k, v) {
                        $("#" + k).after(v);
                    });
                    $('#forget_password_btn').val('Request password reset').attr('disabled', false);
                }
            }
        });
    });

    $("#openlogin").click(function() {

        $('#loginModal').modal('show');
        $('#changelocation').modal('hide');


    });
});