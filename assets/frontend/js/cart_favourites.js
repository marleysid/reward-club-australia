 $(function() {
       $(".favourite").on('click',function(){
           //alert('hi');
          var id = $(this).data('id');
          var type = $(this).data('type');
          //alert(id);
               if (id) {

                        $.ajax({
                            url:  base_url + 'offers/favourites/' + type + '/' +id,
                            type: 'post',
                            cache: false,
                            success: function(data) {
                                //console.log(data);
                                if(data.status == 1) {
                                  alert("Added to your favourite list");
                                    
                                } else if(data.status == 2 ){
                                     alert("Already Added to your Favourites");
                                }else {
                                    alert("Adding to favourite failed");
                                }

                            },
                            error: function() {
                                alert("Adding to favourite failed");
                            },
                            dataType : 'json'


                        });
                    }
                    else {

                    }
           
       });
       
       $('.add-to-cart').on('click', function(){
           var id = $(this).data('id');
           var type = $(this).data('type');
          // alert(id);
             if (id) {

                        $.ajax({
                            url:  base_url + 'offers/cart/' + type + '/' +id,
                            type: 'post',
                            cache: false,
                            success: function(data) {
                               // alert('success');
                                //console.log(data);
                                if(data.status == 1) {
                                  alert("Added to your cart");
                                    
                                }else if(data.status == 2){
                                     alert("Sorry! Out of Stock");
                                }
                                else if(data.status == 3 ){
                                     alert("Already Added to your cart");
                                }
                                else {
                                     alert("Adding to cart failed");
                                }

                            },
                            error: function() {
                                //alert('failed');
                                alert("Adding to cart failed");
                            },
                            dataType : 'json'


                        });
                    }
                    else {

                    }
       });

    });
