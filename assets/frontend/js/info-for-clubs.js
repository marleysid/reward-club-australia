$(function () {

    var validator = $('#addfacilities').validate({
        rules: {
            label: 'required',
            condition: 'required',
            description: 'required'
        },
        messages: {
            condition: 'Please write the conditions'
        },
    });
    var validator = $('#addevents').validate({
        rules: {
            label: 'required',
            condition: 'required',
            description: 'required'
        },
        messages: {
            condition: 'Please write the conditions'
        },
    });

    var validator = $('#addfeature').validate({
        rules: {
            label: 'required',
            condition: 'required',
            description: 'required'
        },
        messages: {
            condition: 'Please write the conditions'
        },

    });
    var validator = $('#addfeature').validate({
        rules: {
            label: 'required',
            condition: 'required',
            description: 'required'
        },
        messages: {
            condition: 'Please write the conditions'
        },

    });
    var validator = $('#addcontacts').validate({
        rules: {
            email1: 'required',
            email2: 'required',
            domain: 'required',
            phonenumber:'required',
            address:'required'
        },
        messages: {
            address: 'Please write your address'
        },

    });
    var validator = $('#addbusiness').validate({
        rules: {
            business_name: 'required',
            address: 'required',
            postcode: 'required',
            contact_name:'required',
            email:'required',
            phone_number:'required'
        },
        messages: {
            address: 'Please write your address'
        },

    });

    $("#addbusiness").on('submit', function () {
        if ($(this).valid()) {
            var addbusiness = $("#addbusiness").serialize();

            if (addbusiness) {

                $.ajax({
                    type: 'post',
                    url: base_url + 'cms/add_business',
                    data: addbusiness,
                    success: function (data) {
                        //console.log(data);

                        if (data.status == 1) {
                            // window.location.href = base_url
                            $('#myModal').modal('show');
                            $('#submitBusiness').modal('hide');
                        } else {
                            alert("l");
                        }

                    },
                    error: function () {

                    },
                    dataType: 'json'


                });
            }
            else {
                alert('kjkd');
            }
        }
        return false;

    });
    var validator = $('#contact_club').validate({
        rules: {
            name_of_club: 'required',
            street_address: 'required',
            postcode: 'required',
            contact_name:'required',
            email:'required',
            phone_number:'required'
        },
        messages: {
            street_address: 'Please write your address'
        },

    });

    $("#contact_club").on('submit', function () {
        if ($(this).valid()) {
            var contact_club = $("#contact_club").serialize();
alert(contact_club);
            if (contact_club) {

                $.ajax({
                    type: 'post',
                    url: base_url + 'cms/add_contact_club',
                    data: contact_club,
                    success: function (data) {
                        //console.log(data);

                        if (data.status == 1) {
                            // window.location.href = base_url
                            $('#myModal').modal('show');
                            $('#clubContact').modal('hide');
                        } else {
                            alert("l");
                        }

                    },
                    error: function () {

                    },
                    dataType: 'json'


                });
            }
            else {
                alert('kjkd');
            }
        }
        return false;

    });
});