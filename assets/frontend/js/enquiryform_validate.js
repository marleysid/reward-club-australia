   $(function(){
       $('.enquiryform').validate({
           rules: {
            first_name: 'required',
            last_name: 'required',
            email: {
                required: true,
                email: true
            },
            address: 'required',
            contact_no: {
                digits:true
            }
            
            
        },
        messages: {
            
        } 
       }); 
        $(".enquiryform").on('submit',function(){
               if ($(this).valid()) {
               var enquirydata = $(".enquiryform").serialize();
                //  alert(registerdata);
                if (enquirydata) {

                    $.ajax({
                        type: 'post',
                        url: base_url + 'cms/add_enquiry',
                        data: enquirydata,
                        success: function(data) {
                             $('.enquiryform')[0].reset();
                            //console.log(data);
                            if (data.status == 1) {
//                                window.location.href = base_url
                                alert("Successfully posted your enquiry");
                               $("div.custom-checkbox-big-holder").find("svg path").remove();
                            } else {
                                alert("Enquiry failed");
                            }
                            
                            

                        },
                        error: function() {

                        },
                        dataType: 'json'


                    });
                }
                else {

                }
            }
            return false;
        });
    });