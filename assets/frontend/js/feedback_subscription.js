  $(function() {
        $.validator.setDefaults({
            ignore: []
        });
        var feedbackvalidator = $('#feedbackform').validate({
            rules: {
                first_name: 'required',
                email: {
                    required: true,
                    email: true
                },
                feedback: 'required'
            },
            messages: {
                first_name: 'Please fill up your name',
                feedback: 'Message field cannot be empty'

            }
        });
        var joinusvalidate = $('#joinus_form').validate({
            rules: {
                member_name: 'required',
                member_email: {
                    required: true,
                    email: true
                }
                
            },errorElement:'span',
        errorPlacement: function(error, element) {
            error.css({color:'#c9302c'})
             error.appendTo ( element.parent() );
//            error.appendTo ( element.parents('#uniform-terms_n_condition').next('.tnc'));
        },
        });
        
        $("#joinus_form").on('submit',function(){
               if ($(this).valid()) {
               var joinusdata = $("#joinus_form").serialize();
                //  alert(registerdata);
                if (joinusdata) {

                    $.ajax({
                        type: 'post',
                        url: base_url + 'news/subscribe',
                        data: joinusdata,
                        success: function(data) {
                             $('#joinus_form')[0].reset();
                            console.log(data); return false;
                            if (data.status == 1) {
//                                window.location.href = base_url
                                alert("Thank you for your subscription");
                               

                            } else {
                                alert("Already Subscribed");
                            }

                        },
                        error: function() {

                        },
                        dataType: 'json'


                    });
                }
                else {

                }
            }
            return false;
        });
        
        $("#feedbackform").on('submit', function() {
               //alert('hi');
              
            if ($(this).valid()) {
               var feedbackdata = $("#feedbackform").serialize();
                //  alert(registerdata);
                if (feedbackdata) {

                    $.ajax({
                        type: 'post',
                        url: base_url + 'feedback',
                        data: feedbackdata,
                        success: function(data) {
                            //console.log(data);
                            if (data.status == 1) {
//                                window.location.href = base_url
                                 $('#feedbacksuccessModal').modal('show');

                            } else {
                                $('#feedback_error_msg').html(data.msg);
                            }

                        },
                        error: function() {

                        },
                        dataType: 'json'


                    });
                }
                else {

                }
            }
            return false;
        });
        
          $("#close-feedbacksuccessmsg").click(function () {
                    window.location.href = window.location.href;
                });
                
          $('#feed_back_Modal').on('show.bs.modal', function (e) {
                     feedbackvalidator.resetForm();
                $('#feedbackform')[0].reset();
      
                });
                
        
        
        fetch_twitts();
//        setInterval(function() {
//            fetch_twitts();
//        },10000)
        
        function fetch_twitts(){
            
            $.get( base_url + 'twitter/get_latest_tweet/', function( data ) {
                if(data.status == 1) {
                    $('#latest-twits-rewards span').fadeOut(function() {
                        $(this).html(data.tweet).fadeIn()
                    });
                }
            }, "json" );
            
        }
    });