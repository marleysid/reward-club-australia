$(function () {
    //for subcategory ajax
    $("#select-category").change(function () {
        $.getJSON(base_url + 'ajax/get_sub_category', {category_id: $(this).val()}, function (data) {
            var sub_category = $("#select-sub-category");
            var category = $("#select-category");
            sub_category.html("").append("<option value=''></option>");
            $.each(data, function (key, value) {
                sub_category.append("<option value='" + value.value + "'>" + value.title + "</option>");
            });
            sub_category.selectBox("refresh");
//            category.selectBox("refresh");
        });
    });
    $("#current-location").change(function () {
        if ($(this).prop("checked")) {
            defaultData = {status: "ERROR", value: ""};
            if (navigator.geolocation) {
            	function success(pos) {
            		reverseGeoLookup(pos.coords.latitude, pos.coords.longitude);
            	}
            	function fail(error) {
            		afterLocationLoad(defaultData);
            	}
            	// Find the users current position.  Cache the location for 5 minutes, timeout after 6 seconds
            	navigator.geolocation.getCurrentPosition(success, fail, {maximumAge: 500000, enableHighAccuracy: true, timeout: 6000});
            } else {
            	afterLocationLoad(defaultData);
            }
        $('#location').val('');
        }
    });

    $("#search-reset").click(function () {
        console.log($("#select-category option:first").val());
        $("#select-category, #location, #keyword").val('');
        $("#select-category").trigger('change');
        $("#current-location").prop('checked', false);
    });
});

function afterLocationLoad(data) {
    if (data.status == "OK") {
        $("#lat").val(data.location.lat);
        $("#lon").val(data.location.lng);
    }
    else {
        console.log('Error loading your location');
    }
}

/**
 * 
 * @param {type} lon
 * @param {type} lat
 * @returns {undefined}
 */
function reverseGeoLookup(lat, lon) {
    var _return = {};
    _return.status = "ERROR";
    _return.data = "";
    //make a ajax request -- in prod just use whatever libraryyou have to provide this
    //probably jquery's $.get
    var req = new XMLHttpRequest();
    //put the longitude and latitude into the API query
    var key = "AIzaSyDG61Xd75hm_RLx8pIZEDVWFJ-R9G3fTZA";
    var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lon + "&key = " + key;
    req.open("GET", url, true);
    //this is just the result callback -- it's the function arg to $.get, essentially
    req.onreadystatechange = function () {
        if (req.readyState == 4) {
            //again jquery will parse for you, but we want the results field
            var result = JSON.parse(req.response).results;
            //the maps API returns a list of increasingly general results
            //i.e. street, suburb, town, city, region, country
            for (var i = 0, length = result.length; i < length; i++) {
                //each result has an address with multiple parts (it's all in the reference)
                //
                var location = result[i].geometry.location;
                
                        _return.status = "OK";
                        _return.location = location;
//                        return;
//                    
                afterLocationLoad(_return);
//                for (var j = 0; j < result[i].address_components.length; j++) {
//                    var component = result[i].address_components[j]
//                    //if the address component has postal code then write it out
//                    if (~component.types.indexOf("postal_code")) {
//                        //out.innerHTML += component.long_name
//                        console.log(component.long_name);
//                        _return.status = "OK";
//                        _return.value = component.long_name;
////                        return;
//                        afterLocationLoad(_return);
//                    }
//                }
            }
        }

    }
    //dispatch the XHR... just use jquery
    req.send();
}
//
//function lookup(input) {
//    $('li').css('cursor', 'pointer');
//    var inputString = $('#' + input);
//    if (inputString.length == 0) {
//        $('#autoSuggestionsList').hide();
//    } else {
//        $('li').css('cursor', 'pointer');
//        $.post("./offers/search_auto", {
//            search : "" + inputString.val() + ""
//        }, function(data) {
//
//            if (data.length > 0) {
//                // $('html,body').animate({
//                //  scrollTop : $('#showhere').offset().top
//                // }, 1560);
//                $('#autoSuggestionsList').html(data);
//                $('#autoSuggestionsList li').slideDown();
//                $('#autoSuggestionsList li').css('cursor', 'pointer');
//            }
//            $('#keyword').focus(function() {
//                $('#autoSuggestionsList').slideDown("fast");
//            });
//
//            $('#autoSuggestionsList li').click(function() {
//                $('#keyword').val($(this).html())
//                $('#autoSuggestionsList').slideUp("fast");
//            });
//
//            $(window).click(function() {
//                if ($("#keyword").is(":focus")) {
//
//                } else {
//                    $('#autoSuggestionsList').slideUp("fast");
//                }
//            });
//        });
//    }
//}



