jQuery(document).ready(function ($) {
	// $(document).click("select.search-category",function(){
	//     alert("The paragraph was clicked.");
	// });
	if (navigator.userAgent.indexOf('Mac OS X') != -1){
	  $("#home").addClass("mac");
	} else if((navigator.appVersion.indexOf("Linux")!= -1)) {
		 $("body").addClass("mac");
	} else{
	  $("#home").addClass("pc");
	}
	function resize(){
	    var heights = $("div#scrollbar2 .table-wrap").innerHeight();
	    document.getElementById("div#scrollbar2 .viewport").style.height = heights + "px";
	}
	//custom scrollbar
	var $scrollbar = $("#scrollbar1, #scrollbar2, #scrollbar3 ");
  	$scrollbar.tinyscrollbar();
  	
	$(".navbar-toggle").on("click", function () {
        $(this).parent().next().toggleClass('active');
        $(".hasJS").toggleClass('mobilemenu');
    });
    $(".mobile-search-icon").on("click", function(){
    	$(".search-wrapper-header, .mobile-search-icon").toggleClass('active');
    });
	
	wrapme();
	
	$("#accordion").find("div.panel:first-child div.panel-heading a.faq_head_wrp").addClass("is-open");
	$("a.faq_head_wrp").on("click", function(){
		$("a.faq_head_wrp").removeClass("is-open");
		
		if($(this).hasClass("is-open")){
			$(this).removeClass("is-open");
			}
		else{
			$(this).addClass("is-open");
			}			
		});
		
	var  winWidth = $(window).innerWidth();
	if(winWidth > 767){
		var navHeight = $("#navbar-collapse").innerHeight();
		$("#navbar-collapse").find("ul li").css("height", '50px');
	}		

	setTimeout(function(){
        var dealThumbWidth = $("div.deal-lists-holder").find("a.thumb-holder").height();
		 var dealThumbWidth1 = $("div.favourites-list-holder").find("a.thumb-holder").height();
		 var dealThumbWidth2 = $("div.cart-list-holder").find("a.thumb-holder").height();		 
		 
		
        $("div.deal-lists-holder").find("div.with_resuld_board").css("height", dealThumbWidth);
		 $("div.favourites-list-holder").find("div.with_resuld_board").css("height", dealThumbWidth1);
		 $("div.cart-list-holder").find("div.with_resuld_board").css("height", dealThumbWidth2);
        
    }, 3000)    
	
	$(window).resize(function(){
		
	var navHeight = $("#navbar-collapse").innerHeight();
	$("#navbar-collapse").find("ul li").css("height", navHeight);
		
		var dealThumbWidth = $("div.deal-lists-holder").find("a.thumb-holder").height();
		 var dealThumbWidth1 = $("div.favourites-list-holder").find("a.thumb-holder").height();
		 var dealThumbWidth2 = $("div.cart-list-holder").find("a.thumb-holder").height();
		 
		
        $("div.deal-lists-holder").find("div.with_resuld_board").css("height", dealThumbWidth);
		 $("div.favourites-list-holder").find("div.with_resuld_board").css("height", dealThumbWidth1);
		 $("div.cart-list-holder").find("div.with_resuld_board").css("height", dealThumbWidth2);	 

			 wrapme();

		 
		});
		
	$("form").find('label.error').on('click', function(){
		$(this).prev('input.error').focus();
	});
	
    $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 800, 'swing');
    });
    $("#owl-service").owlCarousel(
	{
		autoPlay: true, //Set AutoPlay to 3 seconds
        nav: true, // Show next and prev buttons
        dots: false,
		margin: 28,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 5
            }
        },
        loop: true
		});
    
	 //$("#owl-service_01").owlCarousel(
//	 {
//		 	autoPlay: false, //Set AutoPlay to 3 seconds
//            nav: true, // Show next and prev buttons
//            dots: false,
//            responsive: {
//                0: {
//                    items: 1
//                },
//                600: {
//                    items: 2
//                },
//                1000: {
//                    items: 3
//                },
//                1200: {
//                    items: 5
//                }
//            },
//            loop: true
//		 }
//           );

		 $("#owl-news").owlCarousel({
			autoPlay: true, //Set AutoPlay to 3 seconds
            nav: true, // Show next and prev buttons
            dots: false,
			margin: 49,
            responsive: {
                0: {
                    items: 2.5,
                    margin: 15
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4
                },
                1200: {
                    items: 4
                }
            },
            loop: true
		});

		 $("#inner-slider").owlCarousel(
	{
			autoPlay: true, //Set AutoPlay to 3 seconds
            nav: true, // Show next and prev buttons
            dots: false,
			margin: 9,
            responsive: {
                0: {
                    items: 2.5,
                    margin: 9
                },
                600: {
                    items: 5
                },
                1000: {
                    items: 5
                },
                1200: {
                    items: 5
                }
            },
            loop: true
		});

		 $("#owl-discount").owlCarousel({
			autoPlay: true, //Set AutoPlay to 3 seconds
            nav: true, // Show next and prev buttons
            dots: false,
			margin: 10,
            responsive: {
                0: {
                    items: 3,
                    margin: 5
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 3
                },
                1200: {
                    items: 3
                }
            },
            loop: true
		});
			
		$("#owl-myClub").owlCarousel({
				autoPlay: true, //Set AutoPlay to 3 seconds
	            nav: true, // Show next and prev buttons
	            dots: false,
				margin:0,
	            responsive: {
	                0: {
	                    items: 2,
	                    margin: 0
	                },
	                600: {
	                    items: 4
	                },
	                1000: {
	                    items: 5
	                },
	                1200: {
	                    items: 5
	                }
		            },
		            loop: false
				});
		$("#owl-partner").owlCarousel({
				autoPlay: true, //Set AutoPlay to 3 seconds
	            nav: true, // Show next and prev buttons
	            dots: false,
				margin: 30,
	            responsive: {
	                0: {
	                    items: 3,
	                    margin: 5
	                },
	                600: {
	                    items: 5
	                },
	                1000: {
	                    items: 5
	                },
	                1200: {
	                    items: 5
	                }
		            },
		            loop: true
				});	
	
	 $("#owl-service_01, #owl-service").owlCarousel({
			autoPlay: true, //Set AutoPlay to 3 seconds
            nav: true, // Show next and prev buttons
            dots: false,
			margin: 28,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 5
                },
                1200: {
                    items: 5
                }
            },
            loop: true
		});
	  var items_featured = $("#featureCarousel").find('.item').size();	  
	  $("#featureCarousel").owlCarousel({
		  	autoPlay: true, //Set AutoPlay to 3 seconds
            nav: true, // Show next and prev buttons
            dots: false,
			 responsive:{
            0:{ items: 1 }
        },
            loop: items_featured > 1 ? true:false,
		});
	  var items_local = $("#localClubs").find('.item').size();	
	  $("#localClubs").owlCarousel({
 			nav: true,
	    	slideSpeed : 300,
	    	paginationSpeed : 400,
	    	singleItem:true,
	    	autoPlay: 300,
	    	loop: false,
  		});

	  $("#owl-fullSlider").owlCarousel({
 			nav: true,
	    	slideSpeed : 300,
	    	paginationSpeed : 400,
	    	singleItem:true,
	    	loop: true
  		});
		   
		   
		   $("#mainBannerSlider").owlCarousel({
		  	autoPlay: 3000, //Set AutoPlay to 3 seconds
            nav: true, // Show next and prev buttons
            dots: false,
			 responsive:{
            0:{ items: 1 }
        },
            loop: false,
			
		  });
           
		   
		   
		   
		   $("#mainBannerSlider").on('drag.owl.carousel', function(e){
			   	$("#mainBannerSlider").find("div.owl-controls").hide();	
			   });
			$("#mainBannerSlider").on('dragged.owl.carousel', function(e){
			   	$("#mainBannerSlider").find("div.owl-controls").show();	
			   });
			    $("#mainBannerSlider").on('translate.owl.carousel', function(e){
			   	$("#mainBannerSlider").find("div.owl-controls").hide();	
			   });
			    $("#mainBannerSlider").on('translated.owl.carousel', function(e){
			   	$("#mainBannerSlider").find("div.owl-controls").show();	
			   });

   



    var wow = new WOW({
                boxClass: 'wowload', // animated element css class (default is wow)
                animateClass: 'animated', // animation css class (default is animated)
                offset: 0, // distance to the element when triggering the animation (default is 0)
                mobile: true, // trigger animations on mobile devices (default is true)
                live: true        // act on asynchronously loaded content (default is true)
            });

    wow.init();

    $('.carousel').swipe({
        swipeLeft: function () {
            $(this).carousel('next');
        },
        swipeRight: function () {
            $(this).carousel('prev');
        },
        allowPageScroll: 'vertical'
    });

    $(document).on('click','.error', function(){
        $(this).siblings('input').focus();
    });

});


function wrapme(){
	var divs = $(".grid-list-holder div.grid-box-holder");
	var winWidth = $(window).width();

	if(winWidth < 768){
		for(var i = 0; i < divs.length; i+=3) {
			if ( divs.parent().is( "div.wrap" ) ) {
				divs.unwrap();
			}		
		}
	}else{
        var cnt = 0;
		if($(".grid-box-holder").length > 0){ 
			for(var i = 0; i < divs.length; i+=3) {
				var opclass = (cnt%2 == 0)?'wrap':'wrap';
				divs.slice(i, i+3).wrapAll("<div class='"+ opclass +"'></div>");
				cnt++;
			}
		}
	}
}