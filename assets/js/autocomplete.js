
//loading script dynamically 
function loadjscssfile(filename, filetype) {
    if (filetype == "js") { //if filename is a external JavaScript file
        var fileref = document.createElement('script')
        fileref.setAttribute("type", "text/javascript")
        fileref.setAttribute("src", filename)
    }
    else if (filetype == "css") { //if filename is an external CSS file
        var fileref = document.createElement("link")
        fileref.setAttribute("rel", "stylesheet")
        fileref.setAttribute("type", "text/css")
        fileref.setAttribute("href", filename)
    }
    if (typeof fileref != "undefined")
        document.getElementsByTagName("head")[0].appendChild(fileref)
}

$(document).ready(function(){
    loadjscssfile(base_url+"assets/lib/jQuery-Autocomplete/content/styles.css", "css") ////dynamically load and add this .css file
    
    $script(base_url+"assets/lib/jQuery-Autocomplete/dist/jquery.autocomplete.min.js", function () {
        
       /* $('#location').on('keyup',function() {
            if($.trim(this.value) == '')    $('#suburb_id').val(0);
        }).autocomplete({
            serviceUrl: base_url+'ajax/get_suburb_postcode/',
            minChars:3,
            onSelect: function(data){
                return false;
            }
        });*/
        
        if(typeof _all_postcode_list != 'undefined')
        $('#club_postcode').autocomplete({
            lookupLimit: 20,
            lookup: _all_postcode_list,
        });
        
        
        
//        $('#club_suburbs_select').on('keyup',function() {
////            if($.trim(this.value) == '')    $('#suburb_id').val(0);
//        }).autocomplete({
//            serviceUrl: base_url+'ajax/get_suburb_postcode/',
//            params: {post_code: $('#club_postcode').val(), state: $('#club_state').val()},
//            minChars:3,
//            onSelect: function(data){
////                $(this).parent().find('#suburb_id').val(data.id);
//                return false;
//            }
//        });
        
        
        autoComplete('#club_suburbs_select');
    
        //initialize autocomplete on search tab click event
        $('#club_postcode, #club_state').on('change', function() {
            autoComplete('#club_suburbs_select');
        });
        
        function autoComplete(e) {
            $(e).autocomplete('dispose');
            $(e).keyup(function() {
//                if($.trim(this.value) == '')    $(this).parent().find('.suburb_id').val(0);
            }).autocomplete({
            serviceUrl: base_url+'ajax/get_suburbs/',
            params: {post_code: $('#club_postcode').val(), state: $('#club_state').val()},
            minChars:2,
            onSelect: function(data){
//                $(this).parent().find('#suburb_id').val(data.id);
                return false;
            }
            });

        }
        
    });
});