$(document).ready(function() {

    //"use strict";
$("input.switch").bootstrapSwitch();

$('.ajax-toggle.switch').on('switchChange.bootstrapSwitch', function(event, state) {
    var _this = this,
        _url = $(this).data('toggle-href'),
        _id = $(this).data('id');
    $.ajax({
        url:_url,
        data:{id:_id,status: state},
        type : 'POST',
        success: function(data) {
            if(data.status == 'ok') {
                $.gritter.add({
                    title: 'Success',
                    time: 3000,
                    text: 'Data has been updated'
                });                
            }
            
        }
    });
});

$('.img-popup').fancybox();

$('#region_tbl').dataTable({    
        "sPaginationType": "bootstrap",
        stateSave: true 
});
$('#category_tbl').dataTable({ 
    "sPaginationType": "bootstrap",
    stateSave: true    
});
	
$('#company_tbl').dataTable({    
        "sPaginationType": "bootstrap",
        stateSave: true 
});	
$('#rewards_tbl').dataTable({    
        "sPaginationType": "bootstrap",
        stateSave: true 
});
	
$('#club_tbl').dataTable({    
        "sPaginationType": "bootstrap",
        stateSave: true 
});
	
$('#cms_tbl').dataTable({    
        "sPaginationType": "bootstrap",
        stateSave: true 
});
	
$('#product_tbl').dataTable({    
        "sPaginationType": "bootstrap",
        stateSave: true 
});
	
$('#advertisement_tbl').dataTable({    
        "sPaginationType": "bootstrap",
        stateSave: true 
});
$('#order_tbl').dataTable({    
        "sPaginationType": "bootstrap",
		"aaSorting": [[ 0, "desc" ]],
        stateSave: true 
                
});

});