/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.32 : Database - nepalionlineclass
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`nepalionlineclass` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `nepalionlineclass`;

/*Table structure for table `dictionary` */

DROP TABLE IF EXISTS `dictionary`;

CREATE TABLE `dictionary` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nepali_word` varchar(255) NOT NULL,
  `nepali_eq_english` varchar(255) NOT NULL,
  `english_meaning` varchar(255) NOT NULL,
  `dictionary_status` enum('0','1') DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

/*Data for the table `dictionary` */

insert  into `dictionary`(`id`,`nepali_word`,`nepali_eq_english`,`english_meaning`,`dictionary_status`) values (1,'अग्लो','aglo','tall','1');

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `groups` */

insert  into `groups`(`id`,`name`,`description`) values (1,'admin','Administrator'),(2,'teacher','Teacher'),(3,'student','Student');

/*Table structure for table `login_attempts` */

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `login_attempts` */

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_content` text NOT NULL,
  `page_image` varchar(255) NOT NULL,
  `page_meta_title` varchar(255) NOT NULL,
  `page_meta_description` varchar(255) NOT NULL,
  `page_created_ts` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `page_modified_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `page_created_by` int(11) NOT NULL,
  `page_status` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `review_id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `pages` */

insert  into `pages`(`id`,`page_title`,`page_slug`,`page_content`,`page_image`,`page_meta_title`,`page_meta_description`,`page_created_ts`,`page_modified_ts`,`page_created_by`,`page_status`) values (1,'home','home','<p>c</p>\r\n','','n','n','0000-00-00 00:00:00','2014-05-03 20:14:58',0,'1');

/*Table structure for table `site_setting` */

DROP TABLE IF EXISTS `site_setting`;

CREATE TABLE `site_setting` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_title` varchar(255) NOT NULL,
  `site_meta_keyword` varchar(255) NOT NULL,
  `site_meta_description` text NOT NULL,
  `site_email` varchar(255) NOT NULL,
  `site_google_analytics` text NOT NULL,
  `site_facebook` varchar(255) NOT NULL,
  `site_twitter` varchar(255) NOT NULL,
  `site_instagram` varchar(255) NOT NULL,
  `site_status` enum('0','1') NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `site_setting` */

insert  into `site_setting`(`site_id`,`site_title`,`site_meta_keyword`,`site_meta_description`,`site_email`,`site_google_analytics`,`site_facebook`,`site_twitter`,`site_instagram`,`site_status`) values (1,'nepali online class','nepali online class','nepali online class','info@nepalionlineclass.com','','https://www.facebook.com/','https://twitter.com/','http://instagram.com/','1');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varbinary(16) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`id`,`ip_address`,`username`,`password`,`salt`,`email`,`activation_code`,`forgotten_password_code`,`forgotten_password_time`,`remember_code`,`created_on`,`last_login`,`active`,`first_name`,`last_name`,`company`,`phone`) values (1,'\0\0','administrator','$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36','9462e8eee0','admin@admin.com','',NULL,NULL,'10bd3f40a4ebb18c8e7165019d352680f5f34bc7',1268889823,1399197000,1,'Admin','istrator','ADMIN','0');

/*Table structure for table `users_groups` */

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `users_groups` */

insert  into `users_groups`(`id`,`user_id`,`group_id`) values (1,1,1);

/* Procedure structure for procedure `sp_getAds` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getAds` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getAds`()
BEGIN
	SELECT * FROM `advertisement`
	WHERE `ad_status` = '1';
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getCategory` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getCategory` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getCategory`()
BEGIN	
	SELECT `category_id`,`category_name`, `category_status` FROM `rewards_category`
	WHERE `category_status` = '1';
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getDefaultRegion` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getDefaultRegion` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getDefaultRegion`(
	IN plat DOUBLE,
	IN plon DOUBLE
	)
BEGIN
        
	SELECT `region_id`,`region_name`, `states`.`state` AS `region_state`, `region_status`,
	(((ACOS(SIN((plat*PI()/180)) * 
	SIN((`lat`*PI()/180))+COS((plat*PI()/180)) * 
	COS((`lat`*PI()/180)) * COS(((plon - `lon`)
	*PI()/180))))*180/PI())*60*1.1515) AS distance
	FROM `regions`
	JOIN `states` ON `state_id` = `states`.`id`
	where `region_status` = '1'
	ORDER BY distance
	LIMIT 1;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getMyRewards` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getMyRewards` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getMyRewards`(
    in pregion_id int,
    in pcat_id int,
    in plat double,
    in plon double
    )
BEGIN
    
	if(pregion_id != 0 and pcat_id != 0 ) then
		call sp_getRewards(0, pregion_id, pcat_id);
	else 
		CALL sp_getOfferNearMe(plat,plon);
	end if;
		
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getNearbyRewards` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getNearbyRewards` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getNearbyRewards`(in psuburb_id int)
BEGIN
    
	SELECT `rewards_id`,`rewards_text`,`rewards_name`,cat.`category_name`, `company_name`, rc.`company_id`, `company_logo_path`, `company_address`, `company_phone`, `company_website` FROM `rewards`
	JOIN `rewards_company` AS rc ON rc.`company_id` = `rewards`.`company_id`
	JOIN `rewards_category` AS cat ON cat.`category_id` = `rc`.`category_id`
	join `suburbs` on `suburbs`.`id` = `suburb_id`
	where `suburb_id` = psuburb_id;
	
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getOfferNearMe` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getOfferNearMe` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getOfferNearMe`(
	in plat double,
	in plon DOUBLE
    )
BEGIN    
	SELECT `suburb`,`rewards_id`,`rewards_text`,`rewards_name`,cat.`category_name`, rc.`company_id`, `company_name`, `company_logo_path`, `company_address`, `company_phone`, `company_website`,
	(((acos(sin((plat*pi()/180)) * 
	sin((`lat`*pi()/180))+cos((plat*pi()/180)) * 
	cos((`lat`*pi()/180)) * cos(((plon - `lon`)
	*pi()/180))))*180/pi())*60*1.1515) as distance
	FROM `rewards`
	JOIN `rewards_company` AS rc ON rc.`company_id` = `rewards`.`company_id`
	JOIN `rewards_category` AS cat ON cat.`category_id` = `rc`.`category_id`
	JOIN `suburbs` ON `suburbs`.`id` = `suburb_id`
	order by distance;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getProducts` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getProducts` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getProducts`(
    IN pproduct_id INT,
    IN plimit INT,
    IN poffset INT
    )
BEGIN
	IF pproduct_id != 0 THEN
	
		SELECT `product_id`, `product_name`, `product_price`, `product_details`, `product_image_path`, `category_name` FROM `products` 
		JOIN `rewards_category` ON `rewards_category`.`category_id` = `products`.`category_id`
		WHERE `product_status` = '1' AND `product_id` = pproduct_id;
	ELSE
		SELECT `product_id`, `product_name`, `product_price`, `product_details`, `product_image_path`, `category_name` FROM `products` 
		JOIN `rewards_category` ON `rewards_category`.`category_id` = `products`.`category_id`
		WHERE `product_status` = '1'
		LIMIT poffset, plimit;
	END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getRegion` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getRegion` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getRegion`()
BEGIN
	
	SELECT `region_id`,`region_name`, `state` as `region_state`, `region_status` FROM `regions`
	JOIN `states` ON `state_id` = id
	Where `region_status` = '1';
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getReviews` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getReviews` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getReviews`( in pcompany_id int)
BEGIN
	select `company_name`, `rewards_company`.`company_id`,`rating`, `comment`, `reviewer_email`, `reviewer_name`, `reviewer_phone` from `company_rating` 
	join `rewards_company` on `company_rating`.`company_id` = `rewards_company`. `company_id`
	where `rewards_company`.`company_id` = pcompany_id
	order by `created_date` desc;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getRewardClubs` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getRewardClubs` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getRewardClubs`(
    IN pclub_id INT,
    IN pregion_id INT
    )
BEGIN
	DECLARE pQuery VARCHAR (5000);
	
	IF pclub_id != 0 THEN
		SELECT `club_id`,`club_name`,`club_address`, `club_website`, `club_phone`, `club_price`, `suburb`, `suburbs`.`id` as `suburb_id` FROM `clubs`
		join `suburbs` on `suburbs`.`id` = suburb_id
		WHERE `club_status` = '1' and `club_id` = pclub_id;
	ELSE 
		SET @pQuery = "	SELECT `club_id`,`club_name`,`club_address`, `club_website`, `club_phone`, `club_price`, `suburb`, `suburbs`.`id` as `suburb_id` FROM `clubs`
			JOIN `suburbs` ON `suburbs`.`id` = suburb_id
			WHERE `club_status` = '1'";
		
		IF pregion_id != 0 THEN
			SET @pQuery = CONCAT(@pQuery, " AND region_id = ",pregion_id);
		END IF;
		
		PREPARE stmt FROM @pQuery ;
		EXECUTE stmt ;
		DEALLOCATE PREPARE stmt ;
	END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getRewards` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getRewards` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getRewards`(
    IN preward_id INT,
    IN pregion_id INT,
    IN pcategory_id INT
    )
BEGIN
	DECLARE pQuery VARCHAR (5000);
	
	IF preward_id != 0 THEN
		SELECT `rewards_id`,`rewards_text`,`rewards_name`,cat.`category_name`, rc.`company_id`, `company_name`, `company_logo_path`, `company_address`, `company_phone`, `company_website` FROM `rewards`
		JOIN `rewards_company` as rc ON rc.`company_id` = `rewards`.`company_id`
		JOIN `rewards_category` AS cat ON cat.`category_id` = `rc`.`category_id`
		WHERE `rewards_id` = preward_id and `rewards_status` = '1';
	ELSE 
		SET @pQuery = "	SELECT `rewards_id`,`rewards_text`,`rewards_name`,cat.`category_name`, rc.`company_id`, `company_name`, `company_logo_path`, `company_address`, `company_website`, `company_phone` FROM `rewards`
			JOIN `rewards_company` AS rc  ON rc.`company_id` = `rewards`.`company_id`
			JOIN `rewards_category` AS cat ON cat.`category_id` = `rc`.`category_id`
			WHERE `rewards_status` = '1'";
		
		IF pregion_id != 0 THEN
			SET @pQuery = CONCAT(@pQuery, " AND region_id = ",pregion_id);
		END IF;
		
		IF pcategory_id != 0 THEN 
			SET @pQuery = CONCAT(@pQuery, " AND cat.category_id = ",pcategory_id);		
		END IF;
		
		PREPARE stmt FROM @pQuery ;
		EXECUTE stmt ;
		DEALLOCATE PREPARE stmt ;
	END IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getStates` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getStates` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getStates`()
BEGIN
	select * from `states`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_setMembership` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_setMembership` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_setMembership`(
    pclub_id int,
    pfirst_name varchar(255),
    plast_name varchar(255),
    pdob date,
    pemail VARCHAR(100),
    pcontact_no varchar(255),
    pdetails text,
    ppayment_id varchar(100),
    pamount double(10,2),
    preplydata text,
    ptxnId VARCHAR(100),
    ptxnState VARCHAR(100) 
    )
BEGIN
	DECLARE ret INT DEFAULT 0;
	declare m_id int;
	DECLARE cm_id INT;
	
	IF EXISTS(SELECT `club_id` FROM `clubs` WHERE `club_id` = pclub_id) THEN		
		IF NOT EXISTS (SELECT member_email 
		FROM `membership` 
		JOIN `club_member` ON `membership`.`member_id` = `club_member`.`member_id` AND `club_member`.`club_id` = pclub_id
		WHERE `member_email` = pemail ) THEN
		
			INSERT INTO `db_rewards_club`.`membership`
			( `member_first_name`, `member_last_name`, `member_dob`, `member_email`, `member_contact_no`, `member_details` )
			VALUES ( pfirst_name, plast_name, pdob, pemail, pcontact_no, pdetails );
			
			SET @m_id = LAST_INSERT_ID();
			
			INSERT INTO `db_rewards_club`.`club_member`
			( `member_id`, `club_id`)
			VALUES (@m_id, pclub_id );
        
			SET @cm_id = LAST_INSERT_ID();
			
			INSERT INTO `db_rewards_club`.`membership_payment`
			( `cm_id`, `amount_paid`, `payment_id`, `reply_data`, `txnId`, `txnState` )
			VALUES ( @cm_id , pamount, ppayment_id, preplydata, ptxnId, ptxnState );
        
			SET @ret = 1;
		ELSE 
			SET @ret = 2;
		END IF;
	else 
		SET @ret = 3;
	
	end if;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_setOnlineOrder` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_setOnlineOrder` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_setOnlineOrder`(
	IN pproduct_id INT,
	IN pname VARCHAR(60),
	IN pdaddress VARCHAR(255),
	IN pamount DOUBLE(10,2),
	IN pqntity INT,
	IN pemail_add VARCHAR(255),
	IN pcontactno VARCHAR(255),
	IN paymentid VARCHAR(100),
	IN preplydata TEXT,
	IN ptxnId VARCHAR(100),
	IN ptxnstatus VARCHAR(50)
	)
BEGIN
	DECLARE ret INT DEFAULT 0;
	
	IF EXISTS(SELECT `product_id` FROM `products` WHERE `product_id` = pproduct_id) THEN
		
		INSERT INTO `db_rewards_club`.`online_orders`
		( `product_id`, `name`, `amount`, `quantity`, `delivery_address`, `email_address`, `contact_number`, `payment_id`, `reply_data`, `txnId`, `txnState` )
		VALUES ( pproduct_id, pname, pamount, pqntity, pdaddress, pemail_add, pcontactno, paymentid, preplydata, ptxnId, ptxnstate);
		
		SET @ret = 1;
		
	ELSE
		SET @ret = 0;
	END IF;
	
	SELECT @ret AS `status`;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_setReview` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_setReview` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_setReview`(
	In pcompany_id int,
	in prating varchar (10),
	in pcomment text,
	in pemail varchar (25),
	in pname VARCHAR (100),
	in pphone VARCHAR (25)	
    )
BEGIN
    
	DECLARE ret INT DEFAULT 0;
	
	if exists (select `company_id` from `rewards_company` where `company_id` = pcompany_id ) then 
	-- if company id exists --
		if not exists ( select `company_id` from `company_rating` where `company_id` = pcompany_id and `reviewer_email` = pemail) then
			INSERT INTO `db_rewards_club`.`company_rating`
			( `company_id`, `rating`, `comment`, `reviewer_email`, `reviewer_name`, `reviewer_phone` )
			VALUES (pcompany_id, prating, pcomment, pemail, pname, pphone );        
			SET @ret = 1;
		else 
		-- already reviewed --
			set @ret = 2;
		end if;	
	ELSE 
		SET @ret = 0;
	
	end if;
	
	SELECT @ret AS `status`;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
